//
//  MyConfigure.cpp
//  都市猎人
//
//  Created by 张 强 on 12-8-30.
//
//

#include "MyConfigure.h"
USING_NS_CC;


#define  MaxAnimationNum 41

CCAnimation * CreateAnimation(const char * animationName, float delay)
{
    list <CCSpriteFrame *> FrameList;
    
    char buf[32];
    for(int i = 1; i < MaxAnimationNum; ++ i)
    {
        sprintf(buf, "%s%d.png", animationName, i);
        CCSpriteFrame * pSpriteFrame = CCSpriteFrameCache::sharedSpriteFrameCache() -> spriteFrameByName(buf);
        
        if(pSpriteFrame)    FrameList.push_back(pSpriteFrame);
    }
    
    if(FrameList.empty())
    {
        printf("CreateAnimation : 没有找到名为%s的动画序列帧\n", animationName);
        CCAssert(! FrameList.empty(), "生成动画失败");
    }
    
    //制作动画帧
    CCMutableArray<CCSpriteFrame *> * pArray = new CCMutableArray <CCSpriteFrame *>(FrameList.size());
    for(list <CCSpriteFrame *>::iterator it = FrameList.begin(); it != FrameList.end(); ++ it)
    {
        pArray -> addObject(*it);
    }
    
    //生成动画
    CCAnimation * pAnimation = CCAnimation::animationWithFrames(pArray, delay);
    
    //释放动画帧
    pArray -> release();
    FrameList.clear();
    
    return pAnimation;
}

void ShowMessage(ButtonType buttonType, const char * info, CCCallFunc * Func1, CCCallFunc * Func2, CCCallFunc * Func3)
{
    MessageLayer * message = MessageLayer::node();
    message -> SetMessage(buttonType, info);
    message -> setVertexZ(10.0f);
    message -> SetConfirmFunc(Func1);
    message -> SetGiveUpFunc(Func2);
    message -> SetRetryFunc(Func3);
    
    CCDirector::sharedDirector() -> getRunningScene() -> addChild(message, 20, MessageTag);
}

void ShowNetConnect()
{
    NetConnectLayer * connect = NetConnectLayer::node();
    connect -> setVertexZ(10.0f);
    CCDirector::sharedDirector() -> getRunningScene() -> addChild(connect, 21, ConnectTag);
}

void RemoveNetConnect()
{
    CCDirector::sharedDirector() -> getRunningScene() -> removeChildByTag(ConnectTag, true);
}






























