//
//  MyConfigure.h
//  MidNight
//
//  Created by 强 张 on 12-3-27.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNight_MyConfigure_h
#define MidNight_MyConfigure_h

#include <map>
#include <list>
#include <string>
using namespace std;

#include "cocos2d.h"
#include "MessageLayer.h"
#include "NetConnectLayer.h"


#define  MessageTag   1984823
#define  ConnectTag    2090091

#pragma mark Function
#pragma mark {
cocos2d::CCAnimation * CreateAnimation(const char * animationName, float delay);
void    ShowMessage(ButtonType buttonType, const char * info, cocos2d::CCCallFunc * Func1 = NULL, cocos2d::CCCallFunc * Func2 = NULL, cocos2d::CCCallFunc * Func3 = NULL);

void    ShowNetConnect();
void    RemoveNetConnect();
#pragma mark }


#pragma mark -
#pragma mark -
#pragma mark -


#pragma mark Define
#pragma mark {
/////////////////////////////////////////////////

#define     GET_SET(varType, varName, funName)\
                                    public:  varType Get##funName(void) {return varName;}\
                                    public:  void Set##funName(varType var) {varName = var;}

#define     SS_GET(varType, varName, funName)\
                                     public:  varType Get##funName(void) {return varName;}

#define     SS_SET(varType, varName, funName)\
                                    public:  void Set##funName(varType var) {varName = var;}

/////////////////////////////////////////////////

#define     SAFE_DELETE(var)\
                                    if(var) {delete var; var = NULL;}

/////////////////////////////////////////////////

#define  CHARLENGHT  64


#define  StructCopy(dest, src, size)\
int count = 0;\
char * pd = (char *) dest;\
char * ps = (char *) src;\
while(count != size)\
{\
* pd = * ps;\
ps ++;\
pd ++;\
count ++;\
}



#define RemoveRes(plist, image) \
CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(plist);\
CCTextureCache::sharedTextureCache() -> removeTextureForKey(image);

///////////////////////////////////////////////// － 游戏中id管理

#pragma mark - login场景一级界面对话框ID
const int LOGIN_JOB_SELECT_FRAME_ID     = 5000;        // 职业选择
const int LOGIN_NAME_INPUT_FRAME_ID     = 5001;        // 姓名输入

#pragma mark - Game场景一级界面对话框ID
const int SYSTEM_MENU_FRAME_ID          = 1000;         // 系统设置菜单
const int PLAYER_HOME_FRAME_ID          = 10000;        // 大厅
const int FOLLOWER_CENTER_FRAME_ID      = 10001;        // 英雄中心
const int MISSION_CENTER_FRAME_ID       = 10002;        // 任务所
const int SHOP_FRAME_ID                 = 10003;        // 商城
const int TRAIN_PLATFORM_FRAME_ID       = 10004;        // 站台
const int KTV_FRAME_ID                  = 10005;        // ktv
const int SNOCK_CENTER_FRAME_ID         = 10006;        // 桌球房
const int FITNESS_CENTER_FRAME_ID       = 10007;        // 健身房
const int GAME_CENTER_FRAME_ID          = 10008;        // 游戏机房
const int BAR_FRAME_ID                  = 10009;        // 酒吧
const int GAMBLER_CENTER_FRAME_ID       = 10010;        // 赌场
const int STORE_FRAME_ID                = 10011;        // 便利店
const int CARD_GAME_FRAME_ID            = 10012;        // 桌游房
const int BUILDING_LEVELUP_FRAME_ID     = 20000;        // 房产升级
const int BUILDING_CONSTRUCT_FRAME_ID   = 20001;        // 房产建造
const int SIGN_INFO_FRAME_ID            = 20002;        // 签到信息
const int RESIGN_INFO_FRAME_ID          = 20003;        // 补签信息
const int PACKAGE_EXTEND_FRAME_ID       = 20005;        // 背包扩充

#pragma mark - Game场景二级界面对话框ID
const int FOLLOWER_TEAM_FRAME_ID        = 11000;        // 队伍编辑
const int FOLLOWER_LIST_FRAME_ID        = 11001;        // 小弟列表
const int FOLLOWER_MAIL_CHECK_FRAME_ID  = 11004;        // 邮件查询
const int STRANGER_LIST_FRAME_ID        = 11005;        // 添加陌生人列表
const int FRIEND_FIND_FRAME_ID          = 11006;        // 查找好友
const int FRIEND_SELECT_FRAME_ID        = 11007;        // 好友小弟
const int FOLLOWER_SORT_FRAME_ID        = 11008;        // 小弟排序功能
const int MISSION_LIST_FRAME_ID         = 90000;        // 任务关卡
const int FOLLOWER_BUY_FRAME_ID         = 90002;        // 英雄招募
const int SUPERFOLLOWER_BUY_FRAME_ID    = 90003;        // 超级英雄招募
const int FRIEND_FOLLOWER_FRAME_ID      = 90004;        // 好友小弟列表
const int MAIL_LIST_FRAME_ID            = 90005;        // 邮件列表
const int SET_MENU_FRAME_ID             = 90007;        // 设置菜单
const int PLAYER_ATTR_FRAME_ID          = 90009;        // 玩家属性框

#pragma mark - Game场景三级界面对话框ID
const int FRIEND_FOLLOWER_INFO_FRAME_ID = 12000;        // 好友小弟信息
const int MAP_SECOND_FRAME_ID           = 12001;        // 副本二级关卡
const int FOLLOWER_INFO_FRAME_ID        = 12002;        // 小弟信息
const int FOLLOWER_FULLINFO_FRAME_ID    = 12003;        // 小弟详细信息
const int FOLLOWER_TRAIN_INFO_FRAME_ID  = 12004;        // 小弟训练信息
const int FOLLOWER_JOB_INFO_FRAME_ID    = 12005;        // 小弟转职信息
const int FOLLOWER_FRIEND_EDIT_FRAME_ID = 12007;        // 好友小弟编辑
const int FOLLOWER_JOB_SUCCESS_FRAME_ID = 12008;        // 转职成功信息
const int FOLLOWER_TRAIN_SUCCESS_FRAME_ID = 12009;      // 训练成功信息
const int FRIEND_FIND_RESULT_FRAME_ID   = 12010;        // 好友查找信息
const int FRIEND_REMOVE_CONFIRM_FRAME_ID= 12011;        // 删除好友确认
const int FRIEND_REQUEST_CONFIRM_FRAME_ID=12012;        // 添加好友确认
const int FOLLOWER_FULLINFO2_FRAME_ID   = 12013;        // 小弟详细信息 - 2
const int ACTION_RECOVER_FRAME_ID       = 12014;        // 行动力恢复

#pragma mark - Game场景新手引导对话框ID
const int GAMEGUIDE_FRAME_ID            = 13000;        // 新手引导提示对话框

#pragma mark - 系统界面
const int MESSAGEBOX_FRAME_ID           = 1001;         // 提示对话框
const int PLAYERINFO_FRAME_ID           = 1002;         // 玩家信息
#pragma mark }


// 调试宏定义
#define GAME_DEBUG

#endif
