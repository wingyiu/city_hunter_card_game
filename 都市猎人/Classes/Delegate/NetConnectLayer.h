//
//  NetConnectLayer.h
//  都市猎人
//
//  Created by 张 强 on 12-9-6.
//
//

#ifndef _____NetConnectLayer_h
#define _____NetConnectLayer_h

#include "cocos2d.h"

#define  Net_wait 0

class NetConnectLayer : public cocos2d::CCLayer
{
public:
    virtual bool init();
    
    NetConnectLayer();
    ~NetConnectLayer();
    void   onExit();
    
    LAYER_NODE_FUNC(NetConnectLayer);
    
public:
    virtual bool ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    
private:
    cocos2d::CCSize         m_WinSize;
};

#endif
