//
//  GameController.h
//  MidNightCity
//
//  Created by 强 张 on 12-7-18.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_GameController_h
#define MidNightCity_GameController_h

#include "cocos2d.h"
#include "MyConfigure.h"
USING_NS_CC;

enum GameState
{
    GameState_Invalid = 0,
    GameState_Start,
    GameState_Login,
    GameState_BattleGuide,
    GameState_Game,
    GameState_Battle,
    GameState_BackToGame
};

class GameController 
{
public:
    static GameController * ShareInstance();
    
public:
    void    StartGame();
    void    ReadyToGameScene(GameState state);
    
public:
    SS_GET(GameState, m_GameNextState, GameNextState);
    GET_SET(GameState, m_GameState, GameState);
    
protected:
    static GameController * m_Instance;
    
    GameController();
    ~GameController();
    void Initialize();
    
private:
    GameState       m_GameState;
    GameState       m_GameNextState;
    
private:
    CCScene *       m_Loading;
};

#endif
