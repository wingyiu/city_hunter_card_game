//
//  MessageLayer.h
//  都市猎人
//
//  Created by 张 强 on 12-8-28.
//
//

#ifndef MidNightCity_MessageLayer_h
#define MidNightCity_MessageLayer_h

#include "cocos2d.h"

#define Message_Bg 1
#define Message_Up 2
#define Message_Down 3
#define Message_Left 4
#define Message_Right 5
#define Message_RightUp 6
#define Message_LeftUp 7
#define Message_RightDown 8
#define Message_LeftDown 9
#define Message_Info 10
#define Message_Confirm1 11
#define Message_Confirm2 12
#define Message_GiveUp1 13
#define Message_GiveUp2 14
#define Message_Retry1 15
#define Message_Retry2 16

enum ButtonType
{
    BType_Firm = 0,
    BType_GiveUpAndRetry,
    BType_Retry 
};

class MessageLayer : public cocos2d::CCLayer
{
public:
    virtual bool init();
    
    MessageLayer();
    ~MessageLayer();
    void   onExit();
    
    LAYER_NODE_FUNC(MessageLayer);
    
public:
    void            SetMessage(ButtonType buttonType, const char * message);
    
    inline void     SetConfirmFunc(cocos2d::CCCallFunc * func)  {m_CallBack_Confirm = func;}
    inline void     SetGiveUpFunc(cocos2d::CCCallFunc * func)  {m_CallBack_GiveUp = func;}
    inline void     SetRetryFunc(cocos2d::CCCallFunc * func)  {m_CallBack_Retry = func;}
    
protected:
    void        CloseSelf();
    
    virtual bool ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    virtual void ccTouchMoved(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    virtual void ccTouchEnded(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    
private:
    cocos2d::CCSize         m_WinSize;
    cocos2d::CCPoint        m_BgPoint;
    
    cocos2d::CCCallFunc *   m_CallBack_Confirm;
    cocos2d::CCCallFunc *   m_CallBack_GiveUp;
    cocos2d::CCCallFunc *   m_CallBack_Retry;
    
    cocos2d::CCRect        m_BRect_Confirm;
    cocos2d::CCRect        m_BRect_GiveUp;
    cocos2d::CCRect        m_BRect_Retry;
    
    ButtonType             m_ButtonType;
};

#endif







