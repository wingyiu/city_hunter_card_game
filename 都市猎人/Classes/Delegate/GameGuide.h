//
//  GameGuide.h
//  都市猎人
//
//  Created by 孙 惠伟 on 12-9-27.
//
//

#ifndef _____GameGuide_h
#define _____GameGuide_h

#include "PlayerDataManage.h"
#include "Layer_Building.h"
#include "Layer_GuideMark.h"
#include "ServerDataManage.h"
#include "Wrapper.h"

// 楼层遮罩id
const int FLOOR_GUIDE_MASK_UP = 1;
const int FLOOR_GUIDE_MASK_DOWN = 2;

// 引导标志id
const int GUIDE_MARK = 3;

// 提示类型
enum HINT_TYPE
{
    FLOOR_HINT_1 = 1,
    FLOOR_HINT_2,
    FLOOR_HINT_3,
    FLOOR_HINT_4,
    FLOOR_HINT_5,
    FLOOR_HINT_6,
    FLOOR_HINT_7,
    FLOOR_HINT_8,
    FLOOR_HINT_9,
    FLOOR_HINT_10,
};

// 引导步骤
enum GUIDE_TYPE
{
    GUIDE_FIRST_FLOOR = 1,                      // 一层引导
    GUIDE_TEAMEDIT,                             // 二层 : 队伍编辑
    GUIDE_MISSION,                              // 三层 : 副本
    GUIDE_TRAIN,                                // 二层 : 训练
    GUIDE_MISSION_EXTRA,                        // 三层 : 副本 - 特殊
    GUIDE_SHOP,                                 // 四层 : 商城
    GUIDE_FRIEND,                               // 五层 : 好友
    GUIDE_FLOOR_HAVEST,                         // 六层 : 收获
};

class GameGuide
{
public:
    #pragma mark - 获得单列对象函数
    static GameGuide* GetSingle();
    
    #pragma mark - 初始化引导函数
    void InitGuide();
    
    #pragma mark - 获得是否处于建楼引导函数
    bool GetIsBuildGuide() const                {return m_bIsBuildGuide;}
    
    #pragma mark - 获得引导小步骤函数
    int GetFloorSubGuideStep() const            {return m_iSubFloorGuideStep;}
    
    #pragma mark - 完成一步楼层引导函数
    void FinishOneFloorGuide();
    #pragma mark - 准备下一步楼层引导函数
    void PrepareNextFloorGuide();
    
    #pragma mark - 显示新手引导提示界面函数
    void ShowGuideHintFrame(HINT_TYPE eType);
private:
    #pragma mark - 构造函数
    GameGuide();
    #pragma mark - 析构函数
    ~GameGuide();
    
    #pragma mark - 调整楼层遮罩位置函数
    void AdjustFloorGuideMaskPosition(bool bIsShow, FLOORTYPE eType = F_Hint);
    
    #pragma mark - 建造基础楼层函数
    void CreateBaseFloor();
    
    #pragma mark - 一层引导步骤函数
    void FirstFloorGuideStep();
    #pragma mark - 编队引导步骤函数
    void TeamEditGuideStep();
    #pragma mark - 副本引导步骤函数
    void MissionGuideStep();
    #pragma mark - 训练引导步骤函数
    void TrainGuideStep();
    #pragma mark - 副本特殊引导步骤函数
    void MissionExtraGuideStep();
    #pragma mark - 商店引导步骤函数
    void ShopGuideStep();
    #pragma mark - 好友引导步骤函数
    void FriendGuideStep();
    #pragma mark - 收获引导步骤函数
    void FloorHavestGuideStep();
private:
    // 引导对象变量
    Follower* m_pGuideFollower;                 // 引导小弟
    
    // 引导属性变量
    int m_iSubFloorGuideStep;
    
    bool m_bIsBuildGuide;                       // 建楼引导
    
    CCPoint m_ButtonOffset;                     // 引导按钮偏移
};

#endif
