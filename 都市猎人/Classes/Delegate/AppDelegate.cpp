//
//  MidNightCityAppDelegate.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-3-30.
//  Copyright 恺英网络 2012年. All rights reserved.
//

#include "AppDelegate.h"

#include "cocos2d.h"
USING_NS_CC;

#include "GameController.h"
#include "PlayerDataManage.h"
#include "SimpleAudioEngine.h"
#include "Scene_Loading.h"

// 设备是否支持高清
bool g_bIsEnableRetina = false;  

AppDelegate::AppDelegate()
{

}

AppDelegate::~AppDelegate()
{
}

bool AppDelegate::initInstance()
{
    bool bRet = false;
    do 
    {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)

        // Initialize OpenGLView instance, that release by CCDirector when application terminate.
        // The HelloWorld is designed as HVGA.
        CCEGLView * pMainWnd = new CCEGLView();
        CC_BREAK_IF(! pMainWnd
            || ! pMainWnd->Create(TEXT("cocos2d: Hello World"), 480, 320));

#endif  // CC_PLATFORM_WIN32
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        // OpenGLView is initialized in AppDelegate.mm on ios platform, nothing need to do here.
#endif  // CC_PLATFORM_IOS

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)        
        // OpenGLView is initialized in HelloWorld/android/jni/helloworld/main.cpp
		// the default setting is to create a fullscreen view
		// if you want to use auto-scale, please enable view->create(320,480) in main.cpp
#endif  // CC_PLATFORM_ANDROID
        
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WOPHONE)

        // Initialize OpenGLView instance, that release by CCDirector when application terminate.
        // The HelloWorld is designed as HVGA.
        CCEGLView* pMainWnd = new CCEGLView(this);
        CC_BREAK_IF(! pMainWnd || ! pMainWnd->Create(320,480, WM_WINDOW_ROTATE_MODE_CW));

#ifndef _TRANZDA_VM_  
        // on wophone emulator, we copy resources files to Work7/TG3/APP/ folder instead of zip file
        cocos2d::CCFileUtils::setResource("HelloWorld.zip");
#endif

#endif  // CC_PLATFORM_WOPHONE

#if (CC_TARGET_PLATFORM == CC_PLATFORM_AIRPLAY)
		// MaxAksenov said it's NOT a very elegant solution. I agree, haha
		CCDirector::sharedDirector()->setDeviceOrientation(kCCDeviceOrientationLandscapeLeft);
#endif
        
        bRet = true;
    } while (0);
    return bRet;
}

bool AppDelegate::applicationDidFinishLaunching()
{
	// initialize director
	CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView(&CCEGLView::sharedOpenGLView());

    // enable High Resource Mode(2x, such as iphone4) and maintains low resource on other devices.
    g_bIsEnableRetina = pDirector->enableRetinaDisplay(true);

	// sets landscape mode
    pDirector->setDeviceOrientation(kCCDeviceOrientationPortrait);

	// turn on display FPS
	pDirector->setDisplayFPS(false);

	// set FPS. the default value is 1.0/60 if you don't call this
	pDirector->setAnimationInterval(1.0 / 60);

    printf("now the time is %ld",time(NULL));
    srand((unsigned int)time(NULL));
    
    GameController::ShareInstance() -> StartGame();
    
	return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    CCDirector::sharedDirector()->pause();
    CCDirector::sharedDirector()->stopAnimation();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
    
    // 记录进入后台时间
    time_t t;
    time(&t);
    
    //离开的时间
    m_stEnterBackTime = *gmtime(&t);
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    CCDirector::sharedDirector()->resume();
    CCDirector::sharedDirector()->startAnimation();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    
    // 计算过去的时间
    time_t t;
    time(&t);
    
    //回来的时间
    m_stLeaveBackTime = *gmtime(&t);
    
    if(m_stLeaveBackTime.tm_mday != m_stEnterBackTime.tm_mday)
    {
        if(GameController::ShareInstance() -> GetGameState() != GameState_Start &&
           GameController::ShareInstance() -> GetGameState() != GameState_Login &&
           GameController::ShareInstance() -> GetGameState() != GameState_Battle)
            ServerDataManage::ShareInstance() -> RequestDataUpdate();
    }else
    {
        int iPassSecond = (m_stLeaveBackTime.tm_hour * 60 * 60 + m_stLeaveBackTime.tm_min * 60 + m_stLeaveBackTime.tm_sec) -
                          (m_stEnterBackTime.tm_hour * 60 * 60 + m_stEnterBackTime.tm_min * 60 + m_stEnterBackTime.tm_sec);
        
        //十分钟之内回到游戏中
        if(iPassSecond <= 600)
        {
            // 刷新行动力
            if(PlayerDataManage::m_PlayerActivity < PlayerDataManage::m_PlayerMaxActivity)
            {
                if (iPassSecond >= PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime)
                {
                    PlayerDataManage::m_PlayerActivity += 1;
                    iPassSecond -= PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime;
                    
                    PlayerDataManage::m_PlayerActivity += iPassSecond / 300;
                    PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime = 300 - iPassSecond % 300;
                    
                    if (PlayerDataManage::m_PlayerActivity < PlayerDataManage::m_PlayerMaxActivity)
                    {
                        CCDirector::sharedDirector() -> setActionComeBack(true);
                    }
                    else
                    {
                        PlayerDataManage::m_PlayerActivity = PlayerDataManage::m_PlayerMaxActivity;
                        CCDirector::sharedDirector() -> setActionComeBack(false);
                        PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime = 0;
                    }
                }
                else
                {
                    PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime -= iPassSecond;
                }
            }
            
            // 刷新楼层收获
            PlayerFloorDepot plist = *PlayerDataManage::ShareInstance() -> GetMyFloorDepot();
            for (PlayerFloorDepot::iterator it = plist.begin(); it != plist.end(); it++)
            {
                it->second->Floor_HarvestTime -= iPassSecond;
                if (it->second->Floor_HarvestTime <= 0)
                    it->second->Floor_HarvestTime = 0;
            }
        }
        //超过十分钟之后回到游戏中，就刷新数据
        else
        {
            if(GameController::ShareInstance() -> GetGameState() != GameState_Start &&
               GameController::ShareInstance() -> GetGameState() != GameState_Login)
                ServerDataManage::ShareInstance() -> RequestDataUpdate();
        }
    }
}


