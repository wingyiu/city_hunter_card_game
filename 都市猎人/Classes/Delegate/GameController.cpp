//
//  GameController.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-7-18.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "GameController.h"

#include "Scene_Loading.h"
#include "Scene_LogIn.h"
#include "Scene_Game.h"
#include "Scene_StartGame.h"

#include "SystemDataManage.h"

#include "Wrapper.h"

GameController * GameController::m_Instance = NULL;

GameController * GameController::ShareInstance()
{
    if(m_Instance == NULL)
    {
        m_Instance = new GameController();
        m_Instance -> Initialize();
    }
    return m_Instance;
}

GameController::GameController()
{
    //loading资源加载（永久使用的，不释放）
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("Start-hd.plist");
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("Loading-hd.plist");
    
    //永久场景，不释放
    m_Loading = Scene_Loading::scene();
    m_Loading -> retain();
    
    m_GameState = GameState_Start;
    m_GameNextState = GameState_Invalid;
    
    //CocosDenshion::SimpleAudioEngine::sharedEngine() -> setBackgroundMusicVolume(0);
}

GameController::~GameController()
{
    m_Loading -> removeFromParentAndCleanup(true);
    m_Loading -> release();
}

void GameController::Initialize()
{
    PlayerDataManage::ShareInstance();
    
    // 读取所有配置系统数据
    if (SystemDataManage::ShareInstance() -> LoadData_ForAll() == false)
    {
        printf("读取所有配置系统数据出错");
        return;
    }
}


void GameController::StartGame()
{
    CCScene * pStart = Scene_StartGame::scene();
    
    CCDirector::sharedDirector() -> runWithScene(pStart);
}

void GameController::ReadyToGameScene(GameState state)
{
    if(state != GameState_Invalid)
    {
        m_GameNextState = state;
        m_GameState = state;
    }
    else
        return;
    
    CCTransitionFade * pFade = CCTransitionFade::transitionWithDuration(0.6f, m_Loading);
    CCDirector::sharedDirector() -> pushScene(pFade);
    
    Scene_Loading::ShareInstance() -> ReadyToGameScene();
}





















































