//
//  NetConnectLayer.cpp
//  都市猎人
//
//  Created by 张 强 on 12-9-6.
//
//

#include "NetConnectLayer.h"
#include "MyConfigure.h"
USING_NS_CC;

NetConnectLayer::NetConnectLayer()
{
    m_WinSize = CCDirector::sharedDirector() -> getWinSize();
}

NetConnectLayer::~NetConnectLayer()
{
    printf("NetConnectLayer:: 释放完成\n");
}


void NetConnectLayer::onExit()
{
    CCLayer::onExit();
    
    this -> getChildByTag(Net_wait) -> stopAllActions();
    this -> removeAllChildrenWithCleanup(true);
    
    CCTouchDispatcher::sharedDispatcher() -> removeDelegate(this);
}


bool NetConnectLayer::init()
{
    if(! CCLayer::init())
        return false;
    
    this -> setIsTouchEnabled(true);
    CCTouchDispatcher::sharedDispatcher() -> addTargetedDelegate(this, 0, true);
    
    // 初始化背景
    CCSprite * pBack = CCSprite::spriteWithSpriteFrameName("net_wait_back.png");
    pBack -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2));
    this -> addChild(pBack);
    
    // 初始化动画
    CCSprite * pAnim = CCSprite::spriteWithSpriteFrameName("net_wait_1.png");
    pAnim -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2 - 10.0f));
    this -> addChild(pAnim, 0, Net_wait);
    
    CCAnimation * pAnimation = CreateAnimation("net_wait_", 0.06f);
    CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
    CCRepeatForever * pForever = CCRepeatForever::actionWithAction(pAnimate);
    pAnim -> runAction(pForever);
    
    return true;
}

bool NetConnectLayer::ccTouchBegan(CCTouch * pTouch, CCEvent * pEvent)
{
    return true;
}












