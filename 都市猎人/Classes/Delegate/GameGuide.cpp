//
//  GameGuide.cpp
//  都市猎人
//
//  Created by 孙 惠伟 on 12-9-27.
//
//

#include "GameGuide.h"

/************************************************************************/
#pragma mark - GameGuide类获得单列对象函数
/************************************************************************/
GameGuide* GameGuide::GetSingle()
{
    static GameGuide guide;
    return &guide;
}

/************************************************************************/
#pragma mark - GameGuide类初始化引导函数
/************************************************************************/
void GameGuide::InitGuide()
{
    // 回到楼层最低层
    Layer_Building::ShareInstance()->SetPoint(false);
    
    // 任务层引导特殊处理
    if (PlayerDataManage::m_GuideStepMark == GUIDE_MISSION)
        m_iSubFloorGuideStep = 1;
    
    // 准备引导
    PrepareNextFloorGuide();
}

/************************************************************************/
#pragma mark - GameGuide类完成一步楼层引导函数
/************************************************************************/
void GameGuide::FinishOneFloorGuide()
{
    if (PlayerDataManage::m_GuideStepMark == GUIDE_FIRST_FLOOR)             // 一层引导
        FirstFloorGuideStep();
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_TEAMEDIT)           // 二层 : 队伍编辑
        TeamEditGuideStep();
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_MISSION)            // 三层 : 副本
        MissionGuideStep();
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_TRAIN)              // 二层 : 训练
        TrainGuideStep();
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_MISSION_EXTRA)      // 三层 : 副本 - 特殊
        MissionExtraGuideStep();
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_SHOP)               // 四层 : 商城购买
        ShopGuideStep();
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_FRIEND)             // 五层 : 好友
        FriendGuideStep();
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_FLOOR_HAVEST)       // 六层 : 收获
        FloorHavestGuideStep();
}

/************************************************************************/
#pragma mark - GameGuide类准备下一步楼层引导函数
/************************************************************************/
void GameGuide::PrepareNextFloorGuide()
{
    if (PlayerDataManage::m_GuideStepMark == GUIDE_FIRST_FLOOR)                 // 一层引导
    {
        // 打开提示界面
        ShowGuideHintFrame(FLOOR_HINT_1);
        
        // 建楼引导
        m_bIsBuildGuide = true;
    }
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_TEAMEDIT)            // 二层引导
    {
        // 打开提示界面
        ShowGuideHintFrame(FLOOR_HINT_2);
        
        // 加入点击箭头
        map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
        map<int, Floor*>::iterator iter = floorList.find(F_Follower);
        if (iter != floorList.end())
        {
            CCPoint pos = iter->second->getPosition();
            
            CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(ccp(pos.x, pos.y - 20.0f));
                Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
            }
            else
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(pos);
            }
        }
    }
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_MISSION)            // 三层引导
    {
        // 加入点击箭头
        map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
        map<int, Floor*>::iterator iter = floorList.find(F_Mission);
        if (iter != floorList.end())
        {
            CCPoint pos = iter->second->getPosition();
            
            CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(ccp(pos.x, pos.y - 20.0f));
                Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
            }
            else
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(pos);
            }
        }
        
        // 显示遮罩
        AdjustFloorGuideMaskPosition(true, F_Mission);
    }
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_TRAIN)            // 训练引导
    {
        ShowGuideHintFrame(FLOOR_HINT_4);
        
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 加入点击箭头
        map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
        map<int, Floor*>::iterator iter = floorList.find(F_Follower);
        if (iter != floorList.end())
        {
            CCPoint pos = iter->second->getPosition();
            
            CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(ccp(pos.x, pos.y - 20.0f));
                Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
            }
        }
        
        m_iSubFloorGuideStep = -1;
        
        // 显示遮罩
        AdjustFloorGuideMaskPosition(true, F_Follower);
    }
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_MISSION_EXTRA)            // 训练后副本
    {
        ShowGuideHintFrame(FLOOR_HINT_5);
        
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 加入点击箭头
        map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
        map<int, Floor*>::iterator iter = floorList.find(F_Mission);
        if (iter != floorList.end())
        {
            CCPoint pos = iter->second->getPosition();
            
            CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(ccp(pos.x, pos.y - 20.0f));
                Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
            }
        }
        
        // 显示遮罩
        AdjustFloorGuideMaskPosition(true, F_Mission);
    }
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_SHOP)            // 商城
    {
        if (m_iSubFloorGuideStep == 0)
        {
            ShowGuideHintFrame(FLOOR_HINT_6);
        }
        else if (m_iSubFloorGuideStep == 2)
        {
            // 加入点击箭头
            map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
            map<int, Floor*>::iterator iter = floorList.find(F_Shop);
            if (iter != floorList.end())
            {
                CCPoint pos = iter->second->getPosition();
                
                CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    Layer_GuideMark::GetSingle()->setPosition(ccp(pos.x, pos.y - 20.0f));
                    Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
                }
            }
            
            // 显示遮罩
            AdjustFloorGuideMaskPosition(true, F_Shop);
        }
    }
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_FRIEND)
    {
        if (m_iSubFloorGuideStep == 0)
        {
            ShowGuideHintFrame(FLOOR_HINT_7);
            
            // 加入点击箭头
            map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
            map<int, Floor*>::iterator iter = floorList.find(F_Hint);
            if (iter != floorList.end())
            {
                CCPoint pos = iter->second->getPosition();
                
                CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    Layer_GuideMark::GetSingle()->setPosition(ccp(pos.x, pos.y - 20.0f));
                    Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
                }
                else
                {
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    Layer_GuideMark::GetSingle()->setPosition(pos);
                }
            }
            
            m_bIsBuildGuide = true;
        }
        else if (m_iSubFloorGuideStep == 2)
        {
            // 加入点击箭头
            map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
            map<int, Floor*>::iterator iter = floorList.find(F_Platform);
            if (iter != floorList.end())
            {
                CCPoint pos = iter->second->getPosition();
                
                CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    Layer_GuideMark::GetSingle()->setPosition(ccp(pos.x, pos.y - 20.0f));
                    Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
                }
                else
                {
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    Layer_GuideMark::GetSingle()->setPosition(pos);
                }
            }
            
            // 显示遮罩
            AdjustFloorGuideMaskPosition(true, F_Platform);
        }
    }
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_FLOOR_HAVEST)
    {
        if (m_iSubFloorGuideStep == 0)
        {
            // 加入点击箭头
            map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
            map<int, Floor*>::iterator iter = floorList.find(F_Store);
            if (iter != floorList.end())
            {
                CCPoint pos = iter->second->getPosition();
                
                CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    Layer_GuideMark::GetSingle()->setPosition(ccp(pos.x, pos.y - 20.0f));
                    Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
                }
                else
                {
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    Layer_GuideMark::GetSingle()->setPosition(pos);
                }
            }
            
            // 显示遮罩
            AdjustFloorGuideMaskPosition(true, F_Store);
        }
    }
}

/************************************************************************/
#pragma mark - GameGuide类显示新手引导提示界面函数
/************************************************************************/
void GameGuide::ShowGuideHintFrame(HINT_TYPE eType)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(GAMEGUIDE_FRAME_ID);
    if (pFrame != NULL)
    {
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20000));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            // 筛选图片文字
            switch (eType) {
                case FLOOR_HINT_1:
                case FLOOR_HINT_2:
                case FLOOR_HINT_3:
                case FLOOR_HINT_4:
                case FLOOR_HINT_5:
                case FLOOR_HINT_6:
                case FLOOR_HINT_7:
                case FLOOR_HINT_8:
                case FLOOR_HINT_9:
                case FLOOR_HINT_10:
                    sprintf(szTemp, "hint_word_%d.png", eType);
                    pLabel->resetLabelImage(szTemp);
                    break;
                default:
                    break;
            }
        }
    }
    
    // 打开引导对话框
    KNUIFunction::GetSingle()->OpenFrameByJumpType(GAMEGUIDE_FRAME_ID);
}

/************************************************************************/
#pragma mark - GameGuide类构造函数
/************************************************************************/
GameGuide::GameGuide()
{
    // 为引导对象变量赋初值
    
    // 为引导属性变量赋初值
    m_iSubFloorGuideStep = 0;
    
    m_bIsBuildGuide = false;
    
    m_ButtonOffset = ccp(20.0f, -30.0f);
}

/************************************************************************/
#pragma mark - GameGuide类析构函数
/************************************************************************/
GameGuide::~GameGuide()
{
    
}

/************************************************************************/
#pragma mark - GameGuide类调整楼层遮罩位置函数
/************************************************************************/
void GameGuide::AdjustFloorGuideMaskPosition(bool bIsShow, FLOORTYPE eType)
{
    // 获得楼层对象
    Layer_Building* pBuilding = Layer_Building::ShareInstance();
    if (pBuilding == NULL)
        return;
    
    // 是否有遮罩层
    CCLayerColor* pFloorMaskUp = NULL;
    CCLayerColor* pFloorMaskDown = NULL;
    CCNode* pNode = pBuilding->getChildByTag(FLOOR_GUIDE_MASK_UP);
    if (pNode == NULL)
    {
        pFloorMaskUp = CCLayerColor::layerWithColorWidthHeight(ccc4(0, 0, 0, 100), 320.0f, 480.0f);
        pFloorMaskUp->setVertexZ(0.3f);
        pBuilding->addChild(pFloorMaskUp, 1, FLOOR_GUIDE_MASK_UP);
    }
    else
    {
        pFloorMaskUp = dynamic_cast<CCLayerColor*>(pNode);
    }
    pNode = pBuilding->getChildByTag(FLOOR_GUIDE_MASK_DOWN);
    if (pNode == NULL)
    {
        pFloorMaskDown = CCLayerColor::layerWithColorWidthHeight(ccc4(0, 0, 0, 100), 320.0f, 480.0f);
        pFloorMaskDown->setVertexZ(0.3f);
        pBuilding->addChild(pFloorMaskDown, 1, FLOOR_GUIDE_MASK_DOWN);
    }
    else
    {
        pFloorMaskDown = dynamic_cast<CCLayerColor*>(pNode);
    }
    
    pFloorMaskUp->setIsVisible(bIsShow);
    pFloorMaskDown->setIsVisible(bIsShow);
    
    // 是否显示
    if (!bIsShow)
        return;
    
    // 获得楼层列表
    map<int, Floor*> floorList = pBuilding->GetFloorList();
    // 找到楼层位置
    CCPoint pos(0.0f, 0.0f);
    
    map<int, Floor*>::iterator iter = floorList.find(eType);
    if (iter != floorList.end())
    {
        pos = iter->second->getPosition();
        
        if (eType == F_Player)
        {
            // 更新遮罩位置
            pFloorMaskUp->setPosition(ccp(pos.x - 160.0f, pos.y + 54.0f));
            pFloorMaskDown->setPosition(ccp(pos.x - 160.0f, pos.y - 59.0f - 480.0f));
        }
        else if (eType == F_Platform)
        {
            // 更新遮罩位置
            pFloorMaskUp->setPosition(ccp(pos.x - 160.0f, pos.y + 54.0f));
            pFloorMaskDown->setPosition(ccp(pos.x - 160.0f, pos.y - 58.5f - 480.0f));
        }
        else if (eType == F_Hint)
        {
            // 更新遮罩位置
            pFloorMaskUp->setPosition(ccp(pos.x - 160.0f, pos.y + 66.0f));
            pFloorMaskDown->setPosition(ccp(pos.x - 160.0f, pos.y - 72.5f - 480.0f));
        }
        else
        {
            // 更新遮罩位置
            pFloorMaskUp->setPosition(ccp(pos.x - 160.0f, pos.y + 29.0f));
            pFloorMaskDown->setPosition(ccp(pos.x - 160.0f, pos.y - 58.5f - 480.0f));
        }
    }
}

/************************************************************************/
#pragma mark - GameGuide类建造基础楼层函数
/************************************************************************/
void GameGuide::CreateBaseFloor()
{
    FLOORTYPE eType;
    
    // 只建造基础楼层
    if (PlayerDataManage::m_GuideStepMark == GUIDE_FIRST_FLOOR)
        eType = F_Follower;
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_TEAMEDIT)
        eType = F_Mission;
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_MISSION)
        eType = F_Mission;
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_SHOP)
        eType = F_Shop;
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_FRIEND)
        eType = F_Platform;
    else if (PlayerDataManage::m_GuideStepMark == GUIDE_FLOOR_HAVEST)
        eType = F_Store;
    else
        return;
    
    // 是否已经存在
    PlayerFloorDepot plist = *PlayerDataManage::ShareInstance()->GetMyFloorDepot();
    PlayerFloorDepot::iterator iter = plist.find(eType);
    if (iter != plist.end())
        return;
    
    // 建造该楼层
    const FloorData * pFloorData = SystemDataManage::ShareInstance() -> GetData_ForFloor(eType, 1);
    if (pFloorData != NULL)
    {
        //建造新楼层
        PlayerDataManage::ShareInstance()->CreateNewFloor(eType, 1);
        
        //添加到场景
        KNUIFunction::GetSingle()->AddFloorToGameScene(eType);
    }
}

/************************************************************************/
#pragma mark - GameGuide类一层引导步骤函数
/************************************************************************/
void GameGuide::FirstFloorGuideStep()
{
    if (m_iSubFloorGuideStep == 0)
    {
        // 加入点击箭头
        map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
        map<int, Floor*>::iterator iter = floorList.find(F_Hint);
        if (iter != floorList.end())
        {
            CCPoint pos = iter->second->getPosition();
            
            CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(pos);
                Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
        
        // 显示遮罩
        AdjustFloorGuideMaskPosition(true, F_Hint);
    }
    else if (m_iSubFloorGuideStep == 1)
    {
        // 移出点击箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        // 重置小步骤
        m_iSubFloorGuideStep = 0;
        
        // 建立下一个基础楼层
        CreateBaseFloor();
        
        // 建楼引导
        m_bIsBuildGuide = false;
        
        // 递增步骤
        PlayerDataManage::m_GuideStepMark += 1;
        
        // 记录步骤并发送给服务器
        SaveGameGuideInfo(PlayerDataManage::m_GuideStepMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
        ServerDataManage::ShareInstance()->RequestGuideDoneReqest(PlayerDataManage::m_GuideStepMark);
        
        // 关闭遮罩
        AdjustFloorGuideMaskPosition(false);
    }
}

/************************************************************************/
#pragma mark - GameGuide类编队引导步骤函数
/************************************************************************/
void GameGuide::TeamEditGuideStep()
{
    if (m_iSubFloorGuideStep == 0)
    {
        ++m_iSubFloorGuideStep;
        
        // 显示遮罩
        AdjustFloorGuideMaskPosition(true, F_Follower);
    }
    else if (m_iSubFloorGuideStep == 1)
    {
        // 移出点击箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        // 加入箭头，更换队长
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
        if (pFrame != NULL)
        {
            pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20004));
                if (pLabel != NULL)
                {
                    CCPoint pos = pLabel->GetBackSprite()->getPosition();
                    Layer_GuideMark::GetSingle()->setPosition(ccpAdd(pos, m_ButtonOffset));
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
                }
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 2)
    {
        CCNode* pNode = NULL;
        
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
        if (pFrame != NULL)
        {
            pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 加入箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(110, -100), m_ButtonOffset));
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 3)
    {
        CCNode* pNode = NULL;
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 加入箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
            if (pList != NULL)
            {
                pNode = pList->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    m_pGuideFollower = pList->GetGuideFollower();
                    
                    Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(105.0f, 0.0f), m_ButtonOffset));
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    
                    m_pGuideFollower->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK + 100);
                    
                    pList->SetPoint(false);
                }
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 4)
    {
        // 移出箭头
        CCNode* pNode = m_pGuideFollower->getChildByTag(GUIDE_MARK + 100);
        if (pNode != NULL)
            m_pGuideFollower->removeChild(pNode, true);
        
        // 加入箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(0.0f, -130.0f), m_ButtonOffset));
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 5)
    {
        // 移出点击箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 提示
        ShowGuideHintFrame(FLOOR_HINT_3);
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 6)
    {
        // 加入点击箭头
        map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
        map<int, Floor*>::iterator iter = floorList.find(F_Hint);
        if (iter != floorList.end())
        {
            CCPoint pos = iter->second->getPosition();
            
            CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(pos);
                Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
            }
        }
        
        m_iSubFloorGuideStep = 0;
        m_bIsBuildGuide = true;
        PlayerDataManage::m_GuideStepMark += 1;
        
        // 显示遮罩
        AdjustFloorGuideMaskPosition(true, F_Hint);
        
        // 记录步骤并发送给服务器
        SaveGameGuideInfo(PlayerDataManage::m_GuideStepMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
        ServerDataManage::ShareInstance()->RequestGuideDoneReqest(PlayerDataManage::m_GuideStepMark);
    }
}

/************************************************************************/
#pragma mark - GameGuide类副本引导步骤函数
/************************************************************************/
void GameGuide::MissionGuideStep()
{
    if (m_iSubFloorGuideStep == 0)
    {
        // 移出箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        // 建造楼房
        CreateBaseFloor();
        
        // 建楼引导
        m_bIsBuildGuide = false;
        ++m_iSubFloorGuideStep;
        
        // 关闭遮罩
        AdjustFloorGuideMaskPosition(false);
    }
    else if (m_iSubFloorGuideStep == 1)
    {
        // 移出箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        // 加入箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
            if (pList != NULL)
            {
                pNode = pList->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(0.0f, 155.0f), m_ButtonOffset));
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    pList->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
                }
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 2)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
            if (pList != NULL)
            {
                CCNode* pNode = pList->getChildByTag(GUIDE_MARK);
                if (pNode != NULL)
                    pList->removeChild(pNode, true);
            }
        }
        
        // 添加箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(MAP_SECOND_FRAME_ID);
        if (pFrame != NULL)
        {
            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20000));
            if (pLabel != NULL)
            {
                CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    CCPoint pos = pLabel->GetBackSprite()->getPosition();
                    Layer_GuideMark::GetSingle()->setPosition(ccpAdd(pos, m_ButtonOffset));
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
                }
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 3)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MAP_SECOND_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 添加箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_SELECT_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
            if (pList != NULL)
            {
                CCNode* pNode = pList->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    CCPoint newButtonOffect = ccp(m_ButtonOffset.x, m_ButtonOffset.y + 20.0f);
                    Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(105.0f, 130.0f), newButtonOffect));
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    pList->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
                }
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 4)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_SELECT_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
            if (pList != NULL)
            {
                CCNode* pNode = pList->getChildByTag(GUIDE_MARK);
                if (pNode != NULL)
                    pList->removeChild(pNode, true);
            }
        }
        
        // 添加箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20010));
            if (pLabel != NULL)
            {
                CCPoint pos = pLabel->GetBackSprite()->getPosition();
                
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(pos, m_ButtonOffset));
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 5)
    {
        PlayerDataManage::m_GuideStepMark += 1;
        
        m_iSubFloorGuideStep = 0;
        
        // 记录步骤并发送给服务器
        SaveGameGuideInfo(PlayerDataManage::m_GuideStepMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
        ServerDataManage::ShareInstance()->RequestGuideDoneReqest(PlayerDataManage::m_GuideStepMark);
    }
}

/************************************************************************/
#pragma mark - GameGuide类训练引导步骤函数
/************************************************************************/
void GameGuide::TrainGuideStep()
{
    if (m_iSubFloorGuideStep == -1)
    {
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 0)
    {
        // 移出点击箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        // 加入箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
        if (pFrame != NULL)
        {
            pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
                if (pLabel != NULL)
                {
                    CCPoint pos = pLabel->GetBackSprite()->getPosition();
                    Layer_GuideMark::GetSingle()->setPosition(ccpAdd(pos, m_ButtonOffset));
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
                }
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 1)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 加入箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(-110, -100), m_ButtonOffset));
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 2)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 加入箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
            if (pList != NULL)
            {
                CCNode* pNode = pList->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    m_pGuideFollower = pList->GetGuideFollower();
                    
                    Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(105.0f, 0.0f), m_ButtonOffset));
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    
                    m_pGuideFollower->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK + 100);
                    
                    pList->SetPoint(false);
                }
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 3)
    {
        // 移出箭头
        CCNode* pNode = m_pGuideFollower->getChildByTag(GUIDE_MARK + 100);
        if (pNode != NULL)
            m_pGuideFollower->removeChild(pNode, true);
        
        // 加入箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(115.0f, -2.0f), m_ButtonOffset));
                Layer_GuideMark::GetSingle()->ShowGuideMark();
                
                pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 4)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 加入箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20021));
            if (pLabel != NULL)
            {
                CCPoint pos = pLabel->GetBackSprite()->getPosition();
                
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(pos, m_ButtonOffset));
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 5)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 加入箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_SUCCESS_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(0.0f, -130.0f), m_ButtonOffset));
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 6)
    {
        KNUIFunction::GetSingle()->CloseFrameByNormalType(FOLLOWER_TRAIN_INFO_FRAME_ID);
        
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_SUCCESS_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 加入箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(0.0f, -130.0f), m_ButtonOffset));
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 7)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        ShowGuideHintFrame(FLOOR_HINT_5);
        
        m_iSubFloorGuideStep = 0;
        PlayerDataManage::m_GuideStepMark += 1;
        
        // 记录步骤并发送给服务器
        SaveGameGuideInfo(PlayerDataManage::m_GuideStepMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
        ServerDataManage::ShareInstance()->RequestGuideDoneReqest(PlayerDataManage::m_GuideStepMark);
    }
}

/************************************************************************/
#pragma mark - GameGuide类副本特殊引导步骤函数
/************************************************************************/
void GameGuide::MissionExtraGuideStep()
{
    if (m_iSubFloorGuideStep == 0)
    {
        // 加入箭头
        map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
        map<int, Floor*>::iterator iter = floorList.find(F_Mission);
        if (iter != floorList.end())
        {
            CCPoint pos = iter->second->getPosition();
            
            CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(ccp(pos.x, pos.y - 20.0f));
                Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
        
        // 打开遮罩
        AdjustFloorGuideMaskPosition(true, F_Mission);
    }
    else if (m_iSubFloorGuideStep == 1)
    {
        // 移出箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        // 加入箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
            if (pList != NULL)
            {
                pNode = pList->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(0.0f, 155.0f), m_ButtonOffset));
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    pList->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
                }
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 2)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
            if (pList != NULL)
            {
                CCNode* pNode = pList->getChildByTag(GUIDE_MARK);
                if (pNode != NULL)
                    pList->removeChild(pNode, true);
            }
        }
        
        // 添加箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(MAP_SECOND_FRAME_ID);
        if (pFrame != NULL)
        {
            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20000));
            if (pLabel != NULL)
            {
                CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    CCPoint pos = pLabel->GetBackSprite()->getPosition();
                    Layer_GuideMark::GetSingle()->setPosition(ccpAdd(pos, m_ButtonOffset));
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
                }
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 3)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MAP_SECOND_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 添加箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_SELECT_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
            if (pList != NULL)
            {
                CCNode* pNode = pList->getChildByTag(GUIDE_MARK);
                if (pNode == NULL)
                {
                    CCPoint newButtonOffect = ccp(m_ButtonOffset.x, m_ButtonOffset.y + 20.0f);
                    
                    Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(105.0f, 130.0f), newButtonOffect));
                    Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                    pList->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
                }
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 4)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_SELECT_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
            if (pList != NULL)
            {
                CCNode* pNode = pList->getChildByTag(GUIDE_MARK);
                if (pNode != NULL)
                    pList->removeChild(pNode, true);
            }
        }
        
        // 添加箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20010));
            if (pLabel != NULL)
            {
                CCPoint pos = pLabel->GetBackSprite()->getPosition();
                
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(pos, m_ButtonOffset));
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 5)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        PlayerDataManage::m_GuideStepMark += 1;
        m_iSubFloorGuideStep = 0;
        
        // 记录步骤并发送给服务器
        SaveGameGuideInfo(PlayerDataManage::m_GuideStepMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
        ServerDataManage::ShareInstance()->RequestGuideDoneReqest(PlayerDataManage::m_GuideStepMark);
    }
}

/************************************************************************/
#pragma mark - GameGuide类商店引导步骤函数
/************************************************************************/
void GameGuide::ShopGuideStep()
{
    if (m_iSubFloorGuideStep == 0)
    {
        // 加入点击箭头
        map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
        map<int, Floor*>::iterator iter = floorList.find(F_Hint);
        if (iter != floorList.end())
        {
            CCPoint pos = iter->second->getPosition();
            
            CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(ccp(pos.x, pos.y - 20.0f));
                Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
            }
        }
        
        m_bIsBuildGuide = true;
        
        ++m_iSubFloorGuideStep;
        
        // 显示遮罩
        AdjustFloorGuideMaskPosition(true, F_Hint);
    }
    else if (m_iSubFloorGuideStep == 1)
    {
        // 移出箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        CreateBaseFloor();
        m_bIsBuildGuide = false;
        
        ++m_iSubFloorGuideStep;
        
        // 显示遮罩
        AdjustFloorGuideMaskPosition(false);
    }
    else if (m_iSubFloorGuideStep == 2)
    {
        // 移出箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        // 加入箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_BUY_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(-90, -88), m_ButtonOffset));
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 3)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_BUY_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 加入箭头
        pFrame = KNUIManager::GetManager()->GetFrameByID(SHOP_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(0.0f, -130.0f), m_ButtonOffset));
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                pFrame->addChild(Layer_GuideMark::GetSingle(), 1, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 4)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(SHOP_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        
        // 加入点击箭头
        map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
        map<int, Floor*>::iterator iter = floorList.find(F_Hint);
        if (iter != floorList.end())
        {
            CCPoint pos = iter->second->getPosition();
            
            CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(pos);
                Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
            }
            else
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(pos);
            }
        }
        
        PlayerDataManage::m_GuideStepMark += 1;
        
        ShowGuideHintFrame(FLOOR_HINT_7);
        
        m_iSubFloorGuideStep = 0;
        
        // 记录步骤并发送给服务器
        SaveGameGuideInfo(PlayerDataManage::m_GuideStepMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
        ServerDataManage::ShareInstance()->RequestGuideDoneReqest(PlayerDataManage::m_GuideStepMark);
    }
}

/************************************************************************/
#pragma mark - GameGuide类好友引导步骤函数
/************************************************************************/
void GameGuide::FriendGuideStep()
{
    if (m_iSubFloorGuideStep == 0)
    {
        ++m_iSubFloorGuideStep;
        
        m_bIsBuildGuide = true;
        
        // 显示遮罩
        AdjustFloorGuideMaskPosition(true, F_Hint);
    }
    else if (m_iSubFloorGuideStep == 1)
    {
        // 移出箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        CreateBaseFloor();
        
        m_bIsBuildGuide = false;
        ++m_iSubFloorGuideStep;
        
        // 关闭遮罩
        AdjustFloorGuideMaskPosition(false);
    }
    else if (m_iSubFloorGuideStep == 2)
    {
        // 移出箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        ShowGuideHintFrame(FLOOR_HINT_9);
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 3)
    {
        // 加入点击箭头
        map<int, Floor*> floorList = Layer_Building::ShareInstance()->GetFloorList();
        map<int, Floor*>::iterator iter = floorList.find(F_Hint);
        if (iter != floorList.end())
        {
            CCPoint pos = iter->second->getPosition();
            
            CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(ccp(pos.x + 80.0f, pos.y));
                Layer_Building::ShareInstance()->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
            }
            else
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(pos);
            }
        }
        
        // 打开遮罩
        AdjustFloorGuideMaskPosition(true, F_Hint);
        
        ++m_iSubFloorGuideStep;
        
        m_bIsBuildGuide = true;
    }
    else if (m_iSubFloorGuideStep == 4)
    {
        // 移出箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        // 请求建造楼层
        ServerDataManage::ShareInstance()->RequestNewFloor(F_Store);
        
        // 重置建楼标志
        m_bIsBuildGuide = false;
        
        ++m_iSubFloorGuideStep;
        
        // 关闭遮罩
        AdjustFloorGuideMaskPosition(false);
        
        PlayerDataManage::m_GuideStepMark += 1;
        m_iSubFloorGuideStep = 0;
        
        // 完成引导
        SaveGameGuideInfo(PlayerDataManage::m_GuideStepMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
        ServerDataManage::ShareInstance()->RequestGuideDoneReqest(PlayerDataManage::m_GuideBattleMark);
    }
}

/************************************************************************/
#pragma mark - GameGuide类收获引导步骤函数
/************************************************************************/
void GameGuide::FloorHavestGuideStep()
{
    if (m_iSubFloorGuideStep == 0)                              // 提示框步骤
    {
        ShowGuideHintFrame(FLOOR_HINT_9);
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 1)
    {
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 2)
    {
        // 移出箭头
        CCNode* pNode = Layer_Building::ShareInstance()->getChildByTag(GUIDE_MARK);
        if (pNode != NULL)
            Layer_Building::ShareInstance()->removeChild(pNode, true);
        
        // 添加箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(BUILDING_LEVELUP_FRAME_ID);
        if (pFrame != NULL)
        {
            pNode = pFrame->GetSubUIByID(GUIDE_MARK);
            if (pNode == NULL)
            {
                Layer_GuideMark::GetSingle()->ShowGuideMark(TOUCH);
                Layer_GuideMark::GetSingle()->setPosition(ccpAdd(ccp(70.0f, 45.0f), m_ButtonOffset));
                pFrame->addChild(Layer_GuideMark::GetSingle(), 0, GUIDE_MARK);
            }
        }
        
        ++m_iSubFloorGuideStep;
    }
    else if (m_iSubFloorGuideStep == 3)
    {
        // 移出箭头
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(BUILDING_LEVELUP_FRAME_ID);
        if (pFrame != NULL)
        {
            CCNode* pNode = pFrame->getChildByTag(GUIDE_MARK);
            if (pNode != NULL)
                pFrame->removeChild(pNode, true);
        }
        Layer_GuideMark::GetSingle()->release();
        
        ShowGuideHintFrame(FLOOR_HINT_10);
        
        ++m_iSubFloorGuideStep;
        
        PlayerDataManage::m_GuideStepMark += 1;
        PlayerDataManage::m_GuideMark = false;
        
        Layer_Building* pBuilding = Layer_Building::ShareInstance();
        pBuilding->removeChild(pBuilding->getChildByTag(FLOOR_GUIDE_MASK_UP), true);
        pBuilding->removeChild(pBuilding->getChildByTag(FLOOR_GUIDE_MASK_DOWN), true);
        
        // 关闭房产升级
        KNUIFunction::GetSingle()->CloseFrameByJumpType(BUILDING_LEVELUP_FRAME_ID);
        
        // 记录步骤并发送给服务器
        SaveGameGuideInfo(PlayerDataManage::m_GuideStepMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
        ServerDataManage::ShareInstance()->RequestGuideDoneReqest(PlayerDataManage::m_GuideStepMark);
    }
}