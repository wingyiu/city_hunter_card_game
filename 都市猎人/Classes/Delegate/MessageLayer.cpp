//
//  MessageLayer.cpp
//  都市猎人
//
//  Created by 张 强 on 12-8-28.
//
//

#include "MessageLayer.h"
USING_NS_CC;

MessageLayer::MessageLayer()
{
    m_ButtonType = BType_Firm;
    
    m_WinSize = CCDirector::sharedDirector() -> getWinSize();
    m_BgPoint = CCPointMake(m_WinSize.width / 2, m_WinSize.height / 2);
    
    m_CallBack_Confirm = NULL;
    m_CallBack_GiveUp = NULL;
    m_CallBack_Retry = NULL;
}

MessageLayer::~MessageLayer()
{
    printf("MessageLayer:: 释放完成\n");
}


void MessageLayer::onExit()
{
    CCLayer::onExit();
    
    if(m_CallBack_Confirm)
        m_CallBack_Confirm -> release();
    if(m_CallBack_GiveUp)
        m_CallBack_GiveUp -> release();
    if(m_CallBack_Retry)
        m_CallBack_Retry -> release();
    
    this -> removeAllChildrenWithCleanup(true);
}


bool MessageLayer::init()
{
    if(! CCLayer::init())
        return false;

    this -> setIsTouchEnabled(true);
    CCTouchDispatcher::sharedDispatcher() -> addTargetedDelegate(this, 0, true);
    
    CCSprite * pBG = CCSprite::spriteWithSpriteFrameName("message_back.png");
    pBG -> setPosition(m_BgPoint);
    this -> addChild(pBG, 0, Message_Bg);
    
    CCSprite * pUp = CCSprite::spriteWithSpriteFrameName("message_horizontal_up.png");
    this -> addChild(pUp, 0, Message_Up);
    
    CCSprite * pDown = CCSprite::spriteWithSpriteFrameName("message_horizontal_down.png");
    this -> addChild(pDown, 0, Message_Down);
    
    CCSprite * pLeft = CCSprite::spriteWithSpriteFrameName("message_vertical.png");
    this -> addChild(pLeft, 0, Message_Left);
    
    CCSprite * pRight = CCSprite::spriteWithSpriteFrameName("message_vertical.png");
    this -> addChild(pRight, 0, Message_Right);
    
    CCSprite * pRightUpCorner = CCSprite::spriteWithSpriteFrameName("message_right_up.png");
    this -> addChild(pRightUpCorner, 0, Message_RightUp);
    
    CCSprite * pLeftUpCorner = CCSprite::spriteWithSpriteFrameName("message_left_up.png");
    this -> addChild(pLeftUpCorner, 0, Message_LeftUp);
    
    CCSprite * pRightDownCorner = CCSprite::spriteWithSpriteFrameName("message_right_down.png");
    this -> addChild(pRightDownCorner, 0, Message_RightDown);
    
    CCSprite * pLeftDownCorner = CCSprite::spriteWithSpriteFrameName("message_left_down.png");
    this -> addChild(pLeftDownCorner, 0, Message_LeftDown);
    
    return true;
}


void MessageLayer::SetMessage(ButtonType buttonType, const char * message)
{
    // 使用什么按钮
    m_ButtonType = buttonType;
    
    // 计算字符框大小
    CCSize fontSize(0.0f, 0.0f);
    
    // 设置宽度，为9个汉字
    fontSize.width = 140.0f;
    
    // 设置高度
    fontSize.height = (strlen(message) / 27 + 1) * 20.0f;
    
    // 生成新文本对象
    CCLabelTTF * pText = CCLabelTTF::labelWithString(message, fontSize, CCTextAlignmentCenter, "黑体", 15);
    if (pText != NULL)
        this -> addChild(pText, 0, Message_Info);
    
    // 背景尺寸 : 对齐其他控件使用
    CCSize backSize(0.0f, 0.0f);
    // 缩放系数
    float fScaleX = 0.7f;
    float fScaleY = 0.5f;
    
    // 背景缩进
    CCSprite * pBG = dynamic_cast<CCSprite *>(this -> getChildByTag(Message_Bg));
    if (pBG != NULL)
    {
        // 还原对话框缩放系数
        pBG -> setScale(1.0f);
        
        // 计算y轴缩放信息
        if (fontSize.height < 60.0f)
            fontSize.height = 60.0f;
        fScaleY = fontSize.height * 3.5f / (pBG -> getContentSize().height * 2);
        
        // 设置文本位置
        pText -> setPosition(ccp(m_BgPoint.x, m_BgPoint.y + pBG -> getContentSize().height / 2.0f * fScaleY - fontSize.height / 2.0f));
        
        // 缩放背景
        pBG -> setScaleX(fScaleX);
        pBG -> setScaleY(fScaleY);
        
        // 获得背景尺寸
        backSize = pBG -> getContentSize();
        backSize.width *= fScaleX;
        backSize.height *= fScaleY;
    }
    
    // 左上角
    CCSprite * pLeftUpCorner = dynamic_cast<CCSprite *>(this -> getChildByTag(Message_LeftUp));
    if (pLeftUpCorner != NULL)
        pLeftUpCorner -> setPosition(ccp(m_BgPoint.x - backSize.width / 2.0f + 15.0f, m_BgPoint.y + backSize.height / 2.0f - 15.0f));
    
    // 右上角
    CCSprite * pRightUpCorner = dynamic_cast<CCSprite *>(this -> getChildByTag(Message_RightUp));
    if (pRightUpCorner != NULL)
        pRightUpCorner -> setPosition(ccp(m_BgPoint.x + backSize.width / 2.0f - 15.0f, m_BgPoint.y + backSize.height / 2.0f - 15.0f));
    
    // 左下角
    CCSprite * pLeftDownCorner = dynamic_cast<CCSprite *>(this -> getChildByTag(Message_LeftDown));
    if (pLeftDownCorner != NULL)
        pLeftDownCorner -> setPosition(ccp(m_BgPoint.x - backSize.width / 2.0f + 15.0f, m_BgPoint.y - backSize.height / 2.0f + 15.0f));
    
    // 右下角
    CCSprite * pRightDownCorner = dynamic_cast<CCSprite *>(this -> getChildByTag(Message_RightDown));
    if (pRightDownCorner != NULL)
        pRightDownCorner -> setPosition(ccp(m_BgPoint.x + backSize.width / 2.0f - 15.0f, m_BgPoint.y - backSize.height / 2.0f + 15.0f));
    
    // x轴缩进 - 上条
    CCSprite * pUp = dynamic_cast<CCSprite *>(this -> getChildByTag(Message_Up));
    if (pUp != NULL)
    {
        pUp -> setScaleX(fScaleX);
        pUp -> setPosition(ccp(m_BgPoint.x, m_BgPoint.y + backSize.height / 2.0f));
    }
    
    // x轴缩进 - 下条
    CCSprite * pDown = dynamic_cast<CCSprite *>(this -> getChildByTag(Message_Down));
    if (pDown != NULL)
    {
        pDown -> setScaleX(fScaleX);
        pDown -> setPosition(ccp(m_BgPoint.x, m_BgPoint.y - backSize.height / 2.0f));
    }
    
    // y轴缩进 - 左边
    CCSprite * pLeft = dynamic_cast<CCSprite *>(this -> getChildByTag(Message_Left));
    if (pLeft != NULL)
    {
        pLeft -> setScaleY(fScaleY);
        pLeft -> setPosition(ccp(m_BgPoint.x - backSize.width / 2.0f, m_BgPoint.y));
    }
    
    // y轴缩进 - 右边
    CCSprite * pRight = dynamic_cast<CCSprite *>(this -> getChildByTag(Message_Right));
    if (pRight != NULL)
    {
        pRight -> setScaleY(fScaleY);
        pRight -> setPosition(ccp(m_BgPoint.x + backSize.width / 2.0f, m_BgPoint.y));
    }
    
    float y = m_BgPoint.y - backSize.height / 2.0f + 26.0f;
    if(buttonType == BType_Firm)
    {
        // 确定按钮
        CCSprite * pConfirm1 = CCSprite::spriteWithSpriteFrameName("button_confirm_normal.png");
        CCSprite * pConfirm2 = CCSprite::spriteWithSpriteFrameName("button_confirm_select.png");
        pConfirm2 -> setOpacity(0);
        
        pConfirm1 -> setPosition(ccp(m_BgPoint.x, y));
        pConfirm2 -> setPosition(ccp(m_BgPoint.x, y));
        
        CCPoint  pPoint = pConfirm1 -> getPosition();
        CCSize   pSize = pConfirm1 -> getContentSize();
        m_BRect_Confirm = CCRectMake(pPoint.x - pSize.width / 2, pPoint.y - pSize.height / 2, pSize.width, pSize.height);
        
        this -> addChild(pConfirm1, 0, Message_Confirm1);
        this -> addChild(pConfirm2, 0, Message_Confirm2);
    }
    else if(buttonType == BType_GiveUpAndRetry)
    {
        // 放弃按钮
        CCSprite * pGiveUp1 = CCSprite::spriteWithSpriteFrameName("button_giveup_normal.png");
        CCSprite * pGiveUp2 = CCSprite::spriteWithSpriteFrameName("button_giveup_select.png");
        pGiveUp2 -> setOpacity(0);

        pGiveUp1 -> setPosition(ccp(m_BgPoint.x - backSize.width / 2.0f + pGiveUp1 -> getTextureRect().size.width / 2 + 10, y));
        pGiveUp2 -> setPosition(ccp(m_BgPoint.x - backSize.width / 2.0f + pGiveUp1 -> getTextureRect().size.width / 2 + 10, y));
        
        CCPoint  pPoint1 = pGiveUp1 -> getPosition();
        CCSize   pSize1 = pGiveUp1 -> getContentSize();
        m_BRect_GiveUp = CCRectMake(pPoint1.x - pSize1.width / 2, pPoint1.y - pSize1.height / 2, pSize1.width, pSize1.height);
        
        this -> addChild(pGiveUp1, 0, Message_GiveUp1);
        this -> addChild(pGiveUp2, 0, Message_GiveUp2);
        
        // 重试按钮 
        CCSprite * pRetry1 = CCSprite::spriteWithSpriteFrameName("button_retry_normal.png");
        CCSprite * pRetry2 = CCSprite::spriteWithSpriteFrameName("button_retry_select.png");
        pRetry2 -> setOpacity(0);
        
        pRetry1 -> setPosition(ccp(m_BgPoint.x + backSize.width / 2.0f - pRetry1 -> getTextureRect().size.width / 2 - 10, y));
        pRetry2 -> setPosition(ccp(m_BgPoint.x + backSize.width / 2.0f - pRetry1 -> getTextureRect().size.width / 2 - 10, y));
        
        CCPoint  pPoint2 = pRetry1 -> getPosition();
        CCSize   pSize2 = pRetry1 -> getContentSize();
        m_BRect_Retry = CCRectMake(pPoint2.x - pSize2.width / 2, pPoint2.y - pSize2.height / 2, pSize2.width, pSize2.height);
        
        this -> addChild(pRetry1, 0, Message_Retry1);
        this -> addChild(pRetry2, 0, Message_Retry2);
    }
    else if(buttonType == BType_Retry)
    {
        // 重试按钮
        CCSprite * pRetry1 = CCSprite::spriteWithSpriteFrameName("button_retry_normal.png");
        CCSprite * pRetry2 = CCSprite::spriteWithSpriteFrameName("button_retry_select.png");
        pRetry2 -> setOpacity(0);
        
        pRetry1 -> setPosition(ccp(m_BgPoint.x, y));
        pRetry2 -> setPosition(ccp(m_BgPoint.x, y));
        
        CCPoint  pPoint = pRetry1 -> getPosition();
        CCSize   pSize = pRetry1 -> getContentSize();
        m_BRect_Retry = CCRectMake(pPoint.x - pSize.width / 2, pPoint.y - pSize.height / 2, pSize.width, pSize.height);
        
        this -> addChild(pRetry1, 0, Message_Retry1);
        this -> addChild(pRetry2, 0, Message_Retry2);
    }
}

void MessageLayer::CloseSelf()
{
    this -> removeFromParentAndCleanup(true);
}


bool MessageLayer::ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    if(m_ButtonType == BType_Firm)
    {
        if(CCRect::CCRectContainsPoint(m_BRect_Confirm, point))
        {
            CCSprite * pConfirm1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Confirm1));
            CCSprite * pConfirm2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Confirm2));
            pConfirm1 -> setOpacity(0);
            pConfirm2 -> setOpacity(255);
        }
    }
    else if(m_ButtonType == BType_GiveUpAndRetry)
    {
        if(CCRect::CCRectContainsPoint(m_BRect_GiveUp, point))
        {
            CCSprite * pGiveUp1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_GiveUp1));
            CCSprite * pGiveUp2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_GiveUp2));
            pGiveUp1 -> setOpacity(0);
            pGiveUp2 -> setOpacity(255);
        }
        else if(CCRect::CCRectContainsPoint(m_BRect_Retry, point))
        {
            CCSprite * pRetry1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry1));
            CCSprite * pRetry2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry2));
            pRetry1 -> setOpacity(0);
            pRetry2 -> setOpacity(255);
        }
    }
    else if(m_ButtonType == BType_Retry)
    {
        if(CCRect::CCRectContainsPoint(m_BRect_Retry, point))
        {
            CCSprite * pRetry1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry1));
            CCSprite * pRetry2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry2));
            pRetry1 -> setOpacity(0);
            pRetry2 -> setOpacity(255);
        }
    }
    
    return true;
}

void MessageLayer::ccTouchMoved(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    if(m_ButtonType == BType_Firm)
    {
        CCSprite * pConfirm1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Confirm1));
        CCSprite * pConfirm2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Confirm2));
        if(CCRect::CCRectContainsPoint(m_BRect_Confirm, point))
        {
            pConfirm1 -> setOpacity(0);
            pConfirm2 -> setOpacity(255);
        }else
        {
            pConfirm1 -> setOpacity(255);
            pConfirm2 -> setOpacity(0);
        }
    }
    else if(m_ButtonType == BType_GiveUpAndRetry)
    {
        CCSprite * pGiveUp1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_GiveUp1));
        CCSprite * pGiveUp2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_GiveUp2));
        CCSprite * pRetry1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry1));
        CCSprite * pRetry2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry2));
        if(CCRect::CCRectContainsPoint(m_BRect_GiveUp, point))
        {
            pGiveUp1 -> setOpacity(0);
            pGiveUp2 -> setOpacity(255);
        }
        else if(CCRect::CCRectContainsPoint(m_BRect_Retry, point))
        {
            pRetry1 -> setOpacity(0);
            pRetry2 -> setOpacity(255);
        }else
        {
            pGiveUp1 -> setOpacity(255);
            pGiveUp2 -> setOpacity(0);
            
            pRetry1 -> setOpacity(255);
            pRetry2 -> setOpacity(0);
        }
    }
    else if(m_ButtonType == BType_Retry)
    {
        CCSprite * pRetry1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry1));
        CCSprite * pRetry2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry2));
        if(CCRect::CCRectContainsPoint(m_BRect_Retry, point))
        {
            pRetry1 -> setOpacity(0);
            pRetry2 -> setOpacity(255);
        }else
        {
            pRetry1 -> setOpacity(255);
            pRetry2 -> setOpacity(0);
        }
    }
}

void MessageLayer::ccTouchEnded(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    if(m_ButtonType == BType_Firm)
    {
        //确定按钮
        if(CCRect::CCRectContainsPoint(m_BRect_Confirm, point))
        {
            CCSprite * pConfirm1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Confirm1));
            CCSprite * pConfirm2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Confirm2));
            pConfirm1 -> setOpacity(255);
            pConfirm2 -> setOpacity(0);
            
            if(! m_CallBack_Confirm)    CloseSelf();
            else
                this -> runAction(m_CallBack_Confirm);
        }
    }
    else if(m_ButtonType == BType_GiveUpAndRetry)
    {
        CCSprite * pGiveUp1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_GiveUp1));
        CCSprite * pGiveUp2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_GiveUp2));
        CCSprite * pRetry1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry1));
        CCSprite * pRetry2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry2));
        //放弃按钮
        if(CCRect::CCRectContainsPoint(m_BRect_GiveUp, point))
        {
            pGiveUp1 -> setOpacity(255);
            pGiveUp2 -> setOpacity(0);
            
            if(! m_CallBack_GiveUp)
                CloseSelf();
            else
                this -> runAction(m_CallBack_GiveUp);
        }
        //重试按钮
        else if(CCRect::CCRectContainsPoint(m_BRect_Retry, point))
        {
            pRetry1 -> setOpacity(255);
            pRetry2 -> setOpacity(0);
            
            if(! m_CallBack_Retry)
                CloseSelf();
            else
                this -> runAction(m_CallBack_Retry);
        }
    }
    else if(m_ButtonType == BType_Retry)
    {
        //重试按钮
        if(CCRect::CCRectContainsPoint(m_BRect_Retry, point))
        {
            CCSprite * pRetry1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry1));
            CCSprite * pRetry2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Message_Retry2));
            pRetry1 -> setOpacity(255);
            pRetry2 -> setOpacity(0);
            
            if(! m_CallBack_Retry)    CloseSelf();
            else
                this -> runAction(m_CallBack_Retry);
        }
    }
}







