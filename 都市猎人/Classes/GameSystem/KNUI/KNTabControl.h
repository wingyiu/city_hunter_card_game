//
//  KNTabControl.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-4-6.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef KNTabControl_h
#define KNTabControl_h

#include "KNFrame.h"

class KNTabControl : public KNBaseUI
{
public:
    #pragma mark - 构造函数
    KNTabControl();
    #pragma mark - 析构函数
    virtual ~KNTabControl();
    
    #pragma mark - 重写初始化函数－使用自动内存使用
    bool init();
    #pragma mark - 重写退出函数－使用自动内存使用
    virtual void onExit();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 重写可见函数－打开可见并打开第一个选项卡
    void setIsVisible(bool bIsVisible);
    
    #pragma mark - 添加一个页签框函数
    bool AddOneFrameToList(KNFrame* pFrame);
    
    #pragma mark - 设置该窗口激活状态函数
    bool SetTabEnable(int iIndex);
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNTabControl);
protected:
    #pragma mark - 事件函数－点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
private:
    // tab对象变量
    CCSprite* m_pNormalSprite[MAX_TAB_FRAMES];
    CCSprite* m_pSelectSprite[MAX_TAB_FRAMES];
    
    // tab属性变量
    vector<KNFrame*> m_vecFrameList;                // 图层列表
    
    int m_iFrameNum;                                // 选项卡数量
};

#endif
