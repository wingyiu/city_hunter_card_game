//
//  KNScrollView.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-28.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "KNScrollView.h"
#include "KNUIManager.h"

/************************************************************************/
#pragma mark - KNScrollView类构造函数
/************************************************************************/
KNScrollView::KNScrollView()
{
    // 为scrollview对象赋初值
    m_pSlider = NULL;
    m_pSliderBack = NULL;
    
    // 为scrollview属性赋初值
    m_fDeltaTime = 0.0f;
    m_fScrollWidth = 0.0f;
    m_fScrollHeight = 0.0f;
    
    m_LastPoint.x = m_LastPoint.y = 0.0f;
    m_itemSize.width = m_itemSize.height = 0.0f;
    m_fScrollOffsetX = m_fScrollOffsetY = 0.0f;
    m_fSliderPosX = 0.0f;
    
    m_fSlideVelo = 0.0f;
    
    m_eState = RESUME;
    m_eTypeState = VERTICAL;
    
    m_bIsMoving = false;
    m_bIsStopBottonMove = false;
}

/************************************************************************/
#pragma mark - KNScrollView类析构函数
/************************************************************************/
KNScrollView::~KNScrollView()
{
    // 关闭点击事件
    setIsTouchEnabled(false);
}

/************************************************************************/
#pragma mark - KNScrollView类重写初始化函数－使用自动内存使用
/************************************************************************/
bool KNScrollView::init()
{
    if (CCLayer::init() == false)
        return false;
    
    // 获得屏幕尺寸
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    // 激活点击事件
    //setIsTouchEnabled(true);
    
    // 设置可视范围
    SetVisibleRange(CCRect(0.0f, 0.0f, winSize.width, winSize.height));
    
    // 注册更新函数
    schedule(schedule_selector(KNScrollView::update));
    
    return true;
}
/************************************************************************/
#pragma mark - KNScrollView类重写退出函数－使用自动内存使用
/************************************************************************/
void KNScrollView::onExit()
{
    // 关闭点击事件
    //setIsTouchEnabled(false);
}

/************************************************************************/
#pragma mark - KNScrollView类初始化函数
/************************************************************************/
bool KNScrollView::init(UIInfo& stInfo)
{
    // 获得屏幕尺寸
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    // 激活点击事件
    //setIsTouchEnabled(true);
    
    // 设置可视范围
    SetVisibleRange(CCRect(0.0f, 0.0f, winSize.width, winSize.height));
    
    // 注册更新函数
    schedule(schedule_selector(KNScrollView::update));
    
    // 初始化滚动条背景
    m_pSliderBack = CCSprite::spriteWithSpriteFrameName("slider_back.png");
    if (m_pSliderBack != NULL)
    {
        m_pSliderBack->setIsVisible(false);
        m_pSliderBack->setPosition(getPosition());
        addChild(m_pSliderBack, 1);
    }
    
    // 初始化滚动条
    m_pSlider = CCSprite::spriteWithSpriteFrameName("slider.png");
    if (m_pSlider != NULL)
    {
        m_pSlider->setIsVisible(false);
        m_pSlider->setPosition(getPosition());
        addChild(m_pSlider, 1);
    }
    
    return true;
}

/************************************************************************/
#pragma mark - KNScrollView类事件函数－点击
/************************************************************************/
bool KNScrollView::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 停止滑动和动作
    stopAllActions();
    m_fSlideVelo = 0.0f;
    m_eState = STOPING;
    
    m_bIsMoving = false;
    
    return true;
}

/************************************************************************/
#pragma mark - KNScrollView类事件函数－弹起
/************************************************************************/
void KNScrollView::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    // 判断是否超出范围，超出范围就还原坐标，不进行滑动i
    if (AdjustPosition() == true)
        return;
    
    // 没有超出范围则切换为滑动效果
    m_eState = SLIDING;
}

/************************************************************************/
#pragma mark - KNScrollView类事件函数－滑动
/************************************************************************/
void KNScrollView::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得第一次点击事件
    //CCTouch* pTouch = (CCTouch*)(*pTouches->begin());
    
    // 获得屏幕坐标
    //CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    // 点击坐标
    CCPoint curPoint = convertTouchToNodeSpace(pTouch);
    
    // 上次点击坐标
    CCPoint lastPoint = pTouch->previousLocationInView(pTouch->view());
    lastPoint = CCDirector::sharedDirector()->convertToGL(lastPoint);
    lastPoint = convertToNodeSpace(lastPoint);
    
    CCPoint translation = ccpSub(curPoint, lastPoint);
    
    // 限制x或者y移动
    if (m_eTypeState == VERTICAL)
        translation.x = 0.0f;
    else if (m_eTypeState == HORIZONTAL)
        translation.y = 0.0f;
    
    // 计算新位置
    CCPoint temp = ccpAdd(getPosition(), translation);
    
    // 如果超过了，拖动距离减半
    if (m_eTypeState == VERTICAL)
    {
        float fOffset = -(m_fScrollHeight - m_visibleRange.size.height);
        if (temp.y < fOffset || temp.y > m_itemSize.height / 2.0f)
            translation = ccpMult(translation, 0.5f);
    }
    else if (m_eTypeState == HORIZONTAL)
    {
        float fOffset = -(m_fScrollWidth - m_visibleRange.size.width);
        if (temp.x < fOffset || temp.x > 0.0f)
            translation = ccpMult(translation, 0.5f);
    }
    
    CCPoint newPos = ccpAdd(getPosition(), translation);
    
//    // 底部不能拖动
//    if (m_bIsStopBottonMove)
//    {
//        if (newPos.y > m_itemSize.height / 2.0f)
//        {
//            newPos.y = m_itemSize.height / 2.0f;
//            setPosition(newPos);
//            // 设置状态
//            m_eState = DRAGING;
//            
//            m_bIsMoving = true;
//            
//            return;
//        }
//    }
    if (newPos.y < 325.0f)
        setPosition(newPos);
    
    // 计算速度
    curPoint = pTouch->locationInView(pTouch->view());
    curPoint = CCDirector::sharedDirector()->convertToGL(curPoint);
    
    if (m_eTypeState == VERTICAL)
        m_fSlideVelo = (curPoint.y - m_LastPoint.y) / m_fDeltaTime / 100.0f;
    else if (m_eTypeState == HORIZONTAL)
        m_fSlideVelo = (curPoint.x - m_LastPoint.x) / m_fDeltaTime / 100.0f;
    
    // 保存这一帧位置，为了下一帧计算速度
    m_LastPoint = pTouch->locationInView(pTouch->view());
    m_LastPoint = CCDirector::sharedDirector()->convertToGL(m_LastPoint);
    
    // 速度保护
    if (m_fSlideVelo >= 20.0f)
        m_fSlideVelo = 20.0f;
    else if (m_fSlideVelo <= -20.0f)
        m_fSlideVelo = -20.0f;
    
    // 设置状态
    m_eState = DRAGING;
    
    m_bIsMoving = true;
}

/************************************************************************/
#pragma mark - KNScrollView类更新函数
/************************************************************************/
void KNScrollView::update(ccTime dt)
{
    // 得到每帧时间：计算用户释放后的滑动加速度
    m_fDeltaTime = dt;
    
    // 根据状态进行更新
    switch (m_eState)
    {
        case STOPING:
        {
            //AdjustPosition();
        }
            break;
        case DRAGING:
        {
            
        }
            break;
        case SLIDING:
        {
            // 更新图层位置
            CCPoint layerPos = getPosition();
            
            if (m_eTypeState == VERTICAL)
                layerPos.y += m_fSlideVelo;
            else if (m_eTypeState == HORIZONTAL)
                layerPos.x += m_fSlideVelo;
            
            setPosition(layerPos);
            
            // 计算速度
            m_fSlideVelo < 0.0f ? m_fSlideVelo += ACCELERATION : m_fSlideVelo -= ACCELERATION;
            
            // 根据速度进行状态切换
            if (m_fSlideVelo > -0.1f && m_fSlideVelo < 0.1f)
            {
                m_fSlideVelo = 0.0f;
                m_eState = RESUME;
            }
            
            // 滑动超出范围，还原坐标
            if (m_eTypeState == VERTICAL)
            {                
                if (layerPos.y > m_itemSize.height / 2.0f + OVERDISTANCE + m_fScrollOffsetY)
                {
                    m_fSlideVelo = 0.0f;
                    m_eState = RESUME;
                    AdjustPosition();
                }
                else if (layerPos.y < m_visibleRange.size.height - m_fScrollHeight + m_itemSize.height / 2.0f - OVERDISTANCE)
                {
                    m_fSlideVelo = 0.0f;
                    m_eState = RESUME;
                    AdjustPosition();
                }
            }
            else if (m_eTypeState == HORIZONTAL)
            {
                if (layerPos.x > -m_itemSize.width / 2.0f + OVERDISTANCE)
                {
                    m_fSlideVelo = 0.0f;
                    m_eState = RESUME;
                    AdjustPosition();
                }
                else if (layerPos.x < m_visibleRange.size.width - m_fScrollWidth - OVERDISTANCE)
                {     
                    m_fSlideVelo = 0.0f;
                    m_eState = RESUME;
                    AdjustPosition();
                }
            }
        }
            break;
        case RESUME:
        {
            AdjustPosition();
        }
            break;
    }
    
    // 更新滚动条位置
    UpdateSliderPosition();
}

/************************************************************************/
#pragma mark - KNScrollView类设置图层滑动范围
/************************************************************************/
void KNScrollView::SetScrollSize(CCPoint size)
{
    if (size.x < m_visibleRange.size.width)
        m_fScrollWidth = m_visibleRange.size.width;
    else
        m_fScrollWidth = size.x;
    
    if (size.y < m_visibleRange.size.height)
        m_fScrollHeight = m_visibleRange.size.height;
    else
        m_fScrollHeight = size.y;
}

/************************************************************************/
#pragma mark - KNScrollView类设置到上或下函数
/************************************************************************/
void KNScrollView::SetPoint(bool bIsUp)
{
    if (!bIsUp)
    {
        CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.1f, ccp(0.0f, m_itemSize.height / 2.0f + m_fScrollOffsetY));
        if (pMoveTo != NULL)
            runAction(pMoveTo);
    }
    else
    {
        CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.1f, ccp(0.0f, m_visibleRange.size.height - m_fScrollHeight + m_itemSize.height / 2.0f));
        if (pMoveTo != NULL)
            runAction(pMoveTo);
    }
}

// 设备是否支持高清
extern bool g_bIsEnableRetina;

/************************************************************************/
#pragma mark - KNScrollView类重写基类visit函数
/************************************************************************/
void KNScrollView::visit()
{
    // 打开opengl裁减
    glEnable(GL_SCISSOR_TEST);
    // 设置裁减区域，左下角为0 ,0，水平往右为x正方向，竖直向上为y正方向
    if (g_bIsEnableRetina)
        glScissor(m_visibleRange.origin.x * 2, m_visibleRange.origin.y * 2, m_visibleRange.size.width * 2, m_visibleRange.size.height * 2);
    else
        glScissor(m_visibleRange.origin.x, m_visibleRange.origin.y, m_visibleRange.size.width, m_visibleRange.size.height);
    
    // 调用基类方法
    CCLayer::visit();
    
    // 关闭opengl裁减
    glDisable(GL_SCISSOR_TEST);
}

/************************************************************************/
#pragma mark - KNScrollView类修正图层位置到屏幕中
/************************************************************************/
bool KNScrollView::AdjustPosition()
{
    // 获得图层位置
    CCPoint layerPos = getPosition();
    //CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    // 根据不同状态进行恢复
    if (m_eTypeState == VERTICAL)
    {
        // 上底
        if (layerPos.y > m_itemSize.height / 2.0f + m_fScrollOffsetY)
        {
            CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.5f, ccp(0.0f, m_itemSize.height / 2.0f + m_fScrollOffsetY));
            if (pMoveTo != NULL)
            {
                runAction(pMoveTo);
                m_eState = DRAGING;
                return true;
            }
        }
        else if (layerPos.y < m_visibleRange.size.height - m_fScrollHeight + m_itemSize.height / 2.0f)
        {
            CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.5f, ccp(0.0f, m_visibleRange.size.height - m_fScrollHeight + m_itemSize.height / 2.0f));
            if (pMoveTo != NULL)
            {
                runAction(pMoveTo);
                m_eState = DRAGING;
                return true;
            }
        }
    }
    else if (m_eTypeState == HORIZONTAL)
    {
        // 上底
        if (layerPos.x > -m_itemSize.width / 2.0f)
        {
            CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.5f, ccp(-m_itemSize.width / 2.0f, 0.0f));
            if (pMoveTo != NULL)
            {
                runAction(pMoveTo);
                m_eState = DRAGING;
                return true;
            }
        }
        else if (layerPos.x < m_visibleRange.size.width - m_fScrollWidth - m_itemSize.width / 2.0f)
        {
            CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.5f, ccp(m_visibleRange.size.width - m_fScrollWidth - m_itemSize.width / 2.0f, 0.0f));
            if (pMoveTo != NULL)
            {
                runAction(pMoveTo);
                m_eState = DRAGING;
                return true;
            }
        }
    }
    
    m_eState = STOPING;
    
    return false;
}

/************************************************************************/
#pragma mark - KNScrollView类更新滚动条位置函数
/************************************************************************/
void KNScrollView::UpdateSliderPosition()
{
    float scrollheight = 0.0f;
    if (m_fScrollHeight <= m_visibleRange.size.height)
        scrollheight = m_visibleRange.size.height + m_itemSize.height / 2.0f;
    else
    {
        scrollheight = m_visibleRange.size.height - m_fScrollHeight + m_itemSize.height / 2.0f;
        if (scrollheight < 0.0f)
            scrollheight *= -1;
    }
    float y = getPosition().y - m_itemSize.height / 2.0f;
    if (y == 0.0f)
        y = 1.0f;
    float sliderheight = m_visibleRange.size.height - 37.0f;
    float posy = y * sliderheight / scrollheight;
    
    float other = 0.0f;
    if (m_fScrollHeight <= m_visibleRange.size.height)
        other = m_visibleRange.size.height - 37.0f;
    
    m_pSlider->setPosition(ccp(m_fSliderPosX, -getPosition().y - posy + 18.0f + other));
    m_pSliderBack->setPosition(ccp(m_fSliderPosX, -getPosition().y + 140.0f));
    
    float fPosY = getPosition().y;
    if (fPosY < -(m_fScrollHeight - m_visibleRange.size.height - 20.0f))
    {
        float temp1 = -((m_fScrollHeight - m_visibleRange.size.height - 20.0f) + fPosY);
        if (temp1 < 0.0f)
            temp1 = -temp1;
        
        m_BuildingBack1.y = temp1 * 0.8f;
        m_BuildingBack2.y = temp1 * 0.4f;
    }
    else
    {
        m_BuildingBack1.y = m_BuildingBack2.y = 0.0f;
    }
}