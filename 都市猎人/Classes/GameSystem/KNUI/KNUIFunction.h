//
//  KNUIFunction.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-4-16.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_KNUIFunction_h
#define MidNightCity_KNUIFunction_h

#include "KNUIManager.h"

#include "SystemDataManage.h"
#include "ServerDataManage.h"
#include "PlayerDataManage.h"

class KNUIFunction : public CCObject
{
public:
    #pragma mark - 获得单键功能类
    static KNUIFunction* GetSingle() { static KNUIFunction function; return &function; }
    
    #pragma mark - 以弹出方式打开对话框函数
    bool OpenFrameByJumpType(int iFrameID);
    #pragma mark - 以弹出相应方式关闭对话框函数
    bool CloseFrameByJumpType(int iFrameID);
    
    #pragma mark - 以普通方式打开对话框函数
    bool OpenFrameByNormalType(int iFrameID);
    #pragma mark - 以普通方式关闭对话框函数
    bool CloseFrameByNormalType(int iFrameID);
    
    #pragma mark - 特殊窗口打开函数
    bool OpenFrameByJumpTypeSpecial(int iFrameID, CCPoint startPos);
    
    #pragma mark - 更新小弟商店信息
    void UpdateFollowerInfo(bool bIsRefresh = false);
    #pragma mark - 更新超级小弟商店信息
    void UpdateSuperFollowerInfo(bool bIsRefresh = false);
    #pragma mark - 更新小弟训练界面信息
    void UpdateFollowerTrainInfo(Follower* pFollower);
    #pragma mark - 更新小弟转职界面信息
    void UpdateFollowerJobInfo(Follower* pFollower);
    #pragma mark - 刷新小弟队伍列表
    void UpdateFollowerTeamList();
    #pragma mark - 刷新副本列表函数
    void UpdateMapList();
    #pragma mark - 刷新好友列表函数
    void UpdateFriendList();
    #pragma mark - 刷新邮件列表函数
    void UpdateMailList();
    #pragma mark - 刷新任务层新地图函数
    void UpdateMissionFloorHint();
    #pragma mark - 刷新队伍信息函数
    void UpdateTeamInfo();
    #pragma mark - 刷新签到信息函数
    void UpdateSignInfo();
    
    #pragma mark - 加入楼层
    void AddFloorToGameScene(int iType);
    
    #pragma mark - 完成楼层收获函数
    void finishFloorHavest(int iType);
    
    #pragma mark - 提出信息对话框函数
    void OpenMessageBoxFrame(const char* message);
    void OpenSystemMessageBoxFrame(const char* message);
    
    #pragma mark - 显示小弟详细界面函数
    void ShowFollowerFullInfo(Follower* pFollower, int iFrameID);
    #pragma mark - 显示行动力不足提示界面函数
    void ShowActionHintFrame();
    
    #pragma mark - 刷新站台信息函数 : 由于初始化顺序关系，站台内的数据无法再初始化刷新，单独拿出来，初始化好后再刷新
    void UpdatePlatformInfo();
    #pragma mark - 更新玩家id函数
    void UpdatePlayerID();
    #pragma mark - 更新设置信息函数
    void UpdateSetInfo();
    
    #pragma mark - 播放音乐函数
    void PlayerBackMusic(const char* szMusicName);
    #pragma mark - 停止音乐函数
    void StopBackMusic();
    
    #pragma mark - 播放音效函数
    void PlayerEffect(const char* szEffectName);
    
    #pragma mark - 获得经验显示百分比函数
    float GetExpPercent();
    #pragma mark - 获得玩家下一级升级所需经验函数
    int GetPlayerNeedExpForNextLevel();
    
    #pragma mark - 背包是否已满函数
    bool IsPackageFull();
    
    #pragma mark - 显示楼层提示函数
    void ShowFloorHint(FLOORTYPE eType);
private:
    #pragma mark - 构造函数
    KNUIFunction();
    #pragma mark - 析构函数
    virtual ~KNUIFunction();
private:
    bool m_bIsNetLoading;                           // 是否在loading中
};

#endif
