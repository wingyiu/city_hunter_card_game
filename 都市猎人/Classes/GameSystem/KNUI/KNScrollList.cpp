//
//  KNScrollList.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-4-5.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "KNScrollList.h"
#include "KNUIManager.h"

int arrTeam[3] = {0, 0, 0};

/************************************************************************/
#pragma mark - KNScrollList类构造函数
/************************************************************************/
KNScrollList::KNScrollList()
{
    // 为选项列表赋初值
    m_ItemList.clear();
    
    // 为选中选项标记赋初值
    m_iSelectedIndex = 0;
    m_iListType = 0;
    
    index = 0;
}

/************************************************************************/
#pragma mark - KNScrollList类析构函数
/************************************************************************/
KNScrollList::~KNScrollList()
{
    // 移除精灵
    vector<CCLayer*>::iterator iter;
    for (iter = m_ItemList.begin(); iter != m_ItemList.end(); iter++)
        removeChild(*iter, true);
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNScrollList类重写初始化函数－使用自动内存使用
/************************************************************************/
bool KNScrollList::init()
{
    if (CCLayer::init() == false)
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - KNScrollList类重写退出函数－使用自动内存使用
/************************************************************************/
void KNScrollList::onExit()
{
    // 移除精灵
    vector<CCLayer*>::iterator iter;
    for (iter = m_ItemList.begin(); iter != m_ItemList.end(); iter++)
        removeChild(*iter, true);
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNScrollList类初始化函数
/************************************************************************/
bool KNScrollList::init(UIInfo& stInfo)
{
    // 初始化scrollview
    KNScrollView::init(stInfo);
    
    // 事件
    if (stInfo.bIsVisible)
        CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    // 位置
    //setPosition(stInfo.point);
    
    // 区域
    SetVisibleRange(CCRect(stInfo.point.x, stInfo.point.y, stInfo.size.width, stInfo.size.height));
    
    // 设置滑动类型
    KNScrollView::SetScrollType(static_cast<ScrollType>(stInfo.iType));
    
    return true;
}

/************************************************************************/
#pragma mark - KNScrollList类获得当前选中的选项信息函数
/************************************************************************/
ITEMINFO KNScrollList::GetSelectedItemInfo() const
{
    ITEMINFO info;
    return info;
}

/************************************************************************/
#pragma mark - KNScrollList类事件函数－点击
/************************************************************************/
bool KNScrollList::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    
    CCRect visibleRect = GetVisibleRange();
    if (point.x > visibleRect.origin.x && point.x < visibleRect.origin.x + visibleRect.size.width &&
        point.y > visibleRect.origin.y && point.y < visibleRect.origin.y + visibleRect.size.height)
    {
        CCSet pSet;
        pSet.addObject(pTouch);
        KNScrollView::ccTouchesBegan(&pSet, pEvent);
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNScrollList类事件函数－弹起
/************************************************************************/
void KNScrollList::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    CCSet pSet;
    pSet.addObject(pTouch);
    KNScrollView::ccTouchesEnded(&pSet, pEvent);
}

/************************************************************************/
#pragma mark - KNScrollList类事件函数－滑动
/************************************************************************/
void KNScrollList::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
{
    CCSet pSet;
    pSet.addObject(pTouch);
    KNScrollView::ccTouchesMoved(&pSet, pEvent);
}