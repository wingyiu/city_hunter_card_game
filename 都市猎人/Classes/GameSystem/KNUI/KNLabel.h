//
//  KNLabel.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef MyCocosGame_KNLabel_h
#define MyCocosGame_KNLabel_h

#include "KNBaseUI.h"

class KNLabel : public KNBaseUI
{
public:
    #pragma mark - 构造函数
    KNLabel();
    #pragma mark - 析构函数
    virtual ~KNLabel();
    
    #pragma mark - 重写初始化函数－使用自动内存使用
    bool init();
    #pragma mark - 重写退出函数－使用自动内存使用
    void onExit();
    
    #pragma mark - 实现基类函数－初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 重置label图片
    void resetLabelImage(const char* imagename);
    #pragma mark - 设置label文字
    void setLabelText(const char* text, const char* fontname, int iFontSize);
    
    #pragma mark - 获得图片精灵函数
    CCSprite* GetBackSprite() const         {return m_pBack;}
    #pragma mark - 获得文本标签函数
    CCLabelTTF* GetTextLabel() const        {return m_pLabel;}
    #pragma mark - 获得数字标签函数
    CCLabelAtlas* GetNumberLabel() const    {return m_pAtlas;}
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNLabel);
protected:
    #pragma mark - 事件函数－点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
private:
    // label对象
    CCSprite* m_pBack;                      // 背景精灵
    
    CCLabelTTF* m_pLabel;                   // 文本
    
    CCLabelAtlas* m_pAtlas;                 // 数字文本
};

#endif
