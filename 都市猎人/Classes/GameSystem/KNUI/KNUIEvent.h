//
//  KNUIEvent.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef KNUIEvent_h
#define KNUIEvent_h

#include "cocos2d.h"

using cocos2d::CCObject;

#pragma mark -------------------------------------------通用事件函数-------------------------------------------

#pragma mark - 通用事件 : 打开对话框
void openFrame(vector<int> vecParmList);

#pragma mark - 通用事件 : 关闭对话框
void closeFrame(vector<int> vecParmList);
void closeSystemFrame(vector<int> vecParmList);

#pragma mark - 通用事件 : 关闭战斗中的提示对话框
void closeMessageFrameInBattle(vector<int> vecParmList);

#pragma mark - 通用事件 : 打开对话框关闭其他
void openFrameAndCloseOthers(vector<int> vecParmList);

#pragma mark - 通用事件 : 关闭对话框打开其他
void closeFrameAndOpenOthers(vector<int> vecParmList);

#pragma mark -------------------------------------------login事件函数-------------------------------------------

#pragma mark - login事件 : ui(10000) 选择玩家职业
void SelectPlayerJob(vector<int> vecParmList);

#pragma mark - login事件 : ui(10000) button(30000) 进入游戏场景
void EnterGame(vector<int> vecParmList);

#pragma mark -------------------------------------------game事件函数-------------------------------------------

#pragma mark - game事件 : ui(90006) 进入商店
void EnterShop(vector<int> vecParmList);

#pragma mark - game事件 : ui (1000) 设置背景音乐开关
void setBackMusicOnOff(vector<int> vecParmList);

#pragma mark - game事件 : ui (1000) 设置音效开关
void setBackEffectOnOff(vector<int> vecParmList);

#pragma mark - game事件 : ui (1000) 设置显示属性
void setAttr(vector<int> vecParmList);

#pragma mark - game事件 : ui(10001) button(30001) 进入战斗场景
void EnterBattle(vector<int> vecParmList);

#pragma mark - game事件 : ui(10005) button(30004 - 30006) 招募小弟
void RecruitFollower(vector<int> vecParmList);

#pragma mark - game事件 : ui(12001) 进入小弟选择界面
void goToFollowerFrame(vector<int> vecParmList);

#pragma mark - Layer_Building
void buildFloor(vector<int> vecParmList);

#pragma mark - Layer_Building事件 : ui(90002) 刷新小弟列表
void refreshFollowerList(vector<int> vecParmList);

#pragma mark - Layer_Building事件 : ui(20000) 升级楼层
void levelupFloor(vector<int> vecParmList);

#pragma mark - Layer_Building事件 : ui(12006) 关闭升级界面
void closeLevelupFrame(vector<int> vecParmList);

#pragma mark - Layer_Building事件 : ui(11000) 清空队伍信息
//void removeAllTeamMember(vector<int> vecParmList);

#pragma mark - Layer_Building事件 : ui(11001) 确定按钮
void followerConfirm(vector<int> vecParmList);

#pragma mark - Layer_Building事件 : ui(11001) 清空按钮
void followerClear(vector<int> vecParmList);

#pragma mark - Layer_Building事件 : ui(11001) 解雇小弟
void followerFire(vector<int> vecParmList);

#pragma mark - Layer_Building事件 : ui(12004) 训练小弟
void TrainFollower(vector<int> vecParmList);

#pragma mark - game事件 : ui(12005) 转职小弟
void LevelUpFollower(vector<int> vecParmList);

#pragma mark - game事件 : ui(11006) 查找好友
void FindFriend(vector<int> vecParmList);

#pragma mark - game事件 : ui(12010) 添加好友
void addFriend(vector<int> vecParmList);

#pragma mark - game事件 : ui(12007) 删除好友
void removeFriend(vector<int> vecParmList);

#pragma mark - game事件 : ui(12012) 同意加好友
void agreeAddFriend(vector<int> vecParmList);

#pragma mark - game事件 : ui(12012)
void refuseAddFriend(vector<int> vecParmList);

#pragma mark - game事件 : ui(30000)
void recoverAction(vector<int> vecParmList);

#pragma mark - game事件
void showFollowerInfo(vector<int> vecParmList);

#pragma mark - game点击事件 - 移动小副本选择边框
void moveSubMapFrame(vector<int> vecParmList);

#pragma mark - game事件 - 背包扩展
void ExtendPackage(vector<int> vecParmList);

#pragma mark - game事件 - 清理背包
void ClearPackage(vector<int> vecParmList);

#pragma mark - game事件 - 立即收获楼层
void HavestFloor(vector<int> vecParmList);

#pragma mark - game事件 - 筛选小弟列表
void ScreenFollowerList(vector<int> vecParmList);

#pragma mark - game事件 - 排序小弟列表
void sortfollower(vector<int> vecParmList);

#pragma mark - game事件 - 打开小弟列表
void openFollowerList(vector<int> vecParmList);

#pragma mark - game事件 - 打开小弟信息界面
void openFollowerInfoFrame(vector<int> vecParmList);
#pragma mark - game事件 - 打开小弟训练界面
void openFollowerTrainFrame(vector<int> vecParmList);
#pragma mark - game事件 - 打开小弟转职界面
void openFollowerJobFrame(vector<int> vecParmList);
#pragma mark - game事件 - 打开小弟详细界面
void openFollowerFullInfoFrame(vector<int> vecParmList);
#pragma mark - Layer_Building事件 : ui(12006) 打开训练编辑界面
void openTrainEditedFrame(vector<int> vecParmList);
#pragma mark - game事件 : ui(10002) 进入小弟队伍界面
void openTeamList(vector<int> vecParmList);

#pragma mark - game事件 : 设置小弟显示模式
void SetFollowerAttrType(vector<int> vecParmList);
#pragma mark - game事件 : 排序小弟
void SortFollower(vector<int> vecParmList);

#pragma mark - game事件 : 登陆签到事件
void SignGameForDay(vector<int> vecParmList);
#pragma mark - game事件 : 补签事件
void ReSign(vector<int> vecParmList);

#pragma mark - game事件 : 开宝箱事件
void OpenBox(vector<int> vecParmList);

#pragma mark - game事件 : 关闭新手引导对话框函数
void CloseGuideFrame(vector<int> vecParmList);

#pragma mark -------------------------------------------控件回调函数-------------------------------------------

// 控件回调函数
class KNUIEvent : public CCObject
{
public:
    #pragma mark - 构造函数
    KNUIEvent() {}
    #pragma mark - 析构函数
    virtual ~KNUIEvent() {}
    
    #pragma mark - 对话框关闭回调函数
    void CloseFrame(CCObject* pObject);
    
    #pragma mark - 移出界面图层回调函数
    void RemoveLayerFromFrame(CCObject* pObject, int* pOpenFrameID);
    
    #pragma mark - 转职步骤回调函数
    void JobStep(CCObject* pObject, CCObject* p);
};

#endif
