//
//  KNTextField.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef MyCocosGame_KNTextField_h
#define MyCocosGame_KNTextField_h

#include "KNBaseUI.h"

// 文本框类型 : 采用移位操作进行判断
// 1     1       1         1
// 数字   密码   字符和数字   普通键盘
enum TEXTFIELDTYPE
{
    TEXT_NORMAL = 1,                // 普通文本 : 能接受任何字符
    TEXT_ASCII = 2,                 // acsii文本 : 只能输入字符和数字
    TEXT_PASSWORD = 4,              // 密码文本 : 显示为*
    TEXT_NUMBER = 8,                // 数字文件 : 只有数字输入
};

// textfield文本框类
class MyTextField : public CCTextFieldTTF
{
public:
    #pragma mark - 构造函数
    MyTextField();
    #pragma mark - 析构函数
    virtual ~MyTextField();
    
    #pragma mark - 创建文本函数
    static MyTextField* textFieldWithPlaceHolder(const char *placeholder, const char *fontName, float fontSize, CCSize fieldSize, int iTextLength, int iType);
    
    #pragma mark - 事件函数 : 输入文本
    virtual void insertText(const char* text, int len);
    #pragma mark - 重写键盘弹出函数 : 弹出不同键盘
    bool attachWithIME();
    #pragma mark - 重写字符串设置函数 : 添加密码支持
    void setString(const char *text);
    #pragma mark - 重写visit函数 : 显示光标
    virtual void visit();
private:
    // 文本字符串
    string m_InputText;
    
    // 文本长度限制
    int m_iMaxLength;
    
    // 文本类型
    int m_iType;
    
    // 光标显示计时器
    int m_iTime;
    
    // 可见标记
    bool m_bIsVisible;
    
    // 光标闪烁标记
    bool m_bIsLighting;
    
    // 光标位置
    CCPoint m_CursorStartPos;
    CCPoint m_CursorEndPos;
};

// 文本输入控件类
class KNTextField : public KNBaseUI
{
public:
    #pragma mark - 构造函数
    KNTextField();
    #pragma mark - 析构函数
    virtual ~KNTextField();
    
    #pragma mark - 初始化函数
    bool init();
    #pragma mark - 退出函数 : 使用自动内存使用
    void onExit();
    
    #pragma mark - 实现基类函数－初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 获得控件字符串函数
    const char* getText() const                     {return m_pTextField->getString();}
    #pragma mark - 设置控件字符串函数
    void setText(const char* str)                   {m_pTextField->setString(str);}
    
    #pragma mark - 事件函数－点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    
    #pragma mark - 设置光标可见性
    //void SetCursorVisible(bool bIsVisible)          {m_pTextField->SetCursorVisible(bIsVisible);}
    
    #pragma mark - 打开键盘
    void attachWithIME()                            {m_pTextField->attachWithIME();}
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNTextField);
private:
    // textfield对象变量
    MyTextField* m_pTextField;
};

#endif
