//
//  KNProgress.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "KNProgress.h"

/************************************************************************/
#pragma mark - KNProgress类构造函数
/************************************************************************/
KNProgress::KNProgress()
{
    // 为progress对象赋初值
    m_pProgressSprite = NULL;
    
    m_fPercent = 0.0f;
}

/************************************************************************/
#pragma mark - KNProgress类析构函数
/************************************************************************/
KNProgress::~KNProgress()
{
    // 移除滚动条
    removeChild(m_pProgressSprite, true);
}

/************************************************************************/
#pragma mark - 重写初始化函数－使用自动内存使用
/************************************************************************/
bool KNProgress::init()
{
    if (CCLayer::init() == false)
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - 重写退出函数－使用自动内存使用
/************************************************************************/
void KNProgress::onExit()
{
    // 移除滚动条
    removeChild(m_pProgressSprite, true);
}
CCRect rect;
/************************************************************************/
#pragma mark - KNProgress类实现基类函数－初始化函数
/************************************************************************/
bool KNProgress::init(UIInfo& stInfo)
{
    // 初始化进度条精灵
    m_pProgressSprite = CCSprite::spriteWithSpriteFrameName(stInfo.szNormalImage);
    if (m_pProgressSprite != NULL)
    {
        m_pProgressSprite->setPosition(stInfo.point);
        addChild(m_pProgressSprite);
    }
    
    m_Position = stInfo.point;
    
    if (stInfo.fPercentage <= 100.0f)
        m_fPercent = stInfo.fPercentage;
    else
        m_fPercent = 100.0f;
    
    // 获得缩放系数
    float fScale = CC_CONTENT_SCALE_FACTOR();
    
    CCSize size = m_pProgressSprite->getContentSize();
    
    m_stVisibleRange.origin.x = 160.0f + stInfo.point.x - size.width / 2.0f;
    m_stVisibleRange.origin.y = 0.0f;
    m_stVisibleRange.size = CCSize(320.0f, 480.0f);
    
    m_stVisibleRange.origin.x *= fScale;
    m_stVisibleRange.origin.y *= fScale;
    m_stVisibleRange.size.width *= fScale;
    m_stVisibleRange.size.height *= fScale;
    
    rect = m_pProgressSprite->getTextureRect();
    
    return true;
}

/************************************************************************/
#pragma mark - KNProgress类设置可视范围函数
/************************************************************************/
void KNProgress::SetVisibleRect(CCRect rect)
{
    m_stVisibleRange = rect;
    
    // 获得缩放系数
    float fScale = CC_CONTENT_SCALE_FACTOR();
    
    m_stVisibleRange.origin.x *= fScale;
    m_stVisibleRange.origin.y *= fScale;
    m_stVisibleRange.size.width *= fScale;
    m_stVisibleRange.size.height *= fScale;
}

/************************************************************************/
#pragma mark - KNProgress类事件函数－点击
/************************************************************************/
bool KNProgress::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    return false;
}

/************************************************************************/
#pragma mark - KNProgress类重新函数 : 访问函数
/************************************************************************/
void KNProgress::visit()
{
    // 变化进度
    m_pProgressSprite->setPosition(ccp(m_Position.x - (1.0f - m_fPercent / 100.0f) * m_pProgressSprite->getContentSize().width, m_Position.y));
    
    // 打开opengl裁减
    glEnable(GL_SCISSOR_TEST);
    
    // 设置裁减区域，左下角为0 ,0，水平往右为x正方向，竖直向上为y正方向
    glScissor(m_stVisibleRange.origin.x, m_stVisibleRange.origin.y, m_stVisibleRange.size.width, m_stVisibleRange.size.height);
    
    // 调用基类方法
    CCLayer::visit();
    
    // 关闭opengl裁减
    glDisable(GL_SCISSOR_TEST);
}