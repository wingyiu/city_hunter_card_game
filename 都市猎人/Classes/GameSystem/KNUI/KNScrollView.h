//
//  KNScrollView.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-28.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef MyCocosGame_KNScrollView_h
#define MyCocosGame_KNScrollView_h

#include "KNBaseUI.h"
#include "KNLabel.h"

// 滚动条状态枚举
enum ScrollState
{
    STOPING = 0,                        // 停止中
    DRAGING,                            // 拖动中
    SLIDING,                            // 滑动中
    RESUME,                             // 恢复中
};

// 滑动方式枚举
enum ScrollType
{   
    VERTICAL = 0,                       // 只能竖直移动
    HORIZONTAL,                         // 只能水平移动
    BOTH,                               // 竖直和水平都能移动
};

// 加速度速率
const float ACCELERATION = 0.2f;
// 滑动允许超出偏移量
const float OVERDISTANCE = 30.0f;

class KNScrollView : public KNBaseUI
{
public:
    #pragma mark - 构造函数
    KNScrollView();
    #pragma mark - 析构函数
    virtual ~KNScrollView();
    
    #pragma mark - 重写初始化函数－使用自动内存使用
    bool init();
    #pragma mark - 重写退出函数－使用自动内存使用
    virtual void onExit();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 回调函数－逻辑更新
    void update(ccTime dt);
    
    #pragma mark - 设置滑动方式
    void SetScrollType(ScrollType eState)                   {m_eTypeState = eState;}
    #pragma mark - 获得滑动方式
    ScrollType GetScrollType() const                        {return m_eTypeState;}
    
    #pragma mark - 获得滑动状态
    ScrollState GetScrollState() const                      {return m_eState;}
    
    #pragma mark - 设置图层滑动范围
    void SetScrollSize(CCPoint size);
    // 获得图层滑动范围
    CCPoint GetScrollSize() const                           {return ccp(m_fScrollWidth, m_fScrollHeight);}
    
    #pragma mark - 设置图层可视范围
    void SetVisibleRange(CCRect range)                      {m_visibleRange = range;}
    #pragma mark - 获得图层可视范围
    CCRect GetVisibleRange() const                          {return m_visibleRange;}
    
    #pragma mark - 设置选项尺寸函数
    void SetItemSize(CCSize size)                           {m_itemSize = size;}
    #pragma mark - 获得选项尺寸函数
    CCSize GetItemSize() const                              {return m_itemSize;}
    
    #pragma mark - 获得是否滑动过函数
    bool GetIsMovingMark() const                            {return m_bIsMoving;}
    
    #pragma mark - 设置底部滑动状态函数
    void SetIsStopBottonMove(bool bIsStop)                  {m_bIsStopBottonMove = bIsStop;}
    
    #pragma mark - 设置偏移量函数
    void SetOffset(const CCPoint& offset)                   {m_fScrollOffsetX = offset.x; m_fScrollOffsetY = offset.y;}
    
    #pragma mark - 设置竖直滚动条位置
    void SetHorizontalSliderX(float fPosX)                  {m_fSliderPosX = fPosX; m_pSlider->setPosition(ccp(m_fSliderPosX, 0.0f)); m_pSliderBack->setPosition(ccp(m_fSliderPosX, 0.0f));}
    
    #pragma mark - 设置滚动条可用
    void SetSliderEnable(bool bIsEnable)                    {m_pSlider->setIsVisible(bIsEnable); m_pSliderBack->setIsVisible(bIsEnable);}
    
    #pragma mark - 设置到上或下函数
    void SetPoint(bool bIsUp);
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNScrollView);
    CCPoint m_BuildingBack1;            // 楼层专用 : 背景1位置
    CCPoint m_BuildingBack2;            // 楼层专用 : 背景2位置
protected:
    #pragma mark - 事件函数－点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－滑动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
private:
    #pragma mark - 重写基类visit函数
    virtual void visit();
private:
    #pragma mark - 修正图层位置到屏幕中
    bool AdjustPosition();
    #pragma mark - 更新滚动条位置函数
    void UpdateSliderPosition();
private:
    // scrollview对象变量
    CCSprite* m_pSlider;                // 滚动条
    CCSprite* m_pSliderBack;            // 滚动条背景
    
    // scrollview属性变量
    ccTime m_fDeltaTime;                // 引擎一帧过去的时间
    
    CCPoint m_LastPoint;                // 上一帧位置
    CCSize m_itemSize;                  // 选项尺寸
    CCRect m_visibleRange;              // 可视范围
    
    
    
    float m_fSlideVelo;                 // 放开滑动速度
    float m_fScrollWidth;               // 滚动条宽度
    float m_fScrollHeight;              // 滚动条高度
    float m_fScrollOffsetX;             // x轴偏移量
    float m_fScrollOffsetY;             // y轴偏移量
    float m_fSliderPosX;                // 滚动条x偏移坐标
    
    ScrollState m_eState;               // 滑动状态
    ScrollType m_eTypeState;            // 滑动方式
    
    bool m_bIsMoving;                   // 是否滑动过
    bool m_bIsStopBottonMove;           // 是否锁定底部滑动
};

#endif
