//
//  KNButton.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef MyCocosGame_KNButton_h
#define MyCocosGame_KNButton_h

#include "KNBaseUI.h"

class KNButton : public KNBaseUI
{
public:
    #pragma mark - 构造函数
    KNButton();
    #pragma mark - 析构函数
    virtual ~KNButton();
    
    #pragma mark - 重写初始化函数－使用自动内存使用
    bool init();
    
    #pragma mark - 重写退出函数－使用自动内存使用
    void onExit();
    
    #pragma mark - 实现基类函数－初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 事件函数－点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    
    #pragma mark - 设置按钮可用函数
    void setButtonEnable(bool bIsEnable);
    
    #pragma mark - 重置图片纹理函数
    void resetButtonImage(const char* szNormalName, const char* szSelectName);
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNButton);
private:
    // 按钮对象
    CCSprite* m_pNormalSprite;              // 常态图片
    CCSprite* m_pSelectSprite;              // 选择图片
    
    float m_fButtonWidth;                   // 按钮宽度
    float m_fButtonHeight;                  // 按钮高度
    
    bool m_bIsEnable;                       // 可用标记
    
    // ui事件
    void (*m_pButtonFunc)(vector<int> vecParmList);
};

#endif
