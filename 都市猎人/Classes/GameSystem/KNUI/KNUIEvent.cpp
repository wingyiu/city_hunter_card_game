//
//  KNUIEvent.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "KNUIEvent.h"
#include "KNUIManager.h"

#include "Layer_FollowerList.h"
#include "Layer_EquipmentList.h"

#include "Scene_logIn.h"        // add by sunhuiwei 2012-04-11
#include "Scene_Game.h"         // add by sunhuiwei 2012-04-10
#include "Scene_Battle.h"       // add by sunhuiwei 2012-04-23

#include "ServerDataManage.h"
#include "PlayerDataManage.h"

#include "GameController.h"
#include "PlayerDataManage.h"

#include "Wrapper.h"

KNUIEvent g_UIEvent;

#pragma mark -------------------------------------------控件事件函数-------------------------------------------

/************************************************************************/
#pragma mark - 通用事件 : 打开对话框
/************************************************************************/
void openFrame(vector<int> vecParmList)
{
    KNUIFunction::GetSingle()->OpenFrameByJumpType(vecParmList[0]);
}

/************************************************************************/
#pragma mark - 通用事件 : 关闭对话框
/************************************************************************/
void closeFrame(vector<int> vecParmList)
{
    // 获得要关闭的对话框
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(vecParmList[0]);
    if (pFrame != NULL && pFrame->getIsVisible() == true)
    {
        // 关闭楼层遮罩
        if (vecParmList[0] != MESSAGEBOX_FRAME_ID)
            Layer_Game::GetSingle()->CloseFloorMask();
        
        // 列表的控件先关闭
        map<int, KNBaseUI*> list = pFrame->GetUIList();
        map<int, KNBaseUI*>::iterator iter;
        for (iter = list.begin(); iter != list.end(); iter++)
        {
            if (
                iter->second->GetUIType() == FOLLOWERLIST       ||      // 自定义控件－小弟列表
                iter->second->GetUIType() == MAPLIST            ||      // 自定义控件－地图列表
                iter->second->GetUIType() == FRIENDFOLLOWERLIST ||      // 自定义控件－好友小弟列表
                iter->second->GetUIType() == DIAMONDLIST        ||      // 自定义控件－钻石列表
                iter->second->GetUIType() == MAILLIST           ||      // 自定义控件－邮件列表
                iter->second->GetUIType() == MESSAGELIST)               // 自定义控件－公告列表
            {
                iter->second->setIsVisible(false);
            }
        }
        
        // 新手引导
        if (PlayerDataManage::m_GuideMark)
        {
            if (vecParmList[0] == FOLLOWER_CENTER_FRAME_ID)        // 2层，进行小弟队伍和训练引导
            {
                if (PlayerDataManage::m_GuideStepMark == GUIDE_TEAMEDIT)
                {
                    if (GameGuide::GetSingle()->GetFloorSubGuideStep() < 4)
                        return;
                }
                else if (PlayerDataManage::m_GuideStepMark == GUIDE_TRAIN)
                {
                    if (GameGuide::GetSingle()->GetFloorSubGuideStep() < 4)
                        return;
                }
                
                // 完成该楼层引导
                GameGuide::GetSingle()->FinishOneFloorGuide();
            }
            else if (vecParmList[0] == TRAIN_PLATFORM_FRAME_ID)              // 2层，3层关闭界面，推进引导
            {
//                Layer_GameGuide::GetSingle()->FinishCurrentStepAndUpdateNext();
                GameGuide::GetSingle()->FinishOneFloorGuide();
            }
            else if (vecParmList[0] == MISSION_CENTER_FRAME_ID)         // 3层，进行关卡引导
            {
                if (GameGuide::GetSingle()->GetFloorSubGuideStep() < 4)
                    return;
                
                GameGuide::GetSingle()->FinishOneFloorGuide();
            }
            else if (vecParmList[0] == SHOP_FRAME_ID)
            {
                if (GameGuide::GetSingle()->GetFloorSubGuideStep() < 4)
                    return;
                
                GameGuide::GetSingle()->FinishOneFloorGuide();
            }
            else if (vecParmList[0] == BUILDING_LEVELUP_FRAME_ID)
            {
                if (PlayerDataManage::m_GuideMark)
                    return;
            }
            else if (vecParmList[0] == MESSAGEBOX_FRAME_ID)             // 关闭提示对话框，进行下一步引导
            {
//                if (Layer_GameGuide::GetSingle()->GetFloorLockIndex() == F_Follower)        // 小弟引导
//                    Layer_GameGuide::GetSingle()->RunFollowerGuideStep();
//                else if (Layer_GameGuide::GetSingle()->GetFloorLockIndex() == F_Mission)    // 任务关卡引导
//                    Layer_GameGuide::GetSingle()->RunMissionGuideStep();
//                
//                // 打开界面
//                KNUIFunction::GetSingle()->OpenFrameByJumpType(10000 + Layer_GameGuide::GetSingle()->GetFloorLockIndex() - 1);
            }
        }
        
        // 关闭特定界面，进行特殊处理
        if (vecParmList[0] == TRAIN_PLATFORM_FRAME_ID)                  // 站台关闭，更新本地邮件文件
        {
            PlayerDataManage::ShareInstance()->UpdateMyMailListToFile();
        }
        else if (vecParmList[0] == SHOP_FRAME_ID)                       // 商店关闭，清空小弟购买列表
        {
            ServerDataManage::ShareInstance()->ClearWareFollower(1);
            ServerDataManage::ShareInstance()->ClearWareFollower(2);
        }
        else if (vecParmList[0] == FOLLOWER_CENTER_FRAME_ID)             // 关闭队伍编辑界面，刷新队伍信息到服务器
        {
            KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
            if (pFrame != NULL)
            {
                Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
                if (pList != NULL)
                {
                    // 获得队伍列表
                    vector<Follower*> vecList = pList->GetFollowerTeamList();
                    
                    // 设置队伍数据到列表
                    PlayerDataManage::ShareInstance()->SetFollowerTeam(&vecList);
                }
            }
        }
        
        // 使用缩小方式关闭对话框
        CCScaleTo* pScaleTo = CCScaleTo::actionWithDuration(0.2f, 0.0f);
        CCEaseBackIn* pEaseBackIn = CCEaseBackIn::actionWithAction(pScaleTo);
        CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.2f, pFrame->oriPos);
        CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(&g_UIEvent, callfuncN_selector(KNUIEvent::CloseFrame));
        
        pFrame->runAction(pMoveTo);
        pFrame->runAction(CCSequence::actions(pEaseBackIn, pFuncN, NULL));
        
//        CCScaleTo* pScaleTo = CCScaleTo::actionWithDuration(0.15f, 0.0f);
//        CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.15f, pFrame->oriPos);
//        CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(&g_UIEvent, callfuncN_selector(KNUIEvent::CloseFrame));
//        if (pScaleTo != NULL && pFuncN != NULL)
//        {
//            pFrame->runAction(pMoveTo);
//            pFrame->runAction(CCSequence::actions(pScaleTo, pFuncN, NULL));
//        }
    }
}

void closeSystemFrame(vector<int> vecParmList)
{
    // 获得要关闭的对话框
    KNFrame* pFrame = KNUIManager::GetSystemManager()->GetFrameByID(vecParmList[0]);
    if (pFrame != NULL && pFrame->getIsVisible() == true)
    {
        // 使用缩小方式关闭对话框
        CCScaleTo* pScaleTo = CCScaleTo::actionWithDuration(0.15f, 0.0f);
        CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(&g_UIEvent, callfuncN_selector(KNUIEvent::CloseFrame));
        if (pScaleTo != NULL && pFuncN != NULL)
            pFrame->runAction(CCSequence::actions(pScaleTo, pFuncN, NULL));
    }
}

/************************************************************************/
#pragma mark - 通用事件 : 关闭战斗中的提示对话框
/************************************************************************/
void closeMessageFrameInBattle(vector<int> vecParmList)
{
    // 获得要关闭的对话框
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(vecParmList[0]);
    if (pFrame != NULL && pFrame->getIsVisible() == true)
    {
        // 使用缩小方式关闭对话框
        CCScaleTo* pScaleTo = CCScaleTo::actionWithDuration(0.15f, 0.0f);
        CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(&g_UIEvent, callfuncN_selector(KNUIEvent::CloseFrame));
        if (pScaleTo != NULL && pFuncN != NULL)
        {
            pFrame->runAction(CCSequence::actions(pScaleTo, pFuncN, NULL));
        }
        else
        {
            pFrame->setIsVisible(false);
        }
    }
    
    if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
    {
        if(Layer_Enemy::ShareInstance() -> GetGuideStep() == 4)
        {
            Layer_Block::ShareInstance() -> SetOperationEnable(true);
            Layer_Enemy::ShareInstance() -> SetOperationEnable(false);
            
            Layer_GameGuide::GetSingle() -> RunBattleGuideStep1();
            
            Layer_Enemy::ShareInstance() -> SetGuideStep(5);
        }
        else if(Layer_Enemy::ShareInstance() -> GetGuideStep() == 5)
        {
            Layer_Block::ShareInstance() -> SetOperationEnable(true);
            Layer_Enemy::ShareInstance() -> SetOperationEnable(false);
            
            Layer_GameGuide::GetSingle() -> RunBattleGuideStep2();
            
            Layer_Enemy::ShareInstance() -> SetGuideStep(6);
        }
        else if(Layer_Enemy::ShareInstance() -> GetGuideStep() == 6)
        {
            Layer_Block::ShareInstance() -> SetOperationEnable(true);
            Layer_Enemy::ShareInstance() -> SetOperationEnable(false);
            
            Layer_GameGuide::GetSingle() -> RunBattleGuideStep3();
            
            Layer_Enemy::ShareInstance() -> SetGuideStep(7);
        }
    }
}

/************************************************************************/
#pragma mark - 通用事件 : 打开对话框关闭其他
/************************************************************************/
void openFrameAndCloseOthers(vector<int> vecParmList)
{
    // 打开第一个界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(vecParmList[0]);
    if (pFrame != NULL && pFrame->getIsVisible() == false)
    {
        pFrame->setScale(1.0f);
        pFrame->setIsVisible(true);
    }
    
    // 关闭后面几个
    for (int i = 1; i < vecParmList.size(); i++)
    {
        pFrame = KNUIManager::GetManager()->GetFrameByID(vecParmList[i]);
        if (pFrame != NULL && pFrame->getIsVisible() == true)
            pFrame->setIsVisible(false);
    }
    
    // 打开详细信息，播放音效
    if (vecParmList[0] == FOLLOWER_FULLINFO_FRAME_ID)
        KNUIFunction::GetSingle()->PlayerEffect("1.wav");
    
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
    {
        // 小弟引导处理
        if (Layer_GameGuide::GetSingle()->GetFloorLockIndex() == F_Follower)
        {
            // 关闭训练详细界面
            if (vecParmList[1] == FOLLOWER_TRAIN_SUCCESS_FRAME_ID)
            {
                KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_INFO_FRAME_ID);
                if (pFrame != NULL && pFrame->getIsVisible() == true)
                    pFrame->setIsVisible(false);
                
                // 关闭小弟列表
                pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
                if (pFrame != NULL && pFrame->getIsVisible() == true)
                    pFrame->setIsVisible(false);
            }
        }
    }
    else
    {
        if (vecParmList[0] == FOLLOWER_FULLINFO_FRAME_ID || vecParmList[0] == FOLLOWER_TRAIN_INFO_FRAME_ID || vecParmList[0] == FOLLOWER_JOB_INFO_FRAME_ID)
        {
            KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
            if (pFrame != NULL)
            {
                Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
                if (pList != NULL)
                {
                    pList->ShowFollowerInfo(pList->GetEditedFollower());
                    pList->ShowFollowerJobInfo(pList->GetEditedFollower(), false);
                    pList->ShowFollowerTrainInfo(pList->GetEditedFollower(), false);
                }
            }
        }
    }
}

/************************************************************************/
#pragma mark - 通用事件 : 关闭对话框打开其他
/************************************************************************/
void closeFrameAndOpenOthers(vector<int> vecParmList)
{
    // 新手引导，不能回去
    if (PlayerDataManage::m_GuideMark)
    {
        if (vecParmList[0] == FOLLOWER_INFO_FRAME_ID)
            return;
        else if (vecParmList[0] == FOLLOWER_LIST_FRAME_ID)
            return;
        else if (vecParmList[0] == MAP_SECOND_FRAME_ID)
            return;
        else if (vecParmList[0] == FRIEND_SELECT_FRAME_ID)
            return;
        else if (vecParmList[0] == FRIEND_FOLLOWER_INFO_FRAME_ID)
            return;
        else if (vecParmList[0] == FOLLOWER_TRAIN_INFO_FRAME_ID)
            return;
        else if (vecParmList[0] == FOLLOWER_TRAIN_SUCCESS_FRAME_ID)
        {
            GameGuide::GetSingle()->FinishOneFloorGuide();
            KNUIFunction::GetSingle()->CloseFrameByNormalType(FOLLOWER_TRAIN_SUCCESS_FRAME_ID);
            return;
        }
    }
    
    // 关闭第一个界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(vecParmList[0]);
    if (pFrame != NULL && pFrame->getIsVisible() == true)
        pFrame->setIsVisible(false);
    
    // 打开后面几个
    for (int i = 1; i < vecParmList.size(); i++)
    {
        pFrame = KNUIManager::GetManager()->GetFrameByID(vecParmList[i]);
        if (pFrame != NULL && pFrame->getIsVisible() == false)
            pFrame->setIsVisible(true);
    }
    
    // 关闭特定的界面，特殊处理
    if (vecParmList[0] == FOLLOWER_FULLINFO_FRAME_ID)                       // 关闭小弟详细界面，刷新小弟信息界面
    {
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
            if (pList != NULL)
                pList->ShowFollowerInfo(pList->GetEditedFollower());
        }
    }
    else if (vecParmList[0] == FRIEND_SELECT_FRAME_ID)                      // 选择好友界面返回，显示小副本界面
    {
        pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
            if (pList != NULL)
                pList->ShowSubMissionFrame();
        }
    }
    else if (vecParmList[0] == FOLLOWER_JOB_SUCCESS_FRAME_ID)               // 转职成功，进行转职处理
    {
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            // 设置列表状态
            Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
            if (pList != NULL)
                pList->ShowFollowerJobInfo(pList->GetEditedFollower(), true);
        }
    }
}

/************************************************************************/
#pragma mark - login事件 : ui(1000) 选择玩家职业
/************************************************************************/
void SelectPlayerJob(vector<int> vecParmList)
{
    // 职业设置
    if (vecParmList[0] == 0)
        PlayerDataManage::m_PlayerFistGameProfession = GUN;
    else if (vecParmList[0] == 1)
        PlayerDataManage::m_PlayerFistGameProfession = SWORD;
    else if (vecParmList[0] == 2)
        PlayerDataManage::m_PlayerFistGameProfession = FIST;
    
    // 更新登陆界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(LOGIN_JOB_SELECT_FRAME_ID);
    if (pFrame != NULL)
    {
        KNLabel* pLabel = NULL;
        for (int i = 0; i < 3; i++)
        {
            // 标记选中的label
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(30000 + i));
            if (pLabel != NULL)
            {
                if (vecParmList[0] + 30000 == 30000 + i)
                    pLabel->GetBackSprite()->setColor(ccc3(255, 255, 255));
                else
                    pLabel->GetBackSprite()->setColor(ccc3(100, 100, 100));
            }
        }
        
        // 切换提示背景和隐藏背景 : 只切换一次
        static int temp = 0;
        if (temp == 0)
        {
            // 切换提示为背景
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
            if (pLabel != NULL)
                pLabel->setIsVisible(false);
            
            ++temp;
        }
    }
    
    // 打开注册框
    //KNUIFunction::GetSingle()->OpenFrameByJumpTypeSpecial(LOGIN_NAME_INPUT_FRAME_ID, ccp(160.0f, 270.0f))
    vecParmList[0] = LOGIN_NAME_INPUT_FRAME_ID;
    openFrame(vecParmList);
    
    pFrame = KNUIManager::GetManager()->GetFrameByID(LOGIN_NAME_INPUT_FRAME_ID);
    if (pFrame != NULL)
    {
        // 设置打开位置
        pFrame->oriPos = ccp(160.0f, 270.0f);
        
        // 打开键盘
        KNTextField* pTextField = dynamic_cast<KNTextField*>(pFrame->GetSubUIByID(40000));
        if (pTextField != NULL)
            pTextField->attachWithIME();
    }
}

/************************************************************************/
#pragma mark - Layer_Login事件 : button(30000) 进入游戏场景
/************************************************************************/
void EnterGame(vector<int> vecParmList)
{
    // 获得帐号
    const char* szAccount = NULL;
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(LOGIN_NAME_INPUT_FRAME_ID);
    if (pFrame != NULL)
    {
        KNTextField* pField = dynamic_cast<KNTextField*>(pFrame->GetSubUIByID(40000));
        if (pField != NULL)
        {
            szAccount = pField->getText();
            if (strcmp(szAccount, "") == 0)
                return;
            
            memcpy(PlayerDataManage::ShareInstance() -> m_PlayerName, szAccount, sizeof(PlayerDataManage::ShareInstance() -> m_PlayerName));
        }
    }
    
    if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
    {
        //进入战斗引导
        GameController::ShareInstance() -> ReadyToGameScene(GameState_BattleGuide);
    }
    else
    {
        //进入游戏场景
        GameController::ShareInstance() -> ReadyToGameScene(GameState_Game);
    }
}

/************************************************************************/
#pragma mark - game事件 : ui(90006) 进入商店
/************************************************************************/
void EnterShop(vector<int> vecParmList)
{
    ServerDataManage::ShareInstance() -> RequestPayID();
}

/************************************************************************/
#pragma mark - game事件 : ui (1000) 设置背景音乐开关
/************************************************************************/
void setBackMusicOnOff(vector<int> vecParmList)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(SET_MENU_FRAME_ID);
    if (pFrame != NULL)
    {
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
        {
            pLabel->GetBackSprite()->setIsVisible(!pLabel->GetBackSprite()->getIsVisible());
            
            // 设置标记
            PlayerDataManage::ShareInstance()->m_bIsMusicOn = pLabel->GetBackSprite()->getIsVisible();
            
            if (!PlayerDataManage::ShareInstance()->m_bIsMusicOn)
            {
                CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(0);
            }
            else
            {
                CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(1.0f);
                KNUIFunction::GetSingle()->PlayerBackMusic("tower.mp3");
            }
        }
    }
}

/************************************************************************/
#pragma mark - game事件 : ui (1000) 设置音效开关
/************************************************************************/
void setBackEffectOnOff(vector<int> vecParmList)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(SET_MENU_FRAME_ID);
    if (pFrame != NULL)
    {
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setIsVisible(!pLabel->GetBackSprite()->getIsVisible());
        
        // 设置标记
        PlayerDataManage::ShareInstance()->m_bIsEffectOn = pLabel->GetBackSprite()->getIsVisible();
        
        if (!PlayerDataManage::ShareInstance()->m_bIsEffectOn)
        {
            CocosDenshion::SimpleAudioEngine::sharedEngine()->setEffectsVolume(0);
            CocosDenshion::SimpleAudioEngine::sharedEngine()->stopAllEffects();
        }
        else
        {
            CocosDenshion::SimpleAudioEngine::sharedEngine()->setEffectsVolume(1.0f);
        }
    }
}

/************************************************************************/
#pragma mark - game事件 : ui (1000) 设置显示属性
/************************************************************************/
void setAttr(vector<int> vecParmList)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(SET_MENU_FRAME_ID);
    if (pFrame != NULL)
    {
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20004));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setIsVisible(!pLabel->GetBackSprite()->getIsVisible());
    }
}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(10004) button(30001) 进入战斗场景
/************************************************************************/
void EnterBattle(vector<int> vecParmList)
{
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
    {
        GameGuide::GetSingle()->FinishOneFloorGuide();
        
//        // test
//        GameGuide::GetSingle()->PrepareNextFloorGuide();
//        
//        KNUIFunction::GetSingle()->CloseFrameByNormalType(FRIEND_FOLLOWER_INFO_FRAME_ID);
//        KNUIFunction::GetSingle()->CloseFrameByNormalType(FRIEND_SELECT_FRAME_ID);
//        return;
    }
    
    // 背包是否已满
    if (KNUIFunction::GetSingle()->IsPackageFull() == true)
    {
        KNUIFunction::GetSingle()->OpenMessageBoxFrame("背包已满，无法进入副本!");
        return;
    }
    
    // 获得副本序号
    int iMapID = 0;
    int iSubMapIndex = 0;
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
        if (pList != NULL)
        {
            iMapID = pList->GetSelectedMapID();
            iSubMapIndex = pList->GetSubMapIndex();
        }
    }
    
    // 计算实际副本序号
    int iFinalIndex = (iMapID - 1) * 3 + iSubMapIndex;
    
    //给出要进入的副本ID号
    ServerDataManage::ShareInstance() -> SetChoiceMapIndex(iFinalIndex);
    //准备进入副本
    GameController::ShareInstance() -> ReadyToGameScene(GameState_Battle);
    
//    // 新手引导
//    if (PlayerDataManage::m_GuideMark)
//        PlayerDataManage::m_GuideMark = false;

#ifdef GAME_DEBUG
    printf("进副本前 : PlayerLevel = %d\n", PlayerDataManage::m_PlayerLevel);
    printf("进副本前 : PlayerExp = %d\n", PlayerDataManage::m_PlayerExperience);
    printf("进副本前 : PlayerLevelExp = %d\n", PlayerDataManage::m_PlayerLevelUpExperience);
#endif
}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(10005) button(30004 - 30006) 招募小弟
/************************************************************************/
void RecruitFollower(vector<int> vecParmList)
{
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
    {
        if (PlayerDataManage::m_GuideStepMark == GUIDE_SHOP && GameGuide::GetSingle()->GetFloorSubGuideStep() >= 4)
            return;
        
        GameGuide::GetSingle()->FinishOneFloorGuide();
    }
    
    // 播放音效
    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
    
    // 是否超过背包格子数
    if (KNUIFunction::GetSingle()->IsPackageFull() == true)
    {
        KNUIFunction::GetSingle()->OpenMessageBoxFrame("你的背包已满，无法购买！");
        return;
    }
    
    // 请求购买
    ServerDataManage::ShareInstance()->RequestBuyFollower(vecParmList[0], vecParmList[1]);
}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(12001) 进入小弟选择界面
/************************************************************************/
void goToFollowerFrame(vector<int> vecParmList)
{
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
        GameGuide::GetSingle()->FinishOneFloorGuide();
    
    // 判断行动力是否足够
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
        if (pList != NULL)
        {
            if (pList->IsEnterBattleEnable(vecParmList[0] - 1) == false)
            {
                // 显示提示界面
                KNUIFunction::GetSingle()->ShowActionHintFrame();
                return;
            }
            
            // 设置副本序号
            pList->SetSubMapIndex(vecParmList[0]);
        }
    }
    
    // 关闭小副本选择界面
    pFrame = KNUIManager::GetManager()->GetFrameByID(MAP_SECOND_FRAME_ID);
    if (pFrame != NULL && pFrame->getIsVisible() == true)
        pFrame->setIsVisible(false);
    
    // 打开好友小弟界面
    pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_SELECT_FRAME_ID);
    if (pFrame != NULL && pFrame->getIsVisible() == false)
    {
        pFrame->setIsVisible(true);
        
        // 使列表滚动到顶部
        Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
            pList->SetPoint(true);
    }
    
    // 播放音效
    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
}

/************************************************************************/
#pragma mark - Layer_Building
/************************************************************************/
void buildFloor(vector<int> vecParmList)
{
    Layer_Building* pBuilding = Layer_Building::ShareInstance();
    pBuilding->BuildingFloor();
    
    vecParmList[0] = 20001;
    closeFrame(vecParmList);
}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(90002) 刷新小弟列表
/************************************************************************/
void refreshFollowerList(vector<int> vecParmList)
{
    if (vecParmList[0] == 1)
    {
        if (PlayerDataManage::m_PlayerFriendValue < 10)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("友情点不足!");
            return;
        }
    }
    else if (vecParmList[0] == 2)
    {
        if (PlayerDataManage::m_PlayerRMB < 30)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("钻石不足!");
            return;
        }
    }
    
    // 刷新英雄
    ServerDataManage::ShareInstance()->RequestUpdateWareFollower(vecParmList[0]);
}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(20000) 升级楼层
/************************************************************************/
void levelupFloor(vector<int> vecParmList)
{
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
    {
        if (PlayerDataManage::m_GuideStepMark == GUIDE_FLOOR_HAVEST && GameGuide::GetSingle()->GetFloorSubGuideStep() == 2)
            return;
    }
    
    // 是否有楼层在升级
    if (Layer_Building::ShareInstance()->IsFloorInAction() == true)
        return;
    
    // 请求升级
    ServerDataManage::ShareInstance()->RequestLevelUpFloor(Layer_Building::ShareInstance()->GetLevelupFloorID());
    
    // 关闭升级对话框
    vecParmList[0] = 20000;
    closeFrame(vecParmList);
}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(12006) 关闭升级界面
/************************************************************************/
void closeLevelupFrame(vector<int> vecParmList)
{
    closeFrame(vecParmList);
    
    Layer_Building::ShareInstance()->CloseAllFloorTimeUpdate();
}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(11001) 清空队伍信息
/************************************************************************/
//void removeAllTeamMember(vector<int> vecParmList)
//{
//    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
//    if (pFrame != NULL)
//    {
//        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
//        if (pList != NULL)
//        {
//            // 清空小弟队伍列表
//            pList->clearFollowerTeam();
//            
//            // 清空队伍数据列表
//            PlayerDataManage::ShareInstance()->ClearFollowerTeam();
//            
//            // 刷新队伍列表
//            pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TEAM_FRAME_ID);
//            if (pFrame != NULL)
//            {
//                for (int i = 0; i < TeamMaxNum; i++)
//                {
//                    // 刷新小弟icon
//                    KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002 + i * 2));
//                    if (pLabel != NULL)
//                        pLabel->resetLabelImage("label_mask.png");
//                    
//                    // 刷新小弟iconframe
//                    pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002 + i * 2 + 1));
//                    if (pLabel != NULL)
//                        pLabel->resetLabelImage("label_mask.png");
//                }
//            }
//        }
//    }
//}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(11001) 确定按钮
/************************************************************************/
void followerConfirm(vector<int> vecParmList)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->ProcessConfirmEvent();
    }
}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(11001) 清空按钮
/************************************************************************/
void followerClear(vector<int> vecParmList)
{
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
        return;
    
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->ProcessClearEvent();
    }
}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(11001) 解雇小弟
/************************************************************************/
void followerFire(vector<int> vecParmList)
{
    ServerDataManage::ShareInstance()->RequestSellFollower();
}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(12004) 训练小弟
/************************************************************************/
void TrainFollower(vector<int> vecParmList)
{
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
        GameGuide::GetSingle()->FinishOneFloorGuide();
    
    // 播放音效
    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
    
    char szFileName[64] = {0};
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
        {
            // 取得被训练的小弟和训练列表
            Follower* pFollower = pList->GetEditedFollower();
            set<Follower*> followerList = pList->GetTrainList();
            
            // 纪录小弟文件名
            memcpy(szFileName, pFollower->GetData()->Follower_FightIconName, 64);
            
            // 如果没有选择任何的训练小弟，则退出
            if (followerList.size() == 0)
            {
                if (!PlayerDataManage::m_GuideMark)
                    KNUIFunction::GetSingle()->OpenMessageBoxFrame("选择选择，选择！！HOHO");
                return;
            }
            
            // 到最大等级，则退出
            if (pFollower->GetData()->Follower_Level >= pFollower->GetData()->Follower_MaxLevel)
            {
                KNUIFunction::GetSingle()->OpenMessageBoxFrame("该小弟已达最大等级！");
                return;
            }
            
            printf("开始训练 小弟 = %d\n", pFollower->GetData()->Follower_ServerID);
            
            // 设置数据
            list<Follower*> parmList;
            for (set<Follower*>::iterator iter = followerList.begin(); iter != followerList.end(); iter++)
            {
                printf("开始训练 材料 = %d\n", (*iter)->GetData()->Follower_ServerID);
                parmList.push_back((*iter));
            }
            
            int price = int((1.4 * pow(pFollower->GetData()->Follower_Level, 3) - 3 * pow(pFollower->GetData()->Follower_Level, 2) + 10 * pFollower->GetData()->Follower_Level + 192) * pFollower->GetData()->Follower_RegulateAbsorbPrice / 2);
            price = price * (parmList.size());
            
            //可以训练
            if(PlayerDataManage::m_PlayerMoney - price < 0)
            {
                KNUIFunction::GetSingle() -> OpenMessageBoxFrame("当前金钱不够，无法训练该小弟");
                return;
            }
            
            // test
            printf("背包中的小弟: ");
            PlayerFollowerDepot followerList1 = *PlayerDataManage::ShareInstance()->GetMyFollowerDepot();
            for (PlayerFollowerDepot::iterator iter = followerList1.begin(); iter != followerList1.end(); iter++)
            {
                list <Follower*>::iterator it2;
                for (it2 = iter->second.begin(); it2 != iter->second.end(); it2++)
                    printf("%d ", (*it2)->GetData()->Follower_ServerID);
            }
            printf("\n");
            
            // 开始训练
            pFollower->TrainingFollower(&parmList);
        }
    }
    
    // 使按钮不可用
//    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_INFO_FRAME_ID);
//    if (pFrame != NULL)
//    {
//        // 关闭按钮可用
//        for (int i = 0; i < 3; i++)
//        {
//            KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30000 + i));
//            if (pButton != NULL)
//                pButton->setButtonEnable(false);
//        }
//    }
}

/************************************************************************/
#pragma mark - game事件 : ui(12005) 转职小弟
/************************************************************************/
void LevelUpFollower(vector<int> vecParmList)
{
    // 播放音效
    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
    
    char szFileName[64] = {0};
    int iQuality = 0;
    
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
        {
            // 获得转职小弟列表
            Follower* pFollower = pList->GetEditedFollower();
            
            // 记录小弟icon名
            memcpy(szFileName, pFollower->GetData()->Follower_FightIconName, 64);
            
            // 记录小弟品质
            iQuality = pFollower->GetData()->Follower_Quality;
            
            // 如果没有符合的小弟，则退出
            KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_JOB_INFO_FRAME_ID);
            if (pFrame != NULL)
            {
                for (int i = 0; i < 5; i++)
                {
                    // icon
                    KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
                    if (pLabel != NULL)
                    {
                        if (pLabel->GetBackSprite()->getIsVisible() == true && pLabel->GetBackSprite()->getOpacity() != 255)
                        {
                            KNUIFunction::GetSingle()->OpenMessageBoxFrame("没有合适小弟的转职材料！");
                            return;
                        }
                    }
                }
            }
            
            // 检查金钱是否足够
            if (pFollower->GetData()->Follower_EvolvementPrice > PlayerDataManage::m_PlayerMoney)
            {
                KNUIFunction::GetSingle()->OpenMessageBoxFrame("没有足够的金钱!");
                return;
            }
            
            // 该小弟是否已经最大进阶
            if (pFollower->GetData()->Follower_EvolvementID == 0)
            {
                KNUIFunction::GetSingle()->OpenMessageBoxFrame("该小弟已经最大进阶了！");
                return;
            }
            
            // 该小弟是否已经最高级
            if (pFollower->GetData()->Follower_Level != pFollower->GetData()->Follower_MaxLevel)
            {
                KNUIFunction::GetSingle()->OpenMessageBoxFrame("该小弟还没到最大级!");
                return;
            }
            
            // 设置数据
            list<Follower*> parmList;
            set<Follower*> followerList = pList->GetJobList();
            set<Follower*>::iterator iter;
            for (iter = followerList.begin(); iter != followerList.end(); iter++)
                parmList.push_back(*iter);
            
            // 开始转职
            pFollower->EvolvementFollower(&parmList);
        }
    }
}

/************************************************************************/
#pragma mark - game事件 : ui(11006) 查找好友
/************************************************************************/
void FindFriend(vector<int> vecParmList)
{
    // 取得输入id
    const char* szID = NULL;
    int iID = 0;
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FIND_FRAME_ID);
    if (pFrame != NULL)
    {
        KNTextField* pField = dynamic_cast<KNTextField*>(pFrame->GetSubUIByID(40000));
        if (pField != NULL)
        {
            szID = pField->getText();
            if (strcmp(szID, "") == 0)
            {
                KNUIFunction::GetSingle()->OpenMessageBoxFrame("输入点东东吧- -:(");
                return;
            }
            else
            {
                iID = atoi(szID);
            }
        }
    }
    
    // 是否是自己
    if (atoi(PlayerDataManage::ShareInstance()->m_PlayerUserID) == iID)
    {
        KNUIFunction::GetSingle()->OpenMessageBoxFrame("不能查找自己!");
        return;
    }
    
    // 查找是否已经是好友
    map<int, list<FriendInfo> >::iterator it = ServerDataManage::ShareInstance()->m_FriendInfoList.find(0);
    if (it == ServerDataManage::ShareInstance()->m_FriendInfoList.end())
    {
        // 没有列表，直接发送请求
        ServerDataManage::ShareInstance()->RequestFindFriendByUserID(iID);
        return;
    }
    
    list<FriendInfo> friendList = it->second;
    list<FriendInfo>::iterator iter;
    for (iter = friendList.begin(); iter != friendList.end(); iter++)
    {
        if (iter->Fri_UserID == iID)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("这个人已经是你的好友了，2了吧，哈哈！");
            return;
        }
    }
    
    // 发送好友请求
    ServerDataManage::ShareInstance() -> RequestFindFriendByUserID(iID);
}

/************************************************************************/
#pragma mark - game事件 : ui(12010) 添加好友
/************************************************************************/
void addFriend(vector<int> vecParmList)
{
    // 是否超过好友上限
    int iLimit = 20 + PlayerDataManage::m_PlayerLevel * 2;
    if (iLimit > 50)
        iLimit = 50;
    
    map<int, list<FriendInfo> >::iterator it = ServerDataManage::ShareInstance()->m_FriendInfoList.find(0);
    if (it != ServerDataManage::ShareInstance()->m_FriendInfoList.end())
    {
        if (it->second.size() > iLimit)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("已达好友上限！");
            return;
        }
    }
    
    // 发送好友请求
    static int iLastID = 0;
    
    const char* szID = NULL;
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FIND_FRAME_ID);
    if (pFrame != NULL)
    {
        KNTextField* pField = dynamic_cast<KNTextField*>(pFrame->GetSubUIByID(40000));
        if (pField != NULL)
        {
            szID = pField->getText();
            if (strcmp(szID, "") == 0)
                KNUIFunction::GetSingle()->OpenMessageBoxFrame("输入点东东吧- -:(");
            else
            {
                // 记录已发送过的id
                int iID = atoi(szID);
                if (iLastID == iID)
                {
                    KNUIFunction::GetSingle()->OpenMessageBoxFrame("请求已发送，请耐心等待！");
                }
                else
                {
                    iLastID = iID;
                    ServerDataManage::ShareInstance() -> RequestAddFriendByUserID(atoi(szID));
                }
            }
        }
    }
}

/************************************************************************/
#pragma mark - game事件 : ui(12007) 删除好友
/************************************************************************/
void removeFriend(vector<int> vecParmList)
{
    // 删除好友的id
    int iRemoveID = 0;
    static int iLastRemoveID = 0;
    
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
            iRemoveID = pList->GetSelectedFriendID();
    }
    
    // id非法，则退出
    if (iRemoveID == 0)
    {
        printf("好友uid非法，无法删除\n");
        return;
    }
    
    // 发送请求
    if (iLastRemoveID == iRemoveID)
    {
        KNUIFunction::GetSingle()->OpenMessageBoxFrame("请求已发送，请耐心等待！");
    }
    else
    {
        iLastRemoveID = iRemoveID;
        ServerDataManage::ShareInstance()->RequestRemoveFriendByUserID(iRemoveID);
    }
}

/************************************************************************/
#pragma mark - game事件 : ui(12012) 同意加好友
/************************************************************************/
void agreeAddFriend(vector<int> vecParmList)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MAIL_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_MailList* pList = dynamic_cast<Layer_MailList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
        {
            int iEditedFriendServerID = pList->GetEditedFriendServerID();
            
            // 请求好友
            ServerDataManage::ShareInstance()->RequestAgreeAddFriend(iEditedFriendServerID);
        }
    }
}

/************************************************************************/
#pragma mark - game事件 : ui(12012)
/************************************************************************/
void refuseAddFriend(vector<int> vecParmList)
{
    // 刷新邮件列表
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MAIL_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_MailList* pList = dynamic_cast<Layer_MailList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
        {
            // 删除邮件
            PlayerDataManage::ShareInstance()->RemoveMailFromList(pList->GetSelectedMailIndex());
            
            pList->RefreshMailList();
        }
    }
    
    // 添加提示
    KNUIFunction::GetSingle()->OpenMessageBoxFrame("你拒绝了对方！");
    
    // 关闭对话框
    pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_REQUEST_CONFIRM_FRAME_ID);
    if (pFrame != NULL)
        pFrame->setIsVisible(false);
}

/************************************************************************/
#pragma mark - game事件 : ui(30000)
/************************************************************************/
void recoverAction(vector<int> vecParmList)
{
    if(PlayerDataManage::m_PlayerRMB - Price_ReAction >= 0 &&
       PlayerDataManage::m_PlayerActivity < PlayerDataManage::m_PlayerMaxActivity)
    {
        ServerDataManage::ShareInstance() -> RequestBuyAction();
    }
    else if(PlayerDataManage::m_PlayerRMB - Price_ReAction < 0)
    {
        ShowMessage(BType_Firm, "当前钻石不够，无法直接恢复行动力，请进入商店充值钻石");
    }
    else if(PlayerDataManage::m_PlayerActivity == PlayerDataManage::m_PlayerMaxActivity)
    {
        ShowMessage(BType_Firm, "当前行动力已满，不需要恢复");
    }
}

/************************************************************************/
#pragma mark - game事件
/************************************************************************/
void showFollowerInfo(vector<int> vecParmList)
{
    // 获得小弟列表
    WareList followerList = *ServerDataManage::ShareInstance()->GetWareFollowerList();
    WareList::iterator iter = followerList.find(vecParmList[2]);
    if(iter == followerList.end())
        return;
    
    // 获得小弟信息
    map<int, Follower*> shopList = iter->second;
    map<int, Follower*>::iterator shopIter = shopList.find(vecParmList[3]);
    if (shopIter != shopList.end())
        KNUIFunction::GetSingle()->ShowFollowerFullInfo(shopIter->second, FOLLOWER_FULLINFO2_FRAME_ID);
}

/************************************************************************/
#pragma mark - game点击事件 - 移动小副本选择边框
/************************************************************************/
void moveSubMapFrame(vector<int> vecParmList)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MAP_SECOND_FRAME_ID);
    if (pFrame != NULL)
    {
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20039));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setPosition(ccp(0.0f, 83.0f - (3 - vecParmList[2]) * 70.0f));
    }
}

/************************************************************************/
#pragma mark - game事件 - 背包扩展
/************************************************************************/
void ExtendPackage(vector<int> vecParmList)
{
    if(PlayerDataManage::m_PlayerRMB - Price_ExtendPackge >= 0)
    {
        ServerDataManage::ShareInstance() -> RequestExtendPackage();
    }
    else
    {
        ShowMessage(BType_Firm, "当前钻石不够，无法对背包进行扩容，请进入商店充值钻石");
    }
    
    // 刷新背包信息
    KNUIFunction::GetSingle()->UpdatePlatformInfo();
}

/************************************************************************/
#pragma mark - game事件 - 清理背包
/************************************************************************/
void ClearPackage(vector<int> vecParmList)
{
    
}

/************************************************************************/
#pragma mark - game事件 - 立即收获楼层
/************************************************************************/
void HavestFloor(vector<int> vecParmList)
{
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
        GameGuide::GetSingle()->FinishOneFloorGuide();
    
    ServerDataManage::ShareInstance() -> RequestFloorHarvest(Layer_Building::ShareInstance()->GetLevelupFloorID(), 1);
}

/************************************************************************/
#pragma mark - game事件 - 筛选小弟列表
/************************************************************************/
void ScreenFollowerList(vector<int> vecParmList)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->ScreenFollowerList(static_cast<LIMIT_TYPE>(vecParmList[0]));
    }
}

/************************************************************************/
#pragma mark - game事件 - 排序小弟列表
/************************************************************************/
void sortfollower(vector<int> vecParmList)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->SortFollowerList(SORT_STAR_GREAT);
    }
}

/************************************************************************/
#pragma mark - game事件 - 打开小弟列表
/************************************************************************/
void openFollowerList(vector<int> vecParmList)
{
    // 新手引导不能点
    if (PlayerDataManage::m_GuideMark)
        return;
    
    // 打开小弟列表
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL && pFrame->getIsVisible() == false)
    {
        pFrame->setIsVisible(true);
        
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->SetListState(static_cast<FOLLOWER_LIST_STATE>(vecParmList[0]));
    }
    
//    // 关闭英雄中心界面
//    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
//    if (pFrame != NULL && pFrame->getIsVisible() == true)
//        pFrame->setIsVisible(false);
}

/************************************************************************/
#pragma mark - game事件 - 打开小弟信息界面
/************************************************************************/
void openFollowerInfoFrame(vector<int> vecParmList)
{
    // 新手引导，只能点第一个队长
    if (PlayerDataManage::m_GuideMark)
    {
        if (PlayerDataManage::m_GuideStepMark == GUIDE_TEAMEDIT)
        {
            if (vecParmList[0] != 1 || GameGuide::GetSingle()->GetFloorSubGuideStep() == 4)
                return;
            
            GameGuide::GetSingle()->FinishOneFloorGuide();
        }
        else if (PlayerDataManage::m_GuideStepMark == GUIDE_TRAIN)
        {
            if (vecParmList[0] != 0 || GameGuide::GetSingle()->GetFloorSubGuideStep() >= 4)
                return;
            
            GameGuide::GetSingle()->FinishOneFloorGuide();
        }
    }
    // 刷新选中小弟的信息
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        // 设置列表状态
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->ShowFollowerInfo(vecParmList[0]);
    }
    
    // 播放点击音效
    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
    
    // 打开英雄信息界面
    KNUIFunction::GetSingle()->OpenFrameByNormalType(FOLLOWER_INFO_FRAME_ID);
}

/************************************************************************/
#pragma mark - game事件 - 打开小弟训练界面
/************************************************************************/
void openFollowerTrainFrame(vector<int> vecParmList)
{
    // 新手引导不能开
    if (PlayerDataManage::m_GuideMark)
    {
        if (PlayerDataManage::m_GuideStepMark != GUIDE_TRAIN)
            return;
        
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
            if (pList != NULL)
            {
                pList->SetListState(FOLLOWER_TRAIN);
                pList->ShowFollowerTrainInfo(pList->GetEditedFollower(), true);
            }
        }
        GameGuide::GetSingle()->FinishOneFloorGuide();
    }
    
    // 刷新训练界面
    KNUIFunction::GetSingle()->OpenFrameByNormalType(FOLLOWER_LIST_FRAME_ID);
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
        {
            pList->SetListState(FOLLOWER_TRAIN);
            pList->ShowFollowerTrainInfo(pList->GetEditedFollower(), true);
        }
    }
    
    // 打开训练界面
    //KNUIFunction::GetSingle()->OpenFrameByNormalType(FOLLOWER_TRAIN_INFO_FRAME_ID);
    
    // 隐藏小弟信息界面
    KNUIFunction::GetSingle()->CloseFrameByNormalType(FOLLOWER_INFO_FRAME_ID);
}

/************************************************************************/
#pragma mark - game事件 - 打开小弟转职界面
/************************************************************************/
void openFollowerJobFrame(vector<int> vecParmList)
{
    // 新手引导不能开
    if (PlayerDataManage::m_GuideMark)
        return;
    
    // 刷新训练界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->ShowFollowerJobInfo(pList->GetEditedFollower(), false);
    }
    
    // 打开训练界面
    KNUIFunction::GetSingle()->OpenFrameByNormalType(FOLLOWER_JOB_INFO_FRAME_ID);
    
    // 隐藏小弟信息界面
    KNUIFunction::GetSingle()->CloseFrameByNormalType(FOLLOWER_INFO_FRAME_ID);
}

/************************************************************************/
#pragma mark - game事件 - 打开小弟详细界面
/************************************************************************/
void openFollowerFullInfoFrame(vector<int> vecParmList)
{
    // 新手引导不能开
    if (PlayerDataManage::m_GuideMark)
        return;
    
    // 刷新训练界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->ShowFollowerFullInfo(pList->GetEditedFollower());
    }
    
    // 打开训练界面
    KNUIFunction::GetSingle()->OpenFrameByNormalType(FOLLOWER_FULLINFO_FRAME_ID);
    
    // 隐藏小弟信息界面
    KNUIFunction::GetSingle()->CloseFrameByNormalType(FOLLOWER_INFO_FRAME_ID);
}

/************************************************************************/
#pragma mark - Layer_Building事件 : ui(12006) 打开训练编辑界面
/************************************************************************/
void openTrainEditedFrame(vector<int> vecParmList)
{
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
        return;
    
    // 播放音效
    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
    
    // 设置列表状态
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->SetListState(FOLLOWER_TRAIN);
    }
    
    // 打开小弟列表
    KNUIFunction::GetSingle()->OpenFrameByNormalType(FOLLOWER_LIST_FRAME_ID);
    
    // 隐藏小弟训练界面
    KNUIFunction::GetSingle()->CloseFrameByNormalType(FOLLOWER_TRAIN_INFO_FRAME_ID);
}

/************************************************************************/
#pragma mark - game事件 : ui(10002) 进入小弟队伍界面
/************************************************************************/
void openTeamList(vector<int> vecParmList)
{
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
    {
        if (PlayerDataManage::m_GuideStepMark == GUIDE_TRAIN)
            return;
        
        GameGuide::GetSingle()->FinishOneFloorGuide();
    }
    
    // 刷新选中小弟的信息
    KNUIFunction::GetSingle()->OpenFrameByNormalType(FOLLOWER_LIST_FRAME_ID);
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        // 设置列表状态
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->SetListState(FOLLOWER_TEAM);
    }
    
    // 隐藏小弟信息对话框
    KNUIFunction::GetSingle()->CloseFrameByNormalType(FOLLOWER_INFO_FRAME_ID);
}

/************************************************************************/
#pragma mark - game事件 : 设置小弟显示模式
/************************************************************************/
void SetFollowerAttrType(vector<int> vecParmList)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->SetFollowerAttrShowType(static_cast<FOLLOWER_ATTR_SHOW_TYPE>(vecParmList[0]));
    }
}

/************************************************************************/
#pragma mark - game事件 : 排序小弟
/************************************************************************/
void SortFollower(vector<int> vecParmList)
{
    int iType = 0;
    int iSort = 0;
    
    // 获得排序方式
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_SORT_FRAME_ID);
    if (pFrame != NULL)
    {
        // 获得排序类型
        KNRadio* pRadio = NULL;
        for (int i = 0; i < 5; i++)
        {
            pRadio = dynamic_cast<KNRadio*>(pFrame->GetSubUIByID(50002 + i));
            if (pRadio != NULL && pRadio->GetIsSelectedMark() == true)
                break;
            
            ++iType;
        }
        
        iType *= 2;
        
        // 获得排序方式
        for (int i = 0; i < 2; i++)
        {
            pRadio = dynamic_cast<KNRadio*>(pFrame->GetSubUIByID(50007 + i));
            if (pRadio != NULL && pRadio->GetIsSelectedMark() == false)
                break;
            
            ++iSort;
        }
        
        iType += iSort;
    }
    
    // 排序小弟
    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->SortFollowerList(static_cast<SORT_TYPE>(iType));
    }
}

/************************************************************************/
#pragma mark - game事件 : 登陆签到事件
/************************************************************************/
void SignGameForDay(vector<int> vecParmList)
{
    ServerDataManage::ShareInstance()->RequestSignToday();
}

/************************************************************************/
#pragma mark - game事件 : 补签事件
/************************************************************************/
void ReSign(vector<int> vecParmList)
{
    if (vecParmList[0] == 1)
    {
        if (PlayerDataManage::m_PlayerRMB < 1)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("钻石不够！");
            return;
        }
    }
    else if (vecParmList[0] == 2)
    {
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(SIGN_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            Layer_Calendar* pCalendar = dynamic_cast<Layer_Calendar*>(pFrame->GetSubUIByID(80000));
            if (pCalendar != NULL)
            {
                if (pCalendar->GetSignEnableNum() <= 1)
                {
                    KNUIFunction::GetSingle()->OpenMessageBoxFrame("没有需要补签的日期!");
                    return;
                }
                else if (pCalendar->GetSignEnableNum() > PlayerDataManage::m_PlayerRMB)
                {
                    KNUIFunction::GetSingle()->OpenMessageBoxFrame("钻石不够!");
                    return;
                }
            }
        }
    }
    
    // 申请补签
    ServerDataManage::ShareInstance()->RequestLostSign(vecParmList[0]);
}

/************************************************************************/
#pragma mark - game事件 : 开宝箱事件
/************************************************************************/
void OpenBox(vector<int> vecParmList)
{
    // 是否未开启
    if (ServerDataManage::ShareInstance()->m_stSign.iCount < vecParmList[2])
    {
        KNUIFunction::GetSingle()->OpenMessageBoxFrame("该宝箱未开启！");
        return;
    }
    
    // 是否已经开过了
    if (vecParmList[2] == 1)
    {
        // 是否已经开启过
        if (ServerDataManage::ShareInstance()->m_stSign.m_arrAward[0] == 1)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("该宝箱已打开过！");
            return;
        }
    }
    else if (vecParmList[2] == 3)
    {
        // 是否已经开启过
        if (ServerDataManage::ShareInstance()->m_stSign.m_arrAward[1] == 1)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("该宝箱已打开过！");
            return;
        }
    }
    else if (vecParmList[2] == 5)
    {
        // 是否已经开启过
        if (ServerDataManage::ShareInstance()->m_stSign.m_arrAward[2] == 1)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("该宝箱已打开过！");
            return;
        }
    }
    else if (vecParmList[2] == 7)
    {
        // 是否已经开启过
        if (ServerDataManage::ShareInstance()->m_stSign.m_arrAward[3] == 1)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("该宝箱已打开过！");
            return;
        }
    }
    else if (vecParmList[2] == 15)
    {
        // 是否已经开启过
        if (ServerDataManage::ShareInstance()->m_stSign.m_arrAward[4] == 1)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("该宝箱已打开过！");
            return;
        }
    }
    else if (vecParmList[2] == 30)
    {
        // 是否已经开启过
        if (ServerDataManage::ShareInstance()->m_stSign.m_arrAward[5] == 1)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("该宝箱已打开过！");
            return;
        }
    }
    
    // 请求开启宝箱
    ServerDataManage::ShareInstance()->RequestOpenBox(vecParmList[2]);
}

/************************************************************************/
#pragma mark - game事件 : 关闭新手引导对话框函数
/************************************************************************/
void CloseGuideFrame(vector<int> vecParmList)
{
    // 完成引导
    GameGuide::GetSingle()->FinishOneFloorGuide();
    
    KNUIFunction::GetSingle()->CloseFrameByJumpType(13000);
}

#pragma mark -------------------------------------------通用对话框事件函数-------------------------------------------

#pragma mark -------------------------------------------控件回调函数-------------------------------------------

/************************************************************************/
#pragma mark - KNEvent类对话框关闭回调函数
/************************************************************************/
void KNUIEvent::CloseFrame(CCObject* pOjbect)
{
    KNFrame* pFrame = dynamic_cast<KNFrame*>(pOjbect);
    if (pFrame != NULL && pFrame->getIsVisible() == true)
        pFrame->setIsVisible(false);
}

/************************************************************************/
#pragma mark - KNEvent类移出界面图层回调函数
/************************************************************************/
void KNUIEvent::RemoveLayerFromFrame(CCObject* pObject, int* pOpenFrameID)
{
    // 升级的小弟对象
    Follower* pLevelupFollower = NULL;
    int iStarLevel = 0;
    int iMaxStarLevel = 0;
    
    // 刷新训练成功界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        // 得到星级
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
        {
            pLevelupFollower = pList->GetEditedFollower();
            iStarLevel = pList->GetEditedFollower()->GetData()->Follower_Quality;
            iMaxStarLevel = pList->GetEditedFollower()->GetData()->Follower_MaxQuality;
        }
    }
    
    // 开启训练结果
    pFrame = dynamic_cast<KNFrame*>(pObject);
    if (pFrame != NULL)
    {
        // 移出训练共话
        pFrame->removeCommonLayer(0);
        
        // 升级显示成功界面
        if (pLevelupFollower->GetTrainLevelUpMark())
        {
            // 隐藏详细界面
            pFrame->setIsVisible(false);
            
            // 打开训练成功界面
            KNUIFunction::GetSingle()->OpenFrameByJumpType(*pOpenFrameID);
            pFrame = KNUIManager::GetManager()->GetFrameByID(*pOpenFrameID);
            if (pFrame != NULL)
            {
                // 刷新icon
                KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
                if (pLabel != NULL)
                    pLabel->resetLabelImage(pLevelupFollower->GetData()->Follower_FightIconName);
                
                // 刷新星级
                for (int i = 0; i < 5; i++)
                {
                    KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
                    if (pLabel != NULL)
                    {
                        if (i < iStarLevel)
                            pLabel->setIsVisible(true);
                        else
                            pLabel->setIsVisible(false);
                    }
                }
                
                // 刷新星级背景
                for (int i = 0; i < 5; i++)
                {
                    KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20028 + i));
                    if (pLabel != NULL)
                    {
                        if (i < iMaxStarLevel)
                            pLabel->setIsVisible(true);
                        else
                            pLabel->setIsVisible(false);
                    }
                }
                
                // 刷新技能
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20041));
                if (pLabel != NULL)
                {
                    const SkillData* pData = SystemDataManage::ShareInstance()->GetData_ForSkill(pLevelupFollower->GetData()->Follower_CommonlySkillType, pLevelupFollower->GetData()->Follower_CommonlySkillID);
                    if (pData != NULL)
                        pLabel->resetLabelImage(pData->Skill_IconName);
                    else
                        pLabel->resetLabelImage("skill_empty.png");
                }
                
                // 刷新技能等级
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20042));
                if (pLabel != NULL)
                {
                    char szTemp[32] = {0};
                    sprintf(szTemp, "%d", pLevelupFollower->GetData()->Follower_SkillLevel);
                    pLabel->GetNumberLabel()->setString(szTemp);
                }
            }
        }
        else
        {
            // 还原背景
            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20045));
            if (pLabel != NULL)
                pLabel->setIsVisible(true);
            
            // 还原标题
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20000));
            if (pLabel != NULL)
                pLabel->setIsVisible(true);
            
            // 还原背景框
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20036));
            if (pLabel != NULL)
                pLabel->setIsVisible(true);
            
            // 还原关闭按钮
            KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30000));
            if (pButton != NULL)
                pButton->setIsVisible(true);
        }
    }
}

/************************************************************************/
#pragma mark - KNUIEvent类转职步骤回调函数
/************************************************************************/
void KNUIEvent::JobStep(CCObject* pObject, CCObject* p)
{
    // 取得小弟对象
    Follower* pFollower = NULL;
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
        {
            pFollower = pList->GetEditedFollower();
            pList->ShowFollowerJobInfo(pFollower, true);
        }
    }
    
    // 取得对话框
    pFrame = dynamic_cast<KNFrame*>(p);
    CCLayer* pLayer = pFrame->getCommonLayer(0);
    
    // 闪屏
    CCLayerColor* pBack = CCLayerColor::layerWithColor(ccc4(255, 255, 255, 0));
    if (pBack != NULL)
    {
        pBack->setPosition(ccp(-160.0f, -240.0f));
        pLayer->addChild(pBack, 1);
        
        CCFadeIn* pFadeIn = CCFadeIn::actionWithDuration(0.3f);
        CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(0.3f);
        
        pBack->runAction(CCSequence::actions(pFadeIn, pFadeOut, NULL));
    }
    
    // 生成新小弟
    CCSprite* pOldMark = dynamic_cast<CCSprite*>(pLayer->getChildByTag(1212));
    CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(0.3f);
    if (pFadeOut != NULL)
        pOldMark->runAction(pFadeOut);
    
    CCSprite* pNewMark = CCSprite::spriteWithSpriteFrameName(pFollower->GetData()->Follower_FightIconName);
    pNewMark->setAnchorPoint(ccp(0.5f, 0.0f));
    pNewMark->setPosition(ccp(0.0f, -80.0f));
    pNewMark->setOpacity(0);
    pLayer->addChild(pNewMark);
    
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(0.3f);
    CCFadeIn* pFadeIn = CCFadeIn::actionWithDuration(0.1f);
    pNewMark->runAction(CCSequence::actions(pTime, pFadeIn, NULL));
        
    // 创建星星特效
    CCSprite* pStarEffect = CCSprite::spriteWithSpriteFrameName("job_star_effect.png");
    if (pStarEffect != NULL)
    {
        pStarEffect->setPosition(ccp(-75.0f + 37.0f * (pFollower->GetData()->Follower_Quality - 1), -90.0f));
        pStarEffect->setOpacity(0);
        
        pLayer->addChild(pStarEffect);
    }
    
    // 创建动作
    pTime = CCDelayTime::actionWithDuration(1.0f);
    pFadeIn = CCFadeIn::actionWithDuration(0.3f);
    pFadeOut = CCFadeOut::actionWithDuration(0.3f);
    if (pTime != NULL && pFadeIn != NULL && pFadeOut != NULL)
        pStarEffect->runAction(CCSequence::actions(pTime, pFadeIn, pFadeOut, NULL));
    
    // 亮出星星
    char szTemp[32] = {0};
    sprintf(szTemp, "job_star_%d.png", pFollower->GetData()->Follower_Quality);
    CCSprite* pStar = CCSprite::spriteWithSpriteFrameName(szTemp);
    pStar->setPosition(ccp(-75.0f + 37.0f * (pFollower->GetData()->Follower_Quality - 1), -118.0f));
    pStar->setOpacity(0);
    pLayer->addChild(pStar);
    
    pTime = CCDelayTime::actionWithDuration(1.6f);
    pFadeIn = CCFadeIn::actionWithDuration(0.3f);
    if (pFadeIn != NULL)
        pStar->runAction(CCSequence::actions(pTime, pFadeIn, NULL));
    
    // 一段时间后移出动画图层
    pTime = CCDelayTime::actionWithDuration(3.0f);
    CCCallFuncND* pFuncND = CCCallFuncND::actionWithTarget(&g_UIEvent, callfuncND_selector(KNUIEvent::RemoveLayerFromFrame), (void*)&FOLLOWER_JOB_SUCCESS_FRAME_ID);
    if (pTime != NULL && pFuncND != NULL)
        pFrame->runAction(CCSequence::actions(pTime, pFuncND, NULL));
}