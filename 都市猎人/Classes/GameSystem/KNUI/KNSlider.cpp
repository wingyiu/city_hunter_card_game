//
//  KNSlider.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "KNSlider.h"

/************************************************************************/
#pragma mark - KNSlider类构造函数
/************************************************************************/
KNSlider::KNSlider()
{
    // 为slider对象赋初值
    m_pNormal = NULL;
    m_pSelect = NULL;
    
    m_pBar = NULL;
    
    // 为slider属性赋初值
    m_fButtonWidth = 0.0f;
    m_fButtonHeight = 0.0f;
    
    m_bIsSelect = false;
    
    m_eSliderType = SLIDER_VERTICAL;
}

/************************************************************************/
#pragma mark - KNSlider类析构函数
/************************************************************************/
KNSlider::~KNSlider()
{
    // 移除slider对象
    removeChild(m_pNormal, true);
    removeChild(m_pSelect, true);
    removeChild(m_pBar, true);
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNSlider类重写初始化函数－使用自动内存使用
/************************************************************************/
bool KNSlider::init()
{
    if (CCLayer::init() == false)
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - KNSlider类重写退出函数－使用自动内存使用
/************************************************************************/
void KNSlider::onExit()
{
    // 移除slider对象
    removeChild(m_pNormal, true);
    removeChild(m_pSelect, true);
    removeChild(m_pBar, true);
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNSlider类实现基类函数－初始化函数
/************************************************************************/
bool KNSlider::init(UIInfo& stInfo)
{
    // 初始化精灵－滚动条
    m_pBar = CCSprite::spriteWithSpriteFrameName(stInfo.szBarImage);
    if (m_pBar == NULL)
        return false;
    
    m_pBar->setPosition(stInfo.point);
    addChild(m_pBar);
    
    // 初始化精灵－按钮
    m_pNormal = CCSprite::spriteWithSpriteFrameName(stInfo.szNormalImage);
    if (m_pNormal != NULL)
    {
        m_pNormal->setPosition(stInfo.point);
        m_pNormal->setIsVisible(true);
        
        m_fButtonWidth = m_pNormal->getContentSize().width;
        m_fButtonHeight = m_pNormal->getContentSize().height;
        
        addChild(m_pNormal);
    }
    // 初始化精灵－按钮
    m_pSelect = CCSprite::spriteWithSpriteFrameName(stInfo.szSelectImage);
    if (m_pSelect != NULL)
    {
        m_pSelect->setPosition(stInfo.point);
        m_pSelect->setIsVisible(false);
        
        m_fButtonWidth = m_pSelect->getContentSize().width;
        m_fButtonHeight = m_pSelect->getContentSize().height;
        
        addChild(m_pSelect);
    }
    
    // 激活点击事件
    if (stInfo.bIsVisible)
        CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    // 设置滚动条类型
    if (stInfo.iType > 0 && stInfo.iType <= 1)
        m_eSliderType = static_cast<SLIDERTYPE>(stInfo.iType);
    
    // 设置百分比
    setPercent(0.0f);
    
    return true;
}

/************************************************************************/
#pragma mark - KNSlider类事件函数－点击
/************************************************************************/
bool KNSlider::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    point = convertTouchToNodeSpace(pTouch);
    
    CCPoint buttonPos = m_pNormal->getPosition();
    
    if (point.x > buttonPos.x - m_fButtonWidth / 2.0f && point.x < buttonPos.x + m_fButtonWidth / 2.0f &&
        point.y > buttonPos.y - m_fButtonHeight / 2.0f && point.y < buttonPos.y + m_fButtonHeight / 2.0f)
    {
        if (m_pNormal != NULL)
            m_pNormal->setIsVisible(false);
        if (m_pSelect != NULL)
            m_pSelect->setIsVisible(true);
        
        m_bIsSelect = true;
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNSlider类事件函数－弹起
/************************************************************************/
void KNSlider::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{    
    if (m_pNormal != NULL)
        m_pNormal->setIsVisible(true);
    if (m_pSelect != NULL)
        m_pSelect->setIsVisible(false);
    
    m_bIsSelect = false;
}

/************************************************************************/
#pragma mark - KNSlider类事件函数－滑动
/************************************************************************/
void KNSlider::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
{
    if (!m_bIsSelect)
        return;
    
    // 获得屏幕坐标
    //CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    // 点击坐标
    CCPoint curPoint = convertTouchToNodeSpace(pTouch);
    
    // 上次点击坐标
    CCPoint lastPoint = pTouch->previousLocationInView(pTouch->view());
    lastPoint = CCDirector::sharedDirector()->convertToGL(lastPoint);
    lastPoint = convertToNodeSpace(lastPoint);
    
    // 移动向量
    CCPoint translation = ccpSub(curPoint, lastPoint);
    
    if (m_eSliderType == SLIDER_VERTICAL)
        translation.x = 0.0f;
    else if (m_eSliderType == SLIDER_HORIZONTAL)
        translation.y = 0.0f;
    
    // 计算新位置
    CCPoint newPos = ccpAdd(m_pNormal->getPosition(), translation);
    CCSize backSize = m_pBar->getContentSize();
    CCPoint backPos = m_pBar->getPosition();
    
    if (newPos.x < backPos.x - backSize.width / 2.0f)
        newPos.x = backPos.x - backSize.width / 2.0f;
    else if (newPos.x > backPos.x + backSize.width / 2.0f)
        newPos.x = backPos.x + backSize.width / 2.0f;
    if (newPos.y > backPos.y + backSize.height / 2.0f)
        newPos.y = backPos.y + backSize.height / 2.0f;
    else if (newPos.y < backPos.y - backSize.height / 2.0f)
        newPos.y = backPos.y - backSize.height / 2.0f;
    
    
    // 设置新位置
    if (m_pNormal != NULL)
        m_pNormal->setPosition(newPos);
    
    if (m_pSelect != NULL)
        m_pSelect->setPosition(newPos);
}

/************************************************************************/
#pragma mark - KNSlider类设置百分比函数
/************************************************************************/
void KNSlider::setPercent(float fPercent)
{
    if (m_eSliderType == SLIDER_VERTICAL)
    {
        
        
    }
    else
    {
        fPercent /= 100.0f;
        
        CCPoint barPos = m_pBar->getPosition();
        CCSize barSize = m_pBar->getContentSize();
        
        float fOffset = barSize.width * fPercent;
        
        CCPoint newPos;
        newPos.x = barPos.x - barSize.width / 2.0f + fOffset;
        newPos.y = barPos.y;
        
        m_pNormal->setPosition(newPos);
        m_pSelect->setPosition(newPos);
    }
}

/************************************************************************/
#pragma mark - KNSlider类获得百分比函数
/************************************************************************/
float KNSlider::getPercent() const
{
    float fPercent = 0.0f;
    
    CCPoint barPos = m_pBar->getPosition();
    CCSize barSize = m_pBar->getContentSize();
    
    CCPoint buttonPos = m_pNormal->getPosition();
    //CCSize buttonSize = m_pNormal->getContentSize();
    
    fPercent = (buttonPos.x - (barPos.x - barSize.width / 2.0f)) / barSize.width;
    
    return fPercent;
}