//
//  KNButton.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "KNButton.h"
#include "KNUIManager.h"
#include "Layer_GameGuide.h"
#include "GameController.h"

/************************************************************************/
#pragma mark - KNButton类构造函数
/************************************************************************/
KNButton::KNButton()
{
    // 为按钮对象赋初值
    m_pNormalSprite = NULL;
    m_pSelectSprite = NULL;
    
    // 为按钮属性赋初值
    m_pEventFunc = NULL;
    
    m_fButtonWidth = 0.0f;
    m_fButtonHeight = 0.0f;
    
    m_bIsEnable = true;
    
    m_pButtonFunc = NULL;
}

/************************************************************************/
#pragma mark - KNButton类析构函数
/************************************************************************/
KNButton::~KNButton()
{
    // 移除精灵
    removeChild(m_pNormalSprite, true);
    removeChild(m_pSelectSprite, true);
    
    m_pNormalSprite = NULL;
    m_pSelectSprite = NULL;
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNButton类重写初始化函数－使用自动内存使用
/************************************************************************/
bool KNButton::init()
{
    if (CCLayer::init() == false)
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - KNButton类重写退出函数－使用自动内存使用
/************************************************************************/
void KNButton::onExit()
{
    // 移除精灵
    removeChild(m_pNormalSprite, true);
    removeChild(m_pSelectSprite, true);
    
    m_pNormalSprite = NULL;
    m_pSelectSprite = NULL;
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNButton类实现基类函数－初始化函数
/************************************************************************/
bool KNButton::init(UIInfo& stInfo)
{
    m_eUIType = stInfo.eType;
    
    m_pNormalSprite = CCSprite::spriteWithSpriteFrameName(stInfo.szNormalImage);
    m_pSelectSprite = CCSprite::spriteWithSpriteFrameName(stInfo.szSelectImage);
    
    if (m_pNormalSprite != NULL)
    {
        m_pNormalSprite->setIsVisible(true);
        m_pNormalSprite->setPosition(stInfo.point);
        addChild(m_pNormalSprite);
        
        m_fButtonWidth = m_pNormalSprite->getContentSize().width;
        m_fButtonHeight = m_pNormalSprite->getContentSize().height;
    }
    if (m_pSelectSprite != NULL)
    {
        m_pSelectSprite->setIsVisible(false);
        m_pSelectSprite->setPosition(stInfo.point);
        addChild(m_pSelectSprite);
        
        m_fButtonWidth = m_pSelectSprite->getContentSize().width;
        m_fButtonHeight = m_pSelectSprite->getContentSize().height;
    }
    
    for (int i = 0; i < MAX_PARM_NUM; i++)
        m_vecParmList.push_back(stInfo.arrParm[i]);
    
    m_iID = stInfo.iID;
    m_iFrameID = stInfo.iFrameID;
    
    // 添加点击事件
    if (stInfo.bIsVisible)
        CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    // 预先读取音效
    if (strcmp(stInfo.szMusicName, "") != 0)
    {
        memcpy(m_szMusicName, stInfo.szMusicName, FILENAMELENGTH);
        CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect(m_szMusicName);
    }
    
    return true;
}

/************************************************************************/
#pragma mark - KNButton类事件函数－点击
/************************************************************************/
bool KNButton::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 如果不可见，则不处理事件
    if (getIsVisible() == false || !m_bIsEnable)
        return false;
    
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    point = convertToNodeSpace(point);
    
    // 检测按钮是否被点击
    CCPoint buttonPos = m_pNormalSprite->getPosition();
    if (point.x > buttonPos.x - m_fButtonWidth / 2.0f && point.x < buttonPos.x + m_fButtonWidth / 2.0f &&
        point.y > buttonPos.y - m_fButtonHeight / 2.0f && point.y < buttonPos.y + m_fButtonHeight / 2.0f)
    {
        if (m_pNormalSprite != NULL)
            m_pNormalSprite->setIsVisible(false);
        if (m_pSelectSprite != NULL)
            m_pSelectSprite->setIsVisible(true);
        
        // 触发点击事件
        if (m_pEventClickFunc != NULL)
            m_pEventClickFunc(m_vecParmList);
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNButton类事件函数－弹起
/************************************************************************/
void KNButton::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    point = convertToNodeSpace(point);
    
    if (m_pNormalSprite != NULL)
        m_pNormalSprite->setIsVisible(true);
    if (m_pSelectSprite != NULL)
        m_pSelectSprite->setIsVisible(false);
    
    // 检测按钮是否被点击
    CCPoint buttonPos = m_pNormalSprite->getPosition();
    if (point.x > buttonPos.x - m_fButtonWidth / 2.0f && point.x < buttonPos.x + m_fButtonWidth / 2.0f &&
        point.y > buttonPos.y - m_fButtonHeight / 2.0f && point.y < buttonPos.y + m_fButtonHeight / 2.0f)
    {
        if (m_pEventFunc != NULL)
            m_pEventFunc(m_vecParmList);
        
        // 播放音效
        if (strcmp(m_szMusicName, "") != 0)
            KNUIFunction::GetSingle()->PlayerEffect(m_szMusicName);
    }
}

/************************************************************************/
#pragma mark - KNButton类设置按钮可用函数
/************************************************************************/
void KNButton::setButtonEnable(bool bIsEnable)
{
    if (bIsEnable)
    {
        m_pNormalSprite->setColor(ccc3(255, 255, 255));
        m_pSelectSprite->setColor(ccc3(255, 255, 255));
        
        if (m_pButtonFunc != NULL)
            m_pEventFunc = m_pButtonFunc;
    }
    else
    {
        m_pNormalSprite->setColor(ccc3(100, 100, 100));
        m_pSelectSprite->setColor(ccc3(100, 100, 100));
        
        m_pButtonFunc = m_pEventFunc;
    }
    
    m_bIsEnable = bIsEnable;
}

/************************************************************************/
#pragma mark - KNButton类重置图片纹理函数
/************************************************************************/
void KNButton::resetButtonImage(const char* szNormalName, const char* szSelectName)
{
    CCPoint normalPos = m_pNormalSprite->getPosition();
    CCPoint selectPos = m_pSelectSprite->getPosition();
    
    removeChild(m_pNormalSprite, true);
    removeChild(m_pSelectSprite, true);
    
    m_pNormalSprite = CCSprite::spriteWithSpriteFrameName(szNormalName);
    m_pSelectSprite = CCSprite::spriteWithSpriteFrameName(szSelectName);
    
    m_pNormalSprite->setPosition(normalPos);
    m_pSelectSprite->setPosition(selectPos);
    
    addChild(m_pNormalSprite);
    addChild(m_pSelectSprite);
}