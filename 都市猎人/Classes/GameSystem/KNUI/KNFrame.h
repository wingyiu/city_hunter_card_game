//
//  KNFrame.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-26.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef MyCocosGame_KNFrame_h
#define MyCocosGame_KNFrame_h

#include "KNBaseUI.h"
#include <map>

using std::map;
using std::make_pair;

class KNFrame : public CCLayer
{
public:
    #pragma mark - 构造函数
    KNFrame();
    #pragma mark - 析构函数
    virtual ~KNFrame();
    
    #pragma mark - 初始化函数
    bool init();
    #pragma mark - 重写退出函数－使用自动内存使用
    void onExit();
    
    #pragma mark - 根据frame信息初始化函数
    bool InitFrameByInfo(FrameInfo& stInfo);
    
    #pragma mark - 根据id获得一个子控件函数
    KNBaseUI* GetSubUIByID(int iID) const;
    
    #pragma mark - 添加一个ui控件函数
    bool AddOneUIToList(int iID, int iLayer, KNBaseUI* pUI);
    #pragma mark - 移除一个ui控件函数
    bool RemoveOneUIFromList(int iID);
    
    #pragma mark - 设置id
    void SetID(int iID)                                     {m_iID = iID;}
    #pragma mark - 获得id
    int GetID() const                                       {return m_iID;}
    
    #pragma mark - 重写可见函数－打开可见并激活事件函数
    void setIsVisible(bool bIsVisible);
    
    #pragma mark - 获得控件列表函数
    map<int, KNBaseUI*> GetUIList() const                   {return m_subUIList;}
    
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    
    #pragma mark - 设置通用控件函数
    bool setCommonLayer(CCLayer* pLayer, int index, CCPoint pos, bool bIsInsert = true);
    #pragma mark - 获得通用控件函数
    CCLayer* getCommonLayer(int index);
    #pragma mark - 移出通用控件函数
    bool removeCommonLayer(int index);
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNFrame);
public:
    CCPoint oriPos;
private:
    // 子控件列表
    map<int, KNBaseUI*> m_subUIList;
    
    // 自身id
    int m_iID;
    
    // 是否限制响应，在顶层的frame捕捉事件
    bool m_bIsFocusTouch;
    
    // 背景精灵
    CCSprite* m_pBackSprite;
    
    // 通用图层控件
    CCLayer* m_pCommonLayer[MAX_COMMONLAYER_NUM];
};

#endif
