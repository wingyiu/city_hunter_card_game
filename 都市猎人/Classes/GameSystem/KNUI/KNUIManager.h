//
//  KNUIManager.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-26.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef MyCocosGame_KNUIManager_h
#define MyCocosGame_KNUIManager_h

#include "KNFrame.h"
#include "KNUIEvent.h"
#include "KNButton.h"
#include "KNLabel.h"
#include "KNProgress.h"
#include "KNTextField.h"
#include "KNSlider.h"
#include "KNScrollView.h"
//#include "KNScrollList.h"
#include "KNTabControl.h"
#include "KNScrollLayer.h"
#include "KNRadio.h"
#include "Layer_FollowerList.h"
#include "Layer_EquipmentList.h"
#include "Layer_MapList.h"
#include "Layer_FollowerBonus.h"
#include "Layer_FriendFollowerList.h"
#include "Layer_DiamondList.h"
#include "Layer_MailList.h"
#include "Layer_MessageList.h"
#include "Layer_Calendar.h"

#include "tinyxml.h"
#include <string>
#include <vector>

using std::string;
using std::vector;

// ui事件函数指针
typedef void (*pFunc)(vector<int> vecParmList);

class KNUIManager : public CCLayer
{
public:
    #pragma mark - 获得Manager对象函数
    static KNUIManager* GetManager();
    #pragma mark - 创建Manager函数
    static KNUIManager* CreateManager();
    #pragma mark - 获得系统manager对象函数
    static KNUIManager* GetSystemManager();
    #pragma mark - 创建系统manager对象函数
    static KNUIManager* CreateSystemManager();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 重写退出函数 : 使用自动内存使用
    void onExit();
    
    #pragma mark - 释放对象函数
    void destroy();
    
    #pragma mark - 读取ui配置表函数
    bool LoadUIConfigFromFile(const char* xmlFilename);
    
    #pragma mark - 根据id获得一个frame函数
    KNFrame* GetFrameByID(int iID) const;
    
    #pragma mark - 添加一个frame到列表函数
    bool AddOneFrameToList(int iID, int iLayer, KNFrame* pFrame);
    #pragma mark - 从ui列表中删除一个frame函数
    bool RemoveOneFrameFromList(int iID);
    
    #pragma mark - 移出所有引导箭头
    void CloseAllGuideArrow();
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNUIManager);
private:
    #pragma mark - 构造函数
    KNUIManager();
    #pragma mark - 析构函数
    virtual ~KNUIManager();
    
    #pragma mark - 根据ui信息创建一个ui函数
    KNBaseUI* CreateUIByUIInfo(UIInfo& stInfo);
private:
    // ui列表
    map<int, KNFrame*> m_uiList;
    
    // ui事件列表
    map<string, pFunc> m_uiEventList;
};

#endif
