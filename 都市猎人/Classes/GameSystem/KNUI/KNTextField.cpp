//
//  KNTextField.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "KNTextField.h"
#include "KNUIManager.h"

// ----------------------------------MyTextField类定义----------------------------------

static int _calcCharCount(const char * pszText)
{
    int n = 0;
    char ch = 0;
    while ((ch = *pszText))
    {
        CC_BREAK_IF(! ch);
        
        if (0x80 != (0xC0 & ch))
        {
            ++n;
        }
        ++pszText;
    }
    return n;
}

/************************************************************************/
#pragma mark - MyTextField类构造函数
/************************************************************************/
MyTextField::MyTextField()
{
    // 初始化文本字符串
    m_InputText.clear();
    
    // 初始化文本长度限制
    m_iMaxLength = 0;
    
    // 初始化文本类型
    m_iType = TEXT_NORMAL;
    
    // 初始化光标显示计时器
    m_iTime = 0;
    
    // 初始化可见标记
    m_bIsVisible = true;
    
    // 初始化光标闪烁标记
    m_bIsLighting = true;
    
    // 初始化光标位置
    m_CursorStartPos.x = m_CursorStartPos.y = 0.0f;
    m_CursorEndPos.x = m_CursorEndPos.y = 0.0f;
}

/************************************************************************/
#pragma mark - MyTextField类析构函数
/************************************************************************/
MyTextField::~MyTextField()
{
    if (canDetachWithIME())
        detachWithIME();
}

/************************************************************************/
#pragma mark - MyTextField类创建文本函数
/************************************************************************/
MyTextField* MyTextField::textFieldWithPlaceHolder(const char *placeholder, const char *fontName, float fontSize, CCSize fieldSize, int iTextLength, int iType)
{
    MyTextField* pField = new MyTextField();
    if (pField->initWithPlaceHolder("", fieldSize, CCTextAlignmentLeft, fontName, fontSize))
    {
        // 使用自动内存机制
        pField->autorelease();
        
        // 设置提示文本
        pField->setPlaceHolder(placeholder);
        // 设置最大文本长度
        pField->m_iMaxLength = iTextLength;
        // 设置文本类型
        pField->m_iType = iType;
        
        return pField;
    }
    
    CC_SAFE_DELETE(pField);
    return NULL;
}

/************************************************************************/
#pragma mark - MyTextField类事件函数 : 输入文本
/************************************************************************/
void MyTextField::insertText(const char *text, int len)
{
    // 是否为回车，回车关闭
    string s = text;
    if (s[s.size() - 1] == '\n')
    {
        // 关闭虚拟键盘
        detachWithIME();
        
        return;
    }
    
    // 判断文本类型
    const char* oriText = getString();
    
    // 情况1 : 已存在文本达到上限
    if (strlen(oriText) >= m_iMaxLength)
        return;
    
    // 情况2 : 取出可插入文本的数量
    int iDeltaLength = m_iMaxLength - strlen(oriText);
    
    if (len <= iDeltaLength)
    {
        string str = oriText;
        str = str + text;
        setString(str.c_str());
    }
    else
    {
        string temp = text;
        temp = temp.substr(0, iDeltaLength);
        
        string str = oriText;
        str = str + temp;
        setString(str.c_str());
    }
}

/************************************************************************/
#pragma mark - MyTextField类字符串设置函数 : 添加密码支持
/************************************************************************/
void MyTextField::setString(const char *text)
{
    CC_SAFE_DELETE(m_pInputText);
    
    if (text)
    {
        m_pInputText = new std::string(text);
    }
    else
    {
        m_pInputText = new std::string;
    }
    
    // if there is no input text, display placeholder instead
    if (! m_pInputText->length())
    {
        CCLabelTTF::setString(m_pPlaceHolder->c_str());
    }
    else
    {
        if ((m_iType & TEXT_PASSWORD) != 0)
        {
            int length = _calcCharCount(m_pInputText->c_str());
            string password;
            
            for (int i = 0; i < length; i++)
                password = password + '*';
            
            CCLabelTTF::setString(password.c_str());
        }
        else
        {
            CCLabelTTF::setString(m_pInputText->c_str());
        }
    }
    m_nCharCount = _calcCharCount(m_pInputText->c_str());
    
//    // 更新光标位置
//    CCPoint pos = getPosition();
//    CCSize size = getContentSize();
//    
//    m_CursorStartPos.x = m_CursorEndPos.x = pos.x + size.width / 2.0f;
//    m_CursorStartPos.y = pos.y - size.height / 3.0f;
//    m_CursorEndPos.y = pos.y + size.height / 3.0f;
}

/************************************************************************/
#pragma mark - MyTextField类重写visit函数 : 显示光标
/************************************************************************/
void MyTextField::visit()
{
    // 调用基类visit函数 : 显示文本
    CCNode::visit();
    
    return;
    // 显示光标
    if (m_bIsVisible)
    {
        if (++m_iTime % 30 == 0)
            m_bIsLighting = !m_bIsLighting;
        
        if (m_bIsLighting)
        {
            glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
            glLineWidth(5);
            ccDrawLine(m_CursorStartPos, m_CursorEndPos);
            glLineWidth(0);
        }
    }
}

/************************************************************************/
#pragma mark - MyTextField类重写键盘弹出函数 : 弹出不同键盘
/************************************************************************/
bool MyTextField::attachWithIME()
{
    bool bRet = CCIMEDelegate::attachWithIME();
    if (bRet)
    {
        // open keyboard
        CCEGLView * pGlView = CCDirector::sharedDirector()->getOpenGLView();
        if (pGlView)
        {
            if ((m_iType & TEXT_NORMAL) != 0)
                pGlView->setIMEKeyboardDefault();
            else if ((m_iType & TEXT_ASCII) != 0)
                pGlView->setIMEKeyboardAcsii();
            else if ((m_iType & TEXT_NUMBER) != 0)
                pGlView->setIMEKeyboardNumber();
            
            pGlView->setIMEKeyboardState(true);
        }
    }
    return bRet;
}

// ----------------------------------KNTextField类定义----------------------------------

/************************************************************************/
#pragma mark - KNTextField类构造函数
/************************************************************************/
KNTextField::KNTextField()
{
    // 为textfield赋初值
    m_pTextField = NULL;
    
    m_pEventFunc = NULL;
}

/************************************************************************/
#pragma mark - KNTextField类析构函数
/************************************************************************/
KNTextField::~KNTextField()
{
    // 移除文本框
    //m_pTextField->detachWithIME();
    removeChild(m_pTextField, true);
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - 初始化函数
/************************************************************************/
bool KNTextField::init()
{
    if (CCLayer::init() == false)
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - 退出函数 : 使用自动内存使用
/************************************************************************/
void KNTextField::onExit()
{
    // 移除文本框
    //m_pTextField->detachWithIME();
    removeChild(m_pTextField, true);
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNTextField类实现基类函数－初始化函数
/************************************************************************/
bool KNTextField::init(UIInfo& stInfo)
{
    m_pTextField = MyTextField::textFieldWithPlaceHolder(stInfo.szText, stInfo.szFont, stInfo.iFontSize, stInfo.size, stInfo.iTextFieldLength, stInfo.iType);
    if (m_pTextField == NULL)
        return false;
    
    m_pTextField->setColor(ccc3(100, 100, 100));
    m_pTextField->setPosition(stInfo.point);
    addChild(m_pTextField);
    
    // 设置左下为铆点
    m_pTextField->setAnchorPoint(ccp(0.0f, 0.0f));
    
    // 激活点击事件
    if (stInfo.bIsVisible)
        CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    for (int i = 0; i < MAX_PARM_NUM; i++)
        m_vecParmList.push_back(stInfo.arrParm[i]);
    
    return true;
}

/************************************************************************/
#pragma mark - KNTextField类事件函数－点击
/************************************************************************/
bool KNTextField::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    point = convertToNodeSpace(point);
    
    float fWidth = m_pTextField->getContentSize().width;
    float fHeight = m_pTextField->getContentSize().height;
    
    CCPoint textPoint = m_pTextField->getPosition();
    
    if (point.x > textPoint.x - fWidth / 2.0f && point.x < textPoint.x + fWidth &&
        point.y > textPoint.y - fHeight / 2.0f && point.y < textPoint.y + fHeight / 2.0f)
    {
        if (m_pTextField != NULL)
        {   
            if (m_pEventFunc != NULL)
                m_pEventFunc(m_vecParmList);
            
            //SetCursorVisible(true);
            m_pTextField->attachWithIME();
        }
        
        return true;
    }
    else
    {
        if (m_pTextField != NULL)
            m_pTextField->detachWithIME();
        
        //SetCursorVisible(false);
        
        return false;
    }
}