//
//  KNScrollLayer.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-4-13.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//
/*
#ifndef MidNightCity_KNScrollLayer_h
#define MidNightCity_KNScrollLayer_h

#include "KNFrame.h"

#include <set>

using std::set;

class KNScrollLayer : public KNBaseUI
{
public:
    #pragma mark - 构造函数
    KNScrollLayer();
    #pragma mark - 析构函数
    virtual ~KNScrollLayer();
    
    #pragma mark - 重写初始化函数 : 使用自动内存使用
    bool init();
    
    #pragma mark - 重写退出函数 : 使用自动内存使用
    virtual void onExit();
    
    #pragma mark - 重写可视函数 : 打开和关闭layer可视属性
    void setIsVisible(bool bIsVisible);
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 添加一个层到列表函数
    bool AddOneLayerToList(CCLayer* pLayer);
    
    #pragma mark - 事件函数－点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－滑动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNScrollLayer);
protected:
    #pragma mark - 设置页数（在不使用页签layer下使用，达到滚屏的效果）
    void SetPageNum(int iPage)      {m_iTotalPages = iPage;}
    #pragma mark - 获得页数
    int GetPageNum()                {return m_iTotalPages;}
private:
    #pragma mark - 重写基类visit函数
    virtual void visit();
    
    #pragma mark - 移动到指定页函数
    void moveToPage(int page);
    #pragma mark - 移动到下一页函数
	void moveToNextPage();
    #pragma mark - 移动到上一页函数
	void moveToPreviousPage();
public:
    // scroll属性变量
    set<CCLayer*> m_LayerList;      // 图层列表
    
    CCRect m_VisibleRange;          // 可视范围
    
    int m_iCurrentPage;             // 当前页面
    int m_iTotalPages;              // 页面数量
    int m_iPressPos;                // 第一次点击坐标
    
    bool m_bIsSubEvent;             // 是否有字控件事件
    
    KNBaseUI* m_pEventUI;           // 触发事件的控件指针
};

#endif
*/