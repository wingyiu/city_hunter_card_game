//
//  KNTabControl.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-4-6.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "KNTabControl.h"
#include "KNUIFunction.h"

/************************************************************************/
#pragma mark - KNTabControl类构造函数
/************************************************************************/
KNTabControl::KNTabControl()
{
    // 为tab对象变量赋初值
    for (int i = 0; i < MAX_TAB_FRAMES; i++)
    {
        m_pNormalSprite[i] = NULL;
        m_pSelectSprite[i] = NULL;
    }
    
    // 为tab属性变量赋初值在
    m_vecFrameList.clear();
    for (int i = 0; i < MAX_TAB_FRAMES; i++)
        m_vecFrameList.push_back(NULL);
    
    m_iFrameNum = 0;
}

/************************************************************************/
#pragma mark - KNTabControl类析构函数
/************************************************************************/
KNTabControl::~KNTabControl()
{
    // 移除按钮结点
    for (int i = 0; i < MAX_TAB_FRAMES; i++)
    {
        removeChild(m_pNormalSprite[i], true);
        removeChild(m_pSelectSprite[i], true);
    }
    
    // 清空列表
    vector<KNFrame*>::iterator iter;
    for (iter = m_vecFrameList.begin(); iter != m_vecFrameList.end(); iter++)
        removeChild(*iter, true);
    m_vecFrameList.clear();
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNTabControl类重写初始化函数－使用自动内存使用
/************************************************************************/
bool KNTabControl::init()
{
    if (CCLayer::init() == false)
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - KNTabControl类重写退出函数－使用自动内存使用
/************************************************************************/
void KNTabControl::onExit()
{
    // 移除按钮结点
    for (int i = 0; i < MAX_TAB_FRAMES; i++)
    {
        removeChild(m_pNormalSprite[i], true);
        removeChild(m_pSelectSprite[i], true);
    }
    
    // 清空列表
    vector<KNFrame*>::iterator iter;
    for (iter = m_vecFrameList.begin(); iter != m_vecFrameList.end(); iter++)
        removeChild(*iter, true);
    m_vecFrameList.clear();
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNTabControl类初始化函数
/************************************************************************/
bool KNTabControl::init(UIInfo& stInfo)
{
    // 设置位置
    setPosition(stInfo.point);
    
    // 初始化按钮
    for (int i = 0; i < MAX_TAB_FRAMES; i++)
    {
        if (strcmp(stInfo.szTabNormal[i], "") == 0 || strcmp(stInfo.szTabSelect[i], "") == 0)
            continue;
        
        m_pNormalSprite[i] = CCSprite::spriteWithSpriteFrameName(stInfo.szTabNormal[i]);
        if (m_pNormalSprite[i] != NULL)
        {
            m_pNormalSprite[i]->setPosition(ccp(stInfo.tabButtonX[i], stInfo.tabButtonY[i]));
            m_pNormalSprite[i]->setIsVisible(true);
            addChild(m_pNormalSprite[i]);
        }
        
        m_pSelectSprite[i] = CCSprite::spriteWithSpriteFrameName(stInfo.szTabSelect[i]);
        if (m_pSelectSprite[i] != NULL)
        {
            m_pSelectSprite[i]->setPosition(ccp(stInfo.tabButtonX[i], stInfo.tabButtonY[i]));
            m_pSelectSprite[i]->setIsVisible(false);
            addChild(m_pSelectSprite[i]);
        }
        
        m_iFrameNum++;
    }
    
    // 放入一个参数
    m_vecParmList.push_back(0);
    
    // 设置可见
    if (stInfo.bIsVisible)
        CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    // 预先加载音效
    memcpy(m_szMusicName, stInfo.szMusicName, FILENAMELENGTH);
    
    return true;
}

/************************************************************************/
#pragma mark - KNTabControl类重写可见函数－打开可见并打开第一个选项卡
/************************************************************************/
void KNTabControl::setIsVisible(bool bIsVisible)
{
    // 打开自身可见
    CCLayer::setIsVisible(bIsVisible);
    
    if (bIsVisible)
    {
        // 打开第一个选项卡
        SetTabEnable(0);
    }
    else
    {
        for (int i = 0; i < m_iFrameNum; i++)
        {
            if (m_vecFrameList[i] != NULL && m_vecFrameList[i]->getIsVisible() == true)
                m_vecFrameList[i]->setIsVisible(false);
        }
    }
}

/************************************************************************/
#pragma mark - 添加一个页签框函数
/************************************************************************/
bool KNTabControl::AddOneFrameToList(KNFrame* pFrame)
{
    if (pFrame == NULL)
        return false;
    
    for (int i = 0; i < m_iFrameNum; i++)
    {
        if (m_vecFrameList[i] == NULL)
        {
            m_vecFrameList[i] = pFrame;
            addChild(pFrame, 1);
            
            return true;
        }
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNTabControl类设置该窗口激活状态函数
/************************************************************************/
bool KNTabControl::SetTabEnable(int iIndex)
{
    if (iIndex >= m_iFrameNum)
    {
        printf("超过tab %d 最大页签\n", iIndex);
        return false;
    }
    
    // 打开特定的页签
    for (int i = 0; i < m_iFrameNum; i++)
    {
        if (i == iIndex)
        {
            if (m_vecFrameList[i] != NULL && m_vecFrameList[i]->getIsVisible() == false)
            {
                m_vecFrameList[i]->setIsVisible(true);
                
                // 特殊处理
                if (m_vecFrameList[i]->GetID() == FOLLOWER_BUY_FRAME_ID)
                    KNUIFunction::GetSingle()->UpdateFollowerInfo();
                else if (m_vecFrameList[i]->GetID() == SUPERFOLLOWER_BUY_FRAME_ID)
                    KNUIFunction::GetSingle()->UpdateSuperFollowerInfo();
            }
            
            m_pSelectSprite[i]->setIsVisible(true);
            m_pNormalSprite[i]->setIsVisible(false);
        }
        else
        {
            if (m_vecFrameList[i] != NULL && m_vecFrameList[i]->getIsVisible() == true)
                m_vecFrameList[i]->setIsVisible(false);
            
            m_pSelectSprite[i]->setIsVisible(false);
            m_pNormalSprite[i]->setIsVisible(true);
        }
    }
    
    return true;
}

/************************************************************************/
#pragma mark - KNTabControl类事件函数－点击
/************************************************************************/
bool KNTabControl::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
        return false;
    
    // 获得点击标记
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    point = convertToNodeSpace(point);
    
    CCPoint buttonPos;
    CCSize buttonSize;
    
    // 检测那个选项卡被按下
    for (int i = 0; i < m_iFrameNum; i++)
    {
        if (m_pNormalSprite[i] == NULL)
            continue;
        
        buttonPos = m_pNormalSprite[i]->getPosition();
        buttonSize = m_pNormalSprite[i]->getContentSize();
        
        if (point.x > buttonPos.x - buttonSize.width / 2.0f && point.x < buttonPos.x + buttonSize.width / 2.0f &&
            point.y > buttonPos.y - buttonSize.height / 2.0f && point.y < buttonPos.y + buttonSize.height / 2.0f)
        {
            // 切换选项卡
            SetTabEnable(i);
            
            // 触发事件
            if (m_pEventFunc != NULL)
            {
                m_vecParmList[0] = i;
                m_pEventFunc(m_vecParmList);
            }
            
            // 播放音效
            if (strcmp(m_szMusicName, "") != 0)
                KNUIFunction::GetSingle()->PlayerEffect(m_szMusicName);
            
            return true;
        }
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNTabControl类事件函数－弹起
/************************************************************************/
void KNTabControl::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    
}