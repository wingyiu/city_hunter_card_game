//
//  KNRadio.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-4-19.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_KNRadio_h
#define MidNightCity_KNRadio_h

#include "KNFrame.h"

class KNRadio : public KNBaseUI
{
public:
    #pragma mark - 构造函数
    KNRadio();
    #pragma mark - 析构函数
    virtual ~KNRadio();
    
    #pragma mark - 初始化函数 : 使用自动内存使用
    bool init();
    #pragma mark - 退出函数 : 使用自动内存使用
    void onExit();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 获得选中标记函数
    bool GetIsSelectedMark() const              {return m_bIsSelected;}
    #pragma mark - 设置选中标记函数
    void SetIsSelectedMark(bool bIsSelected);
    
    #pragma mark - 设置所属对话框id函数
    void SetFrameOwnerID(int iFrameID)          {m_iFrameID = iFrameID;}
    #pragma mark - 获得所属对话框id函数
    int GetFrameOwnerID() const                 {return m_iFrameID;}
    
    #pragma mark - 获得groupid函数
    int GetGroupID() const                      {return m_iGroupID;}
    
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNRadio);
private:
    // radio对象变量
    CCSprite* m_pNormalImage;                   // 普通图片
    CCSprite* m_pSelectImage;                   // 选中图片
    
    KNFrame* m_pFrame;                          // 所属对话框
    
    // radio属性变量
    bool m_bIsSelected;                         // 是否被选中标记
    
    int m_iFrameID;                             // 属于哪个对话框
    int m_iGroupID;                             // 组id
};

#endif