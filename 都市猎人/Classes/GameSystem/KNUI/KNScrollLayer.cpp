//
//  KNScrollLayer.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-4-13.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "KNScrollLayer.h"

/************************************************************************/
#pragma mark - KNScrollLayer类构造函数
/************************************************************************/
//KNScrollLayer::KNScrollLayer()
//{
//    // 为scroll属性变量赋初值
//    m_LayerList.clear();
//    
//    m_VisibleRange.origin.x = m_VisibleRange.origin.y = 0;
//    m_VisibleRange.size.width = m_VisibleRange.size.height = 0;
//    
//    m_iCurrentPage = 0;
//    m_iTotalPages = 0;
//    m_iPressPos = 0;
//    
//    m_bIsSubEvent = false;
//    
//    m_pEventUI = NULL;
//}

/************************************************************************/
#pragma mark - KNScrollLayer类析构函数
/************************************************************************/
//KNScrollLayer::~KNScrollLayer()
//{
//    // 移除所有图层
//    set<CCLayer*>::iterator iter;
//    for (iter = m_LayerList.begin(); iter != m_LayerList.end(); iter++)
//        removeChild(*iter, true);
//    
//    m_LayerList.clear();
//}

/************************************************************************/
#pragma mark - KNScrollLayer类重写初始化函数 : 使用自动内存使用
/************************************************************************/
//bool KNScrollLayer::init()
//{
//    if (CCLayer::init() == false)
//        return false;
//    
//    return true;
//}

/************************************************************************/
#pragma mark - KNScrollLayer类重写退出函数 : 使用自动内存使用
/************************************************************************/
//void KNScrollLayer::onExit()
//{
//    // 移除所有图层
//    set<CCLayer*>::iterator iter;
//    for (iter = m_LayerList.begin(); iter != m_LayerList.end(); iter++)
//        removeChild(*iter, true);
//    
//    m_LayerList.clear();
//}

/************************************************************************/
#pragma mark - KNScrollLayer类重写可视函数 : 打开和关闭layer可视属性
/************************************************************************/
//void KNScrollLayer::setIsVisible(bool bIsVisible)
//{
//    CCLayer::setIsVisible(bIsVisible);
//    
//    set<CCLayer*>::iterator iter;
//    for (iter = m_LayerList.begin(); iter != m_LayerList.end(); iter++)
//        (*iter)->setIsVisible(bIsVisible);
//}

/************************************************************************/
#pragma mark - KNScrollLayer类初始化函数
/************************************************************************/
//bool KNScrollLayer::init(UIInfo& stInfo)
//{
//    if (CCLayer::init() == false)
//        return false;
//    
//    // 初始化当前页面和页面数量
//    m_iCurrentPage = 1;
//    m_iTotalPages = 0;
//    
//    // 初始化可视范围
//    m_VisibleRange.origin = stInfo.point;
//    m_VisibleRange.size = stInfo.size;
//    
//    // 设置位置
//    //setPosition(stInfo.point);
//    
//    // 设置可见属性
//    setIsVisible(stInfo.bIsVisible);
//    
//    return true;
//}

/************************************************************************/
#pragma mark - KNScrollLayer类添加一个层到列表函数
/************************************************************************/
//bool KNScrollLayer::AddOneLayerToList(CCLayer* pLayer)
//{
//    // 非法图层，退出
//    if (pLayer == NULL)
//        return false;
//    
//    // 查询该图层是否已存在
//    set<CCLayer*>::iterator iter = m_LayerList.find(pLayer);
//    if (iter != m_LayerList.end())
//        return false;
//    
//    // 设置图层新位置
//    pLayer->setPosition(ccp(m_iTotalPages * m_VisibleRange.size.width, 0.0f));
//    
//    // 插入图层到列表和图层结点
//    m_LayerList.insert(pLayer);
//    addChild(pLayer);
//    
//    // 增加图层数量
//    ++m_iTotalPages;
//    
//    return true;
//}

// 设备是否支持高清
//extern bool g_bIsEnableRetina;

/************************************************************************/
#pragma mark - KNScrollLayer类重写基类visit函数
/************************************************************************/
//void KNScrollLayer::KNScrollLayer::visit()
//{
//    // 打开opengl裁减
//    glEnable(GL_SCISSOR_TEST);
//    // 设置裁减区域，左下角为0 ,0，水平往右为x正方向，竖直向上为y正方向
//    if (g_bIsEnableRetina)
//        glScissor(m_VisibleRange.origin.x * 2, m_VisibleRange.origin.y * 2, m_VisibleRange.size.width * 2, m_VisibleRange.size.height * 2);
//    else
//        glScissor(m_VisibleRange.origin.x, m_VisibleRange.origin.y, m_VisibleRange.size.width, m_VisibleRange.size.height);
//    // 调用基类方法
//    CCLayer::visit();
//    // 关闭opengl裁减
//    glDisable(GL_SCISSOR_TEST);
//}

/************************************************************************/
#pragma mark - KNScrollLayer类移动到指定页函数
/************************************************************************/
//void KNScrollLayer::moveToPage(int page)
//{
//    CCEaseBounce* changePage = CCEaseBounce::actionWithAction(CCMoveTo::actionWithDuration(0.3f, ccp(-((page - 1) * m_VisibleRange.size.width), getPosition().y)));
//	runAction(changePage);
//    
//	m_iCurrentPage = page;
//}

/************************************************************************/
#pragma mark - KNScrollLayer类移动到下一页函数
/************************************************************************/
//void KNScrollLayer::moveToNextPage()
//{
//    CCEaseBounce* changePage = CCEaseBounce::actionWithAction(CCMoveTo::actionWithDuration(0.3f, ccp(-(((m_iCurrentPage + 1) - 1) * m_VisibleRange.size.width), getPosition().y)));
//	
//	runAction(changePage);
//    
//    m_iCurrentPage += 1;
//}

/************************************************************************/
#pragma mark - KNScrollLayer类移动到上一页函数
/************************************************************************/
//void KNScrollLayer::moveToPreviousPage()
//{
//    CCEaseBounce* changePage =CCEaseBounce::actionWithAction(CCMoveTo::actionWithDuration(0.3f, ccp(-(((m_iCurrentPage - 1) - 1) * m_VisibleRange.size.width), getPosition().y)));
//    runAction(changePage);
//	
//    m_iCurrentPage -= 1;
//}

/************************************************************************/
#pragma mark - KNScrollLayer类事件函数－点击
/************************************************************************/
//bool KNScrollLayer::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
//{
//    // 获得鼠标点击位置
//	CCPoint touchPoint = pTouch->locationInView(pTouch->view());
//	touchPoint = CCDirector::sharedDirector()->convertToGL(touchPoint);
//    
//    // 如果不在控件范围内，则退出，响应其他控件事件
//    if (touchPoint.x < m_VisibleRange.origin.x || touchPoint.x > m_VisibleRange.origin.x + m_VisibleRange.size.width ||
//        touchPoint.y < m_VisibleRange.origin.y || touchPoint.y > m_VisibleRange.origin.y + m_VisibleRange.size.height)
//        return false;
//	
//    // 纪录点击位置，为以后滑动做辅助
//	m_iPressPos = touchPoint.x;
//    
//    // 调用其他控件事件
//    set<CCLayer*>::iterator iter;
//    for (iter = m_LayerList.begin(); iter != m_LayerList.end(); iter++)
//    {
//        // 获得控件列表
//        KNFrame* pFrame = dynamic_cast<KNFrame*>(*iter);
//        if (pFrame != NULL)
//        {
//            map<int, KNBaseUI*> uiList = pFrame->GetUIList();
//            map<int, KNBaseUI*>::iterator uiIter;
//            for (uiIter = uiList.begin(); uiIter != uiList.end(); uiIter++)
//            {
//                m_bIsSubEvent = uiIter->second->ccTouchBegan(pTouch, pEvent);
//                m_pEventUI = uiIter->second;
//                if (m_bIsSubEvent)
//                    break;
//            }
//            
//            if (m_bIsSubEvent)
//                break;
//        }
//    }
//    
//    return true;
//}

/************************************************************************/
#pragma mark - KNScrollLayer类事件函数－弹起
/************************************************************************/
//void KNScrollLayer::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
//{
//    // 如果有子控件事件，处理子控件事件
//    if (m_bIsSubEvent)
//    {
//        m_pEventUI->ccTouchEnded(pTouch, pEvent);
//        return;
//    }
//    
//    // 获得鼠标点击位置
//	CCPoint touchPoint = pTouch->locationInView(pTouch->view());
//	touchPoint = CCDirector::sharedDirector()->convertToGL(touchPoint);
//    
//    // 如果点击位置不在土层范围内，则退出
//    if (touchPoint.x < m_VisibleRange.origin.x || touchPoint.x > m_VisibleRange.origin.x + m_VisibleRange.size.width ||
//        touchPoint.y < m_VisibleRange.origin.y || touchPoint.y > m_VisibleRange.origin.y + m_VisibleRange.size.height)
//        return;
//	
//    // 进行位置滑动
//    int newX = (int)touchPoint.x;
//    if ((newX - m_iPressPos) < -m_VisibleRange.size.width / 3 && (m_iCurrentPage + 1) <= m_iTotalPages)
//        moveToNextPage();
//    else if ((newX - m_iPressPos) > m_VisibleRange.size.width / 3 && (m_iCurrentPage - 1) > 0)
//        moveToPreviousPage();
//    else
//        moveToPage(m_iCurrentPage);
//}

/************************************************************************/
#pragma mark - KNScrollLayer类事件函数－滑动
/************************************************************************/
//void KNScrollLayer::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
//{
//    if (m_bIsSubEvent)
//        return;
//    
//	CCPoint touchPoint = pTouch->locationInView(pTouch->view());
//	touchPoint = CCDirector::sharedDirector()->convertToGL(touchPoint);
//    
//    if (touchPoint.x < m_VisibleRange.origin.x || touchPoint.x > m_VisibleRange.origin.x + m_VisibleRange.size.width ||
//        touchPoint.y < m_VisibleRange.origin.y || touchPoint.y > m_VisibleRange.origin.y + m_VisibleRange.size.height)
//        return;
//	
//    setPosition(ccp((-(m_iCurrentPage - 1) * m_VisibleRange.size.width) + (touchPoint.x - m_iPressPos), getPosition().y));
//}