//
//  KNSlider.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef MyCocosGame_KNSlider_h
#define MyCocosGame_KNSlider_h

#include "KNBaseUI.h"

// 滚动条类型枚举
enum SLIDERTYPE
{
    SLIDER_VERTICAL = 0,                        // 竖直滚动条
    SLIDER_HORIZONTAL,                          // 水平滚动条
};

class KNSlider : public KNBaseUI
{
public:
    #pragma mark - 构造函数
    KNSlider();
    #pragma mark - 析构函数
    virtual ~KNSlider();
    
    #pragma mark - 重写初始化函数－使用自动内存使用
    bool init();
    #pragma mark - 重写退出函数－使用自动内存使用
    virtual void onExit();
    
    #pragma mark - 实现基类函数－初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 事件函数－点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－滑动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
    
    #pragma mark - 设置百分比函数
    void setPercent(float fPercent);
    #pragma mark - 获得百分比函数
    float getPercent() const;
    
    #pragma mark - 设置滚动条类型函数
    void SetSliderType(SLIDERTYPE eType)        {m_eSliderType = eType;}
    #pragma mark - 获得滚动条类型函数
    SLIDERTYPE GetSliderType()                  {return m_eSliderType;}
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNSlider);
private:
    // slider对象
    CCSprite* m_pNormal;                        // 按钮－normal精灵
    CCSprite* m_pSelect;                        // 按钮－select精灵
    
    CCSprite* m_pBar;                           // 轨迹条精灵
    
    // slider属性
    float m_fButtonWidth;                       // 按钮宽度
    float m_fButtonHeight;                      // 按钮高度
    
    bool m_bIsSelect;                           // 选中标记
    
    SLIDERTYPE m_eSliderType;                   // 滚动条类型
};

#endif
