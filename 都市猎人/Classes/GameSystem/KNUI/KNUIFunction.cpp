//
//  KNUIFunction.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-4-16.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "KNUIFunction.h"
#include "Layer_Building.h"
#include "KNUIEvent.h"
#include "Layer_Game.h"
#include "GameController.h"
#include "Scene_Game.h"
#include "Scene_Battle.h"

extern CCPoint teamPos[];
extern KNUIEvent g_UIEvent;

// 小弟性格查询表，以后最好还是修改下配置表
extern const char* g_pCharacterName[];
// 宝箱开启天数
extern int g_arrDayLimit[];

/************************************************************************/
/* function :   KNUIFunction类构造函数                                    */
/************************************************************************/
KNUIFunction::KNUIFunction()
{
    m_bIsNetLoading = false;
}

/************************************************************************/
/* function :   KNUIFunction类析构函数                                    */
/************************************************************************/
KNUIFunction::~KNUIFunction()
{
    
}

/************************************************************************/
/* function :   KNUIFunction类以弹出方式打开对话框函数                        */
/************************************************************************/
bool KNUIFunction::OpenFrameByJumpType(int iFrameID)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(iFrameID);
    if (pFrame != NULL && pFrame->getIsVisible() == false)
    {
        CCScaleTo* pScaleTo = CCScaleTo::actionWithDuration(0.2f, 1.0f);
        CCEaseBackOut* pEaseBackOut = CCEaseBackOut::actionWithAction(pScaleTo);
        
        pFrame->setIsVisible(true);
        pFrame->runAction(pEaseBackOut);
        
        return true;
        
//        CCScaleTo* pScaleTo1 = CCScaleTo::actionWithDuration(0.15f, 1.2f);
//        CCScaleTo* pScaleTo2 = CCScaleTo::actionWithDuration(0.1f, 1.0f);
//        if (pScaleTo1 != NULL && pScaleTo2 != NULL) {
//            pFrame->setScale(0.0f);
//            pFrame->setIsVisible(true);
//            pFrame->runAction(CCSequence::actions(pScaleTo1, pScaleTo2, NULL));
//            
//            return true;
//        }
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNUIFunction类以弹出相应方式关闭对话框函数
/************************************************************************/
bool KNUIFunction::CloseFrameByJumpType(int iFrameID)
{
    vector<int> vecParmList;
    vecParmList.push_back(iFrameID);
    closeFrame(vecParmList);
    
    return true;
}

/************************************************************************/
#pragma mark - KNUIFunction类以普通方式打开对话框函数
/************************************************************************/
bool KNUIFunction::OpenFrameByNormalType(int iFrameID)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(iFrameID);
    if (pFrame != NULL && pFrame->getIsVisible() == false)
    {
        pFrame->setScale(1.0f);
        pFrame->setIsVisible(true);
            
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNUIFunction类以普通方式关闭对话框函数
/************************************************************************/
bool KNUIFunction::CloseFrameByNormalType(int iFrameID)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(iFrameID);
    if (pFrame != NULL && pFrame->getIsVisible() == true)
    {
        pFrame->setIsVisible(false);
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNUIFunction类特殊窗口打开函数
/************************************************************************/
bool KNUIFunction::OpenFrameByJumpTypeSpecial(int iFrameID, CCPoint startPos)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(iFrameID);
    if (pFrame != NULL && pFrame->getIsVisible() == false)
    {
        CCScaleTo* pScaleTo1 = CCScaleTo::actionWithDuration(0.15f, 1.2f);
        CCScaleTo* pScaleTo2 = CCScaleTo::actionWithDuration(0.1f, 1.0f);
        
        CCPoint pos = ccp(160.0f, 240.0f);
        if (iFrameID == 10000 || iFrameID == 10002 || iFrameID == 10004)
            pos = ccp(160.0f, 135.0f);
        
        CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.1f, pos);
        if (pScaleTo1 != NULL && pScaleTo2 != NULL)
        {
            pFrame->oriPos = startPos;
            pFrame->setPosition(startPos);
            pFrame->setScale(0.0f);
            pFrame->setIsVisible(true);
            pFrame->runAction(pMoveTo);
            pFrame->runAction(CCSequence::actions(pScaleTo1, pScaleTo2, NULL));
            
            // 打开楼层遮罩
            if (PlayerDataManage::m_GuideMark == false)
                Layer_Game::GetSingle()->ShowFloorMask();
            
            return true;
        }
        
        // 如果是商城，关闭sold界面
        if (iFrameID == SHOP_FRAME_ID)
        {
            pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_BUY_FRAME_ID);
            if (pFrame != NULL)
            {
                KNLabel* pLabel = NULL;
                for (int i = 0; i < 3; i++)
                {
                    pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20029 + i));
                    if (pLabel != NULL)
                        pLabel->setIsVisible(false);
                }
            }
            
            pFrame = KNUIManager::GetManager()->GetFrameByID(SUPERFOLLOWER_BUY_FRAME_ID);
            if (pFrame != NULL)
            {
                KNLabel* pLabel = NULL;
                for (int i = 0; i < 3; i++)
                {
                    pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20029 + i));
                    if (pLabel != NULL)
                        pLabel->setIsVisible(false);
                }
            }
        }
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNUIFunction类更新小弟商店信息
/************************************************************************/
void KNUIFunction::UpdateFollowerInfo(bool bIsRefresh)
{
    // 获得装备商店界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_BUY_FRAME_ID);
    if (pFrame != NULL)
    {
        // 清空小弟列表
        if (bIsRefresh)
        {
            for (int i = 1; i < 4; i++)
                pFrame->removeCommonLayer(i);
        }
        
        char szTemp[16] = {0};
        
        // 刷新友情点
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20037));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerFriendValue);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 获得装备商店列表
        WareList followerList = *ServerDataManage::ShareInstance()->GetWareFollowerList();
        WareList::iterator iter = followerList.find(1);
        if(iter == followerList.end())
        {
            for (int i = 0; i < 3; i++)
            {
                // 隐藏购买按钮
                KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30001 + i));
                if (pButton != NULL)
                    pButton->setIsVisible(false);
            }
            
            return;
        }
        
        // 刷新列表
        for (int i = 1; i < 4; ++ i)
        {
            map <int, Follower*>::iterator it = iter -> second.find(i);
            if(it != iter -> second.end())
            {
                // 刷小小弟icon
                KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023 + (i - 1) * 2));
                if (pLabel != NULL)
                    pLabel->resetLabelImage(it->second->GetData()->Follower_IconName);
                
                // 刷新小弟frame
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023 + (i - 1) * 2 + 1));
                if (pLabel != NULL)
                {
                    char szTemp[32] = {0};
                    sprintf(szTemp, "FollowerIcon_%d.png", it->second->GetData()->Follower_Profession);
                    pLabel->resetLabelImage(szTemp);
                }
                
                // 更新价格
                if (pFrame != NULL)
                {
                    KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003 + i));
                    if (pLabel != NULL)
                    {
                        sprintf(szTemp, "%.0f", it -> second -> GetData() -> Follower_Price);
                        pLabel->GetNumberLabel()->setString(szTemp);
                    }
                }
                
                // 刷新星星
                for (int j = 0; j < 5; j++)
                {
                    KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008 + (i - 1) * 5 + j));
                    int iQuality = it -> second -> GetData() -> Follower_Quality;
                    if (pLabel != NULL)
                    {
                        if (j >= iQuality)
                            pLabel->setIsVisible(false);
                        else
                            pLabel->setIsVisible(true);
                    }
                }
                
                // 刷新星星背景
                for (int j = 0; j < 5; j++)
                {
                    KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20040 + (i - 1) * 5 + j));
                    int iMaxQuality = it -> second -> GetData() -> Follower_MaxQuality;
                    if (pLabel != NULL)
                    {
                        if (j >= iMaxQuality)
                            pLabel->setIsVisible(false);
                        else
                            pLabel->setIsVisible(true);
                    }
                }
                
                // 刷新小弟姓名
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20032 + i));
                if (pLabel != NULL)
                {
                    pLabel->setLabelText(it->second->GetData()->Follower_Name, NULL, 0);
                    pLabel->setIsVisible(true);
                }
                
                // 关闭售出图片
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20028 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
                
                // 显示购买按钮
                KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30000 + i));
                if (pButton != NULL)
                    pButton->setIsVisible(true);
            }
            else 
            {
                // 打开售出图片
                KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20028 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(true);
                
                // 隐藏购买按钮
                KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30000 + i));
                if (pButton != NULL)
                    pButton->setIsVisible(false);
                
                // 隐藏小弟姓名
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20032 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
            }
        }
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类更新小弟商店信息
/************************************************************************/
void KNUIFunction::UpdateSuperFollowerInfo(bool bIsRefresh)
{
    // 获得装备商店界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(SUPERFOLLOWER_BUY_FRAME_ID);
    if (pFrame != NULL)
    {
        // 清空小弟列表
        if (bIsRefresh)
        {
            for (int i = 1; i < 4; i++)
                pFrame->removeCommonLayer(i);
        }
        
        char szTemp[16] = {0};
        
        // 刷新钻石点
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20037));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerRMB);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 获得装备商店列表
        WareList followerList = *ServerDataManage::ShareInstance()->GetWareFollowerList();
        WareList::iterator iter = followerList.find(2);
        if(iter == followerList.end())
        {
            for (int i = 0; i < 3; i++)
            {
                // 隐藏购买按钮
                KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30001 + i));
                if (pButton != NULL)
                    pButton->setIsVisible(false);
            }
            
            return;
        }
        
        // 刷新列表
        for (int i = 1; i < 4; ++ i)
        {
            map <int, Follower*>::iterator it = iter -> second.find(i);
            if(it != iter -> second.end())
            {
                // 刷小小弟icon
                KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023 + (i - 1) * 2));
                if (pLabel != NULL)
                    pLabel->resetLabelImage(it->second->GetData()->Follower_IconName);
                
                // 刷新小弟frame
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023 + (i - 1) * 2 + 1));
                if (pLabel != NULL)
                {
                    char szTemp[32] = {0};
                    sprintf(szTemp, "FollowerIcon_%d.png", it->second->GetData()->Follower_Profession);
                    pLabel->resetLabelImage(szTemp);
                }
                
                // 更新价格
                if (pFrame != NULL)
                {
                    KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003 + i));
                    if (pLabel != NULL)
                    {
                        sprintf(szTemp, "%.0f", it -> second -> GetData() -> Follower_Price);
                        pLabel->GetNumberLabel()->setString(szTemp);
                    }
                }
                
                // 刷新星星
                for (int j = 0; j < 5; j++)
                {
                    KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008 + (i - 1) * 5 + j));
                    int iLevel = it -> second ->GetData() -> Follower_Quality;
                    if (pLabel != NULL)
                    {
                        if (j >= iLevel)
                            pLabel->setIsVisible(false);
                        else
                            pLabel->setIsVisible(true);
                    }
                }
                
                // 刷新星星背景
                for (int j = 0; j < 5; j++)
                {
                    KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20040 + (i - 1) * 5 + j));
                    int iMaxQuality = it -> second -> GetData() -> Follower_MaxQuality;
                    if (pLabel != NULL)
                    {
                        if (j >= iMaxQuality)
                            pLabel->setIsVisible(false);
                        else
                            pLabel->setIsVisible(true);
                    }
                }
                
                // 刷新小弟姓名
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20032 + i));
                if (pLabel != NULL)
                {
                    pLabel->setLabelText(it->second->GetData()->Follower_Name, NULL, 0);
                    pLabel->setIsVisible(true);
                }
                
                // 关闭售出图片
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20028 + i));
                if (pLabel != NULL)
                    pLabel->GetBackSprite()->setIsVisible(false);
                
                // 打开购买按钮
                KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30000 + i));
                if (pButton != NULL)
                    pButton->setIsVisible(true);
            }
            else 
            {
                // 打开售出图片
                KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20028 + i));
                if (pLabel != NULL)
                    pLabel->GetBackSprite()->setIsVisible(true);
                
                // 隐藏购买按钮
                KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30000 + i));
                if (pButton != NULL)
                    pButton->setIsVisible(false);
                
                // 隐藏小弟姓名
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20032 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
            }
        }
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类更新小弟训练界面信息
/************************************************************************/
void KNUIFunction::UpdateFollowerTrainInfo(Follower* pFollower)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
        {
            pList->clearTrainList();
            pList->ShowFollowerInfo(pFollower);
            pList->ShowFollowerTrainInfo(pFollower, false);
            pList->RefreshFollowerList();
            pList->RefreshFollowerPosition();
        }
    }
    
    // 刷新训练后界面数值
    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_SUCCESS_FRAME_ID);
    if (pFrame != NULL)
    {
        char szTemp[32] = {0};
        
        // 下一等级
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20022));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_Level);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 下一生命值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_HP);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 下一攻击值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20024));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Attack);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 下一恢复值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20025));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Comeback);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新主动技能icon
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20041));
        if (pLabel != NULL)
        {
            const SkillData* pData = SystemDataManage::ShareInstance()->GetData_ForSkill(pFollower->GetData()->Follower_CommonlySkillType, pFollower->GetData()->Follower_CommonlySkillID);
            if (pData != NULL)
                pLabel->resetLabelImage(pData->Skill_IconName);
        }
        
        // 刷新技能等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20042));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_SkillLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
    
    // 清空界面列表
    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        // 清空训练列表
        for (int i = 0; i < 5; i++)
        {
            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015 + i));
            if (pLabel != NULL)
                pLabel->resetLabelImage("label_mask.png");
            
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20024 + i));
            if (pLabel != NULL)
                pLabel->resetLabelImage("label_mask.png");
        }
        
        // 隐藏背景
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20045));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 隐藏标题
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20000));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 隐藏背景框
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20036));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 隐藏关闭按钮
        KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30000));
        if (pButton != NULL)
            pButton->setIsVisible(false);
        
        // 创建特效动画
        CCLayer* pLayer = CCLayer::node();
        if (pLayer != NULL)
        {
            // 创建背景－上
            CCSprite* pImageBackUp = CCSprite::spriteWithSpriteFrameName("job_backimage_up.png");
            pImageBackUp->setPosition(ccp(0.0f, 144.0f));
            // 创建背景－下
            CCSprite* pImageBackDown = CCSprite::spriteWithSpriteFrameName("job_backimage_down.png");
            pImageBackDown->setPosition(ccp(0.0f, -34.0f));
            // 创建背景－边框
            CCSprite* pImageFrame = CCSprite::spriteWithSpriteFrameName("job_back_frame.png");
            
            // 创建前层动画
            CCSprite* pFront = CCSprite::spriteWithSpriteFrameName("train_front_1.png");
            if (pFront != NULL)
            {
                pFront->setPosition(ccp(0.0f, -20.0f));
                CCAnimation* pTrainFront = CreateAnimation("train_front_", 0.16f);
                if (pTrainFront != NULL)
                {
                    CCAnimate* pFrontAnim = CCAnimate::actionWithAnimation(pTrainFront);
                    if (pFrontAnim != NULL)
                    {
                        CCRepeatForever* pFrontForever = CCRepeatForever::actionWithAction(pFrontAnim);
                        if (pFrontForever != NULL)
                            pFront->runAction(pFrontForever);
                    }
                }
            }
            
            // 创建小弟标志
            CCSprite* pFollowerMark = CCSprite::spriteWithSpriteFrameName(pFollower->GetData()->Follower_FightIconName);
            if (pFollowerMark != NULL)
            {
                pFollowerMark->setAnchorPoint(ccp(0.5f, 0.0f));
                pFollowerMark->setPosition(ccp(0.0f, -80.0f));
            }
            
            // 创建后层动画
            CCSprite* pBack = CCSprite::spriteWithSpriteFrameName("train_back_1.png");
            if (pBack != NULL)
            {
                // 设置位置
                pBack->setPosition(ccp(0.0f, -70.0f));
                
                // 创建动画
                CCAnimation* pTrainBack = CreateAnimation("train_back_", 0.06f);
                if (pTrainBack != NULL)
                {
                    CCAnimate* pBackAnim = CCAnimate::actionWithAnimation(pTrainBack);
                    if (pBackAnim != NULL)
                    {
                        CCRepeatForever* pBackForever = CCRepeatForever::actionWithAction(pBackAnim);
                        if (pBackForever != NULL)
                            pBack->runAction(pBackForever);
                    }
                }
            }
            
            // 创建背景星星 － 最大品质
            CCSprite* pStarBack[5] = {0};
            char szTemp[32] = {0};
            for (int i = 0; i < pFollower->GetData()->Follower_MaxQuality; i++)
            {
                sprintf(szTemp, "job_star_b%d.png", i + 1);
                pStarBack[i] = CCSprite::spriteWithSpriteFrameName(szTemp);
                pStarBack[i]->setPosition(ccp(-75.0f + 37.0f * i, -118.0f));
            }
            
            // 创建星星
            CCSprite* pStar[5] = {0};
            for (int i = 0; i < 5; i++)
            {
                sprintf(szTemp, "job_star_%d.png", i + 1);
                pStar[i] = CCSprite::spriteWithSpriteFrameName(szTemp);
                if (pStar[i])
                {
                    pStar[i]->setPosition(ccp(-75.0f + 37.0f * i, -118.0f));
                    
                    if (i > pFollower->GetData()->Follower_Quality - 1)
                        pStar[i]->setOpacity(0);
                }
            }
            
            // 添加到图层
            pLayer->addChild(pImageBackUp);
            pLayer->addChild(pImageBackDown);
            pLayer->addChild(pImageFrame);
            
            pLayer->addChild(pBack);
            pLayer->addChild(pFollowerMark);
            pLayer->addChild(pFront);
            
            for (int i = 0; i < pFollower->GetData()->Follower_MaxQuality; i++)
                pLayer->addChild(pStarBack[i]);
            
            for (int i = 0; i < 5; i++)
                pLayer->addChild(pStar[i]);
            
            // 隐藏前层动画
            CCDelayTime* pTime = CCDelayTime::actionWithDuration(1.9f);
            CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(0.2f);
            if (pTime != NULL && pFadeOut != NULL)
                pFront->runAction(CCSequence::actions(pTime, pFadeOut, NULL));
            
            // 添加到界面中
            pFrame->setCommonLayer(pLayer, 0, ccp(0.0f, 0.0f));
            
            // 一段时间后移出动画图层
            pTime = CCDelayTime::actionWithDuration(3.0f);
            CCCallFuncND* pFuncND = CCCallFuncND::actionWithTarget(&g_UIEvent, callfuncND_selector(KNUIEvent::RemoveLayerFromFrame), (void*)&FOLLOWER_TRAIN_SUCCESS_FRAME_ID);
            if (pTime != NULL && pFuncND != NULL)
                pFrame->runAction(CCSequence::actions(pTime, pFuncND, NULL));
        }
    }
}

/************************************************************************/
#pragma mark - 更新小弟转职界面信息
/************************************************************************/
void KNUIFunction::UpdateFollowerJobInfo(Follower* pFollower)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
        {
            pList->ShowFollowerInfo(pFollower);
            pList->ShowFollowerJobInfo(pFollower, false);
            pList->RefreshFollowerList();
        }
    }
    
    // 刷新转职后界面数值
    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_JOB_SUCCESS_FRAME_ID);
    if (pFrame != NULL)
    {
        char szTemp[32] = {0};
        
        // 下一等级
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20022));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_Level);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 下一生命值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_HP);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 下一攻击值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20024));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Attack);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 下一恢复值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20025));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Comeback);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新星级
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_Quality)
                    pLabel->setIsVisible(true);
                else
                    pLabel->setIsVisible(false);
            }
        }
        
        // 刷新主动技能icon
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20041));
        if (pLabel != NULL)
        {
            const SkillData* pData = SystemDataManage::ShareInstance()->GetData_ForSkill(pFollower->GetData()->Follower_CommonlySkillType, pFollower->GetData()->Follower_CommonlySkillID);
            if (pData != NULL)
                pLabel->resetLabelImage(pData->Skill_IconName);
        }
        
        // 刷新技能等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20042));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_SkillLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
    
    // 打开小弟转职界面
    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_JOB_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        // 创建特效动画
        CCLayer* pLayer = CCLayer::node();
        if (pLayer != NULL)
        {
            // 创建背景－上
            CCSprite* pImageBackUp = CCSprite::spriteWithSpriteFrameName("job_backimage_up.png");
            pImageBackUp->setPosition(ccp(0.0f, 144.0f));
            // 创建背景－下
            CCSprite* pImageBackDown = CCSprite::spriteWithSpriteFrameName("job_backimage_down.png");
            pImageBackDown->setPosition(ccp(0.0f, -34.0f));
            // 创建背景－边框
            CCSprite* pImageFrame = CCSprite::spriteWithSpriteFrameName("job_back_frame.png");
            
            // 创建前层动画
            CCSprite* pFront = CCSprite::spriteWithSpriteFrameName("job_front_1.png");
            if (pFront != NULL)
            {
                pFront->setPosition(ccp(0.0f, -20.0f));
                
                CCAnimation* pTrainFront = CreateAnimation("job_front_", 0.16f);
                if (pTrainFront != NULL)
                {
                    CCAnimate* pFrontAnim = CCAnimate::actionWithAnimation(pTrainFront);
                    CCCallFuncND* pFuncND = CCCallFuncND::actionWithTarget(&g_UIEvent, callfuncND_selector(KNUIEvent::JobStep), (void*)pFrame);
                    if (pFrontAnim != NULL && pFuncND != NULL)
                        pFront->runAction(CCSequence::actions(pFrontAnim, pFuncND, NULL));
                }
            }
            
            // 创建小弟标志
            const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(pFollower->GetData()->Follower_OldID);
            CCSprite* pFollowerMark = CCSprite::spriteWithSpriteFrameName(pData->Follower_FightIconName);
            if (pFollowerMark != NULL)
            {
                pFollowerMark->setAnchorPoint(ccp(0.5f, 0.0f));
                pFollowerMark->setPosition(ccp(0.0f, -80.0f));
            }
            
            // 创建后层动画
            CCSprite* pBack = CCSprite::spriteWithSpriteFrameName("job_back_1.png");
            if (pBack != NULL)
            {
                // 设置位置
                pBack->setPosition(ccp(0.0f, -70.0f));
                
                // 创建动画
                CCAnimation* pTrainBack = CreateAnimation("job_back_", 0.06f);
                if (pTrainBack != NULL)
                {
                    CCAnimate* pBackAnim = CCAnimate::actionWithAnimation(pTrainBack);
                    if (pBackAnim != NULL)
                    {
                        CCRepeatForever* pBackForever = CCRepeatForever::actionWithAction(pBackAnim);
                        if (pBackForever != NULL)
                            pBack->runAction(pBackForever);
                    }
                }
            }
            
            // 创建背景星星 － 最大品质
            CCSprite* pStarBack[5] = {0};
            char szTemp[32] = {0};
            for (int i = 0; i < pFollower->GetData()->Follower_MaxQuality; i++)
            {
                sprintf(szTemp, "job_star_b%d.png", i + 1);
                pStarBack[i] = CCSprite::spriteWithSpriteFrameName(szTemp);
                pStarBack[i]->setPosition(ccp(-75.0f + 37.0f * i, -118.0f));
            }
            
            // 创建星星
            CCSprite* pStar[5] = {0};
            for (int i = 0; i < 5; i++)
            {
                sprintf(szTemp, "job_star_%d.png", i + 1);
                pStar[i] = CCSprite::spriteWithSpriteFrameName(szTemp);
                if (pStar[i])
                {
                    pStar[i]->setPosition(ccp(-75.0f + 37.0f * i, -118.0f));
                    
                    if (i > pFollower->GetData()->Follower_Quality - 2)
                        pStar[i]->setOpacity(0);
                }
            }
            
            // 添加到图层
            pLayer->addChild(pImageBackUp);
            pLayer->addChild(pImageBackDown);
            pLayer->addChild(pImageFrame);
            
            pLayer->addChild(pBack);
            pLayer->addChild(pFollowerMark, 0, 1212);
            pLayer->addChild(pFront);
            
            for (int i = 0; i < pFollower->GetData()->Follower_MaxQuality; i++)
                pLayer->addChild(pStarBack[i]);
            
            for (int i = 0; i < 5; i++)
                pLayer->addChild(pStar[i]);
            
            // 添加到界面中
            pFrame->setCommonLayer(pLayer, 0, ccp(0.0f, 0.0f));
        }
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类刷新小弟队伍列表
/************************************************************************/
void KNUIFunction::UpdateFollowerTeamList()
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->ShowFollowerTeamInfo();
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类刷新副本列表函数
/************************************************************************/
void KNUIFunction::UpdateMapList()
{
    // 该场景中是否有uimanage控件
    CCNode* pNode = CCDirector::sharedDirector()->getRunningScene()->getChildByTag(GAME_SCENE_ID);
    if (pNode == NULL)
        return;
    
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
        if (pList != NULL)
            pList->RefreshMapList();
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类刷新好友列表函数
/************************************************************************/
void KNUIFunction::UpdateFriendList()
{
    // 该场景中是否有uimanage控件
    CCNode* pNode = CCDirector::sharedDirector()->getRunningScene()->getChildByTag(GAME_SCENE_ID);
    if (pNode == NULL)
        return;
    
    // 刷新好友列表
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
            pList->RefreshListData();
    }
    
    // 刷新陌生人列表
    pFrame = KNUIManager::GetManager()->GetFrameByID(STRANGER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
            pList->RefreshListData();
    }
    
    // 刷新副本好友列表
    pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_SELECT_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
            pList->RefreshListData();
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类刷新邮件列表函数
/************************************************************************/
void KNUIFunction::UpdateMailList()
{
    // 该场景中是否有uimanage控件
    CCNode* pNode = CCDirector::sharedDirector()->getRunningScene()->getChildByTag(GAME_SCENE_ID);
    if (pNode == NULL)
        return;
    
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MAIL_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_MailList* pList = dynamic_cast<Layer_MailList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
            pList->RefreshMailList();
    }
    
    // 如果有邮件，打开邮件标记
    if (PlayerDataManage::ShareInstance()->GetMyMailList()->size() != 0)
        Layer_Game::GetSingle()->SetMenuItemState(1, true);
}

/************************************************************************/
#pragma mark - KNUIFunction类刷新任务层新地图函数
/************************************************************************/
void KNUIFunction::UpdateMissionFloorHint()
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
        if (pList != NULL)
        {
            if (pList->GetNewMapExistMark() == true)
                ShowFloorHint(F_Mission);
        }
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类刷新队伍信息函数
/************************************************************************/
void KNUIFunction::UpdateTeamInfo()
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->ShowFollowerTeamInfo();
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类刷新签到信息函数
/************************************************************************/
void KNUIFunction::UpdateSignInfo()
{
    // 补签天数
    int iSignEnableNum = 0;
    
    // 签到界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(SIGN_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        // 临时文本
        char szTemp[32] = {0};
        
        // 当前月份
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
        {
            time_t t = ServerDataManage::ShareInstance()->m_stSign.iTime;
            tm stTime = *gmtime(&t);
            sprintf(szTemp, "%d", stTime.tm_mon + 1);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 领取金币
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", ServerDataManage::ShareInstance()->m_stSign.iMoney);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 累计天数
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", ServerDataManage::ShareInstance()->m_stSign.iCount);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新宝箱
        for (int i = 0; i < 6; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008 + i));
            if (pLabel != NULL)
            {
                // 已经领取
                if (ServerDataManage::ShareInstance()->m_stSign.m_arrAward[i] == 1)
                {
                    switch (i) {
                        case 0:
                            pLabel->resetLabelImage("treasure_open_1.png");
                            break;
                        case 1:
                            pLabel->resetLabelImage("treasure_open_3.png");
                            break;
                        case 2:
                            pLabel->resetLabelImage("treasure_open_5.png");
                            break;
                        case 3:
                            pLabel->resetLabelImage("treasure_open_7.png");
                            break;
                        case 4:
                            pLabel->resetLabelImage("treasure_open_15.png");
                            break;
                        case 5:
                            pLabel->resetLabelImage("treasure_open_30.png");
                            break;
                        default:
                            break;
                    }
                    
                    continue;
                }
                
                // 为领取
                if (ServerDataManage::ShareInstance()->m_stSign.iCount >= g_arrDayLimit[i])
                {
                    switch (i) {
                        case 0:
                            pLabel->resetLabelImage("treasure_unget_1.png");
                            break;
                        case 1:
                            pLabel->resetLabelImage("treasure_unget_3.png");
                            break;
                        case 2:
                            pLabel->resetLabelImage("treasure_unget_5.png");
                            break;
                        case 3:
                            pLabel->resetLabelImage("treasure_unget_7.png");
                            break;
                        case 4:
                            pLabel->resetLabelImage("treasure_unget_15.png");
                            break;
                        case 5:
                            pLabel->resetLabelImage("treasure_unget_30.png");
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    switch (i) {
                        case 0:
                            pLabel->resetLabelImage("treasure_close_1.png");
                            break;
                        case 1:
                            pLabel->resetLabelImage("treasure_close_3.png");
                            break;
                        case 2:
                            pLabel->resetLabelImage("treasure_close_5.png");
                            break;
                        case 3:
                            pLabel->resetLabelImage("treasure_close_7.png");
                            break;
                        case 4:
                            pLabel->resetLabelImage("treasure_close_15.png");
                            break;
                        case 5:
                            pLabel->resetLabelImage("treasure_close_30.png");
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        
        // 刷新日历
        Layer_Calendar* pCalendar = dynamic_cast<Layer_Calendar*>(pFrame->GetSubUIByID(80000));
        if (pCalendar != NULL)
        {
            pCalendar->RefreshCalendarInfo();
            iSignEnableNum = pCalendar->GetSignEnableNum();
        }
    }
    
    // 补签界面
    pFrame = KNUIManager::GetManager()->GetFrameByID(RESIGN_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        // 临时文本
        char szTemp[32] = {0};
        
        // 可补签天数
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", iSignEnableNum);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 全部补签需要钻石数
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", iSignEnableNum);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 当前拥有钻石
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20004));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerRMB);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类加入楼层
/************************************************************************/
void KNUIFunction::AddFloorToGameScene(int iType)
{
    // 获得楼层数据
    PlayerFloorDepot* pList = PlayerDataManage::ShareInstance() -> GetMyFloorDepot();
    if (pList != NULL)
    {
        PlayerFloorDepot::iterator iter = pList->find(iType);
        if (iter != pList->end())
        {
            Floor* pFloor = Floor::node();
            if (pFloor != NULL && pFloor->InitFloorByData(iter->second) == true)
            {
                Layer_Building::ShareInstance()->AddOneFloorToList(iType, pFloor);
                Layer_Building::ShareInstance()->MoveToCurrentFloor();
                
                pFloor->BeginBuilding();
            }
        }
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类完成楼层收获函数
/************************************************************************/
void KNUIFunction::finishFloorHavest(int iType)
{
    Layer_Building::ShareInstance()->finishFloorHavest(iType);
}

/************************************************************************/
#pragma mark - KNUIFunction类提出信息对话框函数
/************************************************************************/
void KNUIFunction::OpenMessageBoxFrame(const char* message)
{
    // 文本id
    static const int TEXT_ID = 100;
    
    // 非法字符，则退出
    if (message == NULL)
        return;
    
    // 找到对话框
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MESSAGEBOX_FRAME_ID);
    if (pFrame != NULL)
    {
        // 移出先前的文本
        pFrame->removeChild(pFrame->getChildByTag(TEXT_ID), true);
        
        // 计算字符框大小
        CCSize fontSize(0.0f, 0.0f);
        
        // 设置宽度，为9个汉字
        fontSize.width = 140.0f;
        
        // 设置高度
        fontSize.height = (strlen(message) / 27 + 1) * 20.0f;
        
        // 生成新文本对象
        CCLabelTTF* pText = CCLabelTTF::labelWithString(message, fontSize, CCTextAlignmentCenter, "黑体", 15);
        if (pText != NULL)
            pFrame->addChild(pText, 0, TEXT_ID);
        
        // 背景尺寸 : 对齐其他控件使用
        CCSize backSize(0.0f, 0.0f);
        // 缩放系数
        float fScaleX = 0.7f;
        float fScaleY = 0.5f;
        
        // 背景缩进
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008));
        if (pLabel != NULL)
        {
            // 还原对话框缩放系数
            pLabel->GetBackSprite()->setScale(1.0f);
            
            // 计算y轴缩放信息
            if (fontSize.height < 60.0f)
                fontSize.height = 60.0f;
            fScaleY = fontSize.height * 3.5f / pLabel->getContentSize().height;
            
            // 设置文本位置
            pText->setPosition(ccp(0.0f, pLabel->GetBackSprite()->getContentSize().height / 2.0f * fScaleY - fontSize.height / 2.0f));
            
            // 缩放背景
            pLabel->GetBackSprite()->setScaleX(fScaleX);
            pLabel->GetBackSprite()->setScaleY(fScaleY);
            
            // 获得背景尺寸
            backSize = pLabel->GetBackSprite()->getContentSize();
            backSize.width *= fScaleX;
            backSize.height *= fScaleY;
        }
        
        // 左上角
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20000));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setPosition(ccp(-backSize.width / 2.0f + 15.0f, backSize.height / 2.0f - 15.0f));
        
        // 右上角
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setPosition(ccp(backSize.width / 2.0f - 15.0f, backSize.height / 2.0f - 15.0f));
        
        // 左下角
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setPosition(ccp(-backSize.width / 2.0f + 15.0f, -backSize.height / 2.0f + 15.0f));
        
        // 右下角
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setPosition(ccp(backSize.width / 2.0f - 15.0f, -backSize.height / 2.0f + 15.0f));
        
        // x轴缩进 - 上条
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20004));
        if (pLabel != NULL)
        {
            pLabel->GetBackSprite()->setScaleX(fScaleX);
            pLabel->GetBackSprite()->setPosition(ccp(0.0f, backSize.height / 2.0f));
        }
        
        // x轴缩进 - 下条
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
        {
            pLabel->GetBackSprite()->setScaleX(fScaleX);
            pLabel->GetBackSprite()->setPosition(ccp(0.0f, -backSize.height / 2.0f));
        }
        
        // y轴缩进 - 左边
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            pLabel->GetBackSprite()->setScaleY(fScaleY);
            pLabel->GetBackSprite()->setPosition(ccp(-backSize.width / 2.0f, 0.0f));
        }
        
        // y轴缩进 - 右边
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            pLabel->GetBackSprite()->setScaleY(fScaleY);
            pLabel->GetBackSprite()->setPosition(ccp(backSize.width / 2.0f, 0.0f));
        }
        
        // 关闭按钮
        KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30000));
        if (pButton != NULL)
        {
            float y = -backSize.height / 2.0f + 26.0f;
            pButton->setPosition(ccp(0.0f, y));
        }
    }
    
    // 打开提示对话框
    KNUIFunction::GetSingle()->OpenFrameByJumpType(MESSAGEBOX_FRAME_ID);
}

/************************************************************************/
/************************************************************************/
void KNUIFunction::OpenSystemMessageBoxFrame(const char* message)
{
    // 文本id
    static const int TEXT_ID = 100;
    
    // 非法字符，则退出
    if (message == NULL)
        return;
    
    // 找到对话框
    KNFrame* pFrame = KNUIManager::GetSystemManager()->GetFrameByID(MESSAGEBOX_FRAME_ID);
    if (pFrame != NULL)
    {
        // 移出先前的文本
        pFrame->removeChild(pFrame->getChildByTag(TEXT_ID), true);
        
        // 计算字符框大小
        CCSize fontSize(0.0f, 0.0f);
        
        // 设置宽度，为9个汉字
        fontSize.width = 140.0f;
        
        // 设置高度
        fontSize.height = (strlen(message) / 27 + 1) * 20.0f;
        
        // 生成新文本对象
        CCLabelTTF* pText = CCLabelTTF::labelWithString(message, fontSize, CCTextAlignmentCenter, "黑体", 15);
        if (pText != NULL)
            pFrame->addChild(pText, 0, TEXT_ID);
        
        // 背景尺寸 : 对齐其他控件使用
        CCSize backSize(0.0f, 0.0f);
        // 缩放系数
        float fScaleX = 0.7f;
        float fScaleY = 0.5f;
        
        // 背景缩进
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008));
        if (pLabel != NULL)
        {
            // 还原对话框缩放系数
            pLabel->GetBackSprite()->setScale(1.0f);
            
            // 计算y轴缩放信息
            if (fontSize.height < 60.0f)
                fontSize.height = 60.0f;
            fScaleY = fontSize.height * 3.5f / pLabel->getContentSize().height;
            
            // 设置文本位置
            pText->setPosition(ccp(0.0f, pLabel->GetBackSprite()->getContentSize().height / 2.0f * fScaleY - fontSize.height / 2.0f));
            
            // 缩放背景
            pLabel->GetBackSprite()->setScaleX(fScaleX);
            pLabel->GetBackSprite()->setScaleY(fScaleY);
            
            // 获得背景尺寸
            backSize = pLabel->GetBackSprite()->getContentSize();
            backSize.width *= fScaleX;
            backSize.height *= fScaleY;
        }
        
        // 左上角
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20000));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setPosition(ccp(-backSize.width / 2.0f + 15.0f, backSize.height / 2.0f - 15.0f));
        
        // 右上角
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setPosition(ccp(backSize.width / 2.0f - 15.0f, backSize.height / 2.0f - 15.0f));
        
        // 左下角
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setPosition(ccp(-backSize.width / 2.0f + 15.0f, -backSize.height / 2.0f + 15.0f));
        
        // 右下角
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setPosition(ccp(backSize.width / 2.0f - 15.0f, -backSize.height / 2.0f + 15.0f));
        
        // x轴缩进 - 上条
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20004));
        if (pLabel != NULL)
        {
            pLabel->GetBackSprite()->setScaleX(fScaleX);
            pLabel->GetBackSprite()->setPosition(ccp(0.0f, backSize.height / 2.0f));
        }
        
        // x轴缩进 - 下条
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
        {
            pLabel->GetBackSprite()->setScaleX(fScaleX);
            pLabel->GetBackSprite()->setPosition(ccp(0.0f, -backSize.height / 2.0f));
        }
        
        // y轴缩进 - 左边
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            pLabel->GetBackSprite()->setScaleY(fScaleY);
            pLabel->GetBackSprite()->setPosition(ccp(-backSize.width / 2.0f, 0.0f));
        }
        
        // y轴缩进 - 右边
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            pLabel->GetBackSprite()->setScaleY(fScaleY);
            pLabel->GetBackSprite()->setPosition(ccp(backSize.width / 2.0f, 0.0f));
        }
        
        // 关闭按钮
        KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30000));
        if (pButton != NULL)
        {
            float y = -backSize.height / 2.0f + 26.0f;
            pButton->setPosition(ccp(0.0f, y));
        }
    }
    
    // 打开提示对话框
    if (pFrame != NULL && pFrame->getIsVisible() == false) {
        CCScaleTo* pScaleTo1 = CCScaleTo::actionWithDuration(0.15f, 1.2f);
        CCScaleTo* pScaleTo2 = CCScaleTo::actionWithDuration(0.1f, 1.0f);
        if (pScaleTo1 != NULL && pScaleTo2 != NULL) {
            pFrame->setScale(0.0f);
            pFrame->setIsVisible(true);
            pFrame->runAction(CCSequence::actions(pScaleTo1, pScaleTo2, NULL));
        }
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类显示小弟详细界面函数 - battle场景使用
/************************************************************************/
void KNUIFunction::ShowFollowerFullInfo(Follower *pFollower, int iFrameID)
{
    // 非法小弟，则退出
    if (pFollower == NULL)
        return;
    
    // 刷新小弟详细信息界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(iFrameID);
    if (pFrame != NULL)
    {
        // 打开界面
        pFrame->setIsVisible(true);
        
        // 辅助变量
        char szTemp[32] = {0};
        
        // 刷新小弟背景
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "follower_quality_%d.png", pFollower->GetData()->Follower_Quality);
            pLabel->resetLabelImage(szTemp);
        }
        
        // 刷笑小弟头像
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->resetLabelImage(pFollower->GetData()->Follower_FightIconName);
        
        // 刷新小弟外框
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "follower_bigFrame_%d.png", pFollower->GetData()->Follower_Profession);
            pLabel->resetLabelImage(szTemp);
        }
        
        // 刷新小弟等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_Level);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟生命值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_HP);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟攻击力
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Attack);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟恢复值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Comeback);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟性格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009));
        if (pLabel != NULL)
        {
            int iCharacterID = pFollower->GetData()->Follower_CharacterID;
            const Character_Data* pData = SystemDataManage::ShareInstance()->GetData_ForCharacter(iCharacterID);
            if (pData != NULL)
            {
                // 查询性格序号
                int index = 0;
                for (int i = 0; i < 7; i++)
                {
                    if (strcmp(pData->Character_Name, g_pCharacterName[i]) == 0)
                    {
                        index = i + 1;
                        break;
                    }
                }
                
                // 生命文件名
                if (index <= 7)
                {
                    sprintf(szTemp, "character_%d.png", index);
                    pLabel->resetLabelImage(szTemp);
                }
            }
        }
        
        // 刷新主动技能
        const SkillData* pMainData = SystemDataManage::ShareInstance()->GetData_ForSkill(pFollower->GetData()->Follower_CommonlySkillType, pFollower->GetData()->Follower_CommonlySkillID);
        if (pMainData != NULL)
        {
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011));
            if (pLabel != NULL)
                pLabel->resetLabelImage(pMainData->Skill_IconName);
            
            // 技能描述
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012));
            if (pLabel != NULL)
                pLabel->setLabelText(pMainData->Skill_Depict, NULL, 0);
            
            // 技能名字
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20025));
            if (pLabel != NULL)
                pLabel->setLabelText(pMainData->Skill_Name, NULL, 0);
            
            // 技能标记
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20033));
            if (pLabel != NULL)
                pLabel->setIsVisible(true);
            
            // 刷新技能等级
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023));
            if (pLabel != NULL)
            {
                sprintf(szTemp, "%d", pFollower->GetData()->Follower_SkillLevel);
                pLabel->setIsVisible(true);
                pLabel->GetNumberLabel()->setString(szTemp);
            }
        }
        else
        {
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011));
            if (pLabel != NULL)
                pLabel->resetLabelImage("skill_empty.png");
            
            // 技能描述
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012));
            if (pLabel != NULL)
                pLabel->setLabelText("无", NULL, 0);
            
            // 技能名字
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20025));
            if (pLabel != NULL)
                pLabel->setLabelText("无", NULL, 0);
            
            // 技能标记
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20033));
            if (pLabel != NULL)
                pLabel->setIsVisible(false);
            
            // 刷新技能等级
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023));
            if (pLabel != NULL)
                pLabel->setIsVisible(false);
        }
        
        // 刷新队长技能
        const SkillData* pTeamData = SystemDataManage::ShareInstance()->GetData_ForSkill(pFollower->GetData()->Follower_TeamSkillType, pFollower->GetData()->Follower_TeamSkillID);
        if (pTeamData != NULL)
        {
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20013));
            if (pLabel != NULL)
                pLabel->resetLabelImage(pTeamData->Skill_IconName);
            
            // 技能描述
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20014));
            if (pLabel != NULL)
                pLabel->setLabelText(pTeamData->Skill_Depict, NULL, 0);
            
            // 技能名字
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20026));
            if (pLabel != NULL)
                pLabel->setLabelText(pTeamData->Skill_Name, NULL, 0);
        }
        else
        {
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20013));
            if (pLabel != NULL)
                pLabel->resetLabelImage("skill_empty.png");
            
            // 技能描述
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20014));
            if (pLabel != NULL)
                pLabel->setLabelText("", NULL, 0);
            
            // 技能名字
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20026));
            if (pLabel != NULL)
                pLabel->setLabelText("", NULL, 0);
        }
        
        // 刷新最大等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_MaxLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新下一级所需经验
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20016));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_Experience);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新星级
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20018 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_Quality)
                    pLabel->setIsVisible(true);
                else
                    pLabel->setIsVisible(false);
            }
        }
        
        // 刷新星星背景
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20028 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_MaxQuality)
                    pLabel->setIsVisible(true);
                else
                    pLabel->setIsVisible(false);
            }
        }
        
        // 刷新技能等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_SkillLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20027));
        if (pLabel != NULL)
            pLabel->setLabelText(pFollower->GetData()->Follower_Name, NULL, 0);
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类显示行动力不足提示界面函数
/************************************************************************/
void KNUIFunction::ShowActionHintFrame()
{
    // 刷新列表信息
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(ACTION_RECOVER_FRAME_ID);
    if (pFrame != NULL)
    {
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerRMB);
            
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
    
    KNUIFunction::GetSingle()->OpenFrameByJumpType(ACTION_RECOVER_FRAME_ID);
}

/************************************************************************/
#pragma mark - KNUIFunction类刷新站台信息函数
/************************************************************************/
void KNUIFunction::UpdatePlatformInfo()
{
    // 刷新好友数量和好友上限
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
            pList->UpdateFriendValueAndLimit();
    }
    
    // 刷新邮件数量
    pFrame = KNUIManager::GetManager()->GetFrameByID(MAIL_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_MailList* pList = dynamic_cast<Layer_MailList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
            pList->UpdateMailNumber();
    }
    
    // 刷新小弟数字
    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        // 当前小弟数量
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
        {
            // 获得小弟数量
            int iFollowerNum = 0;
            PlayerFollowerDepot followerList = *PlayerDataManage::ShareInstance()->GetMyFollowerDepot();
            for (PlayerFollowerDepot::iterator iter = followerList.begin(); iter != followerList.end(); iter++)
            {
                list <Follower*>::iterator it2;
                for (it2 = iter->second.begin(); it2 != iter->second.end(); it2++)
                    ++iFollowerNum;
            }
            
            // 刷新
            char szTemp[32] = {0};
            if (iFollowerNum / 10 == 0)
                sprintf(szTemp, "0%d", iFollowerNum);
            else
                sprintf(szTemp, "%d", iFollowerNum);
            
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 最大小弟数量
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20004));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", PlayerDataManage::m_iPlayerPackageNum);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类更新玩家id函数
/************************************************************************/
void KNUIFunction::UpdatePlayerID()
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(PLAYER_ATTR_FRAME_ID);
    if (pFrame != NULL)
    {
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
        {
            printf("player id = %s\n", PlayerDataManage::ShareInstance()->m_PlayerUserID);
            pLabel->GetNumberLabel()->setString(PlayerDataManage::ShareInstance()->m_PlayerUserID);
        }
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类更新设置信息函数
/************************************************************************/
void KNUIFunction::UpdateSetInfo()
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(SET_MENU_FRAME_ID);
    if (pFrame != NULL)
    {
        // 音乐
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setIsVisible(PlayerDataManage::ShareInstance()->m_bIsMusicOn);
        
        // 音效
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setIsVisible(PlayerDataManage::ShareInstance()->m_bIsEffectOn);
    }
}

/************************************************************************/
#pragma mark - KNUIFunction类播放音乐函数
/************************************************************************/
void KNUIFunction::PlayerBackMusic(const char* szMusicName)
{
    if (PlayerDataManage::ShareInstance()->m_bIsMusicOn)
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(szMusicName, true);
}

/************************************************************************/
#pragma mark - KNUIFunction类停止音乐函数
/************************************************************************/
void KNUIFunction::StopBackMusic()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic(true);
}

/************************************************************************/
#pragma mark - KNUIFunction类播放音效函数
/************************************************************************/
void KNUIFunction::PlayerEffect(const char* szEffectName)
{
    if (PlayerDataManage::ShareInstance()->m_bIsEffectOn)
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(szEffectName);
}

/************************************************************************/
#pragma mark - KNUIFunction类获得经验显示百分比函数
/************************************************************************/
float KNUIFunction::GetExpPercent()
{
    return 1.0f * PlayerDataManage::m_PlayerExperience / PlayerDataManage::m_PlayerLevelUpExperience * 100.0f;
}

/************************************************************************/
#pragma mark - KNUIFunction类获得玩家下一级升级所需经验函数
/************************************************************************/
int KNUIFunction::GetPlayerNeedExpForNextLevel()
{
    return static_cast<int>(1.4 * pow(PlayerDataManage::m_PlayerLevel, 3) - 3 * pow(PlayerDataManage::m_PlayerLevel, 2) + 10 * PlayerDataManage::m_PlayerLevel + 192);
}

/************************************************************************/
#pragma mark - KNUIFunction类背包是否已满函数
/************************************************************************/
bool KNUIFunction::IsPackageFull()
{
    int num = 0;
    PlayerFollowerDepot followerList = *PlayerDataManage::ShareInstance()->GetMyFollowerDepot();
    for (PlayerFollowerDepot::iterator iter = followerList.begin(); iter != followerList.end(); iter++)
    {
        list <Follower*>::iterator it2;
        for (it2 = iter->second.begin(); it2 != iter->second.end(); it2++)
            ++num;
    }
    
    return num >= PlayerDataManage::m_iPlayerPackageNum;
}

/************************************************************************/
#pragma mark - KNUIFunction类显示楼层提示函数
/************************************************************************/
void KNUIFunction::ShowFloorHint(FLOORTYPE eType)
{
    // 取得楼层列表
    Floor* pFloor = Layer_Building::ShareInstance()->GetFloor(eType);
    if (pFloor != NULL)
        pFloor->ShowHintItem();
    
    // 显示右边菜单
    if (eType == F_Mission)
        Layer_Game::GetSingle()->SetMenuItemState(3, true);
}