//
//  KNFrame.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-26.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "KNFrame.h"

/************************************************************************/
#pragma mark - KNFrame类构造函数
/************************************************************************/
KNFrame::KNFrame()
{
    // 清空子控件列表
    m_subUIList.clear();
    
    // 为自身id赋初值
    m_iID = 0;
    
    // 为背景精灵赋初值
    m_pBackSprite = NULL;
    
    // 通用图层控件
    for (int i = 0; i < MAX_COMMONLAYER_NUM; i++)
        m_pCommonLayer[i] = NULL;
    
    m_bIsFocusTouch = false;
    
    oriPos = ccp(160.0f, 240.0f);
    
    // 初始化
    init();
}

/************************************************************************/
#pragma mark - KNFrame类析构函数
/************************************************************************/
KNFrame::~KNFrame()
{
    // 释放所有控件
    map<int, KNBaseUI*>::iterator iter;
    for (iter = m_subUIList.begin(); iter != m_subUIList.end(); ++iter)
    {
        CCLayer* pLayer = iter->second;
        if (pLayer != NULL)
        {
            CCTouchDispatcher::sharedDispatcher()->removeDelegate(iter->second);
            
            removeChild(pLayer, true);
        }
    }
    
    // 移出通用控件
    for (int i = 0; i < MAX_COMMONLAYER_NUM; i++)
    {
        if (m_pCommonLayer[i] != NULL)
            removeChild(m_pCommonLayer[i], true);
    }
    
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
    m_subUIList.clear();
}

/************************************************************************/
#pragma mark - KNFrame类初始化函数
/************************************************************************/
bool KNFrame::init()
{
    if (CCLayer::init() == false)
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - 重写退出函数－使用自动内存使用
/************************************************************************/
void KNFrame::onExit()
{
    // 释放所有控件
    map<int, KNBaseUI*>::iterator iter;
    for (iter = m_subUIList.begin(); iter != m_subUIList.end(); ++iter)
    {
        CCLayer* pLayer = iter->second;
        if (pLayer != NULL)
        {
            CCTouchDispatcher::sharedDispatcher()->removeDelegate(iter->second);
            
            removeChild(pLayer, true);
        }
    }
    
    // 移出通用控件
    for (int i = 0; i < MAX_COMMONLAYER_NUM; i++)
    {
        if (m_pCommonLayer[i] != NULL)
            removeChild(m_pCommonLayer[i], true);
    }
    
    if (m_pBackSprite != NULL)
        removeChild(m_pBackSprite, true);
    
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
    m_subUIList.clear();
}

/************************************************************************/
#pragma mark - KNFrame类根据frame信息初始化函数
/************************************************************************/
bool KNFrame::InitFrameByInfo(FrameInfo& stInfo)
{
    // 设置id
    m_iID = stInfo.iID;
    // 设置尺寸
    setContentSize(stInfo.size);
    // 设置位置
    setPosition(stInfo.point);
    setAnchorPoint(ccp(0.0f, 0.0f));
    
    // 初始化背景文件
    if (strcmp(stInfo.szBackImage, "") != 0)
    {
        m_pBackSprite = CCSprite::spriteWithSpriteFrameName(stInfo.szBackImage);
        if (m_pBackSprite != NULL)
        {
            m_pBackSprite->setPosition(ccp(0.0f, 0.0f));
            addChild(m_pBackSprite);
        }
    }
    
    if (stInfo.bIsVisible)
        CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    return true;
}

/************************************************************************/
#pragma mark - KNFrame类根据id获得一个子控件函数
/************************************************************************/
KNBaseUI* KNFrame::GetSubUIByID(int iID) const
{
    map<int, KNBaseUI*>::const_iterator iter = m_subUIList.find(iID);
    if (iter != m_subUIList.end())
        return iter->second;
    
    return NULL;
}

/************************************************************************/
#pragma mark - KNFrame类添加一个ui控件函数
/************************************************************************/
bool KNFrame::AddOneUIToList(int iID, int iLayer, KNBaseUI* pUI)
{
    map<int, KNBaseUI*>::iterator iter = m_subUIList.find(iID);
    if (iter == m_subUIList.end())
    {
        m_subUIList.insert(make_pair(iID, pUI));
        addChild(pUI, iLayer);
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNFrame类移除一个ui控件函数
/************************************************************************/
bool KNFrame::RemoveOneUIFromList(int iID)
{
    map<int, KNBaseUI*>::iterator iter = m_subUIList.find(iID);
    if (iter != m_subUIList.end())
    {
        m_subUIList.erase(iter);
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNFrame类重写可见函数－打开可见并激活事件函数
/************************************************************************/
void KNFrame::setIsVisible(bool bIsVisible)
{
    // 打开自身可见
    CCLayer::setIsVisible(bIsVisible);
    
    if (m_iID < 90000)
        m_bIsFocusTouch = bIsVisible;
    
    if (bIsVisible)
        CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    else
        CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
    
    // 打开子控件可见和事件
    map<int, KNBaseUI*>::iterator iter;
    for (iter = m_subUIList.begin(); iter != m_subUIList.end(); iter++)
    {
        iter->second->setIsVisible(bIsVisible);
        
        if (iter->second->GetUIType() != BUTTON)
        {
            if (bIsVisible)
                CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(iter->second, 0, true);
            else
                CCTouchDispatcher::sharedDispatcher()->removeDelegate(iter->second);
        }
    }
    
    // 先加button事件
    for (iter = m_subUIList.begin(); iter != m_subUIList.end(); iter++)
    {
        iter->second->setIsVisible(bIsVisible);
        
        if (iter->second->GetUIType() == BUTTON)
        {
            if (bIsVisible)
                CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(iter->second, 0, true);
            else
                CCTouchDispatcher::sharedDispatcher()->removeDelegate(iter->second);
        }
    }
}

/************************************************************************/
#pragma mark - KNFrame类设置通用控件函数
/************************************************************************/
bool KNFrame::setCommonLayer(CCLayer* pLayer, int index, CCPoint pos, bool bIsInsert)
{
    if (index < 0 || index > MAX_COMMONLAYER_NUM)
        return false;
    
    if (m_pCommonLayer[index] != NULL)
        removeChild(m_pCommonLayer[index], true);
    
    m_pCommonLayer[index] = pLayer;
    
    if (bIsInsert)
    {
        pLayer->setPosition(pos);
        addChild(m_pCommonLayer[index]);
    }
    
    return true;
}

/************************************************************************/
#pragma mark - KNFrame类获得通用控件函数
/************************************************************************/
CCLayer* KNFrame::getCommonLayer(int index)
{
    if (index < 0 || index > MAX_COMMONLAYER_NUM)
        return NULL;
    
    return m_pCommonLayer[index];
}

/************************************************************************/
#pragma mark - KNFrame类移出通用控件函数
/************************************************************************/
bool KNFrame::removeCommonLayer(int index)
{
    if (m_pCommonLayer[index] != NULL)
    {
        m_pCommonLayer[index]->removeAllChildrenWithCleanup(true);
        removeChild(m_pCommonLayer[index], true);
        m_pCommonLayer[index] = NULL;
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNFrame类事件函数 : 点击
/************************************************************************/
bool KNFrame::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
//    if (m_iID == 1000)
//        return false;
    
    return m_bIsFocusTouch;
}