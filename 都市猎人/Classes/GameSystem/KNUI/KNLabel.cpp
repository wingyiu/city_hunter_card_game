//
//  KNLabel.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "KNLabel.h"

/************************************************************************/
#pragma mark - KNLabel类构造函数
/************************************************************************/
KNLabel::KNLabel()
{
    // 为label对象赋初值
    m_pBack = NULL;
    m_pLabel = NULL;
    m_pAtlas = NULL;
}

/************************************************************************/
#pragma mark - KNLabel类析构函数
/************************************************************************/
KNLabel::~KNLabel()
{
    // 移除label对象
    removeChild(m_pBack, true);
    removeChild(m_pLabel, true);
    removeChild(m_pAtlas, true);
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNLabel类重写初始化函数－使用自动内存使用
/************************************************************************/
bool KNLabel::init()
{
    if (CCLayer::init() == false)
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - KNLabel类重写退出函数－使用自动内存使用
/************************************************************************/
void KNLabel::onExit()
{
    // 移除label对象
    removeChild(m_pBack, true);
    removeChild(m_pLabel, true);
    removeChild(m_pAtlas, true);
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNLabel类实现基类函数－初始化函数
/************************************************************************/
bool KNLabel::init(UIInfo& stInfo)
{
    if (strcmp(stInfo.szBackImage, "") != 0)
    {
        m_pBack = CCSprite::spriteWithSpriteFrameName(stInfo.szBackImage);
        if (m_pBack != NULL)
        {
            if (stInfo.arrParm[0] != 0 && stInfo.arrParm[0] < 100.0f && stInfo.arrParm[0] != 0 && stInfo.arrParm[0] != 1 && stInfo.arrParm[0] != 2 && stInfo.arrParm[0] != 3)
                m_pBack->setScale(stInfo.arrParm[0] / 100.0f);
            
            if (stInfo.arrParm[1] != 0)
                m_pBack->setAnchorPoint(ccp(0.5f, 0.0f));
            
            m_pBack->setPosition(stInfo.point);
            addChild(m_pBack);
        }
    }
    
    if (stInfo.iType == 0)
    {
        if (/*strcmp(stInfo.szText, "") != 0 || */strcmp(stInfo.szFont, "") != 0 || stInfo.iFontSize != 0)
        {
            m_pLabel = CCLabelTTF::labelWithString(stInfo.szText, stInfo.size, CCTextAlignmentLeft, stInfo.szFont, stInfo.iFontSize);
            //m_pLabel = CCLabelTTF::labelWithString(stInfo.szText, stInfo.szFont, stInfo.iFontSize);
            if (m_pLabel != NULL)
            {
                m_pLabel->setAnchorPoint(ccp(0.0f, 0.0f));
                m_pLabel->setColor(ccc3(stInfo.iColorR, stInfo.iColorG, stInfo.iColorB));
                m_pLabel->setPosition(stInfo.point);
                addChild(m_pLabel);
            }
        }
    }
    else
    {
        m_pAtlas = CCLabelAtlas::labelWithString(stInfo.szText, stInfo.szNormalImage, stInfo.arrParm[0], stInfo.arrParm[1], '.');
        if (m_pAtlas != NULL)
        {
            m_pAtlas->setScale(stInfo.arrParm[2] / 100.0f);
            m_pAtlas->setPosition(stInfo.point);
            addChild(m_pAtlas);
        }
    }
    
    // 激活点击事件
    if (stInfo.bIsVisible)
        CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    for (int i = 0; i < MAX_PARM_NUM; i++)
        m_vecParmList.push_back(stInfo.arrParm[i]);
    
    m_pEventFunc = NULL;
    
    return true;
}

/************************************************************************/
#pragma mark - KNLabel类重置label图片
/************************************************************************/
void KNLabel::resetLabelImage(const char* imagename)
{
    // 保存原来位置
    CCPoint pos;
    CCPoint point;
    float fScale = 0.0f;
    
    // 如果有原来图片，则先移除
    if (m_pBack != NULL)
    {
        pos = m_pBack->getPosition();
        fScale = m_pBack->getScale();
        point = m_pBack->getAnchorPoint();
        removeChild(m_pBack, true);
    }
    
    m_pBack = CCSprite::spriteWithSpriteFrameName(imagename);
    if (m_pBack != NULL)
    {
        m_pBack->setScale(fScale);
        m_pBack->setPosition(pos);
        m_pBack->setAnchorPoint(point);
        addChild(m_pBack);
    }
}

/************************************************************************/
#pragma mark - KNLabel类设置label文字
/************************************************************************/
void KNLabel::setLabelText(const char* text, const char* fontname, int iFontSize)
{
    // 保存原来位置
    CCPoint pos;
    
    // 得到图片位置
    if (m_pBack != NULL)
        pos = m_pBack->getPosition();
    
    // 如果没有创建文本，创建一个
    if (m_pLabel != NULL)
    {
        m_pLabel->setString(text);
    }
    else if (m_pLabel == NULL)
    {
        m_pLabel = CCLabelTTF::labelWithString(text, fontname, iFontSize);
        if (m_pLabel != NULL)
        {
            m_pLabel->setPosition(pos);
            addChild(m_pLabel);
        }
    }
}

/************************************************************************/
#pragma mark - KNLabel类事件函数－点击
/************************************************************************/
bool KNLabel::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    if (getIsVisible() == false)
        return false;
    
    if (m_pEventFunc == NULL)
        return false;
    
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    point = convertToNodeSpace(point);
    
    // 检测按钮是否被点击
    CCPoint pos = m_pBack->getPosition();
    CCSize size = m_pBack->getContentSize();
    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
    {
        // 触发点击事件
        if (m_pEventClickFunc != NULL)
            m_pEventClickFunc(m_vecParmList);
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNLabel类事件函数－弹起
/************************************************************************/
void KNLabel::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    point = convertToNodeSpace(point);
    
    // 检测按钮是否被点击
    CCPoint pos = m_pBack->getPosition();
    CCSize size = m_pBack->getContentSize();
    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
    {
        if (m_pEventFunc != NULL)
            m_pEventFunc(m_vecParmList);
    }
}