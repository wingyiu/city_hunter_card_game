//
//  KNUIManager.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-26.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "KNUIManager.h"

KNUIManager* g_pUIManager = NULL;
KNUIManager* g_pSystemManager = NULL;

/************************************************************************/
#pragma mark - KNUIManager类构造函数
/************************************************************************/
KNUIManager::KNUIManager()
{
    // 清空ui列表
    m_uiList.clear();
    
    // 清空ui事件列表
    m_uiEventList.clear();
}

/************************************************************************/
#pragma mark - KNUIManager类析构函数
/************************************************************************/
KNUIManager::~KNUIManager()
{
    printf("KNUIManager:: 释放完成\n");
}

/************************************************************************/
#pragma mark - KNUIManager类获得Manager对象函数
/************************************************************************/
KNUIManager* KNUIManager::GetManager()
{
    return g_pUIManager;
}

/************************************************************************/
#pragma mark - KNUIManager类创建Manager函数
/************************************************************************/
KNUIManager* KNUIManager::CreateManager()
{
    g_pUIManager = KNUIManager::node();
    if (g_pUIManager != NULL)
        return g_pUIManager;
    
    return NULL;
}

/************************************************************************/
#pragma mark - KNUIManager类获得系统manager对象函数
/************************************************************************/
KNUIManager* KNUIManager::GetSystemManager()
{
    return g_pSystemManager;
}

/************************************************************************/
#pragma mark - KNUIManager类创建系统manager对象函数
/************************************************************************/
KNUIManager* KNUIManager::CreateSystemManager()
{
    g_pSystemManager = KNUIManager::node();
    if (g_pSystemManager != NULL)
        return g_pSystemManager;
    
    return NULL;
}

/************************************************************************/
#pragma mark - KNUIManager类初始化函数
/************************************************************************/
bool KNUIManager::init()
{
    // 初始化基类
    if (CCLayer::init() == false)
        return false;
    
    // 添加通用事件
    m_uiEventList.insert(make_pair("openFrame", openFrame));                                // 打开对话框
    m_uiEventList.insert(make_pair("closeFrame", closeFrame));                              // 关闭对话框
    m_uiEventList.insert(make_pair("openFrameAndCloseOthers", openFrameAndCloseOthers));    // 打开对话框关闭其他
    m_uiEventList.insert(make_pair("closeFrameAndOpenOthers", closeFrameAndOpenOthers));    // 关闭对话框打开其他
    
    // 添加login事件
    m_uiEventList.insert(make_pair("SelectPlayerJob", SelectPlayerJob));                    // 选择职业
    m_uiEventList.insert(make_pair("EnterGame", EnterGame));                                // 进入游戏场景
    
    // 添加game事件
    m_uiEventList.insert(make_pair("EnterShop", EnterShop));                                // 进入商店
    m_uiEventList.insert(make_pair("setBackMusicOnOff", setBackMusicOnOff));                // 设置背景音乐
    m_uiEventList.insert(make_pair("setBackEffectOnOff", setBackEffectOnOff));              // 设置音效开关
    m_uiEventList.insert(make_pair("setAttr", setAttr));                                    // 设置显示属性
    m_uiEventList.insert(make_pair("RecruitFollower", RecruitFollower));                    // 招募小弟
    m_uiEventList.insert(make_pair("EnterBattle", EnterBattle));                            // 进入战斗场景
    m_uiEventList.insert(make_pair("goToFollowerFrame", goToFollowerFrame));                // 进入小弟选择界面
    m_uiEventList.insert(make_pair("openTeamList", openTeamList));                          // 进入小弟队伍界面
    m_uiEventList.insert(make_pair("buildFloor", buildFloor));                              // 楼层建造
    m_uiEventList.insert(make_pair("refreshFollowerList", refreshFollowerList));            // 刷新小弟商店列表
    m_uiEventList.insert(make_pair("levelupFloor", levelupFloor));                          // 升级楼层
    m_uiEventList.insert(make_pair("closeLevelupFrame", closeLevelupFrame));                // 关闭升级界面，由于时间刷新都在楼层里，所以需要单独的关闭
    m_uiEventList.insert(make_pair("followerConfirm", followerConfirm));                    // 小弟列表确认
    m_uiEventList.insert(make_pair("followerClear", followerClear));                        // 小弟列表清空
    m_uiEventList.insert(make_pair("TrainFollower", TrainFollower));                        // 训练小弟
    m_uiEventList.insert(make_pair("LevelUpFollower", LevelUpFollower));                    // 转职小弟
    m_uiEventList.insert(make_pair("FindFriend", FindFriend));                              // 查找好友
    m_uiEventList.insert(make_pair("addFriend", addFriend));                                // 添加好友
    m_uiEventList.insert(make_pair("removeFriend", removeFriend));                          // 删除好友
    m_uiEventList.insert(make_pair("followerFire", followerFire));                          // 解雇小弟
    m_uiEventList.insert(make_pair("agreeAddFriend", agreeAddFriend));                      // 同意加为好友
    m_uiEventList.insert(make_pair("refuseAddFriend", refuseAddFriend));                    // 拒绝加为好友
    m_uiEventList.insert(make_pair("recoverAction", recoverAction));                        // 恢复行动力
    m_uiEventList.insert(make_pair("showFollowerInfo", showFollowerInfo));                  // 显示详细信息
    m_uiEventList.insert(make_pair("moveSubMapFrame", moveSubMapFrame));                    // 移动小副本选择边框
    m_uiEventList.insert(make_pair("ExtendPackage", ExtendPackage));                        // 背包扩展
    m_uiEventList.insert(make_pair("ClearPackage", ClearPackage));                          // 清理背包
    m_uiEventList.insert(make_pair("HavestFloor", HavestFloor));                            // 立即收获
    m_uiEventList.insert(make_pair("ScreenFollowerList", ScreenFollowerList));              // 筛选小弟列表
    m_uiEventList.insert(make_pair("sortfollower", sortfollower));                          // 排序小弟列表
    m_uiEventList.insert(make_pair("openFollowerList", openFollowerList));                  // 打开小弟列表
    m_uiEventList.insert(make_pair("openFollowerInfoFrame", openFollowerInfoFrame));        // 打开小弟信息界面
    m_uiEventList.insert(make_pair("openFollowerTrainFrame", openFollowerTrainFrame));      // 打开训练界面
    m_uiEventList.insert(make_pair("openFollowerJobFrame", openFollowerJobFrame));          // 打开转职界面
    m_uiEventList.insert(make_pair("openFollowerFullInfoFrame", openFollowerFullInfoFrame));// 打开查看界面
    m_uiEventList.insert(make_pair("openTrainEditedFrame", openTrainEditedFrame));          // 打开训练编辑界面
    m_uiEventList.insert(make_pair("SetFollowerAttrType", SetFollowerAttrType));            // 设置小弟显示属性
    m_uiEventList.insert(make_pair("SortFollower", SortFollower));                          // 排序小弟功能
    m_uiEventList.insert(make_pair("SignGameForDay", SignGameForDay));                      // 登陆签到
    m_uiEventList.insert(make_pair("ReSign", ReSign));                                      // 补签
    m_uiEventList.insert(make_pair("OpenBox", OpenBox));                                    // 开启宝箱
    m_uiEventList.insert(make_pair("CloseGuideFrame", CloseGuideFrame));                    // 关闭新手引导对话框
    
    // 添加battle事件
    m_uiEventList.insert(make_pair("closeMessageFrameInBattle", closeMessageFrameInBattle));// 关闭提示对话框
    
    // 添加loading事件
    m_uiEventList.insert(make_pair("closeSystemFrame", closeSystemFrame));                  // 关闭提示对话框
    
    return true;
}

/************************************************************************/
#pragma mark - KNUIManager类重写退出函数 : 使用自动内存使用
/************************************************************************/
void KNUIManager::onExit()
{
    destroy();
}

/************************************************************************/
#pragma mark - KNUIManager类释放对象函数
/************************************************************************/
void KNUIManager::destroy()
{
    // 释放所有界面
    map<int, KNFrame*>::iterator iter;
    for (iter = m_uiList.begin(); iter != m_uiList.end(); ++iter)
        removeChild(iter->second, true);
    m_uiList.clear();
    
    // 清空所有事件
    m_uiEventList.clear();
    
    //CCTouchDispatcher::sharedDispatcher()->removeAllDelegates();
}

/************************************************************************/
#pragma mark - KNUIManager类读取ui配置表函数
/************************************************************************/
bool KNUIManager::LoadUIConfigFromFile(const char* xmlFilename)
{
    //NSString * string = [[NSString alloc]initWithUTF8String:xmlFilename];
    //NSString * NSfilepath = [[NSBundle mainBundle] pathForResource:string ofType:nil inDirectory:nil];
    //TiXmlDocument uiConfig([NSfilepath UTF8String]);
    
    // 得到全路径
    const char* fullpath = CCFileUtils::fullPathFromRelativePath(xmlFilename);
    
    // 读取xml配置表
    TiXmlDocument uiConfig(fullpath);
    if (uiConfig.LoadFile() == false)
        return false;
    
    // 初始化
    init();
    
    // 获得根结点
    TiXmlElement* pNode = uiConfig.RootElement()->FirstChildElement();
    if (pNode != NULL)
    {
        // 第一层：界面
        while (pNode != NULL)
        {
            // 创建界面框
            KNFrame* pFrame = KNFrame::node();
            if (pFrame == NULL)
            {
                pNode = pNode->NextSiblingElement();
                continue;
            }
            
            // 创建frame属性
            FrameInfo stInfo;
            
            // 第一层属性
            TiXmlAttribute* pNodeAttribute = pNode->FirstAttribute();
            
            while (pNodeAttribute != NULL)
            {
                const char* szTitle = pNodeAttribute->Name();
                CCLog("szTitle is %s",szTitle);
                if (strcmp(szTitle, "id") == 0)
                {
                    stInfo.iID = atoi(pNodeAttribute->Value());
                }
                else if (strcmp(szTitle, "visible") == 0)
                {
                    if (strcmp(pNodeAttribute->Value(), "true") == 0)
                    {
                        pFrame->setIsVisible(true);
                    }
                    else if (strcmp(pNodeAttribute->Value(), "false") == 0)
                    {
                        pFrame->setIsVisible(false);
                    }
                }
                else if (strcmp(szTitle, "x") == 0)
                {
                    stInfo.point.x = atoi(pNodeAttribute->Value());
                }
                else if (strcmp(szTitle, "y") == 0)
                {
                    stInfo.point.y = atoi(pNodeAttribute->Value());
                }
                else if (strcmp(szTitle, "width") == 0)
                {
                    stInfo.size.width = atoi(pNodeAttribute->Value());
                }
                else if (strcmp(szTitle, "height") == 0)
                {
                    stInfo.size.height = atoi(pNodeAttribute->Value());
                }
                else if (strcmp(szTitle, "backimage") == 0)
                {
                    memcpy(stInfo.szBackImage, pNodeAttribute->Value(), FILENAMELENGTH);
                }
                else if (strcmp(szTitle, "layer") == 0)
                {
                    stInfo.iLayer = atoi(pNodeAttribute->Value());
                }
                
                // 下一个属性
                pNodeAttribute = pNodeAttribute->Next();
            }
            
            // 初始化对话框
            if (pFrame->InitFrameByInfo(stInfo) == false)
            {
                removeChild(pFrame, true);
                pFrame = NULL;
                continue;
            }
            
            // 第二层：界面子控件
            TiXmlElement* pSubNode = pNode->FirstChildElement();
            while (pSubNode != NULL)
            {
                // 第二层：子控件属性
                TiXmlAttribute* pSubAttr = pSubNode->FirstAttribute();
                
                // 创建ui信息
                UIInfo stInfo;
                
                // 设置ui类型
                const char* szTitle = pSubNode->Value();
                if (strcmp(szTitle, "button") == 0)
                {
                    stInfo.eType = BUTTON;
                }
                else if (strcmp(szTitle, "label") == 0)
                {
                    stInfo.eType = LABEL;
                }
                else if (strcmp(szTitle, "progress") == 0)
                {
                    stInfo.eType = PROGRESS;
                }
                else if (strcmp(szTitle, "textfield") == 0)
                {
                    stInfo.eType = TEXTFIELD;
                }
                else if (strcmp(szTitle, "slider") == 0)
                {
                    stInfo.eType = SLIDER;
                }
                else if (strcmp(szTitle, "equiplist") == 0)
                {
                    //stInfo.eType = EQUIPMENTLIST;
                }
                else if (strcmp(szTitle, "tab") == 0)
                {
                    stInfo.eType = TAB;
                }
                else if (strcmp(szTitle, "view") == 0)
                {
                    //stInfo.eType = VIEW;
                }
                else if (strcmp(szTitle, "radio") == 0)
                {
                    stInfo.eType = RADIO;
                }
                else if (strcmp(szTitle, "followerview") == 0)
                {
                    stInfo.eType = FOLLOWERLIST;
                }
                else if (strcmp(szTitle, "maplist") == 0)
                {
                    stInfo.eType = MAPLIST;
                }
                else if (strcmp(szTitle, "bonus") == 0)
                {
                    //stInfo.eType = BONUS;
                }
                else if (strcmp(szTitle, "fflist") == 0)
                {
                    stInfo.eType = FRIENDFOLLOWERLIST;
                }
                else if (strcmp(szTitle, "dialist") == 0)
                {
                    stInfo.eType = DIAMONDLIST;
                }
                else if (strcmp(szTitle, "maillist") == 0)
                {
                    stInfo.eType = MAILLIST;
                }
                else if (strcmp(szTitle, "msglist") == 0)
                {
                    stInfo.eType = MESSAGELIST;
                }
                else if (strcmp(szTitle, "calendar") == 0)
                {
                    stInfo.eType = CALENDAR;
                }
                
                // 设置ui属性
                while (pSubAttr != NULL)
                {
                    szTitle = pSubAttr->Name();
                    
                    if (strcmp(szTitle, "id") == 0)
                    {
                        stInfo.iID = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "x") == 0)
                    {
                        stInfo.point.x = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "y") == 0)
                    {
                        stInfo.point.y = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "normalimage") == 0)
                    {
                        memcpy(stInfo.szNormalImage, pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "selectimage") == 0)
                    {
                        memcpy(stInfo.szSelectImage, pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "callfunc") == 0)
                    {
                        memcpy(stInfo.szFunc, pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "callclickfunc") == 0)
                    {
                        memcpy(stInfo.szClickFunc, pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "backimage") == 0)
                    {
                        memcpy(stInfo.szBackImage, pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "font") == 0)
                    {
                        memcpy(stInfo.szFont, pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "fontsize") == 0)
                    {
                        stInfo.iFontSize = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "text") == 0)
                    {
                        memcpy(stInfo.szText, pSubAttr->Value(), LABELTEXTLENGTH);
                    }
                    else if (strcmp(szTitle, "percent") == 0)
                    {
                        stInfo.fPercentage = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "type") == 0)
                    {
                        stInfo.iType = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "length") == 0)
                    {
                        stInfo.iTextFieldLength = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "barimage") == 0)
                    {
                        memcpy(stInfo.szBarImage, pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "width") == 0)
                    {
                        stInfo.size.width = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "height") == 0)
                    {
                        stInfo.size.height = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "data") == 0)
                    {
                        stInfo.iData = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "layer") == 0)
                    {
                        stInfo.iLayer = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "layer1") == 0)
                    {
                        stInfo.arrFrame[0] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "layer2") == 0)
                    {
                        stInfo.arrFrame[1] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "layer3") == 0)
                    {
                        stInfo.arrFrame[2] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "layer4") == 0)
                    {
                        stInfo.arrFrame[3] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "normal1") == 0)
                    {
                        memcpy(stInfo.szTabNormal[0], pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "normal2") == 0)
                    {
                        memcpy(stInfo.szTabNormal[1], pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "normal3") == 0)
                    {
                        memcpy(stInfo.szTabNormal[2], pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "normal4") == 0)
                    {
                        memcpy(stInfo.szTabNormal[3], pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "select1") == 0)
                    {
                        memcpy(stInfo.szTabSelect[0], pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "select2") == 0)
                    {
                        memcpy(stInfo.szTabSelect[1], pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "select3") == 0)
                    {
                        memcpy(stInfo.szTabSelect[2], pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "select4") == 0)
                    {
                        memcpy(stInfo.szTabSelect[3], pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "music") == 0)
                    {
                        memcpy(stInfo.szMusicName, pSubAttr->Value(), FILENAMELENGTH);
                    }
                    else if (strcmp(szTitle, "x1") == 0)
                    {
                        stInfo.tabButtonX[0] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "x2") == 0)
                    {
                        stInfo.tabButtonX[1] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "x3") == 0)
                    {
                        stInfo.tabButtonX[2] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "x4") == 0)
                    {
                        stInfo.tabButtonX[3] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "y1") == 0)
                    {
                        stInfo.tabButtonY[0] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "y2") == 0)
                    {
                        stInfo.tabButtonY[1] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "y3") == 0)
                    {
                        stInfo.tabButtonY[2] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "y4") == 0)
                    {
                        stInfo.tabButtonY[3] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "parm1") == 0)
                    {
                        stInfo.arrParm[0] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "parm2") == 0)
                    {
                        stInfo.arrParm[1] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "parm3") == 0)
                    {
                        stInfo.arrParm[2] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "parm4") == 0)
                    {
                        stInfo.arrParm[3] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "frame1") == 0)
                    {
                        stInfo.arrLayer[0] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "frame2") == 0)
                    {
                        stInfo.arrLayer[1] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "frame3") == 0)
                    {
                        stInfo.arrLayer[2] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "frame4") == 0)
                    {
                        stInfo.arrLayer[3] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "frame5") == 0)
                    {
                        stInfo.arrLayer[4] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "frame6") == 0)
                    {
                        stInfo.arrLayer[5] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "frame7") == 0)
                    {
                        stInfo.arrLayer[6] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "frame8") == 0)
                    {
                        stInfo.arrLayer[7] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "frame9") == 0)
                    {
                        stInfo.arrLayer[8] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "frame10") == 0)
                    {
                        stInfo.arrLayer[9] = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "groupid") == 0)
                    {
                        stInfo.iGroupID = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "select") == 0)
                    {
                        if (strcmp(pSubAttr->Value(), "true") == 0)
                            stInfo.bIsSelect = true;
                        else if (strcmp(pSubAttr->Value(), "false") == 0)
                            stInfo.bIsSelect = false;
                    }
                    else if (strcmp(szTitle, "r") == 0)
                    {
                        stInfo.iColorR = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "g") == 0)
                    {
                        stInfo.iColorG = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "b") == 0)
                    {
                        stInfo.iColorB = atoi(pSubAttr->Value());
                    }
                    else if (strcmp(szTitle, "a") == 0)
                    {
                        stInfo.iAlpha = atoi(pSubAttr->Value());
                    }
                    
                    // 下一个属性
                    pSubAttr = pSubAttr->Next();
                }
                
                // 根据主界面可见来设置可见
                stInfo.bIsVisible = pFrame->getIsVisible();
                // 设置所属对话框id
                stInfo.iFrameID = pFrame->GetID();
                
                // 根据ui信息创建ui
                KNBaseUI* pUI = CreateUIByUIInfo(stInfo);
                
                // 添加到界面
                if (pUI != NULL)
                {
                    if (pFrame->AddOneUIToList(stInfo.iID, stInfo.iLayer, pUI) == false)
                        removeChild(pUI, true);
                }
                
                // 下一个子控件
                pSubNode = pSubNode->NextSiblingElement();
            }
            
            // 添加到列表
            if (AddOneFrameToList(pFrame->GetID(), stInfo.iLayer, pFrame) == false)
                removeChild(pFrame, true);
            
            // 下一个界面结点
            pNode = pNode->NextSiblingElement();
        }
    }
    
    return true;
}

/************************************************************************/
#pragma mark - KNUIManager类根据id获得一个frame函数
/************************************************************************/
KNFrame* KNUIManager::KNUIManager::GetFrameByID(int iID) const
{
    // 查找是否存在该ui
    map<int, KNFrame*>::const_iterator iter = m_uiList.find(iID);
    
    // 如果有，返回ui
    if (iter != m_uiList.end())
        return iter->second;
    
    return NULL;
}

/************************************************************************/
#pragma mark - KNUIManager类添加一个frame到列表函数
/************************************************************************/
bool KNUIManager::AddOneFrameToList(int iID, int iLayer, KNFrame* pFrame)
{
    // ui非法，退出
    if (pFrame == NULL)
        return false;
    
    // 查找是否存在该ui
    map<int, KNFrame*>::const_iterator iter = m_uiList.find(iID);
    
    // 如果有，则返回
    if (iter != m_uiList.end())
        return false;
    
    // 插入该ui到列表
    m_uiList.insert(make_pair(iID, pFrame));
    
    if (iID < 90000)
        addChild(pFrame, iLayer);
    
    return true;
}

/************************************************************************/
#pragma mark - KNUIManager类从ui列表中删除一个frame函数
/************************************************************************/
bool KNUIManager::RemoveOneFrameFromList(int iID)
{
    // 查找是否存在该ui
    map<int, KNFrame*>::iterator iter = m_uiList.find(iID);
    
    // 如果有，删除该ui
    if (iter != m_uiList.end())
    {
        m_uiList.erase(iter);
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - KNUIManager类移出所有引导箭头
/************************************************************************/
void KNUIManager::CloseAllGuideArrow()
{
    map<int, KNFrame*>::iterator iter;
    for (iter = m_uiList.begin(); iter != m_uiList.end(); iter++)
    {
        CCNode* pNode = iter->second->getChildByTag(8888);
        if (pNode != NULL)
            iter->second->removeChild(pNode, true);
    }
    
//    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_SELECT_FRAME_ID);
//    if (pFrame != NULL)
//    {
//        Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
//        if (pList != NULL)
//            pList->CloseGuideArrow();
//    }
    
//    pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
//    if (pFrame != NULL)
//    {
//        Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
//        if (pList != NULL)
//            pList->closeGuide();
//    }
}

/************************************************************************/
#pragma mark - KNUIManager类根据ui信息创建一个ui函数
/************************************************************************/
KNBaseUI* KNUIManager::CreateUIByUIInfo(UIInfo& stInfo)
{
    switch (stInfo.eType) {
        case BUTTON:
        {
            KNButton* pButton = KNButton::node();//new KNButton();
            if (pButton != NULL && pButton->init(stInfo) == true)
            {
                // 设置按钮事件
                map<string, pFunc>::iterator iter = m_uiEventList.find(stInfo.szFunc);
                if (iter != m_uiEventList.end())
                    pButton->SetFunc(iter->second);
                
                // 设置点击事件
                iter = m_uiEventList.find(stInfo.szClickFunc);
                if (iter != m_uiEventList.end())
                    pButton->SetClickFunc(iter->second);
                
                return pButton;
            }
            else
            {
                removeChild(pButton, true);
                pButton = NULL;
            }
        }
            break;
        case LABEL:
        {
            KNLabel* pLabel = KNLabel::node();//new KNLabel();
            if (pLabel != NULL && pLabel->init(stInfo) == true)
            {
                // 设置按钮事件
                map<string, pFunc>::iterator iter = m_uiEventList.find(stInfo.szFunc);
                if (iter != m_uiEventList.end())
                    pLabel->SetFunc(iter->second);
                
                // 设置点击事件
                iter = m_uiEventList.find(stInfo.szClickFunc);
                if (iter != m_uiEventList.end())
                    pLabel->SetClickFunc(iter->second);
                
                return pLabel;
            }
            else
            {
                removeChild(pLabel, true);
                pLabel = NULL;
            }
        }
            break;
        case PROGRESS:
        {
            KNProgress* pProgress = KNProgress::node();//new KNProgress();
            if (pProgress != NULL && pProgress->init(stInfo) == true)
            {
                return pProgress;
            }
            else
            {
                removeChild(pProgress, true);
                pProgress = NULL;
            }
        }
            break;
        case TEXTFIELD:
        {
            KNTextField* pTextField = KNTextField::node();//new KNTextField();
            if (pTextField->init(stInfo) == true)
            {
                map<string, pFunc>::iterator iter = m_uiEventList.find(stInfo.szFunc);
                if (iter != m_uiEventList.end())
                    pTextField->SetFunc(iter->second);
                
                return pTextField;
            }
            else
            {
                removeChild(pTextField, true);
                pTextField = NULL;
            }
        }
            break;
        case SLIDER:
        {
            KNSlider* pSlider = KNSlider::node();//new KNSlider();
            if (pSlider->init(stInfo) == true)
            {
                return pSlider;
            }
            else
            {
                removeChild(pSlider, true);
                pSlider = NULL;
            }
        }
            break;
        case TAB:
        {
            KNTabControl* pTabControl = KNTabControl::node();//new KNTabControl();
            if (pTabControl->init(stInfo) == true)
            {
                for (int i = 0; i < MAX_TAB_FRAMES; i++)
                {
                    if (stInfo.arrFrame[i] == 0)
                        continue;
                    
                    KNFrame* pFrame = GetFrameByID(stInfo.arrFrame[i]);
                    if (pFrame != NULL)
                        pTabControl->AddOneFrameToList(pFrame);
                }
                
                map<string, pFunc>::iterator iter = m_uiEventList.find(stInfo.szFunc);
                if (iter != m_uiEventList.end())
                    pTabControl->SetFunc(iter->second);
                
                return pTabControl;
            }
            else
            {
                removeChild(pTabControl, true);
                pTabControl = NULL;
            }
        }
            break;
        case RADIO:
        {
            KNRadio* pRadio = KNRadio::node();//new KNRadio();
            if (pRadio->init(stInfo) == true)
            {
                // 设置按钮事件
                map<string, pFunc>::iterator iter = m_uiEventList.find(stInfo.szFunc);
                if (iter != m_uiEventList.end())
                    pRadio->SetFunc(iter->second);
                
                return pRadio;
            }
            else
            {
                removeChild(pRadio, true);
                pRadio = NULL;
            }
        }
            break;
        case FOLLOWERLIST:
        {
            Layer_FollowerList* pList = Layer_FollowerList::node();
            if (pList->init(stInfo) == true)
            {
                return pList;
            }
            else
            {
                removeChild(pList, true);
                pList = NULL;
            }
        }
            break;
        case MAPLIST:
        {
            Layer_MapList* pList = Layer_MapList::node();
            if (pList != NULL && pList->init(stInfo) == true)
            {
                return pList;
            }
            else
            {
                removeChild(pList, true);
                pList = NULL;
            }
            break;
        }
        case DIAMONDLIST:
        {
            Layer_DiamondList* pList = Layer_DiamondList::node();
            if (pList != NULL && pList->init(stInfo) == true)
            {
                return pList;
            }
            else
            {
                removeChild(pList, true);
                pList = NULL;
            }
        }
            break;
        case FRIENDFOLLOWERLIST:
        {
            Layer_FriendFollowerList* pList = Layer_FriendFollowerList::node();
            if (pList != NULL && pList->init(stInfo) == true)
            {
                return pList;
            }
            else
            {
                removeChild(pList, true);
                pList = NULL;
            }
        }
            break;
        case MAILLIST:
        {
            Layer_MailList* pList = Layer_MailList::node();
            if (pList != NULL && pList->init(stInfo) == true)
            {
                return pList;
            }
            else
            {
                removeChild(pList, true);
                pList = NULL;
            }
        }
            break;
        case MESSAGELIST:
        {
            Layer_MessageList* pList = Layer_MessageList::node();
            if (pList != NULL && pList->init(stInfo) == true)
            {
                return pList;
            }
            else
            {
                removeChild(pList, true);
                pList = NULL;
            }
        }
            break;
        case CALENDAR:
        {
            Layer_Calendar* pCalendar = Layer_Calendar::node();
            if (pCalendar != NULL && pCalendar->init(stInfo) == true)
            {
                return pCalendar;
            }
            else
            {
                removeChild(pCalendar, true);
                pCalendar = NULL;
            }
        }
            break;
        case NONE:
        {
            return NULL;
        }
    }
    
    return NULL;
}