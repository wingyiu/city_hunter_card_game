//
//  KNBaseUI.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef MyCocosGame_KNBaseUI_h
#define MyCocosGame_KNBaseUI_h

#include "cocos2d.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;

// 文件名长度
#define FILENAMELENGTH      64
// label文本长度
#define LABELTEXTLENGTH     64

// tab最大支持选项卡数
const int MAX_TAB_FRAMES = 4;
// ui控件参数最大数量
const int MAX_PARM_NUM = 4;
// view控件最大页面数量
const int MAX_LAYER_NUM = 10;
// 通用图层最大数量
const int MAX_COMMONLAYER_NUM = 5;

// ui控件类型枚举
enum UIType
{
    BUTTON = 0,                                     // 按钮
    LABEL,                                          // 标签
    PROGRESS,                                       // 进度条
    TEXTFIELD,                                      // ios文本框
    SLIDER,                                         // 滚动条
    //SCROLLLIST,                                     // 滚动列表
    TAB,                                            // 分页选项卡
    //VIEW,                                           // 滚动页
    RADIO,                                          // 单选按钮
    FOLLOWERLIST,                                   // 自定义控件－小弟列表
    //EQUIPMENTLIST,                                  // 自定义控件－装备列表
    MAPLIST,                                        // 自定义控件－地图列表
    //BONUS,                                          // 自定义控件－小弟抽奖
    FRIENDFOLLOWERLIST,                             // 自定义控件－好友小弟列表
    DIAMONDLIST,                                    // 自定义控件－钻石列表
    MAILLIST,                                       // 自定义控件－邮件列表
    MESSAGELIST,                                    // 自定义控件－公告列表
    CALENDAR,                                       // 自定义控件－日历
    NONE,
};

// frame信息
struct FrameInfo
{
    int iID;                                        // frameID
    int iLayer;                                     // framelayer
    
    CCSize size;                                    // frame尺寸
    
    CCPoint point;                                  // frame位置
    
    bool bIsVisible;                                // 可见性
    
    char szBackImage[FILENAMELENGTH];               // 背景图片
    
    // 构造函数
    FrameInfo()     {memset(this, 0, sizeof(FrameInfo));}
};

// ui信息
struct UIInfo
{
    UIType eType;                                   // ui类型
    
    int iID;                                        // uiID
    int iFontSize;                                  // label-字体大小
    int iTextFieldLength;                           // textfield-长度
    int iType;                                      // slider process-类型
    int iData;                                      // list-数据类型
    int iLayer;                                     // layer图层
    
    int iFrameID;                                   // 所属对话框id
    int iGroupID;                                   // radio组id
    
    int arrFrame[MAX_TAB_FRAMES];                   // tab - 选项卡id
    int tabButtonX[MAX_TAB_FRAMES];                 // tab - 按钮坐标
    int tabButtonY[MAX_TAB_FRAMES];                 // tab - 按钮坐标
    
    int arrParm[MAX_PARM_NUM];                      // ui控件参数
    int arrLayer[MAX_LAYER_NUM];                    // view - 页面
    
    int iColorR;                                    // 颜色－红
    int iColorG;                                    // 颜色－绿
    int iColorB;                                    // 颜色－蓝
    int iAlpha;                                     // 颜色－alpha
    
    float fPercentage;                              // progress-百分比
    
    bool bIsVisible;                                // 是否可见
    bool bIsSelect;                                 // 是否选中
    
    CCSize size;                                    // ui尺寸
    
    CCPoint point;                                  // ui坐标
    
    char szNormalImage[FILENAMELENGTH];             // button-图片
    char szSelectImage[FILENAMELENGTH];             // button-选中图片
    
    char szBackImage[FILENAMELENGTH];               // label-背景图片
    char szFont[FILENAMELENGTH];                    // label-字体
    char szText[LABELTEXTLENGTH];                   // label-文本长度
    
    char szBarImage[FILENAMELENGTH];                // slider-滚动条图片
    
    char szFunc[FILENAMELENGTH];                    // 事件函数名
    char szClickFunc[FILENAMELENGTH];               // 点击事件函数名
    
    char szMusicName[FILENAMELENGTH];               // 音乐文件名
    
    char szTabNormal[MAX_TAB_FRAMES][FILENAMELENGTH];// tab按钮
    char szTabSelect[MAX_TAB_FRAMES][FILENAMELENGTH];// tab按钮
    
    // 构造函数
    UIInfo()                                        {memset(this, 0, sizeof(UIInfo));}
};

// ui基类
class KNBaseUI : public CCLayer
{
public:
    #pragma mark - 构造函数
    KNBaseUI()                                              {m_eUIType = NONE; m_vecParmList.clear(); m_pEventFunc = NULL; m_pEventClickFunc = NULL; memset(m_szMusicName, 0, FILENAMELENGTH);}
    #pragma mark - 析构函数
    virtual ~KNBaseUI() {}
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo) = 0;
    
    #pragma mark - 设置id函数
    void SetID(int iID)                                         {m_iID = iID;}
    #pragma mark - 获得id函数
    int GetID() const                                           {return m_iID;}
    
    #pragma mark - 设置ui类型函数
    void SetUIType(UIType eType)                                {m_eUIType = eType;}
    #pragma mark - 获得ui类型函数
    UIType GetUIType() const                                    {return m_eUIType;}
    
    #pragma mark - 设置参数值函数
    void setEventParm(int iValue, int iIndex)                   {m_vecParmList[iIndex] = iValue;}
    
    #pragma mark - 设置按钮事件函数
    void SetFunc(void (*pFunc)(vector<int> vecParmList))        {m_pEventFunc = pFunc;}
    #pragma mark - 设置点击事件函数
    void SetClickFunc(void (*pFunc)(vector<int> vecParmList))   {m_pEventClickFunc = pFunc;}
protected:
    // uiID
    int m_iID;
    
    // frameID
    int m_iFrameID;
    
    // ui类型
    UIType m_eUIType;
    
    // ui参数
    vector<int> m_vecParmList;
    
    // ui事件
    void (*m_pEventFunc)(vector<int> vecParmList);
    void (*m_pEventClickFunc)(vector<int> vecParmList);
    
    // ui音乐名
    char m_szMusicName[FILENAMELENGTH];
};

#endif
