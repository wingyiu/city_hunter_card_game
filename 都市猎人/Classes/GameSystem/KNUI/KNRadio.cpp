//
//  KNRadio.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-4-19.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "KNRadio.h"
#include "KNUIManager.h"

/************************************************************************/
#pragma mark - KNRadio类构造函数
/************************************************************************/
KNRadio::KNRadio()
{
    // 为radio对象变量赋初值
    m_pNormalImage = NULL;
    m_pSelectImage = NULL;
    
    m_pFrame = NULL;
    
    // 为radio属性变量赋初值
    m_bIsSelected = false;
    
    m_iFrameID = 0;
    m_iGroupID = 0;
}

/************************************************************************/
#pragma mark - KNRadio类析构函数
/************************************************************************/
KNRadio::~KNRadio()
{
    // 移除radio对象精灵
    removeChild(m_pNormalImage, true);
    removeChild(m_pSelectImage, true);
    
    // 移除事件处理
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - KNRadio类初始化函数 : 使用自动内存使用
/************************************************************************/
bool KNRadio::init()
{
    if (CCLayer::init() == false)
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - KNRadio类退出函数 : 使用自动内存使用
/************************************************************************/
void KNRadio::onExit()
{
    // 移除radio对象精灵
    removeChild(m_pNormalImage, true);
    removeChild(m_pSelectImage, true);
    
    // 移除事件处理
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - 初始化函数
/************************************************************************/
bool KNRadio::init(UIInfo& stInfo)
{
    // 初始化radio对象精灵
    m_pNormalImage = CCSprite::spriteWithSpriteFrameName(stInfo.szNormalImage);
    if (m_pNormalImage == NULL)
        return false;
    
    // 按钮纹理
    m_pNormalImage->setPosition(stInfo.point);
    addChild(m_pNormalImage);
    
    // 设置控件类型
    m_eUIType = stInfo.eType;
    
    // 设置id
    m_iID = stInfo.iID;
    
    // 设置组id
    m_iGroupID = stInfo.iGroupID;
    
    // 设置所属对话框id
    m_iFrameID = stInfo.iFrameID;
    
    // 设置选中
    m_pNormalImage->setIsVisible(stInfo.bIsSelect);
    m_bIsSelected = stInfo.bIsSelect;
    
    for (int i = 0; i < MAX_PARM_NUM; i++)
        m_vecParmList.push_back(stInfo.arrParm[i]);
    
    // 设置可见
    if (stInfo.bIsVisible)
        CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    // 预先读取音效
    if (strcmp(stInfo.szMusicName, "") != 0)
    {
        memcpy(m_szMusicName, stInfo.szMusicName, FILENAMELENGTH);
        CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect(m_szMusicName);
    }
    
    return true;
}

/************************************************************************/
#pragma mark - KNRadio类设置选中标记函数
/************************************************************************/
void KNRadio::SetIsSelectedMark(bool bIsSelected)
{
    m_bIsSelected = bIsSelected;
    
    m_pNormalImage->setIsVisible(m_bIsSelected);
}

/************************************************************************/
#pragma mark - KNRadio类事件函数 : 点击
/************************************************************************/
bool KNRadio::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击鼠标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    point = convertToNodeSpace(point);
    
    // 获得位置和尺寸
    CCPoint pos = m_pNormalImage->getPosition();
    CCSize size = m_pNormalImage->getContentSize();
    
    // 检测是否被点击
    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
    {
        // 播放音效
        if (strcmp(m_szMusicName, "") != 0)
            KNUIFunction::GetSingle()->PlayerEffect(m_szMusicName);
        
        if (m_bIsSelected)
            return true;
        
        m_bIsSelected = true;
        m_pNormalImage->setIsVisible(m_bIsSelected);
        
        // 关闭其他radio : 这个方法太伤效率，以后有时间要好好想想
        m_pFrame = KNUIManager::GetManager()->GetFrameByID(m_iFrameID);
        if (m_pFrame != NULL)
        {
            // 获得子控件列表
            map<int, KNBaseUI*> uiList = m_pFrame->GetUIList();
            map<int, KNBaseUI*>::iterator iter;
            for (iter = uiList.begin(); iter != uiList.end(); iter++)
            {
                if (iter->second->GetUIType() == RADIO)
                {
                    if (iter->second->GetID() == m_iID)
                        continue;
                    
                    KNRadio* pRadio = dynamic_cast<KNRadio*>(iter->second);
                    if (pRadio != NULL && pRadio->GetGroupID() == m_iGroupID)
                        pRadio->SetIsSelectedMark(false);
                }
            }
        }
        
        // 运行事件
        if (m_pEventFunc != NULL)
            m_pEventFunc(m_vecParmList);
        
        return true;
    }
    
    return false;
}