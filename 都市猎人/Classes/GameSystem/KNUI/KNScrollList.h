//
//  KNScrollList.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-4-5.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef KNScrollList_h
#define KNScrollList_h

#include "KNScrollView.h"
#include "KNUIFunction.h"

// 文本长度限制
const int INFOLENGTH = 32;

// 选项信息
struct ITEMINFO
{
    CCSprite* pIcon;                    // icon图标
    
    CCSprite* pNormal1;                 // 按钮1
    CCSprite* pSelect1;                 // 按钮1
    
    CCSprite* pNormal2;                 // 按钮2
    CCSprite* pSelect2;                 // 按钮2
    
    CCLabelTTF* pInfo1;                 // 文本1
    CCLabelTTF* pInfo2;                 // 文本2
    
    // 构造函数
    ITEMINFO()                          {memset(this, 0, sizeof(ITEMINFO));}
};

class KNScrollList : public KNScrollView
{
public:
    #pragma mark - 构造函数
    KNScrollList();
    #pragma mark - 析构函数
    virtual ~KNScrollList();
    
    #pragma mark - 重写初始化函数－使用自动内存使用
    bool init();
    #pragma mark - 重写退出函数－使用自动内存使用
    virtual void onExit();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 获得当前选中的选项信息函数
    ITEMINFO GetSelectedItemInfo() const;
    
    #pragma mark - 事件函数－点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－滑动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNScrollList);
private:
    #pragma mark - 刷新装备列表函数
    void RefreshEquipmentItem(int iDataType);
private:
    // 选项列表
    vector<CCLayer*> m_ItemList;
    
    // 选中选项标记
    int m_iSelectedIndex;
    int m_iListType;
    
    int index;
};

#endif
