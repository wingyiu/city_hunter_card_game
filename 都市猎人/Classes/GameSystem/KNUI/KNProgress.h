//
//  KNProgress.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-27.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef MyCocosGame_KNProgress_h
#define MyCocosGame_KNProgress_h

#include "KNBaseUI.h"

class KNProgress : public KNBaseUI
{
public:
    #pragma mark - 构造函数
    KNProgress();
    #pragma mark - 析构函数
    virtual ~KNProgress();
    
    #pragma mark - 重写初始化函数－使用自动内存使用
    bool init();
    #pragma mark - 重写退出函数－使用自动内存使用
    void onExit();
    
    #pragma mark - 实现基类函数－初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 设置进度百分比函数
    void SetPercent(float fPercent)                                 {if (fPercent <= 100.0f) m_fPercent = fPercent; else m_fPercent = 100.0f; }
    #pragma mark - 获得进度百分比函数
    float GetPercent() const                                        {return m_fPercent;}
    
    #pragma mark - 设置可视范围函数
    void SetVisibleRect(CCRect rect);
    
    #pragma mark - 获得进度条精灵
    CCSprite* GetProgressSprite() const                             {return m_pProgressSprite;}
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(KNProgress);
protected:
    #pragma mark - 事件函数－点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
private:
    #pragma mark - 重新函数 : 访问函数
    virtual void visit();
private:
    // progress对象变量
    CCSprite* m_pProgressSprite;
    
    // progress属性变量
    CCRect m_stVisibleRange;
    
    CCPoint m_Position;
    
    float m_fPercent;
};

#endif
