//
//  PlayerDataManage.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-4-10.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "PlayerDataManage.h"
#include "Follower.h"
#include "Equipment.h"
#include "Props.h"
#include "Floor.h"
#include "Enemy.h"
#include "KNUIFunction.h"
USING_NS_CC;

PlayerDataManage * PlayerDataManage::m_Instance = NULL;

int PlayerDataManage::m_PlayerActivity = 0;
int PlayerDataManage::m_PlayerMaxActivity = 0;
int PlayerDataManage::m_PlayerMoney = 0;
int PlayerDataManage::m_PlayerRMB = 0;
int PlayerDataManage::m_PlayerExperience = 0;
int PlayerDataManage::m_PlayerLevelUpExperience = 0;
int PlayerDataManage::m_PlayerLevel = 1;
int PlayerDataManage::m_PlayerFriendValue = 0;
int PlayerDataManage::m_MissionCompleteNum = 0;
int PlayerDataManage::m_PlayerSex = 0;
int PlayerDataManage::m_PlayerFistGameProfession = 0;
bool PlayerDataManage::m_GuideMark = true;
int PlayerDataManage::m_GuideStepMark = 0;
bool PlayerDataManage::m_GuideBattleMark = true;
int PlayerDataManage::m_iPlayerPackageNum = 1;

/************************************************************************/
#pragma mark 初始化
/************************************************************************/
void PlayerDataManage::Initialize()
{
    memset(m_Player91ID, 0, sizeof(m_Player91ID));
    memset(m_PlayerUserID, 0, sizeof(m_PlayerUserID));
    memset(m_PlayerName, 0, sizeof(m_PlayerName));
    memset(m_PlayerPassword, 0, sizeof(m_PlayerPassword));
    
    m_PlayerActivityComeBackTime = ReActivityTime;
    
    m_bIsMusicOn = true;
    m_bIsEffectOn = true;
}

PlayerDataManage * PlayerDataManage::ShareInstance()
{
    if(m_Instance == NULL)
    {
        m_Instance = new PlayerDataManage();
        m_Instance -> Initialize();
    }
    return m_Instance;
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark  留存的数据撤销所绑定的图片资源
/************************************************************************/
void PlayerDataManage::RemoveDataImage()
{
    for(PlayerFollowerDepot::iterator it1 = m_MyFollowerDepot.begin(); it1 != m_MyFollowerDepot.end(); ++ it1)
    {
        for(list<Follower*>::iterator it2 = it1 -> second.begin(); it2 != it1 -> second.end(); ++ it2)
        {
            (*it2) -> RemoveImage();
        }
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 留存的数据重新绑定图片资源
/************************************************************************/
void PlayerDataManage::ReSetDataImage()
{
    for(PlayerFollowerDepot::iterator it1 = m_MyFollowerDepot.begin(); it1 != m_MyFollowerDepot.end(); ++ it1)
    {
        for(list<Follower*>::iterator it2 = it1 -> second.begin(); it2 != it1 -> second.end(); ++ it2)
        {
            (*it2) -> SetImage();
        }
    }
}   
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 关于邮件
#pragma mark -
#pragma mark -
#pragma mark -
#pragma mark 读取本地的邮件列表
/************************************************************************/
void PlayerDataManage::ReadMyMailList()
{
    NSMutableArray * ReadDataArray = nil;  
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);  
    NSString * documentsDirectory = [paths objectAtIndex:0];  
    if (!documentsDirectory) return;
    
    NSString * FolderName = [NSString stringWithUTF8String:PlayerDataManage::ShareInstance() -> m_Player91ID];
    NSString * FolderPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:FolderName];
    NSString * appFile = [FolderPath stringByAppendingPathComponent:@"MailInfo"];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:appFile])
    {  
        ReadDataArray = [NSMutableArray arrayWithContentsOfFile:appFile];
    }
    
    PlayerDataManage::ShareInstance() -> ClearMailList();
    
    for(int i = 0; i < [ReadDataArray count]; i ++)
    {
        NSArray * MailInfoArray = [ReadDataArray objectAtIndex:i];
        
        NSString * pID = [MailInfoArray objectAtIndex:0];
        int SenderID = [pID intValue];
        
        NSString * pName = [MailInfoArray objectAtIndex:1];
        const char * SenderName = [pName UTF8String];
        
        NSString * pLevel = [MailInfoArray objectAtIndex:2];
        int SenderLevel = [pLevel intValue];
        
        NSString * pTime = [MailInfoArray objectAtIndex:3];
        int SendTime = [pTime intValue];
        
        NSString * pFollowerID = [MailInfoArray objectAtIndex:4];
        int SenderFollowerID = [pFollowerID intValue];
        
        NSString * pFollowerLevel = [MailInfoArray objectAtIndex:5];
        int SenderFollowerLevel = [pFollowerLevel intValue];
        
        NSString * pType = [MailInfoArray objectAtIndex:6];
        int SendType = [pType intValue];
        
        NSString * pState = [MailInfoArray objectAtIndex:7];
        int MailState = [pState intValue];
        
        MailInfo * MInfo = new MailInfo();
        MInfo -> Mail_Index = i;
        MInfo -> Mail_Type = SendType;        
        MInfo -> Mail_State = MailState;
        MInfo -> Mail_SenderID = SenderID;        
        //MInfo -> Mail_SenderName = SenderName;
        memcpy(MInfo -> Mail_SenderName, SenderName, 32);
        MInfo -> Mail_SenderLevel = SenderLevel;
        MInfo -> Mail_SendTime = SendTime;
        MInfo -> Mail_SendMessage = NULL;  
        MInfo -> Mail_Sender_FollowerID = SenderFollowerID;
        MInfo -> Mail_Sender_FollowerLevel = SenderFollowerLevel;   
        
        m_MyMailList.push_back(MInfo);
    }
}

void PlayerDataManage::UpdateMyMailListToFile()
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);  
    NSString * documentsDirectory = [paths objectAtIndex:0];  
    if (!documentsDirectory) return;
    
    NSString * FolderName = [NSString stringWithUTF8String:PlayerDataManage::ShareInstance() -> m_Player91ID];
    NSString * FolderPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:FolderName];
    NSString * appFile = [FolderPath stringByAppendingPathComponent:@"MailInfo"];
    
    // 如果没有邮件，删除本地文件
    if (m_MyMailList.size() == 0)
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:appFile] == YES)
        {
            NSError* error = nil;
            if ([[NSFileManager defaultManager] removeItemAtPath:appFile error:&error] != YES)
                NSLog(@"%@", [error localizedDescription]);
            
            return;
        }
    }
    
    NSMutableArray * WriteDataArray = [[NSMutableArray alloc] init];
    
    list<MailInfo*>::iterator iter;
    for (iter = m_MyMailList.begin(); iter != m_MyMailList.end(); iter++)
    {
        //0
        NSString * SenderID = [NSString stringWithFormat:@"%d", (*iter)->Mail_SenderID];
        
        //1
        NSString * SenderName = [NSString stringWithFormat:@"%s", (*iter)->Mail_SenderName];
        
        //2
        NSString * SenderLevel = [NSString stringWithFormat:@"%d", (*iter)->Mail_SenderLevel];
        
        //3
        NSString * SendTime = [NSString stringWithFormat:@"%d", (*iter)->Mail_SendTime];
        
        //4
        NSString * SenderFollowerID = [NSString stringWithFormat:@"%d", (*iter)->Mail_Sender_FollowerID];
        
        //5
        NSString * SenderFollowerLevel = [NSString stringWithFormat:@"%d", (*iter)->Mail_Sender_FollowerLevel];
        
        //6
        NSString * SendType = [NSString stringWithFormat:@"%d", (*iter)->Mail_Type];
        
        //7
        NSString * MailState = [NSString stringWithFormat:@"%d", (*iter)->Mail_State];
        
        NSArray * ApplyInfo = [NSArray arrayWithObjects:SenderID, SenderName, SenderLevel, SendTime, SenderFollowerID, SenderFollowerLevel, SendType, MailState, nil];
        [WriteDataArray addObject:ApplyInfo];
    }
    
    [WriteDataArray writeToFile:appFile atomically:NO];
    [WriteDataArray release];
}

void PlayerDataManage::RemoveMailFromList(int iIndex)
{
    list <MailInfo*>::iterator iter;
    for (iter = m_MyMailList.begin(); iter != m_MyMailList.end(); iter++)
    {
        if ((*iter)->Mail_Index == iIndex)
        {
            m_MyMailList.erase(iter);
            return;
        }
    }
    
    printf("没有找到相应的id PlayerDataManage::RemoveMailFromList = %d\n", iIndex);
}

void PlayerDataManage::SetMailStateToOld(int iIndex)
{
    list <MailInfo*>::iterator iter;
    for (iter = m_MyMailList.begin(); iter != m_MyMailList.end(); iter++)
    {
        if ((*iter)->Mail_Index == iIndex)
        {
            (*iter)->Mail_State = MailState_Old;
            return;
        }
    }
}

void PlayerDataManage::ClearMailList()
{
    for(list <MailInfo*>::iterator it = m_MyMailList.begin(); it != m_MyMailList.end(); )
    {
        SAFE_DELETE(*it);
        m_MyMailList.erase(it ++);
    }
    m_MyMailList.clear();
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 关于小弟
#pragma mark -
#pragma mark -
#pragma mark -
#pragma mark 玩家编辑的小弟队伍
/************************************************************************/
void PlayerDataManage::SetFollowerTeam(vector <Follower*> * TeamList)
{
    vector <Follower*> pTeamList = *TeamList;
    
    //重新编辑小弟的队伍
    for(int i = 1; i < TeamMaxNum + 1; ++ i)
    {   
        //如果这个位置为空
        if(! pTeamList[i - 1])
        {
            PlayerFollowerTeam::iterator it = m_MyFollowerTeam.find(i);
            if(it != m_MyFollowerTeam.end())
            {
                //移出该位置的小弟
                it -> second -> SetTeamState(0);
                m_MyFollowerTeam.erase(it);
            }
        }else 
        {
            PlayerFollowerTeam::iterator it = m_MyFollowerTeam.find(i);
            if(it != m_MyFollowerTeam.end())
            {
                //如果该位置的小弟和现有队伍中该位置的小弟的不一样
                if(pTeamList[i - 1] != it -> second)
                {
                    //移出该位置的小弟
                    it -> second -> SetTeamState(0);
                    m_MyFollowerTeam.erase(it);
                    
                    //该位置加入新的小弟
                    Follower * newTeamMember = FindFollowerInDepot(pTeamList[i - 1]);
                    if (newTeamMember == NULL)
                    {
                        KNUIFunction::GetSingle()->OpenSystemMessageBoxFrame("队伍中没有找到小弟");
                        return;
                    }
                    
                    newTeamMember -> SetTeamState(i);
                    m_MyFollowerTeam[i] = newTeamMember;
                }
            }else 
            {
                //该位置加入新的小弟
                Follower * newTeamMember = FindFollowerInDepot(pTeamList[i - 1]);
                if (newTeamMember == NULL)
                {
                    KNUIFunction::GetSingle()->OpenSystemMessageBoxFrame("队伍中没有找到小弟");
                    return;
                }
                
                newTeamMember -> SetTeamState(i);
                m_MyFollowerTeam[i] = newTeamMember;
            }
        }
    }
    
    //将编辑好的小弟的队伍发送给服务器
    ServerDataManage::ShareInstance() -> RequestReferMyFollowerTeam();
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 清空小弟的队伍
/************************************************************************/
void PlayerDataManage::ClearFollowerTeam()
{
    m_MyFollowerTeam.clear();
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向小弟队伍中加入和移除 好友的小弟
/************************************************************************/
void PlayerDataManage::AddFriendFollowerToTeam(Follower * follower)
{
    CCAssert(follower != NULL, "PlayerDataManage::AddFriendFollower()");
    
    PlayerFollowerTeam::iterator it = m_MyFollowerTeam.find(TeamMaxNum + 1);
    if(it != m_MyFollowerTeam.end())
    {
        it -> second -> release();
        m_MyFollowerTeam.erase(it);
    }
    m_MyFollowerTeam[TeamMaxNum + 1] = follower;
}

void PlayerDataManage::RemoveFriendFollowerFromTeam()
{
    PlayerFollowerTeam::iterator it = m_MyFollowerTeam.find(TeamMaxNum + 1);
    if(it != m_MyFollowerTeam.end())
    {
        it -> second -> release();
        m_MyFollowerTeam.erase(it);
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向小弟仓库中加入小弟
/************************************************************************/
void PlayerDataManage::AddFollowerToDepot(Follower * follower, int teamIndex)
{
    //设置队伍状态
    follower -> SetTeamState(teamIndex);
    //设置是否到最大等级了
    if(follower -> GetData() -> Follower_Level >= follower -> GetData() -> Follower_MaxLevel)
    {
        follower -> SetIsMaxLevel(true);
    }
    //设置是否进化到最高阶了
    if(follower -> GetData() -> Follower_EvolvementID == 0)
    {
        follower -> SetIsMaxEvolvement(true);
    }
    
    //将小弟加入到玩家的小弟仓库中
    m_MyFollowerDepot[follower -> GetData() -> Follower_ID].push_back(follower);
    
    //如果小弟同时也在队伍当中
    if(teamIndex != 0)
    {
        m_MyFollowerTeam[teamIndex] = follower;
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 在小弟仓库中找到并返回出小弟
/************************************************************************/
Follower * PlayerDataManage::FindFollowerInDepot(Follower * follower)
{
    PlayerFollowerDepot::iterator it1 = m_MyFollowerDepot.find(follower -> GetData() -> Follower_ID);
    if(it1 != m_MyFollowerDepot.end())
    {
        for(list <Follower *>::iterator it2 = it1 -> second.begin(); it2 != it1 -> second.end(); ++ it2)
        {
            if(*it2 == follower)
            {
                return *it2;
            }
        }
    }
    return NULL;
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 从小弟仓库中移除一个小弟
/************************************************************************/
void PlayerDataManage::RemoveFollowerFromDepot(Follower * follower)
{
    PlayerFollowerDepot::iterator it1 = m_MyFollowerDepot.find(follower -> GetData() -> Follower_ID);
    if(it1 != m_MyFollowerDepot.end())
    {
        for(list <Follower *>::iterator it2 = it1 -> second.begin(); it2 != it1 -> second.end(); )
        {
            if(*it2 == follower)
            {
                (*it2) -> release();
                it1 -> second.erase(it2 ++);
            }
            else   ++ it2;
        }
    }
}


/************************************************************************/
#pragma mark 清除小弟仓库
/************************************************************************/
void PlayerDataManage::ClearFollowerDepot()
{
    for(PlayerFollowerDepot::iterator it1 = m_MyFollowerDepot.begin(); it1 != m_MyFollowerDepot.end(); it1 ++)
    {
        for(list<Follower*>::iterator it2 = it1 -> second.begin(); it2 != it1 -> second.end(); )
        {
            (*it2) -> release();
            it1 -> second.erase(it2 ++);
        }
        it1 -> second.clear();
    }
    m_MyFollowerDepot.clear();
    m_MyFollowerTeam.clear();
}

/************************************************************************/
#pragma mark 更新小弟所在的列表 : 因为小弟转职后Follower_ID会发生变化，导致有的列表找不到
/************************************************************************/
void PlayerDataManage::UpdateFollowerInList(int iOldID, int iNewID, Follower* pFollower)
{
    // id没有变化，则退出
    if (iOldID == iNewID)
        return;
    
    // 移除原id小弟
    PlayerFollowerDepot::iterator it1 = m_MyFollowerDepot.find(iOldID);
    if(it1 != m_MyFollowerDepot.end())
    {
        for(list <Follower *>::iterator it2 = it1 -> second.begin(); it2 != it1 -> second.end(); )
        {
            if(*it2 == pFollower)
                it1 -> second.erase(it2 ++);
            else
                ++it2;
        }
    }
    
    // 添加新id小弟
    m_MyFollowerDepot[iNewID].push_back(pFollower);
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 关于楼层
#pragma mark -
#pragma mark -
#pragma mark -
#pragma mark 初始化建造玩家的基础楼层
/************************************************************************/
void PlayerDataManage::InitBaseFloor()
{
    //PlayerDataManage::m_GuideMark = true;
    //PlayerDataManage::m_GuideStepMark = GUIDE_FIRST_FLOOR;
    
    CreateNewFloor(F_Player, 1);
    CreateNewFloor(F_Hint, 1);
    
    // 新手引导
    if (!PlayerDataManage::m_GuideMark)
    {
        CreateNewFloor(F_Follower, 1);
        CreateNewFloor(F_Mission, 1);
        CreateNewFloor(F_Shop, 1);
        CreateNewFloor(F_Platform, 1);
    }
    else
    {
        if (PlayerDataManage::m_GuideStepMark > GUIDE_FRIEND)
        {
            CreateNewFloor(F_Follower, 1);
            CreateNewFloor(F_Mission, 1);
            CreateNewFloor(F_Shop, 1);
            CreateNewFloor(F_Platform, 1);
        }
        else if (PlayerDataManage::m_GuideStepMark > GUIDE_SHOP)
        {
            CreateNewFloor(F_Follower, 1);
            CreateNewFloor(F_Mission, 1);
            CreateNewFloor(F_Shop, 1);
        }
        else if (PlayerDataManage::m_GuideStepMark >= GUIDE_MISSION)
        {
            CreateNewFloor(F_Follower, 1);
            CreateNewFloor(F_Mission, 1);
        }
        else if (PlayerDataManage::m_GuideStepMark >= GUIDE_TEAMEDIT)
        {
            CreateNewFloor(F_Follower, 1);
        }
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 创建新的楼层
/************************************************************************/
void PlayerDataManage::CreateNewFloor(int type, int level, int time)
{
    //检查是否已经建造加入过了
    PlayerFloorDepot::iterator it = m_MyFloorDepot.find(type);
    if(it != m_MyFloorDepot.end())       return;
    
    //创建楼层数据
    const Floor_Data * SystemData = SystemDataManage::ShareInstance() -> GetData_ForFloor(type, level);
    Floor_Data * MyData = new Floor_Data(); 
    memcpy(MyData, SystemData, sizeof(Floor_Data));
    MyData -> Floor_HarvestTime = time;
    
    //所有的楼层都存在这里
    m_MyFloorDepot[type] = MyData;
    
    //玩家自建的楼层再存一遍，用于收获时间单独计算
    if(type != F_Player &&
       type != F_Follower &&
       type != F_Mission &&
       type != F_Shop &&
       type != F_Platform &&
       type != F_Hint)
    {
        m_MySelfFloorDepot[type] = MyData;
    }
    
    //如果玩家已经有了自建的楼层，就开始倒计时收获的时间
    if(! m_MySelfFloorDepot.empty())
    {
        CCDirector::sharedDirector() -> setCountDown(true);
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 重新刷新楼层数据
/************************************************************************/
bool PlayerDataManage::UpdataFloorData(int type, int level, int time)
{
    //检查是否已经建造加入过了
    PlayerFloorDepot::iterator it = m_MyFloorDepot.find(type);
    if(it == m_MyFloorDepot.end())
        return false;
    
    it -> second -> Floor_Level = level;
    it -> second -> Floor_HarvestTime = time;
    return true;
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 升级楼层
/************************************************************************/
void PlayerDataManage::LevelUpFloor(int type, int level)
{
    PlayerFloorDepot::iterator it1 = m_MyFloorDepot.find(type);
    PlayerFloorDepot::iterator it2 = m_MySelfFloorDepot.find(type);
    
    if(it1 != m_MyFloorDepot.end() && it2 != m_MySelfFloorDepot.end())       
    {
        if(level > it1 -> second -> Floor_Level && level > it2 -> second -> Floor_Level)
        {
            SAFE_DELETE(it1 -> second);
            m_MyFloorDepot.erase(it1);
            m_MySelfFloorDepot.erase(it2);            
            
            const Floor_Data * SystemData = SystemDataManage::ShareInstance() -> GetData_ForFloor(type, level);
            Floor_Data * MyData = new Floor_Data(); 
            memcpy(MyData, SystemData, sizeof(Floor_Data));
            
            m_MyFloorDepot[type] = MyData;
            m_MySelfFloorDepot[type] = MyData;
        }
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 倒计时楼层的收获时间
/************************************************************************/
void PlayerDataManage::CountDownFloorHarvestTime()
{
    for(PlayerFloorDepot::iterator it = m_MySelfFloorDepot.begin(); it != m_MySelfFloorDepot.end(); ++ it)
    {
        if(it -> second -> Floor_HarvestTime > 0)
            it -> second -> Floor_HarvestTime -= 1;
        else 
            it -> second -> Floor_HarvestTime = 0;
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 是否存在该楼层
/************************************************************************/
bool PlayerDataManage::IsFloorExist(int type)
{
    PlayerFloorDepot::iterator it = m_MyFloorDepot.find(type);
    if(it != m_MyFloorDepot.end())
        return true;
    
    return false;
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 获得该楼层
/************************************************************************/
Floor_Data * PlayerDataManage::GetMyFloor(int type)
{
    PlayerFloorDepot::iterator it = m_MyFloorDepot.find(type);
    if(it != m_MyFloorDepot.end())
        return it -> second;
    else 
        return NULL;
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 关于行动力
#pragma mark -
#pragma mark -
#pragma mark -
#pragma mark 倒计时行动力时间
/************************************************************************/
void PlayerDataManage::CountDownActivityTime()
{
    m_PlayerActivityComeBackTime -= 1;
    
    if(m_PlayerActivityComeBackTime <= 0)
    {
        m_PlayerActivityComeBackTime = 300;
        m_PlayerActivity += 1;
        
        //如果行动力满了
        if(m_PlayerActivity >= m_PlayerMaxActivity)
        {
            m_PlayerActivityComeBackTime = 0;
            m_PlayerActivity = m_PlayerMaxActivity;
            CCDirector::sharedDirector() -> setActionComeBack(false);
        }
    }
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/







