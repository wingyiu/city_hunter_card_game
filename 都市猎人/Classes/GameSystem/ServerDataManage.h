//
//  ServerDataManage.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-25.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_ServerDataManage_h
#define MidNightCity_ServerDataManage_h

#include "cocos2d.h"
#include "Enemy_Configure.h"
class Follower;


//战斗中出现的怪物的数量
#define  EnemyNum 5

//好友小弟数量
#define  FriendFollowerNum 100

enum SubMap_State
{
    SubMapState_New = 0,
    SubMapState_Lock,
    SubMapState_UnLock
};

//小弟商品的列表
typedef map <int, map <int, Follower *> >  WareList;


//好友的信息结构
struct FriendInfo
{
    FriendInfo() { memset(this, 0, sizeof(FriendInfo)); }
    
    char    Fri_UserName[CHARLENGHT];
    int     Fri_UserID;
    int     Fri_UserLevel;
    int     Fri_CanBeUse;
    
    int     Fri_FollowerID;
    int     Fri_FollowerCharacterID;
    int     Fri_FollowerAttack;
    int     Fri_FollowerDefense;
    int     Fri_FollowerHp;
    int     Fri_FollowerReply;
    int     Fri_FollowerLevel;
    int     Fri_FollowerSkillLevel;
    int     Fri_FollowerFriendValue;
    int     Fri_FollowerQuality;
    int     Fri_FollowerMaxQuality;
};


//副本战斗中需要的信息的结构
struct BattleInfo
{
    BattleInfo() 
    {
        memset(this, 0, sizeof(BattleInfo)); 
    }
    
    ~BattleInfo()
    {
        for(map <int, vector <Enemy_Data *> >::iterator it1 = EnemyInfo_List.begin(); it1 != EnemyInfo_List.end(); ++ it1)
        {
            for(vector <Enemy_Data *>::iterator it2 = it1 -> second.begin(); it2 != it1 -> second.end(); it2 ++)
            {
                SAFE_DELETE(*it2);
            }
            it1 -> second.clear();
        }
        EnemyInfo_List.clear();
    }
    
    //////////////////////////////////////////////
    
    int     MapIndex;
    int     SubMapIndex;
    int     StepNum;
    
    map <int, vector <Enemy_Data *> >  EnemyInfo_List;
    
    int     ClearLevelDropMoney;
    int     ClearLevelDropExperience;
};

struct BattleAfficheInfo
{
    BattleAfficheInfo() 
    {
        memset(this, 0, sizeof(BattleAfficheInfo)); 
    }
    
    //大副本的ID
    int     MapIndex;   
    //小副本的ID
    int     SubMapIndex;
    
    //大副本使用的图片的ID
    int     MapImageID;
    //大副本使用的名称的ID
    int     MapNameID;
    
    //小副本使用的图片的ID
    int     SubMapImageID;
    
    //小副本的战斗步骤数
    int     SubMapBattleStep;
    //小副本消耗的行动力
    int     SubMapActivity;
    //小副本当前状态
    int     SubMapState;
    //小副本的开启时间
    int     SubMapOpenTime;
    //小副本的持续时间
    int     SubMapKeepTime;
    
    //副本图片文件名
    char    MapImageFilename[CHARLENGHT];
    //小副本图片文件名
    char    SubMapImageFilename[CHARLENGHT];
    //副本名称
    char    MapName[CHARLENGHT];
};
//副本的公告信息列表
typedef map <int, BattleAfficheInfo *>  BattleAfficheInfoList;

// 签到信息
struct SIGN_INFO
{
    time_t iTime;                               // 服务器时间戳
    int iMoney;                                 // 领取金币
    int iCount;                                 // 累计天数
    
    vector<time_t> m_vecSignTime;               // 签到天 : 日期
    int m_arrAward[6];                          // 宝箱开启数
    
    // 构造函数
    SIGN_INFO()                                 {memset(this, 0, sizeof(SIGN_INFO));}
};

class ServerDataManage 
{
public:
    static ServerDataManage * ShareInstance();
   
    
/*********************************************************/
#pragma mark 关于充值
/*********************************************************/
public:
    void            RequestPayID();
    void            RequestPayIDSuccess(const char * PayID);
    
/*********************************************************/
#pragma mark-
/*********************************************************/
    
    
/*********************************************************/
#pragma mark 关于登陆的交互处理
/*********************************************************/
public:
    void            RequestPlayer91Login();
    void            RequestPlayerInfo();
    void            RequestEnterGame();
    
    inline void     ClearBattleAfficheInfo()
    {
        for(BattleAfficheInfoList::iterator it = m_BattleAfficheInfoList.begin(); it != m_BattleAfficheInfoList.end(); )
        {
            SAFE_DELETE(it -> second);
            m_BattleAfficheInfoList.erase(it ++);
        }
        m_BattleAfficheInfoList.clear();
    }
    
    SS_GET(BattleAfficheInfoList*, &m_BattleAfficheInfoList, BattleAfficheInfoList);
    
private:
    //登陆时拿下副本公告信息
    BattleAfficheInfoList   m_BattleAfficheInfoList;
/*********************************************************/
#pragma mark-
/*********************************************************/
    
    
/*********************************************************/
#pragma mark 引导发送
/*********************************************************/
public:
    void            RequestGuideDoneReqest(int step);
/*********************************************************/
#pragma mark-
/*********************************************************/
    
    
/*********************************************************/
#pragma mark 关于好友请求的交互处理
/*********************************************************/
public:
    //获取好友请求
    void            RequestApplyList();
    
/*********************************************************/
#pragma mark －
/*********************************************************/
    
    
/*********************************************************/
#pragma mark 关于楼层的交互处理
/*********************************************************/
public:  
    //请求建造新的楼层
    void            RequestNewFloor(int floor_type);
    //请求建造新的楼层成功
    void            RequestNewFloorSuccess(int floor_type);
    
    //请求升级楼层
    void            RequestLevelUpFloor(int floor_type);
    //请求升级楼层成功
    void            RequestLevelUpFloorSuccess(int floor_type, int floor_level, int floor_harvestTime, int floor_harvestMoney);
    
    //请求楼层的收获
    void            RequestFloorHarvest(int floor_type, int iFastHavest = 0);
    //请求楼层的收获成功
    void            RequestFloorHarvestSuccess(int floor_type, int floor_harvestMoney);
/*********************************************************/
#pragma mark-
/*********************************************************/
    
    
    
    
/*********************************************************/
#pragma mark 关于提交小弟队伍的交互
/*********************************************************/
public:
    void            RequestReferMyFollowerTeam();
/*********************************************************/
#pragma mark-
/*********************************************************/
    
    
    

/*********************************************************/
#pragma mark 关于出售小弟
/*********************************************************/
public:
    void            RequestSellFollower();
    void            RequestSellFollowerSucceed(int price);  
/*********************************************************/
#pragma mark-
/*********************************************************/
        
    
    
    
/*********************************************************/
#pragma mark 关于小弟商店 和购买小弟 的交互处理
/*********************************************************/
public:
    void            RequestWareFollower();
    void            RequestUpdateWareFollower(int type);
    
    void            RequestBuyFollower(int type, int followerIndex);
    void            RequestBuyFollowerSucceed(int type, int followerIndex, int followerserverID, int LevelUpExp, int BeAbsorbExp, int Hp, int Attack, int Defense, int ComeBack);
    
    void            AddWareFollower(int type, int index, Follower * F);
    void            ClearWareFollower(int type);
    
    SS_GET(WareList*, &m_WareFollowerList, WareFollowerList);
    
private:
    WareList        m_WareFollowerList;
/*********************************************************/
#pragma mark-
/*********************************************************/

    
    
    
/*********************************************************/
#pragma mark 关于小弟的训练的交互
/*********************************************************/
public:
    //请求训练小弟
    void        RequestTrainingFollower(Follower * follower, list <Follower *> * plist);
    //训练成功的处理
    void        RequestTrainingFollowerSucceed(int price, int exp, int level, int skillLevel, int levelupExp, int beabsorbExp, int hp, int attack, int defense, int comeback);
    
private:
    Follower *  m_TrainingFollower;
/*********************************************************/
#pragma mark-
/*********************************************************/  
  
    
    
    
/*********************************************************/
#pragma mark 关于小弟的进化的交互
/*********************************************************/
public:
    //请求进化小弟
    void        RequestEvolvementFollower(Follower * follower, list <Follower *> * plist);   
    //进化成功的处理
    void        RequestEvolvementFollowerSucceed(int price, int newID, int newcharacterID, int newserverID, int levelupExp, int beabsorbExp, int hp, int attack, int defense, int comeback);
    
private:
    Follower *  m_EvolvementFollower;
/*********************************************************/
#pragma mark -
/*********************************************************/
    
  
    
/*********************************************************/
#pragma mark 关于搜索加好友
/*********************************************************/ 
public:
    //搜索好友
    void        RequestFindFriendByUserID(int UserID);
    //搜索好友成功
    void        RequestFindFriendByUserIDSucceed(FriendInfo& stInfo);
    //请求加好友
    void        RequestAddFriendByUserID(int UserID);
    //请求加好友成功
    void        RequestAddFriendByUserIDSucceed();
    //删除好友
    void        RequestRemoveFriendByUserID(int UserID);
    //删除好友成功
    void        RequestRemoveFriendByUserIDSucceed(int UserID);
    //同意添加好友
    void        RequestAgreeAddFriend(int UserID);
    //同意添加好友成功
    void        RequestAgreeAddFriendSucceed(FriendInfo& stInfo);
/*********************************************************/
#pragma mark -
/*********************************************************/ 
    
    
    
    
/*********************************************************/
#pragma mark 关于好友信息
/*********************************************************/ 
public:
    void        RequestFriendList();
    void        ClearFriendList(int type);
    
    map         <int, list <FriendInfo> >  m_FriendInfoList;
/*********************************************************/
#pragma mark -
/*********************************************************/
    
    
    
    
/*********************************************************/
#pragma mark 关于进入副本战斗信息的交互处理
/*********************************************************/ 
public:
    void                    RequestEnterBattle();           //进入副本获取副本信息
    
    void                    VerificationBattleInfo();       //验证战斗信息
    void                    VerificationBattleInfoSucceed(int playerlevel);
    void                    VerificationBattleInfoFailed();
    
    void                    ReleaseBattleInfo()             { SAFE_DELETE(m_BattleInfo); }
    
    int                     GetMapIndex()                   { return m_BattleInfo -> MapIndex; }
    int                     GetSubMapIndex()                { return m_BattleInfo -> SubMapIndex; }
    int                     GetStepNum()                    { return m_BattleInfo -> StepNum; }
    vector <Enemy_Data *> * GetEnemyInfoList(int step)      { return &m_BattleInfo -> EnemyInfo_List[step]; }
    int                     GetClearLevelDropMoney()        { return m_BattleInfo -> ClearLevelDropMoney; }
    int                     GetClearLevelDropExperience()   { return m_BattleInfo -> ClearLevelDropExperience; }
    
    void                    RequestRelive(); //向服务器发送复活请求
    void                    RequestReliveSucceed(int money);
    
    SS_SET(int, m_ChoiceMapIndex, ChoiceMapIndex);
    SS_SET(BattleInfo*, m_BattleInfo, BattleInfo);
    
private:
    //选择进入的副本ID
    int                     m_ChoiceMapIndex;   
    //副本战斗信息
    BattleInfo *            m_BattleInfo;
/*********************************************************/
#pragma mark-
/*********************************************************/
    
    
    
/*********************************************************/
#pragma mark 关于刷新数据
/*********************************************************/
public:
    // 请求数据更新
    void                    RequestDataUpdate();
/*********************************************************/
#pragma mark-
/*********************************************************/
    
    
    
    
/*********************************************************/
#pragma mark 关于行动力购买
/*********************************************************/
public:
    void                    RequestBuyAction();
    void                    RequestBuyActionSucceed(int money);
/*********************************************************/
#pragma mark-
/*********************************************************/
    
    
    
/*********************************************************/
#pragma mark 关于小弟背包的扩容
/*********************************************************/
public:
    void                    RequestExtendPackage();
    void                    RequestExtendPackageSucceed(int money, int add);
/*********************************************************/
#pragma mark-
/*********************************************************/
    
    
    
/*********************************************************/
#pragma mark 关于搜索加好友
/*********************************************************/
public:
    void                    RequestNotice();
    void                    RequestNoticeSuccess(const string& str);
public:
    vector<string>          m_vecMessageList;
/*********************************************************/
#pragma mark-
/*********************************************************/
    
    
    
/*********************************************************/
#pragma mark 关于签到
/*********************************************************/
public:
    void                    RequestSignToday();
    void                    RequestSignTodaySuccess();
    
    void                    RequestOpenBox(int iType);
    void                    RequestOpenBoxSuccess(int iType);
    
    void                    RequestLostSign(int iType);
    void                    RequestLostSignSuccess();
public:
    SIGN_INFO               m_stSign;
/*********************************************************/
#pragma mark-
/*********************************************************/
    
    
    
protected:
    static ServerDataManage *   m_Instance;
    void Initialize();
};

#endif









