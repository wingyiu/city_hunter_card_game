//
//  Floor.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Floor_h
#define MidNightCity_Floor_h

#include "Floor_Configure.h"
#include "PlayerDataManage.h"

USING_NS_CC;

// 站台火车车厢数量
const int PLATFORM_TRAIN_NUM = 3;
// 楼层等级星星数量
const int FLOOR_LEVEL_STAR_NUM = 5;
// 站台火车移动速度
const int PLATFORM_TRAIN_SPEED = 200;
// 提示层－光芒
const int HINT_LIGHTING = 10000;
// 提示层－图片
const int HINT_ITEM = 10001;
// 提示层－new动画
const int HINT_NEW = 10002;

// 楼层npc行动状态
enum FLOOR_NPC_ACTION_STATE
{
    NPC_STAY = 0,                       // 待机
    NPC_IDOL_LEFT,                      // 闲逛－左
    NPC_IDOL_RIGHT,                     // 闲逛－右
    NPC_MAKE_STATE = NPC_IDOL_RIGHT,    // 计算状态
};

class Floor : public CCLayer
{
public:
    #pragma mark - 构造函数
    Floor();
    #pragma mark - 析构函数
    virtual ~Floor();
    
    #pragma mark - 重写基类初始化函数
    bool init();
    
    #pragma mark - 退出函数
    virtual void onExit();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 根据楼层数据初始化楼层函数
    bool InitFloorByData(const Floor_Data * pData);
    
    #pragma mark - 开始建造楼层函数
    void BeginBuilding();
    #pragma mark - 开始升级楼层函数
    void BeginLevelup();
    
    #pragma mark - 设置楼层新数据函数
    void SetFloorData(const Floor_Data* pData)                  {m_pData = pData;}
    #pragma mark - 获得楼层数据函数
    const Floor_Data* GetFloorData()                            {return m_pData;}
    #pragma mark - 获得楼层操作状态函数
    bool GetFloorActionState() const                            {return m_bIsFloorInAction;}
    #pragma mark - 获得收获楼层收获状态函数
    bool GetHarvestFloorState() const                           {return m_bIsFloorCanHarvest;}
    
    #pragma mark - 设置楼顶所需金钱和等级函数
    void SetTopFloorNeedMoneyAndLevel(int iLevel, int iMoney);
    #pragma mark - 设置楼层收获时间刷新函数
    void SetHavestFloorTimeRefreshEnable(bool bIsEnable);
    
    #pragma mark - 处理楼层点击事件函数
    void ProcessFloorTouchEvent(CCTouch* pTouch, CCPoint mousePoint);
    #pragma mark - 处理收获成功函数
    void ProcessHavestSucessEvent();
    
    #pragma mark - 特殊处理顶层图片函数
    void RefreshTopFloorImage(const char* szImageName);
    
    #pragma mark - 显示提示图标函数
    void ShowHintItem();
    #pragma mark - 关闭提示图标函数
    void CloseHintItem();
    
    #pragma mark - 使用自定义cocos2d初始化方式
    LAYER_NODE_FUNC(Floor);
private:
    #pragma mark - 初始化玩家楼层函数
    bool InitPlayerFloor(const Floor_Data* pData);
    #pragma mark - 初始化顶层函数
    bool InitTopFloor(const Floor_Data* pData);
    #pragma mark - 初始化站台层函数
    bool InitPlatformFloor(const Floor_Data* pData);
    #pragma mark - 初始化基础楼层函数
    bool InitBaseFloor(const Floor_Data* pData);
    #pragma mark - 初始化其他楼层函数
    bool InitOtherFloor(const Floor_Data* pData);
    
    #pragma mark - 处理站台楼层点击事件函数
    void ProcessPlatformTouchEvent(CCTouch* pTouch, CCPoint mousePoint);
    #pragma mark - 处理收获楼层点击事件函数
    void ProcessHarvestFloorTouchEvent(CCTouch* pTouch, CCPoint mousePoint);
    
    #pragma mark - 处理楼层npc行动函数
    void ProcessFloorNpcAction(CCObject* pObject);
    #pragma mark - 回调函数 : 处理开张标记行动函数
    void ProcessOpenMarkAction(CCObject* pObject);
    #pragma mark - 回调函数 : 处理星星标记闪烁函数
    void ProcessStarLightAction(CCObject* pObject);
    #pragma mark - 回调函数 : 处理钱袋动作函数
    void ProcessBubbleMoneyAction(CCObject* pObject);
    #pragma mark - 回调函数 : 处理收获时间函数
    void ProcessHavestTimeAction(CCObject* pObject);
    #pragma mark - 回调函数 : 处理金钱包动作函数
    void ProcessHavestMoneyAction(CCObject* pObject);
    #pragma mark - 回调函数 : 处理提示动作函数
    void ProcessHintAction(CCObject* pObject);
    #pragma mark - 回调函数 : 处理new动画函数
    void ProcessNewAction(CCObject* pObject);
    
    #pragma mark - 建造步骤1
    void BuildingStep1();
    #pragma mark - 回调函数 : 建造步骤2
    void BuildingStep2();
    #pragma mark - 回调函数 : 建造步骤3
    void BuildingStep3();
    #pragma mark - 回调函数 : 完成建造
    void BuildingFinish();
    
    #pragma mark - 升级步骤1
    void LevelupStep1();
    #pragma mark - 回调函数 : 升级步骤2
    void LevelupStep2();
    #pragma mark - 回调函数 : 升级步骤3
    void LevelupStep3();
    #pragma mark - 回调函数 : 完成升级
    void LevelupFinish();
private:
    // 楼层对象变量
    CCSprite* m_pBackImage;                                     // 楼层背景图片
    CCSprite* m_pNameImage;                                     // 楼层名称
    CCSprite* m_pTimeProgressBack;                              // 时间进度条背景
    CCSprite* m_pSandy;                                         // 沙漏
    CCSprite* m_pLevelBack;                                     // 等级背景
    CCSprite* m_pFloorNpcAnim;                                  // 楼层npc
    
    CCProgressTimer* m_pTimeProgress;                           // 时间进度条
    
    CCSprite* m_pFloorLevelStar[FLOOR_LEVEL_STAR_NUM];          // 楼层等级星星
    CCSprite* m_pStarMask;                                      // 楼层等级星星遮罩
    
    CCLabelAtlas* m_pCurrentLevelNeed;                          // 顶层 : 建当前楼层所需等级
    CCLabelAtlas* m_pCurrentMoneyNeed;                          // 顶层 : 建当前楼层所需金钱
    
    CCSprite* m_pPlatformTrain[PLATFORM_TRAIN_NUM];             // 站台层 : 火车车厢
    CCSprite* m_pPlatformFront;                                 // 站台层 : 前层
    
    CCSprite* m_pBubbleBack;                                    // 收获层 : 泡泡后
    CCSprite* m_pBubbleMoney;                                   // 收获层 : 泡泡中钱袋
    CCSprite* m_pBubbleFront;                                   // 收获层 : 泡泡前
    
    CCSprite* m_pShoppingAnim;                                  // shop动画
    
    CCLayer* m_pHint;                                           // 提示层
    
    // 建造楼层对象变量
    CCSprite* m_pGlassLeftLeft;                                 // 镜子门－左左
    CCSprite* m_pGlassLeft;                                     // 镜子门－左
    CCSprite* m_pGlassRightRight;                               // 精子们－右右
    CCSprite* m_pGlassRight;                                    // 镜子门－右
    CCSprite* m_pGlassMark;                                     // 镜子标志
    
    CCSprite* m_pBuildingBack1;                                 // 建造层 : 1
    CCSprite* m_pBuildingBack2;                                 // 建造层 : 2
    
    CCSprite* m_pBrushAnim;                                     // 刷子动画
    CCSprite* m_pStandByImage;                                  // 待建楼层图片
    
    CCSprite* m_pBulidingProgressBack;                          // 建造进度条背景
    CCProgressTimer* m_pBuildingProgress;                       // 建造进度条
    
    CCSprite* m_pMoneyIconAdd;                                  // 金币加号
    CCLabelAtlas* m_pMoneyAddNum;                               // 金币数字
    
    CCLayer* m_pMoneyHavestMoney;                               // 收获图层
    
    // 楼层属性变量
    const Floor_Data* m_pData;                                  // 楼层数据
    
    bool m_bIsFloorInAction;                                    // 楼层是否处于可操作状态
    bool m_bIsFloorCanHarvest;                                  // 楼层是否处于收获状态
    bool m_bIsTimeRefresh;                                      // 楼层时间刷新标记
    
    float m_fOpenMarkAngle;                                     // 标记旋转角度
    
    FLOOR_NPC_ACTION_STATE m_eNpcState;                         // npc状态
    
    CCPoint m_MoneyTouchPos;                                    // 钱币点击坐标，为世界坐标
};

#endif