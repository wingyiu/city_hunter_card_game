//
//  Floor_Configure.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Floor_Configure_h
#define MidNightCity_Floor_Configure_h

#include "MyConfigure.h"
#include "cocos2d.h"

// 楼层类型枚举
enum FLOORTYPE
{
    F_Player = 1,
    F_Follower = 2,
    F_Mission = 3,
    F_Shop = 4,
    F_Platform = 5,
    F_Store = 6,
    F_Cardgame = 7,
    F_Ktv = 8,
    F_Snock = 9,
    F_Train = 10,
    F_Game = 11,
    F_Bar = 12,
    F_Gamble = 13,
    F_Hint,
};

// 楼层状态美剧
enum FLOOR_STATE
{
    F_COMPLETE = 0,                 // 建造完成
    F_BUILDING,                     // 建造中
    F_STANDBY,                      // 待建中
    F_OPEN,                         // 开业
};

// 楼层信息
typedef struct FloorData
{
    FloorData() { memset(this, 0, sizeof(FloorData)); }
    ~FloorData() {}
    
    //楼层的类型
    int         Floor_Type;
    //楼层的名称
    char        Floor_Name[CHARLENGHT];
    //楼层的背景图片名称
    char        Floor_BgName[CHARLENGHT];
    
    //建造该楼层所需要的玩家等级
    int         Floor_Build_PlayerLevel;
    //建造该楼层所需要的玩家金钱
    int         Floor_Build_PlayerMoney;
    
    //升级该楼层所需要的玩家等级
    int         Floor_Upgrade_PlayerLevel;
    //升级该楼层所需要的玩家金钱
    int         Floor_Upgrade_PlayerMoney;
    
    //该楼层的等级
    int         Floor_Level;
    
    //该楼层收获的钱
    float       Floor_HarvestMoney;
    
    //楼层收获所需的时间
    int         Floor_HarvestTime;
}Floor_Data;

#endif