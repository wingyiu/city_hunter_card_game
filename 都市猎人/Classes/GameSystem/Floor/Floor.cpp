//
//  Floor.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-4-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Floor.h"
#include "Layer_Building.h"
#include "Layer_Game.h"

/************************************************************************/
#pragma mark - Floor类构造函数
/************************************************************************/
Floor::Floor()
{
    // 为楼层对象变量赋初值
    m_pBackImage = NULL;
    m_pNameImage = NULL;
    m_pTimeProgressBack = NULL;
    m_pSandy = NULL;
    m_pLevelBack = NULL;
    m_pFloorNpcAnim = NULL;
    
    m_pTimeProgress = NULL;
    
    for (int i = 0; i < FLOOR_LEVEL_STAR_NUM; i++)
        m_pFloorLevelStar[i] = NULL;
    m_pStarMask = NULL;
    
    m_pCurrentLevelNeed = NULL;
    m_pCurrentMoneyNeed = NULL;
    
    for (int i = 0; i < PLATFORM_TRAIN_NUM; i++)
        m_pPlatformTrain[i] = NULL;
    m_pPlatformFront = NULL;
    
    m_pBubbleBack = NULL;
    m_pBubbleMoney = NULL;
    m_pBubbleFront = NULL;
    
    m_pHint = NULL;
    
    // 为建造楼层对象变量赋初值
    m_pGlassLeftLeft = NULL;
    m_pGlassLeft = NULL;
    m_pGlassRightRight = NULL;
    m_pGlassRight = NULL;
    m_pGlassMark = NULL;
    
    m_pBuildingBack1 = NULL;
    m_pBuildingBack2 = NULL;
    
    m_pBrushAnim = NULL;
    m_pStandByImage = NULL;
    
    m_pBulidingProgressBack = NULL;
    m_pBuildingProgress = NULL;
    
    m_pMoneyIconAdd = NULL;
    m_pMoneyAddNum = NULL;
    
    m_pMoneyHavestMoney = NULL;
    
    // 为楼层属性变量赋初值
    m_pData = NULL;
    
    m_bIsFloorInAction = false;
    m_bIsFloorCanHarvest = false;
    m_bIsTimeRefresh = false;
    
    m_fOpenMarkAngle = 45.0f;
    
    m_eNpcState = NPC_STAY;
    
    m_MoneyTouchPos = ccp(0.0f, 0.0f);
}

/************************************************************************/
#pragma mark - Floor类析构函数
/************************************************************************/
Floor::~Floor()
{
    // 销毁对象
    Destroy();
    
    printf("Floor : Delete \n ");
}

/************************************************************************/
#pragma mark - Floor类重写基类初始化函数
/************************************************************************/
bool Floor::init()
{
    if (CCLayer::init() == false)
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - Floor类退出函数
/************************************************************************/
void Floor::onExit()
{
    // 调用基类退出函数
    CCLayer::onExit();
    // 销毁对象
    Destroy();
}

/************************************************************************/
#pragma mark - Floor类销毁函数
/************************************************************************/
void Floor::Destroy()
{
    // 销毁楼层变量
    removeChild(m_pBackImage, true);
    removeChild(m_pNameImage, true);
    removeChild(m_pTimeProgressBack, true);
    removeChild(m_pSandy, true);
    removeChild(m_pLevelBack, true);
    m_pFloorNpcAnim->stopAllActions();
    removeChild(m_pFloorNpcAnim, true);
    
    removeChild(m_pTimeProgress, true);
    
    for (int i = 0; i < FLOOR_LEVEL_STAR_NUM; i++)
    {
        m_pFloorLevelStar[i]->stopAllActions();
        removeChild(m_pFloorLevelStar[i], true);
    }
    m_pStarMask->stopAllActions();
    removeChild(m_pStarMask, true);
    
    removeChild(m_pCurrentLevelNeed, true);
    removeChild(m_pCurrentMoneyNeed, true);
    
    for (int i = 0; i < PLATFORM_TRAIN_NUM; i++)
        removeChild(m_pPlatformTrain[i], true);
    removeChild(m_pPlatformFront, true);
    
    removeChild(m_pBubbleBack, true);
    removeChild(m_pBubbleMoney, true);
    removeChild(m_pBubbleFront, true);
    
    m_pShoppingAnim->stopAllActions();
    removeChild(m_pShoppingAnim, true);
    
    if (m_pHint != NULL)
    {
        CCSprite* pLighting = dynamic_cast<CCSprite*>(m_pHint->getChildByTag(HINT_LIGHTING));
        if (pLighting != NULL)
        {
            pLighting->stopAllActions();
            m_pHint->removeChild(pLighting, true);
        }
        
        CCSprite* pHint = dynamic_cast<CCSprite*>(m_pHint->getChildByTag(HINT_ITEM));
        if (pHint != NULL)
        {
            pHint->stopAllActions();
            m_pHint->removeChild(pHint, true);
        }
        
        CCSprite* pNew = dynamic_cast<CCSprite*>(m_pHint->getChildByTag(HINT_NEW));
        if (pNew != NULL)
        {
            pNew->stopAllActions();
            m_pHint->removeChild(pNew, true);
        }
        
        removeChild(m_pHint, true);
        m_pHint = NULL;
    }
    
    // 移出建造楼层对象变量
    removeChild(m_pGlassLeftLeft, true);
    removeChild(m_pGlassLeft, true);
    removeChild(m_pGlassRightRight, true);
    removeChild(m_pGlassRight, true);
    removeChild(m_pGlassMark, true);
    
    removeChild(m_pBuildingBack1, true);
    removeChild(m_pBuildingBack2, true);
    
    m_pBrushAnim->stopAllActions();
    removeChild(m_pBrushAnim, true);
    m_pStandByImage->stopAllActions();
    removeChild(m_pStandByImage, true);
    
    removeChild(m_pBulidingProgressBack, true);
    m_pBuildingProgress->stopAllActions();
    removeChild(m_pBuildingProgress, true);
    
    if (m_pMoneyHavestMoney != NULL)
    {
        m_pMoneyIconAdd->stopAllActions();
        m_pMoneyHavestMoney->removeChild(m_pMoneyIconAdd, true);
        m_pMoneyAddNum->stopAllActions();
        m_pMoneyHavestMoney->removeChild(m_pMoneyAddNum, true);
        
        removeChild(m_pMoneyHavestMoney, true);
        m_pMoneyHavestMoney = NULL;
    }
}

/************************************************************************/
#pragma mark - Floor类根据楼层数据初始化楼层函数
/************************************************************************/
bool Floor::InitFloorByData(const Floor_Data * pData)
{
    // 非法数据指针，则退出
    if (pData == NULL)
    {
        printf("Floor::InitFloorByData : 非法数据\n");
        return false;
    }
    
    // 接受数据指针
    m_pData = pData;
    
    // 根据类型进行楼层初始化
    if (m_pData->Floor_Type == F_Player)
        return InitPlayerFloor(pData);
    else if (m_pData->Floor_Type == F_Hint)
        return InitTopFloor(pData);
    else if (m_pData->Floor_Type == F_Platform)
        return InitPlatformFloor(pData);
    else if (m_pData->Floor_Type == F_Follower || m_pData->Floor_Type == F_Mission || m_pData->Floor_Type == F_Shop)
        return InitBaseFloor(pData);
    else
        return InitOtherFloor(pData);
    
    return false;
}

/************************************************************************/
#pragma mark - Floor类开始建造楼层函数
/************************************************************************/
void Floor::BeginBuilding()
{
    // 初始化玻璃纹理
    m_pGlassLeftLeft = CCSprite::spriteWithSpriteFrameName("glass_left_left.png");
    if (m_pGlassLeftLeft != NULL)
    {
        m_pGlassLeftLeft->setPosition(ccp(20.0f, -26.0f));
        addChild(m_pGlassLeftLeft);
    }
    m_pGlassLeft = CCSprite::spriteWithSpriteFrameName("glass_left.png");
    if (m_pGlassLeft != NULL)
    {
        m_pGlassLeft->setPosition(ccp(20.0f, -26.0f));
        addChild(m_pGlassLeft);
    }
    m_pGlassRightRight = CCSprite::spriteWithSpriteFrameName("glass_right_right.png");
    if (m_pGlassRightRight != NULL)
    {
        m_pGlassRightRight->setPosition(ccp(20.0f, -26.0f));
        addChild(m_pGlassRightRight);
    }
    m_pGlassRight = CCSprite::spriteWithSpriteFrameName("glass_right.png");
    if (m_pGlassRight != NULL)
    {
        m_pGlassRight->setPosition(ccp(20.0f, -26.0f));
        addChild(m_pGlassRight);
    }
    
    m_pGlassMark = CCSprite::spriteWithSpriteFrameName("glass_mark.png");
    if (m_pGlassMark != NULL)
    {
        m_pGlassMark->setPosition(ccp(12.0f, -20.0f));
        addChild(m_pGlassMark);
    }
    
    // 初始化建造背景
    m_pBuildingBack1 = CCSprite::spriteWithSpriteFrameName("floor_build_2.png");
    if (m_pBuildingBack1 != NULL)
        addChild(m_pBuildingBack1);
    
    m_pBuildingBack2 = CCSprite::spriteWithSpriteFrameName("floor_build_1.png");
    if (m_pBuildingBack2 != NULL)
        addChild(m_pBuildingBack2);
    
    // 初始化进度条背景
    m_pBulidingProgressBack = CCSprite::spriteWithSpriteFrameName("building_progress_back.png");
    if (m_pBulidingProgressBack != NULL)
    {
        m_pBulidingProgressBack->setPosition(ccp(0.0f, 20.0f));
        addChild(m_pBulidingProgressBack);
    }
    
    m_pBuildingProgress = CCProgressTimer::progressWithFile("building_progress-hd.png");
    if (m_pBuildingProgress != NULL)
    {
        m_pBuildingProgress->setPosition(ccp(0.0f, 20.0f));
        m_pBuildingProgress->setType(kCCProgressTimerTypeHorizontalBarLR);
        addChild(m_pBuildingProgress);
    }
    
    // 初始化刷子动画
    m_pBrushAnim = CCSprite::spriteWithSpriteFrameName("brush_1.png");
    if (m_pBrushAnim != NULL)
    {
        m_pBrushAnim->setPosition(ccp(0.0f, -20.0f));
        addChild(m_pBrushAnim);
        
        // 创建刷子动画
        CCAnimation* pAnimation = CreateAnimation("brush_", 0.08f);
        if (pAnimation != NULL)
        {
            CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnimation);
            if (pAnimate != NULL)
            {
                CCRepeatForever* pForever = CCRepeatForever::actionWithAction(pAnimate);
                if (pForever != NULL)
                    m_pBrushAnim->runAction(pForever);
            }
        }
    }
    
    // 初始化待建楼层
    m_pStandByImage = CCSprite::spriteWithSpriteFrameName("floor_standby.png");
    if (m_pStandByImage != NULL)
    {
        m_pStandByImage->setPosition(ccp(0.0f, 0.0f));
        addChild(m_pStandByImage);
    }
    
    // 关闭星星可见，进行动画
    if (m_pData->Floor_Type > F_Platform)
    {
        for (int i = 0; i < FLOOR_LEVEL_STAR_NUM; i++)
            m_pFloorLevelStar[i]->setIsVisible(false);
    }
    
    // 设置楼层操作状态
    m_bIsFloorInAction = true;
    
    // 开始建造 - 步骤1
    BuildingStep1();
}

/************************************************************************/
#pragma mark - Floor类开始升级楼层函数
/************************************************************************/
void Floor::BeginLevelup()
{
    // 初始化玻璃纹理
    m_pGlassLeftLeft = CCSprite::spriteWithSpriteFrameName("glass_left_left.png");
    if (m_pGlassLeftLeft != NULL)
    {
        m_pGlassLeftLeft->setPosition(ccp(20.0f, -26.0f));
        addChild(m_pGlassLeftLeft);
    }
    m_pGlassLeft = CCSprite::spriteWithSpriteFrameName("glass_left.png");
    if (m_pGlassLeft != NULL)
    {
        m_pGlassLeft->setPosition(ccp(20.0f, -26.0f));
        addChild(m_pGlassLeft);
    }
    m_pGlassRightRight = CCSprite::spriteWithSpriteFrameName("glass_right_right.png");
    if (m_pGlassRightRight != NULL)
    {
        m_pGlassRightRight->setPosition(ccp(20.0f, -26.0f));
        addChild(m_pGlassRightRight);
    }
    m_pGlassRight = CCSprite::spriteWithSpriteFrameName("glass_right.png");
    if (m_pGlassRight != NULL)
    {
        m_pGlassRight->setPosition(ccp(20.0f, -26.0f));
        addChild(m_pGlassRight);
    }
    
    m_pGlassMark = CCSprite::spriteWithSpriteFrameName("glass_mark.png");
    if (m_pGlassMark != NULL)
    {
        m_pGlassMark->setPosition(ccp(12.0f, -20.0f));
        addChild(m_pGlassMark);
    }
    
    // 设置楼层操作状态
    m_bIsFloorInAction = true;
    
    // 开始升级 - 步骤1
    LevelupStep1();
}

/************************************************************************/
#pragma mark - Floor类设置楼顶所需金钱和等级函数
/************************************************************************/
void Floor::SetTopFloorNeedMoneyAndLevel(int iLevel, int iMoney)
{
    char szTemp[32] = {0};
    sprintf(szTemp, "%d", iLevel);
    m_pCurrentLevelNeed->setString(szTemp);
    
    sprintf(szTemp, "%d", iMoney);
    m_pCurrentMoneyNeed->setString(szTemp);
}

/************************************************************************/
#pragma mark - Floor类设置楼层收获时间刷新函数
/************************************************************************/
void Floor::SetHavestFloorTimeRefreshEnable(bool bIsEnable)
{
    if (m_pData->Floor_Type <= F_Platform)
        return;
    
    m_bIsTimeRefresh = bIsEnable;
    ProcessHavestTimeAction(NULL);
}

/************************************************************************/
#pragma mark - Floor类处理楼层点击事件函数
/************************************************************************/
void Floor::ProcessFloorTouchEvent(CCTouch* pTouch, CCPoint mousePoint)
{
    switch (m_pData->Floor_Type) {
        case F_Platform:
            ProcessPlatformTouchEvent(pTouch, mousePoint);
            break;
        case F_Store:
        case F_Cardgame:
        case F_Ktv:
        case F_Snock:
        case F_Train:
        case F_Game:
        case F_Bar:
        case F_Gamble:
            ProcessHarvestFloorTouchEvent(pTouch, mousePoint);
            break;
        default:
            break;
    }
}

/************************************************************************/
#pragma mark - Floor类处理收获成功函数
/************************************************************************/
void Floor::ProcessHavestSucessEvent()
{
    // 播放音效
    KNUIFunction::GetSingle()->PlayerEffect("3.wav");
    
    if (m_MoneyTouchPos.x != 0.0f)
    {
        Layer_Game::GetSingle()->ProcessCoinAnim(m_MoneyTouchPos);
        m_MoneyTouchPos = ccp(0.0f, 0.0f);
    }
    
    //m_bIsFloorInAction = false;
    
    // 获得位置
    CCPoint point = m_pBubbleMoney->getPosition();
    
    // 冒出金币增加数字
//    m_pMoneyIconAdd->setPosition(ccp(point.x - 5.0f, point.y + 8.0f));
//    m_pMoneyAddNum->setPosition(point);
    m_pMoneyHavestMoney->setPosition(point);
    
    // 加号
    CCFadeIn* pFadeIn = CCFadeIn::actionWithDuration(0.4f);
    CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(2.0f);
    if (pFadeIn != NULL && pFadeOut != NULL)
        m_pMoneyIconAdd->runAction(CCSequence::actions(pFadeIn, pFadeOut, NULL));
    
    // 数字
    char szTemp[32] = {0};
    sprintf(szTemp, "%.0f", m_pData->Floor_HarvestMoney);
    m_pMoneyAddNum->setString(szTemp);
    
    pFadeIn = CCFadeIn::actionWithDuration(0.4f);
    pFadeOut = CCFadeOut::actionWithDuration(2.0f);
    if (pFadeIn != NULL && pFadeOut != NULL)
        m_pMoneyAddNum->runAction(CCSequence::actions(pFadeIn, pFadeOut, NULL));
    
    // 图层放大
    CCScaleTo* pScaleTo = CCScaleTo::actionWithDuration(2.4f, 2.5f);
    if (pScaleTo != NULL)
    {
        m_pMoneyHavestMoney->setScale(1.0f);
        m_pMoneyHavestMoney->runAction(pScaleTo);
    }
    
    // 缩小金币泡泡和钱袋
    CCDelayTime* pTime1 = CCDelayTime::actionWithDuration(0.5f);
    CCDelayTime* pTime2 = CCDelayTime::actionWithDuration(0.5f);
    CCDelayTime* pTime3 = CCDelayTime::actionWithDuration(0.5f);
    CCScaleTo* pScaleTo1 = CCScaleTo::actionWithDuration(0.5f, 0.0f);
    CCScaleTo* pScaleTo2 = CCScaleTo::actionWithDuration(0.5f, 0.0f);
    CCScaleTo* pScaleTo3 = CCScaleTo::actionWithDuration(0.5f, 0.0f);
    if (pTime1 != NULL && pTime2 != NULL && pTime3 != NULL && pScaleTo1 != NULL && pScaleTo2 != NULL && pScaleTo3 != NULL)
    {
        m_pBubbleBack->runAction(CCSequence::actions(pTime1, pScaleTo1, NULL));
        m_pBubbleMoney->runAction(CCSequence::actions(pTime2, pScaleTo2, NULL));
        m_pBubbleFront->runAction(CCSequence::actions(pTime3, pScaleTo3, NULL));
    }
    
    m_bIsFloorCanHarvest = false;
    
    // 开始计时
    m_bIsTimeRefresh = true;
    ProcessHavestTimeAction(NULL);
    
    const Floor_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFloor(m_pData->Floor_Type, m_pData->Floor_Level);
    if (pData != NULL)
    {
        m_pTimeProgress->stopAllActions();
        
        // 开始加速进度条
        CCProgressTo* pProgressTo1 = CCProgressTo::actionWithDuration(0.5f, 100.0f);
        CCProgressTo* pProgressTo2 = CCProgressTo::actionWithDuration(0.1f, 0.0f);
        
        float fPercent = 1.0f * m_pData->Floor_HarvestTime / pData->Floor_HarvestTime * 100.0f;
        m_pTimeProgress->setPercentage(100.0f - fPercent);
        
        CCProgressFromTo* pProgressTo = CCProgressFromTo::actionWithDuration(m_pData->Floor_HarvestTime, 100.0f - fPercent, 100.0f);
        CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::ProcessHavestMoneyAction));
        if (pProgressTo1 != NULL && pProgressTo2 != NULL && pProgressTo != NULL && pFunc != NULL)
            m_pTimeProgress->runAction(CCSequence::actions(pProgressTo1, pProgressTo2, pProgressTo, pFunc, NULL));
    }
    
    // 刷新右下菜单
    Layer_Game::GetSingle()->SetMenuItemState(2, Layer_Building::ShareInstance()->RefreshHavestMenu(false));
}

/************************************************************************/
#pragma mark - Floor类特殊处理顶层图片函数
/************************************************************************/
void Floor::RefreshTopFloorImage(const char* szImageName)
{
    if (m_pData->Floor_Type == F_Hint)
    {
        // 移出原先图片
        removeChild(m_pBackImage, true);
        
        // 初始化新图片
        m_pBackImage = CCSprite::spriteWithSpriteFrameName(szImageName);
        addChild(m_pBackImage);
    }
}

/************************************************************************/
#pragma mark - Floor类显示提示图标函数
/************************************************************************/
void Floor::ShowHintItem()
{
    if (m_pHint != NULL)
    {
        // 打开图层
        m_pHint->setIsVisible(true);
        
        // 处理图标动作
        ProcessHintAction(NULL);
        
        // 播放new动画
        if (m_pData->Floor_Type == F_Follower || m_pData->Floor_Type == F_Mission)
            ProcessNewAction(m_pHint->getChildByTag(HINT_NEW));
        
        // 播放剑动画
        if (m_pData->Floor_Type == F_Mission)
        {
            CCSprite* pSword = dynamic_cast<CCSprite*>(m_pHint->getChildByTag(HINT_ITEM));
            if (pSword != NULL)
            {
                CCAnimation* pAnimation = CreateAnimation("sword_", 0.05f);
                CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnimation);
                CCRepeatForever* pForever = CCRepeatForever::actionWithAction(pAnimate);
            
                pSword->runAction(pForever);
            }
        }
    }
}

/************************************************************************/
#pragma mark - Floor类关闭提示图标函数
/************************************************************************/
void Floor::CloseHintItem()
{
    if (m_pHint != NULL)
    {
        // 光芒
        CCSprite* pLighting = dynamic_cast<CCSprite*>(m_pHint->getChildByTag(HINT_LIGHTING));
        if (pLighting != NULL)
            pLighting->stopAllActions();
        
        // 提示
        CCSprite* pHint = dynamic_cast<CCSprite*>(m_pHint->getChildByTag(HINT_ITEM));
        if (pHint != NULL)
            pHint->stopAllActions();
        
        // new
        CCSprite* pNew = dynamic_cast<CCSprite*>(m_pHint->getChildByTag(HINT_NEW));
        if (pNew != NULL)
            pNew->stopAllActions();
        
        // 隐藏提示层
        m_pHint->setIsVisible(false);
    }
}

/************************************************************************/
#pragma mark - Floor类初始化玩家楼层函数
/************************************************************************/
bool Floor::InitPlayerFloor(const Floor_Data* pData)
{
    // 初始化背景图片
    m_pBackImage = CCSprite::spriteWithSpriteFrameName(pData->Floor_BgName);
    if (m_pBackImage != NULL)
    {
        m_pBackImage->setVertexZ(-0.1f);
        addChild(m_pBackImage);
    }
    
    // 初始化楼层名称图片
    m_pNameImage = CCSprite::spriteWithSpriteFrameName(pData->Floor_Name);
    if (m_pNameImage != NULL)
    {
        CCPoint pos = m_pBackImage->getPosition();
        CCSize size = m_pBackImage->getContentSize();
        
        pos.x = pos.x - size.width / 2.0f + m_pNameImage->getContentSize().width / 2.0f + 5.0f;
        pos.y = pos.y + size.height / 2.0f - 58.0f;
        
        m_pNameImage->setPosition(pos);
        addChild(m_pNameImage);
    }
    
    // 初始化人物动画
    char szTemp[32] = {0};
    sprintf(szTemp, "npc%d_floor1_stay1.png", PlayerDataManage::m_PlayerFistGameProfession);
    m_pFloorNpcAnim = CCSprite::spriteWithSpriteFrameName(szTemp);
    if (m_pFloorNpcAnim != NULL)
    {
        m_pFloorNpcAnim->setPosition(ccp(0.0f, -3.0f));
        addChild(m_pFloorNpcAnim);
        
        // 使用npc行动
        ProcessFloorNpcAction(NULL);
    }
    
    // 初始化提示层
    m_pHint = CCLayer::node();
    if (m_pHint != NULL)
    {
        // 初始化光芒
        CCSprite* pLighting = CCSprite::spriteWithSpriteFrameName("light_back.png");
        pLighting->setPosition(ccp(-60.0f, 0.0f));
        m_pHint->addChild(pLighting, 0, HINT_LIGHTING);
        
        // 初始化提示
        CCSprite* pHint = CCSprite::spriteWithSpriteFrameName("menu_item_2.png");
        pHint->setPosition(ccp(-60.0f, 0.0f));
        m_pHint->addChild(pHint, 0, HINT_ITEM);
        
        // 添加层到场景
        addChild(m_pHint);
        
        // 关闭
        ShowHintItem();
    }
    
    return true;
}

/************************************************************************/
#pragma mark - Floor类初始化顶层函数
/************************************************************************/
bool Floor::InitTopFloor(const Floor_Data* pData)
{
    // 初始化背景图片
    m_pBackImage = CCSprite::spriteWithSpriteFrameName(pData->Floor_BgName);
    if (m_pBackImage != NULL)
        addChild(m_pBackImage);
    
    // 初始化楼层建造所需等级
    m_pCurrentLevelNeed = CCLabelAtlas::labelWithString("0", "number_type_1-hd.png", 15, 18, '.');
    if (m_pCurrentLevelNeed != NULL)
    {
        m_pCurrentLevelNeed->setPosition(ccp(-40.0f, 23.0f));
        addChild(m_pCurrentLevelNeed, 1);
    }
    
    // 初始化楼层建造所需金钱
    m_pCurrentMoneyNeed = CCLabelAtlas::labelWithString("0", "number_type_2-hd.png", 11, 18, '.');
    if (m_pCurrentMoneyNeed != NULL)
    {
        m_pCurrentMoneyNeed->setPosition(ccp(-40.0f, -2.0f));
        addChild(m_pCurrentMoneyNeed, 1);
    }
    
    // 初始化楼层名称图片
//    m_pNameImage = CCSprite::spriteWithSpriteFrameName(pData->Floor_Name);
//    if (m_pNameImage != NULL)
//    {
//        CCPoint pos = m_pBackImage->getPosition();
//        CCSize size = m_pBackImage->getContentSize();
//        
//        pos.x = pos.x - size.width / 2.0f + m_pNameImage->getContentSize().width / 2.0f + 10.0f;
//        pos.y = pos.y - size.height / 2.0f - 15.0f;
//        
//        m_pNameImage->setPosition(pos);
//        addChild(m_pNameImage);
//    }
    
    // 刷新信息
    const FloorData * pFloorData = SystemDataManage::ShareInstance() -> GetData_ForFloor(6, 1);
    if (pFloorData != NULL)
        SetTopFloorNeedMoneyAndLevel(pFloorData->Floor_Build_PlayerLevel, pFloorData->Floor_Build_PlayerMoney);
    
    return true;
}

/************************************************************************/
#pragma mark - Floor类初始化站台层函数
/************************************************************************/
bool Floor::InitPlatformFloor(const Floor_Data* pData)
{
    // 初始化背景图片
    m_pBackImage = CCSprite::spriteWithSpriteFrameName(pData->Floor_BgName);
    if (m_pBackImage != NULL)
    {
        m_pBackImage->setVertexZ(-0.4f);
        addChild(m_pBackImage);
    }
    
    // 初始化楼层名称图片
    m_pNameImage = CCSprite::spriteWithSpriteFrameName(pData->Floor_Name);
    if (m_pNameImage != NULL)
    {
        CCPoint pos = m_pBackImage->getPosition();
        CCSize size = m_pBackImage->getContentSize();
        
        pos.x = pos.x - size.width / 2.0f + m_pNameImage->getContentSize().width / 2.0f + 5.0f;
        pos.y = pos.y + 40.0f;
        
        m_pNameImage->setPosition(pos);
        addChild(m_pNameImage, 1);
    }
    
    // 初始化火车车厢
    char szTemp[32] = {0};
    for (int i = 0; i < PLATFORM_TRAIN_NUM; i++)
    {
        sprintf(szTemp, "floor_train_%d.png", i + 1);
        m_pPlatformTrain[i] = CCSprite::spriteWithSpriteFrameName(szTemp);
        if (m_pPlatformTrain[i] != NULL)
        {
            m_pPlatformTrain[i]->setVertexZ(-0.3f + 0.03f * i);
            m_pPlatformTrain[i]->setPosition(ccp(-i * 328.0f - 150.0f, -20.0f));
            addChild(m_pPlatformTrain[i]);
        }
    }
    
    // 初始化前台
    m_pPlatformFront = CCSprite::spriteWithSpriteFrameName("floor_station.png");
    if (m_pPlatformFront != NULL)
    {
        m_pPlatformFront->setVertexZ(-0.1f);
        addChild(m_pPlatformFront);
    }
    
    // 初始化人物动画
    sprintf(szTemp, "npc1_floor%d_stay1.png", m_pData->Floor_Type);
    m_pFloorNpcAnim = CCSprite::spriteWithSpriteFrameName(szTemp);
    if (m_pFloorNpcAnim != NULL)
    {
        m_pFloorNpcAnim->setVertexZ(-0.07f);
        m_pFloorNpcAnim->setPosition(ccp(0.0f, -25.0f));
        addChild(m_pFloorNpcAnim);
        
        // 使npc行动
        ProcessFloorNpcAction(NULL);
    }
    
    // 初始化提示层
    m_pHint = CCLayer::node();
    if (m_pHint != NULL)
    {
        // 初始化光芒
        CCSprite* pLighting = CCSprite::spriteWithSpriteFrameName("light_back.png");
        pLighting->setPosition(ccp(-60.0f, -25.0f));
        m_pHint->addChild(pLighting, 0, HINT_LIGHTING);
        
        // 初始化提示
        CCSprite* pHint = CCSprite::spriteWithSpriteFrameName("menu_item_1.png");
        pHint->setPosition(ccp(-60.0f, -25.0f));
        m_pHint->addChild(pHint, 0, HINT_ITEM);
        
        // 添加层到场景
        addChild(m_pHint);
        
        // 关闭
        CloseHintItem();
    }
    
    return true;
}

/************************************************************************/
#pragma mark - Floor类初始化基础楼层函数
/************************************************************************/
bool Floor::InitBaseFloor(const Floor_Data* pData)
{
    // 初始化背景图片
    m_pBackImage = CCSprite::spriteWithSpriteFrameName(pData->Floor_BgName);
    if (m_pBackImage != NULL)
    {
        m_pBackImage->setVertexZ(-0.1f);
        addChild(m_pBackImage);
    }
    
    // 初始化楼层名称图片
    m_pNameImage = CCSprite::spriteWithSpriteFrameName(pData->Floor_Name);
    if (m_pNameImage != NULL)
    {
        CCPoint pos = m_pBackImage->getPosition();
        CCSize size = m_pBackImage->getContentSize();
        
        pos.x = pos.x - size.width / 2.0f + m_pNameImage->getContentSize().width / 2.0f + 5.0f;
        pos.y = pos.y + 18.0f;
        
        m_pNameImage->setPosition(pos);
        addChild(m_pNameImage);
    }
    
    // 展牌动画
    if (m_pData->Floor_Type == F_Shop)
    {
        m_pShoppingAnim = CCSprite::spriteWithSpriteFrameName("shopping_1.png");
        
        CCAnimation* pAnimation = CreateAnimation("shopping_", 0.05f);
        CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnimation);
        CCRepeatForever* pForever = CCRepeatForever::actionWithAction(pAnimate);
        
        m_pShoppingAnim->setPosition(ccp(-62.0f, -33.0f));
        m_pShoppingAnim->runAction(pForever);
        
        addChild(m_pShoppingAnim);
    }
    
    // 初始化人物动画
    char szTemp[16] = {0};
    sprintf(szTemp, "npc1_floor%d_stay1.png", m_pData->Floor_Type);
    m_pFloorNpcAnim = CCSprite::spriteWithSpriteFrameName(szTemp);
    if (m_pFloorNpcAnim != NULL)
    {
        m_pFloorNpcAnim->setPosition(ccp(0.0f, -25.0f));
        addChild(m_pFloorNpcAnim);
        
        // 使npc行动
        ProcessFloorNpcAction(NULL);
    }
    
    // 初始化提示层
    if (m_pData->Floor_Type == F_Follower)
    {
        m_pHint = CCLayer::node();
        if (m_pHint != NULL)
        {
            // 初始化光芒
            CCSprite* pLighting = CCSprite::spriteWithSpriteFrameName("light_back.png");
            pLighting->setPosition(ccp(-60.0f, -30.0f));
            m_pHint->addChild(pLighting, 0, HINT_LIGHTING);
            
            // 初始化提示
            CCSprite* pFollowerCard = CCSprite::spriteWithSpriteFrameName("follower_card.png");
            pFollowerCard->setPosition(ccp(-60.0f, -30.0f));
            m_pHint->addChild(pFollowerCard, 0, HINT_ITEM);
            
            // 初始化new动画
            CCSprite* pNew = CCSprite::spriteWithSpriteFrameName("sword_new_1.png");
            pNew->setPosition(ccp(-60.0f, -5.0f));
            m_pHint->addChild(pNew, 0, HINT_NEW);
            
            // 添加到场景
            addChild(m_pHint);
            
            // 关闭
            CloseHintItem();
        }
    }
    else if (m_pData->Floor_Type == F_Mission)
    {
        m_pHint = CCLayer::node();
        if (m_pHint != NULL)
        {
            // 初始化光芒
            CCSprite* pLighting = CCSprite::spriteWithSpriteFrameName("light_back.png");
            pLighting->setPosition(ccp(-60.0f, -30.0f));
            m_pHint->addChild(pLighting, 0, HINT_LIGHTING);
            
            // 初始化提示
            CCSprite* pSword = CCSprite::spriteWithSpriteFrameName("sword_1.png");
            pSword->setPosition(ccp(-60.0f, -30.0f));
            m_pHint->addChild(pSword, 0, HINT_ITEM);
            
            // 初始化new动画
            CCSprite* pNew = CCSprite::spriteWithSpriteFrameName("sword_new_1.png");
            pNew->setPosition(ccp(-60.0f, -5.0f));
            m_pHint->addChild(pNew, 0, HINT_NEW);
            
            // 添加到场景
            addChild(m_pHint);
            
            // 关闭
            CloseHintItem();
        }
    }
    
    return true;
}

/************************************************************************/
#pragma mark - Floor类初始化其他楼层函数
/************************************************************************/
bool Floor::InitOtherFloor(const Floor_Data* pData)
{
    // 初始化背景图片
    m_pBackImage = CCSprite::spriteWithSpriteFrameName(pData->Floor_BgName);
    if (m_pBackImage != NULL)
        addChild(m_pBackImage);
    
    // 初始化楼层名称图片
    m_pNameImage = CCSprite::spriteWithSpriteFrameName(pData->Floor_Name);
    if (m_pNameImage != NULL)
    {
        CCPoint pos = m_pBackImage->getPosition();
        CCSize size = m_pBackImage->getContentSize();
        
        pos.x = pos.x - size.width / 2.0f + m_pNameImage->getContentSize().width / 2.0f + 5.0f;
        pos.y = pos.y + 20.0f;
        
        m_pNameImage->setPosition(pos);
        addChild(m_pNameImage);
    }
    
    // 初始化进度条背景
    m_pTimeProgressBack = CCSprite::spriteWithSpriteFrameName("build_progress_back.png");
    if (m_pTimeProgressBack != NULL)
    {
        m_pTimeProgressBack->setPosition(ccp(95.0f, 15.0f));
        addChild(m_pTimeProgressBack);
    }
    
    // 初始化等级背景 : 第六层使用特殊的背景
//    if (m_pData->Floor_Type == F_Store)
//        m_pLevelBack = CCSprite::spriteWithSpriteFrameName("building_level_6.png");
//    else
        m_pLevelBack = CCSprite::spriteWithSpriteFrameName("building_level.png");
    
    if (m_pLevelBack != NULL)
    {
        m_pLevelBack->setPosition(ccp(-1.0f, 32.0f));
        addChild(m_pLevelBack);
    }
    
    // 初始化进度条
    m_pTimeProgress = CCProgressTimer::progressWithFile("build_progress-hd.png");
    if (m_pTimeProgress != NULL)
    {
        m_pTimeProgress->setPosition(95.0f, 15.0f);
        m_pTimeProgress->setType(kCCProgressTimerTypeHorizontalBarLR);
        m_pTimeProgress->setPercentage(50.0f);
        
        const Floor_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFloor(m_pData->Floor_Type, m_pData->Floor_Level);
        if (pData != NULL)
        {
            float fPercent = 1.0f * m_pData->Floor_HarvestTime / pData->Floor_HarvestTime * 100.0f;
            m_pTimeProgress->setPercentage(100.0f - fPercent);
            
            CCProgressTo* pProgressTo = CCProgressTo::actionWithDuration(m_pData->Floor_HarvestTime, 100.0f);
            CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::ProcessHavestMoneyAction));
            if (pProgressTo != NULL && pFunc != NULL)
                m_pTimeProgress->runAction(CCSequence::actions(pProgressTo, pFunc, NULL));
        }
        
        addChild(m_pTimeProgress);
    }
    
    // 初始化沙漏
    m_pSandy = CCSprite::spriteWithSpriteFrameName("build_sandy.png");
    if (m_pSandy != NULL)
    {
        //CCPoint pos = m_pBackImage->getPosition();
        m_pSandy->setPosition(ccp(125.0f, 19.0f));
        addChild(m_pSandy);
    }
    
    // 初始化五角星
    for (int i = 0; i < FLOOR_LEVEL_STAR_NUM; i++)
    {
        m_pFloorLevelStar[i] = CCSprite::spriteWithSpriteFrameName("star.png");
        if (m_pFloorLevelStar[i] != NULL)
        {
            m_pFloorLevelStar[i]->setPosition(ccp(14.0f * i - 6.0f, 18.0f));
            addChild(m_pFloorLevelStar[i]);
            
            // 根据等级显示
            if (i >= m_pData->Floor_Level)
                m_pFloorLevelStar[i]->setIsVisible(false);
        }
    }
    
    // 初始化星星遮罩
    m_pStarMask = CCSprite::spriteWithSpriteFrameName("star_mask.png");
    if (m_pStarMask != NULL)
    {
        m_pStarMask->setOpacity(0);
        addChild(m_pStarMask);
    }
    
    // 初始化人物动画
    char szTemp[32] = {0};
    sprintf(szTemp, "npc1_floor%d_stay1.png", m_pData->Floor_Type);
    m_pFloorNpcAnim = CCSprite::spriteWithSpriteFrameName(szTemp);
    if (m_pFloorNpcAnim != NULL)
    {
        if (m_pData->Floor_Type == F_Cardgame || m_pData->Floor_Type == F_Gamble)
            m_pFloorNpcAnim->setPosition(ccp(-80.0f, -25.0f));
        else if (m_pData->Floor_Type == F_Bar)
            m_pFloorNpcAnim->setPosition(ccp(80.0f, -25.0f));
        else
            m_pFloorNpcAnim->setPosition(ccp(0.0f, -25.0f));
        
        addChild(m_pFloorNpcAnim);
        
        // 使npc行动
        ProcessFloorNpcAction(NULL);
    }
    
    // 初始化泡泡 - 后
    m_pBubbleBack = CCSprite::spriteWithSpriteFrameName("bubble_back.png");
    if (m_pBubbleBack != NULL)
    {
        m_pBubbleBack->setScale(0.0f);
        m_pBubbleBack->setPosition(ccp(80.0f, -20.0f));
        addChild(m_pBubbleBack);
    }
    
    // 初始化泡泡 - 钱袋
    m_pBubbleMoney = CCSprite::spriteWithSpriteFrameName("bubble_money.png");
    if (m_pBubbleMoney != NULL)
    {
        m_pBubbleMoney->setScale(0.0f);
        m_pBubbleMoney->setPosition(ccp(80.0f, -20.0f));
        addChild(m_pBubbleMoney);
    }
    
    // 初始化泡泡 - 前
    m_pBubbleFront = CCSprite::spriteWithSpriteFrameName("bubble_front.png");
    if (m_pBubbleFront != NULL)
    {
        m_pBubbleFront->setScale(0.0f);
        m_pBubbleFront->setPosition(ccp(80.0f, -20.0f));
        addChild(m_pBubbleFront);
    }
    
    // 初始化收获金钱图层
    m_pMoneyHavestMoney = CCLayer::node();
    if (m_pMoneyHavestMoney != NULL)
    {
        // 初始化金币加号
        m_pMoneyIconAdd = CCSprite::spriteWithSpriteFrameName("money_icon_add.png");
        if (m_pMoneyIconAdd != NULL)
        {
            m_pMoneyIconAdd->setOpacity(0);
            m_pMoneyIconAdd->setPosition(ccp(-5.0f, 8.0f));
            m_pMoneyHavestMoney->addChild(m_pMoneyIconAdd);
        }
        
        // 初始化金币数字
        m_pMoneyAddNum = CCLabelAtlas::labelWithString("10000000", "number_type_7-hd.png", 9, 15, '.');
        if (m_pMoneyAddNum != NULL)
        {
            m_pMoneyAddNum->setOpacity(0);
            m_pMoneyHavestMoney->addChild(m_pMoneyAddNum);
        }
        
        // 添加到场景
        m_pMoneyHavestMoney->setAnchorPoint(ccp(0.0f, 0.0f));
        addChild(m_pMoneyHavestMoney);
    }
    
    return true;
}

/************************************************************************/
#pragma mark - Floor类处理站台楼层点击事件函数
/************************************************************************/
void Floor::ProcessPlatformTouchEvent(CCTouch* pTouch, CCPoint mousePoint)
{
    // 设置火车动画
    for (int i = 0; i < PLATFORM_TRAIN_NUM; i++)
    {
        // 如果火车已经在动，则掠过
        if (m_pPlatformTrain[i]->numberOfRunningActions() != 0)
            continue;
        
        // 计算火车终点位置
        CCPoint pos = m_pPlatformTrain[i]->getPosition();
        pos.x += 1200.0f;
        
        float fTime = (pos.x - m_pPlatformTrain[i]->getPosition().x) / PLATFORM_TRAIN_SPEED;
        
        // 创建离开action
        CCMoveTo* pMoveTo1 = CCMoveTo::actionWithDuration(fTime, pos);
        CCMoveTo* pMoveTo2 = CCMoveTo::actionWithDuration(0.01f, ccp(-i * 328.0f - 330.0f, pos.y));
        CCMoveTo* pMoveTo3 = CCMoveTo::actionWithDuration(180.0f / PLATFORM_TRAIN_SPEED, ccp(-i * 328.0f - 150.0f, pos.y));
        
        if (pMoveTo1 != NULL && pMoveTo2 != NULL && pMoveTo3 != NULL)
            m_pPlatformTrain[i]->runAction(CCSequence::actions(pMoveTo1, pMoveTo2, pMoveTo3, NULL));
    }
    
    // 打开界面
    KNUIFunction::GetSingle()->OpenFrameByJumpTypeSpecial(TRAIN_PLATFORM_FRAME_ID, mousePoint);
}

/************************************************************************/
#pragma mark - Floor类处理收获楼层点击事件函数
/************************************************************************/
void Floor::ProcessHarvestFloorTouchEvent(CCTouch* pTouch, CCPoint mousePoint)
{
    // 新手引导
    if (PlayerDataManage::m_GuideMark)
    {
        if (GameGuide::GetSingle()->GetFloorSubGuideStep() == 0)
        {
            GameGuide::GetSingle()->FinishOneFloorGuide();
            return;
        }
        else if (GameGuide::GetSingle()->GetFloorSubGuideStep() == 2)
            GameGuide::GetSingle()->FinishOneFloorGuide();
    }
    
    if (m_bIsFloorCanHarvest && !m_bIsFloorInAction)
    {
        CCPoint point = convertTouchToNodeSpace(pTouch);
        
        CCPoint pos = m_pBubbleMoney->getPosition();
        CCSize size = m_pBubbleMoney->getContentSize();
        
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
        {
            // 计算世界坐标
            m_MoneyTouchPos = pTouch->locationInView(pTouch->view());
            m_MoneyTouchPos = CCDirector::sharedDirector()->convertToGL(m_MoneyTouchPos);
            
            //m_bIsFloorInAction = true;
            
            ServerDataManage::ShareInstance()->RequestFloorHarvest(m_pData->Floor_Type);
        }
        
        return;
    }
    
    // 打开升级界面
    KNUIFunction::GetSingle()->OpenFrameByJumpTypeSpecial(BUILDING_LEVELUP_FRAME_ID, mousePoint);
}

/************************************************************************/
#pragma mark - Floor类楼层npc行动函数
/************************************************************************/
void Floor::ProcessFloorNpcAction(CCObject* pObject)
{
    // 辅助变量
    char szTemp[32] = {0};
    
    // 特殊楼层npc，不会移动，只有一种状态
    if (m_pData->Floor_Type == F_Cardgame || m_pData->Floor_Type == F_Bar || m_pData->Floor_Type == F_Gamble)
    {
        // 初始化动画
        sprintf(szTemp, "npc1_floor%d_stay", m_pData->Floor_Type);
        CCAnimation* pAnim = CreateAnimation(szTemp, 0.3f);
        if (pAnim != NULL)
        {
            CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnim);
            if (pAnimate != NULL)
            {
                // 永久播放
                CCRepeatForever* pForever = CCRepeatForever::actionWithAction(pAnimate);
                if (pForever != NULL)
                    m_pFloorNpcAnim->runAction(pForever);
            }
        }
        
        return;
    }
    
    // 更具状态，随即下一个状态
    if (m_eNpcState != NPC_STAY)
        m_eNpcState = NPC_STAY;
    else
        m_eNpcState = static_cast<FLOOR_NPC_ACTION_STATE>(rand() % NPC_MAKE_STATE + 1);
    
    // 更具状态，创建行动
    switch (m_eNpcState) {
        case NPC_STAY:
        {
            // 停止一切行动
            m_pFloorNpcAnim->stopAllActions();
            
            // 一层和其他不同，区分
            if (m_pData->Floor_Type == F_Player)
                sprintf(szTemp, "npc%d_floor%d_stay", PlayerDataManage::m_PlayerFistGameProfession, m_pData->Floor_Type);
            else
                sprintf(szTemp, "npc1_floor%d_stay", m_pData->Floor_Type);
            
            // 创建动画
            CCAnimation* pAnimation = CreateAnimation(szTemp, 0.3f);
            if (pAnimation != NULL)
            {
                CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnimation);
                if (pAnimate != NULL)
                {
                    CCRepeatForever* pForever = CCRepeatForever::actionWithAction(pAnimate);
                    if (pForever != NULL)
                        m_pFloorNpcAnim->runAction(pForever);
                }
            }
            
            // 创建下一次调用函数action
            CCDelayTime* pTime = CCDelayTime::actionWithDuration(2.0f);
            CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::ProcessFloorNpcAction));
            if (pTime != NULL && pFunc != NULL)
                m_pFloorNpcAnim->runAction(CCSequence::actions(pTime, pFunc, NULL));
        }
            break;
        case NPC_IDOL_LEFT:
        case NPC_IDOL_RIGHT:
        {
            // 停止一切行动
            m_pFloorNpcAnim->stopAllActions();
            
            // 计算下一次行走目标
            CCPoint npcPos = m_pFloorNpcAnim->getPosition();
            CCPoint newPos(0.0f, 0.0f);
            
            newPos.x = rand() % 200 - 80;
            newPos.y = npcPos.y;
            
            // 计算行走时间和朝向
            float fTime = 0.0f;
            if (newPos.x > npcPos.x)
            {
                fTime = (newPos.x - npcPos.x) / 20;
                m_pFloorNpcAnim->setFlipX(true);
            }
            else
            {
                fTime = (npcPos.x - newPos.x) / 20;
                m_pFloorNpcAnim->setFlipX(false);
            }
            
            // 创建行动
            CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(fTime, newPos);
            CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::ProcessFloorNpcAction));
            if (pMoveTo != NULL && pFunc != NULL)
                m_pFloorNpcAnim->runAction(CCSequence::actions(pMoveTo, pFunc, NULL));
            
            // 创建动画
            if (m_pData->Floor_Type == F_Player)
                sprintf(szTemp, "npc%d_floor%d_move", PlayerDataManage::m_PlayerFistGameProfession, m_pData->Floor_Type);
            else
                sprintf(szTemp, "npc1_floor%d_move", m_pData->Floor_Type);
            
            CCAnimation* pAnimation = CreateAnimation(szTemp, 0.2f);
            if (pAnimation != NULL)
            {
                CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnimation);
                if (pAnimate != NULL)
                {
                    CCRepeatForever* pForever = CCRepeatForever::actionWithAction(pAnimate);
                    if (pForever != NULL)
                        m_pFloorNpcAnim->runAction(pForever);
                }
            }
        }
            break;
        default:
            break;
    }
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 处理开张标记行动函数
/************************************************************************/
void Floor::ProcessOpenMarkAction(CCObject* pObject)
{
    // 计算角度
    if (m_fOpenMarkAngle == 45.0f)
        m_fOpenMarkAngle = -45.0f;
    else
        m_fOpenMarkAngle = 45.0f;
    
    // 创建行动
    CCRotateTo* pRotateTo = CCRotateTo::actionWithDuration(0.3f, m_fOpenMarkAngle);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::ProcessOpenMarkAction));
    if (pRotateTo != NULL && pFunc != NULL)
        m_pGlassMark->runAction(CCSequence::actions(pRotateTo, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 处理星星标记闪烁函数
/************************************************************************/
void Floor::ProcessStarLightAction(CCObject* pObject)
{
    CCFadeIn* pFadeIn = CCFadeIn::actionWithDuration(0.5f);
    CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(0.5f);
    if (pFadeIn != NULL && pFadeOut != NULL)
        m_pStarMask->runAction(CCSequence::actions(pFadeIn, pFadeOut, NULL));
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 处理钱袋动作函数
/************************************************************************/
void Floor::ProcessBubbleMoneyAction(CCObject* pObject)
{
    CCPoint pos = m_pBubbleMoney->getPosition();
    
    CCMoveTo* pMoveUp = CCMoveTo::actionWithDuration(1.0f, ccp(pos.x, -15.0f));
    CCMoveTo* pMoveDown = CCMoveTo::actionWithDuration(1.0f, ccp(pos.x, -25.0f));
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::ProcessBubbleMoneyAction));
    if (pMoveUp != NULL && pMoveDown != NULL & pFunc != NULL)
        m_pBubbleMoney->runAction(CCSequence::actions(pMoveUp, pMoveDown, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 处理收获时间函数
/************************************************************************/
void Floor::ProcessHavestTimeAction(CCObject* pObject)
{
    if (!m_bIsTimeRefresh)
        return;
    
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(BUILDING_LEVELUP_FRAME_ID);
    if (pFrame != NULL)
    {
        int iHour = m_pData->Floor_HarvestTime / 60 / 60;
        int iMinute = m_pData->Floor_HarvestTime / 60 - iHour * 60;
        int iSecond = m_pData->Floor_HarvestTime - iHour * 60 * 60 - iMinute * 60;
        
        char szHour[16] = {0};
        char szMinute[16] = {0};
        char szSecond[16] = {0};
        
        if (iHour / 10 == 0)
            sprintf(szHour, "0%d", iHour);
        else        
            sprintf(szHour, "%d", iHour);
        
        if (iMinute / 10 == 0)
            sprintf(szMinute, "0%d", iMinute);
        else
            sprintf(szMinute, "%d", iMinute);
        
        if (iSecond / 10 == 0)
            sprintf(szSecond, "0%d", iSecond);
        else
            sprintf(szSecond, "%d", iSecond);
        
        // 刷新 : 时
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20018));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szHour);
        
        // 刷新 : 分
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20019));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szMinute);
        
        // 刷新 : 秒
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20020));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szSecond);
        
        // 刷新进度条
        KNProgress* pProgress = dynamic_cast<KNProgress*>(pFrame->GetSubUIByID(70000));
        if (pProgress != NULL)
        {
            const Floor_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFloor(m_pData->Floor_Type, m_pData->Floor_Level);
            if (pData != NULL)
            {
                float fPercent = 1.0f * m_pData->Floor_HarvestTime / pData->Floor_HarvestTime * 100.0f;
                pProgress->SetPercent(100.0f - fPercent);
            }
        }
    }
    
    // 时间到了
    if (m_pData->Floor_HarvestTime <= 0)
    {
        ProcessHavestMoneyAction(NULL);
        
        m_bIsTimeRefresh = false;
    }
    
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(1.0f);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::ProcessHavestTimeAction));
    
    if (pTime != NULL && pFunc != NULL)
        runAction(CCSequence::actions(pTime, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 处理金钱包动作函数
/************************************************************************/
void Floor::ProcessHavestMoneyAction(CCObject* pObject)
{
    if (m_pBubbleBack->getScale() == 0.0f)
    {
        m_bIsFloorCanHarvest = true;
        
        Layer_Game::GetSingle()->SetMenuItemState(2, true);
        
        CCScaleTo* pScaleTo1 = CCScaleTo::actionWithDuration(0.5f, 1.0f);
        CCScaleTo* pScaleTo2 = CCScaleTo::actionWithDuration(0.5f, 1.0f);
        CCScaleTo* pScaleTo3 = CCScaleTo::actionWithDuration(0.5f, 1.0f);
        CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::ProcessBubbleMoneyAction));
        if (pScaleTo1 != NULL && pScaleTo2 != NULL && pScaleTo3 != NULL)
        {
            m_pBubbleBack->runAction(pScaleTo1);
            m_pBubbleMoney->runAction(pScaleTo2);
            m_pBubbleFront->runAction(CCSequence::actions(pScaleTo3, pFunc, NULL));
        }
    }
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 处理提示动作函数
/************************************************************************/
void Floor::ProcessHintAction(CCObject* pObject)
{
    if (m_pHint == NULL)
        return;
    
    // 光芒动画
    CCSprite* pLighting = dynamic_cast<CCSprite*>(m_pHint->getChildByTag(HINT_LIGHTING));
    if (pLighting != NULL)
    {
        pLighting->setRotation(0.0f);
        CCRotateBy* pRotateBy = CCRotateBy::actionWithDuration(3.4f, -360.0f);
        CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::ProcessHintAction));
        pLighting->runAction(CCSequence::actions(pRotateBy, pFunc, NULL));
    }
    
    // 提示动画
    CCSprite* pHint = dynamic_cast<CCSprite*>(m_pHint->getChildByTag(HINT_ITEM));
    if (pHint != NULL)
    {
        // 一层和五层一样处理
        if (m_pData->Floor_Type == F_Player || m_pData->Floor_Type == F_Platform)
        {
            CCScaleTo* pScaleTo1 = CCScaleTo::actionWithDuration(0.4f, 1.3f);
            CCRotateTo* pRotateTo1 = CCRotateTo::actionWithDuration(0.4f, -30.0f);
            CCRotateTo* pRotateTo2 = CCRotateTo::actionWithDuration(0.4f, 0.0f);
            CCRotateTo* pRotateTo3 = CCRotateTo::actionWithDuration(0.4f, -30.0f);
            CCRotateTo* pRotateTo4 = CCRotateTo::actionWithDuration(0.4f, 0.0f);
            CCScaleTo* pScaleTo2 = CCScaleTo::actionWithDuration(0.4f, 1.0f);
        
            pHint->runAction(pScaleTo1);
            pHint->runAction(CCSequence::actions(pRotateTo1, pRotateTo2, pRotateTo3, pRotateTo4, pScaleTo2, NULL));
        }
        else if (m_pData->Floor_Type == F_Follower)
        {
            CCMoveTo* pDown = CCMoveTo::actionWithDuration(1.7f, ccp(-60.0f, -30.0f - 5.0f));
            CCMoveTo* pUp = CCMoveTo::actionWithDuration(1.7f, ccp(-60.0f, -30.0f + 5.0f));
            
            pHint->runAction(CCSequence::actions(pDown, pUp, NULL));
        }
    }
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 处理new动画函数
/************************************************************************/
void Floor::ProcessNewAction(CCObject* pObject)
{
    CCSprite* pNew = dynamic_cast<CCSprite*>(pObject);
    if (pNew != NULL)
    {
        CCAnimation* pAnim = CreateAnimation("sword_new_", 0.05f);
        CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnim, false);
        CCDelayTime* pTime = CCDelayTime::actionWithDuration(1.0f);
        CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Floor::ProcessNewAction));
        
        pNew->runAction(CCSequence::actions(pAnimate, pTime, pFuncN, NULL));
    }
}

/************************************************************************/
#pragma mark - Floor类建造步骤1
/************************************************************************/
void Floor::BuildingStep1()
{
    // 时间
    float fTime = 1.0f;
    
    // 隐藏待建楼层
    CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(fTime);
    if (pFadeOut != NULL)
        m_pStandByImage->runAction(pFadeOut);
    
    // 创建刷子动画
    CCAnimation* pAnim = CreateAnimation("brush_", 0.08f);
    if (pAnim != NULL)
    {
        CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnim);
        if (pAnimate != NULL)
        {
            CCRepeatForever* pForever = CCRepeatForever::actionWithAction(pAnimate);
            if (pForever != NULL)
                m_pBrushAnim->runAction(pForever);
        }
    }
    
    // 开始进度计算
    CCProgressTo* pProgressTo = CCProgressTo::actionWithDuration(fTime * 5.0f, 100.0f);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::BuildingStep2));
    if (pProgressTo != NULL && pFunc != NULL)
        m_pBuildingProgress->runAction(CCSequence::actions(pProgressTo, pFunc, NULL));
    
    // 进度到三分之一，隐藏第一个建造背景
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(fTime * 5.0f / 3.0f);
    pFadeOut = CCFadeOut::actionWithDuration(fTime);
    if (pTime != NULL && pFadeOut != NULL)
        m_pBuildingBack2->runAction(CCSequence::actions(pTime, pFadeOut, NULL));
    // 进度到三分之二，隐藏第二个建造背景
    pTime = CCDelayTime::actionWithDuration(fTime * 5.0f / 3.0f * 2.0f);
    pFadeOut = CCFadeOut::actionWithDuration(fTime);
    if (pTime != NULL && pFadeOut != NULL)
        m_pBuildingBack1->runAction(CCSequence::actions(pTime, pFadeOut, NULL));
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 建造步骤2
/************************************************************************/
void Floor::BuildingStep2()
{
    // 关闭步骤1的图片
    m_pStandByImage->setIsVisible(false);
    
    m_pBuildingBack1->setIsVisible(false);
    m_pBuildingBack2->setIsVisible(false);
    
    m_pBuildingProgress->setIsVisible(false);
    m_pBulidingProgressBack->setIsVisible(false);
    
    // 放烟花
    Layer_Building::ShareInstance()->SetFrameEnable(true);
    Layer_Building::ShareInstance()->SetFrameType(m_pData->Floor_Type);
    Layer_Building::ShareInstance()->schedule(schedule_selector(Layer_Building::PlayerFrame), 0.3f);
    
    // 转动旋转标记
    ProcessOpenMarkAction(m_pGlassMark);
    
    // 隐藏开张标记
    CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(1.0f);
    if (pFadeOut != NULL)
        m_pGlassMark->runAction(pFadeOut);
    
    // 隐藏刷子
    pFadeOut = CCFadeOut::actionWithDuration(0.1f);
    if (pFadeOut != NULL)
        m_pBrushAnim->runAction(pFadeOut);
    
    // 打开镜子门-左
    CCPoint pos = m_pGlassLeft->getPosition();
    CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(3.0f, ccp(-40.0f, pos.y));
    if (pMoveTo != NULL)
        m_pGlassLeft->runAction(pMoveTo);
    // 打开镜子门-右
    pos = m_pGlassRight->getPosition();
    pMoveTo = CCMoveTo::actionWithDuration(3.0f, ccp(80.0f, pos.y));
    if (pMoveTo != NULL)
        m_pGlassRight->runAction(pMoveTo);
    
    // 隐藏所有镜子
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(3.0f);
    pFadeOut = CCFadeOut::actionWithDuration(1.0f);
    if (pTime != NULL && pFadeOut != NULL)
        m_pGlassLeft->runAction(CCSequence::actions(pTime, pFadeOut, NULL));
    
    pTime = CCDelayTime::actionWithDuration(3.0f);
    pFadeOut = CCFadeOut::actionWithDuration(1.0f);
    if (pTime != NULL && pFadeOut != NULL)
        m_pGlassLeftLeft->runAction(CCSequence::actions(pTime, pFadeOut, NULL));
    
    pTime = CCDelayTime::actionWithDuration(3.0f);
    pFadeOut = CCFadeOut::actionWithDuration(1.0f);
    if (pTime != NULL && pFadeOut != NULL)
        m_pGlassRight->runAction(CCSequence::actions(pTime, pFadeOut, NULL));
    
    pTime = CCDelayTime::actionWithDuration(3.0f);
    pFadeOut = CCFadeOut::actionWithDuration(1.0f);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::BuildingStep3));
    if (pTime != NULL && pFadeOut != NULL && pFunc)
        m_pGlassRightRight->runAction(CCSequence::actions(pTime, pFadeOut, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 建造步骤3
/************************************************************************/
void Floor::BuildingStep3()
{
    // 关闭烟花
    Layer_Building::ShareInstance()->SetFrameEnable(false);
    
    if (m_pData->Floor_Type <= F_Platform)
    {
        BuildingFinish();
        return;
    }
    
    // 找到升级的星星
    CCSprite* pLevelStar = NULL;
    for (int i = 0; i < FLOOR_LEVEL_STAR_NUM; i++)
    {
        if (m_pFloorLevelStar[i]->getIsVisible() == false)
        {
            pLevelStar = m_pFloorLevelStar[i];
            break;
        }
    }
    
    // 如果没有能升级的星星，则退出
    if (pLevelStar == NULL)
        return;
    
    // 设置星星标记
    m_pStarMask->setPosition(pLevelStar->getPosition());
    pLevelStar->setIsVisible(true);
    
    // 星星小动画
    pLevelStar->setScale(3.0f);
    CCScaleTo* pScaleTo = CCScaleTo::actionWithDuration(0.3f, 1.0f);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::BuildingFinish));
    if (pScaleTo != NULL && pFunc != NULL)
        pLevelStar->runAction(CCSequence::actions(pScaleTo, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 完成建造
/************************************************************************/
void Floor::BuildingFinish()
{
    // 闪烁星星
    if (PlayerDataManage::m_GuideMark)
        GameGuide::GetSingle()->PrepareNextFloorGuide();
    
    if (m_pData->Floor_Type > F_Platform)
        ProcessStarLightAction(NULL);
    
    // 移出建造所需要的资源
    removeChild(m_pGlassLeftLeft, true);
    m_pGlassLeft->stopAllActions();
    removeChild(m_pGlassLeft, true);
    removeChild(m_pGlassRightRight, true);
    m_pGlassRight->stopAllActions();
    removeChild(m_pGlassRight, true);
    m_pGlassMark->stopAllActions();
    removeChild(m_pGlassMark, true);
    
    removeChild(m_pBuildingBack1, true);
    removeChild(m_pBuildingBack2, true);
    
    m_pBrushAnim->stopAllActions();
    removeChild(m_pBrushAnim, true);
    
    m_pStandByImage->stopAllActions();
    removeChild(m_pStandByImage, true);
    
    removeChild(m_pBulidingProgressBack, true);
    m_pBuildingProgress->stopAllActions();
    removeChild(m_pBuildingProgress, true);
    
    // 设置楼层操作标记
    m_bIsFloorInAction = false;
}

/************************************************************************/
#pragma mark - Floor类升级步骤1
/************************************************************************/
void Floor::LevelupStep1()
{
    // 转动旋转标记
    ProcessOpenMarkAction(m_pGlassMark);
    
    // 播放烟花
    Layer_Building::ShareInstance()->SetFrameEnable(true);
    Layer_Building::ShareInstance()->SetFrameType(m_pData->Floor_Type);
    Layer_Building::ShareInstance()->schedule(schedule_selector(Layer_Building::PlayerFrame), 0.3f);
    
    // 停留2秒，开始第二步骤
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(4.0f);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::LevelupStep2));
    if (pTime != NULL && pFunc != NULL)
        runAction(CCSequence::actions(pTime, pFunc, NULL));
    
    // 播放音效
    KNUIFunction::GetSingle()->PlayerEffect("upgrade.mp3");
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 升级步骤2
/************************************************************************/
void Floor::LevelupStep2()
{
    // 停止烟花
    Layer_Building::ShareInstance()->SetFrameEnable(false);
    
    // 隐藏开张标记
    CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(0.5f);
    if (pFadeOut != NULL)
        m_pGlassMark->runAction(pFadeOut);
    
    // 打开镜子门-左
    CCPoint pos = m_pGlassLeft->getPosition();
    CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(3.0f, ccp(-40.0f, pos.y));
    if (pMoveTo != NULL)
        m_pGlassLeft->runAction(pMoveTo);
    // 打开镜子门-右
    pos = m_pGlassRight->getPosition();
    pMoveTo = CCMoveTo::actionWithDuration(3.0f, ccp(80.0f, pos.y));
    if (pMoveTo != NULL)
        m_pGlassRight->runAction(pMoveTo);
    
    // 隐藏所有镜子
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(3.0f);
    pFadeOut = CCFadeOut::actionWithDuration(1.0f);
    if (pTime != NULL && pFadeOut != NULL)
        m_pGlassLeft->runAction(CCSequence::actions(pTime, pFadeOut, NULL));
    
    pTime = CCDelayTime::actionWithDuration(3.0f);
    pFadeOut = CCFadeOut::actionWithDuration(1.0f);
    if (pTime != NULL && pFadeOut != NULL)
        m_pGlassLeftLeft->runAction(CCSequence::actions(pTime, pFadeOut, NULL));
    
    pTime = CCDelayTime::actionWithDuration(3.0f);
    pFadeOut = CCFadeOut::actionWithDuration(1.0f);
    if (pTime != NULL && pFadeOut != NULL)
        m_pGlassRight->runAction(CCSequence::actions(pTime, pFadeOut, NULL));
    
    pTime = CCDelayTime::actionWithDuration(3.0f);
    pFadeOut = CCFadeOut::actionWithDuration(1.0f);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::LevelupStep3));
    if (pTime != NULL && pFadeOut != NULL && pFunc)
        m_pGlassRightRight->runAction(CCSequence::actions(pTime, pFadeOut, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 升级步骤3
/************************************************************************/
void Floor::LevelupStep3()
{
    // 为了数据统一，这里再次刷新下星
    for (int i = 0; i < FLOOR_LEVEL_STAR_NUM; i++)
    {
        m_pFloorLevelStar[i]->setIsVisible(true);
        
        // 因为走到这里，说明服务器已经通过验证了，等级已经添加上去了，所以需要减1,来表现新等级的效果
        if (i >= m_pData->Floor_Level - 1)
            m_pFloorLevelStar[i]->setIsVisible(false);
    }
    
    // 找到升级的星星
    CCSprite* pLevelStar = NULL;
    for (int i = 0; i < FLOOR_LEVEL_STAR_NUM; i++)
    {
        if (m_pFloorLevelStar[i]->getIsVisible() == false)
        {
            pLevelStar = m_pFloorLevelStar[i];
            break;
        }
    }
    
    // 如果没有能升级的星星，则退出
    if (pLevelStar == NULL)
        return;
    
    // 设置星星标记
    m_pStarMask->setPosition(pLevelStar->getPosition());
    pLevelStar->setIsVisible(true);
    
    // 星星小动画
    pLevelStar->setScale(3.0f);
    CCScaleTo* pScaleTo = CCScaleTo::actionWithDuration(0.3f, 1.0f);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Floor::LevelupFinish));
    if (pScaleTo != NULL && pFunc != NULL)
        pLevelStar->runAction(CCSequence::actions(pScaleTo, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Floor类回调函数 : 完成升级
/************************************************************************/
void Floor::LevelupFinish()
{
    // 闪烁星星
    ProcessStarLightAction(NULL);
    
    // 移出建造所需要的资源
    removeChild(m_pGlassLeftLeft, true);
    m_pGlassLeft->stopAllActions();
    removeChild(m_pGlassLeft, true);
    removeChild(m_pGlassRightRight, true);
    m_pGlassRight->stopAllActions();
    removeChild(m_pGlassRight, true);
    m_pGlassMark->stopAllActions();
    removeChild(m_pGlassMark, true);
    
    // 设置楼层操作标记
    m_bIsFloorInAction = false;
}