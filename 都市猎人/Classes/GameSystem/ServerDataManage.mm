//
//  ServerDataManage.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-4-25.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "ServerDataManage.h"
#include "SystemDataManage.h"
#include "PlayerDataManage.h"
#import "ASIFormDataRequest.h"
#import "NetManager.h"
#include "Follower.h"
#include "KNUIFunction.h"
#include "Layer_Building.h"
#include "Layer_Enemy.h"
#include "Layer_Follower.h"
#include "Layer_Block.h"
#include "Layer_Game.h"
#include "Layer_BattleResult.h"
#include "GameController.h"
USING_NS_CC;

ServerDataManage * ServerDataManage::m_Instance = NULL;

/****************************************************/
#pragma mark 初始化
/****************************************************/
ServerDataManage * ServerDataManage::ShareInstance()
{
    if(m_Instance == NULL)
    {
        m_Instance = new ServerDataManage();
        m_Instance -> Initialize();
    }
    return m_Instance;
}

void ServerDataManage::Initialize()
{
    m_ChoiceMapIndex = 0;
    m_BattleInfo = NULL;
    m_TrainingFollower = NULL;
}
/****************************************************/
#pragma mark -
#pragma mark -
/****************************************************/



/*********************************************************/
#pragma mark 关于充值
/*********************************************************/
void ServerDataManage::RequestPayID()
{
    [[NetManager GetSingle] PayIDReqest];
}

void ServerDataManage::RequestPayIDSuccess(const char * PayID)
{

}
/*********************************************************/
#pragma mark-
/*********************************************************/



/****************************************************/
#pragma mark 关于登陆
/****************************************************/
void ServerDataManage::RequestPlayer91Login()
{
    [[NetManager GetSingle] Player91Login];
}

void ServerDataManage::RequestPlayerInfo()
{
    [[NetManager GetSingle] PlayerInfoReqest];
}

void ServerDataManage::RequestEnterGame()
{
    [[NetManager GetSingle] GameLoginReqest];
}
/****************************************************/
#pragma mark -
#pragma mark -
/****************************************************/


/****************************************************/
#pragma mark 关于引导
/****************************************************/
void ServerDataManage::RequestGuideDoneReqest(int step)
{
    [[NetManager GetSingle] GuideDoneReqest:step];
}
/****************************************************/
#pragma mark -
#pragma mark -
/****************************************************/



/*********************************************************/
#pragma mark 关于好友请求
/*********************************************************/
void ServerDataManage::RequestApplyList()
{
    [[NetManager GetSingle] ApplyListRequest];
}
/*********************************************************/
#pragma mark -
/*********************************************************/



/*********************************************************/
#pragma mark 关于小弟队伍的交互
/*********************************************************/
void ServerDataManage::RequestReferMyFollowerTeam()
{
    [[NetManager GetSingle] ReferMyFollowerTeamRequest];
}
/*********************************************************/
#pragma mark-
#pragma mark-
/*********************************************************/



/*********************************************************/
#pragma mark 关于小弟的出售
/*********************************************************/
#pragma mark 请求出售小弟
void ServerDataManage::RequestSellFollower()
{
    [[NetManager GetSingle] SellFollowerRequest];
}

#pragma mark 出售小弟成功
void ServerDataManage::RequestSellFollowerSucceed(int price)
{
    //增加金钱
    PlayerDataManage::m_PlayerMoney += price;
    //刷新界面上的金钱
    Layer_Game::GetSingle() -> UpdateMenuMoney();
    
    //从背包中移除被出售的小弟
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
        {
            set<Follower*> fireList = pList->GetFireList();
            set<Follower*>::iterator iter;
            for (iter = fireList.begin(); iter != fireList.end(); iter++)
                PlayerDataManage::ShareInstance() -> RemoveFollowerFromDepot(*iter);
            
            pList->clearFireList();
            pList->RefreshFollowerList();
        }
    }
}
/*********************************************************/
#pragma mark-
#pragma mark-
/*********************************************************/



/****************************************************/
#pragma mark 关于小弟商店
#pragma mark -
/****************************************************/
#pragma mark 请求小弟商品信息
void ServerDataManage::RequestWareFollower()
{   
    [[NetManager GetSingle] WareFollowerRequest];
}

#pragma mark 请求刷新小弟商品信息
void ServerDataManage::RequestUpdateWareFollower(int type)
{
    [[NetManager GetSingle] UpdateWareFollowerRequest:type];
}

#pragma mark 请求购买小弟
void ServerDataManage::RequestBuyFollower(int type, int followerIndex)
{
    Follower * pFollower = m_WareFollowerList[type][followerIndex];
    if(! pFollower)
    {
        ShowMessage(BType_Firm, "前端出错，购买的这个小弟在前端不存在");
    }
    
    //如果金钱可以购买
    if(PlayerDataManage::m_PlayerMoney >= pFollower -> GetData() -> Follower_Price)
    {
        [[NetManager GetSingle] BuyFollowerRequest:type fID:followerIndex];
    }
    //如果不可以购买
    else 
    {
        ShowMessage(BType_Firm, "当前金钱不够，无法购买该小弟");
    }
}

#pragma mark 购买小弟成功
void ServerDataManage::RequestBuyFollowerSucceed(int type, int followerIndex, int followerserverID, int LevelUpExp, int BeAbsorbExp, int Hp, int Attack, int Defense, int ComeBack)
{
    Follower * pFollower = m_WareFollowerList[type][followerIndex];
    if(! pFollower)
    {
        ShowMessage(BType_Firm, "前端出错，购买的这个小弟在前端不存在");
    }
    
    pFollower -> GetData() -> Follower_Experience = 0;
    pFollower -> GetData() -> Follower_Level = 1;
    pFollower -> GetData() -> Follower_SkillLevel = 1;
    pFollower -> GetData() -> Follower_LevelUpExperience = LevelUpExp;
    pFollower -> GetData() -> Follower_BeAbsorbExperience = BeAbsorbExp;
    pFollower -> GetData() -> Follower_HP = Hp;
    pFollower -> GetData() -> Follower_Attack = Attack;
    pFollower -> GetData() -> Follower_Defense = Defense;
    pFollower -> GetData() -> Follower_Comeback = ComeBack;
    pFollower -> GetData() -> Follower_ServerID = followerserverID;
    
    //扣除购买金钱
    PlayerDataManage::m_PlayerMoney -= pFollower -> GetData() -> Follower_Price;
    //刷新界面上的金钱
    Layer_Game::GetSingle() -> UpdateMenuMoney();
    
    //加入进背包中
    PlayerDataManage::ShareInstance() -> AddFollowerToDepot(pFollower);
    
    //商品列表中移除这个被购买了的小弟
    WareList::iterator it1 = m_WareFollowerList.find(type);
    map <int, Follower *>::iterator it2 = it1 -> second.find(followerIndex);
    it1 -> second.erase(it2);
    
    // 刷新小弟列表
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
            pList->RefreshFollowerList();
    }
    
    // 刷新背包数字
    KNUIFunction::GetSingle()->UpdatePlatformInfo();
    
    // 新手引导 : 完成购买引导
    if (PlayerDataManage::m_GuideMark)
        Layer_GameGuide::GetSingle()->FinishShopCurrentStep();
    
    // 提示购买成功
    ShowMessage(BType_Firm, "购买成功");
    
    // 打开小弟层提示
    KNUIFunction::GetSingle()->ShowFloorHint(F_Follower);
}

#pragma mark 向小弟商品列表中加入商品小弟
void ServerDataManage::AddWareFollower(int type, int index, Follower * F)
{
    WareList::iterator it1 = m_WareFollowerList.find(type);
    if(it1 != m_WareFollowerList.end())
    {
        map <int, Follower *>::iterator it2 = it1 -> second.find(index);
        if(it2 != it1 -> second.end())  
            return;
    }
    
    m_WareFollowerList[type][index] = F;
}

#pragma mark 清空小弟商品列表
void ServerDataManage::ClearWareFollower(int type)
{   
    WareList::iterator it1 = m_WareFollowerList.find(type);
    
    if(it1 != m_WareFollowerList.end())
    {
        for(map <int, Follower *>::iterator it2 = it1 -> second.begin(); it2 != it1 -> second.end(); )
        {
            it2 -> second -> release();
            it1 -> second.erase(it2 ++);
        }
        m_WareFollowerList.erase(it1);
    }
}
/****************************************************/
#pragma mark -
#pragma mark -
/****************************************************/



/*********************************************************/
#pragma mark 关于小弟的训练的交互
/*********************************************************/
void ServerDataManage::RequestTrainingFollower(Follower * follower, list <Follower *> * plist)
{
    [[NetManager GetSingle] TrainingFollowerRequest:follower Flist:plist];
    m_TrainingFollower = follower;
}

void ServerDataManage::RequestTrainingFollowerSucceed(int price, int exp, int level, int skillLevel, int levelupExp, int beabsorbExp, int hp, int attack, int defense, int comeback)
{
    //扣除训练的花费
    PlayerDataManage::m_PlayerMoney -= price;
    //刷新界面上的金钱
    Layer_Game::GetSingle() -> UpdateMenuMoney();
    
    m_TrainingFollower -> TrainingFollowerSucceed(exp, level, skillLevel, levelupExp, beabsorbExp, hp, attack, defense, comeback);
    m_TrainingFollower = NULL;
}
/****************************************************/
#pragma mark -
#pragma mark -
/****************************************************/



/****************************************************/
#pragma mark 关于小弟进化的交互
/****************************************************/
void ServerDataManage::RequestEvolvementFollower(Follower * follower, list <Follower *> * plist)
{
    [[NetManager GetSingle] EvolvementFollowerRequest:follower Flist:plist];
    m_EvolvementFollower = follower;
}

void ServerDataManage::RequestEvolvementFollowerSucceed(int price, int newID, int newcharacterID, int newserverID, int levelupExp, int beabsorbExp, int hp, int attack, int defense, int comeback)
{
    //扣除进化的花费
    PlayerDataManage::m_PlayerMoney -= price;
    //刷新界面上的金钱
    Layer_Game::GetSingle() -> UpdateMenuMoney();
    
    m_EvolvementFollower -> EvolvementFollowerSucceed(newID, newcharacterID, newserverID, levelupExp, beabsorbExp, hp, attack,defense, comeback);
    m_EvolvementFollower = NULL;
}
/****************************************************/
#pragma mark -
#pragma mark -
/****************************************************/




/****************************************************/
#pragma mark 关于楼层
#pragma mark -
/****************************************************/
#pragma mark 向服务器请求建造新的楼层
void ServerDataManage::RequestNewFloor(int floor_type)
{
    const FloorData * pFloorData = SystemDataManage::ShareInstance() -> GetData_ForFloor(floor_type, 1);
    
    if(pFloorData != NULL)
    {
        //如果楼层已经存在
        if(PlayerDataManage::ShareInstance() -> IsFloorExist(floor_type))
        {
            ShowMessage(BType_Firm, "数据出错，该楼层已经存在，无法再建造");
            return;
        }
        //如果等级条件不满足
        else if(PlayerDataManage::m_PlayerLevel < pFloorData -> Floor_Build_PlayerLevel)
        {
            ShowMessage(BType_Firm, "您的当前等级不足，无法建造新的楼层");
            return;
        }
        //如果金钱条件不满足
        else if(PlayerDataManage::m_PlayerMoney < pFloorData -> Floor_Build_PlayerMoney)
        {
            ShowMessage(BType_Firm, "您的当前金钱不足，无法建造新的楼层");
            return;
        }
        else 
        {
            [[NetManager GetSingle] NewFloorReqest:floor_type];
        }
    }
}

#pragma mark 请求建造新的楼层成功
void ServerDataManage::RequestNewFloorSuccess(int floor_type)
{
    const FloorData * pFloorData = SystemDataManage::ShareInstance() -> GetData_ForFloor(floor_type, 1);
    
    if (pFloorData != NULL)
    {
        //建造新楼层
        PlayerDataManage::ShareInstance() -> CreateNewFloor(floor_type, 1, pFloorData -> Floor_HarvestTime);
        
        //添加到场景
        KNUIFunction::GetSingle()->AddFloorToGameScene(floor_type);
        
        //扣除相应的金币
        PlayerDataManage::m_PlayerMoney -= pFloorData -> Floor_Build_PlayerMoney;
        
        //刷新界面上的金钱
        Layer_Game::GetSingle() -> UpdateMenuMoney();
    }
}


#pragma mark 向服务器请求升级楼层
void ServerDataManage::RequestLevelUpFloor(int floor_type)
{
    FloorData * pFloorData = PlayerDataManage::ShareInstance() -> GetMyFloor(floor_type);
    
    if (pFloorData != NULL)
    {
        // 是否已达最高等级
        const FloorData* pNextFloorData = SystemDataManage::ShareInstance()->GetData_ForFloor(floor_type, pFloorData->Floor_Level + 1);
        if (pNextFloorData == NULL)
        {
            ShowMessage(BType_Firm, "该楼层已达最高级!");
            return;
        }
        
        //如果等级条件不满足
        if(PlayerDataManage::m_PlayerLevel < pFloorData -> Floor_Upgrade_PlayerLevel)
        {
            ShowMessage(BType_Firm, "您的当前等级不足，无法升级该楼层");
            return;
        }
        //如果金钱条件不满足
        else if(PlayerDataManage::m_PlayerMoney < pFloorData -> Floor_Upgrade_PlayerMoney)
        {
            ShowMessage(BType_Firm, "您的当前金钱不足，无法升级该楼层");
            return;
        }
        else 
        {
            [[NetManager GetSingle] LevelUpFloorRequest:floor_type];
        }
    }
}

#pragma mark 请求升级楼层成功
void ServerDataManage::RequestLevelUpFloorSuccess(int floor_type, int floor_level, int floor_harvestTime, int floor_harvestMoney)
{
    FloorData * pFloorData = PlayerDataManage::ShareInstance() -> GetMyFloor(floor_type);
    if(pFloorData != NULL)
    {
        //扣除相应的金钱
        PlayerDataManage::m_PlayerMoney -= pFloorData -> Floor_Upgrade_PlayerMoney;
        
        //加上收获的金钱
        if(floor_harvestMoney != pFloorData -> Floor_HarvestMoney)
        {
            char info[128];
            sprintf(info, "升级类型为%d的楼层时，服务器的收获金钱与配置表中的收获金钱不一致\n", floor_type);
            ShowMessage(BType_Firm, info);
        }
        PlayerDataManage::m_PlayerMoney += pFloorData -> Floor_HarvestMoney;
        
        //刷新界面上的金钱
        Layer_Game::GetSingle() -> UpdateMenuMoney();
        
        // 升级楼层数据
        PlayerDataManage::ShareInstance() -> LevelUpFloor(floor_type, floor_level);
        
        // 升级楼层表现
        Layer_Building::ShareInstance() -> LevelupFloor(floor_type);
    }
}

#pragma mark 请求楼层的收获
void ServerDataManage::RequestFloorHarvest(int floor_type, int iFastHavest)
{
    FloorData * pFloorData = PlayerDataManage::ShareInstance() -> GetMyFloor(floor_type);
    
    if(pFloorData != NULL)
    {
        if(iFastHavest == 0)
        {
            if(pFloorData -> Floor_HarvestTime <= 0)
            {
                [[NetManager GetSingle] HarvestFloorRequest:floor_type : iFastHavest];
            }else
            {
                ShowMessage(BType_Firm, "该楼层的收获时间还未到哦，请耐心等待");
            }
        }else if(iFastHavest == 1)
        {
            if(PlayerDataManage::m_PlayerRMB - Price_ImmediatelyHarvest >= 0)
            {
                [[NetManager GetSingle] HarvestFloorRequest:floor_type : iFastHavest];
            }else
            {
                ShowMessage(BType_Firm, "当前钻石不够，无法立即收获该楼层，请进入商店充值钻石");
            }
        }
    }
}

#pragma mark 请求楼层的收获成功
void ServerDataManage::RequestFloorHarvestSuccess(int floor_type, int floor_harvestMoney)
{
    FloorData * pFloorData = PlayerDataManage::ShareInstance() -> GetMyFloor(floor_type);
    
    if(pFloorData != NULL)
    {
        //加上收获的金钱
        if(floor_harvestMoney != pFloorData -> Floor_HarvestMoney)
        {
            char info[128];
            sprintf(info, "升级类型为%d的楼层时，服务器的收获金钱与配置表中的收获金钱不一致\n", floor_type);
            ShowMessage(BType_Firm, info);
        }
        PlayerDataManage::m_PlayerMoney += pFloorData -> Floor_HarvestMoney;
        
        //刷新界面上的金钱
        Layer_Game::GetSingle() -> UpdateMenuMoney();
        
        //重置收获的时间
        pFloorData -> Floor_HarvestTime = SystemDataManage::ShareInstance() -> GetData_ForFloor(floor_type, pFloorData -> Floor_Level) -> Floor_HarvestTime;
        
        //收获表现
        KNUIFunction::GetSingle() -> finishFloorHavest(floor_type);
    }
}
/****************************************************/
#pragma mark -
#pragma mark -
/****************************************************/



/*********************************************************/
#pragma mark 关于搜索加好友
#pragma mark -
/*********************************************************/ 
void ServerDataManage::RequestFindFriendByUserID(int UserID)
{
    [[NetManager GetSingle] FindFriendInfoRequest:UserID];
}

void ServerDataManage::RequestFindFriendByUserIDSucceed(FriendInfo& stInfo)
{
    // 刷新查找结果界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FIND_RESULT_FRAME_ID);
    if (pFrame != NULL && pFrame->getIsVisible() == false)
    {
        // 取得小弟数据
        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(stInfo.Fri_FollowerID);
        if (pData == NULL)
        {
            char info[128];
            sprintf(info, "查找好友，没有找到id = %d的数据\n", stInfo.Fri_FollowerID);
            ShowMessage(BType_Firm, info);
            return;
        }
        
        // 打开界面
        pFrame->setIsVisible(true);
        
        // 小弟头像
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
            pLabel->resetLabelImage(pData->Follower_IconName);
        
        // 小弟边筐
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "info_frame_%d.png", pData->Follower_Profession);
            pLabel->resetLabelImage(szTemp);
        }
        
        // 玩家姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
            pLabel->GetTextLabel()->setString(stInfo.Fri_UserName);
        
        // 好友小弟等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", stInfo.Fri_FollowerLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 好友等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", stInfo.Fri_UserLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
    
    // 隐藏查找对话框
    pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FIND_FRAME_ID);
    if (pFrame != NULL && pFrame->getIsVisible() == true)
        pFrame->setIsVisible(false);
}

void ServerDataManage::RequestAddFriendByUserID(int UserID)
{
    [[NetManager GetSingle] AddFriendByUserIDRequest:UserID];
}

void ServerDataManage::RequestAddFriendByUserIDSucceed()
{
    KNUIFunction::GetSingle()->OpenMessageBoxFrame("发送成功!");
}

void ServerDataManage::RequestRemoveFriendByUserID(int UserID)
{
    [[NetManager GetSingle] RemoveFriendRequest:UserID];
}

void ServerDataManage::RequestRemoveFriendByUserIDSucceed(int UserID)
{
    // 从列表找到好友并删除
    map<int, list<FriendInfo> >::iterator it = ServerDataManage::ShareInstance()->m_FriendInfoList.find(0);
    if (it == ServerDataManage::ShareInstance()->m_FriendInfoList.end())
    {
        ShowMessage(BType_Firm, "ServerDataManage::RequestRemoveFriendByUserIDSucceed - 好友列表不见了???\n");
        return;
    }
    
    list<FriendInfo> friendList = it->second;
    list<FriendInfo>::iterator iter;
    for (iter = friendList.begin(); iter != friendList.end(); iter++)
    {
        if (iter->Fri_UserID == UserID)
        {
            // 删除好友信息
            friendList.erase(iter);
            
            ServerDataManage::ShareInstance()->m_FriendInfoList.erase(it);
            ServerDataManage::ShareInstance()->m_FriendInfoList.insert(make_pair(0, friendList));
            
            // 刷新列表
            KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_FRAME_ID);
            if (pFrame != NULL)
            {
                Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
                if (pList != NULL)
                    pList->RefreshListData();
            }
            
            // 刷新副本好友列表
            pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_SELECT_FRAME_ID);
            if (pFrame != NULL)
            {
                Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
                if (pList != NULL)
                    pList->RefreshListData();
            }
            
            // 关闭对话框
            pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_FRIEND_EDIT_FRAME_ID);
            if (pFrame != NULL)
                pFrame->setIsVisible(false);
            
            pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_REMOVE_CONFIRM_FRAME_ID);
            if (pFrame != NULL)
                pFrame->setIsVisible(false);
            
            break;
        }
    }
    
    // 如果有邮件，打开邮件标记
    list <MailInfo*> mailList = *PlayerDataManage::ShareInstance()->GetMyMailList();
    if (mailList.size() == 0)
        Layer_Game::GetSingle()->SetMenuItemState(1, false);
    else
        Layer_Game::GetSingle()->SetMenuItemState(1, true);
}

void ServerDataManage::RequestAgreeAddFriend(int UserID)
{
    [[NetManager GetSingle] AgreeAddFriendRequest:UserID];
}

void ServerDataManage::RequestAgreeAddFriendSucceed(FriendInfo& stInfo)
{
    // 取得好友列表
    map<int, list<FriendInfo> >::iterator it = ServerDataManage::ShareInstance()->m_FriendInfoList.find(0);
    if (it == ServerDataManage::ShareInstance()->m_FriendInfoList.end())
        return;
    
    // 是否已经存在该好友
    list<FriendInfo> friendList = it->second;
    list<FriendInfo>::iterator iter;
    for (iter = friendList.begin(); iter != friendList.end(); iter++)
    {
        if (iter->Fri_UserID == stInfo.Fri_UserID)
        {
            ShowMessage(BType_Firm, "已添加过该好友!");
            return;
        }
        else if (atoi(PlayerDataManage::ShareInstance()->m_PlayerUserID) == stInfo.Fri_UserID)
        {
            ShowMessage(BType_Firm, "不能加自己");
            return;
        }
    }
    
    // 添加到列表
    it->second.push_back(stInfo);
    
    // 刷新好友列表
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
            pList->RefreshListData();
    }
    
    // 刷新副本好友列表
    pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_SELECT_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FriendFollowerList* pList = dynamic_cast<Layer_FriendFollowerList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
            pList->RefreshListData();
    }
    
    // 刷新邮件列表
    pFrame = KNUIManager::GetManager()->GetFrameByID(MAIL_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_MailList* pList = dynamic_cast<Layer_MailList*>(pFrame->GetSubUIByID(80000));
        if (pList != NULL)
        {
            // 删除邮件
            PlayerDataManage::ShareInstance()->RemoveMailFromList(pList->GetSelectedMailIndex());
            
            pList->RefreshMailList();
        }
    }
    
    // 如果有邮件，打开邮件标记
    list <MailInfo*> mailList = *PlayerDataManage::ShareInstance()->GetMyMailList();
    if (mailList.size() == 0)
        Layer_Game::GetSingle()->SetMenuItemState(1, false);
    else
        Layer_Game::GetSingle()->SetMenuItemState(1, true);
    
    // 刷新邮件数量
    KNUIFunction::GetSingle()->UpdatePlatformInfo();
    
    // 添加提示
    ShowMessage(BType_Firm, "成功加对方为好友哦！");
    
    // 关闭对话框
    pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_REQUEST_CONFIRM_FRAME_ID);
    if (pFrame != NULL)
        pFrame->setIsVisible(false);
}

/*********************************************************/
#pragma mark -
/*********************************************************/ 



/*********************************************************/
#pragma mark 关于好友信息
/*********************************************************/ 
void ServerDataManage::RequestFriendList()
{
    [[NetManager GetSingle] FriendListRequest];
}

void ServerDataManage::ClearFriendList(int type)
{
    map <int, list <FriendInfo> >::iterator it1 = m_FriendInfoList.find(type);
    if(it1 != m_FriendInfoList.end())
    {
        for(list <FriendInfo>::iterator it2 = it1 -> second.begin(); it2 != it1 -> second.end(); )
        {
            it1 -> second.erase(it2 ++);
        }
        m_FriendInfoList.erase(it1);
    }
}
/*********************************************************/
#pragma mark -
#pragma mark -
/*********************************************************/


/****************************************************/
#pragma mark 关于战斗信息
/****************************************************/
#pragma mark 获取战斗副本信息
void ServerDataManage::RequestEnterBattle()
{
    [[NetManager GetSingle] EnterBattleRequest:m_ChoiceMapIndex];
}


#pragma mark 验证战斗信息
void ServerDataManage::VerificationBattleInfo()
{
    CCDirector::sharedDirector() -> getRunningScene() -> removeChildByTag(MessageTag, true);
    [[NetManager GetSingle] BattleVerificationRequest];
}


#pragma mark 验证战斗信息成功
void ServerDataManage::VerificationBattleInfoSucceed(int playerlevel)
{
    Layer_Enemy::ShareInstance() -> m_PlayerBattleMoney += ServerDataManage::ShareInstance() -> GetClearLevelDropMoney();
    PlayerDataManage::m_PlayerMoney += Layer_Enemy::ShareInstance() -> m_PlayerBattleMoney;
    
    Layer_Enemy::ShareInstance() -> m_PlayerBattleExperience += ServerDataManage::ShareInstance() -> GetClearLevelDropExperience();
    
    for(list <Follower *>::iterator it = Layer_Enemy::ShareInstance() -> m_PlayerBattleResList.begin(); it != Layer_Enemy::ShareInstance() -> m_PlayerBattleResList.end(); ++ it)
    {
        PlayerDataManage::ShareInstance() -> AddFollowerToDepot(*it);
    }
    
    Layer_BattleResult * pBattleResult = Layer_BattleResult::node();
    Layer_Display::ShareInstance() -> addChild(pBattleResult);
    
    Layer_Enemy::ShareInstance() -> m_bAllClearLevel = true;
}

#pragma  mark 验证战斗信息失败
void ServerDataManage::VerificationBattleInfoFailed()
{
    Layer_Enemy::ShareInstance() -> m_PlayerBattleMoney += 0;
    PlayerDataManage::m_PlayerMoney += Layer_Enemy::ShareInstance() -> m_PlayerBattleMoney;
    
    Layer_Enemy::ShareInstance() -> m_PlayerBattleExperience += 0;
    
    Layer_BattleResult * pBattleResult = Layer_BattleResult::node();
    Layer_Display::ShareInstance() -> addChild(pBattleResult);
    
    Layer_Enemy::ShareInstance() -> m_bAllClearLevel = false;
}

#pragma mark 发送复活请求
void ServerDataManage::RequestRelive()
{
    if(PlayerDataManage::ShareInstance() -> m_PlayerRMB - Price_ReLive < 0)
    {
        ShowMessage(BType_Firm, "当前钻石不够，无法复活，请进入商店充值钻石");
    }else
    {
        [[NetManager GetSingle] BattleReliveRequest];   
    }
}

void ServerDataManage::RequestReliveSucceed(int money)
{
    PlayerDataManage::m_PlayerRMB = money;
    
    //恢复战斗背景音乐
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> resumeBackgroundMusic();
    
    Layer_Display::ShareInstance() -> removeChildByTag(Layer_DisplayChild_Message, true);
    Layer_Display::ShareInstance() -> DisplayGameOverRemove();
    
    Layer_Follower::ShareInstance() -> DisplayReliveAddHp();
    
    Layer_Enemy::ShareInstance() -> AttackOverFunc();
    
    Layer_Block::ShareInstance() -> RestartBlock();
}
/****************************************************/
#pragma mark -
#pragma mark -
/****************************************************/



/*********************************************************/
#pragma mark 关于行动力购买
/*********************************************************/
void ServerDataManage::RequestBuyAction()
{
    if(PlayerDataManage::m_PlayerRMB - Price_ReAction >= 0 &&
       PlayerDataManage::m_PlayerActivity < PlayerDataManage::m_PlayerMaxActivity)
    {
        [[NetManager GetSingle] BuyActionRequest];
    }
    else if(PlayerDataManage::m_PlayerRMB - Price_ReAction < 0)
    {
        ShowMessage(BType_Firm, "当前钻石不够，无法直接恢复行动力，请进入商店充值钻石");
    }
    else if(PlayerDataManage::m_PlayerActivity == PlayerDataManage::m_PlayerMaxActivity)
    {
        ShowMessage(BType_Firm, "当前行动力已满，不需要恢复");
    }
}

void ServerDataManage::RequestBuyActionSucceed(int money)
{
    PlayerDataManage::m_PlayerRMB = money;
    
    PlayerDataManage::m_PlayerActivity = PlayerDataManage::m_PlayerMaxActivity;
    CCDirector::sharedDirector() -> setActionComeBack(false);
    PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime = 0;
    
    ShowMessage(BType_Firm, "恢复行动力成功，你又可以尽情的战斗啦");
}
/*********************************************************/
#pragma mark-
/*********************************************************/



/*********************************************************/
#pragma mark 关于小弟背包的扩容
/*********************************************************/
void ServerDataManage::RequestExtendPackage()
{
    if(PlayerDataManage::m_PlayerRMB - Price_ExtendPackge >= 0)
    {
        [[NetManager GetSingle] ExtendPackage];      
    }
    else
    {
        ShowMessage(BType_Firm, "当前钻石不够，无法对背包进行扩容，请进入商店充值钻石");
    }
}

void ServerDataManage::RequestExtendPackageSucceed(int money, int add)
{
    PlayerDataManage::m_PlayerRMB = money;
    PlayerDataManage::m_iPlayerPackageNum += add;
    
    char buff[64];
    sprintf(buff, "扩充背包成功，背包增加了%d个格子", add);
    ShowMessage(BType_Firm, buff);
    
    // 刷新背包信息
    KNUIFunction::GetSingle()->UpdatePlatformInfo();
}
/*********************************************************/
#pragma mark-
/*********************************************************/



/*********************************************************/
#pragma mark 关于搜索加好友
/*********************************************************/
void ServerDataManage::RequestNotice()
{
    [[NetManager GetSingle] NoticeRequest];
}

void ServerDataManage::RequestNoticeSuccess(const string& str)
{
    int iStart = 0;
    int iEnd = 0;
    
    while (true)
    {
        iEnd = str.find("end", iStart);
        if (iEnd == string::npos)
            break;
        
        string msg = str.substr(iStart, iEnd - iStart);
        
        ServerDataManage::ShareInstance()->m_vecMessageList.push_back(msg);
        
        iStart = iEnd + 2;
    }
}
/*********************************************************/
#pragma mark-
/*********************************************************/



/****************************************************/
#pragma mark 关于数据更新
/****************************************************/
void ServerDataManage::RequestDataUpdate()
{
    CCDirector::sharedDirector() -> getRunningScene() -> removeChildByTag(MessageTag, true);
    [[NetManager GetSingle] DataUpdateRequest];
}

/****************************************************/
#pragma mark -
#pragma mark -
/****************************************************/



/*********************************************************/
#pragma mark 关于签到
/*********************************************************/
void ServerDataManage::RequestSignToday()
{
    [[NetManager GetSingle] SignTodayRequest];
}

void ServerDataManage::RequestSignTodaySuccess()
{
    // 刷新签到界面
    KNUIFunction::GetSingle()->UpdateSignInfo();
    
    // 关闭签到信息
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(SIGN_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        // 切换签到信息
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
            pLabel->resetLabelImage("sign_info_2.png");
        
        // 隐藏获得金币
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setIsVisible(false);
        
        // 屏蔽今日签到按钮
        KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30001));
        if (pButton != NULL)
        {
            pButton->setButtonEnable(false);
            pButton->resetButtonImage("button_sign_mask.png", "button_sign_mask.png");
        }
    }
}

void ServerDataManage::RequestOpenBox(int iType)
{
    [[NetManager GetSingle] OpenBoxRequest : iType];
}

void ServerDataManage::RequestOpenBoxSuccess(int iType)
{
    switch (iType) {
        case 1:
            ServerDataManage::ShareInstance()->m_stSign.m_arrAward[0] = 1;
            break;
        case 3:
            ServerDataManage::ShareInstance()->m_stSign.m_arrAward[1] = 1;
            break;
        case 5:
            ServerDataManage::ShareInstance()->m_stSign.m_arrAward[2] = 1;
            break;
        case 7:
            ServerDataManage::ShareInstance()->m_stSign.m_arrAward[3] = 1;
            break;
        case 15:
            ServerDataManage::ShareInstance()->m_stSign.m_arrAward[4] = 1;
            break;
        case 30:
            ServerDataManage::ShareInstance()->m_stSign.m_arrAward[5] = 1;
            break;
        default:
            break;
    }
    
    KNUIFunction::GetSingle()->UpdateSignInfo();
}

void ServerDataManage::RequestLostSign(int iType)
{
    [[NetManager GetSingle] SignLostRequest:iType];
}

void ServerDataManage::RequestLostSignSuccess()
{
    KNUIFunction::GetSingle()->UpdateSignInfo();
}
/*********************************************************/
#pragma mark-
/*********************************************************/












