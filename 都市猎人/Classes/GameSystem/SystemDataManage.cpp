//
//  SystemDataManage.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-4-1.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "SystemDataManage.h"
#include "tinyxml.h"
USING_NS_CC;

SystemDataManage * SystemDataManage::m_Instance = NULL;

/************************************************************************/
/* function :     构造函数                                               */
/************************************************************************/
SystemDataManage::SystemDataManage()
{

}

SystemDataManage::~SystemDataManage()
{

}

/************************************************************************/
/* function :     初始化                                                 */
/************************************************************************/
void SystemDataManage::Initialize()
{
    
}

/************************************************************************/
/* function :     获得单例函数                                            */
/************************************************************************/
SystemDataManage * SystemDataManage::ShareInstance()
{
    if(m_Instance == NULL)
    {
        m_Instance = new SystemDataManage();
        m_Instance -> Initialize();
    }
    return m_Instance;
}

#pragma mark -
#define  DocumentLoadFile(filename, nodename)\
const char * fullpath = CCFileUtils::fullPathFromRelativePath(filename);\
TiXmlDocument Document = TiXmlDocument(fullpath);\
if (! Document.LoadFile())\
{\
printf("SystemDataManage : LoadXML %s Error\n", filename);\
return false;\
}\
TiXmlNode * Node = Document.FirstChild(nodename);\
if(! Node)\
{\
printf("SystemDataManage: Load %s Don't have node named %s\n", filename, nodename);\
return false;\
}
#pragma mark -


/************************************************************************/
#pragma mark    读取所有的数据 
bool  SystemDataManage::LoadData_ForAll()
{
    return  LoadData_ForFloor("FloorData.xml") &&
            LoadData_ForCharacter("CharacterData.xml") &&
            LoadData_ForSkill("SkillData.xml") &&
            LoadData_ForFollower("FollowerData.xml");
}
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark    读取所有装备的数据 
bool SystemDataManage::LoadData_ForEquipment(const char * filename)
{
    DocumentLoadFile(filename, "EquipmentData")
    
    //循环读取节点, 并且生成数据结构存入容器
    for(TiXmlNode * EquipmentTypeNode = Node -> FirstChild("EquipmentType"); EquipmentTypeNode; EquipmentTypeNode = EquipmentTypeNode -> NextSibling("EquipmentType"))
    {
        //先读取装备的类型
        int EquipmentType = 0;
        TiXmlElement * EquipmentTypeElement = EquipmentTypeNode -> ToElement();
        EquipmentTypeElement -> QueryIntAttribute("TypeID", &EquipmentType);
        
        for(TiXmlNode * EquipmentNode = EquipmentTypeNode -> FirstChild(); EquipmentNode; EquipmentNode = EquipmentNode -> NextSibling())
        {
            TiXmlElement * Element = EquipmentNode -> ToElement();
            
            Equipment_Data * Data = new Equipment_Data();  
            Data -> Equipment_Type = EquipmentType;
            Element -> QueryIntAttribute("ID", &Data -> Equipment_ID);
            Element -> QueryIntAttribute("Profession", &Data -> Equipment_BindProfession);
            Element -> QueryIntAttribute("Level", &Data -> Equipment_Level);
            Element -> QueryFloatAttribute("HP", &Data -> Equipment_HP);
            Element -> QueryFloatAttribute("Attack", &Data -> Equipment_Attack);
            Element -> QueryFloatAttribute("Defense", &Data -> Equipment_Defense);
            Element -> QueryFloatAttribute("BuyPrice", &Data -> Equipment_BuyPrice);
            Element -> QueryFloatAttribute("SellPrice", &Data -> Equipment_SellPrice);
            Element -> QueryFloatAttribute("IntensifyPrice", &Data -> Equipment_IntensifyPrice);
            Element -> QueryIntAttribute("ComposePropsID", &Data -> Equipment_Compose_PropsID);
            Element -> QueryIntAttribute("ComposePropsNum", &Data -> Equipment_Compose_PropsNum);
            Element -> QueryIntAttribute("DecomposePropsID", &Data -> Equipment_Decompose_PropsID);
            Element -> QueryIntAttribute("DecomposePropsNum", &Data -> Equipment_Decompose_PropsNum);
            Element -> QueryIntAttribute("IntensifyPropsID", &Data -> Equipment_Intensify_PropsID);
            Element -> QueryIntAttribute("IntensifyPropsNum", &Data -> Equipment_Intensify_PropsNum);
            
            memcpy(Data -> Equipment_Name, Element -> Attribute("Name"), CHARLENGHT);
            memcpy(Data -> Equipment_IconName, Element -> Attribute("IconName"), CHARLENGHT);
            
            m_EquipmentDataMap[EquipmentType][Data -> Equipment_ID] = Data;
        }
    }
    return true;
}

const Equipment_Data * SystemDataManage::GetData_ForEquipment(int equipment_type, int equipment_id)
{
    m_EquipmentDataMap_it1 = m_EquipmentDataMap.find(equipment_type);
    if(m_EquipmentDataMap_it1 != m_EquipmentDataMap.end())
    {
        m_EquipmentDataMap_it2 = m_EquipmentDataMap_it1 -> second.find(equipment_id);
        if(m_EquipmentDataMap_it2 != m_EquipmentDataMap_it1 -> second.end())
        {
            return m_EquipmentDataMap_it2 -> second;
        }
        printf("装备数据仓库中类型为%d的装备没有ID为%d的数据\n", equipment_type, equipment_id);
    }
    printf("装备数据仓库中没有类型为%d的数据\n", equipment_type);
    return NULL;
}
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark    读取所有道具的数据 
bool SystemDataManage::LoadData_ForProps(const char * filename)
{
    DocumentLoadFile(filename, "PropsData")
    
    for(TiXmlNode * SubNode = Node -> FirstChild(); SubNode; SubNode = SubNode -> NextSibling())
    {
        TiXmlElement * Element = SubNode -> ToElement();
        
        Props_Data * Data = new Props_Data();  
        Element -> QueryIntAttribute("ID", &Data -> Props_ID);
        Element -> QueryIntAttribute("Type", &Data -> Props_Type);
        Element -> QueryFloatAttribute("BuyPrice", &Data -> Props_BuyPrice);
        Element -> QueryFloatAttribute("SalePrice", &Data -> Props_SalePrice);
        
        memcpy(Data -> Props_Name, Element -> Attribute("Name"), CHARLENGHT);
        memcpy(Data -> Props_IconName, Element -> Attribute("IconName"), CHARLENGHT);
        
        m_PropsDataMap[Data -> Props_ID] = Data;
    }
    return true;
}

const Props_Data * SystemDataManage::GetDate_ForProps(int props_id)
{
    m_PropsDataMap_it = m_PropsDataMap.find(props_id);
    if(m_PropsDataMap_it != m_PropsDataMap.end())
    {
        return m_PropsDataMap_it -> second;
    }
    printf("道具数据仓库中没有ID为%d的数据\n", props_id);
    return NULL;
}
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark    读取所有楼层的数据
bool SystemDataManage::LoadData_ForFloor(const char * filename)
{
    DocumentLoadFile(filename, "FloorData")

    //楼层类型
    for(TiXmlNode * FloorType_Node = Node -> FirstChild(); FloorType_Node; FloorType_Node = FloorType_Node -> NextSibling())
    {
        int FloorType = 0;
        TiXmlElement * FloorType_Element = FloorType_Node -> ToElement();
        FloorType_Element -> QueryIntAttribute("TypeID", &FloorType);
        
        //楼层类型中不同等级的楼层
        for(TiXmlNode * Floor_Node = FloorType_Node -> FirstChild(); Floor_Node; Floor_Node = Floor_Node -> NextSibling())
        {
            TiXmlElement * Floor_Element = Floor_Node -> ToElement();
            
            Floor_Data * Data = new Floor_Data();
            
            //类型
            Data -> Floor_Type = FloorType;
            //等级
            Floor_Element -> QueryIntAttribute("Level", &Data -> Floor_Level);
            
            Floor_Element -> QueryIntAttribute("Build_PlayerLevel", &Data -> Floor_Build_PlayerLevel);
            Floor_Element -> QueryIntAttribute("Build_PlayerMoney", &Data -> Floor_Build_PlayerMoney);

            Floor_Element -> QueryIntAttribute("Upgrade_PlayerLevel", &Data -> Floor_Upgrade_PlayerLevel); 
            Floor_Element -> QueryIntAttribute("Upgrade_PlayerMoney", &Data -> Floor_Upgrade_PlayerMoney); 
            
            float pHarvestTime = 0;
            Floor_Element -> QueryFloatAttribute("HarvestTime", &pHarvestTime);  
            Data -> Floor_HarvestTime = pHarvestTime * 60;
            
            Floor_Element -> QueryFloatAttribute("HarvestMoney", &Data -> Floor_HarvestMoney);  
    
            //楼层的名称
            memcpy(Data -> Floor_Name, Floor_Element -> Attribute("Name"), CHARLENGHT);
            //楼层背景图片名称
            memcpy(Data -> Floor_BgName, Floor_Element -> Attribute("BgImageName"), CHARLENGHT);
            
            m_FloorDataMap[FloorType][Data -> Floor_Level] = Data;
        }
    }
    return true;
}

const Floor_Data * SystemDataManage::GetData_ForFloor(int floor_type, int floor_level)
{
    m_FloorDataMap_it1 = m_FloorDataMap.find(floor_type);
    if(m_FloorDataMap_it1 != m_FloorDataMap.end())
    {
        m_FloorDataMap_it2 = m_FloorDataMap_it1 -> second.find(floor_level);
        if(m_FloorDataMap_it2 != m_FloorDataMap_it1 -> second.end())
        {   
            return m_FloorDataMap_it2 -> second;
        }
        else 
            printf("楼层数据仓库中%d类型的楼层没有等级为%d的数据\n", floor_type, floor_level);   
    }
    else
        printf("楼层数据仓库中没有类型为%d的楼层\n", floor_type);
    return NULL;
}
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark    读取所有技能的数据
bool SystemDataManage::LoadData_ForSkill(const char * filename)
{
    //DocumentLoadFile(filename, "SkillData")
    const char * fullpath = CCFileUtils::fullPathFromRelativePath(filename);
    TiXmlDocument Document = TiXmlDocument(fullpath);
    if (! Document.LoadFile())
    {
        printf("SystemDataManage : LoadXML %s Error\n", filename);
        return false;
    }
    TiXmlNode * Node = Document.FirstChild("SkillData");
    if(! Node)
    {
        printf("SystemDataManage: Load %s Don't have node named %s\n", filename, "SkillData");
        return false;
    }
   
    for(TiXmlNode * SkillType_Node = Node -> FirstChild("SkillType"); SkillType_Node; SkillType_Node = SkillType_Node -> NextSibling("SkillType"))
    {
        int SkillType = 0;
        TiXmlElement * SkillType_Element = SkillType_Node -> ToElement();
        SkillType_Element -> QueryIntAttribute("TypeID", &SkillType);
        
        //技能类型中不同ID的技能
        for(TiXmlNode * Skill_Node = SkillType_Node -> FirstChild(); Skill_Node; Skill_Node = Skill_Node -> NextSibling())
        {
            TiXmlElement * Skill_Element = Skill_Node -> ToElement();

            Skill_Data * Data = new Skill_Data();
            Data -> Skill_Type = SkillType;
            Skill_Element -> QueryIntAttribute("ID", &Data -> Skill_ID);
            Skill_Element -> QueryIntAttribute("MaxLevel", &Data -> Skill_MaxLevel); 
            Skill_Element -> QueryIntAttribute("CD_Round", &Data -> Skill_CDRound);
            Skill_Element -> QueryIntAttribute("Keep_Round", &Data -> Skill_KeepRound);
            Skill_Element -> QueryFloatAttribute("Value", &Data -> Skill_Value);
            
            memcpy(Data -> Skill_Name, Skill_Element -> Attribute("Name"), CHARLENGHT);
            memcpy(Data -> Skill_IconName, Skill_Element -> Attribute("IconName"), CHARLENGHT);
            memcpy(Data -> Skill_Depict, Skill_Element -> Attribute("Depict"), CHARLENGHT);
            
            m_SkillDataMap[SkillType][Data -> Skill_ID] = Data;
        }
    }
    return true;
}

const Skill_Data * SystemDataManage::GetData_ForSkill(int skill_type, int skill_id)
{
    m_SkillDataMap_it1 = m_SkillDataMap.find(skill_type);
    if(m_SkillDataMap_it1 != m_SkillDataMap.end())
    {
        m_SkillDataMap_it2 = m_SkillDataMap_it1 -> second.find(skill_id);
        if(m_SkillDataMap_it2 != m_SkillDataMap_it1 -> second.end())
        {   
            return m_SkillDataMap_it2 -> second;
        }
        else 
            printf("技能数据仓库中%d类型的技能没有ID号为%d的数据\n", skill_type, skill_id);   
    }
    else
        printf("技能数据仓库中没有类型为%d的技能\n", skill_type);
    return NULL;
}
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark 读取所有小弟的性格的数据 
bool SystemDataManage::LoadData_ForCharacter(const char * filename)
{
    DocumentLoadFile(filename, "CharacterData")
    
    //循环读取节点, 并且生成数据结构存入容器
    for(TiXmlNode * SubNode = Node -> FirstChild(); SubNode; SubNode = SubNode -> NextSibling())
    {
        Character_Data * Data = new Character_Data();
        
        TiXmlElement * Element = SubNode -> ToElement();

        Element -> QueryIntAttribute("ID", &Data -> Character_ID);
        Element -> QueryFloatAttribute("HP", &Data -> Character_HP);
        Element -> QueryFloatAttribute("Attack", &Data -> Character_Attack);
        Element -> QueryFloatAttribute("Defense", &Data -> Character_Defense);
        memcpy(Data -> Character_Name, Element -> Attribute("Name"), CHARLENGHT);
        
        m_CharacterDataMap[Data -> Character_ID] = Data;
    }
    return true;
}

const Character_Data * SystemDataManage::GetData_ForCharacter(int character_id)
{
    m_CharacterDataMap_it = m_CharacterDataMap.find(character_id);
    if(m_CharacterDataMap_it != m_CharacterDataMap.end())
    {
        return m_CharacterDataMap_it -> second;
    }
    printf("小弟性格数据仓库中没有ID为%d的数据\n", character_id);
    return NULL;
}
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark  读取所有小弟的数据                                       
bool SystemDataManage::LoadData_ForFollower(const char * filename)
{
    DocumentLoadFile(filename, "FollowerData")
    
    //循环读取节点, 并且生成数据结构存入容器
    for(TiXmlNode * SubNode = Node -> FirstChild(); SubNode; SubNode = SubNode -> NextSibling())
    {
        Follower_Data * Data = new Follower_Data();
        
        TiXmlElement * Element = SubNode -> ToElement();
        
        Element -> QueryIntAttribute("ID", &Data -> Follower_ID);
        
        memcpy(Data -> Follower_Name, Element -> Attribute("Name"), CHARLENGHT);
        memcpy(Data -> Follower_IconName, Element -> Attribute("IconName"), CHARLENGHT);
        memcpy(Data -> Follower_FightIconName, Element -> Attribute("FightIconName"), CHARLENGHT);
        memcpy(Data -> Follower_DropIconName, Element -> Attribute("DropIconName"), CHARLENGHT);
        
        Element -> QueryIntAttribute("Profession", &Data -> Follower_Profession);
        Element -> QueryFloatAttribute("Price", &Data -> Follower_Price);
        
        Element -> QueryFloatAttribute("RegulateHP", &Data -> Follower_RegulateHP);
        Element -> QueryFloatAttribute("RegulateAttack", &Data -> Follower_RegulateAttack);
        Element -> QueryFloatAttribute("RegulateDefense", &Data -> Follower_RegulateDefense);
        Element -> QueryFloatAttribute("RegulateComeback", &Data -> Follower_RegulateComeback);
        Element -> QueryFloatAttribute("RegulateLevelUpExperience", &Data -> Follower_RegulateLevelUpExperience);
        Element -> QueryFloatAttribute("RegulateBeAbsorbExperience", &Data -> Follower_RegulateBeAbsorbExperience);
        Element -> QueryFloatAttribute("RegulateAbsorbPrice", &Data -> Follower_RegulateAbsorbPrice);
        
        Element -> QueryFloatAttribute("HP", &Data -> Follower_HP);
        Element -> QueryFloatAttribute("Attack", &Data -> Follower_Attack);
        Element -> QueryFloatAttribute("Defense", &Data -> Follower_Defense);
        Element -> QueryFloatAttribute("Comeback", &Data -> Follower_Comeback);
        
        Element -> QueryIntAttribute("GrowHPType", &Data -> Follower_GrowHP_Type);
        Element -> QueryIntAttribute("GrowAttackType", &Data -> Follower_GrowAttack_Type);
        Element -> QueryIntAttribute("GrowDefenseType", &Data -> Follower_GrowDefense_Type);
        Element -> QueryIntAttribute("GrowCombackType", &Data -> Follower_GrowComeback_Type);
        
        Element -> QueryIntAttribute("Quality", &Data -> Follower_Quality);
        Element -> QueryIntAttribute("MaxQuality", &Data -> Follower_MaxQuality);
        Element -> QueryIntAttribute("MaxLevel", &Data -> Follower_MaxLevel);

        Element -> QueryIntAttribute("CommonlySkillType", &Data -> Follower_CommonlySkillType);
        Element -> QueryIntAttribute("CommonlySkillID", &Data -> Follower_CommonlySkillID);
        Element -> QueryIntAttribute("TeamSkillType", &Data -> Follower_TeamSkillType);
        Element -> QueryIntAttribute("TeamSkillID", &Data -> Follower_TeamSkillID);

        Element -> QueryIntAttribute("EvolvementPrice", &Data -> Follower_EvolvementPrice);
        Element -> QueryIntAttribute("EvolvementID", &Data -> Follower_EvolvementID);
        char buff[64];
        for(int i = 1; i < 6; ++ i)
        {
            sprintf(buff, "EvolvementFollowerID%d", i);
            if(Element -> QueryIntAttribute(buff, &Data -> Follower_EvolvementFollowerID[i - 1]) == TIXML_NO_ATTRIBUTE)
                break;
        }
        
        memcpy(Data -> Follower_Depict, Element -> Attribute("Depict"), CHARLENGHT);
        
        Data -> SetQualityColor();
        Data -> BalanceProfession();
        
        m_FollowerDataMap[Data -> Follower_ID] = Data;
    }
    return true;
}

const Follower_Data * SystemDataManage::GetData_ForFollower(int follower_id)
{
    m_FollowerDataMap_it = m_FollowerDataMap.find(follower_id);
    if(m_FollowerDataMap_it != m_FollowerDataMap.end())
    {
        return m_FollowerDataMap_it -> second;
    }
    printf("小弟数据仓库中没有ID为%d的数据\n", follower_id);
    return NULL;
}
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark  读取所有敌人的数据
/*bool SystemDataManage::LoadData_ForEnemy(const char * filename)
{
    DocumentLoadFile(filename, "EnemyData")
    
    //循环读取节点, 并且生成数据结构存入容器
    for(TiXmlNode * SubNode = Node -> FirstChild(); SubNode; SubNode = SubNode -> NextSibling())
    {
        Enemy_Data * Data = new Enemy_Data();
        
        TiXmlElement * Element = SubNode -> ToElement();
        
        Element -> QueryIntAttribute("ID", &Data -> Enemy_ID);
        Element -> QueryIntAttribute("Profession", &Data -> Enemy_Profession); 
        Element -> QueryFloatAttribute("HP", &Data -> Enemy_HP);
        Element -> QueryFloatAttribute("Attack", &Data -> Enemy_Attack);
        Element -> QueryFloatAttribute("Defense", &Data -> Enemy_Defense);
        Element -> QueryIntAttribute("CDRound", &Data -> Enemy_CDRound);
        Element -> QueryIntAttribute("CommonlySkillType", &Data -> Enemy_CommonlySkillType);
        Element -> QueryIntAttribute("CommonlySkillID", &Data -> Enemy_CommonlySkillID);
        Element -> QueryIntAttribute("TeamSkillType", &Data -> Enemy_TeamSkillType);
        Element -> QueryIntAttribute("TeamSkillID", &Data -> Enemy_TeamSkillID);
        
        memcpy(Data -> Enemy_IconName, Element -> Attribute("IconName"), CHARLENGHT);
        
        m_EnemyDataMap[Data -> Enemy_ID] = Data;
    }
    return true;
}

const Enemy_Data * SystemDataManage::GetData_ForEnemy(int enemy_id)
{   
    m_EnemyDataMap_it = m_EnemyDataMap.find(enemy_id);
    if(m_EnemyDataMap_it != m_EnemyDataMap.end())
    {
        return m_EnemyDataMap_it -> second;
    }
    printf("怪物数据仓库中没有ID为%d的数据\n", enemy_id);
    return NULL;
}*/
#pragma mark -
/************************************************************************/





























