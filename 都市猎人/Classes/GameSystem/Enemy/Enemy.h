//
//  Enemy.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-17.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Enemy_h
#define MidNightCity_Enemy_h

#include "cocos2d.h"
#include "Enemy_Configure.h"


class Enemy : public cocos2d::CCLayer
{
public:
    virtual bool init();
    ~Enemy();
    
    virtual void SetData(const Enemy_Data * Data);
    SS_GET(Enemy_Data*, &m_Data, Data);
    
    //受到攻击
    void        BeAttack(int attck);
    //减防
    void        DecreaseDefense(float attack);
    //加防
    void        PlusDefense(float attack);
    //减攻
    void        DecreaseAttack(float attack);
    //加攻
    void        PlusAttack(float attack);
    
    MY_LAYER_NODE_FUNC(Enemy);

public:
    float           m_DecreasedDefense;
    float           m_DecreasedAttack;
    
protected:
    Enemy_Data      m_Data;
    
    //原始的攻击力
    float           m_EnemyAttackOriginal;
    //原始的防御力
    float           m_EnemyDefenseOriginal;
};

#endif
