//
//  Enemy.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-4-17.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Enemy.h"
USING_NS_CC;

bool Enemy::init()
{
    if(! CCLayer::init())   return false;
    
    m_DecreasedDefense = 0;
    m_DecreasedAttack = 0;
    
    return true;
}

Enemy::~Enemy()
{
    printf("Enemy:: 释放完成\n");
}

void Enemy::SetData(const Enemy_Data * Data)
{
    memcpy(&m_Data, Data, sizeof(Enemy_Data));
    
    m_EnemyDefenseOriginal = m_Data.Enemy_Defense;
    m_EnemyAttackOriginal = m_Data.Enemy_Attack;
}

void Enemy::BeAttack(int attck)
{
    m_Data.Enemy_HP -= attck;
    if(m_Data.Enemy_HP <= 0.0f) 
        m_Data.Enemy_HP = 0.0f;
}

void Enemy::DecreaseDefense(float attack)
{
    m_Data.Enemy_Defense -= attack;
    if(m_Data.Enemy_Defense <= 0.0f) 
        m_Data.Enemy_Defense = 0.0f;
}

void Enemy::PlusDefense(float attack)
{
    m_Data.Enemy_Defense += attack;
    if(m_Data.Enemy_Defense >= m_EnemyDefenseOriginal)
        m_Data.Enemy_Defense = m_EnemyDefenseOriginal;
}

void Enemy::DecreaseAttack(float attack)
{
    m_Data.Enemy_Attack -= attack;
    if(m_Data.Enemy_Attack <= 0.0f) 
        m_Data.Enemy_Attack = 0.0f;
}

void Enemy::PlusAttack(float attack)
{
    m_Data.Enemy_Attack += attack;
    if(m_Data.Enemy_Attack >= m_EnemyAttackOriginal) 
        m_Data.Enemy_Attack = m_EnemyAttackOriginal;
}





















