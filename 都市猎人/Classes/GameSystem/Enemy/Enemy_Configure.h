//
//  Enemy_Configure.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-17.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Enemy_Configure_h
#define MidNightCity_Enemy_Configure_h

#include "MyConfigure.h"


typedef struct EnemyData
{
    EnemyData() {memset(this, 0, sizeof(EnemyData));}
    ~EnemyData() {}

    //怪物的职业
    int     Enemy_Profession;
    //怪物的HP
    float   Enemy_HP;
    float   Original_HP;
    //怪物的攻击力
    float   Enemy_Attack;
    float   Original_Attack;
    //怪物的防御力
    float   Enemy_Defense;
    float   Original_Defense;
    //怪物的攻击CD回合数
    int     Enemy_CDRound;
    
    //怪物使用的Icon的名称
    char    Enemy_IconName[CHARLENGHT];
    
    //怪物掉落的金钱
    int     Enemy_DropMoney;
    //怪物掉落的小弟ID
    int     Enemy_DropFollower_ID;
    //怪物掉落的小弟的服务器ID
    int     Enemy_DropFollower_ServerID;
    //怪物掉落的小弟的攻击力
    int     Enemy_DropFollower_Attack;    
    //怪物掉落的小弟的防御力
    int     Enemy_DropFollower_Defense;
    //怪物掉落的小弟的恢复力
    int     Enemy_DropFollower_ComeBack;
    //怪物掉落的小弟的HP
    int     Enemy_DropFollower_HP;
    //怪物掉落的小弟的等级
    int     Enemy_DropFollower_Level;
    //怪物掉落的小弟的技能等级
    int     Enemy_DropFollower_SkillLevel;
    //怪物掉落的小弟升级所需经验值    
    int     Enemy_DropFollower_LevelUpExp;
    //怪物掉落的小弟被消耗给予的经验值    
    int     Enemy_DropFollower_BeAbsorbExp;
    //怪物掉落的小弟的性格ID
    int     Enemy_DropFollower_CharacterID;
}Enemy_Data;

#endif
