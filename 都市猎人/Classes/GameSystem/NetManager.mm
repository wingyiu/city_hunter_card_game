//
//  HttpTest.m
//  MidNightCity
//
//  Created by 强 张 on 12-4-16.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#import "NetManager.h"
#import "Scene_Game.h"
#import "Scene_Battle.h"
#import "Scene_Loading.h"
#import "ServerDataManage.h"
#import "SystemDataManage.h"
#import "Layer_Display.h"
#import "Battle_Follower.h"
#include "KNUIFunction.h"
#include "Wrapper.h"
#include "PlayerDataManage.h"
#include "AppController.h"


@implementation NetManager



/************************************************************************/
#pragma mark  NetManager类静态函数 : 获得网络通信对象函数
/************************************************************************/
+ (id) GetSingle
{
    static NetManager* manager = [[NetManager alloc] init];
    return manager;
}

- (id) init
{
    //内网
    URLPath = @"http://cityos.o.kingnet.com";
    //外网
    //URLPath = @"http://ace.mo.kingnet.com";
   
    return self;
}
/************************************************************************/
#pragma mark - 
/************************************************************************/


// 楼层数据结构
struct FLOOR_DATA
{
    int iType;                              // 类型
    int iLevel;                             // 等级
    int iTime;                              // 时间
    
    // 构造函数
    FLOOR_DATA()                            {memset(this, 0, sizeof(FLOOR_DATA));}
};

// 小弟数据结构
struct FOLLOWER_DATA
{
    int iFServerID;                         // 在服务器端的ID号
    int iFId;                               // 数据ID号
    int iFLevel;                            // 等级
    int iFCharacterID;                      // 性格ID号
    int iFExperience;                       // 当前经验值
    int iFSkillLevel;                       // 当前技能等级
    int iFTeamState;                        // 是否在队伍中
    int iFHp;                               // HP
    int iFAttack;                           // 攻击力
    int iFDefense;                          // 防御力
    int iFComeBack;                         // 回复力
    int iFLevelUpExperience;                // 升级还需要多少经验
    int iFBeAbsorbExperience;               // 被消耗给予多少经验
    
    // 构造函数
    FOLLOWER_DATA()                         {memset(this, 0, sizeof(FOLLOWER_DATA));}
};

// 登陆数据
struct LOGIN_DATA
{
    int  PlayerID;                  //玩家ID
    char PlayerName[32];            //玩家名字
    
    int  PlayerLevel;               //玩家等级
    int  PlayerSex;                 //玩家性别
    
    int  PlayerAction;              //玩家行动力
    int  PlayerMaxAction;           //玩家当前等级最大行动力
    int  PlayerFriendPoint;         //玩家友情点
    float PlayerMoney;              //玩家游戏金钱
    float PlayerDiamond;            //玩家充值金钱
    float PlayerExperience;         //玩家经验
    
    vector<FLOOR_DATA> vecFloorList;        // 楼层列表
    vector<FOLLOWER_DATA> vecFollowerList;  // 小弟列表
    
    // 构造函数
    LOGIN_DATA()                    {memset(this, 0, sizeof(LOGIN_DATA));}
};



/************************************************************************/
#pragma mark 91充值
/************************************************************************/
- (void) PayIDReqest
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(PayIDReqestFinish:)];
    [request setDidFailSelector:@selector(PayIDReqestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];

    [request addPostValue:@"charge" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) PayIDReqestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        NSLog(@"%@", data);
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"充值失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        
        NSString * PayID = [object objectForKey:@"order_id"];
        
        //结束联网动画
        RemoveNetConnect();
        
        //开启充值商店
        AppController * controller = (AppController*)[[UIApplication sharedApplication] delegate];
        [controller BuyDiamond:PayID];
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) PayIDReqestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    NSLog(@"%@", [request error]);
}


/************************************************************************/
#pragma mark 91帐户检查
/************************************************************************/
- (void) Player91Login
{
    NSString * path = [NSString stringWithFormat:@"%@?a=platLogin&c=index", URLPath];
    NSURL * url = [NSURL URLWithString:path];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(Player91LoginFinish:)];
    [request setDidFailSelector:@selector(Player91LoginFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_Player91ID];
    [request addPostValue:UserID forKey:@"outerID"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) Player91LoginFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        NSLog(@"%@", data);
         
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0 && error == 100)
        {
            printf("该91帐户首次登陆游戏\n");
            return;
        }

        // 获得数据包内容
        object = [object objectForKey:@"result"];
        
        //昵称
        NSString * PlayerName = [object objectForKey:@"nickname"];
        memcpy(PlayerDataManage::ShareInstance() -> m_PlayerName, [PlayerName UTF8String], sizeof(PlayerDataManage::ShareInstance() -> m_PlayerName));
        //游戏ID
        NSString * PlayerID = [object objectForKey:@"id"];
        memcpy(PlayerDataManage::ShareInstance() -> m_PlayerUserID, [PlayerID UTF8String], sizeof(PlayerDataManage::ShareInstance() -> m_PlayerUserID));
        //密码
        NSString * PlayerPassword = [object objectForKey:@"pwd"];
        memcpy(PlayerDataManage::ShareInstance() -> m_PlayerPassword, [PlayerPassword UTF8String], sizeof(PlayerDataManage::ShareInstance() -> m_PlayerPassword));
        //初选的职业
        PlayerDataManage::m_PlayerFistGameProfession = [[object objectForKey:@"prof"] intValue];
        //存入玩家信息
        SavePlayerInfo(PlayerDataManage::ShareInstance() -> m_Player91ID);
        
        //引导步骤
        PlayerDataManage::m_GuideStepMark = [[object objectForKey:@"guide"] intValue];
        //存入引导步骤
        if(PlayerDataManage::m_GuideStepMark != 0)
        {
            //战斗引导步骤
            PlayerDataManage::m_GuideBattleMark = false;
            SaveBattleGuideInfo(PlayerDataManage::m_GuideBattleMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
            
            //楼层引导步骤
            SaveGameGuideInfo(PlayerDataManage::m_GuideStepMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
            if(PlayerDataManage::m_GuideStepMark == 10)
                PlayerDataManage::m_GuideMark = false;
            else
                PlayerDataManage::m_GuideMark = true;
        }
    }
    @catch (NSException * exception)
    {
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) Player91LoginFailed : (ASIHTTPRequest *) request
{
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重新登陆游戏。");
    }
    NSLog(@"%@", [request error]);
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 新玩家注册
/************************************************************************/
- (void) PlayerInfoReqest
{
    NSString * path = [NSString stringWithFormat:@"%@?a=newUser&c=index", URLPath];
    NSURL * url = [NSURL URLWithString:path];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(PlayerInfoReqestFinish:)];
    [request setDidFailSelector:@selector(PlayerInfoReqestFailed:)];
    
    NSString * n1 = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerName];
    [request addPostValue:n1 forKey:@"UserName"];
    
    NSNumber * n2 = [[NSNumber alloc] initWithInt:PlayerDataManage::m_PlayerFistGameProfession];
    [request addPostValue:n2 forKey:@"FirstGamePro"];

    NSString * User91ID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_Player91ID];
    [request addPostValue:User91ID forKey:@"outerID"];
    
    [request addPostValue:@"newUser" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) PlayerInfoReqestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"注册新玩家返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        NSLog(@"%@", data);
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"注册失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"注册新玩家返回数据异常" userInfo:nil];
            @throw ex;
        }
        // 获取数据信息
        NSDictionary * user = [object objectForKey:@"user"];
        if (! user)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"注册新玩家返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        //用户ID
        id  ObjectUserID = [user objectForKey:@"uid"];
        int NumberUserID = [ObjectUserID intValue];
        NSString * UserID = [NSString stringWithFormat:@"%d", NumberUserID];
        memcpy(PlayerDataManage::ShareInstance() -> m_PlayerUserID, [UserID UTF8String], sizeof(PlayerDataManage::ShareInstance() -> m_PlayerUserID));
        
        //用户密码
        NSString * UserPassword = [user objectForKey:@"pwd"];
        memcpy(PlayerDataManage::ShareInstance() -> m_PlayerPassword, [UserPassword UTF8String], sizeof(PlayerDataManage::ShareInstance() -> m_PlayerPassword));
        
        //保存用户信息
        SavePlayerInfo(PlayerDataManage::ShareInstance() -> m_Player91ID);
        
        //继续loading
        Scene_Loading::ShareInstance() -> ContinueLoading();
        
        printf("注册新玩家完成\n");
    }
    @catch (NSException * exception)
    {
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Scene_Loading::StopLoading));
        Func -> retain();
        ShowMessage(BType_Firm, [[exception reason] UTF8String], Func);
        
        [exception release];
    }
    @finally
    {
      
    }
}

- (void) PlayerInfoReqestFailed : (ASIHTTPRequest *) request
{
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Scene_Loading::StopLoading));
        Func -> retain();
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重新登陆游戏。", Func);
    }
    NSLog(@"%@", [request error]);
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器注册登陆
/************************************************************************/
- (void) GameLoginReqest
{
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(GameLoginReqestFinish:)];
    [request setDidFailSelector:@selector(GameLoginReqestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    [request addPostValue:@"gameLoginInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) GameLoginReqestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求登陆游戏返回数据异常" userInfo:nil];
            @throw ex;
        }

#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"登陆失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }

        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求登陆游戏返回数据异常" userInfo:nil];
            @throw ex;
        }
        // 获取数据信息
        NSDictionary * info = [object objectForKey:@"info"];
        if (!info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求登陆游戏返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        // 创建数据结构体
        LOGIN_DATA stData;
        
        //获得玩家信息
        NSDictionary * userInfo = [info objectForKey:@"user"];
        if (! userInfo)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求登陆游戏返回数据异常" userInfo:nil];
            @throw ex;
        }else 
        {
            //玩家当前等级
            stData.PlayerLevel = [[userInfo objectForKey:@"lv"] intValue];
            PlayerDataManage::m_PlayerLevel = stData.PlayerLevel;
            
            //玩家当前等级升级所需的经验
            PlayerDataManage::m_PlayerLevelUpExperience = int(1.4 * pow(stData.PlayerLevel, 3) - 3 * pow(stData.PlayerLevel, 2) + 10 * stData.PlayerLevel + 192);
            
            //玩家当前的行动力
            stData.PlayerAction = [[userInfo objectForKey:@"action"] intValue];
            PlayerDataManage::m_PlayerActivity = stData.PlayerAction;
            
            //玩家当前等级的最大行动力
            stData.PlayerMaxAction = [[userInfo objectForKey:@"max_action"] intValue];
            PlayerDataManage::m_PlayerMaxActivity = stData.PlayerMaxAction;
            
            //玩家行动力恢复剩余时间
            PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime = [[userInfo objectForKey:@"rtime"] intValue];
            
            //如果玩家当前的行动力不满，就开始倒计时
            if(PlayerDataManage::m_PlayerActivity < PlayerDataManage::m_PlayerMaxActivity)
            {
                CCDirector::sharedDirector() -> setActionComeBack(true);
            }
            else if(PlayerDataManage::m_PlayerActivity == PlayerDataManage::m_PlayerMaxActivity)
            {
                PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime = 0;
            }
            
            //玩家游戏币
            stData.PlayerMoney = [[userInfo objectForKey:@"gb"] floatValue];
            PlayerDataManage::m_PlayerMoney = stData.PlayerMoney;
            
            //玩家充值的金钱
            stData.PlayerDiamond = [[userInfo objectForKey:@"diamond"] floatValue];
            PlayerDataManage::m_PlayerRMB = stData.PlayerDiamond;
        
            //玩家的经验
            float TeampLevel = stData.PlayerLevel - 1;
            stData.PlayerExperience = [[userInfo objectForKey:@"exp"] intValue];
            int TempExp = int(1.4f * pow((TeampLevel + 1), 2) * pow(TeampLevel, 2) / 4.0f - TeampLevel * (TeampLevel + 1) * (TeampLevel * 2.0f + 1) / 2.0f + 10.0f * TeampLevel * (TeampLevel + 1) / 2.0f + 192 * TeampLevel);
            PlayerDataManage::m_PlayerExperience = stData.PlayerExperience - TempExp;
            
            //如果当前是引导，就把经验值清0,自己加
            if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
            {
                PlayerDataManage::m_PlayerExperience = 0;
            }
            
            //玩家的友情点
            stData.PlayerFriendPoint = [[userInfo objectForKey:@"friendPoint"] intValue];
            PlayerDataManage::m_PlayerFriendValue = stData.PlayerFriendPoint;
            
            // 包裹格子数字
            PlayerDataManage::m_iPlayerPackageNum = [[userInfo objectForKey:@"max_bag"] intValue];
        }
        
        // 获得楼层信息
        NSDictionary* buildingInfo = [info objectForKey:@"building"];
        if ((NSNull*)buildingInfo != [NSNull null])
        {
            // 生成解析器
            SBJsonParser* parser = [[SBJsonParser alloc] init];
            
            // 得到数组数据
            NSError* error = nil;
            NSData* jsonData =[NSJSONSerialization dataWithJSONObject:buildingInfo options:NSJSONWritingPrettyPrinted error:&error];
            NSString* string = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSArray* list = [[NSArray alloc] initWithArray:[parser objectWithString:string]];
            
            // 便利数据
            for (NSDictionary* dict in list)
            {
                FLOOR_DATA stInfo;
                
                stInfo.iType = [[dict objectForKey:@"type"] intValue];
                stInfo.iLevel = [[dict objectForKey:@"lv"] intValue];
                stInfo.iTime = [[dict objectForKey:@"time"] intValue];
                
                stData.vecFloorList.push_back(stInfo);
            }
            
            // 释放解析器
            [string release];
            [list release];
            [parser release];
        }
        
        // 获得小弟信息
        NSDictionary * petInfo = [info objectForKey:@"pet"];
        if (! petInfo)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求登陆游戏返回的小弟数据异常" userInfo:nil];
            @throw ex;
        }else
        {
            if([petInfo count] == 0)
            {
                NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"登陆游戏数据异常，返回的小弟数据为空" userInfo:nil];
                @throw ex;
            }else
            {
                NSEnumerator * keyiterator = [petInfo  keyEnumerator];
                
                for(id key in keyiterator)
                {
                    NSDictionary * petNodeInfo = [petInfo objectForKey:key];
                    if(! petNodeInfo)
                    {
                        NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"登陆游戏数据异常，返回的小弟数据有问题" userInfo:nil];
                        @throw ex;
                    }else
                    {
                        FOLLOWER_DATA stInfo;
                        
                        // 数据ID
                        stInfo.iFId = [[petNodeInfo objectForKey:@"ID"] intValue];
                        
                        // 服务器ID
                        stInfo.iFServerID = [key intValue];
                        
                        // 等级
                        stInfo.iFLevel = [[petNodeInfo objectForKey:@"Level"] intValue];
                        
                        // 经验
                        float TeampLevel = stInfo.iFLevel - 1;
                        int iAllExp = [[petNodeInfo objectForKey:@"Exp"] intValue];
                        //int CurrentLevelExp = int(int(1.4f * pow((TeampLevel + 1), 2) * pow(TeampLevel, 2) / 4.0f - TeampLevel * (TeampLevel + 1) * (TeampLevel * 2.0f + 1) / 2.0f + 10.0f * TeampLevel * (TeampLevel + 1) / 2.0f + 192 * TeampLevel) * SystemDataManage::ShareInstance() -> GetData_ForFollower(stInfo.iFId) -> Follower_RegulateLevelUpExperience);
                        int CurrentLevelExp = int(((25.0f / 6.0f) * (TeampLevel * (TeampLevel + 1) * (2 * TeampLevel + 1)) - (25.0f / 2.0f) * (TeampLevel * (TeampLevel + 1)) + 200 * TeampLevel) *  SystemDataManage::ShareInstance() -> GetData_ForFollower(stInfo.iFId) -> Follower_RegulateLevelUpExperience);
                        stInfo.iFExperience = iAllExp - CurrentLevelExp;
                        
                        // 技能等级
                        stInfo.iFSkillLevel = [[petNodeInfo objectForKey:@"skill_lv"] intValue];
                        
                        // 性格
                        stInfo.iFCharacterID = [[petNodeInfo objectForKey:@"Character"] intValue];
                        
                        // 状态
                        stInfo.iFTeamState = [[petNodeInfo objectForKey:@"State"] intValue];
                        
                        //HP
                        stInfo.iFHp = [[petNodeInfo objectForKey:@"HP"] intValue];
                        
                        //攻击力
                        stInfo.iFAttack = [[petNodeInfo objectForKey:@"Attack"] intValue];
                        
                        //防御力
                        stInfo.iFDefense = [[petNodeInfo objectForKey:@"Defense"] intValue];
                        
                        //回复力
                        stInfo.iFComeBack = [[petNodeInfo objectForKey:@"Reply"] intValue];
                        
                        //所需升级经验
                        stInfo.iFLevelUpExperience = [[petNodeInfo objectForKey:@"next_lv"] intValue] - iAllExp;
                        
                        //被消耗给予的经验值
                        stInfo.iFBeAbsorbExperience = [[petNodeInfo objectForKey:@"ExpQu"] intValue];
                        
                        stData.vecFollowerList.push_back(stInfo);
                    }
                }
            }
        }
        
        // 获得副本信息
        BattleAfficheInfoList * pMapList = ServerDataManage::ShareInstance()->GetBattleAfficheInfoList();
        if (pMapList != NULL)
        {
            //先清理一遍
            ServerDataManage::ShareInstance() -> ClearBattleAfficheInfo();
            
            NSDictionary * missionInfo = [info objectForKey:@"raid"];
            if (! missionInfo)
            {
                NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求登陆游戏，返回的副本信息数据异常" userInfo:nil];
                @throw ex;
            }else
            {
                int index = 1;
                NSEnumerator* keyiterator = [missionInfo keyEnumerator];
                for(id key in keyiterator)
                {
                    NSDictionary* dict = [missionInfo objectForKey:key];
                    if (dict)
                    {
                        // 使用new分配，在ServerDataManage中释放
                        BattleAfficheInfo* pInfo = new BattleAfficheInfo();
                        if (pInfo == NULL)
                            continue;
                        
                        //大副本的ID
                        pInfo->MapIndex = [[dict objectForKey:@"f_id"] intValue];
                        
                        //小副本的ID
                        pInfo->SubMapIndex = [key intValue];
                        
                        // 大副本使用的图片的ID
                        pInfo->MapImageID = [[dict objectForKey:@"f_id"] intValue];
                        
                        // 大副本使用的名称的ID
                        pInfo->MapNameID = 0;
                        
                        // 小副本使用的图片的ID
                        pInfo->SubMapImageID = index;
                        
                        // 小副本的战斗步骤数
                        pInfo->SubMapBattleStep = [[dict objectForKey:@"step"] intValue];
                        
                        // 小副本消耗的行动力
                        pInfo->SubMapActivity = [[dict objectForKey:@"action"] intValue];
                        
                        // 小副本当前状态
                        pInfo->SubMapState = [[dict objectForKey:@"enter"] intValue];
                        
                        // 小副本的开启时间
                        pInfo->SubMapOpenTime = [[dict objectForKey:@"time"] intValue];
                        // 小副本的持续时间
                        pInfo->SubMapKeepTime = [[dict objectForKey:@"sustain"] intValue];
                        
                        // 副本图片文件名
                        NSString* mapName = [dict objectForKey:@"f_img"];
                        memcpy(pInfo->MapImageFilename, [mapName UTF8String], 32);
                        
                        // 小副本图片文件名
                        NSString* subMapName = [dict objectForKey:@"img"];
                        memcpy(pInfo->SubMapImageFilename, [subMapName UTF8String], 32);
                        
                        // 副本名称
                        NSString* name = [dict objectForKey:@"f_name"];
                        memcpy(pInfo->MapName, [name UTF8String], 32);
                        
                        // 添加到列表
                        pMapList->insert(make_pair(pInfo->SubMapIndex, pInfo));
                        
                        ++index;
                    }
                }
            }
        }
        
        // 获得签到信息
        NSDictionary* pSign = [info objectForKey:@"login"];
        if (pSign)
        {
            // 当月几月
            ServerDataManage::ShareInstance()->m_stSign.iTime = [[pSign objectForKey:@"now"] intValue];
            
            // 领取金币
            ServerDataManage::ShareInstance()->m_stSign.iMoney = 5000;//[[pSign objectForKey:@"money"] intValue];
            
            // 累计天数
            ServerDataManage::ShareInstance()->m_stSign.iCount = [[pSign objectForKey:@"count"] intValue];
            
            // date
            NSDictionary* dataInfo = [pSign objectForKey:@"date"];
            if ((NSNull*)dataInfo != [NSNull null])
            {
                // 生成解析器
                SBJsonParser* parser = [[SBJsonParser alloc] init];
                
                // 得到数组数据
                NSError* error = nil;
                NSData* jsonData =[NSJSONSerialization dataWithJSONObject:dataInfo options:NSJSONWritingPrettyPrinted error:&error];
                NSString* string = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                NSArray* list = [[NSArray alloc] initWithArray:[parser objectWithString:string]];
                
                // 便利数据
                for (NSDictionary* dict in list)
                {
                    NSString* data = (NSString*)dict;
                    
                    // 是否已存在
                    for (int i = 0; i < ServerDataManage::ShareInstance()->m_stSign.m_vecSignTime.size(); i++)
                    {
                        if (ServerDataManage::ShareInstance()->m_stSign.m_vecSignTime[i] == [data intValue])
                            continue;
                    }
                    
                    ServerDataManage::ShareInstance()->m_stSign.m_vecSignTime.push_back([data intValue]);
                }
                
                // 释放解析器
                [string release];
                [list release];
                [parser release];
            }
            
            // box
            NSDictionary* boxInfo = [pSign objectForKey:@"box"];
            if ((NSNull*)boxInfo != [NSNull null])
            {
                NSString* index;
                int arr[] = {1, 3, 5, 7, 15, 30};
                for (int i = 0; i < 5; i++)
                {
                    index = [NSString stringWithFormat:@"%d", arr[i]];
                    
                    NSString* value = [boxInfo objectForKey:index];
                    if (value)
                    {
                        switch (arr[i]) {
                            case 1:
                                ServerDataManage::ShareInstance()->m_stSign.m_arrAward[0] = 1;
                                break;
                            case 3:
                                ServerDataManage::ShareInstance()->m_stSign.m_arrAward[1] = 1;
                                break;
                            case 5:
                                ServerDataManage::ShareInstance()->m_stSign.m_arrAward[2] = 1;
                                break;
                            case 7:
                                ServerDataManage::ShareInstance()->m_stSign.m_arrAward[3] = 1;
                                break;
                            case 15:
                                ServerDataManage::ShareInstance()->m_stSign.m_arrAward[4] = 1;
                                break;
                            case 30:
                                ServerDataManage::ShareInstance()->m_stSign.m_arrAward[5] = 1;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
        
        // 建造楼层
        vector<FLOOR_DATA>::iterator floorIter;
        for (floorIter = stData.vecFloorList.begin(); floorIter != stData.vecFloorList.end(); floorIter++)
            PlayerDataManage::ShareInstance() -> CreateNewFloor(floorIter->iType, floorIter->iLevel, floorIter->iTime);
        
        // 创建小弟
        PlayerDataManage::ShareInstance() -> ClearFollowerDepot();// 先清理一遍
        
        vector<FOLLOWER_DATA>::iterator followerIter;
        for (followerIter = stData.vecFollowerList.begin(); followerIter != stData.vecFollowerList.end(); followerIter++)
        {
            Follower * pFollower = Follower::node();
            
            //如果是直接进入战斗的引导，就不绑定图片
            if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
                pFollower -> SetData(SystemDataManage::ShareInstance() -> GetData_ForFollower(followerIter->iFId), false);
            //正常的游戏流程
            else
                pFollower -> SetData(SystemDataManage::ShareInstance() -> GetData_ForFollower(followerIter->iFId));
            
            pFollower -> GetData() -> Follower_ServerID = followerIter -> iFServerID;
            pFollower -> GetData() -> Follower_Level = followerIter -> iFLevel;
            pFollower -> GetData() -> Follower_Experience = followerIter -> iFExperience;
            pFollower -> GetData() -> Follower_LevelUpExperience = followerIter -> iFLevelUpExperience;
            pFollower -> GetData() -> Follower_BeAbsorbExperience = followerIter -> iFBeAbsorbExperience;
            pFollower -> GetData() -> Follower_CharacterID = followerIter -> iFCharacterID;
            pFollower -> GetData() -> Follower_HP = followerIter -> iFHp;
            pFollower -> GetData() -> Follower_Attack = followerIter -> iFAttack;
            pFollower -> GetData() -> Follower_Defense = followerIter -> iFDefense;
            pFollower -> GetData() -> Follower_Comeback = followerIter -> iFComeBack;
            pFollower -> GetData() -> Follower_SkillLevel = followerIter -> iFSkillLevel;
            
            PlayerDataManage::ShareInstance()->AddFollowerToDepot(pFollower, followerIter -> iFTeamState);
        }
        
        printf("玩家登陆请求完成\n");
        
        //继续loading
        Scene_Loading::ShareInstance() -> ContinueLoading();
    }
    @catch (NSException * exception)
    {
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Scene_Loading::StopLoading));
        Func -> retain();
        ShowMessage(BType_Firm, [[exception reason] UTF8String], Func);
        
        [exception release];
    }
    @finally
    {

    }
}

- (void) GameLoginReqestFailed : (ASIHTTPRequest *) request
{
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Scene_Loading::StopLoading));
        Func -> retain();
        ShowMessage(BType_Firm, "登陆无法连接服务器，请检查网络或者重新登陆游戏。", Func);
    }
    NSLog(@"%@", [request error]);
} 
/************************************************************************/
#pragma mark - 
/************************************************************************/



/************************************************************************/
#pragma mark 引导步骤完成以后通知服务器
/************************************************************************/
- (void) GuideDoneReqest:(int) step
{
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(GuideDoneReqestFinish:)];
    [request setDidFailSelector:@selector(GuideDoneReqestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSString * info = [NSString stringWithFormat:@"%d", step];
    
    [request addPostValue:info forKey:@"GuideStep"];
    
    [request addPostValue:@"guideStepInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) GuideDoneReqestFinish : (ASIHTTPRequest *) request
{
#ifdef GAME_DEBUG
    NSLog(@"引导结果发送完毕\n");
#endif
}

- (void) GuideDoneReqestFailed : (ASIHTTPRequest *) request
{

}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求好友申请列表
/************************************************************************/
- (void) ApplyListRequest
{
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(ApplyListRequestFinish:)];
    [request setDidFailSelector:@selector(ApplyListRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    [request addPostValue:@"getApplyList" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) ApplyListRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家索取好友请求返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"取好友失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家索取好友请求返回数据异常" userInfo:nil];
            @throw ex;
        }
        NSDictionary * info = [object objectForKey:@"info"];
        if (!info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家索取好友请求返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        // 处理请求信息
        NSMutableArray * ReadDataArray = nil;
        
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * documentsDirectory = [paths objectAtIndex:0];
        if (!documentsDirectory) return;
        
        NSString * FolderName = [NSString stringWithUTF8String:PlayerDataManage::ShareInstance() -> m_Player91ID];
        NSString * FolderPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:FolderName];
        if([[NSFileManager defaultManager] fileExistsAtPath:FolderPath] == NO)
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:FolderPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        NSString * appFile = [FolderPath stringByAppendingPathComponent:@"MailInfo"];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:appFile])
            ReadDataArray = [NSMutableArray arrayWithContentsOfFile:appFile];
        
        if(! ReadDataArray)
        {
            NSMutableArray * WriteDataArray = [[NSMutableArray alloc] init];
            for(id obj in info)
            {
                //0
                int pID = [[obj objectForKey:@"uid"] intValue];
                NSString * SenderID = [NSString stringWithFormat:@"%d", pID];
                
                //1
                NSString * SenderName = [obj objectForKey:@"uname"];
                
                //2
                int pLevel = [[obj objectForKey:@"lv"] intValue];
                NSString * SenderLevel = [NSString stringWithFormat:@"%d", pLevel];
                
                //3
                int pTime = [[obj objectForKey:@"time"] intValue];
                NSString * SendTime = [NSString stringWithFormat:@"%d", pTime];
                
                //4
                int pFollowerID = [[obj objectForKey:@"pet_id"] intValue];
                NSString * SenderFollowerID = [NSString stringWithFormat:@"%d", pFollowerID];
                
                //5
                int pFollowerLevel = [[obj objectForKey:@"pet_lv"] intValue];
                NSString * SenderFollowerLevel = [NSString stringWithFormat:@"%d", pFollowerLevel];
                
                //6
                NSString * SendType = [NSString stringWithFormat:@"%d", MailType_FriendApply];
                
                //7
                NSString * MailState = [NSString stringWithFormat:@"%d", MailState_New];
                
                NSArray * ApplyInfo = [NSArray arrayWithObjects:SenderID, SenderName, SenderLevel, SendTime, SenderFollowerID, SenderFollowerLevel, SendType, MailState, nil];
                [WriteDataArray addObject:ApplyInfo];
            }
            
            [WriteDataArray writeToFile:appFile atomically:NO];
            [WriteDataArray release];
        }
        else
        {
            NSDictionary * info = [object objectForKey:@"info"];
            
            if([info count] != 0)
            {
                for(id obj in info)
                {
                    //0
                    int pID = [[obj objectForKey:@"uid"] intValue];
                    NSString * SenderID = [NSString stringWithFormat:@"%d", pID];
                    
                    //1
                    NSString * SenderName = [obj objectForKey:@"uname"];
                    
                    //2
                    int pLevel = [[obj objectForKey:@"lv"] intValue];
                    NSString * SenderLevel = [NSString stringWithFormat:@"%d", pLevel];
                    
                    //3
                    int pTime = [[obj objectForKey:@"time"] intValue];
                    NSString * SendTime = [NSString stringWithFormat:@"%d", pTime];
                    
                    //4
                    int pFollowerID = [[obj objectForKey:@"pet_id"] intValue];
                    NSString * SenderFollowerID = [NSString stringWithFormat:@"%d", pFollowerID];
                    
                    //5
                    int pFollowerLevel = [[obj objectForKey:@"pet_lv"] intValue];
                    NSString * SenderFollowerLevel = [NSString stringWithFormat:@"%d", pFollowerLevel];
                    
                    //6
                    NSString * SendType = [NSString stringWithFormat:@"%d", MailType_FriendApply];
                    
                    //7
                    NSString * MailState = [NSString stringWithFormat:@"%d", MailState_New];
                    
                    NSArray * ApplyInfo = [NSArray arrayWithObjects:SenderID, SenderName, SenderLevel, SendTime, SenderFollowerID, SenderFollowerLevel, SendType, MailState, nil];
                    [ReadDataArray addObject:ApplyInfo];
                }
                [ReadDataArray writeToFile:appFile atomically:NO];
            }
        }
        
        //继续loading
        Scene_Loading::ShareInstance() -> ContinueLoading();
    }
    @catch (NSException * exception)
    {
        if(Scene_Loading::ShareInstance() -> GetIsLoading())
        {
            CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Scene_Loading::StopLoading));
            Func -> retain();
            ShowMessage(BType_Firm, [[exception reason] UTF8String], Func);
        }else
            ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) ApplyListRequestFailed : (ASIHTTPRequest *) request
{
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        if(Scene_Loading::ShareInstance() -> GetIsLoading())
        {
            CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Scene_Loading::StopLoading));
            Func -> retain();
            ShowMessage(BType_Firm, "游戏连接超时，请检查网络或者重新登陆游戏。", Func);
        }else
            ShowMessage(BType_Firm, "游戏连接超时，请检查网络或者重新登陆游戏。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark - 
/************************************************************************/



/************************************************************************/
#pragma mark  向服务器请求建造新的楼层
/************************************************************************/
- (void) NewFloorReqest : (int) type
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(NewFloorReqestFinish:)];
    [request setDidFailSelector:@selector(NewFloorReqestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * n1 = [[NSNumber alloc] initWithInt:type];
    [request addPostValue:n1 forKey:@"floorType"];
    
    [request addPostValue:@"buildNewFloorInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) NewFloorReqestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家新建楼层请求，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"建楼层失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家新建楼层请求，返回数据异常" userInfo:nil];
            @throw ex;
        }
        NSDictionary * info = [object objectForKey:@"info"];
        if (!info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家新建楼层请求，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        id  pFloorType = [info objectForKey:@"id"];
        int FloorType = [pFloorType intValue];
        
        //新建成功
        ServerDataManage::ShareInstance()->RequestNewFloorSuccess(FloorType);
    
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException * exception)
    {
        //结束联网动画
        RemoveNetConnect();
    
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) NewFloorReqestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark - 
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求升级楼层
/************************************************************************/
- (void) LevelUpFloorRequest:(int) type
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(LevelUpFloorReFinish:)];
    [request setDidFailSelector:@selector(LevelUpFloorFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber* n1 = [[NSNumber alloc] initWithInt:type];
    [request addPostValue:n1 forKey:@"floor_type"];
    
    [request addPostValue:@"buildUpgrade" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) LevelUpFloorReFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家升级楼层请求，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"升级楼层失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家升级楼层请求，返回数据异常" userInfo:nil];
            @throw ex;
        }else
        {
            int type = [[object objectForKey:@"type"] intValue];
            int level = [[object objectForKey:@"lv"] intValue];
            int endTime = [[object objectForKey:@"endTime"] intValue];
            int getMoney = [[object objectForKey:@"getMoney"] intValue];
            
            ServerDataManage::ShareInstance() -> RequestLevelUpFloorSuccess(type, level, endTime, getMoney);
            
            //结束联网动画
            RemoveNetConnect();
        }
    }
    @catch (NSException * exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) LevelUpFloorFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark - 
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求收获楼层
/************************************************************************/
- (void) HarvestFloorRequest:(int) type : (int) fast
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(HarvestFloorRequestFinish:)];
    [request setDidFailSelector:@selector(HarvestFloorRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber* n1 = [[NSNumber alloc] initWithInt:type];
    [request addPostValue:n1 forKey:@"floor_type"];
    
    NSNumber* n2 = [[NSNumber alloc] initWithInt:fast];
    [request addPostValue:n2 forKey:@"fast"];
    
    [request addPostValue:@"harvestFloorInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) HarvestFloorRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求收获楼层，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"收获楼层失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求收获楼层，返回数据异常" userInfo:nil];
            @throw ex;
        }else
        {
            int type = [[object objectForKey:@"floor_type"] intValue];
            int getMoney = [[object objectForKey:@"getMoney"] intValue];
            id idFast = [object objectForKey:@"fast"];
            if(idFast)
            {
                int fast = [idFast intValue];
                if(fast == 1)
                {
                    id value = [object objectForKey:@"value"];
                    int PlayerRMB = [value intValue];
                    PlayerDataManage::m_PlayerRMB = PlayerRMB;
                }
            }
            
            //收获楼层成功
            ServerDataManage::ShareInstance() -> RequestFloorHarvestSuccess(type, getMoney);
            
            //结束联网动画
            RemoveNetConnect();
        }
    }
    @catch (NSException * exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) HarvestFloorRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark - 
/************************************************************************/




/************************************************************************/
#pragma mark 向服务器提交编辑好的小弟的队伍
/************************************************************************/
- (void) ReferMyFollowerTeamRequest
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(ReferMyFollowerTeamRequestFinish:)];
    [request setDidFailSelector:@selector(ReferMyFollowerTeamRequestFailed:)];
    
    PlayerFollowerTeam pList = *PlayerDataManage::ShareInstance() -> GetMyFollowerTeam();
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * f1teamIndex = [[NSNumber alloc] initWithInt:pList[1] -> GetData() -> Follower_ServerID];
    [request addPostValue:f1teamIndex forKey:@"teamIndex1"];
    
    NSNumber * f2teamIndex = [[NSNumber alloc] initWithInt:pList[2] -> GetData() -> Follower_ServerID];
    [request addPostValue:f2teamIndex forKey:@"teamIndex2"];
    
    NSNumber * f3teamIndex = [[NSNumber alloc] initWithInt:pList[3] -> GetData() -> Follower_ServerID];
    [request addPostValue:f3teamIndex forKey:@"teamIndex3"];
    
    NSNumber * f4teamIndex = [[NSNumber alloc] initWithInt:pList[4] -> GetData() -> Follower_ServerID];
    [request addPostValue:f4teamIndex forKey:@"teamIndex4"];
    
    [request addPostValue:@"ReferMyFollowerTeamInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) ReferMyFollowerTeamRequestFinish : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
}

- (void) ReferMyFollowerTeamRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark - 
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求商店中小弟列表和解析
/************************************************************************/
- (void) WareFollowerRequest
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(WareFollowerRequestFinish:)];
    [request setDidFailSelector:@selector(WareFollowerRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    [request addPostValue:@"wareFollowerInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) WareFollowerRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求商店数据，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"请求商店数据失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求商店数据，返回数据异常" userInfo:nil];
            @throw ex;
        }else
        {
            NSDictionary * info = [object objectForKey:@"info"];
            int TypeIndex = 1;
            for (id obj in info)
            {
                NSString * NSTypeIndex = [NSString stringWithFormat:@"%d", TypeIndex];
                NSDictionary * TypeNode = [info objectForKey:NSTypeIndex];
                
                int FollowerIndex = 1;
                for (id obj in TypeNode)
                {
                    NSString * NSFollowerIndex = [NSString stringWithFormat:@"%d", FollowerIndex];
                    NSDictionary * FollowerNode = [TypeNode objectForKey:NSFollowerIndex];
                    
                    if([FollowerNode count] != 0)
                    {
                        id  pID = [FollowerNode objectForKey:@"ID"];
                        int FollowerID = [pID intValue];
                        
                        id pCharacter = [FollowerNode objectForKey:@"Character"];
                        int CharacterID = [pCharacter intValue];
                        
                        Follower * pFollower = Follower::node();
                        pFollower -> SetData(SystemDataManage::ShareInstance() -> GetData_ForFollower(FollowerID));
                        
                        pFollower -> GetData() -> Follower_CharacterID = CharacterID;
                        
                        ServerDataManage::ShareInstance() -> AddWareFollower(TypeIndex, FollowerIndex, pFollower);
                    }
                    ++ FollowerIndex;
                }
                ++ TypeIndex;
            }
            
            KNUIFunction::GetSingle() -> UpdateFollowerInfo();
            KNUIFunction::GetSingle() -> UpdateSuperFollowerInfo();
            
            //结束联网动画
            RemoveNetConnect();
        }
    }
    @catch (NSException * exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
    
    }
}

- (void) WareFollowerRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求刷新商店中小弟列表
/************************************************************************/
- (void) UpdateWareFollowerRequest : (int) type
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(UpdateWareFollowerRequestFinish:)];
    [request setDidFailSelector:@selector(UpdateWareFollowerRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * n1 = [[NSNumber alloc] initWithInt:type];
    [request addPostValue:n1 forKey:@"updateType"];
    
    [request addPostValue:@"updateWareFollowerInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) UpdateWareFollowerRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求刷新商店数据，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"刷新商店数据失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求刷新商店数据，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        id  pUpdateType = [object objectForKey:@"updateType"];
        int UpdateType = [pUpdateType intValue];
        
        ServerDataManage::ShareInstance() -> ClearWareFollower(UpdateType);
        
        NSDictionary * info = [object objectForKey:@"info"];
        for (id obj in info)
        {
            NSString * NSTypeIndex = [NSString stringWithFormat:@"%d", UpdateType];
            NSDictionary * TypeNode = [info objectForKey:NSTypeIndex];
            
            int FollowerIndex = 1;
            for (id obj in TypeNode)
            {
                NSString * NSFollowerIndex = [NSString stringWithFormat:@"%d", FollowerIndex];
                NSDictionary * FollowerNode = [TypeNode objectForKey:NSFollowerIndex];
                
                id  pID = [FollowerNode objectForKey:@"ID"];
                int FollowerID = [pID intValue];
                
                id pCharacter = [FollowerNode objectForKey:@"Character"];
                int CharacterID = [pCharacter intValue];
                
                Follower * pFollower = Follower::node();
                pFollower -> SetData(SystemDataManage::ShareInstance() -> GetData_ForFollower(FollowerID));
                
                pFollower -> GetData() -> Follower_CharacterID = CharacterID;
                
                ServerDataManage::ShareInstance() -> AddWareFollower(UpdateType, FollowerIndex, pFollower);
                
                ++ FollowerIndex;
            }
        }
        
        if(UpdateType == 1)
        {
            // 友情值
            PlayerDataManage::m_PlayerFriendValue = [[object objectForKey:@"value"] intValue];
            KNUIFunction::GetSingle() -> UpdateFollowerInfo(true);
        }
        else if(UpdateType == 2)
        {
            // 钻石值
            PlayerDataManage::m_PlayerRMB = [[object objectForKey:@"value"] intValue];
            KNUIFunction::GetSingle() -> UpdateSuperFollowerInfo(true);
        }
        
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) UpdateWareFollowerRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求购买小弟和解析
/************************************************************************/
- (void) BuyFollowerRequest : (int) type fID: (int) followerIndex
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(BuyFollowerRequestFinish:)];
    [request setDidFailSelector:@selector(BuyFollowerRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * n1 = [[NSNumber alloc] initWithInt:type];
    [request addPostValue:n1 forKey:@"shopType"];
    
    NSNumber * n2 = [[NSNumber alloc] initWithInt:followerIndex];
    [request addPostValue:n2 forKey:@"id"];
    
    [request addPostValue:@"buyFollowerInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) BuyFollowerRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求购买小弟，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"买小弟失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求购买小弟，返回数据异常" userInfo:nil];
            @throw ex;
        }
        NSDictionary * info = [object objectForKey:@"info"];
        if(! info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求购买小弟，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        id  pID = [info objectForKey:@"id"];
        int FollowerIndex = [pID intValue];
        
        id  pServerID = [info objectForKey:@"serverID"];
        int FollowerServerID = [pServerID intValue];
        
        id  pType = [info objectForKey:@"type"];
        int ShopType = [pType intValue];
        
        //血量
        int iFHp = [[object objectForKey:@"HP"] intValue];
        
        //攻击力
        int iFAttack = [[object objectForKey:@"Attack"] intValue];
        
        //防御力
        int iFDefense = [[object objectForKey:@"Defense"] intValue];
        
        //回复力
        int iFComeBack = [[object objectForKey:@"Reply"] intValue];
        
        //所需升级经验
        int iFLevelUpExperience = [[object objectForKey:@"next_lv"] intValue] - 0;
        
        //被消耗给予的经验值
        int iFBeAbsorbExperience = [[object objectForKey:@"ExpQu"] intValue];
        
        ServerDataManage::ShareInstance() -> RequestBuyFollowerSucceed(ShopType, FollowerIndex, FollowerServerID, iFLevelUpExperience, iFBeAbsorbExperience, iFHp, iFAttack, iFDefense, iFComeBack);
        
        if(ShopType == 1)
        {
            KNUIFunction::GetSingle() -> UpdateFollowerInfo(true);
        }
        else if(ShopType == 2)
        {
            KNUIFunction::GetSingle() -> UpdateSuperFollowerInfo(true);
        }
        
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException * exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
    
    }
}

- (void) BuyFollowerRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求训练小弟和解析
/************************************************************************/
- (void) TrainingFollowerRequest : (Follower *) follower Flist:(list <Follower *> *) plist
{
    //开始联网动画
    ShowNetConnect();
    
    list <Follower *>  FList = *plist;
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(TrainingFollowerRequestFinish:)];
    [request setDidFailSelector:@selector(TrainingFollowerRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * n1 = [[NSNumber alloc] initWithInt:follower -> GetData() -> Follower_ServerID];
    [request addPostValue:n1 forKey:@"id"];
    
    int IDArray[FList.size()];
    
    int index = 0;
    for(list <Follower *>::iterator it = FList.begin(); it != FList.end(); ++ it)
    {
        NSNumber * n = [[NSNumber alloc] initWithInt:(*it) -> GetData() -> Follower_ServerID];
        IDArray[index] = [n intValue];
        
        ++ index;
    }
    
    NSString * n3 = NULL;
    if(FList.size() == 1)
        n3 = [NSString stringWithFormat:@"%d", IDArray[0]];
    if(FList.size() == 2)
        n3 = [NSString stringWithFormat:@"%d|%d", IDArray[0], IDArray[1]];
    if(FList.size() == 3)
        n3 = [NSString stringWithFormat:@"%d|%d|%d", IDArray[0], IDArray[1], IDArray[2]];
    if(FList.size() == 4)
        n3 = [NSString stringWithFormat:@"%d|%d|%d|%d", IDArray[0], IDArray[1], IDArray[2], IDArray[3]];
    if(FList.size() == 5)
        n3 = [NSString stringWithFormat:@"%d|%d|%d|%d|%d", IDArray[0], IDArray[1], IDArray[2], IDArray[3], IDArray[4]];
    
    [request addPostValue:n3 forKey:@"list"];
    
    [request addPostValue:@"trainingFollowerInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) TrainingFollowerRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求训练小弟，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger      error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"训练小弟失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求训练小弟，返回数据异常" userInfo:nil];
            @throw ex;
        }
        NSDictionary * info = [object objectForKey:@"info"];
        if(! info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求训练小弟，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        //花费
        int iFprice = [[info objectForKey:@"gb"] intValue];
        
        // 等级
        int iFLevel = [[info objectForKey:@"Level"] intValue];
        
        //总的经验值
        int iAllExp = [[info objectForKey:@"Exp"] intValue];
        
        //技能等级
        int iFSkillLevel = [[info objectForKey:@"skill_lv"] intValue];
        
        //HP
        int iFHp = [[info objectForKey:@"HP"] intValue];
        
        //攻击力
        int iFAttack = [[info objectForKey:@"Attack"] intValue];
        
        //防御力
        int iFDefense = [[info objectForKey:@"Defense"] intValue];
        
        //回复力
        int iFComeBack = [[info objectForKey:@"Reply"] intValue];
        
        //所需升级经验
        int iFLevelUpExperience = [[info objectForKey:@"next_lv"] intValue] - iAllExp;
        
        //被消耗给予的经验值
        int iFBeAbsorbExperience = [[info objectForKey:@"ExpQu"] intValue];
        
        ServerDataManage::ShareInstance() -> RequestTrainingFollowerSucceed(iFprice, iAllExp, iFLevel, iFSkillLevel, iFLevelUpExperience, iFBeAbsorbExperience, iFHp, iFAttack, iFDefense, iFComeBack);
        
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException * exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) TrainingFollowerRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "小弟训练失败，游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求进化小弟和解析
/************************************************************************/
- (void) EvolvementFollowerRequest : (Follower *) follower Flist:(list <Follower *> *) plist
{
    //开始联网动画
    ShowNetConnect();
    
    list <Follower *>  FList = *plist;
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(EvolvementFollowerRequestFinish:)];
    [request setDidFailSelector:@selector(EvolvementFollowerRequestFailed::)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * n1 = [[NSNumber alloc] initWithInt:follower -> GetData() -> Follower_ServerID];
    [request addPostValue:n1 forKey:@"id"];
    
    int IDArray[FList.size()];
    
    int index = 0;
    for(list <Follower *>::iterator it = FList.begin(); it != FList.end(); ++ it)
    {
        NSNumber * n = [[NSNumber alloc] initWithInt:(*it) -> GetData() -> Follower_ServerID];
        IDArray[index] = [n intValue];
        
        ++ index;
    }
    
    NSString * n3 = NULL;
    if(FList.size() == 1)
        n3 = [NSString stringWithFormat:@"%d", IDArray[0]];
    if(FList.size() == 2)
        n3 = [NSString stringWithFormat:@"%d|%d", IDArray[0], IDArray[1]];
    if(FList.size() == 3)
        n3 = [NSString stringWithFormat:@"%d|%d|%d", IDArray[0], IDArray[1], IDArray[2]];
    if(FList.size() == 4)
        n3 = [NSString stringWithFormat:@"%d|%d|%d|%d", IDArray[0], IDArray[1], IDArray[2], IDArray[3]];
    if(FList.size() == 5)
        n3 = [NSString stringWithFormat:@"%d|%d|%d|%d|%d", IDArray[0], IDArray[1], IDArray[2], IDArray[3], IDArray[4]];
    
    [request addPostValue:n3 forKey:@"list"];
    
    [request addPostValue:@"evolvementFollowerInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) EvolvementFollowerRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求进化小弟，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger      error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"进化小弟失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求进化小弟，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        //花费
        int iFPrice = [[object objectForKey:@"gb"] intValue];
        
        //HP
        int iFHp = [[object objectForKey:@"HP"] intValue];
        
        //攻击力
        int iFAttack = [[object objectForKey:@"Attack"] intValue];
        
        //防御力
        int iFDefense = [[object objectForKey:@"Defense"] intValue];
        
        //回复力
        int iFComeBack = [[object objectForKey:@"Reply"] intValue];
        
        //所需升级经验
        int iFLevelUpExperience = [[object objectForKey:@"next_lv"] intValue] - 0;
        
        //被消耗给予的经验值
        int iFBeAbsorbExperience = [[object objectForKey:@"ExpQu"] intValue];
        
        //新的数据ID
        int ID = [[object objectForKey:@"ID"] intValue];
        
        //新的性格ID
        int Character = [[object objectForKey:@"Character"] intValue];
        
        //新的服务器ID
        int serverID = [[object objectForKey:@"serverID"] intValue];
        
        ServerDataManage::ShareInstance() -> RequestEvolvementFollowerSucceed(iFPrice, ID, Character, serverID, iFLevelUpExperience, iFBeAbsorbExperience, iFHp, iFAttack, iFDefense, iFComeBack);
        
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
    
    }
}

- (void) EvolvementFollowerRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "小弟转职失败，游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求出售小弟
/************************************************************************/
- (void) SellFollowerRequest
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(SellFollowerRequestFinish:)];
    [request setDidFailSelector:@selector(SellFollowerRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    string dataString;
    
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        Layer_FollowerList* pList = dynamic_cast<Layer_FollowerList*>(pFrame->GetSubUIByID(70000));
        if (pList != NULL)
        {
            set<Follower*> fireList = pList->GetFireList();
            set<Follower*>::iterator iter;
            for (iter = fireList.begin(); iter != fireList.end(); iter++)
            {
                char szTemp[32] = {0};
                sprintf(szTemp, "%d_", (*iter)->GetData()->Follower_ServerID);
                dataString += szTemp;
            }
        }
    }
    
    // 去掉最后一个_
    dataString = dataString.substr(0, dataString.size() - 1);
    
    NSString* data = [NSString stringWithFormat:@"%s", dataString.c_str()];
    [request addPostValue:data forKey:@"id"];
    
    [request addPostValue:@"sellFollowerInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) SellFollowerRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求出售小弟，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"出售小弟失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求出售小弟，返回数据异常" userInfo:nil];
            @throw ex;
        }
        // 解析价格
        NSDictionary* info = [object objectForKey:@"info"];
        if (! info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求出售小弟，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        ServerDataManage::ShareInstance()->RequestSellFollowerSucceed([[info objectForKey:@"gb"] intValue]);
        
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
    
    }
   
}

- (void) SellFollowerRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "出售小弟失败，游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器搜索好友信息
/************************************************************************/
- (void) FindFriendInfoRequest: (int) userID
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(FindFriendInfoRequestFinish:)];
    [request setDidFailSelector:@selector(FindFriendInfoRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * n1 = [[NSNumber alloc] initWithInt:userID];
    [request addPostValue:n1 forKey:@"friendUID"];
    
    [request addPostValue:@"searchFriend" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) FindFriendInfoRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求搜索好友，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"搜索好友失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求搜索好友，返回数据异常" userInfo:nil];
            @throw ex;
        }
        NSDictionary* info = [object objectForKey:@"info"];
        if (! info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求搜索好友，返回数据异常" userInfo:nil];
            @throw ex;
        }else
        {
            // 解析数据
            FriendInfo stInfo;
            
            // 姓名
            NSString * userName = [info objectForKey:@"uname"];
            memcpy(stInfo.Fri_UserName, [userName UTF8String], 32);
            
            // uid
            stInfo.Fri_UserID = [[object objectForKey:@"uid"] intValue];
            
            // 等级
            stInfo.Fri_UserLevel = [[info objectForKey:@"lv"] intValue];
            
            // 小弟等级
            stInfo.Fri_FollowerLevel = [[info objectForKey:@"pet_lv"] intValue];
            
            // 小弟ID
            stInfo.Fri_FollowerID = [[info objectForKey:@"pet_id"] intValue];
            
            ServerDataManage::ShareInstance()->RequestFindFriendByUserIDSucceed(stInfo);
            
            //结束联网动画
            RemoveNetConnect();
        }
    }
    @catch (NSException * exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
    
    }
}

- (void) FindFriendInfoRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求删除好友
/************************************************************************/
- (void) RemoveFriendRequest: (int) userID
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(RemoveFriendRequestFinish:)];
    [request setDidFailSelector:@selector(RemoveFriendRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * n1 = [[NSNumber alloc] initWithInt:userID];
    [request addPostValue:n1 forKey:@"friendUID"];
    
    [request addPostValue:@"removeFriend" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) RemoveFriendRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求删除好友，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger      error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"删除好友失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求删除好友，返回数据异常" userInfo:nil];
            @throw ex;
        }
        // 解析信息
        NSDictionary* info = [object objectForKey:@"info"];
        if (! info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求删除好友，返回数据异常" userInfo:nil];
            @throw ex;
        }else
        {
            int iRemoveID = [[info objectForKey:@"fid"] intValue];
            if (iRemoveID == 0)
            {
                NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求删除好友数据错误，服务器无该小弟" userInfo:nil];
                @throw ex;
            }
            
            // 成功
            ServerDataManage::ShareInstance()->RequestRemoveFriendByUserIDSucceed(iRemoveID);
            
            // 刷新界面
            vector<int> vecParmList;
            vecParmList.push_back(FOLLOWER_FRIEND_EDIT_FRAME_ID);
            vecParmList.push_back(TRAIN_PLATFORM_FRAME_ID);
            
            closeFrameAndOpenOthers(vecParmList);
        }
        
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
    
    }
}

- (void) RemoveFriendRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求加好友
/************************************************************************/
- (void) AddFriendByUserIDRequest: (int) userID
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(AddFriendByUserIDRequestFinish:)];
    [request setDidFailSelector:@selector(AddFriendByUserIDRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * n1 = [[NSNumber alloc] initWithInt:userID];
    [request addPostValue:n1 forKey:@"friendUID"];
    
    NSNumber * n2 = [[NSNumber alloc] initWithInt:2];
    [request addPostValue:n2 forKey:@"messageType"];
    
    [request addPostValue:@"sendApply" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) AddFriendByUserIDRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"玩家请求添加好友，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger      error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"加好友失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException * exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
    
    }
}

- (void) AddFriendByUserIDRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求同意添加好友
/************************************************************************/
- (void) AgreeAddFriendRequest: (int) friendServerID
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(AgreeAddFriendRequestFinish:)];
    [request setDidFailSelector:@selector(AgreeAddFriendRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * n1 = [[NSNumber alloc] initWithInt:friendServerID];
    [request addPostValue:n1 forKey:@"friendUID"];
    
    NSNumber * n2 = [[NSNumber alloc] initWithInt:2];
    [request addPostValue:n2 forKey:@"messageType"];
    
    [request addPostValue:@"addFriend" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) AgreeAddFriendRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"向服务器请求同意添加好友，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"同意加好友失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"向服务器请求同意添加好友，返回数据异常" userInfo:nil];
            @throw ex;
        }
        NSDictionary* info = [object objectForKey:@"info"];
        if(! info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"向服务器请求同意添加好友，返回数据异常" userInfo:nil];
            @throw ex;
        }else
        {
            FriendInfo stInfo;
            
            // 姓名
            NSString* name = [info objectForKey:@"uname"];
            memcpy(stInfo.Fri_UserName, [name UTF8String], CHARLENGHT);
            
            // uid
            stInfo.Fri_UserID = [[info objectForKey:@"uid"] intValue];
            
            // user level
            stInfo.Fri_UserLevel = [[info objectForKey:@"lv"] intValue];
            
            // 可用
            stInfo.Fri_CanBeUse = 1;
            
            // 小弟id
            stInfo.Fri_FollowerID = [[info objectForKey:@"pet_id"] intValue];
            
            // 性格id
            stInfo.Fri_FollowerCharacterID = [[info objectForKey:@"pet_character"] intValue];
            
            // 攻击力
            stInfo.Fri_FollowerAttack = [[info objectForKey:@"pet_atta"] intValue];
            stInfo.Fri_FollowerDefense = [[info objectForKey:@"pet_defe"] intValue];
            stInfo.Fri_FollowerHp = [[info objectForKey:@"pet_hp"] intValue];
            stInfo.Fri_FollowerReply = [[info objectForKey:@"pet_reply"] intValue];
            stInfo.Fri_FollowerLevel = [[info objectForKey:@"pet_lv"] intValue];
            stInfo.Fri_FollowerSkillLevel = 0;
            stInfo.Fri_FollowerFriendValue = 0;
            stInfo.Fri_FollowerQuality = [[info objectForKey:@"pet_qu"] intValue];
            
            const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(stInfo.Fri_FollowerID);
            if (pData != NULL)
                stInfo.Fri_FollowerMaxQuality = pData->Follower_MaxQuality;
            
            // 添加好友成功
            ServerDataManage::ShareInstance()->RequestAgreeAddFriendSucceed(stInfo);
        }
        
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        [exception release];
    }
    @finally
    {
    
    }
}

- (void) AgreeAddFriendRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器请求好友信息
/************************************************************************/
- (void) FriendListRequest
{
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(FriendListRequestFinish:)];
    [request setDidFailSelector:@selector(FriendListRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    [request addPostValue:@"getFriendPets" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) FriendListRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"向服务器请求好友信息，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"请求好友信息失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"向服务器请求好友信息，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        // 创建好友小弟列表
        list<FriendInfo> friendList;
        list<FriendInfo> strangerList;
        
        // 解析变量
        NSDictionary* dict = nil;
        NSDictionary* pet = nil;
        NSArray* keys = nil;
        id key = nil;
        id value = nil;
        
        // 解析好友信息
        dict = [object objectForKey:@"friend"];
        if (dict)
        {
            keys = [dict allKeys];
            for (int i = 0; i < [keys count]; i++)
            {
                // 取键值
                key = [keys objectAtIndex:i];
                // 取数据
                value = [dict objectForKey:key];
                
                // 解析数据
                
                if (value)
                {
                    FriendInfo stInfo;
                    
                    // uid
                    stInfo.Fri_UserID = [key intValue];
                    
                    // 姓名
                    NSString* name = [value objectForKey:@"name"];
                    memcpy(stInfo.Fri_UserName, [name UTF8String], CHARLENGHT);
                    
                    // 等级
                    stInfo.Fri_UserLevel = [[value objectForKey:@"lv"] intValue];
                    
                    // 好友的小弟
                    pet = [value objectForKey:@"pet"];
                    if ((NSNull*)pet != [NSNull null])
                    {
                        // id
                        stInfo.Fri_FollowerID = [[pet objectForKey:@"ID"] intValue];
                        
                        // level
                        stInfo.Fri_FollowerLevel = [[pet objectForKey:@"Level"] intValue];
                        
                        // 性格id
                        stInfo.Fri_FollowerCharacterID = [[pet objectForKey:@"Character"] intValue];
                        
                        // 攻击力
                        stInfo.Fri_FollowerAttack = [[pet objectForKey:@"Attack"] intValue];
                        
                        // 防御力
                        stInfo.Fri_FollowerDefense = [[pet objectForKey:@"Defense"] intValue];
                        
                        // hp
                        stInfo.Fri_FollowerHp = [[pet objectForKey:@"HP"] intValue];
                        
                        // 回复力
                        stInfo.Fri_FollowerReply = [[pet objectForKey:@"Reply"] intValue];
                        
                        // 技能等级
                        stInfo.Fri_FollowerSkillLevel = [[pet objectForKey:@"skill_lv"] intValue];
                        
                        // 友情值
                        stInfo.Fri_FollowerFriendValue = [[pet objectForKey:@"friend"] intValue];
                        
                        // quality
                        stInfo.Fri_FollowerQuality = [[pet objectForKey:@"Quality"] intValue];
                        
                        // 可用
                        stInfo.Fri_CanBeUse = [[pet objectForKey:@"useable"] intValue];
                    }
                    
                    // 添加到列表
                    friendList.push_back(stInfo);
                }
            }
        }
        
        // 解析陌生人
        dict = [object objectForKey:@"stranger"];
        if (dict)
        {
            keys = [dict allKeys];
            for (int i = 0; i < [keys count]; i++)
            {
                // 取键值
                key = [keys objectAtIndex:i];
                // 取数据
                value = [dict objectForKey:key];
                
                // 解析数据
                if (value)
                {
                    FriendInfo stInfo;
                    
                    // uid
                    stInfo.Fri_UserID = [key intValue];
                    
                    // 姓名
                    NSString* name = [value objectForKey:@"name"];
                    memcpy(stInfo.Fri_UserName, [name UTF8String], CHARLENGHT);
                    
                    // 等级
                    stInfo.Fri_UserLevel = [[value objectForKey:@"lv"] intValue];
                    
                    // 属性
                    pet = [value objectForKey:@"pet"];
                    if ((NSNull*)pet != [NSNull null])
                    {
                        // id
                        stInfo.Fri_FollowerID = [[pet objectForKey:@"ID"] intValue];
                        
                        // level
                        stInfo.Fri_FollowerLevel = [[pet objectForKey:@"Level"] intValue];
                        
                        // 性格id
                        stInfo.Fri_FollowerCharacterID = [[pet objectForKey:@"Character"] intValue];
                        
                        // 攻击力
                        stInfo.Fri_FollowerAttack = [[pet objectForKey:@"Attack"] intValue];
                        
                        // 防御力
                        stInfo.Fri_FollowerDefense = [[pet objectForKey:@"Defense"] intValue];
                        
                        // hp
                        stInfo.Fri_FollowerHp = [[pet objectForKey:@"HP"] intValue];
                        
                        // 回复力
                        stInfo.Fri_FollowerReply = [[pet objectForKey:@"Reply"] intValue];
                        
                        // 技能等级
                        stInfo.Fri_FollowerSkillLevel = [[pet objectForKey:@"skill_lv"] intValue];
                        
                        // 友情值
                        stInfo.Fri_FollowerFriendValue = [[pet objectForKey:@"friend"] intValue];
                        
                        // quality
                        stInfo.Fri_FollowerQuality = [[pet objectForKey:@"Quality"] intValue];
                        
                        // 可用
                        stInfo.Fri_CanBeUse = 1;//[[pet objectForKey:@"useable"] intValue];
                        
                        // 插入到列表
                        strangerList.push_back(stInfo);
                    }
                }
            }
        }
        
        // 放入到数据列表
        ServerDataManage::ShareInstance() -> ClearFriendList(0);
        ServerDataManage::ShareInstance()->m_FriendInfoList.insert(make_pair(0, friendList));

        ServerDataManage::ShareInstance() -> ClearFriendList(1);
        ServerDataManage::ShareInstance()->m_FriendInfoList.insert(make_pair(1, strangerList));
        
        //继续loading
        Scene_Loading::ShareInstance() -> ContinueLoading();
        
        printf("向服务器请求好友信息完成\n");
    }
    @catch (NSException * exception)
    {
        if(Scene_Loading::ShareInstance() -> GetIsLoading())
        {
            CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Scene_Loading::StopLoading));
            Func -> retain();
            ShowMessage(BType_Firm, [[exception reason] UTF8String], Func);
        }else
            ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        
        [exception release];
    }
    @finally
    {
    
    }
}

- (void) FriendListRequestFailed : (ASIHTTPRequest *) request
{
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        if(Scene_Loading::ShareInstance() -> GetIsLoading())
        {
            CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Scene_Loading::StopLoading));
            Func -> retain();
            ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。", Func);
        }else
            ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark  向服务器请求进入副本的副本战斗信息
/************************************************************************/
- (void) EnterBattleRequest : (NSInteger) mapID
{
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(EnterBattleRequestFinish:)];
    [request setDidFailSelector:@selector(EnterBattleRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * n1 = [[NSNumber alloc] initWithInt:mapID];
    [request addPostValue:n1 forKey:@"floor_id"];
    
    NSNumber* n2 = [[NSNumber alloc] initWithInt:Scene_Battle::m_MissionFriendFollowerID];
    [request addPostValue:n2 forKey:@"friend_id"];
    
    [request addPostValue:@"raidInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) EnterBattleRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        BattleInfo * pBattleData = new BattleInfo();
        
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"请求进入副本，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];

        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"进入副本失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"请求进入副本，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        // 解析行动力数据
        PlayerDataManage::m_PlayerActivity = [[object objectForKey:@"action"] intValue];
        
        //如果玩家当前的行动力不满，就开始倒计时
        if(PlayerDataManage::m_PlayerActivity < PlayerDataManage::m_PlayerMaxActivity)
        {
            if(PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime == 0)
            {
                PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime = ReActivityTime;
            }
            
            CCDirector::sharedDirector() -> setActionComeBack(true);
        }
        
        // 解析队伍数据
        NSDictionary* pets = [object objectForKey:@"pets"];
        if (pets)
        {
            NSArray * keys = [pets allKeys];;
            id key = nil;
            id value = nil;
            
            // 解析好友信息
            for (int i = 0; i < [keys count]; i++)
            {
                // 取键值
                key = [keys objectAtIndex:i];
                // 取数据
                value = [pets objectForKey:key];
                
                // 生成好友小弟
                bool bIsFriend = [[value objectForKey:@"friend"] intValue];
                if (bIsFriend)
                {
                    Follower * pFollower = Follower::node();
                    if (pFollower != NULL)
                    {
                        int iFollowerID = [[value objectForKey:@"ID"] intValue];
                        const Follower_Data * pData = SystemDataManage::ShareInstance() -> GetData_ForFollower(iFollowerID);
                        if (pData != NULL)
                        {
                            pFollower -> SetData(pData, false);
                            
                            pFollower -> GetData() -> Follower_SkillLevel = [[value objectForKey:@"skill_lv"] intValue];
                            pFollower -> GetData() -> Follower_HP = [[value objectForKey:@"HP"] intValue];
                            pFollower -> GetData() -> Follower_Attack = [[value objectForKey:@"Attack"] intValue];
                            pFollower -> GetData() -> Follower_Defense = [[value objectForKey:@"Defense"] intValue];
                            pFollower -> GetData() -> Follower_Comeback = [[value objectForKey:@"Reply"] intValue];
                            
                            PlayerDataManage::ShareInstance()->AddFriendFollowerToTeam(pFollower);
                            
                            continue;
                        }
                        else
                        {
                            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"请求进入副本，返回的好友小弟的数据异常" userInfo:nil];
                            @throw ex;
                        }
                    }
                }
                
                // 设置队伍小弟数据
                PlayerFollowerTeam battleList = *PlayerDataManage::ShareInstance()->GetMyFollowerTeam();
                PlayerFollowerTeam::iterator iter;
                for (iter = battleList.begin(); iter != battleList.end(); iter++)
                {
                    Follower_Data* pData = iter->second->GetData();
                    if (pData != NULL)
                    {
                        if (pData->Follower_ServerID == [key intValue])
                        {
                            //等级
                            pData->Follower_Level = [[value objectForKey:@"Level"] intValue];
                            
                            //技能等级
                            pData->Follower_SkillLevel = [[value objectForKey:@"skill_lv"] intValue];
                            
                            // 攻击力
                            pData->Follower_Attack = [[value objectForKey:@"Attack"] intValue];
                            
                            // 防御力
                            pData->Follower_Defense = [[value objectForKey:@"Defense"] intValue];
                            
                            // hp
                            pData->Follower_HP = [[value objectForKey:@"HP"] intValue];
                            
                            // Reply
                            pData->Follower_Comeback = [[value objectForKey:@"Reply"] intValue];
                            
                            break;
                        }
                    }
                }
            }
        }
        
        // 创建副本步骤进程列表
        map <int, vector<Enemy_Data *> > mapStepList;
        
        // 获得地图id
        int iMapID = [[object objectForKey:@"floor_id"] intValue];
        if (iMapID == 0)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"请求进入副本，返回副本id为0，无法进入游戏" userInfo:nil];
            @throw ex;
        }
        if (iMapID % 3 == 0)
            pBattleData->MapIndex = iMapID / 3;
        else
            pBattleData->MapIndex = iMapID / 3 + 1;
        
        pBattleData->SubMapIndex = iMapID;
        
        // 获得副本信息
        NSDictionary* info = [object objectForKey:@"info"];
        NSDictionary* step = nil;
        NSString* index = nil;
        int i = 0;
        if (info)
        {
            while (true)
            {
                // 遍历副本进程数据
                index = [NSString stringWithFormat:@"%d", i + 1];
                
                // 获得进程数据
                step = [info objectForKey:index];
                if (!step)
                    break;
                
                // 创建小弟数据
                vector<Enemy_Data*> vecFollowerList;
                
                // 获得此副本中的小弟
                NSDictionary* enemyInfo = [step objectForKey:@"npc"];
                if (enemyInfo)
                {
                    // 生成解析器
                    SBJsonParser* parser = [[SBJsonParser alloc] init];
                    
                    // 得到数组数据
                    NSError* error = nil;
                    NSData* jsonData =[NSJSONSerialization dataWithJSONObject:enemyInfo options:NSJSONWritingPrettyPrinted error:&error];
                    NSString* string = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    NSArray* list = [[NSArray alloc] initWithArray:[parser objectWithString:string]];
                    
                    // 便利数据
                    for (NSDictionary* dict in list)
                    {
                        // 生成敌人数据，释放在外部
                        EnemyData* pData = new EnemyData();
                        if (pData == NULL)
                            continue;
                        
                        // 读取职业
                        pData->Enemy_Profession = [[dict objectForKey:@"profession"] intValue];
                        
                        // 读取hp
                        pData->Enemy_HP = pData->Original_HP = [[dict objectForKey:@"Hp"] floatValue];
                        
                        // 读取攻击力
                        pData->Enemy_Attack = pData->Original_Attack = [[dict objectForKey:@"Attack"] floatValue];
                        
                        // 读取防御力
                        pData->Enemy_Defense = pData->Original_Defense = [[dict objectForKey:@"Defense"] floatValue];
                        
                        // 读取回合数
                        pData->Enemy_CDRound = [[dict objectForKey:@"AttackRate"] intValue];
                        
                        // 读取icon名称
                        NSString* iconName = [dict objectForKey:@"face"];
                        memcpy(pData->Enemy_IconName, [iconName UTF8String], CHARLENGHT);
                        
                        // 读取金钱
                        NSDictionary* gift = [dict objectForKey:@"loseGift"];
                        if ((NSNull*)gift != [NSNull null])
                        {
                            NSDictionary* pet = [gift objectForKey:@"pet"];
                            if (pet)
                            {
                                // 读取小弟id
                                pData->Enemy_DropFollower_ID = [[pet objectForKey:@"ID"] intValue];
                                // serverid
                                pData->Enemy_DropFollower_ServerID = 0;
                                
                                // 攻击力
                                pData->Enemy_DropFollower_Attack = [[pet objectForKey:@"Attack"] intValue];
                                // 防御力
                                pData->Enemy_DropFollower_Defense = [[pet objectForKey:@"Defense"] intValue];
                                // 恢复力
                                pData->Enemy_DropFollower_ComeBack = [[pet objectForKey:@"Reply"] intValue];
                                // hp
                                pData->Enemy_DropFollower_HP = [[pet objectForKey:@"HP"] intValue];
                                // 技能等级
                                pData->Enemy_DropFollower_SkillLevel = 1;
                                // 小弟所需经验值
                                pData->Enemy_DropFollower_LevelUpExp = [[pet objectForKey:@"next_lv"] intValue];
                                // 小弟经验
                                pData->Enemy_DropFollower_BeAbsorbExp = [[pet objectForKey:@"ExpQu"] intValue];
                                
                                // 读取小弟level
                                pData->Enemy_DropFollower_Level = [[pet objectForKey:@"Level"] intValue];
                                
                                // 读取小弟性格id
                                pData->Enemy_DropFollower_CharacterID = [[pet objectForKey:@"Character"] intValue];
                            }
                            
                            NSDictionary* gb = [gift objectForKey:@"gb"];
                            if (gb)
                                pData->Enemy_DropMoney = [[gift objectForKey:@"gb"] intValue];
                        }
                        vecFollowerList.push_back(pData);
                    }
                    
                    // 释放解析器
                    [string release];
                    [list release];
                    [parser release];
                }
                
                // 添加到列表中
                mapStepList.insert(make_pair(i + 1, vecFollowerList));
                
                // 递增序号
                ++i;
            }
        }
        
        // 设置进程列表到数据
        pBattleData->EnemyInfo_List = mapStepList;
        
        // 读取副本奖励
        NSDictionary* gift = [object objectForKey:@"gift"];
        pBattleData->ClearLevelDropMoney = [[gift objectForKey:@"gb"] intValue];
        pBattleData->ClearLevelDropExperience = [[gift objectForKey:@"exp"] intValue];
        
        // 设置副本进程
        pBattleData->StepNum = mapStepList.size();
        
        // 设置数据到服务器数据
        ServerDataManage::ShareInstance() -> SetBattleInfo(pBattleData);
        
        //继续loading
        Scene_Loading::ShareInstance() -> ContinueLoading();
    }
    @catch (NSException *exception)
    {
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Scene_Loading::StopLoading));
        Func -> retain();
        ShowMessage(BType_Firm, [[exception reason] UTF8String], Func);
        
        [exception release];
    }
    @finally
    {
    
    }
}

- (void) EnterBattleRequestFailed : (ASIHTTPRequest *) request
{
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Scene_Loading::StopLoading));
        Func -> retain();
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。", Func);
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark - 
/************************************************************************/



/************************************************************************/
#pragma mark 副本中复活请求
/************************************************************************/
- (void) BattleReliveRequest
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(BattleReliveRequestFinish:)];
    [request setDidFailSelector:@selector(BattleReliveRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];

    [request addPostValue:@"revive" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) BattleReliveRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"请求战斗中复活，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"战斗复活失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"请求战斗中复活，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        id  ObjectRMB = [object objectForKey:@"info"];
        int PlayerRMB = [ObjectRMB intValue];
        
        //结束联网动画
        RemoveNetConnect();
        
        //复活请求成功
        ServerDataManage::ShareInstance() -> RequestReliveSucceed(PlayerRMB);
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        
        [exception release];
    }
    @finally
    {
    
    }
}

- (void) BattleReliveRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 玩家直接恢复行动力请求
/************************************************************************/
- (void) BuyActionRequest
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(BuyActionRequestFinish:)];
    [request setDidFailSelector:@selector(BuyActionRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    [request addPostValue:@"reaction" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) BuyActionRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"请求购买行动力，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"买行动力失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"请求购买行动力，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        id  ObjectRMB = [object objectForKey:@"info"];
        int PlayerRMB = [ObjectRMB intValue];
        
        //结束联网动画
        RemoveNetConnect();
        
        //直接恢复行动力成功
        ServerDataManage::ShareInstance() -> RequestBuyActionSucceed(PlayerRMB);
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) BuyActionRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 向服务器发送扩充小弟背包的请求
/************************************************************************/
- (void) ExtendPackage
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(ExtendPackageFinish:)];
    [request setDidFailSelector:@selector(ExtendPackageFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    [request addPostValue:@"expandBag" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) ExtendPackageFinish : (ASIHTTPRequest *) request
{
    //{"errno":0,"result":{"info":{"diamond":1985,"add":5},"postType":"expandBag"}}
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"请求扩展背包，返回数据为空" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"扩充背包失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        NSDictionary * info = [object objectForKey:@"info"];
        
        id  ObjectRMB = [info objectForKey:@"diamond"];
        int PlayerRMB = [ObjectRMB intValue];
        
        id  ObjectAdd = [info objectForKey:@"add"];
        int Add = [ObjectAdd intValue];
        
        //结束联网动画
        RemoveNetConnect();
        
        //扩容背包成功
        ServerDataManage::ShareInstance() -> RequestExtendPackageSucceed(PlayerRMB, Add);
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) ExtendPackageFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark 向服务器发送副本完成之后的验证和解析
/************************************************************************/
- (void) BattleVerificationRequest
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(BattleVerificationRequestFinish:)];
    [request setDidFailSelector:@selector(BattleVerificationRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSNumber * n2 = [[NSNumber alloc] initWithInt:ServerDataManage::ShareInstance() -> GetSubMapIndex()];
    [request addPostValue:n2 forKey:@"MapIndex"];
    
    NSNumber * n3 = [[NSNumber alloc] initWithInt:Layer_Display::ShareInstance() -> m_BattleAllAttack];
    [request addPostValue:n3 forKey:@"AllAttack"];
    
    NSNumber * n4 = [[NSNumber alloc] initWithInt:Layer_Display::ShareInstance() -> m_BattleMaxAttack];
    [request addPostValue:n4 forKey:@"MaxAttack"];
    
    NSNumber * n5 = [[NSNumber alloc] initWithInt:Layer_Display::ShareInstance() -> m_Round];
    [request addPostValue:n5 forKey:@"AllClearRound"];
    
    NSNumber * n6 = [[NSNumber alloc] initWithInt:Layer_Block::ShareInstance() -> GetMaxComboNum()];
    [request addPostValue:n6 forKey:@"MaxCombo"];
    
    Battle_FollowerList pList = *Layer_Follower::ShareInstance() -> GetFollowerList();
    
    NSNumber * n7 = [[NSNumber alloc] initWithInt:pList[1] -> GetHost() -> GetData() -> Follower_ID];
    [request addPostValue:n7 forKey:@"MyFollower1_ID"];
    
    NSNumber * n8 = [[NSNumber alloc] initWithInt:pList[1] -> GetHost() -> GetData() -> Follower_Level];
    [request addPostValue:n8 forKey:@"MyFollower1_Level"];
    
    NSNumber * n9 = [[NSNumber alloc] initWithInt:pList[2] -> GetHost() -> GetData() -> Follower_ID];
    [request addPostValue:n9 forKey:@"MyFollower2_ID"];
    
    NSNumber * n10 = [[NSNumber alloc] initWithInt:pList[2] -> GetHost() -> GetData() -> Follower_Level];
    [request addPostValue:n10 forKey:@"MyFollower2_Level"];
    
    NSNumber * n11 = [[NSNumber alloc] initWithInt:pList[3] -> GetHost() -> GetData() -> Follower_ID];
    [request addPostValue:n11 forKey:@"MyFollower3_ID"];
    
    NSNumber * n12 = [[NSNumber alloc] initWithInt:pList[3] -> GetHost() -> GetData() -> Follower_Level];
    [request addPostValue:n12 forKey:@"MyFollower3_Level"];
    
    NSNumber * n13 = [[NSNumber alloc] initWithInt:pList[4] -> GetHost() -> GetData() -> Follower_ID];
    [request addPostValue:n13 forKey:@"MyFollower4_ID"];
    
    NSNumber * n14 = [[NSNumber alloc] initWithInt:pList[4] -> GetHost() -> GetData() -> Follower_Level];
    [request addPostValue:n14 forKey:@"MyFollower4_Level"];
    
    NSNumber * n15 = [[NSNumber alloc] initWithInt:pList[5] -> GetHost() -> GetData() -> Follower_ID];
    [request addPostValue:n15 forKey:@"MyFollower5_ID"];
    
    NSNumber * n16 = [[NSNumber alloc] initWithInt:pList[5] -> GetHost() -> GetData() -> Follower_Level];
    [request addPostValue:n16 forKey:@"MyFollower5_Level"];
    
    
    [request addPostValue:@"battleVerificationInfo" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) BattleVerificationRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"副本验证，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
    
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if(error != 0)
        {
            if(error == 801)
            {
                NSString * EXreason = [NSString stringWithFormat:@"副本战斗信息异常，服务器验证不通过，未能获得副本奖励"];
                NSException * ex = [[NSException alloc]initWithName:@"MyException801" reason:EXreason userInfo:nil];
                
                //战斗验证失败
                ServerDataManage::ShareInstance() -> VerificationBattleInfoFailed();
                @throw ex;
            }
            else
            {
                NSString * EXreason = [NSString stringWithFormat:@"副本验证失败，错误ID%d", error];
                NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
                @throw ex;
            }
        }

        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"副本验证，返回数据异常" userInfo:nil];
            @throw ex;
        }
        NSDictionary* info = [object objectForKey:@"info"];
        if (!info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"副本验证，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
        //获得副本中掉落的小弟的ServerID
        NSDictionary* giftInfo = [info objectForKey:@"gift"];
        if([giftInfo count] != 0)
        {
            NSDictionary* petInfo = [giftInfo objectForKey:@"pet"];
            if([petInfo count] < Layer_Enemy::ShareInstance() -> m_PlayerBattleResList.size())
            {
                NSException * ex = [[NSException alloc]initWithName:@"MyException801" reason:@"数据异常，副本中所获得的小弟的数量，比服务器现在给的ServerID多" userInfo:nil];
                //战斗验证失败
                ServerDataManage::ShareInstance() -> VerificationBattleInfoFailed();
                
                @throw ex;
            }
            else if([petInfo count] > Layer_Enemy::ShareInstance() -> m_PlayerBattleResList.size())
            {
                NSException * ex = [[NSException alloc]initWithName:@"MyException801" reason:@"数据异常，副本中所获得的小弟的数量，比服务器现在给的ServerID少" userInfo:nil];
                //战斗验证失败
                ServerDataManage::ShareInstance() -> VerificationBattleInfoFailed();
                
                @throw ex;
            }
            else
            {
                int petServerID[[petInfo count]];
                
                int index = 0;
                for(id object in petInfo)
                {
                    id objectID = [object objectForKey:@"serverID"];
                    int ServerID = [objectID intValue];
                    petServerID[index] = ServerID;
                    
                    index ++;
                }
                
                index = 0;
                for(list <Follower *>::iterator it = Layer_Enemy::ShareInstance() -> m_PlayerBattleResList.begin(); it != Layer_Enemy::ShareInstance() -> m_PlayerBattleResList.end(); ++ it)
                {
                    (*it) -> GetData() -> Follower_ServerID = petServerID[index];
                    
                    index ++;
                }
            }
        }
        else
        {
            if(Layer_Enemy::ShareInstance() -> m_PlayerBattleResList.size() != 0)
            {
                NSException * ex = [[NSException alloc]initWithName:@"MyException801" reason:@"数据异常，副本中所获得了小弟，但是服务器验证时没有给出ServerID" userInfo:nil];
                //战斗验证失败
                ServerDataManage::ShareInstance() -> VerificationBattleInfoFailed();
                
                @throw ex;
            }
        }
    
        // 获得副本信息
        BattleAfficheInfoList* pMapList = ServerDataManage::ShareInstance()->GetBattleAfficheInfoList();
        if (pMapList != NULL)
        {
            ServerDataManage::ShareInstance() -> ClearBattleAfficheInfo();
            
            NSDictionary* missionInfo = [info objectForKey:@"raid"];
            if (missionInfo)
            {
                int index = 1;
                NSEnumerator* keyiterator = [missionInfo keyEnumerator];
                for(id key in keyiterator)
                {
                    NSDictionary* dict = [missionInfo objectForKey:key];
                    if (dict)
                    {
                        // 使用new分配，在ServerDataManage中释放
                        BattleAfficheInfo* pInfo = new BattleAfficheInfo();
                        if (pInfo == NULL)
                            continue;
                        
                        //大副本的ID
                        pInfo->MapIndex = [[dict objectForKey:@"f_id"] intValue];
                        
                        //小副本的ID
                        pInfo->SubMapIndex = [key intValue];
                        
                        // 大副本使用的图片的ID
                        pInfo->MapImageID = [[dict objectForKey:@"f_id"] intValue];
                        
                        // 大副本使用的名称的ID
                        pInfo->MapNameID = 0;
                        
                        // 小副本使用的图片的ID
                        pInfo->SubMapImageID = index;
                        
                        // 小副本的战斗步骤数
                        pInfo->SubMapBattleStep = [[dict objectForKey:@"step"] intValue];
                        
                        // 小副本消耗的行动力
                        pInfo->SubMapActivity = [[dict objectForKey:@"action"] intValue];
                        
                        // 小副本当前状态
                        pInfo->SubMapState = [[dict objectForKey:@"enter"] intValue];
                        
                        // 小副本的开启时间
                        pInfo->SubMapOpenTime = [[dict objectForKey:@"time"] intValue];
                        // 小副本的持续时间
                        pInfo->SubMapKeepTime = [[dict objectForKey:@"sustain"] intValue];
                        
                        // 副本图片文件名
                        NSString* mapName = [dict objectForKey:@"f_img"];
                        memcpy(pInfo->MapImageFilename, [mapName UTF8String], 32);
                        
                        // 小副本图片文件名
                        NSString* subMapName = [dict objectForKey:@"img"];
                        memcpy(pInfo->SubMapImageFilename, [subMapName UTF8String], 32);
                        
                        // 副本名称
                        NSString* name = [dict objectForKey:@"f_name"];
                        memcpy(pInfo->MapName, [name UTF8String], 32);
                        
                        // 添加到列表
                        pMapList->insert(make_pair(pInfo->SubMapIndex, pInfo));
                        
                        ++index;
                    }
                }
            }
        }
        
        ServerDataManage::ShareInstance()->VerificationBattleInfoSucceed(0);
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
     
        if([[exception name] isEqualToString:@"MyException801"])
        {
            ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        }else
        {
            CCCallFunc * FuncGiveUp = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Layer_Enemy::OutGame));
            CCCallFunc * FuncRetry = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(ServerDataManage::VerificationBattleInfo));
            FuncGiveUp -> retain();
            FuncRetry -> retain();
            ShowMessage(BType_GiveUpAndRetry, [[exception reason] UTF8String], NULL, FuncGiveUp, FuncRetry);
        }
        
        [exception release];
    }
    @finally
    {
    
    }
}

- (void) BattleVerificationRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        CCCallFunc * FuncGiveUp = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Layer_Enemy::OutGame));
        CCCallFunc * FuncRetry = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(ServerDataManage::VerificationBattleInfo));
        FuncGiveUp -> retain();
        FuncRetry -> retain();
        ShowMessage(BType_GiveUpAndRetry, "游戏无法连接服务器，请检查网络或者重试。", NULL, FuncGiveUp, FuncRetry);
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark - 
/************************************************************************/




/************************************************************************/
#pragma mark 公告请求函数
/************************************************************************/
- (void) NoticeRequest
{
    NSURL * url = [NSURL URLWithString:@"http://cityos.o.kingnet.com/notice.txt"];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(NoticeRequestFinish:)];
    [request setDidFailSelector:@selector(NoticeRequestFailed:)];
    
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
}

- (void) NoticeRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"副本验证，返回数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 成功返回
        ServerDataManage::ShareInstance()->RequestNoticeSuccess([data UTF8String]);
        
        //继续loading
        Scene_Loading::ShareInstance() -> ContinueLoading();
    }
    @catch (NSException *exception)
    {
        if([[exception name] isEqualToString:@"MyException801"])
        {
            ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        }else
        {
            CCCallFunc * FuncGiveUp = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(Layer_Enemy::OutGame));
            CCCallFunc * FuncRetry = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(ServerDataManage::VerificationBattleInfo));
            FuncGiveUp -> retain();
            FuncRetry -> retain();
            ShowMessage(BType_GiveUpAndRetry, [[exception reason] UTF8String], NULL, FuncGiveUp, FuncRetry);
        }
        
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) NoticeRequestFailed : (ASIHTTPRequest *) request
{
    
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 数据更新
/************************************************************************/
- (void) DataUpdateRequest
{
    //开始联网动画
    ShowNetConnect();
        
    [self UpdateLogin];
}

#pragma mark 更新登陆数据
- (void) UpdateLogin
{
    NSURL * url = [NSURL URLWithString:URLPath];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    
    ASIFormDataRequest * requestLogin = [ASIFormDataRequest requestWithURL:url];
    [requestLogin setDelegate:self];
    [requestLogin setDidFinishSelector:@selector(UpdateLoginFinish:)];
    [requestLogin setDidFailSelector:@selector(UpdateLoginFailed:)];
    
    [requestLogin addPostValue:UserID forKey:@"UserID"];
    [requestLogin addPostValue:UserPassword forKey:@"UserPassword"];
    [requestLogin addPostValue:@"gameLoginInfo" forKey:@"postType"];
    
    [requestLogin setRequestMethod:@"POST"];
    [requestLogin startAsynchronous];
}

- (void) UpdateLoginFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"刷新数据失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新数据异常" userInfo:nil];
            @throw ex;
        }
        // 获取数据信息
        NSDictionary * info = [object objectForKey:@"info"];
        if (!info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新数据异常" userInfo:nil];
            @throw ex;
        }
        
        // 创建数据结构体
        LOGIN_DATA stData;
        
        //获得玩家信息
        NSDictionary * userInfo = [info objectForKey:@"user"];
        if (! userInfo)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新数据异常" userInfo:nil];
            @throw ex;
        }else
        {
            //玩家当前等级
            stData.PlayerLevel = [[userInfo objectForKey:@"lv"] intValue];
            PlayerDataManage::m_PlayerLevel = stData.PlayerLevel;
            
            //玩家当前等级升级所需的经验
            PlayerDataManage::m_PlayerLevelUpExperience = int(1.4 * pow(stData.PlayerLevel, 3) - 3 * pow(stData.PlayerLevel, 2) + 10 * stData.PlayerLevel + 192);
            
            //玩家当前的行动力
            stData.PlayerAction = [[userInfo objectForKey:@"action"] intValue];
            PlayerDataManage::m_PlayerActivity = stData.PlayerAction;
            
            //玩家当前等级的最大行动力
            stData.PlayerMaxAction = [[userInfo objectForKey:@"max_action"] intValue];
            PlayerDataManage::m_PlayerMaxActivity = stData.PlayerMaxAction;
            
            //玩家行动力恢复剩余时间
            PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime = [[userInfo objectForKey:@"rtime"] intValue];
            
            //如果玩家当前的行动力不满，就开始倒计时
            if(PlayerDataManage::m_PlayerActivity < PlayerDataManage::m_PlayerMaxActivity)
            {
                CCDirector::sharedDirector() -> setActionComeBack(true);
            }
            else if(PlayerDataManage::m_PlayerActivity == PlayerDataManage::m_PlayerMaxActivity)
            {
                PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime = 0;
            }
            
            //玩家游戏币
            stData.PlayerMoney = [[userInfo objectForKey:@"gb"] floatValue];
            PlayerDataManage::m_PlayerMoney = stData.PlayerMoney;
            
            //玩家充值的金钱
            stData.PlayerDiamond = [[userInfo objectForKey:@"diamond"] floatValue];
            PlayerDataManage::m_PlayerRMB = stData.PlayerDiamond;
            
            //玩家的经验
            float TeampLevel = stData.PlayerLevel - 1;
            stData.PlayerExperience = [[userInfo objectForKey:@"exp"] intValue];
            int TempExp = int(1.4f * pow((TeampLevel + 1), 2) * pow(TeampLevel, 2) / 4.0f - TeampLevel * (TeampLevel + 1) * (TeampLevel * 2.0f + 1) / 2.0f + 10.0f * TeampLevel * (TeampLevel + 1) / 2.0f + 192 * TeampLevel);
            PlayerDataManage::m_PlayerExperience = stData.PlayerExperience - TempExp;
            
            //如果当前是引导，就把经验值清0,自己加
            if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
            {
                PlayerDataManage::m_PlayerExperience = 0;
            }
            
            //玩家的友情点
            stData.PlayerFriendPoint = [[userInfo objectForKey:@"friendPoint"] intValue];
            PlayerDataManage::m_PlayerFriendValue = stData.PlayerFriendPoint;
        }
        
        // 获得楼层信息
        NSDictionary* buildingInfo = [info objectForKey:@"building"];
        if ((NSNull*)buildingInfo != [NSNull null])
        {
            // 生成解析器
            SBJsonParser* parser = [[SBJsonParser alloc] init];
            
            // 得到数组数据
            NSError* error = nil;
            NSData* jsonData =[NSJSONSerialization dataWithJSONObject:buildingInfo options:NSJSONWritingPrettyPrinted error:&error];
            NSString* string = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSArray* list = [[NSArray alloc] initWithArray:[parser objectWithString:string]];
            
            // 便利数据
            for (NSDictionary* dict in list)
            {
                FLOOR_DATA stInfo;
                
                stInfo.iType = [[dict objectForKey:@"type"] intValue];
                stInfo.iLevel = [[dict objectForKey:@"lv"] intValue];
                stInfo.iTime = [[dict objectForKey:@"time"] intValue];
                
                stData.vecFloorList.push_back(stInfo);
            }
            
            // 释放解析器
            [string release];
            [list release];
            [parser release];
        }
        
        // 获得小弟信息
        NSDictionary * petInfo = [info objectForKey:@"pet"];
        if (! petInfo)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新小弟数据异常" userInfo:nil];
            @throw ex;
        }else
        {
            if([petInfo count] == 0)
            {
                NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新小弟数据异常，没有小弟" userInfo:nil];
                @throw ex;
            }else
            {
                // 刷新小弟数据
                int followerNum = [petInfo count];
                if(followerNum > PlayerDataManage::ShareInstance() -> GetMyFollowerNum())
                {
                    ShowMessage(BType_Firm, "刷新小弟数据异常，服务器小弟数量多了");
                }else if(followerNum < PlayerDataManage::ShareInstance() -> GetMyFollowerNum())
                {
                    ShowMessage(BType_Firm, "刷新小弟数据异常，服务器小弟数量少了");
                }
            }
        }
        
        // 获得副本信息
        BattleAfficheInfoList * pMapList = ServerDataManage::ShareInstance()->GetBattleAfficheInfoList();
        if (pMapList != NULL)
        {
            //先清理一遍
            ServerDataManage::ShareInstance() -> ClearBattleAfficheInfo();
            
            NSDictionary * missionInfo = [info objectForKey:@"raid"];
            if (! missionInfo)
            {
                NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新副本数据异常" userInfo:nil];
                @throw ex;
            }else
            {
                int index = 1;
                NSEnumerator* keyiterator = [missionInfo keyEnumerator];
                for(id key in keyiterator)
                {
                    NSDictionary* dict = [missionInfo objectForKey:key];
                    if (dict)
                    {
                        // 使用new分配，在ServerDataManage中释放
                        BattleAfficheInfo* pInfo = new BattleAfficheInfo();
                        if (pInfo == NULL)
                            continue;
                        
                        //大副本的ID
                        pInfo->MapIndex = [[dict objectForKey:@"f_id"] intValue];
                        
                        //小副本的ID
                        pInfo->SubMapIndex = [key intValue];
                        
                        // 大副本使用的图片的ID
                        pInfo->MapImageID = [[dict objectForKey:@"f_id"] intValue];
                        
                        // 大副本使用的名称的ID
                        pInfo->MapNameID = 0;
                        
                        // 小副本使用的图片的ID
                        pInfo->SubMapImageID = index;
                        
                        // 小副本的战斗步骤数
                        pInfo->SubMapBattleStep = [[dict objectForKey:@"step"] intValue];
                        
                        // 小副本消耗的行动力
                        pInfo->SubMapActivity = [[dict objectForKey:@"action"] intValue];
                        
                        // 小副本当前状态
                        pInfo->SubMapState = [[dict objectForKey:@"enter"] intValue];
                        
                        // 小副本的开启时间
                        pInfo->SubMapOpenTime = [[dict objectForKey:@"time"] intValue];
                        // 小副本的持续时间
                        pInfo->SubMapKeepTime = [[dict objectForKey:@"sustain"] intValue];
                        
                        // 副本图片文件名
                        NSString* mapName = [dict objectForKey:@"f_img"];
                        memcpy(pInfo->MapImageFilename, [mapName UTF8String], 32);
                        
                        // 小副本图片文件名
                        NSString* subMapName = [dict objectForKey:@"img"];
                        memcpy(pInfo->SubMapImageFilename, [subMapName UTF8String], 32);
                        
                        // 副本名称
                        NSString* name = [dict objectForKey:@"f_name"];
                        memcpy(pInfo->MapName, [name UTF8String], 32);
                        
                        // 添加到列表
                        pMapList->insert(make_pair(pInfo->SubMapIndex, pInfo));
                        
                        ++index;
                    }
                }
            }
        }
        //刷新一遍副本数据
        KNUIFunction::GetSingle() -> UpdateMapList();
        
        // 刷新楼层数据
        vector<FLOOR_DATA>::iterator floorIter;
        for (floorIter = stData.vecFloorList.begin(); floorIter != stData.vecFloorList.end(); floorIter ++)
        {
            if(! PlayerDataManage::ShareInstance() -> UpdataFloorData(floorIter -> iType, floorIter -> iLevel, floorIter -> iTime))
            {
                NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新楼层数据异常" userInfo:nil];
                @throw ex;
            }
        }
        
        [self UpdateFriendList];
    }
    @catch (NSException * exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(ServerDataManage::RequestDataUpdate));
        Func -> retain();
        ShowMessage(BType_Retry, [[exception reason] UTF8String], NULL, NULL, Func);
        
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) UpdateLoginFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(ServerDataManage::RequestDataUpdate));
        Func -> retain();
        ShowMessage(BType_Retry, "无法连接服务器，请检查网络或者重试", NULL, NULL, Func);
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
#pragma mark -


#pragma mark 更新好友数据
- (void) UpdateFriendList
{
    NSURL * url = [NSURL URLWithString:URLPath];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    ASIFormDataRequest * requestFriendPet = [ASIFormDataRequest requestWithURL:url];
    [requestFriendPet setDelegate:self];
    [requestFriendPet setDidFinishSelector:@selector(UpdateFriendListFinish:)];
    [requestFriendPet setDidFailSelector:@selector(UpdateFriendListFailed:)];
    
    [requestFriendPet addPostValue:UserID forKey:@"UserID"];
    [requestFriendPet addPostValue:UserPassword forKey:@"UserPassword"];
    [requestFriendPet addPostValue:@"getFriendPets" forKey:@"postType"];
    
    [requestFriendPet setRequestMethod:@"POST"];
    [requestFriendPet startAsynchronous];
}

- (void) UpdateFriendListFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新数据好友信息异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"刷新数据好友信息失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新数据好友信息异常" userInfo:nil];
            @throw ex;
        }
        
        // 创建好友小弟列表
        list<FriendInfo> friendList;
        list<FriendInfo> strangerList;
        
        // 解析变量
        NSDictionary* dict = nil;
        NSDictionary* pet = nil;
        NSArray* keys = nil;
        id key = nil;
        id value = nil;
        
        // 解析好友信息
        dict = [object objectForKey:@"friend"];
        if (dict)
        {
            keys = [dict allKeys];
            for (int i = 0; i < [keys count]; i++)
            {
                // 取键值
                key = [keys objectAtIndex:i];
                // 取数据
                value = [dict objectForKey:key];
                
                // 解析数据
                
                if (value)
                {
                    FriendInfo stInfo;
                    
                    // uid
                    stInfo.Fri_UserID = [key intValue];
                    
                    // 姓名
                    NSString* name = [value objectForKey:@"name"];
                    memcpy(stInfo.Fri_UserName, [name UTF8String], CHARLENGHT);
                    
                    // 等级
                    stInfo.Fri_UserLevel = [[value objectForKey:@"lv"] intValue];
                    
                    // 好友的小弟
                    pet = [value objectForKey:@"pet"];
                    if ((NSNull*)pet != [NSNull null])
                    {
                        // id
                        stInfo.Fri_FollowerID = [[pet objectForKey:@"ID"] intValue];
                        
                        // level
                        stInfo.Fri_FollowerLevel = [[pet objectForKey:@"Level"] intValue];
                        
                        // 性格id
                        stInfo.Fri_FollowerCharacterID = [[pet objectForKey:@"Character"] intValue];
                        
                        // 攻击力
                        stInfo.Fri_FollowerAttack = [[pet objectForKey:@"Attack"] intValue];
                        
                        // 防御力
                        stInfo.Fri_FollowerDefense = [[pet objectForKey:@"Defense"] intValue];
                        
                        // hp
                        stInfo.Fri_FollowerHp = [[pet objectForKey:@"HP"] intValue];
                        
                        // 回复力
                        stInfo.Fri_FollowerReply = [[pet objectForKey:@"Reply"] intValue];
                        
                        // 技能等级
                        stInfo.Fri_FollowerSkillLevel = [[pet objectForKey:@"skill_lv"] intValue];
                        
                        // 友情值
                        stInfo.Fri_FollowerFriendValue = [[pet objectForKey:@"friend"] intValue];
                        
                        // quality
                        stInfo.Fri_FollowerQuality = [[pet objectForKey:@"Quality"] intValue];
                        
                        // 可用
                        stInfo.Fri_CanBeUse = [[pet objectForKey:@"useable"] intValue];
                    }
                    
                    // 添加到列表
                    friendList.push_back(stInfo);
                }
            }
        }
        
        // 解析陌生人
        dict = [object objectForKey:@"stranger"];
        if (dict)
        {
            keys = [dict allKeys];
            for (int i = 0; i < [keys count]; i++)
            {
                // 取键值
                key = [keys objectAtIndex:i];
                // 取数据
                value = [dict objectForKey:key];
                
                // 解析数据
                if (value)
                {
                    FriendInfo stInfo;
                    
                    // uid
                    stInfo.Fri_UserID = [key intValue];
                    
                    // 姓名
                    NSString* name = [value objectForKey:@"name"];
                    memcpy(stInfo.Fri_UserName, [name UTF8String], CHARLENGHT);
                    
                    // 等级
                    stInfo.Fri_UserLevel = [[value objectForKey:@"lv"] intValue];
                    
                    // 属性
                    pet = [value objectForKey:@"pet"];
                    if ((NSNull*)pet != [NSNull null])
                    {
                        // id
                        stInfo.Fri_FollowerID = [[pet objectForKey:@"ID"] intValue];
                        
                        // level
                        stInfo.Fri_FollowerLevel = [[pet objectForKey:@"Level"] intValue];
                        
                        // 性格id
                        stInfo.Fri_FollowerCharacterID = [[pet objectForKey:@"Character"] intValue];
                        
                        // 攻击力
                        stInfo.Fri_FollowerAttack = [[pet objectForKey:@"Attack"] intValue];
                        
                        // 防御力
                        stInfo.Fri_FollowerDefense = [[pet objectForKey:@"Defense"] intValue];
                        
                        // hp
                        stInfo.Fri_FollowerHp = [[pet objectForKey:@"HP"] intValue];
                        
                        // 回复力
                        stInfo.Fri_FollowerReply = [[pet objectForKey:@"Reply"] intValue];
                        
                        // 技能等级
                        stInfo.Fri_FollowerSkillLevel = [[pet objectForKey:@"skill_lv"] intValue];
                        
                        // 友情值
                        stInfo.Fri_FollowerFriendValue = [[pet objectForKey:@"friend"] intValue];
                        
                        // quality
                        stInfo.Fri_FollowerQuality = [[pet objectForKey:@"Quality"] intValue];
                        
                        // 可用
                        stInfo.Fri_CanBeUse = [[pet objectForKey:@"useable"] intValue];
                        
                        // 插入到列表
                        strangerList.push_back(stInfo);
                    }
                }
            }
        }
        
        // 放入到数据列表
        ServerDataManage::ShareInstance() -> ClearFriendList(0);
        ServerDataManage::ShareInstance() -> m_FriendInfoList.insert(make_pair(0, friendList));
        
        ServerDataManage::ShareInstance() -> ClearFriendList(1);
        ServerDataManage::ShareInstance() -> m_FriendInfoList.insert(make_pair(1, strangerList));
        
        //刷新一遍好友列表
        KNUIFunction::GetSingle() -> UpdateFriendList();
        
        [self UpdateApply];
    }
    @catch (NSException * exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(ServerDataManage::RequestDataUpdate));
        Func -> retain();
        ShowMessage(BType_Retry, [[exception reason] UTF8String], NULL, NULL, Func);
        
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) UpdateFriendListFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(ServerDataManage::RequestDataUpdate));
        Func -> retain();
        ShowMessage(BType_Retry, "无法连接服务器，请检查网络或者重试", NULL, NULL, Func);
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
#pragma mark -


#pragma mark 更新好友请求
- (void) UpdateApply
{
    NSURL * url = [NSURL URLWithString:URLPath];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    ASIFormDataRequest * requestApplyList = [ASIFormDataRequest requestWithURL:url];
    [requestApplyList setDelegate:self];
    [requestApplyList setDidFinishSelector:@selector(UpdateApplyListFinish:)];
    [requestApplyList setDidFailSelector:@selector(UpdateApplyListFailed:)];
    
    [requestApplyList addPostValue:UserID forKey:@"UserID"];
    [requestApplyList addPostValue:UserPassword forKey:@"UserPassword"];
    [requestApplyList addPostValue:@"getApplyList" forKey:@"postType"];
    
    [requestApplyList setRequestMethod:@"POST"];
    [requestApplyList startAsynchronous];
}

- (void) UpdateApplyListFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新数据好友请求获取异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"刷新数据好友请求获取失败，错误ID%d", error];
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新数据好友请求获取异常" userInfo:nil];
            @throw ex;
        }
        NSDictionary * info = [object objectForKey:@"info"];
        if (!info)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"刷新数据好友请求获取异常" userInfo:nil];
            @throw ex;
        }
        
        // 处理请求信息
        NSMutableArray * ReadDataArray = nil;
        
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * documentsDirectory = [paths objectAtIndex:0];
        if (!documentsDirectory) return;
        
        NSString * FolderName = [NSString stringWithUTF8String:PlayerDataManage::ShareInstance() -> m_Player91ID];
        NSString * FolderPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:FolderName];
        if([[NSFileManager defaultManager] fileExistsAtPath:FolderPath] == NO)
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:FolderPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        NSString * appFile = [FolderPath stringByAppendingPathComponent:@"MailInfo"];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:appFile])
            ReadDataArray = [NSMutableArray arrayWithContentsOfFile:appFile];
        
        if(! ReadDataArray)
        {
            NSMutableArray * WriteDataArray = [[NSMutableArray alloc] init];
            for(id obj in info)
            {
                //0
                int pID = [[obj objectForKey:@"uid"] intValue];
                NSString * SenderID = [NSString stringWithFormat:@"%d", pID];
                
                //1
                NSString * SenderName = [obj objectForKey:@"uname"];
                
                //2
                int pLevel = [[obj objectForKey:@"lv"] intValue];
                NSString * SenderLevel = [NSString stringWithFormat:@"%d", pLevel];
                
                //3
                int pTime = [[obj objectForKey:@"time"] intValue];
                NSString * SendTime = [NSString stringWithFormat:@"%d", pTime];
                
                //4
                int pFollowerID = [[obj objectForKey:@"pet_id"] intValue];
                NSString * SenderFollowerID = [NSString stringWithFormat:@"%d", pFollowerID];
                
                //5
                int pFollowerLevel = [[obj objectForKey:@"pet_lv"] intValue];
                NSString * SenderFollowerLevel = [NSString stringWithFormat:@"%d", pFollowerLevel];
                
                //6
                NSString * SendType = [NSString stringWithFormat:@"%d", MailType_FriendApply];
                
                //7
                NSString * MailState = [NSString stringWithFormat:@"%d", MailState_New];
                
                NSArray * ApplyInfo = [NSArray arrayWithObjects:SenderID, SenderName, SenderLevel, SendTime, SenderFollowerID, SenderFollowerLevel, SendType, MailState, nil];
                [WriteDataArray addObject:ApplyInfo];
            }
            
            [WriteDataArray writeToFile:appFile atomically:NO];
            [WriteDataArray release];
        }
        else
        {
            if([info count] != 0)
            {
                for(id obj in info)
                {
                    //0
                    int pID = [[obj objectForKey:@"uid"] intValue];
                    NSString * SenderID = [NSString stringWithFormat:@"%d", pID];
                    
                    //1
                    NSString * SenderName = [obj objectForKey:@"uname"];
                    
                    //2
                    int pLevel = [[obj objectForKey:@"lv"] intValue];
                    NSString * SenderLevel = [NSString stringWithFormat:@"%d", pLevel];
                    
                    //3
                    int pTime = [[obj objectForKey:@"time"] intValue];
                    NSString * SendTime = [NSString stringWithFormat:@"%d", pTime];
                    
                    //4
                    int pFollowerID = [[obj objectForKey:@"pet_id"] intValue];
                    NSString * SenderFollowerID = [NSString stringWithFormat:@"%d", pFollowerID];
                    
                    //5
                    int pFollowerLevel = [[obj objectForKey:@"pet_lv"] intValue];
                    NSString * SenderFollowerLevel = [NSString stringWithFormat:@"%d", pFollowerLevel];
                    
                    //6
                    NSString * SendType = [NSString stringWithFormat:@"%d", MailType_FriendApply];
                    
                    //7
                    NSString * MailState = [NSString stringWithFormat:@"%d", MailState_New];
                    
                    NSArray * ApplyInfo = [NSArray arrayWithObjects:SenderID, SenderName, SenderLevel, SendTime, SenderFollowerID, SenderFollowerLevel, SendType, MailState, nil];
                    [ReadDataArray addObject:ApplyInfo];
                }
                [ReadDataArray writeToFile:appFile atomically:NO];
            }
        }
        //重新读一遍
        PlayerDataManage::ShareInstance() -> ReadMyMailList();
        //再刷新一遍
        KNUIFunction::GetSingle() -> UpdateMailList();
        
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException * exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(ServerDataManage::RequestDataUpdate));
        Func -> retain();
        ShowMessage(BType_Retry, [[exception reason] UTF8String], NULL, NULL, Func);
        
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) UpdateApplyListFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        CCCallFunc * Func = CCCallFunc::actionWithTarget(Scene_Loading::ShareInstance(), callfunc_selector(ServerDataManage::RequestDataUpdate));
        Func -> retain();
        ShowMessage(BType_Retry, "无法连接服务器，请检查网络或者重试", NULL, NULL, Func);
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
#pragma mark -



/************************************************************************/
#pragma mark 签到请求
/************************************************************************/
- (void) SignTodayRequest
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(SignTodayFinish:)];
    [request setDidFailSelector:@selector(SignTodayFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    [request addPostValue:@"sign" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) SignTodayFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"签到数据异常!" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = nil;
            
            switch (error) {
                case 1201:
                    EXreason = [NSString stringWithFormat:@"已经签到过了"];
                    break;
                    
                default:
                    break;
            }
            
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"签到数据异常!" userInfo:nil];
            @throw ex;
        }
        
        // 奖励
        NSDictionary* gift = [object objectForKey:@"gift"];
        if (gift)
        {
            // 钻石
            PlayerDataManage::m_PlayerMoney += [[gift objectForKey:@"gb"] intValue];
            Layer_Game::GetSingle()->UpdateMainData();
        }
        
        // 累计天数
        ServerDataManage::ShareInstance()->m_stSign.iCount = [[object objectForKey:@"count"] intValue];
        
        // 签到日期
        NSDictionary* dataInfo = [object objectForKey:@"date"];
        if ((NSNull*)dataInfo != [NSNull null])
        {
            // 生成解析器
            SBJsonParser* parser = [[SBJsonParser alloc] init];
            
            // 得到数组数据
            NSError* error = nil;
            NSData* jsonData =[NSJSONSerialization dataWithJSONObject:dataInfo options:NSJSONWritingPrettyPrinted error:&error];
            NSString* string = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSArray* list = [[NSArray alloc] initWithArray:[parser objectWithString:string]];
            
            ServerDataManage::ShareInstance()->m_stSign.m_vecSignTime.clear();
            
            // 便利数据
            for (NSDictionary* dict in list)
            {
                NSString* data = (NSString*)dict;
                
                ServerDataManage::ShareInstance()->m_stSign.m_vecSignTime.push_back([data intValue]);
            }
            
            // 释放解析器
            [string release];
            [list release];
            [parser release];
        }
        
        //结束联网动画
        RemoveNetConnect();
        
        // 签到成功
        ServerDataManage::ShareInstance()->RequestSignTodaySuccess();
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) SignTodayFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}

- (void) SignLostRequest : (int) iType
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(SignLostRequestFinish:)];
    [request setDidFailSelector:@selector(SignLostRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSString* type = nil;
    if (iType == 1)
    {
        // 获得未签到的第一天
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(SIGN_INFO_FRAME_ID);
        if (pFrame != NULL)
        {
            time_t t;
            time(&t);
            
            tm stTime = *localtime(&t);
            
            Layer_Calendar* pCalendar = dynamic_cast<Layer_Calendar*>(pFrame->GetSubUIByID(80000));
            if (pCalendar != NULL)
            {
                int iDay = pCalendar->GetFirstUnsignDay();
                if (iDay != -1)
                {
                    stTime.tm_mday = iDay;
                    
                    NSString* year = [NSString stringWithFormat:@"%d", stTime.tm_year + 1900];
                    NSString* month = nil;
                    if (stTime.tm_mon / 10 == 0)
                        month = [NSString stringWithFormat:@"0%d", stTime.tm_mon + 1];
                    else
                        month = [NSString stringWithFormat:@"%d", stTime.tm_mon + 1];
                    NSString* day = nil;
                    if (stTime.tm_mday / 10 == 0)
                        day = [NSString stringWithFormat:@"0%d", stTime.tm_mday];
                    else
                        day = [NSString stringWithFormat:@"%d", stTime.tm_mday];
                    
                    type = [NSString stringWithFormat:@"%@%@%@", year, month, day];
                }
                else
                    KNUIFunction::GetSingle()->OpenMessageBoxFrame("没有补签的日子");
            }
        }
    }
    else if (iType == 2)
    {
        type = @"all";
    }
    
    [request addPostValue:type forKey:@"type"];
    [request addPostValue:@"reSign" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) SignLostRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"签到数据异常!" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = [NSString stringWithFormat:@"error%d", error];
            
            switch (error) {
                case 1204:
                    EXreason = [NSString stringWithFormat:@"没有可补签的日期"];
                    break;
                default:
                    break;
            }
            
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"签到数据异常!" userInfo:nil];
            @throw ex;
        }
        
        // 累计天数
        ServerDataManage::ShareInstance()->m_stSign.iCount = [[object objectForKey:@"count"] intValue];

        // date
        NSDictionary* dataInfo = [object objectForKey:@"date"];
        if ((NSNull*)dataInfo != [NSNull null])
        {
            // 生成解析器
            SBJsonParser* parser = [[SBJsonParser alloc] init];
            
            // 得到数组数据
            NSError* error = nil;
            NSData* jsonData =[NSJSONSerialization dataWithJSONObject:dataInfo options:NSJSONWritingPrettyPrinted error:&error];
            NSString* string = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSArray* list = [[NSArray alloc] initWithArray:[parser objectWithString:string]];
            
            // 清空列表
            ServerDataManage::ShareInstance()->m_stSign.m_vecSignTime.clear();
            
            // 便利数据
            for (NSDictionary* dict in list)
            {
                NSString* data = (NSString*)dict;
                ServerDataManage::ShareInstance()->m_stSign.m_vecSignTime.push_back([data intValue]);
            }
            
            // 释放解析器
            [string release];
            [list release];
            [parser release];
        }
        
        // 扣钱
        NSDictionary* userInfo = [object objectForKey:@"ext"];
        if (userInfo)
        {
            PlayerDataManage::m_PlayerRMB = [[userInfo objectForKey:@"diamond"] intValue];
            Layer_Game::GetSingle()->UpdateMainData();
        }
        
        //结束联网动画
        RemoveNetConnect();
        
        // 签到成功
        ServerDataManage::ShareInstance()->RequestLostSignSuccess();
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) SignLostRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark
/************************************************************************/



/************************************************************************/
#pragma mark 开宝箱
/************************************************************************/
- (void) OpenBoxRequest : (int) iType
{
    //开始联网动画
    ShowNetConnect();
    
    NSURL * url = [NSURL URLWithString:URLPath];
    
    ASIFormDataRequest * request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(OpenBoxRequestFinish:)];
    [request setDidFailSelector:@selector(OpenBoxRequestFailed:)];
    
    NSString * UserID = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerUserID];
    [request addPostValue:UserID forKey:@"UserID"];
    NSString * UserPassword = [[NSString alloc] initWithUTF8String:PlayerDataManage::ShareInstance() -> m_PlayerPassword];
    [request addPostValue:UserPassword forKey:@"UserPassword"];
    
    NSString* day = [NSString stringWithFormat:@"%d", iType];
    [request addPostValue:day forKey:@"day"];
    [request addPostValue:@"openBox" forKey:@"postType"];
    
    [request setRequestMethod:@"POST"];
    [request startAsynchronous];
}

- (void) OpenBoxRequestFinish : (ASIHTTPRequest *) request
{
    @try
    {
        // 获得数据
        NSString * data = [request responseString];
        if(! data)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"打开宝箱数据异常" userInfo:nil];
            @throw ex;
        }
        
#ifdef GAME_DEBUG
        NSLog(@"%@", data);
#endif
        
        // 解析字符串
        SBJsonParser * parser = [[SBJsonParser alloc] init];
        NSDictionary * object = [parser objectWithString:data];
        [parser release];
        
        // 检测数据包是否正确
        NSInteger error = [[object objectForKey:@"errno"] intValue];
        if (error != 0)
        {
            NSString * EXreason = nil;
            
            switch (error) {
                case 1202:
                    EXreason = [NSString stringWithFormat:@"宝箱已经开过了"];
                    break;
                case 1203:
                    EXreason = [NSString stringWithFormat:@"宝箱打开失败"];
                    break;
                default:
                    break;
            }
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:EXreason userInfo:nil];
            @throw ex;
        }
        
        // 获得数据包内容
        object = [object objectForKey:@"result"];
        if(! object)
        {
            NSException * ex = [[NSException alloc]initWithName:@"MyException" reason:@"打开宝箱数据异常" userInfo:nil];
            @throw ex;
        }
        
        // 获得钻石
        PlayerDataManage::m_PlayerRMB += [[object objectForKey:@"diamond"] intValue];
        Layer_Game::GetSingle()->UpdateMainData();
        
        // 宝箱类型
        int iDay = [[object objectForKey:@"day"] intValue];
        
        // 成功刷新界面
        ServerDataManage::ShareInstance()->RequestOpenBoxSuccess(iDay);
        
        //结束联网动画
        RemoveNetConnect();
    }
    @catch (NSException *exception)
    {
        //结束联网动画
        RemoveNetConnect();
        
        ShowMessage(BType_Firm, [[exception reason] UTF8String]);
        
        [exception release];
    }
    @finally
    {
        
    }
}

- (void) OpenBoxRequestFailed : (ASIHTTPRequest *) request
{
    //结束联网动画
    RemoveNetConnect();
    
    if([[request error] code] == ASIRequestTimedOutErrorType || [[request error] code] == ASIConnectionFailureErrorType)
    {
        ShowMessage(BType_Firm, "游戏无法连接服务器，请检查网络或者重试。");
    }
    
#ifdef GAME_DEBUG
    NSLog(@"%@", [request error]);
#endif
}
/************************************************************************/
#pragma mark
/************************************************************************/

@end


















