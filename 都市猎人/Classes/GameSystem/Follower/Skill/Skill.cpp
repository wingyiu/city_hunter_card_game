//
//  Skill.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-4-13.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include  "Skill.h"

#include "Enemy.h"
#include "Battle_Enemy.h"
#include "Layer_Enemy.h"

#include "Follower.h"
#include "Battle_Follower.h"
#include "Layer_Follower.h"

#include "Layer_Display.h"

#include "KNUIFunction.h"
USING_NS_CC;


/************************************************************************/
#pragma mark 初始化
/************************************************************************/
Skill::Skill()
{
    m_User = NULL;
    
    m_SkillValue = 0.0f;
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 析构
/************************************************************************/
Skill::~Skill()  
{
    m_User = NULL;
    
    printf("Skill : 释放完成\n");
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 静态构造函数
/************************************************************************/
Skill * Skill::skillWithSpriteFrameName(const char * pszSpriteFrameName)
{
    CCSpriteFrame * pFrame = CCSpriteFrameCache::sharedSpriteFrameCache() -> spriteFrameByName(pszSpriteFrameName);
    
    char msg[256] = {0};
    sprintf(msg, "Invalid spriteFrameName: %s", pszSpriteFrameName);
    CCAssert(pFrame != NULL, msg);
	return skillWithSpriteFrame(pFrame);
}

Skill * Skill::skillWithSpriteFrame(CCSpriteFrame * pSpriteFrame)
{    
    Skill * skill = new Skill();
    if (skill && skill -> initWithSpriteFrame(pSpriteFrame))
    {
        return skill;
    }
    CC_SAFE_DELETE(skill);
    return NULL;
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 设置技能的数据
/************************************************************************/
void Skill::SetData(const Skill_Data * Data)
{
    memcpy(&m_Data, Data, sizeof(Skill_Data));
    
    //CCLayerColor * pMask = CCLayerColor::layerWithColorWidthHeight(ccc4(255, 255, 255, 0), this -> getTextureRect().size.width, this -> getTextureRect().size.height);
    //this -> addChild(pMask, 0, SkillChildTag_Mask);
    
    //队长技能的话，就把持续回合设置的很长
    if(m_Data.Skill_KeepRound == -1)
        m_Data.Skill_KeepRound = 100000;
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 处理技能逻辑
/************************************************************************/
void Skill::DisposalSkill()
{
    int SkillType = m_Data.Skill_Type;
    int SkillID = m_Data.Skill_ID;
    
    Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
    
    if(SkillType == 2)
    {
        //对群体目标减少（N点）的防御值
        if((SkillID >= 1 && SkillID <= 3) || SkillID == 7)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateDecreaseDefense(m_Data.Skill_Value);
            }
        }
        //对群体目标减少防御值百分之（N）的防御值
        else if(SkillID >= 4 && SkillID <= 6)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateDecreaseDefense(it -> second -> GetData() -> Original_Defense * m_Data.Skill_Value);
            }
        }
    }
    else if(SkillType == 3)
    {
        //减少敌人N点攻击力
        if(SkillID >= 1 && SkillID <= 3)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateDecreaseAttack(m_Data.Skill_Value);
            }
        }
        //减少敌人百分之N的攻击力
        else if(SkillID >= 4 && SkillID <= 6)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateDecreaseAttack(it -> second -> GetData() -> Original_Attack * m_Data.Skill_Value);
            }
        }
    }
    else if(SkillType == 5)
    {
        //减少敌人每人（N点）攻击力
        if(SkillID >= 1 && SkillID <= 3)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateDecreaseAttack(m_Data.Skill_Value);
            }
        }
        //减少敌人每人百分之（N）攻击力
        else if((SkillID >= 4 && SkillID <= 7) || (SkillID >= 10 && SkillID <= 11))
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateDecreaseAttack(it -> second -> GetData() -> Original_Attack * m_Data.Skill_Value);
            }
        }
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 每回合触发技能
/************************************************************************/
void Skill::DisposalKeepSkill()
{
    CCScaleTo * pBig = CCScaleTo::actionWithDuration(0.2f, 2.2f);
    CCScaleTo * pSmall = CCScaleTo::actionWithDuration(0.2f, 1.0f);
    
    this -> runAction(CCSequence::actions(pBig, pSmall, NULL));
    DisposalKeepSkill2();
}

void Skill::DisposalKeepSkill2()
{
    int SkillType = m_Data.Skill_Type;
    int SkillID = m_Data.Skill_ID;
    
    Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
    
    m_Data.Skill_KeepRound -= 1;
    
    if(SkillType == 1)
    {
        //对群体目标每回合造成（N点）的伤害
        if(SkillID >= 13 && SkillID <= 15)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateBeAttackByFollower(m_Data.Skill_Value, m_User);
                it -> second -> DisplayBeAttackByKeepSkill();
            }
        }
        //对群体目标每个回合造成攻击力百分之（N）的伤害 
        else if(SkillID >= 16 && SkillID <= 18)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateBeAttackByFollower(m_User -> GetHost() -> GetData() -> Follower_Attack * m_Data.Skill_Value, m_User);
                it -> second -> DisplayBeAttackByKeepSkill();
            }
        }
        //对群体目标每个回合造成固定伤害（N）点
        else if(SkillID == 27)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateBeAttackByFollower(m_Data.Skill_Value, m_User);
                it -> second -> DisplayBeAttackByKeepSkill();
            }
        }
    }
    else if(SkillType == 4)
    {
        //每回合恢复回复力百分之（N）的生命值
        if((SkillID >= 7 && SkillID <= 9) || (SkillID >= 14 && SkillID <= 15))
        {
            Layer_Follower::ShareInstance() -> DisplayAddHpBySkill(Layer_Follower::ShareInstance() -> GetFollowerAddedHp() * m_Data.Skill_Value);
        }
        //每回合恢复（N点）的生命值  
        else if((SkillID >= 10 && SkillID <= 12) || (SkillID >= 16 && SkillID <= 17))
        {
            Layer_Follower::ShareInstance() -> DisplayAddHpBySkill(m_Data.Skill_Value);
        }
    }
    else if(SkillType == 8)
    {
        //每回合恢复回复力百分之N的生命值    
        if(SkillID >= 1 && SkillID <= 3)
        {
            Layer_Follower::ShareInstance() -> DisplayAddHpBySkill(Layer_Follower::ShareInstance() -> GetFollowerAddedHp() * m_Data.Skill_Value);
        }
        //每回合恢复N点生命值
        else if(SkillID >= 4 && SkillID <= 6)
        {
            Layer_Follower::ShareInstance() -> DisplayAddHpBySkill(m_Data.Skill_Value);
        }
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 关闭技能逻辑
/************************************************************************/ 
void Skill::CloseSkill()
{
    int SkillType = m_Data.Skill_Type;
    int SkillID = m_Data.Skill_ID;
    
    Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
    
    if(SkillType == 2)
    {
        //对群体目标减少（N点）的防御值
        if((SkillID >= 1 && SkillID <= 3) || SkillID == 7)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculatePlusDefense(m_Data.Skill_Value);
            }
        }
        //对群体目标减少防御值百分之（N）的防御值      
        else if(SkillID >= 4 && SkillID <= 6)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculatePlusDefense(it -> second -> GetData() -> Original_Defense * m_Data.Skill_Value);
            }
        }
    }
    else if(SkillType == 3)
    {
        //减少敌人N点攻击力
        if(SkillID >= 1 && SkillID <= 3)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculatePlusAttack(m_Data.Skill_Value);
            }
        }
        //减少敌人百分之N的攻击力
        else if(SkillID >= 4 && SkillID <= 6)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculatePlusAttack(it -> second -> GetData() -> Original_Attack * m_Data.Skill_Value);
            }
        }
    }
    else if(SkillType == 5)
    {
        //减少敌人每人（N点）攻击力
        if(SkillID >= 1 && SkillID <= 3)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculatePlusAttack(m_Data.Skill_Value);
            }
        }
        //减少敌人每人百分之（N）攻击力
        else if((SkillID >= 4 && SkillID <= 7) || (SkillID >= 10 && SkillID <= 11))
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculatePlusAttack(it -> second -> GetData() -> Original_Attack * m_Data.Skill_Value);
            }
        }
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/






















































