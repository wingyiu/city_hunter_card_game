//
//  Skill.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-13.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Skill_h
#define MidNightCity_Skill_h

#include "cocos2d.h"
#include "Skill_Configure.h"

class Battle_Follower;
class Battle_Enemy;
//////////////////////////////////////////////////////////////////////////////////


#define  SkillChildTag_Icon  0
#define  SkillChildTag_Mask 1

class Skill : public cocos2d::CCSprite
{
public: 
    Skill();
    ~Skill();
    
    static Skill * skillWithSpriteFrameName(const char * pszSpriteFrameName);
    static Skill * skillWithSpriteFrame(cocos2d::CCSpriteFrame * pSpriteFrame);
        
    void SetData(const Skill_Data * data);
 
public:
    //处理技能
    virtual void DisposalSkill();
    //处理持续性技能
    virtual void DisposalKeepSkill();
    virtual void DisposalKeepSkill2();
    
    //关闭技能逻辑
    virtual void CloseSkill();
   
public:    
    SS_GET(Skill_Data *, &m_Data, Data);
    
public:  
    GET_SET(Battle_Follower *, m_User, User);
    
private:
    //技能的数据
    Skill_Data              m_Data;
    //使用者
    Battle_Follower *       m_User;

    //值
    float                   m_SkillValue;
};



#endif




















