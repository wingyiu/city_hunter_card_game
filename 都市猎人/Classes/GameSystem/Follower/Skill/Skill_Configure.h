//
//  SkillDataConfigure.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-9.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Skill_Configure_h
#define MidNightCity_Skill_Configure_h

#include "MyConfigure.h"

//技能的品价等级枚举
enum SKILL_APPRAISELEVEL
{
    C = 1, B = 2, A = 3, S = 4
};


//一共多少技能级别
#define  SKILL_LEVEL                     4
//一共多少技能评价级别
#define  SKILLPPRAISE_LEVEL                4
//一共多少种职业
#define  PROFESSION_KIND          3


typedef struct SkillData
{
    SkillData() 
    { 
        memset(this, 0, sizeof(SkillData)); 
        //初始等级都是1
        Skill_Level = 1;
    }
    ~SkillData() {}
    
    //技能类型
    int  Skill_Type;
    //技能ID号
    int  Skill_ID; 
    //技能的名称
    char Skill_Name[CHARLENGHT];
   
    //该技能当前等级(1 2 3 4 ／ 初级 中级 高级 终级)
    int Skill_Level;
    //该技能所能够升级到的最大的等级
    int Skill_MaxLevel;
    //该技能评价等级(C B A S)
    int Skill_AppraiseLevel;
    
    //该技能的cd时间 (会根据等级的不同而变化)
    int     Skill_CDRound;
    //该技能的持续回合数
    int     Skill_KeepRound;
    
    //技能的作用值
    float   Skill_Value;
    
    //技能icon资源名称
    char Skill_IconName[CHARLENGHT];

    //对技能的描述
    char Skill_Depict[CHARLENGHT];
}Skill_Data;


#endif












































































