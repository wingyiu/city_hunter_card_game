//
//  Follower_Configrue.h
//  MidNightCity
//
//  Created by 强 张 on 12-3-31.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_FOLLOWERCONFIGURE_h
#define MidNightCity_FOLLOWERCONFIGURE_h

#include "MyConfigure.h"
#include "cocos2d.h"

#include "Skill_Configure.h"


//小弟的职业枚举
enum Follower_Profession
{
    SWORD = 1, FIST = 2, GUN = 3, ALL = 4
};

//小弟的品质枚举
enum Follower_Quality
{
    PuTong = 1, LiangHao = 2, YouXiu = 3, WanMei = 4, ZhuoYue = 5
};

#define  MaxEvolvementProps   3

typedef struct FollowerData
{
    FollowerData()
    {
        memset(this, 0, sizeof(FollowerData)); 
        Follower_Level = 1;
    }
    ~FollowerData() {}
    
    void BalanceProfession()
    {
        switch (Follower_Profession)
        {
            case SWORD:
                OverCome_Profession = FIST;
                BeOverCome_Profession = GUN;
                break;
            case FIST:
                OverCome_Profession = GUN;
                BeOverCome_Profession = SWORD;
                break;
            case GUN:
                OverCome_Profession = SWORD;
                BeOverCome_Profession = FIST;
                break;
            default:
                break;
        }
    }
    
    void SetQualityColor()
    {
        switch(Follower_Quality)
        {
            case PuTong:
                Follower_Color = cocos2d::ccc4(255, 255, 255, 255);
                break;
            case LiangHao:
                Follower_Color = cocos2d::ccc4(0, 255, 0, 255);
                break;
            case YouXiu:
                Follower_Color = cocos2d::ccc4(0, 0, 255, 255);
                break;
            case WanMei:
                Follower_Color = cocos2d::ccc4(141, 22, 180, 255);
                break;
            case ZhuoYue:
                Follower_Color = cocos2d::ccc4(232, 120, 30, 255);
                break;
            default:
                break;
        }
    }
    
    //小弟的ID号
    int     Follower_ID;
    //小弟的进化前的ID号
    int     Follower_OldID;
    //小弟在服务器中的ID号
    int     Follower_ServerID;
    //小弟进化之后的ID
    int     Follower_EvolvementID;
    
    //小弟的名称
    char    Follower_Name[CHARLENGHT];
    //小弟的icon名称
    char    Follower_IconName[CHARLENGHT];
    //小弟的战斗icon名称
    char    Follower_FightIconName[CHARLENGHT];
    //小弟掉落的icon名称
    char    Follower_DropIconName[CHARLENGHT];
    //小弟的描述
    char    Follower_Depict[CHARLENGHT];
    
    //小弟的职业
    int     Follower_Profession;
    //小弟的职业所对应克制哪个职业
    int     OverCome_Profession;
    //小弟的职业所对应被哪个职业克制
    int     BeOverCome_Profession;
    
    float   Follower_Price;                     //招募的价格 
    
    float   Follower_RegulateHP;                //HP的调整系数
    float   Follower_RegulateAttack;            //攻击的调整系数
    float   Follower_RegulateDefense;           //防御的调整系数
    float   Follower_RegulateComeback;          //回复的调整系数
    
    
    int     Follower_CharacterID;      //性格类型的ID
    
    
    float   Follower_HP;               //生命值
    float   Follower_Attack;           //攻击力
    float   Follower_Defense;          //防御力
    float   Follower_Comeback;         //恢复力
    
    int     Follower_GrowHP_Type;           //HP属性成长类型
    int     Follower_GrowAttack_Type;       //攻击力属性成长类型
    int     Follower_GrowDefense_Type;      //防御力属性成长类型
    int     Follower_GrowComeback_Type;     //恢复力属性成长类型
    
 
    int     Follower_Quality;               //小弟的品质
    int     Follower_MaxQuality;            //小弟能达到的最高品质
    cocos2d::ccColor4B  Follower_Color;     //根据品质所显示的不同的颜色
    
    
    int     Follower_Level;                     //小弟的等级
    int     Follower_MaxLevel;                  //小弟能达到的最高等级
    
    int     Follower_Experience;                            //当前经验值
    int     Follower_LevelUpExperience;                     //升级所需要的经验值
    float   Follower_RegulateLevelUpExperience;             //经验值的调整系数
    
    
    int             Follower_SkillLevel;            //技能等级
    
    int             Follower_CommonlySkillType;     //主动技能类型
    int             Follower_CommonlySkillID;       //主动技能ID
    
    
    int             Follower_TeamSkillType;         //队长技能类型
    int             Follower_TeamSkillID;           //队长技能ID
    
   
    int             Follower_BeAbsorbExperience;            //小弟被吸收之后给予的经验值
    float           Follower_RegulateBeAbsorbExperience;    //小弟被吸收之后给予的经验值的调整值
    
    int             Follower_AbsorbPrice;                   //小弟吸收别人所需的金钱
    float           Follower_RegulateAbsorbPrice;           //小弟吸收别人所需的金钱的调整值
    
    int             Follower_EvolvementPrice;               //小弟进化所需要的金钱
    int             Follower_EvolvementFollowerID[5];       //小弟进化所需要的素材（其他小弟的ID）
}Follower_Data;


#endif














