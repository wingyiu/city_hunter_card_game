//
//  Follower.h
//  MidNight
//
//  Created by 强 张 on 12-3-26.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//


#ifndef MidNightCity_Follower_h
#define MidNightCity_Follower_h

#include "cocos2d.h"
#include "Follower_Configure.h"
#include "Sprite_MySprite.h"

class Character;

// 小弟纹理tag标志
#define     FollowerChild_Icon                  0
#define     FollowerChild_IconFrame             1
#define     FollowerChild_ItemBack              2
#define     FollowerChild_BigIcon               3
#define     FollowerChild_SelectFrame           5
#define     FollowerChild_TeamTag               6
#define     FollowerChild_Name                  7
#define     FollowerChild_Star                  8
#define     FollowerChild_MaxStar               9
#define     FollowerChild_Attack                10
#define     FollowerChild_Hp                    11
#define     FollowerChild_Recover               12
#define     FollowerChild_ButtonNormal          13
#define     FollowerChild_ButtonSelect          14
#define     FollowerChild_Lv                    16
#define     FollowerChild_ShowType_1            17
#define     FollowerChild_ShowType_2            18
#define     FollowerChild_Mask                  19
//#define     FollowerChild_ButtonMask            20

// 小弟属性显示类型
enum FOLLOWER_ATTR_SHOW_TYPE
{
    FOLLOWER_ATTR_TYPE_1 = 0,                   // 显示武将名称，星级，等级
    FOLLOWER_ATTR_TYPE_2,                       // 显示攻击力，血量，回复
};

class Follower : public cocos2d::CCLayer 
{
public:
    #pragma mark - 构造函数
    Follower();
    #pragma mark - 析构函数
    virtual ~Follower();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 设置数据函数
    void SetData(const Follower_Data * Data, bool bSetImage = true);
    
    #pragma mark - 设置纹理函数
    void SetImage();
    #pragma mark - 移除纹理函数
    void RemoveImage();
    
    #pragma mark - 计算小弟的属性
    void CalculateFollowerAttribute();
    
    #pragma mark - 小弟训练
    void TrainingFollower(list<Follower *> * flist);
    #pragma mark - 小弟训练成功
    void TrainingFollowerSucceed(int exp, int level, int skillLevel, int levelupExp, int beabsorbExp, int hp, int attack, int defense, int comeback);
    
    #pragma mark - 小弟进化
    void EvolvementFollower(list <Follower *> * flist);
    #pragma mark - 小弟进化成功
    void EvolvementFollowerSucceed(int newID, int newcharacterID, int newserverID, int levelupExp, int beabsorbExp, int hp, int attack, int defense, int comeback);
    
    #pragma mark - 小弟升级技能
    void UpgradeSkill();
    
    #pragma mark - 设置小弟属性显示方式函数
    void SetFollowerAttrShowType(FOLLOWER_ATTR_SHOW_TYPE eType, bool bIsEdit = false);
    
    #pragma mark - 设置小弟可用函数
    void SetFollowerEnableMark(bool bIsEnable);
    #pragma mark - 获得小弟可用函数
    bool GetFollowerEnableMark() const                          {return m_bIsEnable;}
    
    #pragma mark - 刷新小弟数据到ui界面函数
    void RefreshDataToUI();
    
    SS_GET(Follower_Data *, &m_Data, Data);
    SS_GET(bool, m_bIsTrainLevelUp, TrainLevelUpMark);
    SS_SET(int, m_Data.Follower_ServerID, ServerID);
    
    GET_SET(int, m_TeamState, TeamState);
    GET_SET(bool, m_IsMaxLevel, IsMaxLevel);
    GET_SET(bool, m_IsMaxEvolvement, IsMaxEvolvement);
    GET_SET(bool, m_bIsLock, IsLock);
    
    #pragma mark - 连接自定义初始化方式
    MY_LAYER_NODE_FUNC(Follower);
protected:
    //基础的数据
    Follower_Data        m_Data;
    
    //小弟的队伍状态
    int                  m_TeamState;
    //小弟的升级状态
    bool                 m_IsMaxLevel;
    //小弟的进化状态
    bool                 m_IsMaxEvolvement;
    //小弟的技能升级状态
    bool                 m_bIsTrainLevelUp;
    //小弟的锁定状态
    bool                 m_bIsLock;
    //小弟的可用状态
    bool                 m_bIsEnable;
    
    //小弟训练素材列表
    list <Follower *>    m_TrainingList;
};


#endif











































