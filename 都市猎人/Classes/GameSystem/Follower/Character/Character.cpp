//
//  Character.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-4-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Character.h"
USING_NS_CC;

bool Character::init()
{
    if(! CCLayer::init())   return false;
    
    return true;
}

Character::~Character()
{
    printf("Character:释放完成 \n");
}

void Character::SetData(const Character_Data * Data)
{
    memcpy(&m_Data, Data, sizeof(Character_Data));
}

