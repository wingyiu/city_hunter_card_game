//
//  Character.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Character_h
#define MidNightCity_Character_h

#include "cocos2d.h"
#include "Character_Configure.h"

class Character : public cocos2d::CCLayer
{
public:
    virtual bool init();
    ~Character();
    
    virtual void SetData(const Character_Data * Data);
    
    SS_GET(Character_Data *, &m_Data, Data);
    MY_LAYER_NODE_FUNC(Character);
    
protected:
    Character_Data  m_Data;
    
};

#endif
