//
//  Character_Configure.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Character_Configure_h
#define MidNightCity_Character_Configure_h

#include "MyConfigure.h"

typedef struct CharacterData
{
    CharacterData() { memset(this, 0, sizeof(CharacterData));}
    ~CharacterData(){}
    
    //性格类型的ID号
    int     Character_ID;
    //性格名称
    char    Character_Name[CHARLENGHT];
    //性格中的血值
    float   Character_HP;
    //性格中的攻击值
    float   Character_Attack;
    //性格中的防御值
    float   Character_Defense;
    //性格中的恢复值
    float   Character_Comeback;
}Character_Data;

#endif
