//
//  Follower.cpp
//  MidNight
//
//  Created by 强 张 on 12-3-26.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//


#include "Follower.h"
#include "SystemDataManage.h"
#include "PlayerDataManage.h"
#include "Skill.h"
#include "Character.h"
#include "KNUIFunction.h"
USING_NS_CC;

Follower::Follower()
{
    
}

/**************************************************/
#pragma mark 初始化
/**************************************************/
bool Follower::init()
{
    if(! CCLayer::init())   return false;
    
    m_bIsTrainLevelUp = false;
    m_bIsLock = false;
    m_TeamState = 0;
    m_IsMaxLevel = false;
    m_IsMaxEvolvement = false;
    m_bIsEnable = true;
    
    return true;
}

Follower::~Follower()
{
    this -> removeAllChildrenWithCleanup(true);
    
    printf("Follower:: 释放完成\n");
}
/**************************************************/
#pragma mark -
/**************************************************/



/**************************************************/
#pragma mark    复制数据初始化
/**************************************************/
void Follower::SetData(const FollowerData * Data, bool bSetImage)
{
    memcpy(&m_Data, Data, sizeof(Follower_Data));
    if(bSetImage)   SetImage();
}
/**************************************************/
#pragma mark -
/**************************************************/



/**************************************************/
#pragma mark   绑定图片资源
/**************************************************/
void Follower::SetImage()
{
    // 移出原先纹理
    RemoveImage();
    
    // 临时文本
    char buf[32] = {0};
    
    // 小弟选项背景
    sprintf(buf, "follower_item_back_%d.png", m_Data.Follower_Profession);
    CCSprite* pItemBack = CCSprite::spriteWithSpriteFrameName(buf);
    addChild(pItemBack, 0, FollowerChild_ItemBack);
    
    //小弟的小Icon
    CCSprite * Icon = CCSprite::spriteWithSpriteFrameName(m_Data.Follower_IconName);
    Icon->setPosition(ccp(-101.5f, 0.0f));
    this -> addChild(Icon, 0, FollowerChild_Icon);
    
    //小弟小Icon的外框
    sprintf(buf, "FollowerIcon_%d.png",  m_Data.Follower_Profession);
    CCSprite * IconFrame = CCSprite::spriteWithSpriteFrameName(buf);
    IconFrame->setPosition(ccp(-101.5f, 0.0f));
    this -> addChild(IconFrame, 1, FollowerChild_IconFrame);
    
    // 小弟等级
    sprintf(buf, "%d", m_Data.Follower_Level);
    CCLabelAtlas* pLevel = CCLabelAtlas::labelWithString(buf, "Number_EnemyCD-hd.png", 9, 15, '.');
    pLevel->setPosition(ccp(-25.0f, -8.0f));
    addChild(pLevel, 0, FollowerChild_Lv);
    
    //小弟的大Icon
    CCSprite * BigIcon = CCSprite::spriteWithSpriteFrameName(m_Data.Follower_FightIconName);
    BigIcon -> setIsVisible(false);
    this -> addChild(BigIcon, 0, FollowerChild_BigIcon);
    
    //小弟被选择中的Icon
    CCSprite * BigIconFrameSelecte = CCSprite::spriteWithSpriteFrameName("ok_mark.png");
    BigIconFrameSelecte -> setIsVisible(false);
    BigIconFrameSelecte -> setPosition(ccp(105.0f, 0.0f));
    this -> addChild(BigIconFrameSelecte, 0, FollowerChild_SelectFrame);
    
    //小弟在队伍中的标记
    CCSprite * TeamTag = CCSprite::spriteWithSpriteFrameName("team_mark.png");
    TeamTag -> setIsVisible(false);
    TeamTag -> setPosition(ccp(-129.5f, 2.0f));
    this -> addChild(TeamTag, 3, FollowerChild_TeamTag);
    
    // 小弟名称
    CCLabelTTF* pFollowerName = CCLabelTTF::labelWithString(m_Data.Follower_Name, "黑体", 15);
    //pFollowerName->setIsVisible(false);
    pFollowerName->setPosition(ccp(5.0f, 17.0f));
    pFollowerName->setColor(ccc3(255, 255, 0));
    addChild(pFollowerName, 0, FollowerChild_Name);
    
    // 小弟最大星级
    Sprite_MySprite* pFollowerMaxStar = Sprite_MySprite::mySpriteWithSpriteFrameName("star_back.png");
    //pFollowerMaxStar->setIsVisible(false);
    pFollowerMaxStar->setPosition(ccp(-15.0f, -15.0f));
    for (int i = 0; i < m_Data.Follower_MaxQuality; i++)
        pFollowerMaxStar->addOnePositionOffset(ccp(18.0f * i, 0.0f));
    addChild(pFollowerMaxStar, 0, FollowerChild_MaxStar);
    
    // 小弟星级
    Sprite_MySprite* pFollowerStar = Sprite_MySprite::mySpriteWithSpriteFrameName("star.png");
    //pFollowerStar->setIsVisible(false);
    pFollowerStar->setPosition(ccp(-15.0f, -15.0f));
    for (int i = 0; i < m_Data.Follower_Quality; i++)
        pFollowerStar->addOnePositionOffset(ccp(18.0f * i, 0.0f));
    addChild(pFollowerStar, 0, FollowerChild_Star);
    
    // 小弟属性显示1
    CCSprite* pShowType1 = CCSprite::spriteWithSpriteFrameName("follower_show_type_1.png");
    pShowType1->setIsVisible(false);
    pShowType1->setPosition(ccp(-45.0f, 0.0f));
    addChild(pShowType1, 0, FollowerChild_ShowType_1);
    
    // 小弟属性显示2
    CCSprite* pShowType2 = CCSprite::spriteWithSpriteFrameName("follower_show_type_2.png");
    //pShowType2->setIsVisible(false);
    pShowType2->setPosition(ccp(-45.0f, 0.0f));
    addChild(pShowType2, 0, FollowerChild_ShowType_2);
    
    // 小弟生命值
    sprintf(buf, "%.f", m_Data.Follower_HP);
    CCLabelAtlas* pFollowerHp = CCLabelAtlas::labelWithString(buf, "number_type_9-hd.png", 9, 15, '.');
    pFollowerHp->setIsVisible(false);
    pFollowerHp->setPosition(ccp(0.0f, 8.0f));
    addChild(pFollowerHp, 0, FollowerChild_Hp);
    
    // 小弟攻击值
    sprintf(buf, "%.f", m_Data.Follower_Attack);
    CCLabelAtlas* pFollowerAttack = CCLabelAtlas::labelWithString(buf, "number_type_9-hd.png", 9, 15, '.');
    pFollowerAttack->setIsVisible(false);
    pFollowerAttack->setPosition(ccp(0.0f, -8.0f));
    addChild(pFollowerAttack, 0, FollowerChild_Attack);
    
    // 小弟回复力
    sprintf(buf, "%.f", m_Data.Follower_RegulateHP);
    CCLabelAtlas* pFollowerRecover = CCLabelAtlas::labelWithString(buf, "number_type_9-hd.png", 9, 15, '.');
    pFollowerRecover->setIsVisible(false);
    pFollowerRecover->setPosition(ccp(0.0f, -22.0f));
    addChild(pFollowerRecover, 0, FollowerChild_Recover);
    
    // 按钮－普通
    CCSprite* pButtonNormal = CCSprite::spriteWithSpriteFrameName("follower_select_normal.png");
    //pButtonNormal->setIsVisible(false);
    pButtonNormal->setPosition(ccp(105.0f, 0.0f));
    addChild(pButtonNormal, 0, FollowerChild_ButtonNormal);
    
    // 按钮－选择
    CCSprite* pButtonSelect = CCSprite::spriteWithSpriteFrameName("follower_select_select.png");
    pButtonSelect->setIsVisible(false);
    pButtonSelect->setPosition(ccp(105.0f, 0.0f));
    addChild(pButtonSelect, 0, FollowerChild_ButtonSelect);
    
    // 小弟遮罩
    CCSprite* pMask = CCSprite::spriteWithSpriteFrameName("follower_item_mask.png");
    pMask->setIsVisible(false);
    addChild(pMask, 2, FollowerChild_Mask);
    
    // 小弟按钮遮罩
//    CCSprite* pButtonMask = CCSprite::spriteWithSpriteFrameName("follower_select_mask.png");
//    pButtonMask->setIsVisible(false);
//    addChild(pButtonMask, 1, FollowerChild_ButtonMask);
}
/**************************************************/
#pragma mark -
/**************************************************/



/**************************************************/
#pragma mark 撤销绑定的图片资源
/**************************************************/
void Follower::RemoveImage()
{
    if(this -> getChildByTag(FollowerChild_Icon))                   this -> removeChildByTag(FollowerChild_Icon, true);
    if(this -> getChildByTag(FollowerChild_IconFrame))              this -> removeChildByTag(FollowerChild_IconFrame, true);
    if(this -> getChildByTag(FollowerChild_ItemBack))               this -> removeChildByTag(FollowerChild_ItemBack, true);
    if(this -> getChildByTag(FollowerChild_BigIcon))                this -> removeChildByTag(FollowerChild_BigIcon, true);
    if(this -> getChildByTag(FollowerChild_SelectFrame))            this -> removeChildByTag(FollowerChild_SelectFrame, true);
    if(this -> getChildByTag(FollowerChild_TeamTag))                this -> removeChildByTag(FollowerChild_TeamTag, true);
    if(this -> getChildByTag(FollowerChild_Name))                   this -> removeChildByTag(FollowerChild_Name, true);
    if(this -> getChildByTag(FollowerChild_Star))                   this -> removeChildByTag(FollowerChild_Star, true);
    if(this -> getChildByTag(FollowerChild_MaxStar))                this -> removeChildByTag(FollowerChild_MaxStar, true);
    if(this -> getChildByTag(FollowerChild_Attack))                 this -> removeChildByTag(FollowerChild_Attack, true);
    if(this -> getChildByTag(FollowerChild_Hp))                     this -> removeChildByTag(FollowerChild_Hp, true);
    if(this -> getChildByTag(FollowerChild_Recover))                this -> removeChildByTag(FollowerChild_Recover, true);
    if(this -> getChildByTag(FollowerChild_ButtonNormal))           this -> removeChildByTag(FollowerChild_ButtonNormal, true);
    if(this -> getChildByTag(FollowerChild_ButtonSelect))           this -> removeChildByTag(FollowerChild_ButtonSelect, true);
//    if(this -> getChildByTag(FollowerChild_Mask))                   this -> removeChildByTag(FollowerChild_Mask, true);
}
/**************************************************/
#pragma mark -
/**************************************************/



/**************************************************/
#pragma mark 计算小弟的属性
/**************************************************/
void Follower::CalculateFollowerAttribute()
{
    /*const FollowerData * pData = SystemDataManage::ShareInstance() -> GetData_ForFollower(m_Data.Follower_ID);
    
    //根据性格改变HP基础属性
    if(m_Data.Follower_GrowHP_Type == 1)
    {
        int Val1 = 3.4 * ((log(m_Data.Follower_Level) + 1) * (m_Data.Follower_Level + 4));
        int Val2 = Val1 - 17;
        int Val3 = m_Data.Follower_RegulateHP * Val2 * m_Character -> GetData() -> Character_HP;
        m_Data.Follower_HP = Val3 + pData -> Follower_HP;
    }
    else if(m_Data.Follower_GrowHP_Type == 2)
    {
        int Val1 = m_Data.Follower_RegulateHP * 0.1f * (m_Data.Follower_Level - 1) * m_Character -> GetData() -> Character_HP;
        m_Data.Follower_HP = Val1 + pData -> Follower_HP;
    }
    else if(m_Data.Follower_GrowHP_Type == 3)
    {
        int Val1 = 0.17f * (0.033f * (m_Data.Follower_Level * m_Data.Follower_Level) + 0.67 * m_Data.Follower_Level + 4) * (m_Data.Follower_Level + 4);
        int Val2 = Val1 - 4;
        int Val3 = m_Data.Follower_RegulateHP * Val2 * m_Character -> GetData() -> Character_HP;
        m_Data.Follower_HP = Val3 + pData -> Follower_HP;        
    }
    
    //根据性格改变Attack基础属性
    if(m_Data.Follower_GrowAttack_Type == 1)
    {
        int Val1 = (int)(m_Data.Follower_RegulateAttack * 60 * log(m_Data.Follower_Level)) * m_Character -> GetData() -> Character_Attack;
        m_Data.Follower_Attack = Val1 + pData -> Follower_Attack;
    }
    else if(m_Data.Follower_GrowAttack_Type == 2)
    {
        int Val1 = m_Data.Follower_RegulateAttack * 0.1f * (m_Data.Follower_Level - 1) * m_Character -> GetData() -> Character_Attack;
        m_Data.Follower_Attack = Val1 + pData -> Follower_Attack;
    }
    else if(m_Data.Follower_GrowAttack_Type == 3)
    {
        int Val1 = 0.033f * (m_Data.Follower_Level * m_Data.Follower_Level) + 0.67 * m_Data.Follower_Level + 4;
        int Val2 = Val1 - 4;
        int Val3 = m_Data.Follower_RegulateAttack * Val2 * m_Character -> GetData() -> Character_Attack;
        m_Data.Follower_Attack = Val3 + pData -> Follower_Attack;
    }
    
    //根据性格改变Defense基础属性
    if(m_Data.Follower_GrowDefense_Type == 1)
    {
        m_Data.Follower_Defense = pData -> Follower_Defense;
    }
    else if(m_Data.Follower_GrowDefense_Type == 2)
    {
        m_Data.Follower_Defense = pData -> Follower_Defense;
    }
    else if(m_Data.Follower_GrowDefense_Type == 3)
    {
        m_Data.Follower_Defense = pData -> Follower_Defense;
    }
    
    //根据性格改变Comeback基础属性
    if(m_Data.Follower_GrowComeback_Type == 1)
    {
        int Val1 = (int)(m_Data.Follower_RegulateComeback * 9 * log(m_Data.Follower_Level)) * m_Character -> GetData() -> Character_Comeback;
        m_Data.Follower_Comeback = Val1 + pData -> Follower_Comeback; 
    }
    else if(m_Data.Follower_GrowComeback_Type == 2)
    {
        int Val1 = m_Data.Follower_RegulateComeback * 0.1f * (m_Data.Follower_Level - 1) * m_Character -> GetData() -> Character_Comeback;
        m_Data.Follower_Comeback = Val1 + pData -> Follower_Comeback; 
    }
    else if(m_Data.Follower_GrowComeback_Type == 3)
    {
        int Val1 = m_Data.Follower_RegulateComeback * 0.15f * (0.033 * (m_Data.Follower_Level * m_Data.Follower_Level) + 0.67 * m_Data.Follower_Level + 4) * m_Character -> GetData() -> Character_Comeback;
        m_Data.Follower_Comeback = Val1 + pData -> Follower_Comeback;         
    }
    
    //计算升级所需的经验值
    m_Data.Follower_LevelUpExperience = (1.4f * (m_Data.Follower_Level * m_Data.Follower_Level * m_Data.Follower_Level) -
                                         3 * (m_Data.Follower_Level * m_Data.Follower_Level) + 
                                         10 * m_Data.Follower_Level + 192) * m_Data.Follower_RegulateLevelUpExperience;
    
    //计算被消耗给予的经验值
    m_Data.Follower_BeAbsorbExperience = (1.4f * (m_Data.Follower_Level * m_Data.Follower_Level * m_Data.Follower_Level) -
                                          3 * (m_Data.Follower_Level * m_Data.Follower_Level) + 
                                          10 * m_Data.Follower_Level + 192) / 2 * m_Data.Follower_RegulateBeAbsorbExperience; */
}
/**************************************************/
#pragma mark -
/**************************************************/



/**************************************************/
#pragma mark 小弟训练和训练成功的处理
/**************************************************/
void Follower::TrainingFollower(list<Follower *> * flist)
{
    m_TrainingList = *flist;
    
    //int price = int((1.4 * pow(m_Data.Follower_Level, 3) - 3 * pow(m_Data.Follower_Level, 2) + 10 * m_Data.Follower_Level + 192) * m_Data.Follower_RegulateAbsorbPrice / 2);
    int price = int((m_Data.Follower_Level * 100) * m_Data.Follower_RegulateAbsorbPrice);
    price = price * (m_TrainingList.size());

    //可以训练
    if(PlayerDataManage::m_PlayerMoney - price >= 0)
    {
        ServerDataManage::ShareInstance() -> RequestTrainingFollower(this, flist);
    }
    //不可以训练
    else 
    {
        ShowMessage(BType_Firm, "当前金钱不够，无法训练该小弟");
        m_TrainingList.clear();
    }
}

void Follower::TrainingFollowerSucceed(int exp, int level, int skillLevel, int levelupExp, int beabsorbExp, int hp, int attack, int defense, int comeback)
{
    //int CurrentLevelExp = int(int(1.4f * pow((TeampLevel + 1), 2) *
    //pow(TeampLevel, 2) / 4.0f - TeampLevel * (TeampLevel + 1) * (TeampLevel * 2.0f + 1) / 2.0f + 10.0f * TeampLevel * (TeampLevel + 1) / 2.0f + 192 * TeampLevel) * m_Data.Follower_RegulateLevelUpExperience);
    
    float TeampLevel = level - 1;
    int CurrentLevelExp = int(((25.0f / 6.0f) * (TeampLevel * (TeampLevel + 1) * (2 * TeampLevel + 1)) - (25.0f / 2.0f) * (TeampLevel * (TeampLevel + 1)) + 200 * TeampLevel) * m_Data.Follower_RegulateLevelUpExperience);
    
    m_Data.Follower_Experience = exp - CurrentLevelExp;
    m_Data.Follower_LevelUpExperience = levelupExp;
    m_Data.Follower_BeAbsorbExperience = beabsorbExp;
    m_Data.Follower_HP = hp;
    m_Data.Follower_Attack = attack;
    m_Data.Follower_Defense = defense;
    m_Data.Follower_Comeback = comeback;
    
    //小弟是否升级
    {
        if (m_Data.Follower_Level != level)
            m_bIsTrainLevelUp = true;
        else
            m_bIsTrainLevelUp = false;
        
        m_Data.Follower_Level = level;
        if(m_Data.Follower_Level >= m_Data.Follower_MaxLevel)
        {
            m_IsMaxLevel = true;
        }
    }
    
    if(m_Data.Follower_SkillLevel < skillLevel)
    {
        m_Data.Follower_SkillLevel = skillLevel;
    }else 
        m_Data.Follower_SkillLevel = skillLevel;

    //移除训练素材小弟
    for(list <Follower*>::iterator it = m_TrainingList.begin(); it != m_TrainingList.end(); )
    {
        printf("结束训练 材料 = %d\n", (*it)->GetData()->Follower_ServerID);
        PlayerDataManage::ShareInstance() -> RemoveFollowerFromDepot(*it);
        m_TrainingList.erase(it ++);
    }
    m_TrainingList.clear();
    
    // test
#ifdef GAME_DEBUG
    printf("背包中的小弟: ");
    PlayerFollowerDepot followerList = *PlayerDataManage::ShareInstance()->GetMyFollowerDepot();
    for (PlayerFollowerDepot::iterator iter = followerList.begin(); iter != followerList.end(); iter++)
    {
        list <Follower*>::iterator it2;
        for (it2 = iter->second.begin(); it2 != iter->second.end(); it2++)
            printf("%d ", (*it2)->GetData()->Follower_ServerID);
    }
    printf("\n");
#endif
    
    // 新手引导 : 完成小弟训练
    if (PlayerDataManage::m_GuideMark)
        Layer_GameGuide::GetSingle()->FinishFollowerCurrentStep();
    
    // 刷新ui界面
    RefreshDataToUI();
    // 刷新下队伍列表，因为有的小弟会在队伍里，以后找个效率高点的方法
    KNUIFunction::GetSingle()->UpdateFollowerTeamList();
    
    KNUIFunction::GetSingle() -> UpdateFollowerTrainInfo(this);
}
/**************************************************/
#pragma mark -
/**************************************************/


/**************************************************/
#pragma mark 小弟进化和进化成功的处理
/**************************************************/
void Follower::EvolvementFollower(list <Follower *> * flist)
{
    m_TrainingList = *flist;
    
    //可以进化
    if(PlayerDataManage::m_PlayerMoney - m_Data.Follower_EvolvementPrice >= 0)
    {
        ServerDataManage::ShareInstance() -> RequestEvolvementFollower(this, flist);
    }
    //不可以进化
    else
    {
        ShowMessage(BType_Firm, "当前金钱不够，该小弟无法转职");
        m_TrainingList.clear();
    }   
}

void Follower::EvolvementFollowerSucceed(int newID, int newcharacterID, int newserverID, int levelupExp, int beabsorbExp, int hp, int attack, int defense, int comeback)
{
    int iOldID = m_Data.Follower_ID;
    
    const Follower_Data * FD = SystemDataManage::ShareInstance() -> GetData_ForFollower(newID);
    this -> SetData(FD);
    
    m_Data.Follower_OldID = iOldID;
    m_Data.Follower_CharacterID = newcharacterID;
    m_Data.Follower_Experience = 0;
    m_Data.Follower_Level = 1;
    m_Data.Follower_SkillLevel = 1;
    m_Data.Follower_LevelUpExperience = levelupExp;
    m_Data.Follower_BeAbsorbExperience = beabsorbExp;
    m_Data.Follower_HP = hp;
    m_Data.Follower_Attack = attack;
    m_Data.Follower_Defense = defense;
    m_Data.Follower_Comeback = comeback;
    m_Data.Follower_ServerID = newserverID;
    
    // 更新小弟所在列表
    if (m_Data.Follower_ID != iOldID)
        PlayerDataManage::ShareInstance()->UpdateFollowerInList(iOldID, newID, this);
    
    //设置是否进化到最高阶了
    if(m_Data.Follower_EvolvementID == 0)
    {
        m_IsMaxEvolvement = true;
    }
    
    //移除进化素材小弟
    for(list <Follower*>::iterator it = m_TrainingList.begin(); it != m_TrainingList.end(); )
    {
        PlayerDataManage::ShareInstance() -> RemoveFollowerFromDepot(*it);
        m_TrainingList.erase(it ++);
    }
    m_TrainingList.clear();
    
    // 刷新界面
    RefreshDataToUI();
    // 刷新下队伍列表，因为有的小弟会在队伍里，以后找个效率高点的方法
    KNUIFunction::GetSingle()->UpdateFollowerTeamList();
    
    KNUIFunction::GetSingle() -> UpdateFollowerJobInfo(this);
}
/**************************************************/
#pragma mark -
/**************************************************/



/**************************************************/
#pragma mark 小弟升级技能
/**************************************************/
void Follower::UpgradeSkill()
{   
  
}
/**************************************************/
#pragma mark -
/**************************************************/

/**************************************************/
#pragma mark - Follower类设置小弟属性显示方式函数
/**************************************************/
void Follower::SetFollowerAttrShowType(FOLLOWER_ATTR_SHOW_TYPE eType, bool bIsEdit)
{
    // 关闭所有属性显示
    getChildByTag(FollowerChild_ShowType_1)->setIsVisible(false);
    getChildByTag(FollowerChild_ShowType_2)->setIsVisible(false);
    
    getChildByTag(FollowerChild_Attack)->setIsVisible(false);
    getChildByTag(FollowerChild_Hp)->setIsVisible(false);
    getChildByTag(FollowerChild_Recover)->setIsVisible(false);
    
    getChildByTag(FollowerChild_Name)->setIsVisible(false);
    getChildByTag(FollowerChild_Lv)->setIsVisible(false);
    getChildByTag(FollowerChild_Star)->setIsVisible(false);
    getChildByTag(FollowerChild_MaxStar)->setIsVisible(false);
    
    // 打开相应的选项
    if (eType == FOLLOWER_ATTR_TYPE_1)
    {
        getChildByTag(FollowerChild_ShowType_1)->setIsVisible(true);
        
        getChildByTag(FollowerChild_Attack)->setIsVisible(true);
        getChildByTag(FollowerChild_Hp)->setIsVisible(true);
        getChildByTag(FollowerChild_Recover)->setIsVisible(true);
    }
    else if (eType == FOLLOWER_ATTR_TYPE_2)
    {
        getChildByTag(FollowerChild_ShowType_2)->setIsVisible(true);
        
        getChildByTag(FollowerChild_Name)->setIsVisible(true);
        getChildByTag(FollowerChild_Lv)->setIsVisible(true);
        getChildByTag(FollowerChild_Star)->setIsVisible(true);
        getChildByTag(FollowerChild_MaxStar)->setIsVisible(true);
    }
    
    // 打开编辑按钮
    if (bIsEdit)
    {
        getChildByTag(FollowerChild_ButtonNormal)->setIsVisible(false);
        getChildByTag(FollowerChild_ButtonSelect)->setIsVisible(false);
    }
    else
    {
        getChildByTag(FollowerChild_ButtonNormal)->setIsVisible(true);
        getChildByTag(FollowerChild_ButtonSelect)->setIsVisible(false);
    }
}

/**************************************************/
#pragma mark - Follower类设置小弟可用函数
/**************************************************/
void Follower::SetFollowerEnableMark(bool bIsEnable)
{
    m_bIsEnable = bIsEnable;
    
    CCNode* pNode = getChildByTag(FollowerChild_Mask);
    if (pNode != NULL)
        pNode->setIsVisible(!m_bIsEnable);
}

/**************************************************/
#pragma mark - Follower类刷新小弟数据到ui界面函数
/**************************************************/
void Follower::RefreshDataToUI()
{
    // 临时文本
    char buf[32] = {0};
    // 纹理坐标
    CCPoint pos(0.0f, 0.0f);
    
    // 小弟的小Icon
    CCNode* pNode = getChildByTag(FollowerChild_Icon);
    if (pNode != NULL)
    {
        pos = pNode->getPosition();
        removeChild(pNode, true);
    }
    CCSprite* pIcon = CCSprite::spriteWithSpriteFrameName(m_Data.Follower_IconName);
    pIcon->setPosition(pos);
    addChild(pIcon, 0, FollowerChild_Icon);
    
    // 小弟等级
    sprintf(buf, "%d", m_Data.Follower_Level);
    CCLabelAtlas* pLevel = dynamic_cast<CCLabelAtlas*>(getChildByTag(FollowerChild_Lv));
    if (pLevel != NULL)
        pLevel->setString(buf);
    
    // 小弟的大Icon
    pNode = getChildByTag(FollowerChild_BigIcon);
    if (pNode != NULL)
    {
        pos = pNode->getPosition();
        removeChild(pNode, true);
    }
    CCSprite* BigIcon = CCSprite::spriteWithSpriteFrameName(m_Data.Follower_FightIconName);
    BigIcon->setIsVisible(false);
    BigIcon->setPosition(pos);
    addChild(BigIcon, 0, FollowerChild_BigIcon);
    
    // 小弟最大星级
    Sprite_MySprite* pFollowerMaxStar = dynamic_cast<Sprite_MySprite*>(getChildByTag(FollowerChild_MaxStar));
    pFollowerMaxStar->clearAllPositionOffset();
    for (int i = 0; i < m_Data.Follower_MaxQuality; i++)
        pFollowerMaxStar->addOnePositionOffset(ccp(18.0f * i, 0.0f));
    
    // 小弟星级
    Sprite_MySprite* pFollowerStar = dynamic_cast<Sprite_MySprite*>(getChildByTag(FollowerChild_Star));
    pFollowerStar->clearAllPositionOffset();
    for (int i = 0; i < m_Data.Follower_Quality; i++)
        pFollowerStar->addOnePositionOffset(ccp(18.0f * i, 0.0f));
    
    // 小弟攻击力
    sprintf(buf, "%.f", m_Data.Follower_Attack);
    CCLabelAtlas* pFollowerAttack = dynamic_cast<CCLabelAtlas*>(getChildByTag(FollowerChild_Attack));
    if (pFollowerAttack != NULL)
        pFollowerAttack->setString(buf);
    
    // 小弟hp
    sprintf(buf, "%.f", m_Data.Follower_HP);
    CCLabelAtlas* pFollowerHp = dynamic_cast<CCLabelAtlas*>(getChildByTag(FollowerChild_Hp));
    if (pFollowerHp != NULL)
        pFollowerHp->setString(buf);
    
    // 小弟回复力
    sprintf(buf, "%.f", m_Data.Follower_RegulateHP);
    CCLabelAtlas* pFollowerRecover = dynamic_cast<CCLabelAtlas*>(getChildByTag(FollowerChild_Recover));
    if (pFollowerRecover != NULL)
        pFollowerRecover->setString(buf);
}

























