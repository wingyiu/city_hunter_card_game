//
//  SystemDataManage.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-1.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_SystemDataManage_h
#define MidNightCity_SystemDataManage_h

#include "MyConfigure.h"
#include "Equipment_Configure.h"
#include "Props_Configure.h"
#include "Follower_Configure.h"
#include "Enemy_Configure.h"
#include "Character_Configure.h"
#include "Skill_Configure.h"
#include "Floor_Configure.h"

//数据管理类
class SystemDataManage
{
public:
    static SystemDataManage * ShareInstance();
    
    //读取所有的装备信息
    bool LoadData_ForAll();
    
    //装备信息
    bool LoadData_ForEquipment(const char * filename);
    const Equipment_Data * GetData_ForEquipment(int equipment_type, int equipment_id);
    //得到有多少装备类型
    int GetEquipmentTypeNum()           {return m_EquipmentDataMap.size();}
    //某个类型有多少个装备的数据
    int GetEquipmentNum(int type)       
    {
        m_EquipmentDataMap_it1 = m_EquipmentDataMap.find(type);
        if(m_EquipmentDataMap_it1 != m_EquipmentDataMap.end()) 
            return m_EquipmentDataMap_it1 -> second.size();
        else 
            printf("SystemDataManage / GetEquipmentNum(): 装备数据仓库中没有类型为%d的装备\n", type);
        return 0;
    }
    
    //道具信息
    bool LoadData_ForProps(const char * filename);
    const Props_Data * GetDate_ForProps(int props_id);
    
    //楼层信息
    bool LoadData_ForFloor(const char * filename);
    const Floor_Data * GetData_ForFloor(int floor_type, int floor_level);
    
    //小弟技能信息
    bool LoadData_ForSkill(const char * filename);
    const Skill_Data * GetData_ForSkill(int skill_type, int skill_id);
    
    //小弟性格信息
    bool LoadData_ForCharacter(const char * filename);
    const Character_Data * GetData_ForCharacter(int character_id);
    
    //小弟信息
    bool LoadData_ForFollower(const char * filename);
    const Follower_Data * GetData_ForFollower(int follower_id);
    //获取小弟的数据的数量
    int GetFollowerNum() {return m_FollowerDataMap.size();}
    
    //怪物信息
    //bool LoadData_ForEnemy(const char * filename);
    //const Enemy_Data * GetData_ForEnemy(int enemy_id);
    //获取怪物的数据的数量
    //int GetEnemyNum()   {return m_EnemyDataMap.size();}
    
protected:
    static SystemDataManage * m_Instance;
    
    SystemDataManage();
    ~SystemDataManage();
    void Initialize();
    
private:
    //装备的存储map
    map <int,  map <int, Equipment_Data * > >               m_EquipmentDataMap;
    map <int,  map <int, Equipment_Data * > > ::iterator    m_EquipmentDataMap_it1;
    map <int, Equipment_Data * >::iterator                  m_EquipmentDataMap_it2;
    
    //道具的存储map
    map <int,  Props_Data * >               m_PropsDataMap;
    map <int,  Props_Data * >::iterator     m_PropsDataMap_it;
    
    //楼层的信息储存map
    map <int,  map <int, Floor_Data * > >               m_FloorDataMap;
    map <int,  map <int, Floor_Data * > >::iterator     m_FloorDataMap_it1;
    map <int, Floor_Data * >::iterator                  m_FloorDataMap_it2;
    
    //技能的信息储存map
    map <int, map <int, Skill_Data *> >                   m_SkillDataMap;
    map <int, map <int, Skill_Data *> >::iterator         m_SkillDataMap_it1;
    map <int, Skill_Data *>::iterator                     m_SkillDataMap_it2;
    
    //小弟性格数据储存
    map <int,  Character_Data *>                m_CharacterDataMap;
    map <int,  Character_Data * >::iterator     m_CharacterDataMap_it;
    
    //小弟的数据存储
    map <int, Follower_Data *>                 m_FollowerDataMap;
    map <int, Follower_Data *>::iterator       m_FollowerDataMap_it;
    
    //怪物的数据存储
    map <int, Enemy_Data *>                 m_EnemyDataMap;
    map <int, Enemy_Data *>::iterator       m_EnemyDataMap_it;
};

#endif



























