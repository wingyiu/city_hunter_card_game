//
//  Equipment.h
//  MidNightCity
//
//  Created by 强 张 on 12-3-31.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Equipment_h
#define MidNightCity_Equipment_h

#include "Equipment_Configure.h"
#include "cocos2d.h"

class Equipment : public cocos2d::CCLayer
{
public:
    virtual bool init();
    ~Equipment();
    
    virtual void SetData(const Equipment_Data * Data);

    SS_GET(Equipment_Data *, &m_Data, Data);
    MY_LAYER_NODE_FUNC(Equipment);
    
protected:
    Equipment_Data  m_Data;
    
private:
    
};


#endif



































