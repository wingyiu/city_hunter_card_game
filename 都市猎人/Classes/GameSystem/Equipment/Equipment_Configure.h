//
//  Equipment_Configure.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-10.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Equipment_Configure_h
#define MidNightCity_Equipment_Configure_h

#include "MyConfigure.h"

enum  Equipment_Type
{
    WEAPON = 1, ARMET = 2, LORICAE = 3, CIRCLET = 4
};

//装备的数据
typedef struct EquipmentData
{
    EquipmentData() {memset(this, 0, sizeof(EquipmentData));}
    ~EquipmentData(){ }
    
    //装备的ID号
    int         Equipment_ID;
    //装备的类型
    int         Equipment_Type;
    //装备的Icon名称
    char        Equipment_IconName[CHARLENGHT];
    //装备的名称
    char        Equipment_Name[CHARLENGHT];
    
    int         Equipment_BindProfession;   //可被绑定的职业类型
    int         Equipment_Level;            //装备的等级
    float       Equipment_BuyPrice;         //装备的购买价格
    float       Equipment_SellPrice;        //装备的卖出价格
    float       Equipment_IntensifyPrice;    //装备的强化价格
    float       Equipment_HP;               //装备的HP
    float       Equipment_Attack;           //装备的攻击力
    float       Equipment_Defense;          //装备的防御力
    
    //该装备合成需要的道具ID号
    int         Equipment_Compose_PropsID;
    //该装备合成需要的道具的数量
    int         Equipment_Compose_PropsNum;
    
    //分解装备得到的道具ID号
    int         Equipment_Decompose_PropsID;
    //分解装备得到的道具的数量
    int         Equipment_Decompose_PropsNum;
    
    //该装备合成需要的道具ID号
    int         Equipment_Intensify_PropsID;
    //该装备合成需要的道具的数量
    int         Equipment_Intensify_PropsNum;
    
}Equipment_Data;

#endif
