//
//  Equipment.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-3-31.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Equipment.h"
USING_NS_CC;

bool Equipment::init()
{
    if(! CCLayer::init())   return false;
    
    return true;    
}

Equipment::~Equipment()
{
    printf("Equipment Delete\n");
}

void Equipment::SetData(const Equipment_Data * Data)
{
    memcpy(&m_Data, Data, sizeof(Equipment_Data));
    
    CCSpriteFrame * IconImage = CCSpriteFrameCache::sharedSpriteFrameCache() -> spriteFrameByName(m_Data.Equipment_IconName);
    if(! IconImage)
    {
        printf("Equipment / SetData(): 该装备没有找到名为%s的CCSpriteFrame\n", m_Data.Equipment_IconName);
    }else 
    {
        CCSprite * Icon = CCSprite::spriteWithSpriteFrame(IconImage);
        this -> setContentSize(CCSizeMake(Icon -> getTextureRect().size.width, Icon -> getTextureRect().size.height));
        this -> addChild(Icon);
    }
}























































