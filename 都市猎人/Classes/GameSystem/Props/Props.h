//
//  Props.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Props_h
#define MidNightCity_Props_h

#include "Props_Configure.h"
#include "cocos2d.h"

class Props : public cocos2d::CCLayer
{
public:
    virtual bool init();
    virtual void SetData(const Props_Data * Data);
    
    SS_GET(Props_Data *, &m_Data, Data);
    MY_LAYER_NODE_FUNC(Props);

protected:
    Props_Data m_Data;
};

#endif
