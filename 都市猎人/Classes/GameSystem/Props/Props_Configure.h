//
//  Props_Configure.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Props_Configure_h
#define MidNightCity_Props_Configure_h

#include "MyConfigure.h"

//道具的类型枚举
enum  Props_Type
{
    HeCheng = 1, QiangHua = 2, JinHua = 3, HuoDong = 4
};

typedef struct PropsData
{
    PropsData() { memset(this, 0, sizeof(PropsData)); }
    ~PropsData() {}
    
    //道具的ID号
    int     Props_ID;
    //道具的名称
    char    Props_Name[CHARLENGHT];
    //道具的类型
    int     Props_Type;
    //道具的Icon名称
    char    Props_IconName[CHARLENGHT];
    //道具的描述
    char    Props_Depict[CHARLENGHT];
    //道具的购买价格
    float   Props_BuyPrice;
    //道具的出售价格
    float   Props_SalePrice;
}Props_Data;

#endif
