//
//  HttpTest.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-16.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"
#import "ASINetworkQueue.h"
#import "JSON.h"
#import "cocos2d.h"
#import "ServerDataManage.h"

USING_NS_CC;

@interface NetManager : NSObject
{
    NSString * URLPath;
}

//静态函数 : 获得网络通信对象函数
+ (id) GetSingle;

//充值
- (void) PayIDReqest;
- (void) PayIDReqestFinish : (ASIHTTPRequest *) request;
- (void) PayIDReqestFailed : (ASIHTTPRequest *) request;

//91帐户检查
- (void) Player91Login;
- (void) Player91LoginFinish : (ASIHTTPRequest *) request;
- (void) Player91LoginFailed : (ASIHTTPRequest *) request;

//新玩家
- (void) PlayerInfoReqest;
- (void) PlayerInfoReqestFinish : (ASIHTTPRequest *) request;
- (void) PlayerInfoReqestFailed : (ASIHTTPRequest *) request;

//向服务器注册登陆
- (void) GameLoginReqest;
- (void) GameLoginReqestFinish : (ASIHTTPRequest *) request;
- (void) GameLoginReqestFailed : (ASIHTTPRequest *) request;

//引导步骤完成以后通知服务器
- (void) GuideDoneReqest:(int) step;
- (void) GuideDoneReqestFinish : (ASIHTTPRequest *) request;
- (void) GuideDoneReqestFailed : (ASIHTTPRequest *) request;

//向服务器请求好友申请列表
- (void) ApplyListRequest;
- (void) ApplyListRequestFinish : (ASIHTTPRequest *) request;
- (void) ApplyListRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求建造新的楼层
- (void) NewFloorReqest:(int) type; 
- (void) NewFloorReqestFinish : (ASIHTTPRequest *) request;
- (void) NewFloorReqestFailed : (ASIHTTPRequest *) request;

//向服务器请求升级楼层
- (void) LevelUpFloorRequest:(int) type;
- (void) LevelUpFloorReFinish : (ASIHTTPRequest *) request;
- (void) LevelUpFloorFailed : (ASIHTTPRequest *) request;

//向服务器请求收获楼层
- (void) HarvestFloorRequest:(int) type : (int) fast;
- (void) HarvestFloorRequestFinish : (ASIHTTPRequest *) request;
- (void) HarvestFloorRequestFailed : (ASIHTTPRequest *) request;

//向服务器提交小弟队伍列表
- (void) ReferMyFollowerTeamRequest;
- (void) ReferMyFollowerTeamRequestFinish : (ASIHTTPRequest *) request;
- (void) ReferMyFollowerTeamRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求商店中小弟列表
- (void) WareFollowerRequest;
- (void) WareFollowerRequestFinish : (ASIHTTPRequest *) request;
- (void) WareFollowerRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求刷新商店中小弟列表
- (void) UpdateWareFollowerRequest : (int) type;
- (void) UpdateWareFollowerRequestFinish : (ASIHTTPRequest *) request;
- (void) UpdateWareFollowerRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求购买小弟
- (void) BuyFollowerRequest : (int) type fID: (int) followerIndex;
- (void) BuyFollowerRequestFinish : (ASIHTTPRequest *) request;
- (void) BuyFollowerRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求训练小弟
- (void) TrainingFollowerRequest : (Follower *) follower Flist:(list <Follower *> *) plist;
- (void) TrainingFollowerRequestFinish : (ASIHTTPRequest *) request;
- (void) TrainingFollowerRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求进化小弟
- (void) EvolvementFollowerRequest : (Follower *) follower Flist:(list <Follower *> *) plist;
- (void) EvolvementFollowerRequestFinish : (ASIHTTPRequest *) request;
- (void) EvolvementFollowerRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求出售小弟
- (void) SellFollowerRequest;
- (void) SellFollowerRequestFinish : (ASIHTTPRequest *) request;
- (void) SellFollowerRequestFailed : (ASIHTTPRequest *) request;

//向服务器搜索好友信息
- (void) FindFriendInfoRequest: (int) userID;
- (void) FindFriendInfoRequestFinish : (ASIHTTPRequest *) request;
- (void) FindFriendInfoRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求删除好友
- (void) RemoveFriendRequest: (int) userID;
- (void) RemoveFriendRequestFinish : (ASIHTTPRequest *) request;
- (void) RemoveFriendRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求加好友
- (void) AddFriendByUserIDRequest: (int) userID;
- (void) AddFriendByUserIDRequestFinish : (ASIHTTPRequest *) request;
- (void) AddFriendByUserIDRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求同意添加好友
- (void) AgreeAddFriendRequest: (int) friendServerID;
- (void) AgreeAddFriendRequestFinish : (ASIHTTPRequest *) request;
- (void) AgreeAddFriendRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求好友信息
- (void) FriendListRequest;
- (void) FriendListRequestFinish : (ASIHTTPRequest *) request;
- (void) FriendListRequestFailed : (ASIHTTPRequest *) request;

//向服务器请求进入副本的副本战斗信息
- (void) EnterBattleRequest : (NSInteger) mapID;
- (void) EnterBattleRequestFinish : (ASIHTTPRequest *) request;
- (void) EnterBattleRequestFailed : (ASIHTTPRequest *) request;

//副本中复活请求
- (void) BattleReliveRequest;
- (void) BattleReliveRequestFinish : (ASIHTTPRequest *) request;
- (void) BattleReliveRequestFailed : (ASIHTTPRequest *) request;

//玩家直接恢复行动力请求
- (void) BuyActionRequest;
- (void) BuyActionRequestFinish : (ASIHTTPRequest *) request;
- (void) BuyActionRequestFailed : (ASIHTTPRequest *) request;

//玩家扩充小弟背包请求
- (void) ExtendPackage;
- (void) ExtendPackageFinish : (ASIHTTPRequest *) request;
- (void) ExtendPackageFailed : (ASIHTTPRequest *) request;

//副本完成之后的验证发送函数
- (void) BattleVerificationRequest;
- (void) BattleVerificationRequestFinish : (ASIHTTPRequest *) request;
- (void) BattleVerificationRequestFailed : (ASIHTTPRequest *) request;

//公告请求函数
- (void) NoticeRequest;
- (void) NoticeRequestFinish : (ASIHTTPRequest *) request;
- (void) NoticeRequestFailed : (ASIHTTPRequest *) request;

//数据更新
- (void) DataUpdateRequest;

- (void) UpdateLogin;
- (void) UpdateLoginFinish : (ASIHTTPRequest *) request;
- (void) UpdateLoginFailed : (ASIHTTPRequest *) request;

- (void) UpdateFriendList;
- (void) UpdateFriendListFinish : (ASIHTTPRequest *) request;
- (void) UpdateFriendListFailed : (ASIHTTPRequest *) request;

- (void) UpdateApply;
- (void) UpdateApplyListFinish : (ASIHTTPRequest *) request;
- (void) UpdateApplyListFailed : (ASIHTTPRequest *) request;

//签到请求
- (void) SignTodayRequest;
- (void) SignTodayFinish : (ASIHTTPRequest *) request;
- (void) SignTodayFailed : (ASIHTTPRequest *) request;

- (void) SignLostRequest : (int) iType;
- (void) SignLostRequestFinish : (ASIHTTPRequest *) request;
- (void) SignLostRequestFailed : (ASIHTTPRequest *) request;

//开宝箱
- (void) OpenBoxRequest : (int) iType;
- (void) OpenBoxRequestFinish : (ASIHTTPRequest *) request;
- (void) OpenBoxRequestFailed : (ASIHTTPRequest *) request;

@end
