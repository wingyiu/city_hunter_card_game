//
//  PlayerDataManage.h
//  MidNightCity
//
//  Created by 强 张 on 12-4-10.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_PlayerDataManage_h
#define MidNightCity_PlayerDataManage_h

#include "cocos2d.h"
#include "MyConfigure.h"
#include "ServerDataManage.h"
#include "SystemDataManage.h"
class Follower;
class Floor;

//小弟队伍的中可编辑小弟的最大数量
#define  TeamMaxNum 4

//行动力恢复的时间间隔
#define  ReActivityTime     300

//复活需要的钻石数量
#define   Price_ReLive     5
//立即恢复行动力需要的钻石数量
#define   Price_ReAction   5
//扩充小弟背包需要的钻石数量
#define   Price_ExtendPackge 5
//超级商店刷新小弟需要的钻石数量
#define   Price_UpdateSuperShop  30
//立即收获楼层需要的钻石数量
#define   Price_ImmediatelyHarvest 1




typedef map <int, list <Follower*> >    PlayerFollowerDepot;
typedef map <int, Follower *>           PlayerFollowerTeam;

typedef map <int, Floor_Data *>         PlayerFloorDepot;

enum  
{
    MailType_Message = 0,
    MailType_FriendApply = 1
};
enum 
{
    MailState_New = 0,
    MailState_Old,
    MailState_Delete
};
struct MailInfo
{
    MailInfo() { memset(this, 0, sizeof(MailInfo)); }
    
    int             Mail_Index;
    int             Mail_Type;
    int             Mail_State;
    
    int             Mail_SenderID;
    char            Mail_SenderName[32];
    int             Mail_SenderLevel;
    int             Mail_SendTime;
    const char *    Mail_SendMessage;
    
    int             Mail_Sender_FollowerID;
    int             Mail_Sender_FollowerLevel;    
};

class PlayerDataManage
{
    friend class ServerDataManage;
    
public:
    //玩家行动力
    static int  m_PlayerActivity;
    //玩家行动力上限
    static int  m_PlayerMaxActivity;
    //玩家行动力恢复时间
    int         m_PlayerActivityComeBackTime;
    //玩家金钱
    static int  m_PlayerMoney;
    //玩家钻石
    static int  m_PlayerRMB;
    //玩家经验值
    static int  m_PlayerExperience;
    //玩家升级经验值
    static int  m_PlayerLevelUpExperience;
    //玩家等级
    static int  m_PlayerLevel;
    //玩家的友情点
    static int  m_PlayerFriendValue;
    //已通关副本数
    static int  m_MissionCompleteNum;
    //是否在新手引导中
    static bool m_GuideMark;
    static int  m_GuideStepMark;
    static bool m_GuideBattleMark;
    //玩家背包格子上限
    static int  m_iPlayerPackageNum;
    
    // 音乐控制标记
    bool m_bIsMusicOn;
    // 音效控制标记
    bool m_bIsEffectOn;
    
    //玩家的性别
    static int  m_PlayerSex;
    //玩家初次进入游戏选择的小弟职业
    static int  m_PlayerFistGameProfession;
    
    //玩家的91唯一标识
    char     m_Player91ID[64];
    //玩家ID
    char     m_PlayerUserID[32];
    //玩家的昵称
    char     m_PlayerName[32];
    //玩家的密码
    char     m_PlayerPassword[64];
    
public:
    static PlayerDataManage * ShareInstance();
    void RemoveDataImage();
    void ReSetDataImage();
    
    ///////////关于邮件///////////      
public:
    void                ReadMyMailList();
    void                UpdateMyMailListToFile();
    void                RemoveMailFromList(int iIndex);
    void                SetMailStateToOld(int iIndex);
    void                ClearMailList();
    
    SS_GET(list <MailInfo *>*, &m_MyMailList, MyMailList);
private:
    list <MailInfo *>       m_MyMailList;
    
    
    ///////////关于小弟///////////      
public:
    //设置小弟队伍
    void                SetFollowerTeam(vector <Follower*> * TeamList);
    //清空小弟队伍
    void                ClearFollowerTeam();
    
    //加好友小弟进队伍
    void                AddFriendFollowerToTeam(Follower * follower);
    //移出好友小弟从队伍中
    void                RemoveFriendFollowerFromTeam();
    
    //往背包中加入小弟（同时会加入队伍中）
    void                AddFollowerToDepot(Follower * follower, int teamIndex = 0);
    //在背包中找某个小弟
    Follower   *        FindFollowerInDepot(Follower * follower);
    //背包中移除某个小弟
    void                RemoveFollowerFromDepot(Follower * follower);
    //清空背包
    void                ClearFollowerDepot();
    //更新小弟所在的列表 : 因为小弟转职后Follower_ID会发生变化，导致有的列表找不到
    void                UpdateFollowerInList(int iOldID, int iNewID, Follower* pFollower);
    
    //获取当前小弟的数量
    inline int          GetMyFollowerNum()
    {
        int num = 0;
        for(PlayerFollowerDepot::iterator it = m_MyFollowerDepot.begin(); it != m_MyFollowerDepot.end(); ++ it)
        {
            num += it -> second.size();
        }
        return num;
    }
    
    SS_GET(PlayerFollowerDepot *, &m_MyFollowerDepot, MyFollowerDepot);
    SS_GET(PlayerFollowerTeam *, &m_MyFollowerTeam, MyFollowerTeam);  
    
private:
    //玩家当前拥有的小弟
    PlayerFollowerDepot         m_MyFollowerDepot;
    //玩家编辑的小弟队伍
    PlayerFollowerTeam          m_MyFollowerTeam;
    /////////////////////////////
    
    
    ///////////关于楼层///////////    
public:
    void            InitBaseFloor();  
    void            CreateNewFloor(int type, int level, int time = 0);
    bool            UpdataFloorData(int type, int level, int time);
    void            LevelUpFloor(int type, int level);
    void            CountDownFloorHarvestTime();
    bool            IsFloorExist(int type);
    Floor_Data *    GetMyFloor(int type);
    
    SS_GET(PlayerFloorDepot *, &m_MyFloorDepot, MyFloorDepot);
    
private:
    //玩家当前的楼层
    PlayerFloorDepot            m_MyFloorDepot;
    //玩家当前自建的楼层
    PlayerFloorDepot            m_MySelfFloorDepot;
    /////////////////////////////
    
    
    
    ///////////关于行动力///////////
public:
    void            CountDownActivityTime();        
    /////////////////////////////
    
protected:
    //单例指针
    static PlayerDataManage * m_Instance;
    void Initialize();
    
};

#endif










