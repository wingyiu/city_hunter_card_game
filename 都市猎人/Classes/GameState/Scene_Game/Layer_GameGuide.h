//
//  Layer_GameGuide.h
//  MidNightCity
//
//  Created by 孙 惠伟 on 12-8-21.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_GameGuide_h
#define MidNightCity_Layer_GameGuide_h

#include "cocos2d.h"

USING_NS_CC;

// 提示箭头方向
enum ARROW_DIRECTION
{
    ARROW_UP = 0,               // 上
    ARROW_DOWN,                 // 下
    ARROW_LEFT,                 // 左
    ARROW_RIGHT,                // 右
};

// 类定义
class Layer_GameGuide : public CCLayer
{
public:
    #pragma mark - 静态函数 : 获得当前实例对象
    static Layer_GameGuide* GetSingle();
    
    #pragma mark - 构造函数
    Layer_GameGuide();
    #pragma mark - 析构函数
    virtual ~Layer_GameGuide();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 退出函数
    virtual void onExit();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 获得战斗新手引导步骤函数
    int GetBattleGuideStep() const                      {return m_iBattleStep;}
    #pragma mark - 获得楼层锁定索引函数
    int GetFloorLockIndex() const                       {return m_iFloorEnableIndex;}
    #pragma mark - 获得小弟引导索引函数
    int GetFollowerGuideIndex() const                   {return m_iFollowerGuideIndex;}
    #pragma mark - 获得商店引导索引函数
    int GetShopGuideIndex() const                       {return m_iShopGuideIndex;}
    #pragma mark - 获得是否界面引导函数
    bool GetSubGuide() const                            {return m_bIsSubGuide;}
    #pragma mark - 获得楼层第一次提示标记函数
    bool GetFloorFirstHintMark() const                  {return m_bIsFloorFirstHint;}
    #pragma mark - 获得是否完成任务关卡引导函数
    bool GetMissionFinishMark() const                   {return m_bIsFinishMissionGuide;}
    
    #pragma mark - 显示提示箭头函数
    void ShowGuideArrow(CCPoint pos, ARROW_DIRECTION eDirection);
    #pragma mark - 显示特殊显示箭头函数
    void ShowSpecialGuideArrow(CCPoint pos);
    #pragma mark - 关闭提示箭头函数
    void CloseGuideArrow()                              {m_pGuideArrow->stopAllActions(); m_pGuideArrow->setIsVisible(false);}
    #pragma mark - 显示楼层提示函数
    void ShowFloorHit();
    
    #pragma mark - 显示方块点击区域函数
    bool LimitBlockTouchRange(CCPoint point);
    
    #pragma mark - 战斗步骤1函数
    void RunBattleGuideStep1();
    #pragma mark - 完成战斗步骤1函数
    void FinishBattleGuideStep1();
    
    #pragma mark - 战斗步骤2函数
    void RunBattleGuideStep2();
    #pragma mark - 完成战斗步骤2函数
    void FinishBattleGuideStep2();
    
    #pragma mark - 战斗步骤3函数
    void RunBattleGuideStep3();
    #pragma mark - 完成战斗步骤3函数
    void FinishBattleGuideStep3();
    
    #pragma mark - 战斗步骤4函数
    void RunBattleGuideStep4();
    #pragma mark - 完成战斗步骤4函数
    void FinishBattleGuideStep4();
    
    #pragma mark - 战斗步骤5函数
    void RunBattleGuideStep5();
    #pragma mark - 完成战斗步骤5函数
    void FinishBattleGuideStep5();
    
    #pragma mark - 战斗步骤6函数
    void RunBattleGuideStep6();
    #pragma mark - 完成战斗步骤6函数
    void FinishBattleGuideStep6();
    
    #pragma mark - 楼层步骤1函数
    void RunGameGuideStep();
    #pragma mark - 完成当前步骤并更新下一步骤函数
    void FinishCurrentStepAndUpdateNext();
    
    #pragma mark - 小弟步骤1函数
    void RunFollowerGuideStep();
    #pragma mark - 完成当前步骤并更新下一步骤函数
    void FinishFollowerCurrentStep();
    
    #pragma mark - 任务关卡步骤1函数
    void RunMissionGuideStep();
    #pragma mark - 完成当前步骤并更新下一步骤函数
    void FinishMissionCurrentStep();
    
    #pragma mark - 商店步骤1函数
    void RunShopGuideStep();
    #pragma mark - 完成当前步骤并更新下一步骤函数
    void FinishShopCurrentStep();
    
    #pragma mark - cocos2d初始化函数
    LAYER_NODE_FUNC(Layer_GameGuide);
private:
    #pragma mark - 回调函数 : 处理箭头动画函数
    void ProcessGuideArrowAnim(CCObject* pObject);
    #pragma mark - 回调函数 : 处理特殊箭头动画函数
    void ProcessSpecialGuideArrowAnim(CCObject* pObject);
private:
    static Layer_GameGuide* m_Instance;
    
    // 新手引导对象变量
    CCSprite* m_pGuideArrow;                            // 提示箭头
    
    // 新手引导属性变量
    int m_iBattleStep;                                  // 战斗引导步数
    int m_iFloorEnableIndex;                            // 楼层可用索引
    int m_iFollowerGuideIndex;                          // 小弟层引导索引
    int m_iMissionGuideIndex;                           // 任务层引导索引
    int m_iShopGuideIndex;                              // 商店层引导索引
    
    bool m_bIsSubGuide;                                 // 小界面引导
    bool m_bIsFloorFirstHint;                           // 楼层第一次点击提示
    bool m_bIsFinishMissionGuide;                       // 是否完成任务关卡引导
    
    ARROW_DIRECTION m_eDirection;                       // 箭头方向枚举
};

#endif