//
//  Layer_GuideMark.cpp
//  都市猎人
//
//  Created by 孙 惠伟 on 12-9-28.
//
//

#include "Layer_GuideMark.h"

/************************************************************************/
#pragma mark - Layer_GuideMark类获得单列对象函数
/************************************************************************/
Layer_GuideMark* Layer_GuideMark::GetSingle()
{
    static Layer_GuideMark* pGuide = Layer_GuideMark::node();
    if (pGuide != NULL)
        return pGuide;
    
    return NULL;
}

/************************************************************************/
#pragma mark - Layer_GuideMark类初始化函数
/************************************************************************/
bool Layer_GuideMark::init()
{
    if (CCLayer::init() == false)
        return false;
    
    // 初始化引导标志
    m_pGuideMark = CCSprite::spriteWithSpriteFrameName("guide_hand.png");
    m_pGuideMark->setIsVisible(false);
    addChild(m_pGuideMark);
    
    retain();
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_GuideMark类退出函数
/************************************************************************/
void Layer_GuideMark::onExit()
{
    CCLayer::onExit();
}

/************************************************************************/
#pragma mark - Layer_GuideMark类显示引导标志函数
/************************************************************************/
void Layer_GuideMark::ShowGuideMark(MARKTYPE eType)
{
    // 停止一切动作
    m_pGuideMark->stopAllActions();
    m_pGuideMark->setIsVisible(true);
    
    // 根据type处理动作
    switch (eType) {
        case TOUCH:
            ProcessTouchEffect(m_pGuideMark);
            break;
        default:
            break;
    }
}

/************************************************************************/
#pragma mark - Layer_GuideMark类构造函数
/************************************************************************/
Layer_GuideMark::Layer_GuideMark()
{
    
}

/************************************************************************/
#pragma mark - Layer_GuideMark类析构函数
/************************************************************************/
Layer_GuideMark::~Layer_GuideMark()
{
    
}

/************************************************************************/
#pragma mark - Layer_GuideMark类回调函数 : 初始化点击效果函数
/************************************************************************/
void Layer_GuideMark::ProcessTouchEffect(CCObject* pObject)
{
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(0.5f);
    CCScaleTo* pScaleTo1 = CCScaleTo::actionWithDuration(0.3f, 0.8f);
    CCScaleTo* pScaleTo2 = CCScaleTo::actionWithDuration(0.3f, 1.0f);
    CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_GuideMark::ProcessTouchEffect));
    
    m_pGuideMark->runAction(CCSequence::actions(pTime, pScaleTo1, pScaleTo2, pFuncN, NULL));
}