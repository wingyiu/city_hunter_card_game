//
//  Layer_MessageList.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-7-24.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_MessageList.h"
#include "ServerDataManage.h"

/************************************************************************/
#pragma mark - Layer_MessageList类构造函数
/************************************************************************/
Layer_MessageList::Layer_MessageList()
{
    
}

/************************************************************************/
#pragma mark - Layer_MessageList类析构函数
/************************************************************************/
Layer_MessageList::~Layer_MessageList()
{
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_MessageList类初始化函数 : 使用自动内存使用
/************************************************************************/
bool Layer_MessageList::init()
{
    return CCLayer::init();
}

/************************************************************************/
#pragma mark - Layer_MessageList类销毁函数
/************************************************************************/
void Layer_MessageList::Destroy()
{
    vector<MESSAGE_INFO>::iterator iter;
    for (iter = m_vecMessageList.begin(); iter != m_vecMessageList.end(); iter++)
    {
        removeChild(iter->pBack, true);
        removeChild(iter->pTitle, true);
        removeChild(iter->pText, true);
    }
    m_vecMessageList.clear();
}

/************************************************************************/
#pragma mark - Layer_MessageList类重写退出函数 : 使用自动内存使用
/************************************************************************/
void Layer_MessageList::onExit()
{
    CCLayer::onExit();
    
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_MessageList类初始化函数
/************************************************************************/
bool Layer_MessageList::init(UIInfo& stInfo)
{
    // 初始化scrollview
    KNScrollView::init(stInfo);
    
    // 设置可视范围
    SetVisibleRange(CCRect(stInfo.point.x, stInfo.point.y, stInfo.size.width, stInfo.size.height));
    
    // 刷新公告信息
    RefreshMessageList();
    
    // 设置滚动条偏移
    KNScrollView::SetHorizontalSliderX(138.0f);
    
    // 打开滚动条
    KNScrollView::SetSliderEnable(true);
    KNScrollView::SetItemSize(CCSize(320.0f, 130.0f));
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_MessageList类刷新公告列表函数
/************************************************************************/
void Layer_MessageList::RefreshMessageList()
{
    // 释放原有公告
    vector<MESSAGE_INFO>::iterator iter;
    for (iter = m_vecMessageList.begin(); iter != m_vecMessageList.end(); iter++)
    {
        removeChild(iter->pBack, true);
        removeChild(iter->pTitle, true);
        removeChild(iter->pText, true);
    }
    m_vecMessageList.clear();
    
    // 取得列表
    vector<string> vecMsg = ServerDataManage::ShareInstance()->m_vecMessageList;
    
    // 设置高度
    float height = vecMsg.size() * 130.0f;
    KNScrollView::SetScrollSize(ccp(160.0f, height));
    if (height < KNScrollView::GetScrollSize().y)
        height = KNScrollView::GetScrollSize().y;
    
    height -= 70.0f;
    
    // 文本
    string title;
    string info;
    
    for (int i = 0; i < vecMsg.size(); i++)
    {
        MESSAGE_INFO stInfo;
        
        int iStart = 0;
        int iEnd = 0;
        
        // 初始化背景
        stInfo.pBack = CCSprite::spriteWithSpriteFrameName("msg_back.png");
        if (stInfo.pBack != NULL)
        {
            stInfo.pBack->setPosition(ccp(0.0f, height - 50.0f));
            addChild(stInfo.pBack);
        }
        
        // 初始化标题
        stInfo.pTitle = CCLabelTTF::labelWithString("", CCSize(260.0f, 100.0f), CCTextAlignmentLeft, "黑体", 16);
        if (stInfo.pTitle != NULL)
        {
            // 取得标题
            iEnd = vecMsg[i].find(":", i);
            title = vecMsg[i].substr(iStart, iEnd);
            
            stInfo.pTitle->setPosition(ccp(0.0f, height + 46.0f - 50.0f));
            stInfo.pTitle->setString(title.c_str());
            stInfo.pTitle->setColor(ccc3(255, 255, 0));
            addChild(stInfo.pTitle);
            
            // 设置开始位置，跳过: \r \n
            iStart = iEnd + 3;
        }
        
        // 初始化内容
        stInfo.pText = CCLabelTTF::labelWithString("", CCSize(260.0f, 100.0f), CCTextAlignmentLeft, "黑体", 13);
        if (stInfo.pText != NULL)
        {
            // 取得内容
            info = vecMsg[i].substr(iStart, vecMsg[i].size());
            
            stInfo.pText->setPosition(ccp(3.0f, height - 59.0f));
            //stInfo.pText->setString("都市猎人盛大开测！  QQ群:2602179101.内侧体验游戏！多重惊喜天天有！2.内侧冲级！公测领取至尊大奖！3.内侧提交BUG，直接赢取游戏内RMB奖励！4.游戏内更多功能加速开启中，敬请期待...");
            stInfo.pText->setString(info.c_str());
            addChild(stInfo.pText);
        }
        
        // 递增高度
        height -= 130.0f;
        
        // 添加到列表
        m_vecMessageList.push_back(stInfo);
    }
    
    // 移动到最顶层
    KNScrollView::SetPoint(true);
}

/************************************************************************/
#pragma mark - Layer_MessageList类事件函数 : 点击
/************************************************************************/
bool Layer_MessageList::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    
    CCRect visibleRect = GetVisibleRange();
    if (point.x > visibleRect.origin.x && point.x < visibleRect.origin.x + visibleRect.size.width &&
        point.y > visibleRect.origin.y && point.y < visibleRect.origin.y + visibleRect.size.height)
    {
        return true;
    }
    
    return false;
}