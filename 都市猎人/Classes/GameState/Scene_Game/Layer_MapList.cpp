//
//  Layer_MapList.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-5-3.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_MapList.h"
#include "GameGuide.h"

static const char* g_MapStateFilename[2] = {"mission_new_mark.png", "mission_clear_mark.png"};

/************************************************************************/
#pragma mark - Layer_MapList类构造函数
/************************************************************************/
Layer_MapList::Layer_MapList()
{
    // 为地图列表对象变量赋初值
    m_pBatchNode = NULL;
    
    m_vecMapItemList.clear();
    
    m_pSelectFrame = NULL;
    
    // 为地图列表属性变量赋初值
    m_iSelectIndex = 0;
    m_iSubSelectIndex = 0;
    m_iSelectMapID = 0;
    
    m_bIsNewMapExist = false;
}

/************************************************************************/
#pragma mark - Layer_MapList类析构函数
/************************************************************************/
Layer_MapList::~Layer_MapList()
{
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_MapList类初始化函数 : 使用自动内存使用
/************************************************************************/
bool Layer_MapList::init()
{
    return CCLayer::init();
}

/************************************************************************/
#pragma mark - Layer_MapList类销毁函数
/************************************************************************/
void Layer_MapList::Destroy()
{
    if (m_pBatchNode == NULL)
        return;
    
    // 释放地图选项
    vector<MAP_ITEM_INFO>::iterator iter;
    for (iter = m_vecMapItemList.begin(); iter != m_vecMapItemList.end(); iter++)
    {
        removeChild(iter->pBackImage, true);
        removeChild(iter->pMapName, true);
        
        m_pBatchNode->removeChild(iter->pFrameImage, true);
        m_pBatchNode->removeChild(iter->pStateImage, true);
        
        m_pBatchNode->removeChild(iter->pRemainTimeImage, true);
        m_pBatchNode->removeChild(iter->pMoneyImage, true);
        m_pBatchNode->removeChild(iter->pTimeUnit, true);
        
        m_pBatchNode->removeChild(iter->pActionImage, true);
        m_pBatchNode->removeChild(iter->pRate, true);
        
        m_pBatchNode->removeChild(iter->pRemainWord, true);
        m_pBatchNode->removeChild(iter->pBeiWord, true);
        
        removeChild(iter->pNumber1, true);
        removeChild(iter->pNumber2, true);
        
#ifdef GAME_DEBUG
        removeChild(iter->pMapID, true);
#endif
    }
    
    // 移出选择框
    m_pBatchNode->removeChild(m_pSelectFrame, true);
    
    // 释放精灵节点集合
    removeChild(m_pBatchNode, true);
    m_pBatchNode = NULL;
    
    CCTouchDispatcher::sharedDispatcher() -> removeDelegate(this);
}

/************************************************************************/
#pragma mark - Layer_MapList类退出函数 : 使用自动内存使用
/************************************************************************/
void Layer_MapList::onExit()
{
    CCLayer::onExit();
    
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_MapList类初始化函数
/************************************************************************/
bool Layer_MapList::init(UIInfo& stInfo)
{
    if (CCLayer::init() == false)
        return false;
    
    // 初始化scrollview
    KNScrollView::init(stInfo);
    
    // 事件
    if (stInfo.bIsVisible)
        CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    // 区域
    SetVisibleRange(CCRect(stInfo.point.x, stInfo.point.y, stInfo.size.width, stInfo.size.height));
    
    // 设置滑动类型
    KNScrollView::SetScrollType(static_cast<ScrollType>(stInfo.iType));
    
    // 保存类型
    m_eUIType = stInfo.eType;
    
    // 初始化精灵节点
    m_pBatchNode = CCSpriteBatchNode::batchNodeWithFile("game_1-hd.pvr.ccz");
    if (m_pBatchNode != NULL)
        addChild(m_pBatchNode, 1);
    
    // 刷新列表
    RefreshMapList();
    
    // 初始化选择标记
    m_pSelectFrame = CCSprite::spriteWithSpriteFrameName("map_select_mask.png");
    if (m_pSelectFrame != NULL)
    {
        CCPoint pos(0.0f, 0.0f);
        if (m_vecMapItemList.size() != 0)
            pos = m_vecMapItemList[m_vecMapItemList.size() - 1].pFrameImage->getPosition();
        
        m_pSelectFrame->setPosition(ccp(pos.x, pos.y + 2.0f));
        m_pBatchNode->addChild(m_pSelectFrame);
    }
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_MapList类获取大副本信息列表函数
/************************************************************************/
void Layer_MapList::GetMapInfoFromData()
{
    // 移出原先副本列表
    vector<MAP_ITEM_INFO>::iterator iter;
    for (iter = m_vecMapItemList.begin(); iter != m_vecMapItemList.end(); iter++)
    {
        removeChild(iter->pBackImage, true);
        removeChild(iter->pMapName, true);
        
        m_pBatchNode->removeChild(iter->pFrameImage, true);
        m_pBatchNode->removeChild(iter->pStateImage, true);
        
        m_pBatchNode->removeChild(iter->pRemainTimeImage, true);
        m_pBatchNode->removeChild(iter->pMoneyImage, true);
        m_pBatchNode->removeChild(iter->pTimeUnit, true);
        
        m_pBatchNode->removeChild(iter->pActionImage, true);
        m_pBatchNode->removeChild(iter->pRate, true);
        
        m_pBatchNode->removeChild(iter->pRemainWord, true);
        m_pBatchNode->removeChild(iter->pBeiWord, true);
        
        removeChild(iter->pNumber1, true);
        removeChild(iter->pNumber2, true);
    }
    m_vecMapItemList.clear();
    
    BattleAfficheInfoList* pList = ServerDataManage::ShareInstance()->GetBattleAfficheInfoList();
    if (pList != NULL)
    {
        int index = 1;
        BattleAfficheInfoList::iterator iter;
        
        // 遍历小副本信息，创建大副本列表
        while (true)
        {
            // 超过索引，则推出
            if (index > pList->size())
                break;
            
            // 创建副本信息
            MAP_ITEM_INFO stInfo;
            
            // 获得地图ID
            for (iter = pList->begin(); iter != pList->end(); iter++)
            {
                if (iter->second->MapIndex == index)
                {
                    stInfo.iMapID = iter->second->MapIndex;
                    
                    memcpy(stInfo.szMapNameFilename, iter->second->MapName, CHARLENGHT);
                    memcpy(stInfo.szMapImageFilename, iter->second->MapImageFilename, CHARLENGHT);
                    
                    stInfo.iBeginTime = iter->second->SubMapOpenTime;
                    stInfo.iMoneyAward = 0;
                    stInfo.iFollowerRate = 0;
                    
                    break;
                }
            }
            
            // 获得地图小副本id和小副本步骤
            int i = 0;
            for (iter = pList->begin(); iter != pList->end(); iter++)
            {
                if (iter->second->MapIndex == index)
                {
                    if (i < 3)
                    {
                        memcpy(stInfo.szSubImageFilename[i], iter->second->SubMapImageFilename, CHARLENGHT);
                        stInfo.iSubBattleStep[i] = iter->second->SubMapBattleStep;
                        stInfo.iSubTime[i] = iter->second->SubMapOpenTime;
                        stInfo.iSubMoney[i] = 0;
                        stInfo.iSubMapID[i] = iter->second->SubMapIndex;
                        stInfo.iSubAction[i] = iter->second->SubMapActivity;
                        stInfo.iSubState[i] = iter->second->SubMapState;
                        
                        if (stInfo.iMark == 0 && iter->second->SubMapState == 1)
                            stInfo.iMark = 1;
                        
                        ++i;
                    }
                }
            }
            
            // 获得地图行动力
//            for (iter = pList->begin(); iter != pList->end(); iter++)
//            {
//                if (iter->second->MapIndex == index)
//                    stInfo.iAction = iter->second->SubMapActivity;
//            }
            
            // 获得地图小副本状态
            for (iter = pList->begin(); iter != pList->end(); iter++)
            {
                if (iter->second->MapIndex == index)
                    stInfo.iState = 0;//iter->second->SubMapState;
            }
            
            // 添加到大副本列表中
            if (stInfo.iMapID != 0)
                m_vecMapItemList.push_back(stInfo);
            
            // 递增副本索引，查找下一个
            ++index;
        }
    }
}

/************************************************************************/
#pragma mark - Layer_MapList类刷新副本列表函数
/************************************************************************/
void Layer_MapList::RefreshMapList()
{
    // 获得副本信息
    GetMapInfoFromData();
    
    // 根据大副本列表初始化界面列表
    char szTemp[32] = {0};
    float height = 0.0f;
    
    // 计算副本高度
    vector<MAP_ITEM_INFO>::reverse_iterator iter;
    for (iter = m_vecMapItemList.rbegin(); iter != m_vecMapItemList.rend(); iter++)
        height += 70.0f;
    // 设置列表高度
    KNScrollView::SetScrollSize(ccp(320, height));
    if (height < KNScrollView::GetScrollSize().y)
        height = KNScrollView::GetScrollSize().y;
    
    height -= 70.0f;
    
    // 刷新列表
    for (iter = m_vecMapItemList.rbegin(); iter != m_vecMapItemList.rend(); iter++)
    {
        // 判断副本是否可见
        if (iter->iState == 1)
            continue;
        
        // 初始化副本背景
        iter->pBackImage = CCSprite::spriteWithSpriteFrameName(iter->szMapImageFilename);
        if (iter->pBackImage != NULL)
        {
            iter->pBackImage->setPosition(ccp(0.0f, height + 5.0f));
            addChild(iter->pBackImage);
        }
        
        // 初始化副本名称
        if (strcmp(iter->szMapImageFilename, "") != 0)
        {
            iter->pMapName = CCSprite::spriteWithSpriteFrameName(iter->szMapNameFilename);
            if (iter->pMapName != NULL)
            {
                iter->pMapName->setPosition(ccp(-55.0f, height - 3.0f));
                addChild(iter->pMapName);
            }
        }
        else
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("副本名称文件名为空，初始化失败！");
        }
        
        // 初始化副本边框
        sprintf(szTemp, "mission_back_%d.png", iter->iMapID % 3 + 1);
        iter->pFrameImage = CCSprite::spriteWithSpriteFrameName(szTemp);
        if (iter->pFrameImage != NULL)
        {
            iter->pFrameImage->setPosition(ccp(0.0f, height));
            m_pBatchNode->addChild(iter->pFrameImage);
        }
        
        // 初始化副本状态标记
        if (iter->iMark)
        {
            iter->pStateImage = CCSprite::spriteWithSpriteFrameName(g_MapStateFilename[0]);
            if (iter->pStateImage != NULL)
            {
                iter->pStateImage->setPosition(ccp(-100.0f, height + 20.0f));
                m_pBatchNode->addChild(iter->pStateImage, 1);
            }
            
            m_bIsNewMapExist = true;
        }
        else
        {
            if (m_vecMapItemList.size() != 1)
            {
                iter->pStateImage = CCSprite::spriteWithSpriteFrameName(g_MapStateFilename[1]);
                if (iter->pStateImage != NULL)
                {
                    iter->pStateImage->setPosition(ccp(-100.0f, height + 20.0f));
                    m_pBatchNode->addChild(iter->pStateImage, 1);
                }
            }
            else
            {
                iter->pStateImage = CCSprite::spriteWithSpriteFrameName(g_MapStateFilename[0]);
                if (iter->pStateImage != NULL)
                {
                    iter->pStateImage->setPosition(ccp(-100.0f, height + 20.0f));
                    m_pBatchNode->addChild(iter->pStateImage, 1);
                }
                
                m_bIsNewMapExist = true;
            }
        }
        
        // 初始化金钱图标 - 上
        if (iter->iMoneyAward != 0)
        {
            // 信息条
            iter->pMoneyImage = CCSprite::spriteWithSpriteFrameName("map_info_money.png");
            iter->pMoneyImage->setPosition(ccp(90.0f, height + 10.0f));
            m_pBatchNode->addChild(iter->pMoneyImage, 1);
            
            // "倍"图标
            iter->pBeiWord = CCSprite::spriteWithSpriteFrameName("mark_rate.png");
            iter->pBeiWord->setPosition(ccp(100.0f, height + 10.0f));
            m_pBatchNode->addChild(iter->pBeiWord, 1);
            
            // 初始化数字
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", iter->iMoneyAward);
            iter->pNumber1 = CCLabelAtlas::labelWithString(szTemp, "number_type_3-hd.png", 8, 14, '.');
            if (iter->pNumber1 != NULL)
            {
                iter->pNumber1->setPosition(ccp(80.0f, height + 3.0f));
                addChild(iter->pNumber1, 1);
            }
        }
        
        // 初始化行动力图标 - 上
        if (iter->iAction != 0)
        {
            // 信息条
            iter->pActionImage = CCSprite::spriteWithSpriteFrameName("map_info_energy.png");
            iter->pActionImage->setPosition(ccp(90.0f, height + 10.0f));
            m_pBatchNode->addChild(iter->pActionImage, 1);
            
            // 初始化数字
            char szTemp[32] = {0};
            sprintf(szTemp, "%d.5", iter->iAction);
            iter->pNumber1 = CCLabelAtlas::labelWithString(szTemp, "number_type_3-hd.png", 8, 14, '.');
            if (iter->pNumber1 != NULL)
            {
                iter->pNumber1->setPosition(ccp(80.0f, height + 3.0f));
                addChild(iter->pNumber1, 1);
            }
        }
        
        // 初始化掉率 - 上
        if (iter->iFollowerRate != 0)
        {
            // 信息条
            iter->pRate = CCSprite::spriteWithSpriteFrameName("map_info_item.png");
            iter->pRate->setPosition(ccp(90.0f, height + 10.0f));
            m_pBatchNode->addChild(iter->pRate, 1);
            
            // "倍"图标
            iter->pBeiWord = CCSprite::spriteWithSpriteFrameName("mark_rate.png");
            iter->pBeiWord->setPosition(ccp(100.0f, height + 10.0f));
            m_pBatchNode->addChild(iter->pBeiWord, 1);
            
            // 初始化数字
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", iter->iFollowerRate);
            iter->pNumber1 = CCLabelAtlas::labelWithString(szTemp, "number_type_3-hd.png", 8, 14, '.');
            if (iter->pNumber1 != NULL)
            {
                iter->pNumber1->setPosition(ccp(80.0f, height + 3.0f));
                addChild(iter->pNumber1, 1);
            }
        }
        
        // 初始化副本时间 - 下
        if (iter->iBeginTime != 0)
        {
            // 信息条
            iter->pRemainTimeImage = CCSprite::spriteWithSpriteFrameName("map_info_sandy.png");
            iter->pRemainTimeImage->setPosition(ccp(80.0f, height - 10.0f));
            m_pBatchNode->addChild(iter->pRemainTimeImage, 1);
            
            // "剩余"图标
            iter->pRemainWord = CCSprite::spriteWithSpriteFrameName("mark_any.png");
            iter->pRemainWord->setPosition(ccp(70.0f, height - 10.0f));
            m_pBatchNode->addChild(iter->pRemainWord, 1);
            
            // 初始化时间单位
            iter->pTimeUnit = CCSprite::spriteWithSpriteFrameName("mark_day.png");
            if (iter->pTimeUnit != NULL)
            {
                iter->pTimeUnit->setPosition(ccp(105.0f, height - 10.0f));
                m_pBatchNode->addChild(iter->pTimeUnit, 1);
            }
            
            // 初始化数字
            char szTemp[32] = {0};
            sprintf(szTemp, "0%d", iter->iBeginTime);
            iter->pNumber2 = CCLabelAtlas::labelWithString(szTemp, "number_type_3-hd.png", 8, 14, '.');
            if (iter->pNumber2 != NULL)
            {
                iter->pNumber2->setPosition(ccp(82.0f, height - 17.0f));
                addChild(iter->pNumber2, 1);
            }
        }
        
#ifdef GAME_DEBUG
        // 地图ID
        sprintf(szTemp, "MapID = %d", iter->iMapID);
        iter->pMapID = CCLabelTTF::labelWithString(szTemp, "黑体", 15);
        if (iter->pMapID != NULL)
        {
            iter->pMapID->setPosition(ccp(-100.0f, height));
            addChild(iter->pMapID, 1);
        }
#endif
        
        // 增加高度
        height -= 70.0f;
    }
    
    // 设置列表宽高度
    KNScrollView::SetItemSize(CCSize(160.0f, 60.0f));
    KNScrollView::SetHorizontalSliderX(138.0f);
    
    // 打开滚动条
    KNScrollView::SetSliderEnable(true);
    
    // 滑动到顶端
    KNScrollView::SetPoint(true);
}

/************************************************************************/
#pragma mark - Layer_MapList类显示小副本界面函数
/************************************************************************/
void Layer_MapList::ShowSubMissionFrame()
{
    MAP_ITEM_INFO stInfo = m_vecMapItemList[m_iSelectIndex];
    
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MAP_SECOND_FRAME_ID);
    if (pFrame != NULL && pFrame->getIsVisible() == false)
    {
        // 打开界面
        pFrame->setScale(1.0f);
        pFrame->setIsVisible(true);
        
        // 获得小副本数量
        int iMapNum = 0;
        for (int i = 0; i < 3; i++)
        {
            if (strcmp(stInfo.szSubImageFilename[i], "") != 0)
                ++iMapNum;
        }
        int iSubMapNum = iMapNum;
        
        KNLabel* pLabel = NULL;
        
        // 使选择框停留在第一位
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20039));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setPosition(ccp(0.0f, 83.0f));
        
        // 刷新副本信息
        for (int i = 0; i < 3; i++)
        {
            if (strcmp(stInfo.szSubImageFilename[iMapNum - 1], "") != 0 && iMapNum != 0)
            {
                // num
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20033 + i));
                if (pLabel != NULL)
                {
                    char szTemp[32] = {0};
                    sprintf(szTemp, "%d", iMapNum);
                    pLabel->GetNumberLabel()->setString(szTemp);
                }
                
                // 设置参数
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20000 + i));
                if (pLabel != NULL)
                    pLabel->setEventParm(iMapNum, 0);
                
                // frame
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(true);
                
                // num1
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20018 + i));
                if (pLabel != NULL)
                {
                    if (stInfo.iSubAction[iMapNum - 1] != 0)
                    {
                        char szTemp[32] = {0};
                        sprintf(szTemp, "%d", stInfo.iSubAction[iMapNum - 1]);
                        pLabel->GetNumberLabel()->setString(szTemp);
                        pLabel->setIsVisible(true);
                    }
                    else
                    {
                        pLabel->setIsVisible(false);
                    }
                }
                
                // num2
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20021 + i));
                if (pLabel != NULL)
                {
                    if (stInfo.iSubBattleStep[iMapNum - 1] != 0)
                    {
                        char szTemp[32] = {0};
                        sprintf(szTemp, "%d", stInfo.iSubBattleStep[iMapNum - 1]);
                        pLabel->GetNumberLabel()->setString(szTemp);
                        pLabel->setIsVisible(true);
                    }
                    else
                    {
                        pLabel->setIsVisible(false);
                    }
                }
                
                // condition
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006 + i * 2));
                if (pLabel != NULL)
                {
                    if (stInfo.iSubAction[iMapNum - 1] != 0)
                        pLabel->setIsVisible(true);
                    else
                        pLabel->setIsVisible(false);
                }
                
                // condition
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007 + i * 2));
                if (pLabel != NULL)
                {
                    if (stInfo.iSubBattleStep[iMapNum - 1] != 0)
                        pLabel->setIsVisible(true);
                    else
                        pLabel->setIsVisible(false);
                }
                
                // 副本数
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20030 + i));
                if (pLabel != NULL)
                {
                    pLabel->setIsVisible(true);
                    
                    char szTemp[32] = {0};
                    sprintf(szTemp, "%d", stInfo.iMapID);
                    pLabel->GetNumberLabel()->setString(szTemp);
                    
                    if (stInfo.iMapID / 10 != 0)
                    {
                        CCPoint pos = pLabel->GetNumberLabel()->getPosition();
                        pos.x = -50.0f;
                        pLabel->GetNumberLabel()->setPosition(pos);
                    }
                    else
                    {
                        CCPoint pos = pLabel->GetNumberLabel()->getPosition();
                        pos.x = -40.0f;
                        pLabel->GetNumberLabel()->setPosition(pos);
                    }
                }
                
                // 显示小副本数
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20033 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(true);
                
                // 隐藏标记
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20036 + i));
                if (pLabel != NULL)
                {
                    if (m_vecMapItemList.size() == 1 && iSubMapNum == 1)
                        pLabel->resetLabelImage("mission_new_mark.png");
                    else
                    {
                        if (stInfo.iSubState[iMapNum - 1] != 0)
                            pLabel->resetLabelImage("mission_new_mark.png");
                        else
                            pLabel->resetLabelImage("mission_clear_mark.png");
                    }
                }
                
#ifdef GAME_DEBUG
                // id
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20024 + i));
                if (pLabel != NULL)
                {
                    char szTemp[32] = {0};
                    sprintf(szTemp, "sub id = %d", stInfo.iSubMapID[i]);
                    pLabel->setLabelText(szTemp, NULL, 0);
                }
#endif
                
                // 递减
                --iMapNum;
            }
            else
            {
                // icon
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20000 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
                
                // frame
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
                
                // condition
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006 + i * 2));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007 + i * 2));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
                
                // time
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
                
                // gun
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
                
                // num1
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20018 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
                
                // num2
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20021 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
                
                // 副本数
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20030 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
                
                // 显示小副本数
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20033 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
                
                // 隐藏标记
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20036 + i));
                if (pLabel != NULL)
                    pLabel->setIsVisible(false);
            }
        }
    }
}

/************************************************************************/
#pragma mark - Layer_MapList类是否可以进入关卡函数
/************************************************************************/
bool Layer_MapList::IsEnterBattleEnable(int iSubMapIndex)
{
    if (iSubMapIndex < 0 || iSubMapIndex > 3)
        return false;
    
    MAP_ITEM_INFO stInfo = m_vecMapItemList[m_iSelectIndex];
    if (stInfo.iSubAction[iSubMapIndex] > PlayerDataManage::m_PlayerActivity)
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_MapList类事件函数 : 点击
/************************************************************************/
bool Layer_MapList::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    
    CCRect visibleRect = GetVisibleRange();
    if (point.x > visibleRect.origin.x && point.x < visibleRect.origin.x + visibleRect.size.width &&
        point.y > visibleRect.origin.y && point.y < visibleRect.origin.y + visibleRect.size.height)
    {
        KNScrollView::ccTouchBegan(pTouch, pEvent);
        
        // 获得点击坐标
        point = convertToNodeSpace(point);
        
        // 检测那个楼层被选中
        CCPoint pos(0.0f, 0.0f);
        CCSize size(0.0f, 0.0f);
        vector<MAP_ITEM_INFO>::iterator iter;
        for (iter = m_vecMapItemList.begin(); iter != m_vecMapItemList.end(); iter++)
        {
            pos = iter->pFrameImage->getPosition();
            size = iter->pFrameImage->getContentSize();
            
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                // 设置选择标记
                m_pSelectFrame->setPosition(ccp(pos.x, pos.y + 2.0f));
                
                // 播放音效
                KNUIFunction::GetSingle()->PlayerEffect("1.wav");
                break;
            }
        }
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_MapList类事件函数 : 弹起
/************************************************************************/
void Layer_MapList::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    KNScrollView::ccTouchEnded(pTouch, pEvent);
    
    if (GetIsMovingMark())
        return;
    
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    point = convertToNodeSpace(point);
    
    // 检测那个楼层被选中
    CCPoint pos(0.0f, 0.0f);
    CCSize size(0.0f, 0.0f);
    vector<MAP_ITEM_INFO>::iterator iter;
    m_iSelectIndex = 0;
    for (iter = m_vecMapItemList.begin(); iter != m_vecMapItemList.end(); iter++)
    {
        pos = iter->pFrameImage->getPosition();
        size = iter->pFrameImage->getContentSize();
        
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
        {
            // 显示小副本
            ShowSubMissionFrame();
            
            // 记录选择的副本id
            m_iSelectMapID = iter->iMapID;
            if (m_iSelectMapID == 0)
            {
                KNUIFunction::GetSingle()->OpenMessageBoxFrame("mapid = 0了");
                return;
            }
            
            // 隐藏自己
            KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_CENTER_FRAME_ID);
            if (pFrame != NULL && pFrame->getIsVisible() == true)
                pFrame->setIsVisible(false);
            
            // 新手引导
            if (PlayerDataManage::m_GuideMark)
                GameGuide::GetSingle()->FinishOneFloorGuide();
            
            break;
        }
        
        ++m_iSelectIndex;
    }
}

/************************************************************************/
#pragma mark - Layer_MapList类事件函数 : 滑动
/************************************************************************/
void Layer_MapList::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
{
    KNScrollView::ccTouchMoved(pTouch, pEvent);
}