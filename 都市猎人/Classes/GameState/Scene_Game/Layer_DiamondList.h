//
//  Layer_DiamondList.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-7-23.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_DiamondList_h
#define MidNightCity_Layer_DiamondList_h

#include "cocos2d.h"

#include "KNScrollView.h"

// 钻石选项信息
struct DIAMOND_INFO
{
    CCSprite* pInfo;                            // 信息背景
    
    CCLabelAtlas* pPrice;                       // 价格数字
    
    CCSprite* pButtonNormal;                    // 按钮 : 普通
    CCSprite* pButtonSelect;                    // 按钮 : 选择
    
    // 构造函数
    DIAMOND_INFO()                              {memset(this, 0, sizeof(DIAMOND_INFO));}
};

// 列表类定义
class Layer_DiamondList : public KNScrollView
{
public:
    #pragma mark - 构造函数
    Layer_DiamondList();
    #pragma mark - 析构函数
    virtual ~Layer_DiamondList();
    
    #pragma mark - 初始化函数 : 使用自动内存使用
    bool init();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 重写退出函数 : 使用自动内存使用
    virtual void onExit();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 刷新钻石商店列表函数
    void RefreshDiamondShopList();
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(Layer_DiamondList);
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 划动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
private:
    // 钻石列表对象变量
    CCSpriteBatchNode* m_pBatchNode;                // 绘制节点
    
    vector<DIAMOND_INFO> m_vecDiamondList;          // 钻石商品列表
    
    // 钻石列表属性变量
};

#endif
