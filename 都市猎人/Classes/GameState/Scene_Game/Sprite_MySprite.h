//
//  Sprite_MySprite.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-7-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Sprite_MySprite_h
#define MidNightCity_Sprite_MySprite_h

#include "cocos2d.h"

USING_NS_CC;

class Sprite_MySprite : public CCSprite
{
public:
    #pragma mark - 静态函数 : 生成精灵函数
    static Sprite_MySprite* mySpriteWithSpriteFrameName(const char* filename);
    
    #pragma mark - 添加一个偏移位置函数
    void addOnePositionOffset(CCPoint point);
    
    #pragma mark - 清空所有偏移位置函数
    void clearAllPositionOffset();
    
    #pragma mark - 连接cocos2d初始化方式
    LAYER_NODE_FUNC(Sprite_MySprite);
private:
    #pragma mark - 构造函数
    Sprite_MySprite();
    #pragma mark - 析构函数
    virtual ~Sprite_MySprite();
    
    #pragma mark - 重写函数 : visit函数
    virtual void visit();
private:
    // 自定义精灵对象变量
    
    // 自定义精灵属性变量
    vector<CCPoint> m_vecPosOffsetList;                     // 偏移位置列表
};

#endif
