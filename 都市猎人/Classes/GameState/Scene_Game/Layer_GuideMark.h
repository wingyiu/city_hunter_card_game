//
//  Layer_GuideMark.h
//  都市猎人
//
//  Created by 孙 惠伟 on 12-9-28.
//
//

#ifndef MidNightCity_Layer_GuideMark_h
#define MidNightCity_Layer_GuideMark_h

#include "cocos2d.h"

USING_NS_CC;

enum MARKTYPE
{
    TOUCH = 0,                  // 点击
};

class Layer_GuideMark : public CCLayer
{
public:
    #pragma mark - 获得单列对象函数
    static Layer_GuideMark* GetSingle();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 退出函数
    virtual void onExit();
    
    #pragma mark - 显示引导标志函数
    void ShowGuideMark(MARKTYPE eType = TOUCH);
    
    #pragma mark - 连接cocos2d初始化方式
    LAYER_NODE_FUNC(Layer_GuideMark);
private:
    #pragma mark - 构造函数
    Layer_GuideMark();
    #pragma mark - 析构函数
    virtual ~Layer_GuideMark();
    
    #pragma mark - 回调函数 : 初始化点击效果函数
    void ProcessTouchEffect(CCObject* pObject);
private:
    // 引导标志对象变量
    CCSprite* m_pGuideMark;
    
    // 引导标志属性变量
};

#endif
