//
//  Layer_MailList.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-7-23.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_MailList.h"
#include "KNUIManager.h"
#include "Layer_Game.h"
#include <time.h>

/************************************************************************/
#pragma mark - Layer_MailList类构造函数
/************************************************************************/
Layer_MailList::Layer_MailList()
{
    // 为邮件列表对象变量赋初值
    m_vecMailList.clear();
    
    // 为邮件列表属性变量赋初值
    m_iEditedFriendServerID = 0;
    m_iSelectedMailIndex = 0;
}

/************************************************************************/
#pragma mark - Layer_MailList类析构函数
/************************************************************************/
Layer_MailList::~Layer_MailList()
{
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_MailList类初始化函数 : 使用自动内存使用
/************************************************************************/
bool Layer_MailList::init()
{
    return CCLayer::init();
}

/************************************************************************/
#pragma mark - Layer_MailList类销毁函数
/************************************************************************/
void Layer_MailList::Destroy()
{
    vector<MAIL_ITEM_INFO>::iterator iter;
    for (iter = m_vecMailList.begin(); iter != m_vecMailList.end(); iter++)
    {
        removeChild(iter->pIcon, true);
        removeChild(iter->pFrame, true);
        removeChild(iter->pLvMark, true);
        removeChild(iter->pLvBack, true);
        removeChild(iter->pState, true);
        
        removeChild(iter->pCheckNormal, true);
        removeChild(iter->pCheckSelect, true);
        
        removeChild(iter->pHandleNormal, true);
        removeChild(iter->pHandleSelect, true);
        
        removeChild(iter->pFollowerName, true);
        removeChild(iter->pMailSendTime, true);
        
        removeChild(iter->pFollowerLevel, true);
        removeChild(iter->pSenderLevel, true);
    }
    m_vecMailList.clear();
}

/************************************************************************/
#pragma mark - Layer_MailList类重写退出函数 : 使用自动内存使用
/************************************************************************/
void Layer_MailList::onExit()
{
    CCLayer::onExit();
    
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_MailList类初始化函数
/************************************************************************/
bool Layer_MailList::init(UIInfo& stInfo)
{
    // 初始化scrollview
    KNScrollView::init(stInfo);
    
    // 设置可视范围
    SetVisibleRange(CCRect(stInfo.point.x, stInfo.point.y, stInfo.size.width, stInfo.size.height));
    
    // 初始化精灵集合
    RefreshMailList();
    
    // 设置列表属性
    KNScrollView::SetItemSize(CCSize(160.0f, 60.0f));
    KNScrollView::SetHorizontalSliderX(138.0f);
    KNScrollView::SetSliderEnable(true);
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_MailList类刷新钻石商店列表函数
/************************************************************************/
void Layer_MailList::RefreshMailList()
{
    // 移出所有小弟
    vector<MAIL_ITEM_INFO>::iterator iter;
    for (iter = m_vecMailList.begin(); iter != m_vecMailList.end(); iter++)
    {
        removeChild(iter->pIcon, true);
        removeChild(iter->pFrame, true);
        removeChild(iter->pLvMark, true);
        removeChild(iter->pLvBack, true);
        removeChild(iter->pState, true);
        
        removeChild(iter->pCheckNormal, true);
        removeChild(iter->pCheckSelect, true);
        
        removeChild(iter->pHandleNormal, true);
        removeChild(iter->pHandleSelect, true);
        
        removeChild(iter->pFollowerName, true);
        removeChild(iter->pMailSendTime, true);
        
        removeChild(iter->pFollowerLevel, true);
        removeChild(iter->pSenderLevel, true);
    }
    m_vecMailList.clear();
    
    // 计算列表高度
    float height = 0.0f;
    list <MailInfo*> mailList = *PlayerDataManage::ShareInstance()->GetMyMailList();
    height = mailList.size() * 60.0f;
    
    KNScrollView::SetScrollSize(ccp(320.0f, height));
    if (height < KNScrollView::GetScrollSize().y)
        height = KNScrollView::GetScrollSize().y;
    
    height -= 60.0f;
    
    // 获得数据刷新列表
    list<MailInfo*>::iterator mailIter;
    for (mailIter = mailList.begin(); mailIter != mailList.end(); mailIter++)
    {
        // 是否已经拥有该邮件
        if (IsMailExist((*mailIter)->Mail_SenderID) == true)
            continue;
        
        // 建立邮件选项结构
        MAIL_ITEM_INFO stInfo;
        
        // 获得小弟信息
        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower((*mailIter)->Mail_Sender_FollowerID);
        if (pData == NULL)
        {
            printf("mail follower id error = %d\n", (*mailIter)->Mail_Sender_FollowerID);
            continue;
        }
        
        // 更新邮件数据
        stInfo.iIndex = (*mailIter)->Mail_Index;
        stInfo.iMailType = (*mailIter)->Mail_Type;
        stInfo.iServerID = (*mailIter)->Mail_SenderID;
        stInfo.iFollowerID = (*mailIter)->Mail_Sender_FollowerID;
        stInfo.iLevel = (*mailIter)->Mail_SenderLevel;
        stInfo.iFollowerLevel = (*mailIter)->Mail_Sender_FollowerLevel;
        
        memcpy(stInfo.szPlayerName, (*mailIter)->Mail_SenderName, 32);
        printf("%s\n", stInfo.szPlayerName);
        
        // 更新icon - 小弟
        stInfo.pIcon = CCSprite::spriteWithSpriteFrameName(pData->Follower_IconName);
        if (stInfo.pIcon != NULL)
        {
            stInfo.pIcon->setPosition(ccp(-105.0f, height));
            addChild(stInfo.pIcon);
        }
        
        // 更新frame
        char szTemp[64] = {0};
        sprintf(szTemp, "info_frame_%d.png", pData->Follower_Profession);
        stInfo.pFrame = CCSprite::spriteWithSpriteFrameName(szTemp);
        if (stInfo.pFrame != NULL)
        {
            stInfo.pFrame->setPosition(ccp(0.0f, height));
            addChild(stInfo.pFrame);
        }
        
        // 更新lv标签
        stInfo.pLvMark = CCSprite::spriteWithSpriteFrameName("label_lv.png");
        if (stInfo.pLvMark != NULL)
        {
            stInfo.pLvMark->setPosition(ccp(-100.0f, height - 20.0f));
            addChild(stInfo.pLvMark);
        }
        
        // 更新lv背景
        stInfo.pLvBack = CCSprite::spriteWithSpriteFrameName("friend_level_mark.png");
        if (stInfo.pLvBack != NULL)
        {
            stInfo.pLvBack->setPosition(ccp(20.0f, height));
            addChild(stInfo.pLvBack);
        }
        
        // 更新状态
        if ((*mailIter)->Mail_State == MailState_New)
        {
            stInfo.pState = CCSprite::spriteWithSpriteFrameName("mission_new_mark.png");
            if (stInfo.pState != NULL)
            {
                stInfo.pState->setPosition(ccp(-85.0f, height + 20.0f));
                addChild(stInfo.pState);
            }
        }
        
        // 更新按钮
        if ((*mailIter)->Mail_Type == MailType_Message)
        {
            stInfo.pCheckNormal = CCSprite::spriteWithSpriteFrameName("button_mailcheck_normal.png");
            if (stInfo.pCheckNormal != NULL)
            {
                stInfo.pCheckNormal->setPosition(ccp(100.0f, height));
                addChild(stInfo.pCheckNormal);
            }
            stInfo.pCheckSelect = CCSprite::spriteWithSpriteFrameName("button_mailcheck_select.png");
            if (stInfo.pCheckSelect != NULL)
            {
                stInfo.pCheckSelect->setIsVisible(false);
                
                stInfo.pCheckSelect->setPosition(ccp(100.0f, height));
                addChild(stInfo.pCheckSelect);
            }
        }
        else if ((*mailIter)->Mail_Type == MailType_FriendApply)
        {
            stInfo.pHandleNormal = CCSprite::spriteWithSpriteFrameName("button_mailprocess_normal.png");
            if (stInfo.pHandleNormal != NULL)
            {
                stInfo.pHandleNormal->setPosition(ccp(100.0f, height));
                addChild(stInfo.pHandleNormal);
            }
            stInfo.pHandleSelect = CCSprite::spriteWithSpriteFrameName("button_mailprocess_select.png");
            if (stInfo.pHandleSelect != NULL)
            {
                stInfo.pHandleSelect->setIsVisible(false);
                
                stInfo.pHandleSelect->setPosition(ccp(100.0f, height));
                addChild(stInfo.pHandleSelect);
            }
        }
        
        // 姓名
        printf("MailSenderName = %s\n", (*mailIter)->Mail_SenderName);
        stInfo.pFollowerName = CCLabelTTF::labelWithString((*mailIter)->Mail_SenderName, "黑体", 11);
        if (stInfo.pFollowerName != NULL)
        {
            stInfo.pFollowerName->setPosition(ccp(-75.0f, height));
            stInfo.pFollowerName->setColor(ccc3(0, 0, 0));
            stInfo.pFollowerName->setAnchorPoint(ccp(0.0f, 0.0f));
            addChild(stInfo.pFollowerName);
        }
        
        // 发送时间
        time_t time = (*mailIter)->Mail_SendTime;
        struct tm* ptm = gmtime(&time);
        sprintf(szTemp, "%d年%d月%d日", ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday);
        stInfo.pMailSendTime = CCLabelTTF::labelWithString(szTemp, "黑体", 9);
        if (stInfo.pMailSendTime != NULL)
        {
            stInfo.pMailSendTime->setPosition(ccp(-75.0f, height - 15.0f));
            stInfo.pMailSendTime->setColor(ccc3(0, 0, 0));
            stInfo.pMailSendTime->setAnchorPoint(ccp(0.0f, 0.0f));
            addChild(stInfo.pMailSendTime);
        }
        
        // 更新好友小弟等级
        sprintf(szTemp, "%d", (*mailIter)->Mail_Sender_FollowerLevel);
        stInfo.pFollowerLevel = CCLabelAtlas::labelWithString(szTemp, "RoundNum-hd.png", 10, 15, '.');
        if (stInfo.pFollowerLevel != NULL)
        {
            stInfo.pFollowerLevel->setPosition(ccp(-95.0f, height - 25.0f));
            stInfo.pFollowerLevel->setScale(0.7f);
            addChild(stInfo.pFollowerLevel);
        }
        
        // 更新好友等级
        sprintf(szTemp, "%d", (*mailIter)->Mail_SenderLevel);
        stInfo.pSenderLevel = CCLabelAtlas::labelWithString(szTemp, "UINum-hd.png", 8, 14, '.');
        if (stInfo.pSenderLevel != NULL)
        {
            stInfo.pSenderLevel->setPosition(ccp(25.0f, height - 8.0f));
            addChild(stInfo.pSenderLevel);
        }
        
        // 递减高度
        height -= 60.0f;
        
        // 添加到列表
        m_vecMailList.push_back(stInfo);
    }
    
    // 滑动到顶端
    KNScrollView::SetPoint(true);
}

/************************************************************************/
#pragma mark - Layer_MailList类显示邮件信息函数
/************************************************************************/
void Layer_MailList::ShowMailInfo()
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_MAIL_CHECK_FRAME_ID);
    if (pFrame != NULL && pFrame->getIsVisible() == false)
        pFrame->setIsVisible(true);
}

/************************************************************************/
#pragma mark - Layer_MailList类显示好友请求邮件函数
/************************************************************************/
void Layer_MailList::ShowFriendRequestMailInfo(MAIL_ITEM_INFO& stInfo)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_REQUEST_CONFIRM_FRAME_ID);
    if (pFrame != NULL && pFrame->getIsVisible() == false)
    {
        pFrame->setIsVisible(true);
        
        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(stInfo.iFollowerID);
        if (pData == NULL)
        {
            printf("Layer_MailList::ShowFriendRequestMailInfo id error = %d\n", stInfo.iFollowerID);
            return;
        }
        
        // 小弟头像
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
            pLabel->resetLabelImage(pData->Follower_IconName);
        
        // 小弟边筐
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "info_frame_%d.png", pData->Follower_Profession);
            pLabel->resetLabelImage(szTemp);
        }
        
        // 玩家姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
            pLabel->GetTextLabel()->setString(stInfo.szPlayerName);
        
        // 好友小弟等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", stInfo.iFollowerLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 好友等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", stInfo.iLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 邮件内容姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20010));
        if (pLabel != NULL)
            pLabel->GetTextLabel()->setString(stInfo.szPlayerName);
    }
}

/************************************************************************/
#pragma mark - Layer_MailList类更新邮件数量函数
/************************************************************************/
void Layer_MailList::UpdateMailNumber()
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MAIL_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", static_cast<int>(m_vecMailList.size()));
            
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
}

/************************************************************************/
#pragma mark - Layer_MailList类事件函数 : 点击
/************************************************************************/
bool Layer_MailList::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    
    CCRect visibleRect = GetVisibleRange();
    if (point.x > visibleRect.origin.x && point.x < visibleRect.origin.x + visibleRect.size.width &&
        point.y > visibleRect.origin.y && point.y < visibleRect.origin.y + visibleRect.size.height)
    {
        KNScrollView::ccTouchBegan(pTouch, pEvent);
        
        // 转换坐标
        point = convertToNodeSpace(point);
        
        // 遍历小弟列表，检测那个小弟被点击
        vector<MAIL_ITEM_INFO>::iterator iter;
        
        CCPoint pos(0.0f, 0.0f);
        CCSize size(0.0f, 0.0f);
        for (iter = m_vecMailList.begin(); iter != m_vecMailList.end(); iter++)
        {
            if (iter->iMailType == MailType_Message)
            {
                pos = iter->pCheckNormal->getPosition();
                size = iter->pCheckNormal->getContentSize();
                
                if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                    point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                {
                    // 变换按钮状态
                    iter->pCheckNormal->setIsVisible(false);
                    iter->pCheckSelect->setIsVisible(true);
                    
                    return true;
                }
            }
            else if (iter->iMailType == MailType_FriendApply)
            {
                pos = iter->pHandleNormal->getPosition();
                size = iter->pHandleNormal->getContentSize();
                
                if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                    point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                {
                    // 变换按钮状态
                    iter->pHandleNormal->setIsVisible(false);
                    iter->pHandleSelect->setIsVisible(true);
                    
                    return true;
                }
            }
        }
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_MailList类事件函数 : 弹起
/************************************************************************/
void Layer_MailList::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    KNScrollView::ccTouchEnded(pTouch, pEvent);
    
    // 获得点击坐标
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    // 遍历小弟列表，检测那个小弟被点击
    vector<MAIL_ITEM_INFO>::iterator iter;
    
    CCPoint pos(0.0f, 0.0f);
    CCSize size(0.0f, 0.0f);
    for (iter = m_vecMailList.begin(); iter != m_vecMailList.end(); iter++)
    {
        if (iter->iMailType == MailType_Message)
        {
            pos = iter->pCheckNormal->getPosition();
            size = iter->pCheckNormal->getContentSize();
            
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                // 显示邮件信息
                ShowMailInfo();
            }
            
            // 变换按钮状态
            iter->pCheckNormal->setIsVisible(true);
            iter->pCheckSelect->setIsVisible(false);
        }
        else if (iter->iMailType == MailType_FriendApply)
        {
            pos = iter->pHandleNormal->getPosition();
            size = iter->pHandleNormal->getContentSize();
            
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                // 显示好友请求邮件
                ShowFriendRequestMailInfo(*iter);
                
                // 记录被编辑的好友ServerID
                m_iEditedFriendServerID = iter->iServerID;
                // 记录被处理的邮件序号
                m_iSelectedMailIndex = iter->iIndex;
                
                // 关闭新邮件标记
                if (iter->pState != NULL)
                    iter->pState->setIsVisible(false);
                PlayerDataManage::ShareInstance()->SetMailStateToOld(iter->iIndex);
            }
            
            // 变换按钮状态
            iter->pHandleNormal->setIsVisible(true);
            iter->pHandleSelect->setIsVisible(false);
        }
    }
}

/************************************************************************/
#pragma mark - Layer_MailList类事件函数 : 滑动
/************************************************************************/
void Layer_MailList::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
{
    KNScrollView::ccTouchMoved(pTouch, pEvent);
}

/************************************************************************/
#pragma mark - Layer_MailList类是否已存在该邮件函数
/************************************************************************/
bool Layer_MailList::IsMailExist(int iSenderID)
{
    vector<MAIL_ITEM_INFO>::iterator iter;
    for (iter = m_vecMailList.begin(); iter != m_vecMailList.end(); iter++)
    {
        if (iter->iServerID == iSenderID)
            return true;
    }
    
    return false;
}