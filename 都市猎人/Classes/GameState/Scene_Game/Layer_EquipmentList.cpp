//
//  Layer_EquipmentList.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-5-2.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//
//
//#include "Layer_EquipmentList.h"
//#include "KNUIManager.h"

/************************************************************************/
#pragma mark - Layer_EquipmentList类构造函数
/************************************************************************/
//Layer_EquipmentList::Layer_EquipmentList()
//{
//    // 为装备列表对象变量赋初值
//    m_vecEquipmentList.clear();
//    
//    // 为装备列表属性变量赋初值
//    m_eEquipType = WEAPON;
//    
//    m_eState = CHECK;
//    
//    m_iEnableIndex = 0;
//}

/************************************************************************/
#pragma mark - Layer_EquipmentList类析构函数
/************************************************************************/
//Layer_EquipmentList::~Layer_EquipmentList()
//{
//    // 销毁所有对象
//    Destroy();
//}

/************************************************************************/
#pragma mark - Layer_EquipmentList类销毁函数
/************************************************************************/
//void Layer_EquipmentList::Destroy()
//{
//    // 销毁装备列表
//    vector<Equipment*>::iterator iter;
//    for (iter = m_vecEquipmentList.begin(); iter != m_vecEquipmentList.end(); iter++)
//        removeChild((*iter), true);
//    m_vecEquipmentList.clear();
//}

/************************************************************************/
#pragma mark - Layer_EquipmentList类初始化函数 : 使用自动内存使用
/************************************************************************/
//bool Layer_EquipmentList::init()
//{
//    return CCLayer::init();
//}

/************************************************************************/
#pragma mark - Layer_EquipmentList类退出函数 : 使用自动内存使用
/************************************************************************/
//void Layer_EquipmentList::onExit()
//{
//    // 销毁所有对象
//    Destroy();
//}

/************************************************************************/
#pragma mark - Layer_EquipmentList类初始化函数
/************************************************************************/
//bool Layer_EquipmentList::init(UIInfo& stInfo)
//{
//    // 初始化基类
//    KNScrollLayer::init(stInfo);
//    
//    // 设置列表类型
//    m_eEquipType = static_cast<Equipment_Type>(stInfo.iData);
//    
//    // 设置列表状态
//    m_eState = CHECK;
//    
//    // 保存类型
//    m_eUIType = stInfo.eType;
//    
//    // 刷新列表
//    RefreshItemListFromDynamicData();
//    
//    return true;
//}

/************************************************************************/
#pragma mark - Layer_EquipmentList类从仓库数据更新列表选项函数
/************************************************************************/
//void Layer_EquipmentList::RefreshItemListFromDynamicData()
//{
    // 清空列表
    /*for (vector<Equipment*>::iterator iter = m_vecEquipmentList.begin(); iter != m_vecEquipmentList.end(); iter++)
        removeChild(*iter, true);
    m_vecEquipmentList.clear();
    
    KNScrollLayer::SetPageNum(1);
    
    // 获得数据列表
    MyObjectDepot dataList = *PlayerDataManage::ShareInstance()->Get_MyObjectDepot(My_Equipment);
    
    // 更新到列表
    CCPoint pos(0.0f, 0.0f);
    int index = 0;
    float fOffsetX = 0.0f;
    for (MyObjectDepot::iterator iter = dataList.begin(); iter != dataList.end(); iter++)
    {
        // 不是本列表类型跳过
        if (iter->first != m_eEquipType)
            continue;
        
        list <CCLayer *>::iterator it2;
        for (it2 = iter->second.begin(); it2 != iter->second.end(); it2++)
        {
            Equipment* pEquipment = dynamic_cast<Equipment*>(*it2);
            if (pEquipment != NULL)
            {
                pos.x = -105.0f + (index % 4) * 70.0f + fOffsetX;
                pos.y = 70.0f - (index / 4) * 70.0f;
                pEquipment->setPosition(pos);
                addChild(pEquipment);
                
                // 添加到类表
                m_vecEquipmentList.push_back(pEquipment);
                
                ++index;
                
                // test
                if (index > 11)
                {
                    index = 0;
                    fOffsetX += 285.0f;
                    
                    KNScrollLayer::SetPageNum(KNScrollLayer::GetPageNum() + 1);
                }
            }
        }
        
        break;
    }*/
//}

/************************************************************************/
#pragma mark - Layer_EquipmentList类事件函数 : 点击
/************************************************************************/
//bool Layer_EquipmentList::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
//{
//    // 获得点击坐标
//    CCPoint point = pTouch->locationInView(pTouch->view());
//    point = CCDirector::sharedDirector()->convertToGL(point);
//    point = convertToNodeSpace(point);
//    
//    if (KNScrollLayer::ccTouchBegan(pTouch, pEvent) == true)
//    {
//        CCPoint equipPos(0.0f, 0.0f);
//        CCSize equipSize(0.0f, 0.0f);
//        
//        // 检测那件装备被点击
//        /*vector<Equipment*>::iterator iter;
//        for (iter = m_vecEquipmentList.begin(); iter != m_vecEquipmentList.end(); iter++)
//        {
//            equipPos = (*iter)->getPosition();
//            equipSize = (*iter)->getContentSize();
//            if (point.x > equipPos.x - equipSize.width / 2.0f && point.x < equipPos.x + equipSize.width / 2.0f &&
//                point.y > equipPos.y - equipSize.height / 2.0f && point.y < equipPos.y + equipSize.height / 2.0f)
//            {
//                if (m_eState == EQUIP)
//                {
//                    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(11004);
//                    if (pFrame != NULL)
//                    {
//                        Follower* pFollower = dynamic_cast<Follower*>(pFrame->getCommonLayer(0));
//                        if (pFollower != NULL)
//                        {
//                            pFollower->SetEquipment(*iter);
//                            
//                            KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(11004);
//                            if (pFrame != NULL)
//                            {
//                                KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011 + m_eEquipType - 1));
//                                if (pLabel != NULL)
//                                    pLabel->resetLabelImage((*iter)->GetData()->Equipment_IconName);
//                            }
//                            
//                            RefreshItemListFromDynamicData();
//                            
//                            //m_eState = CHECK;
//                            
//                            break;
//                        }
//                    }
//                }
//                break;
//            }
//        }*/
//        
//        return true;
//    }
//    
//    return false;
//}

/************************************************************************/
#pragma mark - Layer_EquipmentList类事件函数 : 弹起
/************************************************************************/
//void Layer_EquipmentList::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
//{
//    KNScrollLayer::ccTouchEnded(pTouch, pEvent);
//}

/************************************************************************/
#pragma mark - Layer_EquipmentList类事件函数 : 滑动
/************************************************************************/
//void Layer_EquipmentList::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
//{
//    KNScrollLayer::ccTouchMoved(pTouch, pEvent);
//}