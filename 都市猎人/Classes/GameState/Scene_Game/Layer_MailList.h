//
//  Layer_MailList.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-7-23.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_MailList_h
#define MidNightCity_Layer_MailList_h

#include "KNScrollView.h"
#include "SystemDataManage.h"

// 邮件列表选项信息
struct MAIL_ITEM_INFO
{
    CCSprite* pIcon;                            // 小弟头像icon
    CCSprite* pFrame;                           // 小弟底框
    CCSprite* pLvMark;                          // 等级标记
    CCSprite* pLvBack;                          // 等级背景
    CCSprite* pState;                           // 状态
    
    CCSprite* pCheckNormal;                     // 查看按钮-普通
    CCSprite* pCheckSelect;                     // 查看按钮-选择
    
    CCSprite* pHandleNormal;                    // 处理按钮-普通
    CCSprite* pHandleSelect;                    // 处理按钮-选择
    
    CCLabelTTF* pFollowerName;                  // 小弟姓名
    CCLabelTTF* pMailSendTime;                  // 邮件发送时间
    
    CCLabelAtlas* pFollowerLevel;               // 好友小弟等级
    CCLabelAtlas* pSenderLevel;                 // 发送者等级
    
    int iIndex;                                 // 邮件序号
    int iMailType;                              // 邮件类型
    int iServerID;                              // 服务器标识ID
    int iFollowerID;                            // 小弟id
    int iLevel;                                 // 等级
    int iFollowerLevel;                         // 玩家等级
    
    char szPlayerName[32];                      // 玩家姓名
    
    // 构造函数
    MAIL_ITEM_INFO()                            {memset(this, 0, sizeof(MAIL_ITEM_INFO));}
};

// 列表类定义
class Layer_MailList : public KNScrollView
{
public:
    #pragma mark - 构造函数
    Layer_MailList();
    #pragma mark - 析构函数
    virtual ~Layer_MailList();
    
    #pragma mark - 初始化函数 : 使用自动内存使用
    bool init();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 重写退出函数 : 使用自动内存使用
    virtual void onExit();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 刷新钻石商店列表函数
    void RefreshMailList();
    
    #pragma mark - 显示邮件信息函数
    void ShowMailInfo();
    #pragma mark - 显示好友请求邮件函数
    void ShowFriendRequestMailInfo(MAIL_ITEM_INFO& stInfo);
    
    #pragma mark - 获得被编辑的好友ServerID函数
    int GetEditedFriendServerID() const                         {return m_iEditedFriendServerID;}
    #pragma mark - 获得被选择的邮件序号函数
    int GetSelectedMailIndex() const                            {return m_iSelectedMailIndex;}
    
    #pragma mark - 更新邮件数量函数
    void UpdateMailNumber();
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(Layer_MailList);
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 划动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
private:
    #pragma mark - 是否已存在该邮件函数
    bool IsMailExist(int iSenderID);
private:
    // 邮件列表对象变量
    vector<MAIL_ITEM_INFO> m_vecMailList;                           // 好友小弟列表
    
    // 邮件列表属性变量
    int m_iEditedFriendServerID;                                    // 被编辑的好友serverid
    int m_iSelectedMailIndex;                                       // 被选择的邮件序号
};

#endif