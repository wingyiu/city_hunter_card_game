//
//  Scene_Game.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-3-30.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Scene_Game.h"
#include "Layer_Enemy.h"
#include "GameController.h"
#include "Layer_Block.h"

/************************************************************************/
#pragma mark - Scene_Game类创建场景函数
/************************************************************************/
CCScene * Scene_Game::scene()
{
    // 创建场景
    CCScene* scene = CCScene::node();
    if (scene == NULL)
        return NULL;
    
    // 创建game图层
    Scene_Game* game = Scene_Game::node();
    if (game == NULL)
        return NULL;
    
    // 添加图层到场景
    scene->addChild(game, 0, GAME_SCENE_ID);
  
    return scene;
}

/************************************************************************/
#pragma mark - Scene_Game类构造函数
/************************************************************************/
Scene_Game::Scene_Game()
{
    
}

/************************************************************************/
#pragma mark - Scexne_Game类析构函数
/************************************************************************/
Scene_Game::~Scene_Game()
{
    printf("Scene_Game:: 释放完成\n");
}

/************************************************************************/
#pragma mark - Scene_Game类初始化函数
/************************************************************************/
bool Scene_Game::init()
{
    // 初始化基类
    if(CCLayer::init() == false)
        return false;
    
    // 新手引导 : 初始化新手引导类
    Layer_GameGuide* pGuide = Layer_GameGuide::node();
    if (pGuide != NULL)
        pGuide->retain();

    // 初始化uimanager
    KNUIManager* pUIManager = KNUIManager::CreateManager();
    if (pUIManager->LoadUIConfigFromFile("gameUI_config.xml") == true)
    {
        pUIManager->setVertexZ(2.0f);
        this -> addChild(pUIManager, 1, GAME_UI_ID);
    }
    
    // 初始化游戏场景
    Layer_Game* pGame = Layer_Game::node();
    if (pGame != NULL)
    {
        pGame->setVertexZ(1.0f);
        this -> addChild(pGame);
    }
    
    // 新手引导 : 返回提示提示小弟引导
    if (PlayerDataManage::m_GuideMark)
        Layer_GameGuide::GetSingle()->ShowFloorHit();
    
    // 预读音效
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("upgrade.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("2.wav");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("3.wav");
    
    return true;
}

/************************************************************************/
#pragma mark - Scene_Game类退出函数
/************************************************************************/
void Scene_Game::onExit()
{
    // 调用基类退出函数
    CCLayer::onExit();
    
    // 移除ui层
    removeChild(getChildByTag(GAME_UI_ID), true);
    
    // 移除场景所有节点，再做一次清除
    removeAllChildrenWithCleanup(true);
    this -> removeFromParentAndCleanup(true);
    
    //留存的数据撤销所绑定的图片资源
    PlayerDataManage::ShareInstance() -> RemoveDataImage();
    
    // 移除场景资源
    RemoveRes("game_building-hd.plist", "game_building-hd.pvr.ccz");
    RemoveRes("game_1-hd.plist", "game_1-hd.pvr.ccz");
    RemoveRes("game_2-hd.plist", "game_2-hd.pvr.ccz");
    RemoveRes("game_3-hd.plist", "game_3-hd.pvr.ccz");
    RemoveRes("game_follower-hd.plist", "game_follower-hd.pvr.ccz");
    RemoveRes("npc-hd.plist", "npc-hd.pvr.ccz");
}

/*bool Scene_Game::ccTouchBegan(CCTouch * pTouch, CCEvent * pEvent)
{
    GameController::ShareInstance() -> ReadyToGameScene(GameState_Battle);
    return true;
}*/


















