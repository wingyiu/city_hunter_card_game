//
//  Layer_MessageList.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-7-24.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_MessageList_h
#define MidNightCity_Layer_MessageList_h

#include "KNScrollView.h"

// 公告信息结构
struct MESSAGE_INFO
{
    CCSprite* pBack;                        // 背景
    
    CCLabelTTF* pTitle;                     // 标题文本
    CCLabelTTF* pText;                      // 内容文本
    
    // 构造函数
    MESSAGE_INFO()                          {memset(this, 0, sizeof(MESSAGE_INFO));}
};

// 类自定义
class Layer_MessageList : public KNScrollView
{
public:
    #pragma mark - 构造函数
    Layer_MessageList();
    #pragma mark - 析构函数
    virtual ~Layer_MessageList();
    
    #pragma mark - 初始化函数 : 使用自动内存使用
    bool init();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 重写退出函数 : 使用自动内存使用
    virtual void onExit();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 刷新公告列表函数
    void RefreshMessageList();
    
    #pragma mark - 连接cocos初始化方法
    LAYER_NODE_FUNC(Layer_MessageList);
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
private:
    // 公告对象变量
    vector<MESSAGE_INFO> m_vecMessageList;
    
    // 公告属性变量
};

#endif
