//
//  Layer_FollowerList.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-4-28.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_FollowerList_h
#define MidNightCity_Layer_FollowerList_h

#include "KNScrollView.h"
#include "KNUIFunction.h"
#include "PlayerDataManage.h"
#include "Layer_GameGuide.h"
#include "Follower.h"

// 列表状态
enum FOLLOWER_LIST_STATE
{
    FOLLOWER_TEAM = 0,              // 队伍编辑
    PACKAGE_CHECK,                  // 英雄背包
    FOLLOWER_FIRE,                  // 英雄售卖
    FOLLOWER_TRAIN,                 // 小弟训练
    FOLLOWER_TEAM_COMPLETE,         // 队伍编辑完成
    
    FOLLOWER_NONE,                  // 没有任何类型
};

// 显示状态
enum LIMIT_TYPE
{
    LIMIT_ALL = 0,                  // 全部显示
    LIMIT_SWORD = SWORD,            // 刀
    LIMIT_GUN = GUN,                // 枪
    LIMIT_FIST = FIST,              // 拳
};

// 排序状态
enum SORT_TYPE
{
    SORT_STAR_GREAT = 0,            // 星级排序－升序
    SORT_STAR_LESS,                 // 星级排序－降序
    SORT_LEVEL_GREAT,               // 等级排序－升序
    SORT_LEVEL_LESS,                // 等级排序－降序
    SORT_ATTACK_GREAT,              // 攻击力排序－升序
    SORT_ATTACK_LESS,               // 攻击力排序－降序
    SORT_HP_GREAT,                  // hp排序－升序
    SORT_HP_LESS,                   // hp排序－降序
    SROT_RECOVER_GREAT,             // 回复力排序－升序
    SORT_RECOVER_LESS,              // 回复力排序－降序
};

// 小弟排序 - 星级
bool StarGreat(Follower* p1, Follower* p2);
bool StarLess(Follower* p1, Follower* p2);
// 小弟排序 - 等级
bool LevelGreat(Follower* p1, Follower* p2);
bool LevelLess(Follower* p1, Follower* p2);
// 小弟排序 - 攻击力
bool AttackGreat(Follower* p1, Follower* p2);
bool AttackLess(Follower* p1, Follower* p2);
// 小弟排序 - 血量
bool HPGreat(Follower* p1, Follower* p2);
bool HPLess(Follower* p1, Follower* p2);
// 小弟排序 - 回复力
bool RECOVERGreat(Follower* p1, Follower* p2);
bool RECOVERLess(Follower* p1, Follower* p2);

// 好友列表类定义
class Layer_FollowerList : public KNScrollView
{
public:
    #pragma mark - 构造函数
    Layer_FollowerList();
    #pragma mark - 析构函数
    virtual ~Layer_FollowerList();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 重写退出函数
    virtual void onExit();
    
    #pragma mark - 重写可见函数－打开可见并激活事件函数
    void setIsVisible(bool bIsVisible);
    
    #pragma mark - 刷新小弟列表
    void RefreshFollowerList();
    #pragma mark - 筛选小弟列表
    void ScreenFollowerList(LIMIT_TYPE eType);
    #pragma mark - 排序小弟列表
    void SortFollowerList(SORT_TYPE eType);
    
    #pragma mark - 刷新小弟队伍列表函数
    void RefreshFollowerTeamList();
    #pragma mark - 刷新小弟位置函数
    void RefreshFollowerPosition();
    
    #pragma mark - 设置列表状态函数
    void SetListState(FOLLOWER_LIST_STATE eState);
    #pragma mark - 获得列表状态函数
    FOLLOWER_LIST_STATE GetListState() const                {return m_eState;}
    
    #pragma mark - 设置编辑队员序号函数
    void SetEditMemberIndex(int iEditMemberIndex)           {m_iEditMemberIndex = iEditMemberIndex;}
    #pragma mark - 获得编辑队员序号函数
    int GetEditMemberIndex() const                          {return m_iEditMemberIndex;}
    
    #pragma mark - 获得被编辑的小弟函数
    Follower* GetEditedFollower() const                     {return m_pEditedFollower;}
    #pragma mark - 获得引导小弟函数
    Follower* GetGuideFollower() const                      {return m_pGuideFollower;}
    
    #pragma mark - 设置小弟成员函数
    void setFollowerMember(Follower* pFollower, int index);
    #pragma mark - 清空小弟队伍列表函数
    void clearFollowerTeam();
    #pragma mark - 获得小弟队伍列表函数
    vector<Follower*> GetFollowerTeamList() const           {return m_vecTeamList;}
    
    #pragma mark - 获得训练列表函数
    set<Follower*> GetTrainList() const                     {return m_setTrainList;}
    #pragma mark - 清空训练列表函数
    void clearTrainList();
    
    #pragma mark - 获得转职列表函数
    set<Follower*> GetJobList() const                       {return m_setJobList;}
    
    #pragma mark - 获得解雇列表函数
    set<Follower*> GetFireList() const                      {return m_setFireList;}
    #pragma mark - 清空解雇列表函数
    void clearFireList();
    
    #pragma mark - 获得小弟列表函数
    vector<Follower*> GetFollowerList() const               {return m_vecFollowerList;}
    
    #pragma mark - 设置小弟属性显示函数
    void SetFollowerAttrShowType(FOLLOWER_ATTR_SHOW_TYPE eType);
    
    #pragma mark - 显示小弟信息函数
    void ShowFollowerInfo(Follower* pFollower);
    #pragma mark - 显示小弟信息函数
    void ShowFollowerInfo(int iTeamIndex);
    #pragma mark - 显示小弟详细信息函数
    void ShowFollowerFullInfo(Follower* pFollower);
    #pragma mark - 显示小弟训练信息函数
    void ShowFollowerTrainInfo(Follower* pFollower, bool bIsRefreshSuccessData);
    #pragma mark - 显示小弟转职信息函数
    void ShowFollowerJobInfo(Follower* pFollower, bool bIsRefreshSuccessData);
    #pragma mark - 显示小弟队伍信息函数
    void ShowFollowerTeamInfo();
    
    #pragma mark - 处理确认按钮函数
    void ProcessConfirmEvent();
    #pragma mark - 处理重置按钮函数
    void ProcessClearEvent();
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(Layer_FollowerList);
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 滑动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
private:
    #pragma mark - 处理小弟事件
    void ProcessFollowerEvent(CCPoint point);
private:
    #pragma mark - 该小弟是否在战斗列表中函数
    bool IsFollowerInBattleList(Follower* pFollower);
    #pragma mark - 该小弟是否在训练列表中函数
    bool IsFollowerInTrainList(Follower* pFollower);
    #pragma mark - 该小弟是否在包裹列表中函数
    bool IsFollowerInPackList(int iFollowerID, Follower* pSelf);
    #pragma mark - 该小弟是否在解雇列表中函数
    bool IsFollowerInFireList(Follower* pFollower);
    
    #pragma mark - 刷新训练素材列表函数
    void RefreshTrainMaterialList();
    
    #pragma mark - 设置列表训练信息显示函数
    void SetListTrainInfoDisplay(bool bIsDisplay);
    #pragma mark - 设置列表解雇信息显示函数
    void SetListFireInfoDisplay(bool bIsDisplay);
    
    #pragma mark - 计算训练信息函数(这个函数灰主动刷新界面)
    void CaleTrainInfo();
    #pragma mark - 计算解雇信息函数(这个函数灰主动刷新界面)
    void CaleFireInfo();
private:
    // 小弟对象变量
    vector<Follower*> m_vecFollowerList;                    // 小弟列表，用于刷新列表使用
    vector<Follower*> m_vecTeamList;                        // 队伍列表
    
    set<Follower*> m_setTrainList;                          // 训练消耗列表
    set<Follower*> m_setJobList;                            // 转职消耗列表
    set<Follower*> m_setFireList;                           // 解雇小弟列表
    
    Follower* m_pEditedFollower;                            // 被点击的小弟
    Follower* m_pGuideFollower;                             // 引导小弟
    
    // 小弟属性变量
    FOLLOWER_LIST_STATE m_eState;                           // 列表状态
    LIMIT_TYPE m_eLimitType;                                // 当前筛选状态
    FOLLOWER_ATTR_SHOW_TYPE m_eFollowerAttr;                // 小弟属性显示状态
    
    int m_iEditMemberIndex;                                 // 队伍状态使用 : 被编辑的队员
    
    bool m_bIsButtonPressed;                                // 按钮是否被点击
};

#endif
