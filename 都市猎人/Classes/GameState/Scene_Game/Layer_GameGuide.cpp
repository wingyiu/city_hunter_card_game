//
//  Layer_GameGuide.cpp
//  MidNightCity
//
//  Created by 孙 惠伟 on 12-8-21.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_GameGuide.h"
#include "Layer_Block.h"
#include "Layer_Building.h"
#include "Layer_Display.h"
#include "Wrapper.h"

Layer_GameGuide* Layer_GameGuide::m_Instance = NULL;

// 连接方块数组
extern BLOCK_INFO m_arrBlockItem[VETICAL_BLOCK_NUM][HORIZON_BLOCK_NUM];

//SWORD = 1, FIST = 2, GUN = 3, ALL = 4


// 战斗步骤1,方块布局
int g_blockStep1[5][6] =
{
    4, 2, 1, 4, 3, 2,
    4, 2, 1, 4, 3, 2,
    2, 1, 4, 3, 2, 4,
    1, 4, 3, 2, 4, 3,
    1, 4, 3, 2, 4, 3,
};

int g_blockStep1Clear[5][6] =
{
    1, 4, 3, 2, 4, 3,
    1, 4, 3, 2, 4, 3,
    4, 3, 2, 4, 3, 1,
    1, 4, 3, 2, 4, 3,
    1, 4, 3, 2, 4, 3,
};

// 战斗步骤2,方块布局
int g_blockStep2[5][6] =
{
    //    2, 2, 4, 2, 4, 1,
    //    1, 4, 3, 2, 4, 1,
    //    2, 3, 4, 1, 2, 2,
    //    1, 1, 2, 2, 4, 3,
    //    2, 2, 1, 4, 3, 3,
    4, 4, 2, 4, 4, 1,
    4, 4, 1, 4, 4, 2,
    2, 2, 3, 2, 2, 1,
    1, 1, 4, 1, 1, 4,
    3, 3, 4, 3, 3, 1,
};

// 战斗步骤2,方块布局
int g_blockStep3[5][6] =
{
    //    2, 2, 4, 2, 4, 1,
    //    1, 4, 3, 2, 4, 1,
    //    2, 3, 4, 1, 2, 2,
    //    1, 1, 2, 2, 4, 3,
    //    2, 2, 1, 4, 3, 3,
    4, 4, 3, 4, 4, 1,
    4, 4, 2, 4, 4, 2,
    2, 2, 5, 2, 2, 1,
    1, 1, 2, 1, 1, 4,
    3, 3, 1, 3, 3, 1,
};


// 楼层提示文本
const char* g_pFloorHintMsg[] =
{
    "欢迎来到《都市猎人》!\n 1楼是系统楼层哦！记得常来查看公告内容哦！",
    "恭喜你，成功通关！\n刚才你在战斗中获得了强力英雄，快点让他加入你的队伍吧！",
    "3楼是任务楼层哦！\n在这里可以进行刺激的冒险！现在就让我们来体验一次吧！",
    "4楼是……………………………………(^O^)！",
    "5楼是^^^^^^^^^^^^^^……",
};

/************************************************************************/
#pragma mark - 静态函数 : 获得唯一对象实例函数
/************************************************************************/
Layer_GameGuide* Layer_GameGuide::GetSingle()
{
    return m_Instance;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类构造函数
/************************************************************************/
Layer_GameGuide::Layer_GameGuide()
{
    // 为新手引导对象变量赋值
    m_pGuideArrow = NULL;
    
    // 为新手引导属性变量赋值
    m_iBattleStep = 1;
    m_iFloorEnableIndex = 2;
    m_iFollowerGuideIndex = 1;
    m_iMissionGuideIndex = 1;
    m_iShopGuideIndex = 1;
    
    m_bIsSubGuide = false;
    m_bIsFloorFirstHint = false;
    m_bIsFinishMissionGuide = false;
    
    m_eDirection = ARROW_UP;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类析构函数
/************************************************************************/
Layer_GameGuide::~Layer_GameGuide()
{
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_GameGuide类初始化函数
/************************************************************************/
bool Layer_GameGuide::init()
{
    if (CCLayer::init() == false)
        return false;
    
    // 初始化新手引导箭头
    m_pGuideArrow = CCSprite::spriteWithSpriteFrameName("guide_arrow.png");
    m_pGuideArrow->setIsVisible(false);
    m_pGuideArrow -> retain();
    addChild(m_pGuideArrow);
    
    m_Instance = this;
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类退出函数
/************************************************************************/
void Layer_GameGuide::onExit()
{
    CCLayer::onExit();
    
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_GameGuide类销毁函数
/************************************************************************/
void Layer_GameGuide::Destroy()
{
    // 移出引导箭头
    removeChild(m_pGuideArrow, true);
}

/************************************************************************/
#pragma mark - Layer_GameGuide类显示提示箭头函数
/************************************************************************/
void Layer_GameGuide::ShowGuideArrow(CCPoint pos, ARROW_DIRECTION eDirection)
{
    m_pGuideArrow->setIsVisible(true);
    m_pGuideArrow->stopAllActions();
    m_pGuideArrow->setPosition(pos);
    
    m_eDirection = eDirection;
    
    switch (eDirection) {
        case ARROW_UP:          // 上
            m_pGuideArrow->setRotation(0.0f);
            break;
        case ARROW_DOWN:        // 下
            m_pGuideArrow->setRotation(180.0f);
            break;
        case ARROW_LEFT:        // 左
            m_pGuideArrow->setRotation(-90.0f);
            break;
        case ARROW_RIGHT:       // 右
            m_pGuideArrow->setRotation(90.0f);
            break;
        default:
            break;
    }
    
    ProcessGuideArrowAnim(m_pGuideArrow);
}

/************************************************************************/
#pragma mark - Layer_GameGuide类显示特殊显示箭头函数
/************************************************************************/
void Layer_GameGuide::ShowSpecialGuideArrow(CCPoint pos)
{
    m_pGuideArrow->setIsVisible(true);
    m_pGuideArrow->stopAllActions();
    m_pGuideArrow->setPosition(pos);
    
    m_pGuideArrow->setRotation(90.0f);
    
    ProcessSpecialGuideArrowAnim(m_pGuideArrow);
}

/************************************************************************/
#pragma mark - Layer_GameGuide类显示楼层提示函数
/************************************************************************/
void Layer_GameGuide::ShowFloorHit()
{
    return;
    if (PlayerDataManage::m_GuideStepMark == 1)
    {
        KNUIFunction::GetSingle()->OpenMessageBoxFrame(g_pFloorHintMsg[m_iFloorEnableIndex - 1]);
    }
    else if (PlayerDataManage::m_GuideStepMark == 2)
    {
        KNUIFunction::GetSingle()->OpenFrameByJumpType(FOLLOWER_CENTER_FRAME_ID);
        
        m_iFollowerGuideIndex = 2;
        RunFollowerGuideStep();
    }
    else if (PlayerDataManage::m_GuideStepMark == 3)
        KNUIFunction::GetSingle()->OpenMessageBoxFrame(g_pFloorHintMsg[m_iFloorEnableIndex - 1]);
    
    m_bIsFloorFirstHint = false;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类显示方块点击区域函数
/************************************************************************/
bool Layer_GameGuide::LimitBlockTouchRange(CCPoint point)
{
    CCPoint blockPos(0.0f, 0.0f);
    CCSize blockSize(0.0f, 0.0f);
    
    if (m_iBattleStep >= 3)
    {
        for (int i = 0; i < VETICAL_BLOCK_NUM; ++i)
        {
            // 获得精灵位置
            blockPos = m_arrBlockItem[i][2].pBlock->getPosition();
            
            // 获得精灵尺寸
            blockSize = m_arrBlockItem[i][2].pBlock->getContentSize();
            
            // 是否被点击
            if (point.x > blockPos.x - blockSize.width / 2.0f && point.x < blockPos.x + blockSize.width / 2.0f &&
                point.y > blockPos.y - blockSize.height / 2.0f && point.y < blockPos.y + blockSize.height / 2.0f)
            {
                return true;
            }
        }
    }
    else
    {
        for (int i = 0; i < HORIZON_BLOCK_NUM; ++i)
        {
            // 获得精灵位置
            blockPos = m_arrBlockItem[2][i].pBlock->getPosition();
            
            // 获得精灵尺寸
            blockSize = m_arrBlockItem[2][i].pBlock->getContentSize();
            
            // 是否被点击
            if (point.x > blockPos.x - blockSize.width / 2.0f && point.x < blockPos.x + blockSize.width / 2.0f &&
                point.y > blockPos.y - blockSize.height / 2.0f && point.y < blockPos.y + blockSize.height / 2.0f)
            {
                return true;
            }
        }
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类战斗步骤1函数
/************************************************************************/
void Layer_GameGuide::RunBattleGuideStep1()
{
    // 显示拖动列
    for (int i = 0; i < HORIZON_BLOCK_NUM; i++)
    {
        m_arrBlockItem[2][i].pBlock->setVertexZ(2.5f);
        m_arrBlockItem[2][i].pCube->setVertexZ(2.5f);
        m_arrBlockItem[2][i].pFrame->setVertexZ(2.5f);
    }
    
    // 显示提示箭头
    ShowSpecialGuideArrow(m_arrBlockItem[2][0].pBlock->getPosition());
    
    // 特殊处理，新手引导打开遮罩
    Layer_Block::ShareInstance()->SetLayerAlpha(true);
    
    // 记录引导步数
    m_iBattleStep = 1;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类完成战斗步骤1函数
/************************************************************************/
void Layer_GameGuide::FinishBattleGuideStep1()
{
    // 显示拖动列
    for (int i = 0; i < HORIZON_BLOCK_NUM; i++)
    {
        m_arrBlockItem[2][i].pBlock->setVertexZ(0.0f);
        m_arrBlockItem[2][i].pCube->setVertexZ(0.0f);
        m_arrBlockItem[2][i].pFrame->setVertexZ(0.0f);
    }
    
    // 关闭提示箭头
    m_pGuideArrow->stopAllActions();
    m_pGuideArrow->setIsVisible(false);
    
    m_iBattleStep = 2;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类战斗步骤2函数
/************************************************************************/
void Layer_GameGuide::RunBattleGuideStep2()
{
    // 显示拖动列
    for (int i = 0; i < HORIZON_BLOCK_NUM; i++)
    {
        m_arrBlockItem[2][i].pBlock->setVertexZ(2.5f);
        m_arrBlockItem[2][i].pCube->setVertexZ(2.5f);
        m_arrBlockItem[2][i].pFrame->setVertexZ(2.5f);
    }
    
    // 显示提示箭头
    ShowSpecialGuideArrow(m_arrBlockItem[2][0].pBlock->getPosition());
}

/************************************************************************/
#pragma mark - Layer_GameGuide类完成战斗步骤2函数
/************************************************************************/
void Layer_GameGuide::FinishBattleGuideStep2()
{
    // 显示拖动列
    for (int i = 0; i < HORIZON_BLOCK_NUM; i++)
    {
        m_arrBlockItem[2][i].pBlock->setVertexZ(0.0f);
        m_arrBlockItem[2][i].pCube->setVertexZ(0.0f);
        m_arrBlockItem[2][i].pFrame->setVertexZ(0.0f);
    }
    
    // 关闭提示箭头
    m_pGuideArrow->stopAllActions();
    m_pGuideArrow->setIsVisible(false);
    
    m_iBattleStep = 3;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类战斗步骤3函数
/************************************************************************/
void Layer_GameGuide::RunBattleGuideStep3()
{
    // 显示拖动列
    for (int i = 0; i < VETICAL_BLOCK_NUM; i++)
    {
        m_arrBlockItem[i][2].pBlock->setVertexZ(2.5f);
        m_arrBlockItem[i][2].pCube->setVertexZ(2.5f);
        m_arrBlockItem[i][2].pFrame->setVertexZ(2.5f);
    }
    
    // 显示提示箭头
    //ShowSpecialGuideArrow(m_arrBlockItem[2][0].pBlock->getPosition());
    ShowGuideArrow(m_arrBlockItem[2][2].pBlock->getPosition(), ARROW_UP);
}

/************************************************************************/
#pragma mark - Layer_GameGuide类完成战斗步骤3函数
/************************************************************************/
void Layer_GameGuide::FinishBattleGuideStep3()
{
    // 显示拖动列
    for (int i = 0; i < VETICAL_BLOCK_NUM; i++)
    {
        m_arrBlockItem[i][2].pBlock->setVertexZ(0.0f);
        m_arrBlockItem[i][2].pCube->setVertexZ(0.0f);
        m_arrBlockItem[i][2].pFrame->setVertexZ(0.0f);
    }
    
    // 关闭提示箭头
    m_pGuideArrow->stopAllActions();
    m_pGuideArrow->setIsVisible(false);
    
    m_iBattleStep = 4;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类战斗步骤4函数
/************************************************************************/
void Layer_GameGuide::RunBattleGuideStep4()
{
    // 显示拖动列
    for (int i = 0; i < HORIZON_BLOCK_NUM; i++)
    {
        m_arrBlockItem[2][i].pBlock->setVertexZ(2.5f);
        m_arrBlockItem[2][i].pCube->setVertexZ(2.5f);
        m_arrBlockItem[2][i].pFrame->setVertexZ(2.5f);
    }
    
    // 显示提示箭头
    //ShowSpecialGuideArrow(m_arrBlockItem[2][0].pBlock->getPosition());
    ShowGuideArrow(m_arrBlockItem[2][2].pBlock->getPosition(), ARROW_UP);
}

/************************************************************************/
#pragma mark - Layer_GameGuide类完成战斗步骤4函数
/************************************************************************/
void Layer_GameGuide::FinishBattleGuideStep4()
{
    // 显示拖动列
    for (int i = 0; i < HORIZON_BLOCK_NUM; i++)
    {
        m_arrBlockItem[2][i].pBlock->setVertexZ(0.0f);
        m_arrBlockItem[2][i].pCube->setVertexZ(0.0f);
        m_arrBlockItem[2][i].pFrame->setVertexZ(0.0f);
    }
    
    // 关闭提示箭头
    m_pGuideArrow->stopAllActions();
    m_pGuideArrow->setIsVisible(false);
    
    m_iBattleStep = 5;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类战斗步骤5函数
/************************************************************************/
void Layer_GameGuide::RunBattleGuideStep5()
{
    this -> removeChild(m_pGuideArrow, true);
    Layer_Display::ShareInstance() -> addChild(m_pGuideArrow);
    
    // 指向第一个小弟
    ShowGuideArrow(ccp(55.0f, 260.0f), ARROW_UP);
}

/************************************************************************/
#pragma mark - Layer_GameGuide类完成战斗步骤5函数
/************************************************************************/
void Layer_GameGuide::FinishBattleGuideStep5()
{
    m_pGuideArrow->stopAllActions();
    m_pGuideArrow->setIsVisible(false);
}

/************************************************************************/
#pragma mark - Layer_GameGuide类战斗步骤6函数
/************************************************************************/
void Layer_GameGuide::RunBattleGuideStep6()
{
    // 指向第一个小弟
    ShowGuideArrow(ccp(94.0f, 95.0f), ARROW_UP);
}

/************************************************************************/
#pragma mark - Layer_GameGuide类完成战斗步骤6函数
/************************************************************************/
void Layer_GameGuide::FinishBattleGuideStep6()
{
    m_pGuideArrow->stopAllActions();
    m_pGuideArrow->setIsVisible(false);
    m_pGuideArrow -> removeFromParentAndCleanup(true);
    m_pGuideArrow -> release();
}

/************************************************************************/
#pragma mark - Layer_GameGuide类楼层步骤1函数
/************************************************************************/
void Layer_GameGuide::RunGameGuideStep()
{
    return;
    // 使处于引导的楼层高亮
    //Layer_Building::ShareInstance()->AdjustFloorMaskAndArrowPosition(static_cast<FLOORTYPE>(m_iFloorEnableIndex));
    
    // 打开第一次点击提示
    m_bIsFloorFirstHint = true;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类完成当前步骤并更新下一步骤函数
/************************************************************************/
void Layer_GameGuide::FinishCurrentStepAndUpdateNext()
{
    return;
    // 超过站台层，结束新手引导
    if (m_iFloorEnableIndex >= F_Mission)
    {
        KNUIManager::GetManager()->CloseAllGuideArrow();
        //Layer_Building::ShareInstance()->closeAllGuide();
        //PlayerDataManage::m_GuideMark = false;
        
        KNUIFunction::GetSingle()->OpenMessageBoxFrame("恭喜你，完成了新手训练，快来体验下吧!");
        return;
    }
    
    // 进行下一个楼层
    m_iFloorEnableIndex += 1;
    
    // 进行引导
    RunGameGuideStep();
}

/************************************************************************/
#pragma mark - Layer_GameGuide类小弟步骤1函数
/************************************************************************/
void Layer_GameGuide::RunFollowerGuideStep()
{
    return;
    // 添加箭头
    CCSprite* pArrow = NULL;
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
    if (pFrame != NULL)
    {
        CCNode* pNode = pFrame->getChildByTag(8888);
        if (pNode == NULL)
        {
            CCSprite* pSprite = CCSprite::spriteWithSpriteFrameName("guide_arrow.png");
            pSprite->setRotation(90.0f);
            // 设置箭头位置
            if (PlayerDataManage::m_GuideStepMark == 1)
                pSprite->setPosition(ccp(-100.0f, 100.0f));
            else if (PlayerDataManage::m_GuideStepMark == 2)
                pSprite->setPosition(ccp(-100.0f, 55.0f));
            
            pSprite->setOpacity(254);
            pFrame->addChild(pSprite, 0, 8888);
            ProcessGuideArrowAnim(pSprite);
            
            pArrow = pSprite;
        }
        else
        {
            pArrow = dynamic_cast<CCSprite*>(pNode);
        }
    }
    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        CCNode* pNode = pFrame->getChildByTag(8888);
        if (pNode == NULL)
        {
            CCSprite* pSprite = CCSprite::spriteWithSpriteFrameName("guide_arrow.png");
            // 设置箭头位置
            pSprite->setPosition(ccp(85.0f, -135.0f));
            pFrame->addChild(pSprite, 0, 8888);
            ProcessGuideArrowAnim(pSprite);
        }
    }
    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        CCNode* pNode = pFrame->getChildByTag(8888);
        if (pNode == NULL)
        {
            CCSprite* pSprite = CCSprite::spriteWithSpriteFrameName("guide_arrow.png");
            // 设置箭头位置
            pSprite->setPosition(ccp(-85.0f, -198.0f));
            pFrame->addChild(pSprite, 0, 8888);
            ProcessGuideArrowAnim(pSprite);
        }
    }
    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        CCNode* pNode = pFrame->getChildByTag(8888);
        if (pNode == NULL)
        {
            CCSprite* pSprite = CCSprite::spriteWithSpriteFrameName("guide_arrow.png");
            // 设置箭头位置
            pSprite->setPosition(ccp(80.0f, -60.0f));
            pSprite->setIsVisible(false);
            pFrame->addChild(pSprite, 0, 8888);
            ProcessGuideArrowAnim(pSprite);
        }
    }
    
    if (m_iFollowerGuideIndex == 1)
    {
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TEAM_FRAME_ID);
        if (pFrame != NULL)
        {
            CCSprite* pSprite = CCSprite::spriteWithSpriteFrameName("guide_arrow.png");
            if (pSprite != NULL)
            {
                pSprite->setPosition(ccp(100.0f, 50.0f));
                pFrame->addChild(pSprite, 0, 8888);
                ProcessGuideArrowAnim(pSprite);
            }
        }
        
        // 设置箭头位置
        pArrow->setPosition(ccp(-100.0f, 100.0f));
    }
    else if (m_iFollowerGuideIndex == 2)
    {
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TEAM_FRAME_ID);
        if (pFrame != NULL)
        {
            CCSprite* pSprite = CCSprite::spriteWithSpriteFrameName("guide_arrow.png");
            if (pSprite != NULL)
            {
                pSprite->setPosition(ccp(100.0f, 50.0f));
                pFrame->addChild(pSprite, 0, 8888);
                ProcessGuideArrowAnim(pSprite);
            }
        }
        
        // 设置箭头位置
        pArrow->setPosition(ccp(-100.0f, 100.0f));
    }
}

/************************************************************************/
#pragma mark - Layer_GameGuide类完成当前步骤并更新下一步骤函数
/************************************************************************/
void Layer_GameGuide::FinishFollowerCurrentStep()
{
    return;
    m_iFollowerGuideIndex += 1;
    
    CCSprite* pArrow = NULL;
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
    if (pFrame != NULL)
    {
        CCNode* pNode = pFrame->getChildByTag(8888);
        if (pNode != NULL)
            pArrow = dynamic_cast<CCSprite*>(pNode);
    }
    
    if (m_iFollowerGuideIndex == 2)
    {
        // 设置箭头位置
        pArrow->stopAllActions();
        pArrow->setPosition(ccp(-100.0f, 55.0f));
        ProcessGuideArrowAnim(pArrow);
    }
    else if (m_iFollowerGuideIndex == 3)
    {
        // 设置箭头位置
        pArrow->setOpacity(255);
        pArrow->stopAllActions();
        pArrow->setRotation(0.0f);
        pArrow->setPosition(ccp(0.0f, -190.0f));
        ProcessGuideArrowAnim(pArrow);
    }
    //    else if (m_iFollowerGuideIndex == 4)
    //    {
    //        // 设置箭头位置
    //        pArrow->setPosition(ccp(-100.0f, -35.0f));
    //    }
    //    else if (m_iFollowerGuideIndex == 5)
    //    {
    //        // 设置箭头位置
    //        pArrow->setPosition(ccp(-100.0f, -80.0f));
    //    }
    
    // 记录新手指引
    PlayerDataManage::m_GuideStepMark++;
    SaveGameGuideInfo(PlayerDataManage::m_GuideStepMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
    ServerDataManage::ShareInstance()->RequestGuideDoneReqest(PlayerDataManage::m_GuideStepMark);
}

/************************************************************************/
#pragma mark - Layer_GameGuide类任务关卡步骤1函数
/************************************************************************/
void Layer_GameGuide::RunMissionGuideStep()
{
    return;
    //    // 第一步，初始化指引箭头
    //    if (m_iMissionGuideIndex == 1)
    //    {
    //        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
    //        if (pFrame != NULL)
    //        {
    //            Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
    //            if (pList != NULL)
    //                pList->SetGuideArrowPosition();
    //        }
    //    }
}

/************************************************************************/
#pragma mark - Layer_GameGuide类完成当前步骤并更新下一步骤函数
/************************************************************************/
void Layer_GameGuide::FinishMissionCurrentStep()
{
    
}

/************************************************************************/
#pragma mark - Layer_GameGuide类商店步骤1函数
/************************************************************************/
void Layer_GameGuide::RunShopGuideStep()
{
    return;
    // 第一步，初始化指引箭头
    if (m_iShopGuideIndex == 1)
    {
        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_BUY_FRAME_ID);
        if (pFrame != NULL)
        {
            CCSprite* pSprite = CCSprite::spriteWithSpriteFrameName("guide_arrow.png");
            if (pSprite != NULL)
            {
                pSprite->setPosition(ccp(-90.0f, -110.0f));
                pFrame->addChild(pSprite);
                ProcessGuideArrowAnim(pSprite);
            }
        }
    }
}

/************************************************************************/
#pragma mark - Layer_GameGuide类完成当前步骤并更新下一步骤函数
/************************************************************************/
void Layer_GameGuide::FinishShopCurrentStep()
{
    m_iShopGuideIndex = 2;
}

/************************************************************************/
#pragma mark - Layer_GameGuide类回调函数 : 处理箭头动画函数
/************************************************************************/
void Layer_GameGuide::ProcessGuideArrowAnim(CCObject* pObject)
{
    // 获得箭头精灵
    CCSprite* pArrow = dynamic_cast<CCSprite*>(pObject);
    if (pArrow == NULL)
        return;
    
    // 获得位置
    CCPoint pos = pArrow->getPosition();
    
    if (pArrow->getOpacity() == 254)
    {
        CCMoveTo* pMoveTo1 = CCMoveTo::actionWithDuration(0.5f, ccp(pos.x - 5.0f, pos.y));
        CCMoveTo* pMoveTo2 = CCMoveTo::actionWithDuration(1.0f, ccp(pos.x + 10.0f, pos.y));
        CCMoveTo* pMoveTo3 = CCMoveTo::actionWithDuration(0.5f, pos);
        CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_GameGuide::ProcessGuideArrowAnim));
        if (pMoveTo1 != NULL && pMoveTo2 != NULL && pMoveTo3 != NULL && pFuncN != NULL)
            pArrow->runAction(CCSequence::actions(pMoveTo1, pMoveTo2, pMoveTo3, pFuncN, NULL));
        
        return;
    }
    
    switch (m_eDirection) {
        case ARROW_UP:          // 上
        case ARROW_DOWN:        // 下
        {
            CCMoveTo* pMoveTo1 = CCMoveTo::actionWithDuration(0.5f, ccp(pos.x, pos.y + 5.0f));
            CCMoveTo* pMoveTo2 = CCMoveTo::actionWithDuration(1.0f, ccp(pos.x, pos.y - 10.0f));
            CCMoveTo* pMoveTo3 = CCMoveTo::actionWithDuration(0.5f, pos);
            CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_GameGuide::ProcessGuideArrowAnim));
            if (pMoveTo1 != NULL && pMoveTo2 != NULL && pMoveTo3 != NULL && pFuncN != NULL)
                pArrow->runAction(CCSequence::actions(pMoveTo1, pMoveTo2, pMoveTo3, pFuncN, NULL));
        }
            break;
        case ARROW_LEFT:        // 左
        case ARROW_RIGHT:       // 右
        {
            CCMoveTo* pMoveTo1 = CCMoveTo::actionWithDuration(0.5f, ccp(pos.x - 5.0f, pos.y));
            CCMoveTo* pMoveTo2 = CCMoveTo::actionWithDuration(1.0f, ccp(pos.x + 10.0f, pos.y));
            CCMoveTo* pMoveTo3 = CCMoveTo::actionWithDuration(0.5f, pos);
            CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_GameGuide::ProcessGuideArrowAnim));
            if (pMoveTo1 != NULL && pMoveTo2 != NULL && pMoveTo3 != NULL && pFuncN != NULL)
                pArrow->runAction(CCSequence::actions(pMoveTo1, pMoveTo2, pMoveTo3, pFuncN, NULL));
        }
            break;
        default:
            break;
    }
}

/************************************************************************/
#pragma mark - Layer_GameGuide类回调函数 : 处理特殊箭头动画函数
/************************************************************************/
void Layer_GameGuide::ProcessSpecialGuideArrowAnim(CCObject* pObject)
{
    // 获得箭头精灵
    CCSprite* pArrow = dynamic_cast<CCSprite*>(pObject);
    if (pArrow == NULL)
        return;
    
    // 获得位置
    CCPoint pos = pArrow->getPosition();
    
    CCMoveTo* pMoveTo1 = CCMoveTo::actionWithDuration(1.0f, ccp(pos.x + 100.0f, pos.y));
    CCMoveTo* pMoveTo2 = CCMoveTo::actionWithDuration(0.1f, pos);
    CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_GameGuide::ProcessSpecialGuideArrowAnim));
    if (pMoveTo1 != NULL && pMoveTo2 != NULL && pFuncN != NULL)
        pArrow->runAction(CCSequence::actions(pMoveTo1, pMoveTo2, pFuncN, NULL));
}