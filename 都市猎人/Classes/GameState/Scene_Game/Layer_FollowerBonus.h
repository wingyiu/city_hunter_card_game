//
//  Layer_FollowerBonus.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-5-10.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//
/*
#ifndef MidNightCity_Layer_FollowerBonus_h
#define MidNightCity_Layer_FollowerBonus_h

#include "KNBaseUI.h"

// 卡片总数量
const int MAX_CARD_NUM = 8;

class Layer_FollowerBonus : public KNBaseUI
{
public:
    #pragma mark - 构造函数
    Layer_FollowerBonus();
    #pragma mark - 析构函数
    virtual ~Layer_FollowerBonus();
    
    #pragma mark - 初始化函数 : 使用自动内存使用
    bool init();
    
    #pragma mark - 重写退出函数 : 使用自动内存使用
    virtual void onExit();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 使用cocos2d初始化方式
    LAYER_NODE_FUNC(Layer_FollowerBonus);
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 滑动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
private:
    #pragma mark - 重写基类visit函数
    virtual void visit();
private:
    // 抽奖对象变量
    CCSprite* m_pCardSprite[MAX_CARD_NUM];
    
    // 抽奖属性变量
    int m_iSelectedIndex;
};

#endif
*/