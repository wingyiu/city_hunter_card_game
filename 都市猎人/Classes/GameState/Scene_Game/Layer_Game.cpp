//
//  Layer_Game.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-6-19.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_Game.h"
Layer_Game * Layer_Game::m_Instance = NULL;

// 小弟性格查询表，以后最好还是修改下配置表
const char* g_pCharacterName[] = {"深沉", "坚强", "稳健", "狡诈", "迟缓", "无畏", "平庸"};

/************************************************************************/
#pragma mark - Layer_Game类静态函数 : 获得唯一对象实例函数
/************************************************************************/
Layer_Game* Layer_Game::GetSingle()
{
    if(m_Instance)
        return m_Instance;
    return NULL;
}

/************************************************************************/
#pragma mark - Layer_Game类构造函数
/************************************************************************/
Layer_Game::Layer_Game()
{
    // 为游戏layer对象变量赋初值
    m_pBuildingLayer = NULL;
    
    m_pMenuBack = NULL;
    m_pExpProgressBack = NULL;
    m_pEnergyProgressBack = NULL;
    
    m_pSetButtonNormal = NULL;
    m_pSetButtonSelect = NULL;
    
    for (int i = 0; i < MENU_ICON_NUM; i++)
        m_pMenuItem[i] = NULL;
    
    m_pExpProgress = NULL;
    m_pEnergyProgress = NULL;
    
    m_pMoneyNum1 = NULL;
    m_pMoneyNum2 = NULL;
    
    m_pPlayerLevelNum = NULL;
    
    m_pMenuItemBack = NULL;
    for (int i = 0; i < ICON_NUM; i++)
        m_pMoneyIcon[i] = NULL;
    
    m_pTimeMask1 = NULL;
    m_pTimeMask2 = NULL;
    m_pTimeMaohao = NULL;
    
    m_pCurAction = NULL;
    m_pFulAction = NULL;
    m_pActionSlide = NULL;
    
    m_pFloorMask = NULL;
    
    // 为游戏layer属性变量赋初值
    m_iExpFinalValue = 100.0f;
    m_iActionFinalValue = 100.0f;
    
    m_iMoneyNum1 = 0;
    m_iMoneyNum2 = 0;
    
    for (int i = 0; i < MENU_ICON_NUM; i++)
        m_bIsMenuItemState[i] = false;
    
    m_bIsMoneyRefresh = false;
    
    m_Instance = this;
}

/************************************************************************/
#pragma mark - Layer_Game类析构函数
/************************************************************************/
Layer_Game::~Layer_Game()
{
    Destroy();
    printf("Layer_Game:: 释放完成\n");
}

/************************************************************************/
#pragma mark - Layer_Game类初始化函数
/************************************************************************/
bool Layer_Game::init()
{
    // 初始化基类
    if (CCLayer::init() == false)
        return false;
    
    // 播放背景音乐
    KNUIFunction::GetSingle()->PlayerBackMusic("tower.mp3");
    
    // 初始化背景图片
    CCLayerColor* pLayer = CCLayerColor::layerWithColor(ccc4(86, 216, 250, 255));
    if (pLayer != NULL)
    {
        pLayer->setVertexZ(-1.0f);
        addChild(pLayer);
    }
    
    // 初始化楼层图层
    m_pBuildingLayer = Layer_Building::node();
    if (m_pBuildingLayer != NULL)
    {
        m_pBuildingLayer->SetOffset(ccp(0.0f, 52.0f));
        m_pBuildingLayer->setVertexZ(-0.4f);
        m_pBuildingLayer->SetPoint(false);
        addChild(m_pBuildingLayer);
    }
    
    // 初始化菜单背景
    m_pMenuBack = CCSprite::spriteWithSpriteFrameName("main_menu.png");
    if (m_pMenuBack != NULL)
    {
        m_pMenuBack->setPosition(ccp(160.0f, 31.0f));
        addChild(m_pMenuBack);
    }
    
    // 初始化设置按钮
    m_pSetButtonNormal = CCSprite::spriteWithSpriteFrameName("set_button_normal.png");
    m_pSetButtonSelect = CCSprite::spriteWithSpriteFrameName("set_button_select.png");
    if (m_pSetButtonNormal != NULL && m_pSetButtonSelect != NULL)
    {
        m_pSetButtonNormal->setIsVisible(true);
        m_pSetButtonSelect->setIsVisible(false);
        
        m_pSetButtonNormal->setPosition(ccp(300.0f, 20.0f));
        m_pSetButtonSelect->setPosition(ccp(300.0f, 20.0f));
        
        addChild(m_pSetButtonNormal);
        addChild(m_pSetButtonSelect);
    }
    
    // 初始化菜单背景
    m_pMenuItemBack = CCSprite::spriteWithSpriteFrameName("menu_item_back.png");
    if (m_pMenuItemBack != NULL)
    {
        m_pMenuItemBack->cocos2d::CCNode::setAnchorPoint(ccp(0.0f, 0.0f));
        m_pMenuItemBack->setPosition(ccp(278.0f, 52.0f));
        addChild(m_pMenuItemBack);
    }
    
    // 初始化菜单选项
    for (int i = 0; i < MENU_ICON_NUM - 1; i++)
    {
        // 创建图层
        m_pMenuItem[i] = CCLayer::node();
        if (m_pMenuItem[i] != NULL)
        {
            m_pMenuItem[i]->setPosition(ccp(298.0f, 75.0f + i * 54.0f));
            m_pMenuItem[i]->setAnchorPoint(ccp(0.0f, 0.0f));
            addChild(m_pMenuItem[i]);
            
            // icon图表
            char szTemp[32] = {0};
            sprintf(szTemp, "menu_item_%d.png", i + 1);
            CCSprite* pIcon = CCSprite::spriteWithSpriteFrameName(szTemp);
            if (pIcon != NULL)
            {
                pIcon->setColor(ccc3(230, 230, 230));
                m_pMenuItem[i]->addChild(pIcon, 0, ICON_ID);
            }
            
            // 数字背景
            CCSprite* pNumBack = CCSprite::spriteWithSpriteFrameName("menu_num_back.png");
            if (pNumBack != NULL)
            {
                pNumBack->setPosition(ccp(5.0f, 10.0f));
                m_pMenuItem[i]->addChild(pNumBack, 0, NUM_BACK_ID);
            }
            
            // 数字
            CCLabelAtlas* pAtlas = CCLabelAtlas::labelWithString("5", "number_type_6-hd.png", 7, 10, '.');
            if (pAtlas != NULL)
            {
                pAtlas->setPosition(ccp(0.0f, 6.0f));
                m_pMenuItem[i]->addChild(pAtlas, 0, NUMBER_ID);
            }
            
            MenuAnim(pIcon);
        }
    }
    
    // 初始化第一个菜单
    m_pMenuItem[MENU_ICON_NUM - 1] = CCLayer::node();
    if (m_pMenuItem[MENU_ICON_NUM - 1] != NULL)
    {
        // 剑
        CCSprite* pSword = CCSprite::spriteWithSpriteFrameName("sword_1.png");
        if (pSword != NULL)
        {
            pSword->setPosition(ccp(0.0f, -5.0f));
            m_pMenuItem[MENU_ICON_NUM - 1]->addChild(pSword, 0, ICON_ID);
        }
        
        SwordAnim(pSword);
        
        // new
        CCSprite* pNew = CCSprite::spriteWithSpriteFrameName("sword_new_1.png");
        if (pNew != NULL)
        {
            pNew->setPosition(ccp(0.0f, 15.0f));
            m_pMenuItem[MENU_ICON_NUM - 1]->addChild(pNew, 0, NUMBER_ID);
        }
        
        NewAnim(pNew);
        
        m_pMenuItem[MENU_ICON_NUM - 1]->setPosition(ccp(300.0f, 75.0f + (MENU_ICON_NUM - 1) * 54.0f));
        m_pMenuItem[MENU_ICON_NUM - 1]->setAnchorPoint(ccp(0.0f, 0.0f));
        addChild(m_pMenuItem[MENU_ICON_NUM - 1]);
    }
    
    // 初始化经验进度条
    m_pExpProgress = KNProgress::node();
    if (m_pExpProgress != NULL)
    {
        UIInfo stInfo;
        
        memcpy(stInfo.szNormalImage, "exp_progress.png", 64);
        stInfo.point = ccp(72.0f, 34.0f);
        stInfo.fPercentage = KNUIFunction::GetSingle()->GetExpPercent();
        
        if (m_pExpProgress->init(stInfo) == true)
        {
            m_pExpProgress->SetVisibleRect(CCRectMake(10.0f, 0.0f, 320.0f, 480.0f));
            addChild(m_pExpProgress);
        }
    }
    // 初始化能量进度条
    m_pEnergyProgress = KNProgress::node();
    if (m_pEnergyProgress != NULL)
    {
        UIInfo stInfo;
        
        memcpy(stInfo.szNormalImage, "energy_progress.png", 64);
        stInfo.point = ccp(64.0f, 14.0f);
        stInfo.fPercentage = 1.0f * PlayerDataManage::m_PlayerActivity / PlayerDataManage::m_PlayerMaxActivity * 100.0f;
        
        if (m_pEnergyProgress->init(stInfo) == true)
        {
            m_pEnergyProgress->SetVisibleRect(CCRectMake(10.0f, 0.0f, 320.0f, 480.0f));
            addChild(m_pEnergyProgress);
        }
    }
    
    // 初始化经验进度条背景
    m_pExpProgressBack = CCSprite::spriteWithSpriteFrameName("exp_mark.png");
    if (m_pExpProgressBack != NULL)
    {
        m_pExpProgressBack->setPosition(ccp(24.0f, 35.0f));
        m_pExpProgressBack->setVertexZ(0.5f);
        addChild(m_pExpProgressBack);
    }
    // 初始化能量进度条背景
    m_pEnergyProgressBack = CCSprite::spriteWithSpriteFrameName("energy_mark.png");
    if (m_pEnergyProgressBack != NULL)
    {
        m_pEnergyProgressBack->setPosition(ccp(11.0f, 14.0f));
        m_pEnergyProgressBack->setVertexZ(0.5f);
        addChild(m_pEnergyProgressBack);
    }
    
    // 初始化金钱1数字
    char szTemp[32] = {0};
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerMoney);
    m_pMoneyNum1 = CCLabelAtlas::labelWithString(szTemp, "number_type_7-hd.png", 9, 15, '.');
    if (m_pMoneyNum1 != NULL)
    {
        m_iMoneyNum1 = PlayerDataManage::m_PlayerMoney;
        m_pMoneyNum1->setPosition(ccp(160.0f, 26.0f));
        addChild(m_pMoneyNum1);
    }
    
    // 初始化金钱2数字
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerRMB);
    m_pMoneyNum2 = CCLabelAtlas::labelWithString(szTemp, "number_type_11-hd.png", 9, 14, '.');
    if (m_pMoneyNum2 != NULL)
    {
        m_iMoneyNum2 = PlayerDataManage::m_PlayerRMB;
        m_pMoneyNum2->setPosition(ccp(220.0f, 5.0f));
        addChild(m_pMoneyNum2);
    }
    
    // 初始化玩家等级
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerLevel);
    m_pPlayerLevelNum = CCLabelAtlas::labelWithString(szTemp, "UINum-hd.png", 8, 14, '.');
    if (m_pPlayerLevelNum != NULL)
    {
        m_pPlayerLevelNum->setPosition(ccp(25.0f, 35.0f));
        m_pPlayerLevelNum->setVertexZ(0.5f);
        addChild(m_pPlayerLevelNum);
    }
    
    // 初始化金币
    for (int i = 0; i < ICON_NUM; i++)
    {
        m_pMoneyIcon[i] = CCSprite::spriteWithSpriteFrameName("money_icon.png");
        if (m_pMoneyIcon[i] != NULL)
        {
            m_pMoneyIcon[i]->setIsVisible(false);
            addChild(m_pMoneyIcon[i]);
        }
    }
    
    // 初始化金币遮罩
    m_pMoneyIconMask = CCSprite::spriteWithSpriteFrameName("money_icon_mask.png");
    if (m_pMoneyIconMask != NULL)
    {
        m_pMoneyIconMask->setScale(0.8f);
        m_pMoneyIconMask->setPosition(ccp(147.0f, 34.0f));
        m_pMoneyIconMask->setOpacity(0);
        addChild(m_pMoneyIconMask);
    }
    
    // 初始化time mark
    m_pTimeMask1 = CCLabelAtlas::labelWithString("10", "number_type_3-hd.png", 8, 14, '.');//CCSprite::spriteWithSpriteFrameName("mark-fen.png");
    m_pTimeMask1->setPosition(ccp(142.0f, 6.0f));
    addChild(m_pTimeMask1);
    
    m_pTimeMask2 = CCLabelAtlas::labelWithString("10", "number_type_3-hd.png", 8, 14, '.');//CCSprite::spriteWithSpriteFrameName("mark-fen.png");
    m_pTimeMask2->setPosition(ccp(162.0f, 6.0f));
    addChild(m_pTimeMask2);
    
    m_pTimeMaohao =  CCSprite::spriteWithSpriteFrameName("mark-maohao.png");
    m_pTimeMaohao->setPosition(ccp(160.0f, 13.0f));
    addChild(m_pTimeMaohao);
    
    // 初始化行动力数值
    m_pCurAction = CCLabelAtlas::labelWithString("0", "RoundNum-hd.png", 10, 15, '.');
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerActivity);
    m_pCurAction->setScale(0.8f);
    m_pCurAction->setPosition(ccp(61.0f, 2.5f));
    m_pCurAction->setString(szTemp);
    addChild(m_pCurAction);
    
    m_pFulAction = CCLabelAtlas::labelWithString("100", "RoundNum-hd.png", 10, 15, '.');
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerMaxActivity);
    m_pFulAction->setScale(0.8f);
    m_pFulAction->setPosition(ccp(84.0f, 2.5f));
    m_pFulAction->setString(szTemp);
    addChild(m_pFulAction);
    
    m_pActionSlide = CCSprite::spriteWithSpriteFrameName("action_slide.png");
    m_pActionSlide->setPosition(ccp(80.0f, 8.0f));
    addChild(m_pActionSlide);
    
    // 初始化楼层遮罩
    m_pFloorMask = CCLayerColor::layerWithColor(ccc4(0, 0, 0, 100));
    m_pFloorMask->setOpacity(0);
    m_pFloorMask->setVertexZ(0.8f);
    addChild(m_pFloorMask);
    
    // 注册行动力更新函数
    schedule(schedule_selector(Layer_Game::UpdateActionRecoverTime), 0.5f);
    
    // 注册事件
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    // 如果有邮件，打开邮件标记
    if (PlayerDataManage::ShareInstance()->GetMyMailList()->size() != 0)
        SetMenuItemState(1, true);
    
    // 更新小弟战斗列表
    KNUIFunction::GetSingle()->UpdateFollowerTeamList();
    // 更新好友数量
    KNUIFunction::GetSingle()->UpdatePlatformInfo();
    // 更新好友id
    KNUIFunction::GetSingle()->UpdatePlayerID();
    // 更新设置信息
    KNUIFunction::GetSingle()->UpdateSetInfo();
    // 更新英雄中心队伍信息
    KNUIFunction::GetSingle()->UpdateTeamInfo();
    
    // 更新菜单状态
    UpdateMenuItemState();
    
    // 新手引导不开启某些图标
    //PlayerDataManage::m_GuideMark = true;
    //PlayerDataManage::m_GuideStepMark = GUIDE_FIRST_FLOOR;
    if (PlayerDataManage::m_GuideMark)
    {
        GameGuide::GetSingle()->InitGuide();
    }
    else
    {
        // 更新任务楼层标记
        KNUIFunction::GetSingle()->UpdateMissionFloorHint();
        // 开启登陆签到界面
        KNUIFunction::GetSingle()->UpdateSignInfo();
        KNUIFunction::GetSingle()->OpenFrameByNormalType(SIGN_INFO_FRAME_ID);
    }
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_Game类销毁函数
/************************************************************************/
void Layer_Game::onExit()
{
    // 调用基类退出函数
    CCLayer::onExit();
    
    // 销毁所有对象
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_Game类销毁函数
/************************************************************************/
void Layer_Game::Destroy()
{
    // 移除游戏layer对象变量
    removeChild(m_pBuildingLayer, true);
    
    removeChild(m_pMenuBack, true);
    removeChild(m_pExpProgressBack, true);
    removeChild(m_pEnergyProgressBack, true);
    
    removeChild(m_pSetButtonNormal, true);
    removeChild(m_pSetButtonSelect, true);
    
    for (int i = 0; i < MENU_ICON_NUM; i++)
        removeChild(m_pMenuItem[i], true);
    
    removeChild(m_pExpProgress, true);
    removeChild(m_pEnergyProgress, true);
    
    removeChild(m_pMoneyNum1, true);
    removeChild(m_pMoneyNum2, true);
    
    removeChild(m_pPlayerLevelNum, true);
    
    for (int i = 0; i < ICON_NUM; i++)
        removeChild(m_pMoneyIcon[i], true);
    
    removeChild(m_pTimeMask1, true);
    removeChild(m_pTimeMask2, true);
    removeChild(m_pTimeMaohao, true);
    
    removeChild(m_pCurAction, true);
    removeChild(m_pFulAction, true);
    removeChild(m_pActionSlide, true);
    
    removeChild(m_pMenuItemBack, true);
    
    // 移除所有节点
    removeAllChildrenWithCleanup(true);
    
    // 移除事件处理
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
    
    // 移出行动力恢复函数
    unschedule(schedule_selector(Layer_Game::UpdateActionRecoverTime));
}

/************************************************************************/
#pragma mark - Layer_Game类处理金币动画函数
/************************************************************************/
void Layer_Game::ProcessCoinAnim(CCPoint pos)
{
    CCPoint point = pos;//convertTouchToNodeSpace(pTouch);
    
    // 设置金币位置并打开显示
    for (int i = 0; i < ICON_NUM; i++)
    {
        m_pMoneyIcon[i]->setPosition(ccp(point.x - 20.0f, point.y - 20.0f));
        m_pMoneyIcon[i]->setIsVisible(true);
        
        // 计算终点位置
        CCPoint pos(0.0f, 0.0f);
        
        pos.x = point.x + 20.0f * cos(360.0f / ICON_NUM * 6.28f / 360.0f * i);
        pos.y = point.y + 20.0f * sin(360.0f / ICON_NUM * 6.28f / 360.0f * i);
        
        CCMoveTo* pMoveTo1 = CCMoveTo::actionWithDuration(0.5f, pos);
        CCMoveTo* pMoveTo2 = CCMoveTo::actionWithDuration(0.5f, ccp(145.0f, 35.0f));
        CCScaleTo* pScalTo = CCScaleTo::actionWithDuration(0.5f, 0.6f);
        CCDelayTime* pTime = CCDelayTime::actionWithDuration(0.1f * i);
        CCCallFuncN* pFunc = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Game::ProcessCoinAdd));
        if (pTime != NULL && pScalTo != NULL && pMoveTo1 != NULL && pMoveTo2 != NULL && pFunc != NULL)
        {
            m_pMoneyIcon[i]->runAction(pScalTo);
            m_pMoneyIcon[i]->runAction(CCSequence::actions(pTime, pMoveTo2, pFunc, NULL));
        }
    }
    
    // 增加金钱数量
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(1.0f);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Game::UpdateMenuMoney));
    if (pTime != NULL && pFunc != NULL)
        runAction(CCSequence::actions(pTime, pFunc, NULL));
    
    // 刷新菜单按钮
    if (Layer_Building::ShareInstance()->RefreshHavestMenu(false) == false)
        SetMenuItemState(2, false);
}

/************************************************************************/
#pragma mark - Layer_Game类回调函数 : 处理金币增加函数
/************************************************************************/
void Layer_Game::ProcessCoinAdd(CCObject* pObject)
{
    // 关闭金币显示
    CCSprite* pSprite = dynamic_cast<CCSprite*>(pObject);
    if (pSprite != NULL)
        pSprite->setIsVisible(false);
    
    // 闪烁下金币
    CCFadeIn* pFadeIn = CCFadeIn::actionWithDuration(0.3f);
    CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(0.3f);
    if (pFadeIn != NULL && pFadeOut != NULL)
        m_pMoneyIconMask->runAction(CCSequence::actions(pFadeIn, pFadeOut, NULL));
}

/************************************************************************/
#pragma mark - Layer_Game类设置菜单显示函数
/************************************************************************/
void Layer_Game::SetMenuItemState(int index, bool bIsShow)
{
    if (index < 0 || index > MENU_ICON_NUM)
        return;
    
    m_bIsMenuItemState[index] = bIsShow;
    
    if (index == 2 && bIsShow)
    {
        char szTemp[32] = {0};
        sprintf(szTemp, "%d", Layer_Building::ShareInstance()->GetEnableHavestFloorNum());
        
        CCLabelAtlas* pNumber = dynamic_cast<CCLabelAtlas*>(m_pMenuItem[index]->getChildByTag(NUMBER_ID));
        if (pNumber != NULL)
            pNumber->setString(szTemp);
    }
    else if (index == 1 && bIsShow)
    {
        char szTemp[32] = {0};
        sprintf(szTemp, "%d", static_cast<int>(PlayerDataManage::ShareInstance()->GetMyMailList()->size()));
        
        CCLabelAtlas* pNumber = dynamic_cast<CCLabelAtlas*>(m_pMenuItem[index]->getChildByTag(NUMBER_ID));
        if (pNumber != NULL)
            pNumber->setString(szTemp);
        
        // 打开楼层提示
        KNUIFunction::GetSingle()->ShowFloorHint(F_Platform);
    }
    
    UpdateMenuItemState();
}

/************************************************************************/
#pragma mark - Layer_Game类更新菜单状态函数
/************************************************************************/
void Layer_Game::UpdateMenuItemState()
{
    // 更新菜单选项
    float fOpenNum = 0.0f;
    int index = 0;
    
    for (int i = 0; i < MENU_ICON_NUM; i++)
    {
        if (!m_bIsMenuItemState[i])
        {
            m_pMenuItem[i]->setIsVisible(false);
        }
        else
        {
            m_pMenuItem[i]->setPosition(ccp(298.0f, 75.0f + index++ * 54.0f));
            
            m_pMenuItem[i]->setIsVisible(true);
            
            fOpenNum += 1.0f;
        }
    }
    
    // 更新菜单背景
    float fScale = fOpenNum / MENU_ICON_NUM;// / m_pMenuItemBack->getScaleY();
    m_pMenuItemBack->setScaleY(fScale);
}

/************************************************************************/
#pragma mark - Layer_Game类更新金钱数量函数
/************************************************************************/
void Layer_Game::UpdateMenuMoney()
{
    char szTemp[32] = {0};
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerMoney);
    m_pMoneyNum1->setString(szTemp);
    
    return;
    if (m_bIsMoneyRefresh)
        return;
    
    m_bIsMoneyRefresh = true;
    
    if (m_iMoneyNum1 <= PlayerDataManage::m_PlayerMoney)
        PlayerMoneyAdd(NULL);
    else if (m_iMoneyNum1 > PlayerDataManage::m_PlayerMoney)
        PlayerMoneySub(NULL);
}

/************************************************************************/
#pragma mark - Layer_Game类更新主界面数据函数
/************************************************************************/
void Layer_Game::UpdateMainData()
{
    // 更新玩家等级
    char szTemp[32] = {0};
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerLevel);
    m_pPlayerLevelNum->setString(szTemp);
    
    // 更新进度条
    m_pExpProgress->SetPercent(KNUIFunction::GetSingle()->GetExpPercent());
    m_pEnergyProgress->SetPercent(1.0f * PlayerDataManage::m_PlayerActivity / PlayerDataManage::m_PlayerMaxActivity * 100.0f);
    
    // 刷新行动力数值
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerActivity);
    if (PlayerDataManage::m_PlayerActivity / 100 != 0)
        m_pCurAction->setPosition(ccp(52.0f, 2.5f));
    m_pCurAction->setString(szTemp);
    
    // 刷新行动力上限数值
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerMaxActivity);
    m_pFulAction->setString(szTemp);
    
    // 刷新金钱
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerMoney);
    m_pMoneyNum1->setString(szTemp);
    
    // 刷新钻石
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerRMB);
    m_pMoneyNum2->setString(szTemp);
}

/************************************************************************/
#pragma mark - Layer_Game类显示楼层遮罩函数
/************************************************************************/
void Layer_Game::ShowFloorMask()
{
    m_pFloorMask->setOpacity(100);
    m_pFloorMask->setIsVisible(true);
}

/************************************************************************/
#pragma mark - Layer_Game类关闭楼层遮罩函数
/************************************************************************/
void Layer_Game::CloseFloorMask()
{
    m_pFloorMask->setOpacity(0);
    m_pFloorMask->setIsVisible(false);
}

/************************************************************************/
#pragma mark - Layer_Game类事件函数 : 点击
/************************************************************************/
bool Layer_Game::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    CCPoint pos(0.0f, 0.0f);
    CCSize size(0.0f, 0.0f);
    
    // 点击 : 设置按钮
    pos = m_pSetButtonNormal->getPosition();
    size = m_pSetButtonNormal->getContentSize();
    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
    {
        m_pSetButtonNormal->setIsVisible(false);
        m_pSetButtonSelect->setIsVisible(true);
        
        return true;
    }
    
    // 点击 : 菜单
    for (int i = 0; i < MENU_ICON_NUM; i++)
    {
        pos = m_pMenuItem[i]->getPosition();
        size = m_pMenuItem[i]->getChildByTag(ICON_ID)->getContentSize();
        
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
        {
            return true;
        }
    }
    
    // 点击 : 菜单栏
    pos = m_pMenuBack->getPosition();
    size = m_pMenuBack->getContentSize();
    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
    {
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_Game类事件函数 : 弹起
/************************************************************************/
void Layer_Game::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    CCPoint pos = m_pSetButtonNormal->getPosition();
    CCSize size = m_pSetButtonNormal->getContentSize();
    
    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f && m_pSetButtonSelect->getIsVisible() == true)
    {
        // 打开设置对话框
        CCPoint pos = pTouch->locationInView(pTouch->view());
        pos = CCDirector::sharedDirector()->convertToGL(pos);
        KNUIFunction::GetSingle()->OpenFrameByJumpTypeSpecial(1000, pos);
        
        // 打开楼层遮罩
        ShowFloorMask();
        
        // 播放音效
        KNUIFunction::GetSingle()->PlayerEffect("1.wav");
        
        return;
    }
    
    // 点击 : 菜单
    for (int i = 0; i < MENU_ICON_NUM; i++)
    {
        // 如果图标没亮，则跳过
        if (m_pMenuItem[i]->getIsVisible() == false)
            continue;
        
        // 取得按钮位置和大小
        pos = m_pMenuItem[i]->getPosition();
        size = m_pMenuItem[i]->getChildByTag(ICON_ID)->getContentSize();
        
        // 是否被点击
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
        {
            if (i == 1)                 // 邮件
            {
                // 打开界面
                KNUIFunction::GetSingle()->OpenFrameByJumpTypeSpecial(TRAIN_PLATFORM_FRAME_ID, ccp(160.0f, 240.0f));
                
                // 打开第二选项卡
                KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(TRAIN_PLATFORM_FRAME_ID);
                if (pFrame != NULL && pFrame->getIsVisible() == true)
                {
                    KNTabControl* pTabControl = dynamic_cast<KNTabControl*>(pFrame->GetSubUIByID(60000));
                    if (pTabControl != NULL)
                        pTabControl->SetTabEnable(1);
                }
                
                break;
            }
            else if (i == 2)            // 楼层收获
            {
                Layer_Building::ShareInstance()->RefreshHavestMenu(true);
                break;
            }
            else if (i == 3)
            {
                KNUIFunction::GetSingle()->OpenFrameByJumpTypeSpecial(MISSION_CENTER_FRAME_ID, ccp(160.0f, 240.0f));
                break;
            }
        }
    }
    
    // 还原按钮状态
    m_pSetButtonNormal->setIsVisible(true);
    m_pSetButtonSelect->setIsVisible(false);
    
    // 点击 : 菜单栏
    pos = m_pMenuBack->getPosition();
    size = m_pMenuBack->getContentSize();
    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
    {
        ShowPlayerInfoFrame();
        return;
    }
}

/************************************************************************/
#pragma mark - Layer_Game类回调函数 : 菜单动画函数
/************************************************************************/
void Layer_Game::MenuAnim(CCObject* pObject)
{
    CCSprite* pSprite = dynamic_cast<CCSprite*>(pObject);
    
    pSprite->setColor(ccc3(255, 255, 255));
    
    CCScaleTo* pScaleTo1 = CCScaleTo::actionWithDuration(0.4f, 1.3f);
    CCRotateTo* pRotateTo1 = CCRotateTo::actionWithDuration(0.4f, -30.0f);
    CCRotateTo* pRotateTo2 = CCRotateTo::actionWithDuration(0.4f, 0.0f);
    CCRotateTo* pRotateTo3 = CCRotateTo::actionWithDuration(0.4f, -30.0f);
    CCRotateTo* pRotateTo4 = CCRotateTo::actionWithDuration(0.4f, 0.0f);
    CCCallFuncN* pFuncN1 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Game::MenuColorChange));
    CCScaleTo* pScaleTo2 = CCScaleTo::actionWithDuration(0.4f, 1.0f);
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(1.0f);
    CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Game::MenuAnim));
    
    pSprite->runAction(pScaleTo1);
    pSprite->runAction(CCSequence::actions(pRotateTo1, pRotateTo2, pRotateTo3, pRotateTo4, pFuncN1, pScaleTo2, pTime, pFuncN, NULL));
}

/************************************************************************/
#pragma mark - Layer_Game类回调函数 : 颜色改变函数
/************************************************************************/
void Layer_Game::MenuColorChange(CCObject* pObject)
{
    CCSprite* pSprite = dynamic_cast<CCSprite*>(pObject);
    
    pSprite->setColor(ccc3(240, 240, 240));
}

/************************************************************************/
#pragma mark - Layer_Game类回调函数 : 剑动画函数
/************************************************************************/
void Layer_Game::SwordAnim(CCObject* pObject)
{
    CCSprite* pSprite = dynamic_cast<CCSprite*>(pObject);
    
    CCAnimation* pAnim = CreateAnimation("sword_", 0.05f);
    CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnim);
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(1.0f);
    CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Game::SwordAnim));
    
    pSprite->runAction(CCSequence::actions(pAnimate, pTime, pFuncN, NULL));
}

/************************************************************************/
#pragma mark - Layer_Game类回调函数 : new函数
/************************************************************************/
void Layer_Game::NewAnim(CCObject* pObject)
{
    CCSprite* pSprite = dynamic_cast<CCSprite*>(pObject);
    
    CCAnimation* pAnim = CreateAnimation("sword_new_", 0.05f);
    CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnim);
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(1.0f);
    CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Game::NewAnim));
    
    pSprite->runAction(CCSequence::actions(pAnimate, pTime, pFuncN, NULL));
}

/************************************************************************/
#pragma mark - Layer_Game类回调函数 : 金钱数量增加函数
/************************************************************************/
void Layer_Game::PlayerMoneyAdd(CCObject* pObject)
{
    if (m_iMoneyNum1 < PlayerDataManage::m_PlayerMoney)
    {
        char szTemp[32] = {0};
        
        m_iMoneyNum1 += 10.0f;
        
        sprintf(szTemp, "%d", m_iMoneyNum1);
        m_pMoneyNum1->setString(szTemp);
        
        CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Game::PlayerMoneyAdd));
        if (pFunc != NULL)
            runAction(pFunc);
    }
    else
    {
        m_iMoneyNum1 = PlayerDataManage::m_PlayerMoney;
        m_bIsMoneyRefresh = false;
    }
}

/************************************************************************/
#pragma mark - Layer_Game类回调函数 : 金钱数量减少函数
/************************************************************************/
void Layer_Game::PlayerMoneySub(CCObject* pObject)
{
    if (m_iMoneyNum1 > PlayerDataManage::m_PlayerMoney)
    {
        char szTemp[32] = {0};
        
        m_iMoneyNum1 -= 10.0f;
        
        sprintf(szTemp, "%d", m_iMoneyNum1);
        m_pMoneyNum1->setString(szTemp);
        
        CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Game::PlayerMoneySub));
        if (pFunc != NULL)
            runAction(pFunc);
    }
    else
    {
        m_iMoneyNum1 = PlayerDataManage::m_PlayerMoney;
        m_bIsMoneyRefresh = false;
    }
}

/************************************************************************/
#pragma mark - Layer_Game类更新回调函数 : 处理行动力时间函数
/************************************************************************/
void Layer_Game::UpdateActionRecoverTime(ccTime dt)
{
    int iHour = PlayerDataManage::ShareInstance()->m_PlayerActivityComeBackTime / 60 / 60;
    int iMinute = PlayerDataManage::ShareInstance()->m_PlayerActivityComeBackTime / 60 - iHour * 60;
    int iSecond = PlayerDataManage::ShareInstance()->m_PlayerActivityComeBackTime - iHour * 60 * 60 - iMinute * 60;
    
    char szMinute[16] = {0};
    char szSecond[16] = {0};
    
    if (iMinute / 10 == 0)
        sprintf(szMinute, "0%d", iMinute);
    else
        sprintf(szMinute, "%d", iMinute);
    
    if (iSecond / 10 == 0)
        sprintf(szSecond, "0%d", iSecond);
    else
        sprintf(szSecond, "%d", iSecond);
    
    m_pTimeMask1->setString(szMinute);
    m_pTimeMask2->setString(szSecond);
    
    // 时间到，刷新界面信息
    UpdateMainData();
    
    // 刷新玩家信息时间
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(PLAYERINFO_FRAME_ID);
    if (pFrame != NULL && pFrame->getIsVisible() == true)
    {
        // 当前行动力恢复时间
        int iHour = PlayerDataManage::ShareInstance()->m_PlayerActivityComeBackTime / 60 / 60;
        int iMinute = PlayerDataManage::ShareInstance()->m_PlayerActivityComeBackTime / 60 - iHour * 60;
        int iSecond = PlayerDataManage::ShareInstance()->m_PlayerActivityComeBackTime - iHour * 60 * 60 - iMinute * 60;
        
        char szHour[16] = {0};
        char szMinute[16] = {0};
        char szSecond[16] = {0};
        
        if (iHour / 10 == 0)
            sprintf(szHour, "0%d", iHour);
        else
            sprintf(szHour, "%d", iHour);
        
        if (iMinute / 10 == 0)
            sprintf(szMinute, "0%d", iMinute);
        else
            sprintf(szMinute, "%d", iMinute);
        
        if (iSecond / 10 == 0)
            sprintf(szSecond, "0%d", iSecond);
        else
            sprintf(szSecond, "%d", iSecond);
        
        // 时
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szHour);
        
        // 分
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20013));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szMinute);
        
        // 秒
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20014));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szSecond);
        
        // 全部行动力恢复时间
        int iDelta = PlayerDataManage::m_PlayerMaxActivity - PlayerDataManage::m_PlayerActivity;
        int iFullTime = PlayerDataManage::ShareInstance()->m_PlayerActivityComeBackTime + iDelta * 300;
        
        iHour = iFullTime / 60 / 60;
        iMinute = iFullTime / 60 - iHour * 60;
        iSecond = iFullTime - iHour * 60 * 60 - iMinute * 60;
        
        if (iHour / 10 == 0)
            sprintf(szHour, "0%d", iHour);
        else
            sprintf(szHour, "%d", iHour);
        
        if (iMinute / 10 == 0)
            sprintf(szMinute, "0%d", iMinute);
        else
            sprintf(szMinute, "%d", iMinute);
        
        if (iSecond / 10 == 0)
            sprintf(szSecond, "0%d", iSecond);
        else
            sprintf(szSecond, "%d", iSecond);
        
        // 时
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szHour);
        
        // 分
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20016));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szMinute);
        
        // 秒
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20017));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szSecond);
    }
}

/************************************************************************/
#pragma mark - Layer_Game类显示玩家信息框函数
/************************************************************************/
void Layer_Game::ShowPlayerInfoFrame()
{
//    <!-->玩家姓名<-->
//    <label id = 20002 x = -140 y = 105 font = "黑体" fontsize = 13 text = "上帝之手" r = 255 g = 255 b = 255></label>
//    
//    <!-->数字 : 等级<-->
//    <label id = 20003 x = 0 y = 82 normalimage = "number_type_9-hd.png" text = "10" parm1 = 9 parm2 = 15 parm3 = 100 type = 1></label>
//    <!-->数字 : 当前经验<-->
//    <label id = 20004 x = 0 y = 58 normalimage = "number_type_9-hd.png" text = "10" parm1 = 9 parm2 = 15 parm3 = 100 type = 1></label>
//    <!-->数字 : 升级经验<-->
//    <label id = 20005 x = 30 y = 58 normalimage = "number_type_9-hd.png" text = "10" parm1 = 9 parm2 = 15 parm3 = 100 type = 1></label>
//    <!-->数字 : 金币<-->
//    <label id = 20006 x = 0 y = 34 normalimage = "number_type_9-hd.png" text = "10" parm1 = 9 parm2 = 15 parm3 = 100 type = 1></label>
//    <!-->数字 : 钻石<-->
//    <label id = 20007 x = 0 y = 11 normalimage = "number_type_9-hd.png" text = "10" parm1 = 9 parm2 = 15 parm3 = 100 type = 1></label>
//    <!-->数字 : 好友数<-->
//    <label id = 20008 x = 0 y = -12 normalimage = "number_type_9-hd.png" text = "10" parm1 = 9 parm2 = 15 parm3 = 100 type = 1></label>
//    <!-->数字 : 友情点<-->
//    <label id = 20009 x = 0 y = -34 normalimage = "number_type_9-hd.png" text = "10" parm1 = 9 parm2 = 15 parm3 = 100 type = 1></label>
//    <!-->数字 : 当前行动力<-->
//    <label id = 20010 x = 0 y = -58 normalimage = "number_type_9-hd.png" text = "10" parm1 = 9 parm2 = 15 parm3 = 100 type = 1></label>
//    <!-->数字 : 行动力上限<-->
//    <label id = 20011 x = 30 y = -58 normalimage = "number_type_9-hd.png" text = "10" parm1 = 9 parm2 = 15 parm3 = 100 type = 1></label>
//    
//    <!-->数字 : 时 分 秒<-->
//    <label id = 20012 x = -20 y = -80 normalimage = "expNum-hd.png" text = "00" parm1 = 14 parm2 = 19 parm3 = 65 type = 1></label>
//    <label id = 20013 x = 4 y = -80 normalimage = "expNum-hd.png" text = "19" parm1 = 14 parm2 = 19 parm3 = 65 type = 1></label>
//    <label id = 20014 x = 30 y = -80 normalimage = "expNum-hd.png" text = "00" parm1 = 14 parm2 = 19 parm3 = 65 type = 1></label>
//    
//    <!-->数字 : 时 分 秒<-->
//    <label id = 20015 x = -20 y = -95 normalimage = "expNum-hd.png" text = "00" parm1 = 14 parm2 = 19 parm3 = 65 type = 1></label>
//    <label id = 20016 x = 4 y = -95 normalimage = "expNum-hd.png" text = "19" parm1 = 14 parm2 = 19 parm3 = 65 type = 1></label>
//    <label id = 20017 x = 30 y = -95 normalimage = "expNum-hd.png" text = "00" parm1 = 14 parm2 = 19 parm3 = 65 type = 1></label>
//    
//    <!-->冒号<-->
//    <label id = 20018 x = 2 y = -75 backimage = "mark-maohao.png"></label>
//    <label id = 20019 x = 26 y = -75 backimage = "mark-maohao.png"></label>
//    
//    <label id = 20020 x = 2 y = -90 backimage = "mark-maohao.png"></label>
//    <label id = 20021 x = 26 y = -90 backimage = "mark-maohao.png"></label>
//    
//    <!-->斜杠<-->
//    <label id = 20022 x = 23 y = 64 backimage = "action_slide.png"></label>
//    <label id = 20022 x = 23 y = 64 backimage = "action_slide.png"></label>
//    
//    <label id = 20023 x = 23 y = -52 backimage = "action_slide.png"></label>
//    <label id = 20024 x = 23 y = -52 backimage = "action_slide.png"></label>
    
    // 刷新信息
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(PLAYERINFO_FRAME_ID);
    if (pFrame != NULL)
    {
        // 临时文本
        char szTemp[32] = {0};
        
        // 玩家姓名
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->setLabelText(PlayerDataManage::ShareInstance()->m_PlayerName, NULL, 0);
        
        // 玩家等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 玩家当前经验
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20004));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerExperience);
            pLabel->GetNumberLabel()->setString(szTemp);
            
            // 获得位置
            CCPoint pos = pLabel->GetNumberLabel()->getPosition();
            CCSize size = pLabel->GetNumberLabel()->getContentSize();
            
            // 刷新斜杠位置
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20022));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setPosition(ccp(pos.x + size.width + 5.0f, pos.y + 6.0f));
            
            // 刷新下级经验
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
            if (pLabel != NULL)
                pLabel->GetNumberLabel()->setPosition(ccp(pos.x + size.width + 10.0f, pos.y));
        }
        
        // 玩家下级经验
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerLevelUpExperience);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 玩家金币
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerMoney);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 玩家钻石
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerRMB);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 玩家好友数量
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008));
        if (pLabel != NULL)
        {
            // 获得小弟数量
            int iFollowerNum = 0;
            PlayerFollowerDepot followerList = *PlayerDataManage::ShareInstance()->GetMyFollowerDepot();
            for (PlayerFollowerDepot::iterator iter = followerList.begin(); iter != followerList.end(); iter++)
            {
                list <Follower*>::iterator it2;
                for (it2 = iter->second.begin(); it2 != iter->second.end(); it2++)
                    ++iFollowerNum;
            }
            
            sprintf(szTemp, "%d", iFollowerNum);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 玩家友情点
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerFriendValue);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 玩家当前行动力
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20010));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerActivity);
            pLabel->GetNumberLabel()->setString(szTemp);
            
            // 获得位置
            CCPoint pos = pLabel->GetNumberLabel()->getPosition();
            CCSize size = pLabel->GetNumberLabel()->getContentSize();
            
            // 刷新斜杠位置
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setPosition(ccp(pos.x + size.width + 5.0f, pos.y + 6.0f));
            
            // 刷新下级经验
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011));
            if (pLabel != NULL)
                pLabel->GetNumberLabel()->setPosition(ccp(pos.x + size.width + 10.0f, pos.y));
        }
        
        // 玩家行动力上限
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerMaxActivity);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
    
    // 打开对话框
    KNUIFunction::GetSingle()->OpenFrameByJumpType(PLAYERINFO_FRAME_ID);
}