//
//  KNBuildingLayer.h
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-29.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#ifndef BuildingLayer_h
#define BuildingLayer_h

#include "KNScrollView.h"
#include "Floor.h"
#include "KNUIManager.h"
#include "KNUIFunction.h"
#include "ServerDataManage.h"
#include "PlayerDataManage.h"
#include "Layer_FollowerList.h"
#include "Layer_GameGuide.h"
#include "GameGuide.h"

#include  "MyConfigure.h"

// 最大楼层数量
const int MAX_FLOOR_NUM = 14;
// 最大云层数量
const int MAX_CLOUD_NUM = 1;

class Layer_Building : public KNScrollView
{
public:
    #pragma mark - 获得block实例对象
    static Layer_Building* ShareInstance();
    
    #pragma mark - 构造函数
    Layer_Building();
    #pragma mark - 析构函数
    virtual ~Layer_Building();
    
    #pragma mark - 初始化函数
    bool init();
    #pragma mark - 退出函数
    virtual void onExit();
    
    #pragma mark - 添加一个楼层到列表函数
    bool AddOneFloorToList(int iType, Floor* pFloorLayer);
    
    #pragma mark - 建造楼层
    void BuildingFloor();
    
    #pragma mark - 播放烟花
    void PlayerFrame();
    #pragma mark - 设置烟花标志
    void SetFrameEnable(bool bIsEnable)                                     {m_bIsFrameEnable = bIsEnable;}
    #pragma mark － 设置烟花播放的楼层类型函数
    void SetFrameType(int iType)                                            {m_iFrameFloorType = iType;}
    
    #pragma mark - 移动到当前层
    void MoveToCurrentFloor();
    
    #pragma mark - 设置楼层
    void LevelupFloor(int iType);
    
    #pragma mark - 设置扩展楼层信息函数
    void SetExtraFloorInfo(int iFloorType);
    
    #pragma mark - 关闭所有升级时间更新函数
    void CloseAllFloorTimeUpdate();
    
    #pragma mark - 获得需要升级的楼层id函数
    int GetLevelupFloorID();
    #pragma mark - 获得需要升级的楼层等级函数
    int GetLevelupFloorLevel();
    
    #pragma mark - 是否有楼层处于活动中函数
    bool IsFloorInAction();
    
    #pragma mark - 完成楼层升级函数
    void finishFloorHavest(int iType);
    
    #pragma mark - 更新菜单收获状态函数
    bool RefreshHavestMenu(bool bIsMoveTo);
    #pragma mark - 获得可收获的楼层数函数
    int GetEnableHavestFloorNum();
    
    #pragma mark - 获得楼层列表函数
    map<int, Floor*> GetFloorList()                                         {return m_FloorList;}
    
    #pragma mark - 获得楼层对象函数
    Floor* GetFloor(int iType);
    
    #pragma mark - 显示提示箭头函数
    void ShowGuideArrow(CCPoint pos, ARROW_DIRECTION eDirection);
    #pragma mark - 调整楼层遮罩和箭头位置函数
    void AdjustFloorMaskAndArrowPosition(FLOORTYPE eType);
    #pragma mark - 关闭新手引导图片函数
    void closeAllGuide();
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(Layer_Building);
protected:
    #pragma mark - 事件函数－点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数－弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数-滑动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
private:
    #pragma mark - 释放函数
    void Destroy();
    
    #pragma mark - 回调函数 : 云层移动
    void cloudMove(CCObject* pObject);
    
    #pragma mark - 回调函数 : 楼层更新
    void floorUpdate(ccTime dt);
    
    #pragma mark - 回调函数 : 处理箭头动画函数
    void ProcessGuideArrowAnim(CCObject* pObject);
private:
    static Layer_Building * m_Instance;
    
    // 楼层对象变量
    map<int, Floor*> m_FloorList;                                           // 楼层列表
    
    CCSprite* m_pBuildingBase;                                              // 底层
    CCSprite* m_pBuildingBack1;                                             // 背景1
    CCSprite* m_pBuildingBack2;                                             // 背景2
    
    CCSprite* m_pCloud[MAX_CLOUD_NUM];                                      // 云层
    
    CCSprite* m_pGuideArrow;                                                // 新手引导 : 引导箭头
    
    CCLayerColor* m_pFloorMaskUp;                                           // 新手引导 : 楼层上遮罩
    CCLayerColor* m_pFloorMaskDown;                                         // 新手引导 : 楼层下遮罩
    
    // 楼层属性变量
    float m_fBuildingHeight;                                                // 楼层总高度
    
    int m_iFloorType;                                                       // 楼层类型 : 同时表示楼层数量
    int m_iCurrentLevelFloorType;                                           // 当前升级楼层
    int m_iFrameFloorType;
    
    bool m_bIsFrameEnable;                                                  // 烟花开启标志
    
    ARROW_DIRECTION m_eDirection;                                           // 箭头方向枚举
};

#endif
