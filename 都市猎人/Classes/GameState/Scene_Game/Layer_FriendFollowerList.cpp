//
//  Layer_FriendFollowerList.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-5-15.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_FriendFollowerList.h"
#include "Scene_Battle.h"

// 小弟性格查询表，以后最好还是修改下配置表
extern const char* g_pCharacterName[];

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类构造函数
/************************************************************************/
Layer_FriendFollowerList::Layer_FriendFollowerList()
{
    // 为好友小弟对象变量赋初值
    m_vecFriendFollowerList.clear();
    
    // 为好友小弟属性变量赋初值
    m_ibSelectedIndex = 0;
    m_iSelectFriendID = 0;
    
    m_eFriendType = FRIEND_NONE;
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类析构函数
/************************************************************************/
Layer_FriendFollowerList::~Layer_FriendFollowerList()
{
    // 销毁所有对象
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类初始化函数
/************************************************************************/
bool Layer_FriendFollowerList::init(UIInfo& stInfo)
{
    // 初始化scrollview
    KNScrollView::init(stInfo);
    
    // 设置可视范围
    SetVisibleRange(CCRect(stInfo.point.x, stInfo.point.y, stInfo.size.width, stInfo.size.height));
    
    // 设置列表类型
    m_eFriendType = static_cast<FRIEND_LIST_TYPE>(stInfo.iType);
    
    // 根据类型刷新列表
    RefreshListData();
    
    // 设置列表属性
    KNScrollView::SetItemSize(CCSize(160.0f, 60.0f));
    KNScrollView::SetHorizontalSliderX(138.0f);
    KNScrollView::SetSliderEnable(true);
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类退出函数
/************************************************************************/
void Layer_FriendFollowerList::onExit()
{
    CCLayer::onExit();
    
    // 销毁所有对象
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类刷新列表函数
/************************************************************************/
void Layer_FriendFollowerList::RefreshListData()
{
    switch (m_eFriendType) {
        case FRIEND_FRIEND:
            RefreshFriendList();
            break;
        case FRIEND_FOLLOWER:
            RefreshFriendFollowerList();
            break;
        case FRIEND_STRANGER:
            RefreshStrangerList();
            break;
        default:
            break;
    }
    
    // 滑动到顶端
    KNScrollView::SetPoint(true);
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类销毁函数
/************************************************************************/
void Layer_FriendFollowerList::Destroy()
{
    // 移出所有小弟
    vector<FRIEND_ITEM_INFO>::iterator iter;
    for (iter = m_vecFriendFollowerList.begin(); iter != m_vecFriendFollowerList.end(); iter++)
    {
        removeChild(iter->pIcon, true);
        
        removeChild(iter->pFrame, true);
        
        removeChild(iter->pSelectButtonNormal, true);
        removeChild(iter->pSelectButtonSelect, true);
        
        removeChild(iter->pAddButtonNormal, true);
        removeChild(iter->pAddButtonSelect, true);
        
        removeChild(iter->pFollowerName, true);
        removeChild(iter->pFollowerLoginTime, true);
        
        removeChild(iter->pFollowerLevel, true);
        removeChild(iter->pFriendLevel, true);
        removeChild(iter->pFriendValue, true);
    }
    
    m_vecFriendFollowerList.clear();
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类显示好友小弟编辑界面
/************************************************************************/
void Layer_FriendFollowerList::showFriendFollowerEditFrame(FRIEND_ITEM_INFO& stInfo)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_FRIEND_EDIT_FRAME_ID);
    if (pFrame != NULL && pFrame->getIsVisible() == false)
    {
        // 打开界面
        pFrame->setIsVisible(true);
        
        // 小弟头像
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
            pLabel->resetLabelImage(stInfo.szSmallIconName);
        
        // 小弟边筐
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "info_frame_%d.png", stInfo.iProfression);
            pLabel->resetLabelImage(szTemp);
        }
        
        // 玩家姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
            pLabel->GetTextLabel()->setString(stInfo.szPlayerName);
        
        // 好友小弟等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", stInfo.iFollowerLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 好友等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", stInfo.iLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
    
    // 刷新确认对话框
    pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_REMOVE_CONFIRM_FRAME_ID);
    if (pFrame != NULL)
    {
        // 小弟头像
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
            pLabel->resetLabelImage(stInfo.szSmallIconName);
        
        // 小弟边筐
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "info_frame_%d.png", stInfo.iProfression);
            pLabel->resetLabelImage(szTemp);
        }
        
        // 玩家姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
            pLabel->GetTextLabel()->setString(stInfo.szPlayerName);
        
        // 好友小弟等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", stInfo.iFollowerLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 好友等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", stInfo.iLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类显示好友小弟信息界面
/************************************************************************/
void Layer_FriendFollowerList::showFriendFollowerInfoFrame(FRIEND_ITEM_INFO stInfo)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        char szTemp[32] = {0};
        
        // 刷新小弟头像
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->resetLabelImage(stInfo.szIconName);
        
        // 刷新小弟外框
//        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
//        if (pLabel != NULL)
//        {
//            sprintf(szTemp, "follower_bigFrame_%d.png", stInfo.iProfression);
//            pLabel->resetLabelImage(szTemp);
//        }
        
        // 刷新小弟等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", stInfo.iFollowerLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟生命值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", stInfo.iHp);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟攻击力
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", stInfo.iAttack);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟恢复值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", stInfo.iRecover);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟性格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009));
        if (pLabel != NULL)
        {
            int iCharacterID = stInfo.iCharacterID;
            const Character_Data* pData = SystemDataManage::ShareInstance()->GetData_ForCharacter(iCharacterID);
            if (pData != NULL)
            {
                // 查询性格序号
                int index = 0;
                for (int i = 0; i < 7; i++)
                {
                    if (strcmp(pData->Character_Name, g_pCharacterName[i]) == 0)
                    {
                        index = i + 1;
                        break;
                    }
                }
                
                // 生命文件名
                if (index <= 7)
                {
                    sprintf(szTemp, "character_%d.png", index);
                    pLabel->resetLabelImage(szTemp);
                }
            }
        }
        
        // 刷新星级
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
            if (pLabel != NULL)
            {
                if (i < stInfo.iFollowerQuality)
                    pLabel->setIsVisible(true);
                else
                    pLabel->setIsVisible(false);
            }
        }
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20022 + i));
            if (pLabel != NULL)
            {
                if (i < stInfo.iMaxQuality)
                    pLabel->setIsVisible(true);
                else
                    pLabel->setIsVisible(false);
            }
        }
        
        // 刷新小弟姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20021));
        if (pLabel != NULL)
        {
            const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(stInfo.iFollowerID);
            if (pData != NULL)
                pLabel->setLabelText(pData->Follower_Name, NULL, 0);
        }
    }
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类刷新好友数量和好友上限函数
/************************************************************************/
void Layer_FriendFollowerList::UpdateFriendValueAndLimit()
{
    // 刷新好友数量和好友上限
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_FRAME_ID);
    if (pFrame != NULL)
    {
        char szTemp[32] = {0};
        
        // 好友数量
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
        {
            if (m_vecFriendFollowerList.size() / 10 == 0)
                sprintf(szTemp, "0%d", static_cast<int>(m_vecFriendFollowerList.size()));
            else
                sprintf(szTemp, "%d", static_cast<int>(m_vecFriendFollowerList.size()));
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 好友上限
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d",  20 + PlayerDataManage::m_PlayerLevel * 2);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类事件函数 : 点击
/************************************************************************/
bool Layer_FriendFollowerList::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    
    CCRect visibleRect = GetVisibleRange();
    if (point.x > visibleRect.origin.x && point.x < visibleRect.origin.x + visibleRect.size.width &&
        point.y > visibleRect.origin.y && point.y < visibleRect.origin.y + visibleRect.size.height)
    {
        KNScrollView::ccTouchBegan(pTouch, pEvent);
        
        // 转换坐标
        point = convertToNodeSpace(point);
        
        // 遍历小弟列表，检测那个小弟被点击
        vector<FRIEND_ITEM_INFO>::iterator iter;
        
        CCPoint pos(0.0f, 0.0f);
        CCSize size(0.0f, 0.0f);
        for (iter = m_vecFriendFollowerList.begin(); iter != m_vecFriendFollowerList.end(); iter++)
        {
            if (m_eFriendType == FRIEND_FRIEND || m_eFriendType == FRIEND_FOLLOWER)
            {
                pos = iter->pSelectButtonNormal->getPosition();
                size = iter->pSelectButtonNormal->getContentSize();
                
                if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                    point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                {
                    // 变换按钮状态
                    iter->pSelectButtonNormal->setIsVisible(false);
                    iter->pSelectButtonSelect->setIsVisible(true);
                    
                    // 播放音效
                    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
                    
                    return true;
                }
            }
            else if (m_eFriendType == FRIEND_STRANGER)
            {
                pos = iter->pAddButtonNormal->getPosition();
                size = iter->pAddButtonNormal->getContentSize();
                
                if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                    point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                {
                    // 变换按钮状态
                    iter->pAddButtonNormal->setIsVisible(false);
                    iter->pAddButtonSelect->setIsVisible(true);
                    
                    // 播放音效
                    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
                    
                    return true;
                }
            }
        }
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类事件函数 : 弹起
/************************************************************************/
void Layer_FriendFollowerList::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    KNScrollView::ccTouchEnded(pTouch, pEvent);
    
    // 如果滑动中，则不响应点击事件
    if (GetIsMovingMark())
        return;
    
    // 获得点击坐标
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    // 遍历小弟列表，检测那个小弟被点击
    vector<FRIEND_ITEM_INFO>::iterator iter;
    
    CCPoint pos(0.0f, 0.0f);
    CCSize size(0.0f, 0.0f);
    for (iter = m_vecFriendFollowerList.begin(); iter != m_vecFriendFollowerList.end(); iter++)
    {
        if (m_eFriendType == FRIEND_FRIEND)
        {
            pos = iter->pSelectButtonNormal->getPosition();
            size = iter->pSelectButtonNormal->getContentSize();
            
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                if (!GetIsMovingMark())
                {
                    // 显示好友小弟编辑
                    showFriendFollowerEditFrame(*iter);

                    // 隐藏好友小弟列表
                    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(TRAIN_PLATFORM_FRAME_ID);
                    if (pFrame != NULL && pFrame->getIsVisible() == true)
                        pFrame->setIsVisible(false);
                    
                    // 记录小弟ID
                    m_iSelectFriendID = iter->iServerID;
                }
            }
            
            // 还原按钮状态
            iter->pSelectButtonNormal->setIsVisible(true);
            iter->pSelectButtonSelect->setIsVisible(false);
        }
        else if (m_eFriendType == FRIEND_FOLLOWER)
        {
            pos = iter->pSelectButtonNormal->getPosition();
            size = iter->pSelectButtonSelect->getContentSize();
            
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                if (!GetIsMovingMark())
                {
                    if (iter->iEnable == 1)
                    {
                        // 打开好友小弟对话框
                        KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FRIEND_FOLLOWER_INFO_FRAME_ID);
                        if (pFrame != NULL && pFrame->getIsVisible() == false)
                        {
                            pFrame->setIsVisible(true);
                            
                            // 刷新界面信息
                            showFriendFollowerInfoFrame(*iter); 
                        }
                        
                        // 记录选择好友的serverID
                        Scene_Battle::m_MissionFriendFollowerID = iter->iServerID;
                        
                        // 记录友情点
                        Scene_Battle::m_MissionFriendValue = iter->iFriendValue;
                        
                        // 新手引导 : 增加箭头
                        if (PlayerDataManage::m_GuideMark)
                            GameGuide::GetSingle()->FinishOneFloorGuide();
                    }
                    else
                    {
                        KNUIFunction::GetSingle()->OpenMessageBoxFrame("该小弟已使用过，不可在使用!");
                    }
                }
            }
            
            // 还原按钮状态
            iter->pSelectButtonNormal->setIsVisible(true);
            iter->pSelectButtonSelect->setIsVisible(false);
        }
        else if (m_eFriendType == FRIEND_STRANGER)
        {
            pos = iter->pAddButtonNormal->getPosition();
            size = iter->pAddButtonSelect->getContentSize();
            
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                if (!GetIsMovingMark())
                {
                    // 是否超过好友上限
                    int iLimit = 20 + PlayerDataManage::m_PlayerLevel * 2;
                    if (iLimit > 50)
                        iLimit = 50;
                    
                    map<int, list<FriendInfo> >::iterator it = ServerDataManage::ShareInstance()->m_FriendInfoList.find(0);
                    if (it != ServerDataManage::ShareInstance()->m_FriendInfoList.end())
                    {
                        if (it->second.size() > iLimit)
                        {
                            KNUIFunction::GetSingle()->OpenMessageBoxFrame("已达好友上限！");
                            return;
                        }
                    }
                    
                    // 记录已发送过的id
                    static int iLastID = 0;
                    if (iLastID == iter->iServerID)
                    {
                        KNUIFunction::GetSingle()->OpenMessageBoxFrame("请求已发送，请耐心等待！");
                    }
                    else
                    {
                        iLastID = iter->iServerID;
                        ServerDataManage::ShareInstance() -> RequestAddFriendByUserID(iter->iServerID);
                    }
                }
            }
            
            // 还原按钮状态
            iter->pAddButtonNormal->setIsVisible(true);
            iter->pAddButtonSelect->setIsVisible(false);
        }
    }
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类事件函数 : 滑动
/************************************************************************/
void Layer_FriendFollowerList::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
{
    KNScrollView::ccTouchMoved(pTouch, pEvent);
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类刷新小弟列表函数
/************************************************************************/
void Layer_FriendFollowerList::RefreshFriendList()
{
    // 移出所有小弟
    vector<FRIEND_ITEM_INFO>::iterator iter;
    for (iter = m_vecFriendFollowerList.begin(); iter != m_vecFriendFollowerList.end(); iter++)
    {
        removeChild(iter->pIcon, true);
        removeChild(iter->pFrame, true);
        removeChild(iter->pLvMark, true);
        removeChild(iter->pLvBack, true);
        
        removeChild(iter->pSelectButtonNormal, true);
        removeChild(iter->pSelectButtonSelect, true);
        
        removeChild(iter->pAddButtonNormal, true);
        removeChild(iter->pAddButtonSelect, true);
        
        removeChild(iter->pFollowerName, true);
        removeChild(iter->pFollowerLoginTime, true);
        
        removeChild(iter->pFollowerLevel, true);
        removeChild(iter->pFriendLevel, true);
        removeChild(iter->pFriendValue, true);
    }
    m_vecFriendFollowerList.clear();
    
    // 取得好友列表
    map<int, list<FriendInfo> >::iterator it = ServerDataManage::ShareInstance()->m_FriendInfoList.find(0);
    if (it == ServerDataManage::ShareInstance()->m_FriendInfoList.end())
        return;
    list<FriendInfo> friendList = it->second;
    
    // 恢复默认位置
    setPosition(0.0f, 0.0f);
    
    // 计算列表高度
    float height = 0.0f;
    list<FriendInfo>::iterator heightIter;
    for (heightIter = friendList.begin(); heightIter != friendList.end(); heightIter++)
    {
        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(heightIter->Fri_FollowerID);
        if (pData != NULL)
            height += 60.0f;
    }
    
    KNScrollView::SetScrollSize(ccp(320.0f, height));
    if (height < KNScrollView::GetScrollSize().y)
        height = KNScrollView::GetScrollSize().y;
    
    height -= 60.0f;
    
    // 刷新列表
    list<FriendInfo>::iterator friendIter;
    for (friendIter = friendList.begin(); friendIter != friendList.end(); friendIter++)
    {
        // 选项信息
        FRIEND_ITEM_INFO stInfo;
        
        // 获得好友小弟数据
        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(friendIter->Fri_FollowerID);
        if (pData == NULL)
        {
            printf("Layer_FriendFollowerList::RefreshFriendList error = %d\n", friendIter->Fri_FollowerID);
            continue;
        }
        
        // 接受数据
        stInfo.iServerID = friendIter->Fri_UserID;
        stInfo.iFollowerID = friendIter->Fri_FollowerID;
        stInfo.iFollowerQuality = friendIter->Fri_FollowerQuality;
        stInfo.iLevel = friendIter->Fri_UserLevel;
        stInfo.iFollowerLevel = friendIter->Fri_FollowerLevel;
        stInfo.iHp = friendIter->Fri_FollowerHp;
        stInfo.iAttack = friendIter->Fri_FollowerAttack;
        stInfo.iRecover = friendIter->Fri_FollowerReply;
        stInfo.iCharacterID = friendIter->Fri_FollowerCharacterID;
        stInfo.iEnable = friendIter->Fri_CanBeUse;
        stInfo.iProfression = pData->Follower_Profession;
        stInfo.iMaxQuality = pData->Follower_MaxQuality;
        
        memcpy(stInfo.szSmallIconName, pData->Follower_IconName, 32);
        memcpy(stInfo.szIconName, pData->Follower_FightIconName, 32);
        memcpy(stInfo.szPlayerName, friendIter->Fri_UserName, 32);
        
        // 更新icon - 小弟
        stInfo.pIcon = CCSprite::spriteWithSpriteFrameName(pData->Follower_IconName);
        if (stInfo.pIcon != NULL)
        {
            stInfo.pIcon->setPosition(ccp(-105.0f, height));
            addChild(stInfo.pIcon);
        }
        
        // 更新frame
        char szTemp[32] = {0};
        sprintf(szTemp, "info_frame_%d.png", pData->Follower_Profession);
        stInfo.pFrame = CCSprite::spriteWithSpriteFrameName(szTemp);
        if (stInfo.pFrame != NULL)
        {
            stInfo.pFrame->setPosition(ccp(0.0f, height));
            addChild(stInfo.pFrame);
        }
        
        // 更新lv标签
        stInfo.pLvMark = CCSprite::spriteWithSpriteFrameName("label_lv.png");
        if (stInfo.pLvMark != NULL)
        {
            stInfo.pLvMark->setPosition(ccp(-100.0f, height - 20.0f));
            addChild(stInfo.pLvMark);
        }
        
        // 更新lv背景
        stInfo.pLvBack = CCSprite::spriteWithSpriteFrameName("friend_level_mark.png");
        if (stInfo.pLvBack != NULL)
        {
            stInfo.pLvBack->setPosition(ccp(20.0f, height));
            addChild(stInfo.pLvBack);
        }
        
        // 更新选择按钮
        stInfo.pSelectButtonNormal = CCSprite::spriteWithSpriteFrameName("select_button_normal.png");
        if (stInfo.pSelectButtonNormal != NULL)
        {
            stInfo.pSelectButtonNormal->setPosition(ccp(100.0f, height));
            addChild(stInfo.pSelectButtonNormal);
        }
        stInfo.pSelectButtonSelect = CCSprite::spriteWithSpriteFrameName("select_button_select.png");
        if (stInfo.pSelectButtonSelect != NULL)
        {
            stInfo.pSelectButtonSelect->setPosition(ccp(100.0f, height));
            stInfo.pSelectButtonSelect->setIsVisible(false);
            addChild(stInfo.pSelectButtonSelect);
        }
        
        // 姓名
        stInfo.pFollowerName = CCLabelTTF::labelWithString(friendIter->Fri_UserName, "黑体", 13);
        if (stInfo.pFollowerName != NULL)
        {
            stInfo.pFollowerName->setPosition(ccp(-65.0f, height - 8.0f));
            stInfo.pFollowerName->setColor(ccc3(0, 0, 0));
            stInfo.pFollowerName->setAnchorPoint(ccp(0.0f, 0.0f));
            addChild(stInfo.pFollowerName);
        }
        
        // 更新好友小弟等级
        sprintf(szTemp, "%d", friendIter->Fri_FollowerLevel);
        stInfo.pFollowerLevel = CCLabelAtlas::labelWithString(szTemp, "RoundNum-hd.png", 10, 15, '.');
        if (stInfo.pFollowerLevel != NULL)
        {
            stInfo.pFollowerLevel->setPosition(ccp(-95.0f, height - 25.0f));
            stInfo.pFollowerLevel->setScale(0.7f);
            addChild(stInfo.pFollowerLevel);
        }
        
        // 更新好友等级
        sprintf(szTemp, "%d", friendIter->Fri_UserLevel);
        stInfo.pFriendLevel = CCLabelAtlas::labelWithString(szTemp, "UINum-hd.png", 8, 14, '.');
        if (stInfo.pFriendLevel != NULL)
        {
            stInfo.pFriendLevel->setPosition(ccp(25.0f, height - 8.0f));
            addChild(stInfo.pFriendLevel);
        }
        
        // 递减高度
        height -= 60.0f;
        
        // 添加到列表
        m_vecFriendFollowerList.push_back(stInfo);
    }
    
    UpdateFriendValueAndLimit();
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类刷新添加好友小弟列表函数
/************************************************************************/
void Layer_FriendFollowerList::RefreshFriendFollowerList()
{
    // 移出所有小弟
    vector<FRIEND_ITEM_INFO>::iterator iter;
    for (iter = m_vecFriendFollowerList.begin(); iter != m_vecFriendFollowerList.end(); iter++)
    {
        removeChild(iter->pIcon, true);
        removeChild(iter->pFrame, true);
        removeChild(iter->pLvMark, true);
        removeChild(iter->pLvBack, true);
        removeChild(iter->pRelate, true);
        removeChild(iter->pFriendBack, true);
        
        removeChild(iter->pSelectButtonNormal, true);
        removeChild(iter->pSelectButtonSelect, true);
        
        removeChild(iter->pAddButtonNormal, true);
        removeChild(iter->pAddButtonSelect, true);
        
        removeChild(iter->pFollowerName, true);
        removeChild(iter->pFollowerLoginTime, true);
        
        removeChild(iter->pFollowerLevel, true);
        removeChild(iter->pFriendLevel, true);
        removeChild(iter->pFriendValue, true);
    }
    m_vecFriendFollowerList.clear();
    
    // 恢复默认高度
    setPosition(ccp(0.0f, 0.0f));
    
    // 计算列表高度
    float height = 0.0f;
    
    // 好友，陌生人列表
    map<int, list<FriendInfo> > mapList = ServerDataManage::ShareInstance()->m_FriendInfoList;
    map<int, list<FriendInfo> >::iterator mapIter;
    
    list<FriendInfo> friendList;
    list<FriendInfo> strangeList;
    
    mapIter = mapList.find(0);
    if (mapIter != mapList.end())
        friendList = mapIter->second;
    
    mapIter = mapList.find(1);
    if (mapIter != mapList.end())
        strangeList = mapIter->second;
    
    // 计算高度
    list<FriendInfo>::iterator friendIter;
    for (friendIter = friendList.begin(); friendIter != friendList.end(); friendIter++)
    {
        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(friendIter->Fri_FollowerID);
        if (pData != NULL)
            height += 60.0f;
    }
    
    height += 3 * 60.0f;
    KNScrollView::SetScrollSize(ccp(320.0f, height));
    if (height < KNScrollView::GetScrollSize().y)
        height = KNScrollView::GetScrollSize().y;
    
    height -= 60.0f;
    
    // 陌生人个数
    vector<FriendInfo> vecStrangerList;
    list<FriendInfo>::iterator strangerIter;
    for (strangerIter = strangeList.begin(); strangerIter != strangeList.end(); strangerIter++)
    {
        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(strangerIter->Fri_FollowerID);
        if (pData != NULL)
        {
            if (IsSelf(strangerIter->Fri_UserID) == false && IsInFriendList(strangerIter->Fri_UserID) == false)
                vecStrangerList.push_back(*strangerIter);
        }
    }
    
    // 取三个陌生人
    vector<FriendInfo> vecList;
    if (vecStrangerList.size() >= 3)
    {
        while (vecList.size() < 3)
        {
            int iRand = rand() % vecStrangerList.size();
            
            if (vecList.size() == 0)
                vecList.push_back(vecStrangerList[iRand]);
            else
            {
                for (int i = 0; i < vecList.size(); i++)
                {
                    if (vecList[i].Fri_UserID == vecStrangerList[iRand].Fri_UserID)
                        continue;
                }
                
                vecList.push_back(vecStrangerList[iRand]);
            }
        }
    }
    else
    {
        for (int i = 0; i < vecStrangerList.size(); i++)
            vecList.push_back(vecStrangerList[i]);
    }
    
    // 刷新陌生人
    vector<FriendInfo>::iterator vecIter;
    for (vecIter = vecList.begin(); vecIter != vecList.end(); vecIter++)
    {
        // 选项信息
        FRIEND_ITEM_INFO stInfo;
        
        // 获得好友小弟数据
        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(vecIter->Fri_FollowerID);
        if (pData == NULL)
        {
            printf("Layer_FriendFollowerList::RefreshFriendList error = %d\n", vecIter->Fri_FollowerID);
            continue;
        }
        
        // 接受数据
        stInfo.iServerID = vecIter->Fri_UserID;
        stInfo.iFollowerID = vecIter->Fri_FollowerID;
        stInfo.iFollowerQuality = vecIter->Fri_FollowerQuality;
        stInfo.iLevel = vecIter->Fri_UserLevel;
        stInfo.iFollowerLevel = vecIter->Fri_FollowerLevel;
        stInfo.iHp = vecIter->Fri_FollowerHp;
        stInfo.iAttack = vecIter->Fri_FollowerAttack;
        stInfo.iRecover = vecIter->Fri_FollowerReply;
        stInfo.iCharacterID = vecIter->Fri_FollowerCharacterID;
        stInfo.iEnable = vecIter->Fri_CanBeUse;
        stInfo.iProfression = pData->Follower_Profession;
        stInfo.iMaxQuality = pData->Follower_MaxQuality;
        
        memcpy(stInfo.szIconName, pData->Follower_FightIconName, 32);
        
        // 更新icon - 小弟
        stInfo.pIcon = CCSprite::spriteWithSpriteFrameName(pData->Follower_IconName);
        if (stInfo.pIcon != NULL)
        {
            stInfo.pIcon->setPosition(ccp(-105.0f, height));
            addChild(stInfo.pIcon);
        }
        
        // 更新frame
        char szTemp[32] = {0};
        sprintf(szTemp, "info_frame_%d.png", pData->Follower_Profession);
        stInfo.pFrame = CCSprite::spriteWithSpriteFrameName(szTemp);
        if (stInfo.pFrame != NULL)
        {
            stInfo.pFrame->setPosition(ccp(0.0f, height));
            addChild(stInfo.pFrame);
        }
        
        // 更新lv标签
        stInfo.pLvMark = CCSprite::spriteWithSpriteFrameName("label_lv.png");
        if (stInfo.pLvMark != NULL)
        {
            stInfo.pLvMark->setPosition(ccp(-100.0f, height - 20.0f));
            addChild(stInfo.pLvMark);
        }
        
        // 更新lv背景
        stInfo.pLvBack = CCSprite::spriteWithSpriteFrameName("friend_level_mark.png");
        if (stInfo.pLvBack != NULL)
        {
            stInfo.pLvBack->setPosition(ccp(20.0f, height + 8.0f));
            addChild(stInfo.pLvBack);
        }
        
        // 关系标记
        stInfo.pRelate = CCSprite::spriteWithSpriteFrameName("releate_stranger.png");
        stInfo.pRelate->setPosition(ccp(-48.0f, height - 7.0f));
        addChild(stInfo.pRelate);
        
        // 友情点背景
        stInfo.pFriendBack = CCSprite::spriteWithSpriteFrameName("releate_value.png");
        stInfo.pFriendBack->setPosition(ccp(40.0f, height - 10.0f));
        addChild(stInfo.pFriendBack);
        
        // 更新选择按钮
        stInfo.pSelectButtonNormal = CCSprite::spriteWithSpriteFrameName("select_button_normal.png");
        if (stInfo.pSelectButtonNormal != NULL)
        {
            stInfo.pSelectButtonNormal->setPosition(ccp(100.0f, height));
            addChild(stInfo.pSelectButtonNormal);
        }
        stInfo.pSelectButtonSelect = CCSprite::spriteWithSpriteFrameName("select_button_select.png");
        if (stInfo.pSelectButtonSelect != NULL)
        {
            stInfo.pSelectButtonSelect->setPosition(ccp(100.0f, height));
            stInfo.pSelectButtonSelect->setIsVisible(false);
            addChild(stInfo.pSelectButtonSelect);
        }
        
        // 姓名
        stInfo.pFollowerName = CCLabelTTF::labelWithString(vecIter->Fri_UserName, "黑体", 13);
        if (stInfo.pFollowerName != NULL)
        {
            stInfo.pFollowerName->setPosition(ccp(-65.0f, height));
            stInfo.pFollowerName->setColor(ccc3(0, 0, 0));
            stInfo.pFollowerName->setAnchorPoint(ccp(0.0f, 0.0f));
            addChild(stInfo.pFollowerName);
        }
        
        // 更新好友小弟等级
        sprintf(szTemp, "%d", vecIter->Fri_FollowerLevel);
        stInfo.pFollowerLevel = CCLabelAtlas::labelWithString(szTemp, "RoundNum-hd.png", 10, 15, '.');
        if (stInfo.pFollowerLevel != NULL)
        {
            stInfo.pFollowerLevel->setPosition(ccp(-95.0f, height - 25.0f));
            stInfo.pFollowerLevel->setScale(0.7f);
            addChild(stInfo.pFollowerLevel);
        }
        
        // 更新好友等级
        sprintf(szTemp, "%d", vecIter->Fri_UserLevel);
        stInfo.pFriendLevel = CCLabelAtlas::labelWithString(szTemp, "UINum-hd.png", 8, 14, '.');
        if (stInfo.pFriendLevel != NULL)
        {
            stInfo.pFriendLevel->setPosition(ccp(25.0f, height));
            addChild(stInfo.pFriendLevel);
        }
        
        // 更新友情点
        sprintf(szTemp, "%d", 5);
        stInfo.pFriendValue = CCLabelAtlas::labelWithString(szTemp, "number_type_14-hd.png", 9, 15, '.');
        if (stInfo.pFriendValue != NULL)
        {
            stInfo.pFriendValue->setPosition(ccp(5.0f, height - 15.0f));
            addChild(stInfo.pFriendValue);
        }
        stInfo.iFriendValue = 5;
        
        // 递减高度
        height -= 60.0f;
        
        // 添加到列表
        m_vecFriendFollowerList.push_back(stInfo);
    }
    
    // 刷新好友
    for (friendIter = friendList.begin(); friendIter != friendList.end(); friendIter++)
    {
        // 使用过的小弟，跳过
        if (friendIter->Fri_CanBeUse != 1)
            continue;
        
        // 选项信息
        FRIEND_ITEM_INFO stInfo;
        
        // 获得好友小弟数据
        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(friendIter->Fri_FollowerID);
        if (pData == NULL)
        {
            printf("Layer_FriendFollowerList::RefreshFriendList error = %d\n", friendIter->Fri_FollowerID);
            continue;
        }
        
        // 接受数据
        stInfo.iServerID = friendIter->Fri_UserID;
        stInfo.iFollowerID = friendIter->Fri_FollowerID;
        stInfo.iFollowerQuality = friendIter->Fri_FollowerQuality;
        stInfo.iLevel = friendIter->Fri_UserLevel;
        stInfo.iFollowerLevel = friendIter->Fri_FollowerLevel;
        stInfo.iHp = friendIter->Fri_FollowerHp;
        stInfo.iAttack = friendIter->Fri_FollowerAttack;
        stInfo.iRecover = friendIter->Fri_FollowerReply;
        stInfo.iCharacterID = friendIter->Fri_FollowerCharacterID;
        stInfo.iEnable = friendIter->Fri_CanBeUse;
        stInfo.iProfression = pData->Follower_Profession;
        stInfo.iMaxQuality = pData->Follower_MaxQuality;
        
        memcpy(stInfo.szIconName, pData->Follower_FightIconName, 32);
        
        // 更新icon - 小弟
        stInfo.pIcon = CCSprite::spriteWithSpriteFrameName(pData->Follower_IconName);
        if (stInfo.pIcon != NULL)
        {
            stInfo.pIcon->setPosition(ccp(-105.0f, height));
            addChild(stInfo.pIcon);
        }
        
        // 更新frame
        char szTemp[32] = {0};
        sprintf(szTemp, "info_frame_%d.png", pData->Follower_Profession);
        stInfo.pFrame = CCSprite::spriteWithSpriteFrameName(szTemp);
        if (stInfo.pFrame != NULL)
        {
            stInfo.pFrame->setPosition(ccp(0.0f, height));
            addChild(stInfo.pFrame);
        }
        
        // 更新lv标签
        stInfo.pLvMark = CCSprite::spriteWithSpriteFrameName("label_lv.png");
        if (stInfo.pLvMark != NULL)
        {
            stInfo.pLvMark->setPosition(ccp(-100.0f, height - 20.0f));
            addChild(stInfo.pLvMark);
        }
        
        // 更新lv背景
        stInfo.pLvBack = CCSprite::spriteWithSpriteFrameName("friend_level_mark.png");
        if (stInfo.pLvBack != NULL)
        {
            stInfo.pLvBack->setPosition(ccp(20.0f, height + 8.0f));
            addChild(stInfo.pLvBack);
        }
        
        // 关系标记
        stInfo.pRelate = CCSprite::spriteWithSpriteFrameName("releate_friend.png");
        stInfo.pRelate->setPosition(ccp(-53.0f, height - 7.0f));
        addChild(stInfo.pRelate);
        
        // 友情点背景
        stInfo.pFriendBack = CCSprite::spriteWithSpriteFrameName("releate_value.png");
        stInfo.pFriendBack->setPosition(ccp(40.0f, height - 10.0f));
        addChild(stInfo.pFriendBack);
        
        // 更新选择按钮
        stInfo.pSelectButtonNormal = CCSprite::spriteWithSpriteFrameName("select_button_normal.png");
        if (stInfo.pSelectButtonNormal != NULL)
        {
            stInfo.pSelectButtonNormal->setPosition(ccp(100.0f, height));
            addChild(stInfo.pSelectButtonNormal);
        }
        stInfo.pSelectButtonSelect = CCSprite::spriteWithSpriteFrameName("select_button_select.png");
        if (stInfo.pSelectButtonSelect != NULL)
        {
            stInfo.pSelectButtonSelect->setPosition(ccp(100.0f, height));
            stInfo.pSelectButtonSelect->setIsVisible(false);
            addChild(stInfo.pSelectButtonSelect);
        }
        
        // 姓名
        stInfo.pFollowerName = CCLabelTTF::labelWithString(friendIter->Fri_UserName, "黑体", 13);
        if (stInfo.pFollowerName != NULL)
        {
            stInfo.pFollowerName->setPosition(ccp(-65.0f, height));
            stInfo.pFollowerName->setColor(ccc3(0, 0, 0));
            stInfo.pFollowerName->setAnchorPoint(ccp(0.0f, 0.0f));
            addChild(stInfo.pFollowerName);
        }
        
        // 更新好友小弟等级
        sprintf(szTemp, "%d", friendIter->Fri_FollowerLevel);
        stInfo.pFollowerLevel = CCLabelAtlas::labelWithString(szTemp, "RoundNum-hd.png", 10, 15, '.');
        if (stInfo.pFollowerLevel != NULL)
        {
            stInfo.pFollowerLevel->setPosition(ccp(-95.0f, height - 25.0f));
            stInfo.pFollowerLevel->setScale(0.7f);
            addChild(stInfo.pFollowerLevel);
        }
        
        // 更新好友等级
        sprintf(szTemp, "%d", friendIter->Fri_UserLevel);
        stInfo.pFriendLevel = CCLabelAtlas::labelWithString(szTemp, "UINum-hd.png", 8, 14, '.');
        if (stInfo.pFriendLevel != NULL)
        {
            stInfo.pFriendLevel->setPosition(ccp(25.0f, height));
            addChild(stInfo.pFriendLevel);
        }
        
        // 更新友情点
        sprintf(szTemp, "%d", 10);
        stInfo.pFriendValue = CCLabelAtlas::labelWithString(szTemp, "number_type_14-hd.png", 9, 15, '.');
        if (stInfo.pFriendValue != NULL)
        {
            stInfo.pFriendValue->setPosition(ccp(0.0f, height - 16.0f));
            addChild(stInfo.pFriendValue);
        }
        stInfo.iFriendValue = 10;
        
        // 递减高度
        height -= 60.0f;
        
        // 添加到列表
        m_vecFriendFollowerList.push_back(stInfo);
    }
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类刷新陌生人列表函数
/************************************************************************/
void Layer_FriendFollowerList::RefreshStrangerList()
{
    // 移出所有小弟
    vector<FRIEND_ITEM_INFO>::iterator iter;
    for (iter = m_vecFriendFollowerList.begin(); iter != m_vecFriendFollowerList.end(); iter++)
    {
        removeChild(iter->pIcon, true);
        removeChild(iter->pFrame, true);
        removeChild(iter->pLvMark, true);
        removeChild(iter->pLvBack, true);
        
        removeChild(iter->pSelectButtonNormal, true);
        removeChild(iter->pSelectButtonSelect, true);
        
        removeChild(iter->pAddButtonNormal, true);
        removeChild(iter->pAddButtonSelect, true);
        
        removeChild(iter->pFollowerName, true);
        removeChild(iter->pFollowerLoginTime, true);
        
        removeChild(iter->pFollowerLevel, true);
        removeChild(iter->pFriendLevel, true);
        removeChild(iter->pFriendValue, true);
    }
    m_vecFriendFollowerList.clear();
    
    // 恢复默认高度
    setPosition(ccp(0.0f, 0.0f));
    
    // 取得陌生人列表
    map<int, list<FriendInfo> >::iterator it = ServerDataManage::ShareInstance()->m_FriendInfoList.find(1);
    if (it == ServerDataManage::ShareInstance()->m_FriendInfoList.end())
        return;
    list<FriendInfo> friendList = it->second;
    
    // 计算列表高度
    float height = 0.0f;
    list<FriendInfo>::iterator heightIter;
    int index = 0;
    for (heightIter = friendList.begin(); heightIter != friendList.end(); heightIter++)
    {
        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(heightIter->Fri_FollowerID);
        if (pData != NULL)
        {
            height += 60.0f;
            
            if (index++ > 30)
                break;
        }
    }
    
    KNScrollView::SetScrollSize(ccp(320.0f, height));
    if (height < KNScrollView::GetScrollSize().y)
        height = KNScrollView::GetScrollSize().y;
    
    height -= 60.0f;
    
    // 刷新列表
    list<FriendInfo>::iterator friendIter;
    index = 0;
    for (friendIter = friendList.begin(); friendIter != friendList.end(); friendIter++)
    {
        // 选项信息
        FRIEND_ITEM_INFO stInfo;
        
        // 获得好友小弟数据
        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(friendIter->Fri_FollowerID);
        if (pData == NULL)
        {
            printf("Layer_FriendFollowerList::RefreshFriendList error = %d\n", friendIter->Fri_FollowerID);
            continue;
        }
        
        // 设置数据
        stInfo.iServerID = friendIter->Fri_UserID;
        stInfo.iFollowerID = friendIter->Fri_FollowerID;
        stInfo.iFollowerQuality = friendIter->Fri_FollowerQuality;
        
        // 更新icon - 小弟
        stInfo.pIcon = CCSprite::spriteWithSpriteFrameName(pData->Follower_IconName);
        if (stInfo.pIcon != NULL)
        {
            stInfo.pIcon->setPosition(ccp(-105.0f, height));
            addChild(stInfo.pIcon);
        }
        
        // 更新frame
        char szTemp[32] = {0};
        sprintf(szTemp, "info_frame_%d.png", pData->Follower_Profession);
        stInfo.pFrame = CCSprite::spriteWithSpriteFrameName(szTemp);
        if (stInfo.pFrame != NULL)
        {
            stInfo.pFrame->setPosition(ccp(0.0f, height));
            addChild(stInfo.pFrame);
        }
        
        // 更新lv标签
        stInfo.pLvMark = CCSprite::spriteWithSpriteFrameName("label_lv.png");
        if (stInfo.pLvMark != NULL)
        {
            stInfo.pLvMark->setPosition(ccp(-100.0f, height - 20.0f));
            addChild(stInfo.pLvMark);
        }
        
        // 更新lv背景
        stInfo.pLvBack = CCSprite::spriteWithSpriteFrameName("friend_level_mark.png");
        if (stInfo.pLvBack != NULL)
        {
            stInfo.pLvBack->setPosition(ccp(20.0f, height));
            addChild(stInfo.pLvBack);
        }
        
        // 更新选择按钮
        stInfo.pAddButtonNormal = CCSprite::spriteWithSpriteFrameName("button_addfriend_normal.png");
        if (stInfo.pAddButtonNormal != NULL)
        {
            stInfo.pAddButtonNormal->setPosition(ccp(100.0f, height));
            addChild(stInfo.pAddButtonNormal);
        }
        stInfo.pAddButtonSelect = CCSprite::spriteWithSpriteFrameName("button_addfriend_select.png");
        if (stInfo.pAddButtonSelect != NULL)
        {
            stInfo.pAddButtonSelect->setPosition(ccp(100.0f, height));
            stInfo.pAddButtonSelect->setIsVisible(false);
            addChild(stInfo.pAddButtonSelect);
        }
        
        // 姓名
        stInfo.pFollowerName = CCLabelTTF::labelWithString(friendIter->Fri_UserName, "黑体", 13);
        if (stInfo.pFollowerName != NULL)
        {
            stInfo.pFollowerName->setPosition(ccp(-65.0f, height - 8.0f));
            stInfo.pFollowerName->setColor(ccc3(0, 0, 0));
            stInfo.pFollowerName->setAnchorPoint(ccp(0.0f, 0.0f));
            addChild(stInfo.pFollowerName);
        }
        
        // 更新好友小弟等级
        sprintf(szTemp, "%d", friendIter->Fri_FollowerLevel);
        stInfo.pFollowerLevel = CCLabelAtlas::labelWithString(szTemp, "RoundNum-hd.png", 10, 15, '.');
        if (stInfo.pFollowerLevel != NULL)
        {
            stInfo.pFollowerLevel->setPosition(ccp(-95.0f, height - 25.0f));
            stInfo.pFollowerLevel->setScale(0.7f);
            addChild(stInfo.pFollowerLevel);
        }
        
        // 更新好友等级
        sprintf(szTemp, "%d", friendIter->Fri_UserLevel);
        stInfo.pFriendLevel = CCLabelAtlas::labelWithString(szTemp, "UINum-hd.png", 8, 14, '.');
        if (stInfo.pFriendLevel != NULL)
        {
            stInfo.pFriendLevel->setPosition(ccp(25.0f, height - 8.0f));
            addChild(stInfo.pFriendLevel);
        }
        
        // 递减高度
        height -= 60.0f;
        
        // 添加到列表
        m_vecFriendFollowerList.push_back(stInfo);
        
        if (index++ > 30)
            break;
    }
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类判断是否是自己函数
/************************************************************************/
bool Layer_FriendFollowerList::IsSelf(int iServerID)
{
    // 获得玩家id
    int iUserID = atoi(PlayerDataManage::ShareInstance()->m_PlayerUserID);
    if (iServerID == iUserID)
        return true;
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_FriendFollowerList类判断是否在好友列表中函数
/************************************************************************/
bool Layer_FriendFollowerList::IsInFriendList(int iServerID)
{
    // 好友，陌生人列表
    map<int, list<FriendInfo> > mapList = ServerDataManage::ShareInstance()->m_FriendInfoList;
    map<int, list<FriendInfo> >::iterator iter = mapList.find(0);
    
    list<FriendInfo> friendList;
    if (iter != mapList.end())
        friendList = iter->second;
    
    // 是否在列表中
    list<FriendInfo>::iterator friendIter;
    for (friendIter = friendList.begin(); friendIter != friendList.end(); friendIter++)
    {
        if (iServerID == friendIter->Fri_UserID)
            return true;
    }
    
    return false;
}