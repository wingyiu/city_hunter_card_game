//
//  Layer_DiamondList.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-7-23.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_DiamondList.h"

/************************************************************************/
#pragma mark - Layer_DiamondList类构造函数
/************************************************************************/
Layer_DiamondList::Layer_DiamondList()
{
    // 为钻石列表对象变量赋初值
    m_pBatchNode = NULL;
    
    m_vecDiamondList.clear();
    
    // 为钻石列表属性变量赋初值
}

/************************************************************************/
#pragma mark - Layer_DiamondList类析构函数
/************************************************************************/
Layer_DiamondList::~Layer_DiamondList()
{
    Destroy();
}   

/************************************************************************/
#pragma mark - Layer_DiamondList类初始化函数 : 使用自动内存使用
/************************************************************************/
bool Layer_DiamondList::init()
{
    return CCLayer::init();
}

/************************************************************************/
#pragma mark - Layer_DiamondList类销毁函数
/************************************************************************/
void Layer_DiamondList::Destroy()
{
    if (m_pBatchNode == NULL)
        return;
    
    // 移出商品列表
    vector<DIAMOND_INFO>::iterator iter;
    for (iter = m_vecDiamondList.begin(); iter != m_vecDiamondList.end(); iter++)
    {
        m_pBatchNode->removeChild(iter->pInfo, true);
        m_pBatchNode->removeChild(iter->pButtonNormal, true);
        m_pBatchNode->removeChild(iter->pButtonSelect, true);
        
        removeChild(iter->pPrice, true);
    }
    m_vecDiamondList.clear();
    
    // 移出绘制节点
    removeChild(m_pBatchNode, true);
    
    CCTouchDispatcher::sharedDispatcher() -> removeDelegate(this);
}

/************************************************************************/
#pragma mark - Layer_DiamondList类重写退出函数 : 使用自动内存使用
/************************************************************************/
void Layer_DiamondList::onExit()
{
    CCLayer::onExit();
    
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_DiamondList类初始化函数
/************************************************************************/
bool Layer_DiamondList::init(UIInfo& stInfo)
{
    // 初始化scrollview
    KNScrollView::init(stInfo);
    
    // 事件
    if (stInfo.bIsVisible)
        CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    // 区域
    SetVisibleRange(CCRect(stInfo.point.x, stInfo.point.y, stInfo.size.width, stInfo.size.height));
    
    // 设置滑动类型
    KNScrollView::SetScrollType(static_cast<ScrollType>(stInfo.iType));
    
    // 初始化绘制节点
    m_pBatchNode = CCSpriteBatchNode::batchNodeWithFile("game_1-hd.pvr.ccz");
    if (m_pBatchNode != NULL)
        addChild(m_pBatchNode);
    else
        return false;
    
    // 设置列表宽高度
    KNScrollView::SetItemSize(CCSize(160.0f, 100.0f));
    KNScrollView::SetHorizontalSliderX(138.0f);
    KNScrollView::SetSliderEnable(true);
    
    // 刷新列表
    RefreshDiamondShopList();
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_DiamondList类刷新钻石商店列表函数
/************************************************************************/
void Layer_DiamondList::RefreshDiamondShopList()
{
    // 移出商品列表
    vector<DIAMOND_INFO>::iterator iter;
    for (iter = m_vecDiamondList.begin(); iter != m_vecDiamondList.end(); iter++)
    {
        m_pBatchNode->removeChild(iter->pInfo, true);
        m_pBatchNode->removeChild(iter->pButtonNormal, true);
        m_pBatchNode->removeChild(iter->pButtonSelect, true);
        
        removeChild(iter->pPrice, true);
    }
    m_vecDiamondList.clear();
    
    // 刷新列表
    float height = 0.0f;
    for (int i = 0; i < 20; i++)
    {
        // 创建选项信息
        DIAMOND_INFO stInfo;
        
        // 计算位置
        float x = (i % 2) * 140.0f - 75.0f;
        float y = (i / 2) * 100.0f;
        
        // 初始化钻石信息背景
        stInfo.pInfo = CCSprite::spriteWithSpriteFrameName("diamond_item_1.png");
        
        // 初始化钻石价格
        stInfo.pPrice = CCLabelAtlas::labelWithString("34.90", "Number_FollowerAttack1-hd.png", 12, 18, '.');
        if (stInfo.pPrice != NULL)
        {
            stInfo.pPrice->setPosition(ccp(x, y - 10.0f));
            addChild(stInfo.pPrice, 1);
        }
        
        // 初始化购买按钮
        stInfo.pButtonNormal = CCSprite::spriteWithSpriteFrameName("button_diabuy_normal.png");
        stInfo.pButtonSelect = CCSprite::spriteWithSpriteFrameName("button_diabuy_select.png");
        
        // 设置属性
        if (stInfo.pInfo != NULL && stInfo.pButtonNormal != NULL && stInfo.pButtonSelect)
        {
            stInfo.pInfo->setPosition(ccp(x, y));
            
            stInfo.pButtonNormal->setPosition(ccp(x + 30.0f, y - 30.0f));
            stInfo.pButtonSelect->setPosition(ccp(x + 30.0f, y - 30.0f));
            
            stInfo.pButtonNormal->setIsVisible(true);
            stInfo.pButtonSelect->setIsVisible(false);
            
            m_pBatchNode->addChild(stInfo.pInfo);
            m_pBatchNode->addChild(stInfo.pButtonNormal);
            m_pBatchNode->addChild(stInfo.pButtonSelect);
            
            // 添加到列表
            m_vecDiamondList.push_back(stInfo);
            
            height += 50.0f;
        }
    }
    
    // 设置列表尺寸
    KNScrollView::SetScrollSize(ccp(320.0f, height));
    
    // 滑动到顶端
    KNScrollView::SetPoint(true);
}

/************************************************************************/
#pragma mark - Layer_DiamondList类事件函数 : 点击
/************************************************************************/
bool Layer_DiamondList::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    
    CCRect visibleRect = GetVisibleRange();
    if (point.x > visibleRect.origin.x && point.x < visibleRect.origin.x + visibleRect.size.width &&
        point.y > visibleRect.origin.y && point.y < visibleRect.origin.y + visibleRect.size.height)
    {
        KNScrollView::ccTouchBegan(pTouch, pEvent);
        
        // 检测那个被点击
        CCPoint pos(0.0f, 0.0f);
        CCSize size(0.0f, 0.0f);
        
        // 转换坐标
        point = convertToNodeSpace(point);
        
        // 遍历所有选项
        vector<DIAMOND_INFO>::iterator iter;
        for (iter = m_vecDiamondList.begin(); iter != m_vecDiamondList.end(); iter++)
        {
            // 获得位置和尺寸
            pos = iter->pButtonNormal->getPosition();
            size = iter->pButtonNormal->getContentSize();
            
            // 是否被点击
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f
                )
            {
                // 还原按钮状态
                iter->pButtonNormal->setIsVisible(false);
                iter->pButtonSelect->setIsVisible(true);
                
                return true;
            }
        }
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_DiamondList类事件函数 : 弹起
/************************************************************************/
void Layer_DiamondList::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    // 调用基类事件
    KNScrollView::ccTouchEnded(pTouch, pEvent);
    
    // 获得点击坐标
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    // 遍历所有选项
    CCPoint pos(0.0f, 0.0f);
    CCSize size(0.0f, 0.0f);
    
    vector<DIAMOND_INFO>::iterator iter;
    for (iter = m_vecDiamondList.begin(); iter != m_vecDiamondList.end(); iter++)
    {
        // 获得位置和尺寸
        pos = iter->pButtonNormal->getPosition();
        size = iter->pButtonNormal->getContentSize();
        
        // 是否被点击
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f
            )
        {
            //if (GetIsMovingMark())
            //    return;
        }
        
        // 还原按钮状态
        iter->pButtonNormal->setIsVisible(true);
        iter->pButtonSelect->setIsVisible(false);
    }
}

/************************************************************************/
#pragma mark - Layer_DiamondList类事件函数 : 划动
/************************************************************************/
void Layer_DiamondList::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
{
    KNScrollView::ccTouchMoved(pTouch, pEvent);
}