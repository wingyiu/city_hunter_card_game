//
//  Sprite_MySprite.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-7-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Sprite_MySprite.h"

/************************************************************************/
#pragma mark - Sprite_MySprite类静态函数 : 生成精灵函数
/************************************************************************/
Sprite_MySprite* Sprite_MySprite::mySpriteWithSpriteFrameName(const char* filename)
{
    Sprite_MySprite* pSprite = new Sprite_MySprite();
    if (pSprite != NULL)
    {
        if (pSprite->initWithSpriteFrameName(filename) == true)
        {
            pSprite->autorelease();
            return pSprite;
        }
    }
    
    delete pSprite;
    pSprite = NULL;
    
    return NULL;
}

/************************************************************************/
#pragma mark - Sprite_MySprite类添加一个偏移位置函数
/************************************************************************/
void Sprite_MySprite::addOnePositionOffset(CCPoint point)
{
    m_vecPosOffsetList.push_back(point);
}

/************************************************************************/
#pragma mark - Sprite_MySprite类清空所有偏移位置函数
/************************************************************************/
void Sprite_MySprite::clearAllPositionOffset()
{
    m_vecPosOffsetList.clear();
}

/************************************************************************/
#pragma mark - Sprite_MySprite类构造函数
/************************************************************************/
Sprite_MySprite::Sprite_MySprite()
{
    // 为自定义精灵对象变量赋初值
    
    // 为自定义精灵属性变量赋初值
    m_vecPosOffsetList.clear();
}

/************************************************************************/
#pragma mark - Sprite_MySprite类析构函数
/************************************************************************/
Sprite_MySprite::~Sprite_MySprite()
{
    
}

/************************************************************************/
#pragma mark - Sprite_MySprite类重写函数 : visit函数
/************************************************************************/
void Sprite_MySprite::visit()
{
    // 如果不可见，则退出
    if (getIsVisible() == false)
        return;
    
    // 关闭深度缓冲
    glDisable(GL_DEPTH_TEST);
    
    // 调用基类访问函数
    //CCSprite::visit();
    
    // 获得原始位置
    CCPoint pos = getPosition();
    
    // 偏移绘制
    vector<CCPoint>::iterator iter;
    for (iter = m_vecPosOffsetList.begin(); iter != m_vecPosOffsetList.end(); iter++)
    {
        setPosition(ccp(pos.x + iter->x, pos.y + iter->y));
        CCSprite::visit();
    }
    
    // 还原原始位置
    setPosition(pos);
    
    // 打开深度缓冲
    glEnable(GL_DEPTH_TEST);
}