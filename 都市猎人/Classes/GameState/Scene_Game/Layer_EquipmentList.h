//
//  Layer_FollowerEquip.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-5-2.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//
/*
#ifndef MidNightCity_Layer_FollowerEquip_h
#define MidNightCity_Layer_FollowerEquip_h

#include "KNScrollLayer.h"
#include "PlayerDataManage.h"
#include "Equipment.h"
#include "Follower.h"

#pragma mark - 打开列表状态
enum LIST_STATE
{
    CHECK = 0,              // 查询状态
    EQUIP,                  // 装备状态
};

#pragma mark - 装备列表类定义
class Layer_EquipmentList : public KNScrollLayer
{
public:
    #pragma mark - 构造函数
    Layer_EquipmentList();
    #pragma mark - 析构函数 
    virtual ~Layer_EquipmentList();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 初始化函数 : 使用自动内存使用
    bool init();
    
    #pragma mark - 重写退出函数 : 使用自动内存使用
    virtual void onExit();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 从仓库数据更新列表选项函数
    void RefreshItemListFromDynamicData();
    
    #pragma mark - 设置列表状态
    void SetListState(LIST_STATE eState)                        {m_eState = eState;}
    #pragma mark - 获得列表状态
    LIST_STATE GetListState()                                   {return m_eState;}
    
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 滑动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(Layer_EquipmentList);
private:
    // 装备列表对象变量
    vector<Equipment*> m_vecEquipmentList;                      // 视图列表
    
    // 装备列表属性变量
    Equipment_Type m_eEquipType;                                // 装备类型
    
    LIST_STATE m_eState;                                        // 列表状态
    
    int m_iEnableIndex;                                         // 当前激活的装备视图
};

#endif
 */
