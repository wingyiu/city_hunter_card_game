//
//  Layer_Calendar.cpp
//  都市猎人
//
//  Created by 孙 惠伟 on 12-9-25.
//
//

#include "Layer_Calendar.h"
#include "ServerDataManage.h"

// 月份天数
const int g_arrMonthDays[12] = {31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
// 宝箱开启天数
int g_arrDayLimit[] = {1, 3, 5, 7, 15, 30};

/************************************************************************/
#pragma mark - Layer_Calendar类构造函数
/************************************************************************/
Layer_Calendar::Layer_Calendar()
{
    // 为日历对象变量赋初值
    m_pPassSign = NULL;
    m_pNowSign = NULL;
    m_pFutureSign = NULL;
    
    m_pSignOK = NULL;
    m_pSignNot = NULL;
    
    for (int i = 0; i < MONTH_DAYS; i++)
        m_pDayNumber[i] = NULL;
    
    // 未日历属性变量赋初值
    memset(&m_stDate, 0, sizeof(tm));
}

/************************************************************************/
#pragma mark - Layer_Calendar类析构函数
/************************************************************************/
Layer_Calendar::~Layer_Calendar()
{
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_Calendar类初始化函数
/************************************************************************/
bool Layer_Calendar::init()
{
    return CCLayer::init();
}

/************************************************************************/
#pragma mark - Layer_Calendar类初始化函数
/************************************************************************/
bool Layer_Calendar::init(UIInfo& stInfo)
{
    // 设置位置
    setPosition(stInfo.point);
    
    // 初始化过去的背景
    m_pPassSign = Sprite_MySprite::mySpriteWithSpriteFrameName("sign_pass_back.png");
    addChild(m_pPassSign);
    
    // 初始化现在的背景
    m_pNowSign = Sprite_MySprite::mySpriteWithSpriteFrameName("sign_now_back.png");
    addChild(m_pNowSign);
    
    // 初始化未来的背景
    m_pFutureSign = Sprite_MySprite::mySpriteWithSpriteFrameName("sign_future_back.png");
    addChild(m_pFutureSign);
    
    // 初始化签到标志
    m_pSignOK = Sprite_MySprite::mySpriteWithSpriteFrameName("sign_ok.png");
    addChild(m_pSignOK);
    
    // 初始化未签到标志
    m_pSignNot = Sprite_MySprite::mySpriteWithSpriteFrameName("sign_not.png");
    addChild(m_pSignNot);
    
    // 刷新日历
    RefreshCalendarInfo();
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_Calendar类销毁函数
/************************************************************************/
void Layer_Calendar::Destroy()
{
    removeChild(m_pPassSign, true);
    removeChild(m_pNowSign, true);
    removeChild(m_pFutureSign, true);
    
    removeChild(m_pSignOK, true);
    removeChild(m_pSignNot, true);
    
    for (int i = 0; i < MONTH_DAYS; i++)
        removeChild(m_pDayNumber[i], true);
}

/************************************************************************/
#pragma mark - Layer_Calendar类重写退出函数
/************************************************************************/
void Layer_Calendar::onExit()
{
    CCLayer::onExit();
    
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_Calendar类刷新日历信息函数
/************************************************************************/
void Layer_Calendar::RefreshCalendarInfo()
{
    // 移除数字 : 日
    for (int i = 0; i < MONTH_DAYS; i++)
        removeChild(m_pDayNumber[i], true);
    
    // 获得当前日期
    time_t t = ServerDataManage::ShareInstance()->m_stSign.iTime;
    m_stDate = *localtime(&t);
    
    // 清空背景标记位置
    m_pPassSign->clearAllPositionOffset();
    m_pNowSign->clearAllPositionOffset();
    m_pFutureSign->clearAllPositionOffset();
    
    m_pSignOK->clearAllPositionOffset();
    m_pSignNot->clearAllPositionOffset();
    
    // 序列
    int index = 0;
    int iDayIndex = 0;
    int iSignIndex = 0;
    
    CCPoint pos(0.0f, 0.0f);
    
    // 背景 : 空白日期
    int iDay = GetWeekdayFor1th();
    for (int i = 0; i < iDay; i++)
    {
        pos.x = index % 7 * 41.0f;
        pos.y = -index / 7 * 31.0f;
        
        m_pFutureSign->addOnePositionOffset(pos);
        
        ++index;
    }
    
    // 记录日子和签到标志起始点
    iDayIndex = index;
    iSignIndex = index;
    
    // 背景 : 当前日期
    for (int i = 0; i < m_stDate.tm_mday; i++)
    {
        pos.x = index % 7 * 41.0f;
        pos.y = -index / 7 * 31.0f;
        
        if (m_stDate.tm_mday - 1 != i)
            m_pPassSign->addOnePositionOffset(pos);
        else
            m_pNowSign->addOnePositionOffset(pos);
        
        ++index;
    }
    
    // 初始化签到标志
    for (int i = 1; i <= m_stDate.tm_mday; i++)
    {
        pos.x = iSignIndex % 7 * 41.0f;
        pos.y = -iSignIndex / 7 * 31.0f;
        
        if (IsSignAlready(i) == true)
            m_pSignOK->addOnePositionOffset(ccp(pos.x + 7.0f, pos.y - 3.0f));
        else
            m_pSignNot->addOnePositionOffset(ccp(pos.x + 7.0f, pos.y - 3.0f));
        
        ++iSignIndex;
    }
    
    // 初始化数字 : 日
    char szTemp[16] = {0};
    int iMonthDays = GetMonthDays();
    for (int i = 0; i < iMonthDays; i++)
    {
        pos.x = iDayIndex % 7 * 41.0f;
        pos.y = -iDayIndex / 7 * 31.0f;
        
        sprintf(szTemp, "%d", i + 1);
        m_pDayNumber[i] = CCLabelTTF::labelWithString(szTemp, "Arial", 14);
        m_pDayNumber[i]->setColor(ccc3(0, 0, 0));
        m_pDayNumber[i]->setPosition(ccp(pos.x - 10.0f, pos.y + 7.0f));
        addChild(m_pDayNumber[i]);
        
        ++iDayIndex;
    }
    
    // 剩余格子
    for (int i = 0; i < 42 - m_stDate.tm_mday - iDay; i++)
    {
        pos.x = index % 7 * 41.0f;
        pos.y = -index / 7 * 31.0f;
        
        m_pFutureSign->addOnePositionOffset(pos);
        
        ++index;
    }
    
    m_iSignEnableNum = m_stDate.tm_mday - ServerDataManage::ShareInstance()->m_stSign.m_vecSignTime.size();
}

/************************************************************************/
#pragma mark - Layer_Calendar类获得未签到的第一天函数
/************************************************************************/
int Layer_Calendar::GetFirstUnsignDay()
{
    for (int i = 1; i <= GetMonthDays(); i++)
    {
        if (IsSignAlready(i) == false)
            return i;
    }
    
    return -1;
}

/************************************************************************/
#pragma mark - Layer_Calendar类事件函数 : 点击
/************************************************************************/
bool Layer_Calendar::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    return false;
}

/************************************************************************/
#pragma mark - Layer_Calendar类事件函数 : 弹起
/************************************************************************/
void Layer_Calendar::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    
}

/************************************************************************/
#pragma mark - Layer_Calendar类事件函数 : 滑动
/************************************************************************/
void Layer_Calendar::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
{
    
}

/************************************************************************/
#pragma mark - Layer_Calendar类获得月份天数函数
/************************************************************************/
int Layer_Calendar::GetMonthDays()
{
    // 2月特别处理
    if (m_stDate.tm_mon + 1 == 2)
    {
        int iYear = m_stDate.tm_year + 1900;
        if ((iYear % 4 == 0 && iYear % 100 != 0) ||
            (iYear % 100 == 0 && iYear % 400 == 0))
        {
            return 29;
        }
        else
        {
            return 28;
        }
    }
    
    return g_arrMonthDays[m_stDate.tm_mon];
}

/************************************************************************/
#pragma mark - Layer_Calendar类获得1号的星期数函数
/************************************************************************/
int Layer_Calendar::GetWeekdayFor1th()
{
    int day = m_stDate.tm_wday;
    int tm_day = m_stDate.tm_mday;
    
    while (tm_day > 7)
        tm_day -= 7;
    
    while (tm_day != 1)
    {
        day -= 1;
        if (day == -1)
            day = 6;
        
        tm_day -= 1;
    }
    
    return day;
}

/************************************************************************/
#pragma mark - Layer_Calendar类该日是否签到过函数
/************************************************************************/
bool Layer_Calendar::IsSignAlready(int iDay)
{
    vector<time_t> vecSignTime = ServerDataManage::ShareInstance()->m_stSign.m_vecSignTime;
    vector<time_t>::iterator iter;
    tm stTime;
    for (iter = vecSignTime.begin(); iter != vecSignTime.end(); iter++)
    {
        // 解析时间
        time_t t = *iter;
        stTime = *localtime(&t);
        
        // 是否已签到
        if (stTime.tm_mon == m_stDate.tm_mon && stTime.tm_mday == iDay)
            return true;
    }
    
    return false;
}