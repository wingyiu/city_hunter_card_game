//
//  Scene_Game.h
//  MidNightCity
//
//  Created by 强 张 on 12-3-30.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Scene_Game_h
#define MidNightCity_Scene_Game_h

#include "cocos2d.h"

#include "KNUImanager.h"
#include "Layer_Game.h"
#include "Layer_Building.h"

// Game场景ui层id
const int GAME_UI_ID = 1002;
const int GAME_SCENE_ID = 123123;

USING_NS_CC;

class Scene_Game : public CCLayer
{
public:
    #pragma mark - 创建场景函数
    static CCScene* scene();
    
    #pragma mark - 构造函数
    Scene_Game();
    #pragma mark - 析构函数
    virtual ~Scene_Game();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 退出函数
    virtual void onExit();
    
    #pragma mark - 连接基类初始化
    LAYER_NODE_FUNC(Scene_Game);
};

#endif
