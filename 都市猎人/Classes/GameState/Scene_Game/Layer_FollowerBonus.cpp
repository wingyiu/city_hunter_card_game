//
//  Layer_FollowerBonus.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-5-10.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_FollowerBonus.h"

extern bool g_bIsEnableRetina;

/************************************************************************/
#pragma mark - Layer_FollowerBonus类构造函数
/************************************************************************/
//Layer_FollowerBonus::Layer_FollowerBonus()
//{
//    // 为抽奖对象变量赋初值
//    memset(m_pCardSprite, 0, sizeof(CCSprite*) * MAX_CARD_NUM);
//    
//    // 为抽奖属性变量赋初值
//    m_iSelectedIndex = 0;
//}

/************************************************************************/
#pragma mark - Layer_FollowerBonus类析构函数
/************************************************************************/
//Layer_FollowerBonus::~Layer_FollowerBonus()
//{
//    // 销毁所有对象
//    Destroy();
//}

/************************************************************************/
#pragma mark - Layer_FollowerBonus类初始化函数 : 使用自动内存使用
/************************************************************************/
//bool Layer_FollowerBonus::init()
//{
//    return true;
//}

/************************************************************************/
#pragma mark - Layer_FollowerBonus类重写退出函数 : 使用自动内存使用
/************************************************************************/
//void Layer_FollowerBonus::onExit()
//{
//    // 销毁所有对象
//    Destroy();
//}

/************************************************************************/
#pragma mark - Layer_FollowerBonus类销毁函数
/************************************************************************/
//void Layer_FollowerBonus::Destroy()
//{
//    for (int i = 0; i < MAX_CARD_NUM; i++)
//        removeChild(m_pCardSprite[i], true);
//}

/************************************************************************/
#pragma mark - Layer_FollowerBonus类初始化函数
/************************************************************************/
//bool Layer_FollowerBonus::init(UIInfo& stInfo)
//{
//    // 初始化扑克
//    for (int i = 0; i < MAX_CARD_NUM; i++)
//    {
//        m_pCardSprite[i] = CCSprite::spriteWithSpriteFrameName("poke.png");
//        if (m_pCardSprite[i] != NULL)
//        {
//            m_pCardSprite[i]->setPosition(ccp(0.0f, 0.0f));
//            addChild(m_pCardSprite[i]);
//        }
//    }
//    
//    return true;
//}

/************************************************************************/
#pragma mark - Layer_FollowerBonus类事件函数 : 点击
/************************************************************************/
//bool Layer_FollowerBonus::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
//{
//    //CCFlipX3D* pFlipX3D = CCFlipX3D::actionWithDuration(2.0f);
//    //if (pFlipX3D != NULL)
//    //    m_pCardSprite[0]->runAction(pFlipX3D);
//    
//    // 获得鼠标点击坐标
//    CCPoint point = pTouch->locationInView(pTouch->view());
//    point = CCDirector::sharedDirector()->convertToGL(point);
//    point = convertToNodeSpace(point);
//    
//    // 检测那张牌被点中
//    CCPoint cardPos(0.0f, 0.0f);
//    CCSize cardSize(0.0f, 0.0f);
//    for (int i = MAX_CARD_NUM - 1; i >= 0; i--)
//    {
//        cardPos = m_pCardSprite[i]->getPosition();
//        cardSize = m_pCardSprite[i]->getContentSize();
//        
//        if (point.x > cardPos.x - cardSize.width / 2.0f && point.x < cardPos.x + cardSize.width / 2.0f &&
//            point.y > cardPos.y - cardSize.height / 2.0f && point.y < cardPos.y + cardSize.height / 2.0f)
//        {
//            m_iSelectedIndex = i;
//            return true;
//        }
//    }
//    
//    return false;
//}

/************************************************************************/
#pragma mark - Layer_FollowerBonus类事件函数 : 弹起
/************************************************************************/
//void Layer_FollowerBonus::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
//{
//    
//}

/************************************************************************/
#pragma mark - Layer_FollowerBonus类事件函数 : 滑动
/************************************************************************/
//void Layer_FollowerBonus::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
//{
//    // 点击坐标
//    CCPoint curPoint = convertTouchToNodeSpace(pTouch);
//    
//    // 上次点击坐标
//    CCPoint lastPoint = pTouch->previousLocationInView(pTouch->view());
//    lastPoint = CCDirector::sharedDirector()->convertToGL(lastPoint);
//    lastPoint = convertToNodeSpace(lastPoint);
//    
//    CCPoint translation = ccpSub(curPoint, lastPoint);
//    CCPoint cardPos = m_pCardSprite[m_iSelectedIndex]->getPosition();
//    CCPoint newPos = ccpAdd(cardPos, translation);
//    
//    m_pCardSprite[m_iSelectedIndex]->setPosition(newPos);
//}

/************************************************************************/
#pragma mark - KNScrollLayer类重写基类visit函数
/************************************************************************/
//void Layer_FollowerBonus::visit()
//{
//    // 打开opengl裁减
//    glEnable(GL_SCISSOR_TEST);
//    // 设置裁减区域，左下角为0 ,0，水平往右为x正方向，竖直向上为y正方向
//    if (g_bIsEnableRetina)
//        glScissor(16, 180, 600, 440);
//    else
//        glScissor(8, 90, 300, 220);
//    // 调用基类方法
//    CCLayer::visit();
//    // 关闭opengl裁减
//    glDisable(GL_SCISSOR_TEST);
//}