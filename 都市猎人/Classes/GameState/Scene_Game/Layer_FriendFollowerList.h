//
//  Layer_FriendFollowerList.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-5-15.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_FriendFollowerList_h
#define MidNightCity_Layer_FriendFollowerList_h

#include "KNScrollView.h"
#include "Follower.h"
#include "ServerDataManage.h"
#include "SystemDataManage.h"
#include "KNUIfunction.h"
#include "GameGuide.h"

// 好友小弟列表信息长度
const int INFO_TEXT_LENGTH = 32;

// 好友小弟列表选项信息
struct FRIEND_ITEM_INFO
{
    CCSprite* pIcon;                            // 小弟头像icon
    CCSprite* pFrame;                           // 小弟底框
    CCSprite* pLvMark;                          // 等级标签
    CCSprite* pLvBack;                          // 等级背景
    CCSprite* pRelate;                          // 关系
    CCSprite* pFriendBack;                      // 友情点背景
    
    CCSprite* pSelectButtonNormal;              // 选择按钮-普通
    CCSprite* pSelectButtonSelect;              // 选择按钮-选择
    
    CCSprite* pAddButtonNormal;                 // 添加按钮-普通
    CCSprite* pAddButtonSelect;                 // 添加按钮-选择
    
    CCLabelTTF* pFollowerName;                  // 小弟姓名
    CCLabelTTF* pFollowerLoginTime;             // 小弟登陆事件
    
    CCLabelAtlas* pFollowerLevel;               // 好友小弟等级
    CCLabelAtlas* pFriendLevel;                 // 好友等级
    CCLabelAtlas* pFriendValue;                 // 友情点
    
    int iServerID;                              // 服务器标识ID
    int iFollowerID;                            // 小弟id
    int iFollowerQuality;                       // 小弟品质
    int iLevel;                                 // 等级
    int iFollowerLevel;                         // 玩家等级
    int iHp;                                    // 生命值
    int iAttack;                                // 攻击力
    int iRecover;                               // 恢复值
    int iCharacterID;                           // 性格
    int iProfression;                           // 职业
    int iEnable;                                // 可用
    int iFriendValue;                           // 友情点
    int iMaxQuality;                            // 最大品质
    
    char szSmallIconName[32];                   // 小icon文件名
    char szIconName[32];                        // icon文件名
    char szPlayerName[32];                      // 玩家姓名
    
    FRIEND_ITEM_INFO()                          {memset(this, 0, sizeof(FRIEND_ITEM_INFO));}
};

// 列表类型
enum FRIEND_LIST_TYPE
{
    FRIEND_FRIEND = 0,                          // 好友
    FRIEND_FOLLOWER,                            // 好友小弟
    FRIEND_STRANGER,                            // 陌生好友
    FRIEND_NONE,                                // 什么都不是^_^
};

// 类定义
class Layer_FriendFollowerList : public KNScrollView
{
public:
    #pragma mark - 构造函数
    Layer_FriendFollowerList();
    #pragma mark - 析构函数
    virtual ~Layer_FriendFollowerList();
    
    #pragma mark - 初始化函数
    bool init()                                                     {return CCLayer::init();}
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 退出函数
    virtual void onExit();
    
    #pragma mark - 刷新列表函数
    void RefreshListData();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 获得被选择的小弟序号
    int GetSelectedFollowerIndex() const                            {return m_ibSelectedIndex;}
    
    #pragma mark - 显示好友小弟编辑界面
    void showFriendFollowerEditFrame(FRIEND_ITEM_INFO& stInfo);
    #pragma mark - 显示好友小弟信息界面
    void showFriendFollowerInfoFrame(FRIEND_ITEM_INFO stInfo);
    
    #pragma mark - 获得选择的小弟serverID函数
    int GetSelectedFriendID() const                                 {return m_iSelectFriendID;}
    
    #pragma mark - 刷新好友数量和好友上限函数
    void UpdateFriendValueAndLimit();
    
    #pragma mark － 使用cocos2d初始化
    LAYER_NODE_FUNC(Layer_FriendFollowerList);
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 滑动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
private:
    #pragma mark - 刷新好友列表函数
    void RefreshFriendList();
    #pragma mark - 刷新添加好友小弟列表函数
    void RefreshFriendFollowerList();
    #pragma mark - 刷新陌生人列表函数
    void RefreshStrangerList();
    
    #pragma mark - 判断是否是自己函数
    bool IsSelf(int iServerID);
    #pragma mark - 判断是否在好友列表中函数
    bool IsInFriendList(int iServerID);
private:
    // 好友小弟对象变量
    vector<FRIEND_ITEM_INFO> m_vecFriendFollowerList;               // 好友小弟列表
    
    // 好友小弟属性变量
    int m_ibSelectedIndex;
    int m_iSelectFriendID;                                          // 被选择的好友小弟ID
    
    FRIEND_LIST_TYPE m_eFriendType;                                 // 列表类型
};

#endif
