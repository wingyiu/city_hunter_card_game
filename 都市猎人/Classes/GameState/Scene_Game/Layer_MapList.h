//
//  Layer_MapList.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-5-3.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_MapList_h
#define MidNightCity_Layer_MapList_h

#include "KNScrollView.h"
#include "KNUIFunction.h"
#include "Layer_GameGuide.h"
#include "MyConfigure.h"

// 地图副本数量
const int MAX_MAP_NUM = 10;

// 地图选项信息
struct MAP_ITEM_INFO
{
    int iMapID;                             // 大地图id
    
    char szMapImageFilename[CHARLENGHT];    // 大副本文件名
    char szMapNameFilename[CHARLENGHT];     // 大副本名称
    char szSubImageFilename[3][CHARLENGHT]; // 小副本图片文件名
    
    int iSubBattleStep[3];              // 小副本战斗次数
    int iSubMapID[3];                   // 小副本id
    int iSubTime[3];                    // 小副本时间
    int iSubMoney[3];                   // 小副本金钱
    int iSubAction[3];                  // 小副本行动力
    int iSubState[3];                   // 小副本状态
    
    int iBeginTime;                     // 开启时间
    int iAction;                        // 消耗行动力
    int iMoneyAward;                    // 金钱奖励
    int iFollowerRate;                  // 小弟掉落id
    
    int iState;                         // 地图状态
    int iMark;                          // 地图标记
    
    CCSprite* pBackImage;               // 背景图片
    CCSprite* pMapName;                 // 副本名称
    CCSprite* pFrameImage;              // 边框图片
    CCSprite* pStateImage;              // 状态图片
    
    CCSprite* pRemainTimeImage;         // 剩余时间图标
    CCSprite* pMoneyImage;              // 金钱图标
    CCSprite* pTimeUnit;                // 时间单位
    
    CCSprite* pActionImage;             // 行动力图标
    CCSprite* pRate;                    // 概率
    
    CCSprite* pRemainWord;              // 剩余字
    CCSprite* pBeiWord;                 // 倍
    
    CCLabelAtlas* pNumber1;             // 显示数字1
    CCLabelAtlas* pNumber2;             // 显示数字2
    
#ifdef GAME_DEBUG
    CCLabelTTF* pMapID;                 // 地图id
#endif
    
    // 构造函数
    MAP_ITEM_INFO()                     {memset(this, 0, sizeof(MAP_ITEM_INFO));}
};

// 类定义
class Layer_MapList : public KNScrollView
{
public:
    #pragma mark - 构造函数
    Layer_MapList();
    #pragma mark - 析构函数
    virtual ~Layer_MapList();
    
    #pragma mark - 初始化函数 : 使用自动内存使用
    bool init();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 重写退出函数 : 使用自动内存使用
    virtual void onExit();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 获取大副本信息列表函数
    void GetMapInfoFromData();
    
    #pragma mark - 刷新副本列表函数
    void RefreshMapList();
    
    #pragma mark - 获得选中的地图
    int GetSelectedMapID() const                    {return m_iSelectMapID;}
    #pragma mark - 获得新地图标记
    bool GetNewMapExistMark() const                 {return m_bIsNewMapExist;}
    
    #pragma mark - 设置小副本序号
    void SetSubMapIndex(int index)                  {m_iSubSelectIndex = index;}
    #pragma mark - 获得小副本序号
    int GetSubMapIndex() const                      {return m_iSubSelectIndex;}
    
    #pragma mark - 显示小副本界面函数
    void ShowSubMissionFrame();
    
    #pragma mark - 是否可以进入关卡函数
    bool IsEnterBattleEnable(int iSubMapIndex);
    
    #pragma mark - 连接基类初始化函数
    LAYER_NODE_FUNC(Layer_MapList);
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 滑动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
private:
    // 地图列表对象变量
    CCSpriteBatchNode* m_pBatchNode;                // 精灵节点集
    
    vector<MAP_ITEM_INFO> m_vecMapItemList;         // 地图列表
    
    CCSprite* m_pSelectFrame;                       // 选择框
    
    // 地图列表属性变量
    int m_iSelectIndex;                             // 被选中的地图序号
    int m_iSubSelectIndex;                          // 小副本序号
    int m_iSelectMapID;                             // 获得选择副本id
    
    bool m_bIsNewMapExist;                          // 是否有新地图
};

#endif
