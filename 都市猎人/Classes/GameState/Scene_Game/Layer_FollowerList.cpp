//
//  Layer_FollowerList.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-4-28.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_FollowerList.h"
#include "KNUIManager.h"
#include "Follower.h"
#include "GameGuide.h"

// 小弟性格查询表，以后最好还是修改下配置表
extern const char* g_pCharacterName[];

// 小弟item行间距
const float g_FollowerRowDist = 66.0f;

// 小弟排序 - 星级
bool StarGreat(Follower* p1, Follower* p2)
{
    return p1->GetData()->Follower_Quality < p2->GetData()->Follower_Quality;
}
bool StarLess(Follower* p1, Follower* p2)
{
    return p1->GetData()->Follower_Quality > p2->GetData()->Follower_Quality;
}
// 小弟排序 - 等级
bool LevelGreat(Follower* p1, Follower* p2)
{
    return p1->GetData()->Follower_Level < p2->GetData()->Follower_Level;
}
bool LevelLess(Follower* p1, Follower* p2)
{
    return p1->GetData()->Follower_Level > p2->GetData()->Follower_Level;
}
// 小弟排序 - 攻击力
bool AttackGreat(Follower* p1, Follower* p2)
{
    return p1->GetData()->Follower_Attack < p2->GetData()->Follower_Attack;
}
bool AttackLess(Follower* p1, Follower* p2)
{
    return p1->GetData()->Follower_Attack > p2->GetData()->Follower_Attack;
}
// 小弟排序 - 血量
bool HPGreat(Follower* p1, Follower* p2)
{
    return p1->GetData()->Follower_HP < p2->GetData()->Follower_HP;
}
bool HPLess(Follower* p1, Follower* p2)
{
    return p1->GetData()->Follower_HP > p2->GetData()->Follower_HP;
}
// 小弟排序 - 回复力
bool RECOVERGreat(Follower* p1, Follower* p2)
{
    return p1->GetData()->Follower_RegulateHP < p2->GetData()->Follower_RegulateHP;
}
bool RECOVERLess(Follower* p1, Follower* p2)
{
    return p1->GetData()->Follower_RegulateHP > p2->GetData()->Follower_RegulateHP;
}

/************************************************************************/
#pragma mark - Layer_FollowerList类构造函数
/************************************************************************/
Layer_FollowerList::Layer_FollowerList()
{
    // 为小弟对象变量赋初值
    m_vecFollowerList.clear();
    
    for (int i = 0; i < 4; i++)
        m_vecTeamList.push_back(NULL);
    
    m_setTrainList.clear();
    m_setJobList.clear();
    m_setFireList.clear();
    
    m_pEditedFollower = NULL;
    m_pGuideFollower = NULL;
    
    // 为小弟属性变量赋初值
    m_eState = FOLLOWER_NONE;
    m_eLimitType = LIMIT_ALL;
    m_eFollowerAttr = FOLLOWER_ATTR_TYPE_2;
    
    m_iEditMemberIndex = 0;
    
    m_bIsButtonPressed = false;
}

/************************************************************************/
#pragma mark - Layer_FollowerList类析构函数
/************************************************************************/
Layer_FollowerList::~Layer_FollowerList()
{
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_FollowerList类初始化函数 : 使用自动内存使用
/************************************************************************/
bool Layer_FollowerList::init()
{
    return CCLayer::init();
}

/************************************************************************/
#pragma mark - Layer_FollowerList类初始化函数
/************************************************************************/
bool Layer_FollowerList::init(UIInfo& stInfo)
{
    // 初始化控件
    KNScrollView::init(stInfo);
    
    // 设置可视区域
    SetVisibleRange(CCRect(stInfo.point.x, stInfo.point.y, stInfo.size.width, stInfo.size.height));
    
    // 保存类型
    m_eUIType = stInfo.eType;
    
    // 设置可见属性
    setIsVisible(stInfo.bIsVisible);
    
    // 设置滚动条x偏移位置
    KNScrollView::SetHorizontalSliderX(138.0f);
    
    // 打开滚动条
    KNScrollView::SetSliderEnable(true);
    
    // 设置选项尺寸
    KNScrollView::SetItemSize(CCSize(320.0f, 60.0f));
    
    // 刷新队伍列表
    RefreshFollowerTeamList();
    // 刷新列表
    RefreshFollowerList();
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_FollowerList类销毁函数
/************************************************************************/
void Layer_FollowerList::Destroy()
{
    // 移出所有小弟图层
    vector<Follower*>::iterator iter;
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
        removeChild(*iter, true);
    m_vecFollowerList.clear();
}

/************************************************************************/
#pragma mark - Layer_FollowerList类重写退出函数 : 使用自动内存使用
/************************************************************************/
void Layer_FollowerList::onExit()
{
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_FollowerList类重写可见函数－打开可见并激活事件函数
/************************************************************************/
void Layer_FollowerList::setIsVisible(bool bIsVisible)
{
    // 调用基类函数
    CCLayer::setIsVisible(bIsVisible);
    
    // 关闭对话框，根据不同状态进行处理
    if (!bIsVisible)
    {
        switch (m_eState) {
            case FOLLOWER_TEAM:     // 队伍编辑
                KNUIFunction::GetSingle()->OpenFrameByNormalType(FOLLOWER_INFO_FRAME_ID);
                break;
            case FOLLOWER_TRAIN:    // 英雄训练
                break;
            case FOLLOWER_TEAM_COMPLETE:// 队伍编辑完成
            case PACKAGE_CHECK:     // 英雄背包
            case FOLLOWER_FIRE:     // 英雄售卖
                // 打开英雄中心
                KNUIFunction::GetSingle()->OpenFrameByNormalType(FOLLOWER_CENTER_FRAME_ID);
                break;
            default:
                break;
        }
        
        // 复原列表类型
        m_eState = FOLLOWER_NONE;
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类刷新小弟列表
/************************************************************************/
void Layer_FollowerList::RefreshFollowerList()
{
    // 移出所有小弟图层
    vector<Follower*>::iterator iter;
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
        removeChild(*iter, false);
    m_vecFollowerList.clear();
    
    // 重新获得小弟信息，计算列表高度
    int num = 0;
    float height = 0.0f;
    PlayerFollowerDepot followerList = *PlayerDataManage::ShareInstance()->GetMyFollowerDepot();
    for (PlayerFollowerDepot::iterator iter = followerList.begin(); iter != followerList.end(); iter++)
        num += iter->second.size();
    
    // 计算高度
    num += 1;       // 加一个空位，不然训练和解雇的时候，小弟选项被界面遮挡
    height = num * g_FollowerRowDist;
    
    // 设置列表尺寸
    KNScrollView::SetScrollSize(ccp(320.0f, height));
    
    // 小弟第一个是高度的下一行开始的，减少一行高度
    height -= g_FollowerRowDist;
    
    // 刷新列表
    float x = 0;
    float y = 0;
    for (PlayerFollowerDepot::iterator iter = followerList.begin(); iter != followerList.end(); iter++)
    {
        list <Follower*>::iterator it2;
        for (it2 = iter->second.begin(); it2 != iter->second.end(); it2++)
        {
            Follower* pFollower = dynamic_cast<Follower*>(*it2);
            if (pFollower != NULL)
            {
                // 刷新数据
                pFollower->RefreshDataToUI();
                
                // 计算位置
                x = 0.0f;
                y = height;
                
                // 设置小弟位置
                pFollower->setPosition(ccp(x, y));
                addChild(pFollower);
                
                // 打开小弟icon和边框
                pFollower->getChildByTag(FollowerChild_Icon)->setIsVisible(true);
                pFollower->getChildByTag(FollowerChild_IconFrame)->setIsVisible(true);
                
                // 添加小弟到列表
                m_vecFollowerList.push_back(pFollower);
                
                // 增加序列号
                height -= g_FollowerRowDist;
            }
        }
    }
    
    // 筛选下小弟
    ScreenFollowerList(LIMIT_ALL);
    
    // 滑动到顶端
    KNScrollView::SetPoint(true);
    
    // 更新数字信息
    KNUIFunction::GetSingle()->UpdatePlatformInfo();
}

/************************************************************************/
#pragma mark - Layer_FollowerList类筛选小弟列表
/************************************************************************/
void Layer_FollowerList::ScreenFollowerList(LIMIT_TYPE eType)
{
    // 记录当前筛选类型
    m_eLimitType = eType;
    
    // 获得列表高度
    float height = 0.0f;
    vector<Follower*>::iterator iter;
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
    {
        if (eType != LIMIT_ALL && (*iter)->GetData()->Follower_Profession != eType)
            continue;
        
        height += g_FollowerRowDist;
    }
    
    height += g_FollowerRowDist;
    
    // 设置列表高度
    KNScrollView::SetScrollSize(ccp(160.0f, height));
    height = KNScrollView::GetScrollSize().y - g_FollowerRowDist;
    
    // 刷新列表 - 队员优先
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
    {
        // 过滤不需要显示的小弟
        if (eType != LIMIT_ALL && (*iter)->GetData()->Follower_Profession != eType)
        {
            (*iter)->setIsVisible(false);
            continue;
        }
        
        // 是否队员，不是则跳过
        if (IsFollowerInBattleList(*iter) == false)
            continue;
        
        // 显示出来
        (*iter)->setIsVisible(true);
        
        // 设置小弟位置
        (*iter)->setPosition(ccp(0.0f, height));
        
        // 更新位置
        height -= g_FollowerRowDist;
    }
    
    // 刷新列表 - 除队员外小弟
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
    {
        // 过滤不需要显示的小弟
        if (eType != LIMIT_ALL && (*iter)->GetData()->Follower_Profession != eType)
        {
            (*iter)->setIsVisible(false);
            continue;
        }
        
        // 是否队员，不是则跳过
        if (IsFollowerInBattleList(*iter) == true)
            continue;
        
        // 显示出来
        (*iter)->setIsVisible(true);
        
        // 设置小弟位置
        (*iter)->setPosition(ccp(0.0f, height));
        
        // 更新位置
        height -= g_FollowerRowDist;
        
        // 接受一个作为引导小弟
        m_pGuideFollower = *iter;
    }
    
    // 回到列表顶端
    KNScrollView::SetPoint(true);
}

/************************************************************************/
#pragma mark - Layer_FollowerList类排序小弟列表
/************************************************************************/
void Layer_FollowerList::SortFollowerList(SORT_TYPE eType)
{
    // 排序列表
    switch (eType) {
        case SORT_STAR_GREAT:               // 星级排序－升序
            sort(m_vecFollowerList.begin(), m_vecFollowerList.end(), StarGreat);
            break;
        case SORT_STAR_LESS:                // 星级排序－降序
            sort(m_vecFollowerList.begin(), m_vecFollowerList.end(), StarLess);
            break;
        case SORT_LEVEL_GREAT:              // 等级排序－升序
            sort(m_vecFollowerList.begin(), m_vecFollowerList.end(), LevelGreat);
            break;
        case SORT_LEVEL_LESS:               // 等级排序－降序
            sort(m_vecFollowerList.begin(), m_vecFollowerList.end(), LevelLess);
            break;
        case SORT_ATTACK_GREAT:             // 攻击力排序－升序
            sort(m_vecFollowerList.begin(), m_vecFollowerList.end(), AttackGreat);
            break;
        case SORT_ATTACK_LESS:              // 攻击力排序－降序
            sort(m_vecFollowerList.begin(), m_vecFollowerList.end(), AttackLess);
            break;
        case SORT_HP_GREAT:                 // hp排序－升序
            sort(m_vecFollowerList.begin(), m_vecFollowerList.end(), HPGreat);
            break;
        case SORT_HP_LESS:                  // hp排序－降序
            sort(m_vecFollowerList.begin(), m_vecFollowerList.end(), HPLess);
            break;
        case SROT_RECOVER_GREAT:            // 回复力排序－升序
            sort(m_vecFollowerList.begin(), m_vecFollowerList.end(), RECOVERGreat);
            break;
        case SORT_RECOVER_LESS:             // 回复力排序－降序
            sort(m_vecFollowerList.begin(), m_vecFollowerList.end(), RECOVERLess);
            break;
    }
    
    // 重新筛选列表
    ScreenFollowerList(m_eLimitType);
}

/************************************************************************/
#pragma mark - Layer_FollowerList类刷新小弟队伍列表函数
/************************************************************************/
void Layer_FollowerList::RefreshFollowerTeamList()
{
    // 获得队伍列表
    PlayerFollowerTeam battleList = *PlayerDataManage::ShareInstance()->GetMyFollowerTeam();
    PlayerFollowerTeam::iterator iter;
    int index = 0;
    for (iter = battleList.begin(); iter != battleList.end(); iter++)
        m_vecTeamList[index++] = iter->second;
}

/************************************************************************/
#pragma mark - Layer_FollowerList类刷新小弟位置函数
/************************************************************************/
void Layer_FollowerList::RefreshFollowerPosition()
{
    return;
    // 排序辅助变量
    int index = 0;
    float x = 0;
    float y = 0;
    
    // 获得列表高度
    float height = KNScrollView::GetScrollSize().y - 60.0f;
    
    // 先排序队伍位置，要求为第一排显示
    vector<Follower*>::iterator teamIter;
    for (teamIter = m_vecTeamList.begin(); teamIter != m_vecTeamList.end(); teamIter++)
    {
        // 如果小弟非法，跳过
        if (*teamIter == NULL)
            continue;
        
        // 计算位置
        x = (index % 4) * 70.0f - 105.0f;
        y = height - (index / 4) * 60.0f;
        
        // 设置位置
        (*teamIter)->setPosition(ccp(x, y));
        
        // 递增序号
        ++index;
    }
    
    // 刷新其他小弟位置
    vector<Follower*>::iterator otherIter;
    for (otherIter = m_vecFollowerList.begin(); otherIter != m_vecFollowerList.end(); otherIter++)
    {
        // 再队伍中，则不进行排序
        if (IsFollowerInBattleList(*otherIter) == true)
            continue;
        
        // 计算位置
        x = (index % 4) * 70.0f - 105.0f;
        y = height - (index / 4) * 60.0f;
        
        (*otherIter)->setPosition(ccp(x, y));
        
        // 递增序号
        ++index;
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类设置列表状态函数
/************************************************************************/
void Layer_FollowerList::SetListState(FOLLOWER_LIST_STATE eState)
{
    // 接受改变
    m_eState = eState;
    
    // 刷新选项卡按钮
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        KNTabControl* pTabControl = dynamic_cast<KNTabControl*>(pFrame->GetSubUIByID(60000));
        if (pTabControl != NULL)
            pTabControl->SetTabEnable(m_eLimitType);
    }
    
    // 关闭列表所有信息ui
    SetListTrainInfoDisplay(false);
    SetListFireInfoDisplay(false);
    
    // 还原小弟显示
    vector<Follower*>::iterator iter;
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
    {
        // 关闭小弟选择标记
        (*iter)->getChildByTag(FollowerChild_SelectFrame)->setIsVisible(false);
        
        // 小弟是否可用
        (*iter)->SetFollowerEnableMark(true);
        
        // 打开队伍标记
        if (IsFollowerInBattleList(*iter) == true)
            (*iter)->getChildByTag(FollowerChild_TeamTag)->setIsVisible(true);
    }
    
    // 根据改变处理
    if (m_eState == FOLLOWER_TEAM)
    {
        vector<Follower*>::iterator iter;
        for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
        {
            // 设置显示样式
            (*iter)->SetFollowerAttrShowType(m_eFollowerAttr, false);
            
            // 队伍中的半透明
            if (IsFollowerInBattleList(*iter))
                (*iter)->SetFollowerEnableMark(false);
        }
    }
    else if (m_eState == PACKAGE_CHECK)
    {
        vector<Follower*>::iterator iter;
        for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
        {
            // 设置显示样式
            (*iter)->SetFollowerAttrShowType(m_eFollowerAttr, false);
        }
    }
    else if (m_eState == FOLLOWER_FIRE)
    {
        // 打开解雇信息
        SetListFireInfoDisplay(true);
        
        // 清空解雇列表
        clearFireList();
        
        vector<Follower*>::iterator iter;
        for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
        {
            // 设置显示样式
            (*iter)->SetFollowerAttrShowType(m_eFollowerAttr, true);
            
            // 队伍中的半透明
            if (IsFollowerInBattleList(*iter) == true)
                (*iter)->SetFollowerEnableMark(false);
        }
    }
    else if (m_eState == FOLLOWER_TRAIN)
    {
        // 打开训练信息
        SetListTrainInfoDisplay(true);
        
        // 清空训练列表
        clearTrainList();
        
        vector<Follower*>::iterator iter;
        for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
        {
            // 设置显示样式
            (*iter)->SetFollowerAttrShowType(m_eFollowerAttr, true);
            
            // 队伍中的半透明
            if (IsFollowerInBattleList(*iter) == true)
                (*iter)->SetFollowerEnableMark(false);
            else
            {
                if (*iter != m_pEditedFollower)
                    m_pGuideFollower = *iter;
            }
            
            // 打开选择标记
            if (IsFollowerInTrainList(*iter) == true)
                (*iter)->getChildByTag(FollowerChild_SelectFrame)->setIsVisible(true);
        }
        
        // 自己半透明
        m_pEditedFollower->SetFollowerEnableMark(false);
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类设置小弟成员函数
/************************************************************************/
void Layer_FollowerList::setFollowerMember(Follower* pFollower, int index)
{
    if (index >= 0 && index < TeamMaxNum)
    {
        // 是否在队伍中
        vector<Follower*>::iterator iter;
        int i = 0;
        for (iter = m_vecTeamList.begin(); iter != m_vecTeamList.end(); iter++)
        {
            if (*iter == pFollower)
            {
                Follower* pTemp = m_vecTeamList[index];
                m_vecTeamList[index] = pFollower;
                m_vecTeamList[i] = pTemp;
                
                return;
            }
            
            ++i;
        }
        
        // 关闭原队伍队员标记
        if (m_vecTeamList[index] != NULL)
            m_vecTeamList[index]->getChildByTag(FollowerChild_TeamTag)->setIsVisible(false);
        
        // 设置新队员
        m_vecTeamList[index] = pFollower;
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类清空小弟队伍列表函数
/************************************************************************/
void Layer_FollowerList::clearFollowerTeam()
{
    for (int i = 0; i < TeamMaxNum; i++)
        m_vecTeamList[i] = NULL;
}

/************************************************************************/
#pragma mark - Layer_FollowerList类清空训练列表函数
/************************************************************************/
void Layer_FollowerList::clearTrainList()
{
    // 关闭选项标记
    set<Follower*>::iterator iter;
    for (iter = m_setTrainList.begin(); iter != m_setTrainList.end(); iter++)
        (*iter)->getChildByTag(FollowerChild_SelectFrame)->setIsVisible(false);
    
    // 清空列表
    m_setTrainList.clear();
    
    // 计算界面
    CaleTrainInfo();
}

/************************************************************************/
#pragma mark - Layer_FollowerList类清空解雇列表函数
/************************************************************************/
void Layer_FollowerList::clearFireList()
{
    // 关闭选项标记
    set<Follower*>::iterator iter;
    for (iter = m_setFireList.begin(); iter != m_setFireList.end(); iter++)
        (*iter)->getChildByTag(FollowerChild_SelectFrame)->setIsVisible(false);
    
    // 清空列表
    m_setFireList.clear();
    
    // 计算界面
    CaleFireInfo();
}

/************************************************************************/
#pragma mark - Layer_FollowerList类设置小弟属性显示函数
/************************************************************************/
void Layer_FollowerList::SetFollowerAttrShowType(FOLLOWER_ATTR_SHOW_TYPE eType)
{
    m_eFollowerAttr = eType;
    
    // 显示显示模式
    vector<Follower*>::iterator iter;
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
    {
        if (m_eState == FOLLOWER_TRAIN || m_eState == FOLLOWER_FIRE)
            (*iter)->SetFollowerAttrShowType(m_eFollowerAttr, true);
        else
            (*iter)->SetFollowerAttrShowType(m_eFollowerAttr, false);
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类显示小弟信息函数
/************************************************************************/
void Layer_FollowerList::ShowFollowerInfo(Follower* pFollower)
{
    // 接受被点击的小弟
    m_pEditedFollower = pFollower;
    
    // 获得小弟信息界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        // 辅助变量
        char szTemp[32] = {0};
        
        // 刷新小弟背景
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "follower_quality_%d.png", pFollower->GetData()->Follower_Quality);
            pLabel->resetLabelImage(szTemp);
        }
        
        // 刷笑小弟头像
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->resetLabelImage(pFollower->GetData()->Follower_FightIconName);
        
        // 刷新小弟外框
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "follower_bigFrame_%d.png", pFollower->GetData()->Follower_Profession);
            pLabel->resetLabelImage(szTemp);
        }
        
        // 刷新小弟等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_Level);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟生命值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_HP);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟攻击力
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Attack);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟恢复值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Comeback);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟性格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009));
        if (pLabel != NULL)
        {
            int iCharacterID = pFollower->GetData()->Follower_CharacterID;
            const Character_Data* pData = SystemDataManage::ShareInstance()->GetData_ForCharacter(iCharacterID);
            if (pData != NULL)
            {
                // 查询性格序号
                int index = 0;
                for (int i = 0; i < 7; i++)
                {
                    if (strcmp(pData->Character_Name, g_pCharacterName[i]) == 0)
                    {
                        index = i + 1;
                        break;
                    }
                }
                
                // 生命文件名
                if (index <= 7)
                {
                    sprintf(szTemp, "character_%d.png", index);
                    pLabel->resetLabelImage(szTemp);
                }
            }
        }
        
        // 刷新星级
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_Quality)
                    pLabel->GetBackSprite()->setIsVisible(true);
                else
                    pLabel->GetBackSprite()->setIsVisible(false);
            }
        }
        
        // 刷新星星背景
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20022 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_MaxQuality)
                    pLabel->GetBackSprite()->setIsVisible(true);
                else
                    pLabel->GetBackSprite()->setIsVisible(false);
            }
        }
        
        // 刷新主动技能
        const SkillData* pData = SystemDataManage::ShareInstance()->GetData_ForSkill(pFollower->GetData()->Follower_CommonlySkillType, pFollower->GetData()->Follower_CommonlySkillID);
        if (pData != NULL)
        {
            // skill back
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20018));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(true);
            
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20019));
            if (pLabel != NULL)
                pLabel->resetLabelImage(pData->Skill_IconName);
            
            // level
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20020));
            if (pLabel != NULL)
            {
                sprintf(szTemp, "%d", pFollower->GetData()->Follower_SkillLevel);
                pLabel->GetNumberLabel()->setIsVisible(true);
                pLabel->GetNumberLabel()->setString(szTemp);
            }
        }
        else
        {
            // skill back
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20018));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(false);
            
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20019));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(false);
            
            // level
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20020));
            if (pLabel != NULL)
                pLabel->GetNumberLabel()->setIsVisible(false);
        }
        
        // 刷新小弟姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20021));
        if (pLabel != NULL)
            pLabel->setLabelText(pFollower->GetData()->Follower_Name, NULL, 0);
        
        // 训练
        KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30001));
        if (pButton != NULL)
        {
            if (pFollower->GetData()->Follower_Level >= pFollower->GetData()->Follower_MaxLevel)
                pButton->setButtonEnable(false);
            else
                pButton->setButtonEnable(true);
        }
        
        // 转职
        pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30002));
        if (pButton != NULL)
        {
            if (pFollower->GetData()->Follower_EvolvementID == 0)
                pButton->setButtonEnable(false);
            else
                pButton->setButtonEnable(true);
        }
        
        // 根据是否队伍编辑来决定按钮可见
        pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30004));
        if (pButton != NULL)
        {
            if (IsFollowerInBattleList(pFollower) == false)
            {
                // 使替换按钮不可用
                pButton->setButtonEnable(false);
            }
            else
            {
                // 使替换按钮不可用
                pButton->setButtonEnable(true);
            }
        }
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类显示小弟信息函数
/************************************************************************/
void Layer_FollowerList::ShowFollowerInfo(int iTeamIndex)
{
    if (iTeamIndex < 0 || iTeamIndex > 3)
        return;
    
    if (m_vecTeamList[iTeamIndex] != NULL)
        ShowFollowerInfo(m_vecTeamList[iTeamIndex]);
}

/************************************************************************/
#pragma mark - Layer_FollowerList类显示小弟详细信息函数
/************************************************************************/
void Layer_FollowerList::ShowFollowerFullInfo(Follower* pFollower)
{
    // 刷新小弟详细信息界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_FULLINFO_FRAME_ID);
    if (pFrame != NULL)
    {
        // 辅助变量
        char szTemp[32] = {0};
        
        // 刷新小弟背景
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "follower_quality_%d.png", pFollower->GetData()->Follower_Quality);
            pLabel->resetLabelImage(szTemp);
        }
        
        // 刷笑小弟头像
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->resetLabelImage(pFollower->GetData()->Follower_FightIconName);
        
        // 刷新小弟外框
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "follower_bigFrame_%d.png", pFollower->GetData()->Follower_Profession);
            pLabel->resetLabelImage(szTemp);
        }
        
        // 刷新小弟等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_Level);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟生命值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_HP);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟攻击力
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Attack);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟恢复值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Comeback);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新小弟性格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009));
        if (pLabel != NULL)
        {
            int iCharacterID = pFollower->GetData()->Follower_CharacterID;
            const Character_Data* pData = SystemDataManage::ShareInstance()->GetData_ForCharacter(iCharacterID);
            if (pData != NULL)
            {
                // 查询性格序号
                int index = 0;
                for (int i = 0; i < 7; i++)
                {
                    if (strcmp(pData->Character_Name, g_pCharacterName[i]) == 0)
                    {
                        index = i + 1;
                        break;
                    }
                }
                
                // 生命文件名
                if (index <= 7)
                {
                    sprintf(szTemp, "character_%d.png", index);
                    pLabel->resetLabelImage(szTemp);
                }
            }
        }
        
        // 刷新主动技能
        const SkillData* pMainData = SystemDataManage::ShareInstance()->GetData_ForSkill(pFollower->GetData()->Follower_CommonlySkillType, pFollower->GetData()->Follower_CommonlySkillID);
        if (pMainData != NULL)
        {
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011));
            if (pLabel != NULL)
            {
                pLabel->GetBackSprite()->setIsVisible(true);
                pLabel->resetLabelImage(pMainData->Skill_IconName);
            }
            
            // 技能描述
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012));
            if (pLabel != NULL)
                pLabel->setLabelText(pMainData->Skill_Depict, NULL, 0);
            
            // 技能名字
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20025));
            if (pLabel != NULL)
                pLabel->setLabelText(pMainData->Skill_Name, NULL, 0);
            
            // 技能标记
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20033));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(true);
            
            // 刷新技能等级
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023));
            if (pLabel != NULL)
            {
                sprintf(szTemp, "%d", pFollower->GetData()->Follower_SkillLevel);
                pLabel->GetNumberLabel()->setIsVisible(true);
                pLabel->GetNumberLabel()->setString(szTemp);
            }
        }
        else
        {
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(false);
            
            // 技能描述
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012));
            if (pLabel != NULL)
                pLabel->setLabelText("无", NULL, 0);
            
            // 技能名字
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20025));
            if (pLabel != NULL)
                pLabel->setLabelText("无", NULL, 0);
            
            // 技能标记
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20033));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(false);
            
            // 刷新技能等级
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023));
            if (pLabel != NULL)
                pLabel->GetNumberLabel()->setIsVisible(false);
        }
        
        // 刷新队长技能
        const SkillData* pTeamData = SystemDataManage::ShareInstance()->GetData_ForSkill(pFollower->GetData()->Follower_TeamSkillType, pFollower->GetData()->Follower_TeamSkillID);
        if (pTeamData != NULL)
        {
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20013));
            if (pLabel != NULL)
            {
                pLabel->GetBackSprite()->setIsVisible(true);
                pLabel->resetLabelImage(pTeamData->Skill_IconName);
            }
            
            // 技能描述
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20014));
            if (pLabel != NULL)
                pLabel->setLabelText(pTeamData->Skill_Depict, NULL, 0);
            
            // 技能名字
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20026));
            if (pLabel != NULL)
                pLabel->setLabelText(pTeamData->Skill_Name, NULL, 0);
        }
        else
        {
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20013));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(false);
            
            // 技能描述
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20014));
            if (pLabel != NULL)
                pLabel->setLabelText("无", NULL, 0);
            
            // 技能名字
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20026));
            if (pLabel != NULL)
                pLabel->setLabelText("无", NULL, 0);
        }
        
        // 刷新最大等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_MaxLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新下一级所需经验
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20016));
        if (pLabel != NULL)
        {
            if (pFollower->GetData()->Follower_Level >= pFollower->GetData()->Follower_MaxLevel)
                sprintf(szTemp, "%d", 0);
            else
                sprintf(szTemp, "%d", pFollower->GetData()->Follower_LevelUpExperience);
            
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新星级20028
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20018 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_Quality)
                    pLabel->GetBackSprite()->setIsVisible(true);
                else
                    pLabel->GetBackSprite()->setIsVisible(false);
            }
        }
        
        // 刷新星星背景
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20028 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_MaxQuality)
                    pLabel->GetBackSprite()->setIsVisible(true);
                else
                    pLabel->GetBackSprite()->setIsVisible(false);
            }
        }
        
        // 刷新小弟姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20027));
        if (pLabel != NULL)
            pLabel->setLabelText(pFollower->GetData()->Follower_Name, NULL, 0);
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类显示小弟训练信息函数
/************************************************************************/
void Layer_FollowerList::ShowFollowerTrainInfo(Follower* pFollower, bool bIsRefreshSuccessData)
{
    // 小弟非法则退出
    if (pFollower == NULL)
        return;
    
    // 信息字符串
    char szTemp[32] = {0};
    
    // 获得相应的小弟信息界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        // 刷新小弟背景
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20001));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "follower_quality_%d.png", pFollower->GetData()->Follower_Quality);
            pLabel->resetLabelImage(szTemp);
        }
        
        // 刷新小弟头像
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->resetLabelImage(pFollower->GetData()->Follower_FightIconName);
        
        // 刷新小弟外框
        //        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        //        if (pLabel != NULL)
        //        {
        //            sprintf(szTemp, "follower_bigFrame_%d.png", pFollower->GetData()->Follower_Profession);
        //            pLabel->resetLabelImage(szTemp);
        //        }
        
        // 刷新小弟等级
        sprintf(szTemp, "%d", pFollower->GetData()->Follower_Level);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新小弟生命值
        sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_HP);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新小弟攻击值
        sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Attack);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新小弟恢复值
        sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Comeback);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新小弟性格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009));
        if (pLabel != NULL)
        {
            int iCharacterID = pFollower->GetData()->Follower_CharacterID;
            const Character_Data* pData = SystemDataManage::ShareInstance()->GetData_ForCharacter(iCharacterID);
            if (pData != NULL)
            {
                // 查询性格序号
                int index = 0;
                for (int i = 0; i < 7; i++)
                {
                    if (strcmp(pData->Character_Name, g_pCharacterName[i]) == 0)
                    {
                        index = i + 1;
                        break;
                    }
                }
                
                // 生命文件名
                if (index <= 7)
                {
                    sprintf(szTemp, "character_%d.png", index);
                    pLabel->resetLabelImage(szTemp);
                }
            }
        }
        
        // 刷新当前经验
        sprintf(szTemp, "%d", pFollower->GetData()->Follower_Experience);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新需求经验
        sprintf(szTemp, "%d", pFollower->GetData()->Follower_LevelUpExperience);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新消耗金币
        //int iPrice = int((1.4 * pow(pFollower->GetData()->Follower_Level, 3) - 3 * pow(pFollower->GetData()->Follower_Level, 2) + 10 * pFollower->GetData()->Follower_Level + 192) * pFollower->GetData()->Follower_RegulateAbsorbPrice / 2);
        int iPrice = int((pFollower->GetData()->Follower_Level * 100) * pFollower->GetData()->Follower_RegulateAbsorbPrice);
        iPrice = iPrice * (m_setTrainList.size());
        sprintf(szTemp, "%d", iPrice);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20013));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新获得经验
        set<Follower*>::iterator iter;
        int iExp = 0;
        for (iter = m_setTrainList.begin(); iter != m_setTrainList.end(); iter++)
            iExp += (*iter)->GetData()->Follower_BeAbsorbExperience;
        sprintf(szTemp, "%d", iExp);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20014));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新星级
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20030 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_Quality)
                    pLabel->GetBackSprite()->setIsVisible(true);
                else
                    pLabel->GetBackSprite()->setIsVisible(false);
            }
        }
        
        // 刷新星星背景
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20035 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_MaxQuality)
                    pLabel->GetBackSprite()->setIsVisible(true);
                else
                    pLabel->GetBackSprite()->setIsVisible(false);
            }
        }
        
        // 刷新小弟姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20022));
        if (pLabel != NULL)
            pLabel->setLabelText(pFollower->GetData()->Follower_Name, NULL, 0);
        
        // 刷新主动技能icon
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20041));
        if (pLabel != NULL)
        {
            const SkillData* pData = SystemDataManage::ShareInstance()->GetData_ForSkill(pFollower->GetData()->Follower_CommonlySkillType, pFollower->GetData()->Follower_CommonlySkillID);
            if (pData != NULL)
                pLabel->resetLabelImage(pData->Skill_IconName);
        }
        
        //        // 刷新主动技能描述
        //        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20041));
        //        if (pLabel != NULL)
        //        {
        //            const SkillData* pData = SystemDataManage::ShareInstance()->GetData_ForSkill(pFollower->GetData()->Follower_TeamSkillType, pFollower->GetData()->Follower_TeamSkillID);
        //            if (pData != NULL)
        //                pLabel->resetLabelImage(pData->Skill_IconName);
        //        }
        
        // 刷新技能等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20042));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_SkillLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
    
    // 刷新训练成功界面，只刷新训练前的数据
    if (!bIsRefreshSuccessData)
        return;
    
    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_SUCCESS_FRAME_ID);
    if (pFrame != NULL)
    {
        // 刷新小弟头像
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->resetLabelImage(pFollower->GetData()->Follower_FightIconName);
        
        // 等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_Level);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // hp
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_HP);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 攻击力
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Attack);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 恢复值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Comeback);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 性格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009));
        if (pLabel != NULL)
        {
            int iCharacterID = pFollower->GetData()->Follower_CharacterID;
            const Character_Data* pData = SystemDataManage::ShareInstance()->GetData_ForCharacter(iCharacterID);
            if (pData != NULL)
            {
                // 查询性格序号
                int index = 0;
                for (int i = 0; i < 7; i++)
                {
                    if (strcmp(pData->Character_Name, g_pCharacterName[i]) == 0)
                    {
                        index = i + 1;
                        break;
                    }
                }
                
                // 生命文件名
                if (index <= 7)
                {
                    sprintf(szTemp, "character_%d.png", index);
                    pLabel->resetLabelImage(szTemp);
                }
            }
        }
        
        // 刷新星级
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_Quality)
                    pLabel->setIsVisible(true);
                else
                    pLabel->setIsVisible(false);
            }
        }
        
        // 刷新小弟姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20027));
        if (pLabel != NULL)
            pLabel->setLabelText(pFollower->GetData()->Follower_Name, NULL, 0);
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类显示小弟转职信息函数
/************************************************************************/
void Layer_FollowerList::ShowFollowerJobInfo(Follower* pFollower, bool bIsRefreshSuccessData)
{
    // 非法小弟，则退出
    if (pFollower == NULL)
        return;
    
    // 信息字符串
    char szTemp[32] = {0};
    
    // 获得转职界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_JOB_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        // 刷新小弟头像
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->resetLabelImage(pFollower->GetData()->Follower_FightIconName);
        
        // 刷新小弟外框
        
        // 刷新小弟底图
        
        // 刷新小弟等级
        sprintf(szTemp, "%d", pFollower->GetData()->Follower_Level);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新小弟生命值
        sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_HP);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新小弟攻击值
        sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Attack);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新小弟恢复值
        
        // 刷新小弟性格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009));
        if (pLabel != NULL)
        {
            int iCharacterID = pFollower->GetData()->Follower_CharacterID;
            const Character_Data* pData = SystemDataManage::ShareInstance()->GetData_ForCharacter(iCharacterID);
            if (pData != NULL)
            {
                // 查询性格序号
                int index = 0;
                for (int i = 0; i < 7; i++)
                {
                    if (strcmp(pData->Character_Name, g_pCharacterName[i]) == 0)
                    {
                        index = i + 1;
                        break;
                    }
                }
                
                // 生命文件名
                if (index <= 7)
                {
                    sprintf(szTemp, "character_%d.png", index);
                    pLabel->resetLabelImage(szTemp);
                }
            }
        }
        
        // 刷新星级
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20030 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_Quality)
                    pLabel->GetBackSprite()->setIsVisible(true);
                else
                    pLabel->GetBackSprite()->setIsVisible(false);
            }
        }
        
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20035 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_MaxQuality)
                    pLabel->GetBackSprite()->setIsVisible(true);
                else
                    pLabel->GetBackSprite()->setIsVisible(false);
            }
        }
        
        // 刷新小弟姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20022));
        if (pLabel != NULL)
            pLabel->setLabelText(pFollower->GetData()->Follower_Name, NULL, 0);
        
        // 刷新转职金钱
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_EvolvementPrice);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新主动技能
        const SkillData* pData = SystemDataManage::ShareInstance()->GetData_ForSkill(pFollower->GetData()->Follower_CommonlySkillType, pFollower->GetData()->Follower_CommonlySkillID);
        if (pData != NULL)
        {
            // skill back
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20040));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(true);
            
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20041));
            if (pLabel != NULL)
            {
                pLabel->GetBackSprite()->setIsVisible(true);
                pLabel->resetLabelImage(pData->Skill_IconName);
            }
            
            // level
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20042));
            if (pLabel != NULL)
            {
                pLabel->GetNumberLabel()->setIsVisible(true);
                sprintf(szTemp, "%d", pData->Skill_Level);
                pLabel->GetNumberLabel()->setString(szTemp);
            }
        }
        else
        {
            // skill back
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20040));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(false);
            
            // icon
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20041));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(false);
            
            // level
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20042));
            if (pLabel != NULL)
                pLabel->GetNumberLabel()->setIsVisible(false);
        }
        
        // 存在标记
        bool bIsExist = false;
        
        // 刷新小弟转职需要的小弟列表
        for (int i = 0; i < 5; i++)
        {
            // 刷新小弟图标
            int id = m_pEditedFollower->GetData()->Follower_EvolvementFollowerID[i];
            if (id == 0)
            {
                // icon
                KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
                if (pLabel != NULL)
                    pLabel->GetBackSprite()->setIsVisible(false);
                
                // frame
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20024 + i));
                if (pLabel != NULL)
                    pLabel->GetBackSprite()->setIsVisible(false);
                
                continue;
            }
            
            // 重置存在标记
            bIsExist = false;
            
            // 找一个这个id的小弟
            for (int j = 0; j < m_vecFollowerList.size(); j++)
            {
                if (m_vecFollowerList[j]->GetData()->Follower_ID == id)
                {
                    // 如果是自身，则跳过
                    if (m_vecFollowerList[j] == m_pEditedFollower)
                        continue;
                    
                    // 是否在队伍中
                    if (IsFollowerInBattleList(m_vecFollowerList[j]) == false)
                    {
                        m_setJobList.insert(m_vecFollowerList[j]);
                        
                        // icon
                        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
                        if (pLabel != NULL)
                        {
                            pLabel->GetBackSprite()->setIsVisible(true);
                            pLabel->GetBackSprite()->setOpacity(255);
                            pLabel->resetLabelImage(m_vecFollowerList[j]->GetData()->Follower_IconName);
                        }
                        
                        // frame
                        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20024 + i));
                        if (pLabel != NULL)
                        {
                            char szTemp[32] = {0};
                            sprintf(szTemp, "FollowerIcon_%d.png", m_vecFollowerList[j]->GetData()->Follower_Profession);
                            
                            pLabel->GetBackSprite()->setIsVisible(true);
                            pLabel->GetBackSprite()->setOpacity(255);
                            pLabel->resetLabelImage(szTemp);
                        }
                        
                        // 设置存在标记
                        bIsExist = true;
                        
                        break;
                    }
                }
            }
            
            // 存在了则跳过
            if (bIsExist)
                continue;
            
            // 获得小弟信息
            const Follower_Data * data = SystemDataManage::ShareInstance()->GetData_ForFollower(id);
            if (data != NULL)
            {
                // icon
                KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
                if (pLabel != NULL)
                {
                    pLabel->resetLabelImage(data->Follower_IconName);
                    pLabel->GetBackSprite()->setIsVisible(true);
                    pLabel->GetBackSprite()->setOpacity(100);
                }
                
                // frame
                pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20024 + i));
                if (pLabel != NULL)
                {
                    char szTemp[32] = {0};
                    sprintf(szTemp, "FollowerIcon_%d.png", data->Follower_Profession);
                    pLabel->resetLabelImage(szTemp);
                    pLabel->GetBackSprite()->setIsVisible(true);
                    pLabel->GetBackSprite()->setOpacity(100);
                }
                
                continue;
            }
            
            // 没有找到，关闭icon
            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(false);
            
            // frame
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20024 + i));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(false);
        }
    }
    
    // 刷新专职成功界面，只刷新转职前的数据
    if (!bIsRefreshSuccessData)
        return;
    
    pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_JOB_SUCCESS_FRAME_ID);
    if (pFrame != NULL)
    {
        // 刷新小弟头像
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->resetLabelImage(pFollower->GetData()->Follower_FightIconName);
        
        // 刷新星级
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_Quality)
                    pLabel->setIsVisible(true);
                else
                    pLabel->setIsVisible(false);
            }
        }
        
        // 等级
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20005));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFollower->GetData()->Follower_Level);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // hp
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20006));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_HP);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 攻击力
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20007));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Attack);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 恢复值
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20008));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFollower->GetData()->Follower_Comeback);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 性格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009));
        if (pLabel != NULL)
        {
            int iCharacterID = pFollower->GetData()->Follower_CharacterID;
            const Character_Data* pData = SystemDataManage::ShareInstance()->GetData_ForCharacter(iCharacterID);
            if (pData != NULL)
            {
                // 查询性格序号
                int index = 0;
                for (int i = 0; i < 7; i++)
                {
                    if (strcmp(pData->Character_Name, g_pCharacterName[i]) == 0)
                    {
                        index = i + 1;
                        break;
                    }
                }
                
                // 生命文件名
                if (index <= 7)
                {
                    sprintf(szTemp, "character_%d.png", index);
                    pLabel->resetLabelImage(szTemp);
                }
            }
        }
        
        // 刷新星级
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012 + i));
            if (pLabel != NULL)
            {
                if (i < pFollower->GetData()->Follower_Quality)
                    pLabel->setIsVisible(true);
                else
                    pLabel->setIsVisible(false);
            }
        }
        
        // 刷新小弟姓名
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20027));
        if (pLabel != NULL)
            pLabel->setLabelText(pFollower->GetData()->Follower_Name, NULL, 0);
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类显示小弟队伍信息函数
/************************************************************************/
void Layer_FollowerList::ShowFollowerTeamInfo()
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_CENTER_FRAME_ID);
    if (pFrame != NULL)
    {
        // 刷新队伍icon
        for (int i = 0; i < m_vecTeamList.size(); i++)
        {
            if (m_vecTeamList[i] == NULL)
                continue;
            
            // 刷新小弟icon
            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002 + i * 2));
            if (pLabel != NULL)
                pLabel->resetLabelImage(m_vecTeamList[i]->GetData()->Follower_IconName);
            
            // 刷新小弟iconframe
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002 + i * 2 + 1));
            if (pLabel != NULL)
            {
                char szTemp[32] = {0};
                sprintf(szTemp, "FollowerIcon_%d.png",  m_vecTeamList[i]->GetData()->Follower_Profession);
                pLabel->resetLabelImage(szTemp);
            }
            
            // 刷新小弟等级
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20021 + i));
            if (pLabel != NULL)
            {
                char szTemp[32] = {0};
                sprintf(szTemp, "%d", m_vecTeamList[i]->GetData()->Follower_Level);
                pLabel->GetNumberLabel()->setString(szTemp);
            }
        }
        
        // 属性
        float fTotalHp = 0.0f;          // 队伍合计hp
        float fRecoverValue = 0.0f;     // 队伍恢复力
        float fAttackSword = 0.0f;      // 队伍剑攻击力
        float fAttackGun = 0.0f;        // 队伍枪攻击力
        float fAttackFist = 0.0f;       // 队伍拳攻击力
        
        char szTemp[32] = {0};          // 数值字符串
        
        // 获得技能描述
        const Skill_Data* pData = SystemDataManage::ShareInstance()->GetData_ForSkill(m_vecTeamList[0]->GetData()->Follower_TeamSkillType, m_vecTeamList[0]->GetData()->Follower_TeamSkillID);
        if (pData != NULL)
        {
            // 技能icon
            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015));
            if (pLabel != NULL)
                pLabel->resetLabelImage(pData->Skill_IconName);
            
            // 技能名
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20025));
            if (pLabel != NULL)
                pLabel->setLabelText(pData->Skill_Name, NULL, 0);
            
            // 技能描述
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20016));
            if (pLabel != NULL)
                pLabel->setLabelText(pData->Skill_Depict, NULL, 0);
        }
        else
        {
            // 技能icon
            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015));
            if (pLabel != NULL)
                pLabel->resetLabelImage("skill_empty.png");
            
            // 技能名
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20025));
            if (pLabel != NULL)
                pLabel->setLabelText("", NULL, 0);
            
            // 技能描述
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20016));
            if (pLabel != NULL)
                pLabel->setLabelText("", NULL, 0);
        }
        
        // 计算队伍属性
        vector<Follower*>::iterator iter;
        for (iter = m_vecTeamList.begin(); iter != m_vecTeamList.end(); iter++)
        {
            if (*iter == NULL)
                continue;
            
            // 计算hp属性
            fTotalHp += (*iter)->GetData()->Follower_HP;
            
            // 计算恢复力
            fRecoverValue += (*iter)->GetData()->Follower_Comeback;
            
            // 计算攻击力
            switch ((*iter)->GetData()->Follower_Profession)
            {
                case SWORD:     // 剑
                    fAttackSword += (*iter)->GetData()->Follower_Attack;
                    break;
                case GUN:       // 枪
                    fAttackGun += (*iter)->GetData()->Follower_Attack;
                    break;
                case FIST:      // 拳
                    fAttackFist += (*iter)->GetData()->Follower_Attack;
                    break;
                default:
                    break;
            }
        }
        
        // 刷新数值到界面－hp
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20010));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", fTotalHp);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新数值到界面－恢复力
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", fRecoverValue);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新数值到界面－剑攻击力
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", fAttackSword);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新数值到界面－hp
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20013));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", fAttackGun);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新数值到界面－hp
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20014));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", fAttackFist);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类处理确认按钮函数
/************************************************************************/
void Layer_FollowerList::ProcessConfirmEvent()
{
    if (m_eState == FOLLOWER_TRAIN)
    {
        if (m_setTrainList.size() == 0)
        {
            KNUIFunction::GetSingle()->OpenMessageBoxFrame("选择一个素材吧^_^");
            return;
        }
        
        ShowFollowerTrainInfo(m_pEditedFollower, false);
        RefreshTrainMaterialList();
        
        KNUIFunction::GetSingle()->CloseFrameByNormalType(FOLLOWER_LIST_FRAME_ID);
        KNUIFunction::GetSingle()->OpenFrameByNormalType(FOLLOWER_TRAIN_INFO_FRAME_ID);
        
        // 新手引导
        if (PlayerDataManage::m_GuideMark)
            GameGuide::GetSingle()->FinishOneFloorGuide();
    }
    else if (m_eState == FOLLOWER_FIRE)
    {
        if (m_setFireList.size() != 0)
            ServerDataManage::ShareInstance()->RequestSellFollower();
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类处理重置按钮函数
/************************************************************************/
void Layer_FollowerList::ProcessClearEvent()
{
    if (m_eState == FOLLOWER_TRAIN)
        clearTrainList();
    else if (m_eState == FOLLOWER_FIRE)
        clearFireList();
}

/************************************************************************/
#pragma mark - Layer_FollowerList类事件函数 : 点击
/************************************************************************/
bool Layer_FollowerList::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    point = CCDirector::sharedDirector()->convertToGL(point);
    
    // 如果不在列表中点击，则不处理列表事件
    CCRect visibleRect = GetVisibleRange();
    if (point.x > visibleRect.origin.x && point.x < visibleRect.origin.x + visibleRect.size.width &&
        point.y > visibleRect.origin.y && point.y < visibleRect.origin.y + visibleRect.size.height)
    {
        KNScrollView::ccTouchBegan(pTouch, pEvent);
        
        // 获得图层坐标
        point = convertTouchToNodeSpace(pTouch);
        
        // 位置和尺寸，用于点击判断
        CCPoint pos(0.0f, 0.0f);
        CCSize size(0.0f, 0.0f);
        
        // 遍历选中的小弟
        vector<Follower*>::iterator iter;
        for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
        {
            // 获得小弟的位置和大小，因为使用小弟对象，所以取小弟对象中的icon作为尺寸
            pos = (*iter)->getPosition();
            
            // 根据列表的状态进行处理
            switch (m_eState) {
                case FOLLOWER_TEAM:             // 队伍编辑
                case PACKAGE_CHECK:             // 英雄背包
                {
                    // 获得按钮真实位置和尺寸
                    pos = ccpAdd(pos, (*iter)->getChildByTag(FollowerChild_ButtonNormal)->getPosition());
                    size = (*iter)->getChildByTag(FollowerChild_ButtonNormal)->getContentSize();
                    
                    // 判断是否被点击
                    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                    {
                        (*iter)->getChildByTag(FollowerChild_ButtonNormal)->setIsVisible(false);
                        (*iter)->getChildByTag(FollowerChild_ButtonSelect)->setIsVisible(true);
                        
                        m_bIsButtonPressed = true;
                    }
                }
                    break;
                case FOLLOWER_FIRE:             // 英雄售卖
                    break;
                case FOLLOWER_TRAIN:            // 小弟训练
                    break;
                case FOLLOWER_TEAM_COMPLETE:    // 编队完成
                    break;
                case FOLLOWER_NONE:             // 不处理
                    break;
            }
        }
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_FollowerList类事件函数 : 弹起
/************************************************************************/
void Layer_FollowerList::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    KNScrollView::ccTouchEnded(pTouch, pEvent);
    
    m_bIsButtonPressed = false;
    
    // 如果滑动了列表，则不进行弹起操作
    if (GetIsMovingMark())
        return;
    
    // 进行小弟点击事件处理
    CCPoint point = convertTouchToNodeSpace(pTouch);
    ProcessFollowerEvent(point);
}

/************************************************************************/
#pragma mark - Layer_FollowerList类事件函数 : 滑动
/************************************************************************/
void Layer_FollowerList::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
{
    if (m_bIsButtonPressed)
        return;
    
    KNScrollView::ccTouchMoved(pTouch, pEvent);
}

/************************************************************************/
#pragma mark - Layer_FollowerList类处理小弟事件
/************************************************************************/
void Layer_FollowerList::ProcessFollowerEvent(CCPoint point)
{
    // 位置和尺寸，用于点击判断
    CCPoint pos(0.0f, 0.0f);
    CCSize size(0.0f, 0.0f);
    
    // 遍历选中的小弟
    vector<Follower*>::iterator iter;
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
    {
        // 不显示的小弟，过滤掉
        if ((*iter)->getIsVisible() == false)
            continue;
        
        // 获得小弟的位置和大小，因为使用小弟对象，所以取小弟对象中的icon作为尺寸
        pos = (*iter)->getPosition();
            
        // 根据列表的状态进行处理
        switch (m_eState) {
            case FOLLOWER_TEAM:             // 队伍编辑
            {
                // 获得按钮真实位置和尺寸
                pos = ccpAdd(pos, (*iter)->getChildByTag(FollowerChild_ButtonNormal)->getPosition());
                size = (*iter)->getChildByTag(FollowerChild_ButtonNormal)->getContentSize();
                
                // 判断是否被点击
                if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                    point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                {
                    // 播放点击音效
                    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
                    
                    // 小弟是否可用
                    if ((*iter)->GetFollowerEnableMark() == false)
                        break;
                    
                    // 找到需要更换的小弟序列
                    m_iEditMemberIndex = 0;
                    for (int i = 0; i < m_vecTeamList.size(); i++)
                    {
                        if (m_vecTeamList[i] == m_pEditedFollower)
                            break;
                        
                        m_iEditMemberIndex++;
                    }
                    
                    // 关闭队伍标记
                    m_vecTeamList[m_iEditMemberIndex]->getChildByTag(FollowerChild_TeamTag)->setIsVisible(false);
                    (*iter)->getChildByTag(FollowerChild_TeamTag)->setIsVisible(true);
                    
                    // 编入队伍
                    m_vecTeamList[m_iEditMemberIndex] = *iter;
                    
                    // 完成编队
                    m_eState = FOLLOWER_TEAM_COMPLETE;
                    
                    // 隐藏小弟列表
                    KNUIFunction::GetSingle()->CloseFrameByNormalType(FOLLOWER_LIST_FRAME_ID);
                    
                    // 刷新队伍列表
                    ShowFollowerTeamInfo();
                    
                    // 排序
                    ScreenFollowerList(m_eLimitType);
                    
                    // 新手引导完成
                    if (PlayerDataManage::m_GuideMark)
                        GameGuide::GetSingle()->FinishOneFloorGuide();
                }
                else
                {
                    // 外框位置和尺寸
                    pos = ccpAdd(pos, (*iter)->getChildByTag(FollowerChild_ItemBack)->getPosition());
                    size = (*iter)->getChildByTag(FollowerChild_ItemBack)->getContentSize();
                    
                    // 点击后显示小弟详细信息
                    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                    {
                        // 播放点击音效
                        KNUIFunction::GetSingle()->PlayerEffect("1.wav");
                        
                        KNUIFunction::GetSingle()->ShowFollowerFullInfo(*iter, FOLLOWER_FULLINFO2_FRAME_ID);
                    }
                }
                
                (*iter)->getChildByTag(FollowerChild_ButtonNormal)->setIsVisible(true);
                (*iter)->getChildByTag(FollowerChild_ButtonSelect)->setIsVisible(false);
            }
                break;
            case FOLLOWER_TEAM_COMPLETE:
                break;
            case PACKAGE_CHECK:             // 英雄背包
            {
                // 获得按钮真实位置和尺寸
                pos = ccpAdd(pos, (*iter)->getChildByTag(FollowerChild_ButtonNormal)->getPosition());
                size = (*iter)->getChildByTag(FollowerChild_ButtonNormal)->getContentSize();
                
                // 判断是否被点击
                if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                    point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                {
                    // 播放点击音效
                    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
                    
                    // 记录被点击的小弟
                    m_pEditedFollower = *iter;
                    
                    // 刷新小弟信息
                    ShowFollowerInfo(*iter);
                    
                    // 打开小弟信息对话框
                    KNUIFunction::GetSingle()->OpenFrameByNormalType(FOLLOWER_INFO_FRAME_ID);
                }
                else
                {
                    // 外框位置和尺寸
                    pos = ccpAdd(pos, (*iter)->getChildByTag(FollowerChild_ItemBack)->getPosition());
                    size = (*iter)->getChildByTag(FollowerChild_ItemBack)->getContentSize();
                    
                    // 点击后显示小弟详细信息
                    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                    {
                        // 播放点击音效
                        KNUIFunction::GetSingle()->PlayerEffect("1.wav");
                        
                        KNUIFunction::GetSingle()->ShowFollowerFullInfo(*iter, FOLLOWER_FULLINFO2_FRAME_ID);
                    }
                }
                
                // 还原按钮状态
                (*iter)->getChildByTag(FollowerChild_ButtonNormal)->setIsVisible(true);
                (*iter)->getChildByTag(FollowerChild_ButtonSelect)->setIsVisible(false);
            }
                break;
            case FOLLOWER_FIRE:             // 英雄售卖
            {
                // 获得按钮真实位置和尺寸
                pos = ccpAdd(pos, (*iter)->getChildByTag(FollowerChild_SelectFrame)->getPosition());
                size = (*iter)->getChildByTag(FollowerChild_SelectFrame)->getContentSize();
                
                // 判断是否被点击
                if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                    point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                {
                    // 播放点击音效
                    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
                    
                    // 小弟是否可用
                    if ((*iter)->GetFollowerEnableMark() == false)
                        break;
                    
                    // 是否在列表中
                    set<Follower*>::iterator it = m_setFireList.find(*iter);
                    if (it != m_setFireList.end())
                    {
                        m_setFireList.erase(it);
                        
                        CCNode* pNode = (*iter)->getChildByTag(FollowerChild_SelectFrame);
                        if (pNode != NULL)
                            pNode->setIsVisible(false);
                    }
                    else
                    {
                        m_setFireList.insert(*iter);
                        
                        CCNode* pNode = (*iter)->getChildByTag(FollowerChild_SelectFrame);
                        if (pNode != NULL)
                            pNode->setIsVisible(true);
                    }
                    
                    // 计算解雇信息
                    CaleFireInfo();
                }
                else
                {
                    // 外框位置和尺寸
                    pos = ccpAdd(pos, (*iter)->getChildByTag(FollowerChild_ItemBack)->getPosition());
                    size = (*iter)->getChildByTag(FollowerChild_ItemBack)->getContentSize();
                    
                    // 点击后显示小弟详细信息
                    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                    {
                        // 播放点击音效
                        KNUIFunction::GetSingle()->PlayerEffect("1.wav");
                        
                        KNUIFunction::GetSingle()->ShowFollowerFullInfo(*iter, FOLLOWER_FULLINFO2_FRAME_ID);
                    }
                }
            }
                break;
            case FOLLOWER_TRAIN:            // 小弟训练
            {
                // 获得按钮真实位置和尺寸
                pos = ccpAdd(pos, (*iter)->getChildByTag(FollowerChild_SelectFrame)->getPosition());
                size = (*iter)->getChildByTag(FollowerChild_SelectFrame)->getContentSize();
                
                // 判断是否被点击
                if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                    point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                {
                    // 新手引导
                    if (PlayerDataManage::m_GuideMark)
                    {
                        if (PlayerDataManage::m_GuideStepMark == GUIDE_TRAIN && GameGuide::GetSingle()->GetFloorSubGuideStep() != 3)
                            break;
                        
                        // 完成一步
                        GameGuide::GetSingle()->FinishOneFloorGuide();
                    }
                    
                    // 播放点击音效
                    KNUIFunction::GetSingle()->PlayerEffect("1.wav");
                    
                    // 小弟是否可用
                    if ((*iter)->GetFollowerEnableMark() == false)
                        break;
                    
                    // 插入到列表
                    if (m_setTrainList.size() < 5)
                    {
                        // 是否在列表中
                        set<Follower*>::iterator it = m_setTrainList.find(*iter);
                        if (it != m_setTrainList.end())
                        {
                            m_setTrainList.erase(it);
                            
                            CCNode* pNode = (*iter)->getChildByTag(FollowerChild_SelectFrame);
                            if (pNode != NULL)
                                pNode->setIsVisible(false);
                        }
                        else
                        {
                            m_setTrainList.insert(*iter);
                            
                            CCNode* pNode = (*iter)->getChildByTag(FollowerChild_SelectFrame);
                            if (pNode != NULL)
                                pNode->setIsVisible(true);
                        }
                        
                        // 计算训练信息
                        CaleTrainInfo();
                    }
                }
                else
                {
                    // 新手引导
                    if (PlayerDataManage::m_GuideMark)
                        break;
                    
                    // 外框位置和尺寸
                    pos = ccpAdd(pos, (*iter)->getChildByTag(FollowerChild_ItemBack)->getPosition());
                    size = (*iter)->getChildByTag(FollowerChild_ItemBack)->getContentSize();
                    
                    // 点击后显示小弟详细信息
                    if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                        point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
                    {
                        // 播放点击音效
                        KNUIFunction::GetSingle()->PlayerEffect("1.wav");
                        
                        KNUIFunction::GetSingle()->ShowFollowerFullInfo(*iter, FOLLOWER_FULLINFO2_FRAME_ID);
                    }
                }
            }
                break;
            case FOLLOWER_NONE:             // 不处理
                break;
        }
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类该小弟是否在战斗列表中函数
/************************************************************************/
bool Layer_FollowerList::IsFollowerInBattleList(Follower* pFollower)
{
    vector<Follower*>::iterator iter;
    for (iter = m_vecTeamList.begin(); iter != m_vecTeamList.end(); iter++)
    {
        if (*iter == pFollower)
            return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_FollowerList类该小弟是否在训练列表中函数
/************************************************************************/
bool Layer_FollowerList::IsFollowerInTrainList(Follower* pFollower)
{
    set<Follower*>::iterator iter = m_setTrainList.find(pFollower);
    if (iter == m_setTrainList.end())
        return false;
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_FollowerList类该小弟是否在包裹列表中函数
/************************************************************************/
bool Layer_FollowerList::IsFollowerInPackList(int iFollowerID, Follower* pSelf)
{
    if (pSelf == NULL)
        return false;
    
    vector<Follower*>::iterator iter;
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
    {
        if ((*iter)->GetData()->Follower_ID == iFollowerID || *iter == pSelf)
            return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_FollowerList类该小弟是否在解雇列表中函数
/************************************************************************/
bool Layer_FollowerList::IsFollowerInFireList(Follower* pFollower)
{
    if (pFollower == NULL)
        return false;
    
    set<Follower*>::iterator iter;
    for (iter = m_setFireList.begin(); iter != m_setFireList.end(); iter++)
    {
        if (*iter == pFollower)
            return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_FollowerList类刷新训练素材列表函数
/************************************************************************/
void Layer_FollowerList::RefreshTrainMaterialList()
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_TRAIN_INFO_FRAME_ID);
    if (pFrame != NULL)
    {
        KNLabel* pLabel = NULL;
        
        // 清空原先列表
        for (int i = 0; i < 5; i++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015 + i));
            if (pLabel != NULL)
                pLabel->resetLabelImage("label_mask.png");
            
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20024 + i));
            if (pLabel != NULL)
                pLabel->resetLabelImage("label_mask.png");
        }
        
        // 刷新训练素材到界面
        set<Follower*>::iterator iter;
        int index = 0;
        for (iter = m_setTrainList.begin(); iter != m_setTrainList.end(); iter++)
        {
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015 + index));
            if (pLabel != NULL)
                pLabel->resetLabelImage((*iter)->GetData()->Follower_IconName);
            
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20024 + index));
            if (pLabel != NULL)
            {
                char szTemp[32] = {0};
                sprintf(szTemp, "FollowerIcon_%d.png", (*iter)->GetData()->Follower_Profession);
                pLabel->resetLabelImage(szTemp);
            }
            
            index++;
        }
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类设置列表训练信息显示函数
/************************************************************************/
void Layer_FollowerList::SetListTrainInfoDisplay(bool bIsDisplay)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        // 训练信息
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20010));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setIsVisible(bIsDisplay);
        
        // 素材数量
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setIsVisible(bIsDisplay);
        
        // 素材总数量
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20012));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setIsVisible(bIsDisplay);
        
        // 斜杠
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20013));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setIsVisible(bIsDisplay);
        
        // 获得经验
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20014));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setIsVisible(bIsDisplay);
        
        // 消耗金币
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setIsVisible(bIsDisplay);
        
        // 重置按钮
        KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30002));
        if (pButton != NULL)
            pButton->setIsVisible(bIsDisplay);
        
        // 确认按钮
        pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30003));
        if (pButton != NULL)
        {
            pButton->setIsVisible(bIsDisplay);
            pButton->resetButtonImage("button_packConfirm_normal.png", "button_packConfirm_select.png");
        }
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类设置列表解雇信息显示函数
/************************************************************************/
void Layer_FollowerList::SetListFireInfoDisplay(bool bIsDisplay)
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        // 解雇信息
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20016));
        if (pLabel != NULL)
            pLabel->GetBackSprite()->setIsVisible(bIsDisplay);
        
        // 解雇小弟数量
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20017));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setIsVisible(bIsDisplay);
        
        // 获得金币
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20018));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setIsVisible(bIsDisplay);
        
        // 重置按钮
        KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30002));
        if (pButton != NULL)
            pButton->setIsVisible(bIsDisplay);
        
        // 确认按钮
        pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30003));
        if (pButton != NULL)
        {
            pButton->setIsVisible(bIsDisplay);
            pButton->resetButtonImage("button_packFire_normal.png", "button_packFire_select.png");
        }
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类计算训练信息函数
/************************************************************************/
void Layer_FollowerList::CaleTrainInfo()
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        // 临时文本
        char szTemp[32] = {0};
        
        // 刷新素材数量
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", static_cast<int>(m_setTrainList.size()));
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新获得经验
        set<Follower*>::iterator iter;
        int iExp = 0;
        for (iter = m_setTrainList.begin(); iter != m_setTrainList.end(); iter++)
            iExp += (*iter)->GetData()->Follower_BeAbsorbExperience;
        sprintf(szTemp, "%d", iExp);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20014));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
        
        // 刷新消耗金币
        //int iPrice = int((1.4 * pow(m_pEditedFollower->GetData()->Follower_Level, 3) - 3 * pow(m_pEditedFollower->GetData()->Follower_Level, 2) + 10 * m_pEditedFollower->GetData()->Follower_Level + 192) * m_pEditedFollower->GetData()->Follower_RegulateAbsorbPrice / 2);
        int iPrice = int((m_pEditedFollower->GetData()->Follower_Level * 100) * m_pEditedFollower->GetData()->Follower_RegulateAbsorbPrice);
        iPrice = iPrice * (m_setTrainList.size());
        sprintf(szTemp, "%d", iPrice);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015));
        if (pLabel != NULL)
            pLabel->GetNumberLabel()->setString(szTemp);
    }
}

/************************************************************************/
#pragma mark - Layer_FollowerList类计算解雇信息函数(这个函数灰主动刷新界面)
/************************************************************************/
void Layer_FollowerList::CaleFireInfo()
{
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(FOLLOWER_LIST_FRAME_ID);
    if (pFrame != NULL)
    {
        // 临时文本
        char szTemp[32] = {0};
        
        // 刷新解雇小弟数量
        KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20017));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", static_cast<int>(m_setFireList.size()));
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 刷新解雇小弟获得金钱
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20018));
        if (pLabel != NULL)
        {
            float fPrice = 0.0f;
            set<Follower*>::iterator iter;
            for (iter = m_setFireList.begin(); iter != m_setFireList.end(); iter++)
                fPrice += (*iter)->GetData()->Follower_Price / 100.0f;
            
            sprintf(szTemp, "%.0f", fPrice);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
}