//
//  Layer_Game.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-6-19.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_Game_h
#define MidNightCity_Layer_Game_h

#include "Layer_Building.h"

const int MENU_ICON_NUM = 4;

// iconid
const int ICON_ID = 1001;
// numback
const int NUM_BACK_ID = 1002;
// number
const int NUMBER_ID = 1003;
// 金币数量
const int ICON_NUM = 5;

class Layer_Game : public CCLayer
{
public:
    #pragma mark - 静态函数 : 获得唯一对象实例函数
    static Layer_Game* GetSingle();
    
    #pragma mark - 构造函数
    Layer_Game();
    #pragma mark - 析构函数
    virtual ~Layer_Game();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 退出函数
    virtual void onExit();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 处理金币动画函数
    void ProcessCoinAnim(CCPoint pos);
    #pragma mark - 回调函数 : 处理金币增加函数
    void ProcessCoinAdd(CCObject* pObject);
    
    #pragma mark - 设置菜单显示函数
    void SetMenuItemState(int index, bool bIsShow);
    
    #pragma mark - 更新菜单状态函数
    void UpdateMenuItemState();
    
    #pragma mark - 更新金钱数量函数
    void UpdateMenuMoney();
    #pragma mark - 更新主界面数据函数
    void UpdateMainData();
    
    #pragma mark - 显示楼层遮罩函数
    void ShowFloorMask();
    #pragma mark - 关闭楼层遮罩函数
    void CloseFloorMask();
    
    #pragma mark - cocos2d初始化连接
    LAYER_NODE_FUNC(Layer_Game);
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
private:
    #pragma mark - 回调函数 : 菜单动画函数
    void MenuAnim(CCObject* pObject);
    
    #pragma mark - 回调函数 : 颜色改变函数
    void MenuColorChange(CCObject* pObject);
    
    #pragma mark - 回调函数 : 剑动画函数
    void SwordAnim(CCObject* pObject);
    #pragma mark - 回调函数 : new函数
    void NewAnim(CCObject* pObject);
    
    #pragma mark - 回调函数 : 金钱数量增加函数
    void PlayerMoneyAdd(CCObject* pObject);
    #pragma mark - 回调函数 : 金钱数量减少函数
    void PlayerMoneySub(CCObject* pObject);
    
    #pragma mark - 更新回调函数 : 处理行动力时间函数
    void UpdateActionRecoverTime(ccTime dt);
    
    #pragma mark - 显示玩家信息框函数
    void ShowPlayerInfoFrame();
private:
    static Layer_Game * m_Instance;
    
    // 游戏layer对象变量
    Layer_Building* m_pBuildingLayer;                       // 楼层图层
    
    CCSprite* m_pMenuBack;                                  // 菜单背景
    CCSprite* m_pExpProgressBack;                           // 经验进度条背景
    CCSprite* m_pEnergyProgressBack;                        // 能量进度条背景
    
    CCSprite* m_pSetButtonNormal;                           // 设置按钮－正常
    CCSprite* m_pSetButtonSelect;                           // 设置按钮－选择
    
    CCLayer* m_pMenuItem[MENU_ICON_NUM];                    // 菜单选项
    CCSprite* m_pMenuItemBack;                              // 菜单背景
    
    KNProgress* m_pExpProgress;                             // 经验进度条
    KNProgress* m_pEnergyProgress;                          // 能量进度条
    
    CCLabelAtlas* m_pMoneyNum1;                             // 金钱数字1
    CCLabelAtlas* m_pMoneyNum2;                             // 金钱数字2
    
    CCLabelAtlas* m_pPlayerLevelNum;                        // 玩家等级
    
    CCSprite* m_pMoneyIcon[ICON_NUM];                       // 金币
    
    CCSprite* m_pMoneyIconMask;                             // 金币遮罩
    
    CCLabelAtlas* m_pTimeMask1;                             // 时间1
    CCLabelAtlas* m_pTimeMask2;                             // 时间2
    CCSprite* m_pTimeMaohao;                                // 时间冒号
    
    CCLabelAtlas* m_pCurAction;                             // 当前行动力
    CCLabelAtlas* m_pFulAction;                             // 满行动力
    CCSprite* m_pActionSlide;                               // 斜杠
    
    CCLayerColor* m_pFloorMask;                             // 楼层遮罩
    
    // 游戏layer属性变量
    int m_iExpFinalValue;                                   // 经验值
    int m_iActionFinalValue;                                // 行动力值
    
    int m_iMoneyNum1;                                       // 金钱数字1
    int m_iMoneyNum2;                                       // 金钱数字2
    
    bool m_bIsMenuItemState[MENU_ICON_NUM];                 // 菜单显示状态
    bool m_bIsMoneyRefresh;                                 // 金钱是否刷新中，避免调用两次死循环
};

#endif
