//
//  Layer_Calendar.h
//  都市猎人
//
//  Created by 孙 惠伟 on 12-9-25.
//
//

#ifndef MidNightCity_Layer_Calendar_h
#define MidNightCity_Layer_Calendar_h

#include "KNBaseUI.h"
#include "Sprite_MySprite.h"

USING_NS_CC;

// 月天数
const int MONTH_DAYS = 31;

class Layer_Calendar : public KNBaseUI
{
public:
    #pragma mark - 构造函数
    Layer_Calendar();
    #pragma mark - 析构函数
    virtual ~Layer_Calendar();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 初始化函数
    virtual bool init(UIInfo& stInfo);
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 重写退出函数
    virtual void onExit();
    
    #pragma mark - 刷新日历信息函数
    void RefreshCalendarInfo();
    
    #pragma mark - 获得可以补签数量函数
    int GetSignEnableNum() const                    {return m_iSignEnableNum;}
    
    #pragma mark - 获得未签到的第一天函数
    int GetFirstUnsignDay();
    
    #pragma mark - 连接cocos2d初始化方式
    LAYER_NODE_FUNC(Layer_Calendar);
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 滑动
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
private:
    #pragma mark - 获得月份天数函数
    int GetMonthDays();
    #pragma mark - 获得1号的星期数函数
    int GetWeekdayFor1th();
    
    #pragma mark - 该日是否签到过函数
    bool IsSignAlready(int iDay);
private:
    // 日历对象变量
    Sprite_MySprite* m_pPassSign;                   // 过去的背景
    Sprite_MySprite* m_pNowSign;                    // 现在的背景
    Sprite_MySprite* m_pFutureSign;                 // 未来的背景
    
    Sprite_MySprite* m_pSignOK;                     // 签到标志
    Sprite_MySprite* m_pSignNot;                    // 未签到标志
    
    CCLabelTTF* m_pDayNumber[MONTH_DAYS];           // 数字 : 日
    
    // 日历属性变量
    tm m_stDate;
    
    int m_iSignEnableNum;                           // 可以补签的数量
};

#endif
