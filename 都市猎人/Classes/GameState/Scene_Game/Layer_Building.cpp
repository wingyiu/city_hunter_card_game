//
//  KNBuildingLayer.cpp
//  MyCocosGame
//
//  Created by 惠伟 孙 on 12-3-29.
//  Copyright (c) 2012年 上海恺英网络科技有限公司. All rights reserved.
//

#include "Layer_Building.h"
USING_NS_CC;

bool bIsTest = true;
     
Layer_Building * Layer_Building::m_Instance = NULL;

const int sss = 20.0f;

/************************************************************************/
#pragma mark - Layer_Building类获得block实例对象
/************************************************************************/
Layer_Building * Layer_Building::ShareInstance()
{
    if(m_Instance)
        return m_Instance;
    return NULL;
}

/************************************************************************/
#pragma mark - Layer_Building类构造函数
/************************************************************************/
Layer_Building::Layer_Building()
{
    // 为楼层对象变量赋初值
    m_FloorList.clear();
    
    m_pBuildingBase = NULL;
    m_pBuildingBack1 = NULL;
    m_pBuildingBack2 = NULL;
    
    for (int i = 0; i < MAX_CLOUD_NUM; i++)
        m_pCloud[i] = NULL;
    
    m_pGuideArrow = NULL;
    
    m_pFloorMaskUp = NULL;
    m_pFloorMaskDown = NULL;
    
    // 为楼层属性变量赋初值
    m_fBuildingHeight = 0.0f;
    
    m_iFloorType = F_Player;
    m_iCurrentLevelFloorType = -1;
    
    m_Instance = this;
    
    m_bIsFrameEnable = false;
}

/************************************************************************/
#pragma mark - Layer_Building类析构函数
/************************************************************************/
Layer_Building::~Layer_Building()
{
    // 销毁所有对象
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_Building类退出函数
/************************************************************************/
void Layer_Building::onExit()
{
    // 基类退出
    CCLayer::onExit();
    
    // 关闭背景音乐
    KNUIFunction::GetSingle()->StopBackMusic();
    
    // 销毁所有对象
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_Building类初始化函数
/************************************************************************/
bool Layer_Building::init()
{
    // 获得屏幕尺寸
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    // 设置图层大小
    SetScrollSize(ccp(winSize.width * 2.0f, winSize.height));
    // 设置滑动方式
    SetScrollType(VERTICAL);
    
    // 初始化基类
    UIInfo stInfo;
    KNScrollView::init(stInfo);
    
    // 地下层
    m_pBuildingBase = CCSprite::spriteWithSpriteFrameName("floor_base.png");
    if (m_pBuildingBase != NULL)
    {
        m_pBuildingBase->setPosition(ccp(160.0f, -198.0f));
        addChild(m_pBuildingBase);
    }
    
    // 背景1
    m_pBuildingBack1 = CCSprite::spriteWithSpriteFrameName("building_back1.png");
    if (m_pBuildingBack1 != NULL)
    {
        m_pBuildingBack1->setVertexZ(-0.6f);
        addChild(m_pBuildingBack1);
    }
    
    // 初始化云层
    for (int i = 0; i < MAX_CLOUD_NUM; i++)
    {
        m_pCloud[i] = CCSprite::spriteWithSpriteFrameName("cloud.png");
        if (m_pCloud[i] != NULL)
        {
            m_pCloud[i]->setVertexZ(-0.55f + i * 0.01f);
            addChild(m_pCloud[i]);
        }
    }
    
    // 背景2
    m_pBuildingBack2 = CCSprite::spriteWithSpriteFrameName("building_back2.png");
    if (m_pBuildingBack2 != NULL)
    {
        m_pBuildingBack2->setVertexZ(-0.5f);
        addChild(m_pBuildingBack2);
    }
    
    // 从数据中获得楼层列表
    PlayerFloorDepot plist = *PlayerDataManage::ShareInstance()->GetMyFloorDepot();
    for (PlayerFloorDepot::iterator it = plist.begin(); it != plist.end(); it++)
    {
        Floor* pFloor = Floor::node();
        if (pFloor != NULL && pFloor->InitFloorByData(it->second) == true)
            AddOneFloorToList(pFloor -> GetFloorData() -> Floor_Type, pFloor);
    }
    
    // 更新提示层位置
    map<int, Floor*>::iterator iter = m_FloorList.find(F_Hint);
    if (iter != m_FloorList.end())
    {
        // 刷新信息
        const FloorData * pFloorData = SystemDataManage::ShareInstance() -> GetData_ForFloor(m_iFloorType, 1);
        if (pFloorData != NULL)
            iter->second->SetTopFloorNeedMoneyAndLevel(pFloorData->Floor_Build_PlayerLevel, pFloorData->Floor_Build_PlayerMoney);
    }
    
    // 移动云层
    for (int i = 0; i < MAX_CLOUD_NUM; i++)
    {
        if (m_pCloud[i] != NULL)
            cloudMove(m_pCloud[i]);
    }
    
    // 设置可视区域
    KNScrollView::SetVisibleRange(CCRect(0.0f, 0.0f, winSize.width, winSize.height));
    // 设置楼层是图范围
    SetItemSize(CCSize(250.0f, 129.0f));
    setPosition(ccp(0.0f, 62.0f));
    
    // 添加背景高度
    KNScrollView::SetScrollSize(ccp(160.0f, m_fBuildingHeight + 400.0f));
    
    // 关闭底部滑动
    KNScrollView::SetIsStopBottonMove(true);
    
    // 添加事件
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    // 注册楼层更新
    schedule(schedule_selector(Layer_Building::floorUpdate));
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_Building类添加一个楼层到列表函数
/************************************************************************/
bool Layer_Building::AddOneFloorToList(int iType, Floor* pFloorLayer)
{
    // 是否超过最大楼层数
    if (m_FloorList.size() >= MAX_FLOOR_NUM)
        return false;
    
    // 查询是否已经存在该楼层
    map<int, Floor*>::iterator iter = m_FloorList.find(iType);
    if (iter != m_FloorList.end())
        return false;
    
    // 楼层顶部尖角高度 : photoshop测量的- -|||
    const float fFloorTopHeight = 30.0f;
    
    // 获得屏幕尺寸
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    // 设置楼层位置
    pFloorLayer->setPosition(ccp(winSize.width / 2.0f, m_fBuildingHeight));
    
    // 设置下一层楼层基点
    if (iType == F_Player)          // 玩家层高度不同，特殊处理
        m_fBuildingHeight += (101.0f - 46.0f + 59.0f);
    else if (iType == F_Platform)   // 站台层高度不同，特殊处理
        m_fBuildingHeight += (53.0f + 59.0f);
    else if (iType == F_Hint)       // 提示层不需要加高度
    {   }
    else
        m_fBuildingHeight += (59.0f - fFloorTopHeight + 59.0f);
    
    // 添加楼层到图层
    addChild(pFloorLayer);
    
    // 添加到列表
    m_FloorList.insert(make_pair(iType, pFloorLayer));
    
    // 记录下一层楼层类型
    if (iType != F_Hint)
    {
        m_iFloorType = iType + 1;
    }
    else            // 特殊处理顶层图片
    {
        if (m_iFloorType == F_Store)
            pFloorLayer->RefreshTopFloorImage("floor_14_special.png");
    }
    
    // 更新提示层位置
    iter = m_FloorList.find(F_Hint);
    if (iter != m_FloorList.end())
    {
        // 设置顶层位置
        iter->second->setPosition(ccp(winSize.width / 2.0f, m_fBuildingHeight - 59.0f + 73.0f));
        
        // 设置图层大小
        SetScrollSize(ccp(winSize.width, m_fBuildingHeight + 191 + 200));
        
        // 刷新信息
        const FloorData * pFloorData = SystemDataManage::ShareInstance() -> GetData_ForFloor(iType + 1, 1);
        if (pFloorData != NULL)
            iter->second->SetTopFloorNeedMoneyAndLevel(pFloorData->Floor_Build_PlayerLevel, pFloorData->Floor_Build_PlayerMoney);
        
        // 更新背景高度
        m_pBuildingBack1->setPosition(ccp(160.0f, iter->second->getPosition().y + sss));
        m_pBuildingBack2->setPosition(ccp(160.0f, iter->second->getPosition().y + 80.0f));
        
        return true;
    }
    
    // 设置图层大小
    SetScrollSize(ccp(winSize.width, m_fBuildingHeight));
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_Building类事件函数－点击
/************************************************************************/
bool Layer_Building::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    return KNScrollView::ccTouchBegan(pTouch, pEvent);
}

/************************************************************************/
#pragma mark - Layer_Building类事件函数－弹起
/************************************************************************/
void Layer_Building::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    // 调用基类事件函数－防止滑动停止
    KNScrollView::ccTouchEnded(pTouch, pEvent);
    
    if (IsFloorInAction() == true)
        return;
    
    // 如果滑动中，则不响应点击事件
    if (GetIsMovingMark())
        return;
    
    // 获得点击坐标
    CCPoint point = pTouch->locationInView(pTouch->view());
    CCPoint glPoint = CCDirector::sharedDirector()->convertToGL(point);
    point = CCDirector::sharedDirector()->convertToGL(point);
    point = convertToNodeSpace(point);
    
    // 检测那层被点击
    CCPoint floorPos;
    CCSize floorSize;
    
    map<int, Floor*>::iterator iter;
    for (iter = m_FloorList.begin(); iter != m_FloorList.end(); iter++)
    {
        floorPos = iter->second->getPosition();
        floorSize = CCSize(320.0f, 131.0f);
        
        // 因为楼层多了一个墙角，所以两个楼层的中心点不对齐，所以需要往下偏移，校准中心点
        floorPos.y -= 15.0f;
        // 去除楼层墙角的高度
        floorSize.height -= 30.0f;
        
        if (point.x > floorPos.x - floorSize.width / 2.0f && point.x < floorPos.x + floorSize.width / 2.0f &&
            point.y > floorPos.y - floorSize.height / 2.0f && point.y < floorPos.y + floorSize.height / 2.0f)
        {
            // 新手引导 : 指引玩家点击特定的楼层
            //PlayerDataManage::m_GuideMark = false;
            if (PlayerDataManage::m_GuideMark)
            {
                // 建楼引导
                if (GameGuide::GetSingle()->GetIsBuildGuide())
                {
                    if (iter->first != F_Hint)
                        return;
                }
                else
                {
                    if (PlayerDataManage::m_GuideStepMark < GUIDE_TRAIN)
                    {
                        if (iter->first != PlayerDataManage::m_GuideStepMark)
                            return;
                    }
                    else
                    {
                        if (PlayerDataManage::m_GuideStepMark == GUIDE_TRAIN)
                        {
                            if (iter->first != F_Follower)
                                return;
                        }
                        else if (PlayerDataManage::m_GuideStepMark == GUIDE_MISSION_EXTRA)
                        {
                            if (iter->first != F_Mission)
                                return;
                        }
                        else if (PlayerDataManage::m_GuideStepMark == GUIDE_SHOP)
                        {
                            if (iter->first != F_Shop)
                                return;
                        }
                        else if (PlayerDataManage::m_GuideStepMark == GUIDE_FRIEND)
                        {
                            if (iter->first != F_Platform)
                                return;
                        }
                        else if (PlayerDataManage::m_GuideStepMark == GUIDE_FLOOR_HAVEST)
                        {
                            if (iter->first != F_Store)
                                return;
                        }
                    }
                }
            }
            
            // 出于行动中的楼层，无法被点击
            if (iter->second->GetFloorActionState() == true)
            {
                m_bIsFrameEnable = false;
                break;
            }
            
            // 播放音效
            KNUIFunction::GetSingle()->PlayerEffect("1.wav");
            
            // 记录当前点击的楼层
            m_iCurrentLevelFloorType = iter->second->GetFloorData()->Floor_Type;
            
            // 点击关闭提示
            iter->second->CloseHintItem();
            
            // 楼层点击处理
            switch (iter->second->GetFloorData()->Floor_Type) {
                case F_Player:      // 大厅
                    KNUIFunction::GetSingle()->OpenFrameByJumpTypeSpecial(PLAYER_HOME_FRAME_ID, ccp(160.0f, glPoint.y));
                    break;
                case F_Follower:    // 英雄中心
                    if (PlayerDataManage::m_GuideMark)
                        GameGuide::GetSingle()->FinishOneFloorGuide();
                    
                    KNUIFunction::GetSingle()->OpenFrameByJumpTypeSpecial(FOLLOWER_CENTER_FRAME_ID, ccp(160.0f, glPoint.y));
                    break;
                case F_Mission:     // 任务所
                {
                    if (PlayerDataManage::m_GuideMark)
                        GameGuide::GetSingle()->FinishOneFloorGuide();
                    
                    if (KNUIFunction::GetSingle()->IsPackageFull() == true)
                    {
                        KNUIFunction::GetSingle()->OpenFrameByJumpType(PACKAGE_EXTEND_FRAME_ID);
                        break;
                    }
                    
                    // 打开界面
                    KNUIFunction::GetSingle()->OpenFrameByJumpTypeSpecial(MISSION_CENTER_FRAME_ID, ccp(160.0f, glPoint.y));
                    
                    // 列表回到最顶部
                    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(MISSION_LIST_FRAME_ID);
                    if (pFrame != NULL)
                    {
                        Layer_MapList* pList = dynamic_cast<Layer_MapList*>(pFrame->GetSubUIByID(50000));
                        if (pList != NULL)
                            pList->SetPoint(true);
                    }
                }
                    break;
                case F_Shop:        // 商城
                    if (PlayerDataManage::m_GuideMark)
                        GameGuide::GetSingle()->FinishOneFloorGuide();
                    
                    if (KNUIFunction::GetSingle()->IsPackageFull() == true)
                    {
                        KNUIFunction::GetSingle()->OpenFrameByJumpType(PACKAGE_EXTEND_FRAME_ID);
                    }
                    else
                    {
                        KNUIFunction::GetSingle()->OpenFrameByJumpTypeSpecial(SHOP_FRAME_ID, ccp(160.0f, glPoint.y));
                        ServerDataManage::ShareInstance() -> RequestWareFollower();
                    }
                    break;
                case F_Platform:    // 站台
                    if (PlayerDataManage::m_GuideMark)
                    {
                        GameGuide::GetSingle()->FinishOneFloorGuide();
                        break;
                    }
                    
                    iter->second->ProcessFloorTouchEvent(pTouch, ccp(160.0f, glPoint.y));
                    break;
                case F_Store:       // 便利店
                case F_Ktv:         // ktv
                case F_Snock:       // 桌球房
                case F_Train:       // 健身房
                case F_Game:        // 游戏机房
                case F_Bar:         // 酒吧
                case F_Gamble:      // 赌场
                case F_Cardgame:    // 桌游房
                    CloseAllFloorTimeUpdate();
                    iter->second->ProcessFloorTouchEvent(pTouch, ccp(160.0f, glPoint.y));
                    SetExtraFloorInfo(iter->second->GetFloorData()->Floor_Type);
                    iter->second->SetHavestFloorTimeRefreshEnable(true);
                    break;
                case F_Hint:        // 顶层
                {
                    if (PlayerDataManage::m_GuideMark && GameGuide::GetSingle()->GetIsBuildGuide())
                    {
                        // 完成引导步骤
                        GameGuide::GetSingle()->FinishOneFloorGuide();
                        break;
                    }
                    
                    if (m_iFloorType >= F_Hint)
                        break;
                    
                    // 刷新要建造的楼层名
                    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(BUILDING_CONSTRUCT_FRAME_ID);
                    if (pFrame != NULL)
                    {
                        // 取得要建造的楼层信息
                        const FloorData * pFloorData = SystemDataManage::ShareInstance() -> GetData_ForFloor(m_iFloorType, 1);
                        if (pFloorData != NULL)
                        {
                            char szTemp[32] = {0};
                            
                            // name
                            KNLabel* pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20011));
                            if (pLabel != NULL)
                            {
                                sprintf(szTemp, "name%d.png", m_iFloorType);
                                pLabel->resetLabelImage(szTemp);
                            }
                            
                            // icon
                            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20013));
                            if (pLabel != NULL)
                            {
                                sprintf(szTemp, "floor_icon_%d.png", m_iFloorType);
                                pLabel->resetLabelImage(szTemp);
                            }
                            
                            // 收获钱
                            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009));
                            if (pLabel != NULL)
                            {
                                sprintf(szTemp, "%.0f", pFloorData->Floor_HarvestMoney);
                                pLabel->GetNumberLabel()->setString(szTemp);
                            }
                            
                            // 需要钱
                            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20010));
                            if (pLabel != NULL)
                            {
                                sprintf(szTemp, "%d", pFloorData->Floor_Build_PlayerMoney);
                                pLabel->GetNumberLabel()->setString(szTemp);
                            }
                            
                            // 分离收获时间
                            int iHour = pFloorData->Floor_HarvestTime / 60 / 60;
                            int iMinute = pFloorData->Floor_HarvestTime / 60 - iHour * 60;
                            int iSecond = pFloorData->Floor_HarvestTime - iHour * 60 * 60 - iMinute * 60;
                            
                            // 时
                            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20014));
                            if (pLabel != NULL)
                            {
                                // 少于两位数，补0
                                if (iHour / 10 == 0)
                                    sprintf(szTemp, "0%d", iHour);
                                else
                                    sprintf(szTemp, "%d", iHour);
                                
                                pLabel->GetNumberLabel()->setString(szTemp);
                            }
                            
                            // 分
                            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015));
                            if (pLabel != NULL)
                            {
                                // 少于两位数，补0
                                if (iMinute / 10 == 0)
                                    sprintf(szTemp, "0%d", iMinute);
                                else
                                    sprintf(szTemp, "%d", iMinute);
                                
                                pLabel->GetNumberLabel()->setString(szTemp);
                            }
                            
                            // 秒
                            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20016));
                            if (pLabel != NULL)
                            {
                                // 少于两位数，补0
                                if (iSecond / 10 == 0)
                                    sprintf(szTemp, "0%d", iSecond);
                                else
                                    sprintf(szTemp, "%d", iSecond);
                                
                                pLabel->GetNumberLabel()->setString(szTemp);
                            }
                            
                            // 打开建造界面
                            KNUIFunction::GetSingle()->OpenFrameByJumpTypeSpecial(BUILDING_CONSTRUCT_FRAME_ID, ccp(160.0f, glPoint.y));
                        }
                    }
                }
                    break;
                default:
                    break;
            }
            break;
        }
    }
}

/************************************************************************/
#pragma mark - Layer_Building类事件函数－滑动
/************************************************************************/
void Layer_Building::ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent)
{
    KNScrollView::ccTouchMoved(pTouch, pEvent);
}

/************************************************************************/
#pragma mark - Layer_Building类建造楼层
/************************************************************************/
void Layer_Building::BuildingFloor()
{
    // 获得房产场景
    if (m_iFloorType > F_Hint)
        return;
    
    // 顶层特殊处理
    if (m_iFloorType == F_Store)
    {
        map<int, Floor*>::iterator iter = m_FloorList.find(F_Hint);
        if (iter != m_FloorList.end())
            iter->second->RefreshTopFloorImage("floor_14.png");
    }
    
    // 请求建造楼层
    ServerDataManage::ShareInstance()->RequestNewFloor(m_iFloorType);
}

/************************************************************************/
#pragma mark - Layer_Building类播放烟花
/************************************************************************/
void Layer_Building::PlayerFrame()
{
    if (!m_bIsFrameEnable)
    {
        unschedule(schedule_selector(Layer_Building::PlayerFrame));
        return;
    }
    
    CCParticleSystem* m_pLevelExplosion = CCParticleExplosion::node();
    if (m_pLevelExplosion != NULL)
    {
        m_pLevelExplosion->setTexture(CCTextureCache::sharedTextureCache()->addImage("explosion_star.png"));
        m_pLevelExplosion->setIsAutoRemoveOnFinish(true);
        
        m_pLevelExplosion->setLife(0.02f);
        m_pLevelExplosion->setSpeed(80.0f);
        m_pLevelExplosion->setEmissionRate(400);
        m_pLevelExplosion->setGravity(ccp(0.0f, -2.0f));
        
        m_pLevelExplosion->setPosition(ccp(rand() % 320, (m_iFrameFloorType) * 90.0f - 90.0f));
        
        addChild(m_pLevelExplosion, 10);
    }
}

/************************************************************************/
#pragma mark - Layer_Building类移动到当前层
/************************************************************************/
void Layer_Building::MoveToCurrentFloor()
{
    CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.5f, ccp(0.0f, -m_fBuildingHeight + 400));
    if (pMoveTo != NULL)
        runAction(pMoveTo);
    else
        setPosition(ccp(0.0f, -m_fBuildingHeight + 400));
}

/************************************************************************/
#pragma mark - Layer_Building类设置楼层
/************************************************************************/
void Layer_Building::LevelupFloor(int iType)
{
    map<int, Floor*>::iterator iter = m_FloorList.find(iType);
    if (iter != m_FloorList.end())
    {
        PlayerFloorDepot plist = *PlayerDataManage::ShareInstance()->GetMyFloorDepot();
        PlayerFloorDepot::iterator it = plist.find(iType);
        if (it != plist.end())
        {
            // 同时收获
            iter->second->ProcessHavestSucessEvent();
            
            // 设置新数据
            iter->second->SetFloorData(it->second);
            
            // 开始升级
            iter->second->BeginLevelup();
        }
    }
}

/************************************************************************/
#pragma mark - Layer_Building类设置扩展楼层信息函数
/************************************************************************/
void Layer_Building::SetExtraFloorInfo(int iFloorType)
{
//    <label id = 20001 x = 0 y = 71 backimage = "building_levelup_back_up.png"></label>
//    <label id = 20029 x = 0 y = -26 backimage = "building_levelup_back_down.png"></label>
    // 取得升级界面
    KNFrame* pFrame = KNUIManager::GetManager()->GetFrameByID(BUILDING_LEVELUP_FRAME_ID);
    if (pFrame == NULL)
        return;
    
    KNLabel* pLabel = NULL;
    
    // 查找当前楼层
    map<int, Floor*>::iterator iter = m_FloorList.find(iFloorType);
    if (iter == m_FloorList.end())
        return;
    
    // 刷新当前楼层信息
    int iLevel = iter->second->GetFloorData()->Floor_Level;
    const FloorData* pFloorLastData = SystemDataManage::ShareInstance() -> GetData_ForFloor(iFloorType, iLevel);
    if (pFloorLastData != NULL)
    {
        // 设置楼层文本标签
        char szTemp[32] = {0};
        sprintf(szTemp, "name%d.png", iFloorType);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20025));
        if (pLabel != NULL)
            pLabel->resetLabelImage(szTemp);
        
        // 设置星星等级
        for (int i = 0; i < 5; i++)
        {
            // 当前楼层等级
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20004 + i));
            if (i < iLevel)
                pLabel->GetBackSprite()->setIsVisible(true);
            else
                pLabel->GetBackSprite()->setIsVisible(false);
        }
        
        // 当前等级收获价格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20015));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFloorLastData->Floor_HarvestMoney);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 升级价格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20017));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", pFloorLastData->Floor_Upgrade_PlayerMoney);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 分离当前等级收获时间
        int iHour = pFloorLastData->Floor_HarvestTime / 60 / 60;
        int iMinute = pFloorLastData->Floor_HarvestTime / 60 - iHour * 60;
        int iSecond = pFloorLastData->Floor_HarvestTime - iHour * 60 * 60 - iMinute * 60;
        
        // 当前等级收获时间
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20018));
        if (pLabel != NULL)
        {
            if (iHour / 10 == 0)
                sprintf(szTemp, "0%d", iHour);
            else
                sprintf(szTemp, "%d", iHour);
            
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20019));
        if (pLabel != NULL)
        {
            if (iMinute / 10 == 0)
                sprintf(szTemp, "0%d", iMinute);
            else
                sprintf(szTemp, "%d", iMinute);
            
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20020));
        if (pLabel != NULL)
        {
            if (iSecond / 10 == 0)
                sprintf(szTemp, "0%d", iSecond);
            else
                sprintf(szTemp, "%d", iSecond);
            
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 当前等级数字
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20027));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", iLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
    
    // 刷新下一级楼层信息
    int iNextLevel = iLevel + 1;
    const FloorData* pFloorNextData = SystemDataManage::ShareInstance() -> GetData_ForFloor(iFloorType, iNextLevel);
    if (pFloorNextData != NULL)
    {
        // 设置楼层文本标签
        char szTemp[32] = {0};
        sprintf(szTemp, "name%d.png", iFloorType);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20026));
        if (pLabel != NULL)
            pLabel->resetLabelImage(szTemp);
        
        // 设置星星等级
        for (int i = 0; i < 5; i++)
        {
            // 下一级楼层等级
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009 + i));
            if (i < iNextLevel)
                pLabel->GetBackSprite()->setIsVisible(true);
            else
                pLabel->GetBackSprite()->setIsVisible(false);
        }
        
        // 下一级收获价格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20016));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%.0f", pFloorNextData->Floor_HarvestMoney);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 分离下一等级收获时间
        int iHour = pFloorNextData->Floor_HarvestTime / 60 / 60;
        int iMinute = pFloorNextData->Floor_HarvestTime / 60 - iHour * 60;
        int iSecond = pFloorNextData->Floor_HarvestTime - iHour * 60 * 60 - iMinute * 60;
        
        // 当前等级收获时间
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20021));
        if (pLabel != NULL)
        {
            if (iHour / 10 == 0)
                sprintf(szTemp, "0%d", iHour);
            else
                sprintf(szTemp, "%d", iHour);
            
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20022));
        if (pLabel != NULL)
        {
            if (iMinute / 10 == 0)
                sprintf(szTemp, "0%d", iMinute);
            else
                sprintf(szTemp, "%d", iMinute);
            
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023));
        if (pLabel != NULL)
        {
            if (iSecond / 10 == 0)
                sprintf(szTemp, "0%d", iSecond);
            else
                sprintf(szTemp, "%d", iSecond);
            
            pLabel->GetNumberLabel()->setString(szTemp);
        }
        
        // 下一等级数字
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20028));
        if (pLabel != NULL)
        {
            sprintf(szTemp, "%d", iNextLevel);
            pLabel->GetNumberLabel()->setString(szTemp);
        }
    }
    else
    {
        // 设置楼层文本标签
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20026));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 隐藏建造花费背景
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20002));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 隐藏建造花费
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20003));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 设置星星等级
        for (int i = 0; i < 5; i++)
        {
            // 下一级楼层等级
            pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20009 + i));
            if (pLabel != NULL)
                pLabel->GetBackSprite()->setIsVisible(false);
        }
        
        // 下一级收获价格
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20016));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 隐藏建造花费
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20017));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 当前等级收获时间
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20021));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20022));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20023));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 隐藏立即升级按钮文本
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20024));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 下一等级数字
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20028));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 背景隐藏
        pLabel = dynamic_cast<KNLabel*>(pFrame->GetSubUIByID(20029));
        if (pLabel != NULL)
            pLabel->setIsVisible(false);
        
        // 立即升级
        KNButton* pButton = dynamic_cast<KNButton*>(pFrame->GetSubUIByID(30002));
        if (pButton != NULL)
            pButton->setIsVisible(false);
    }
}

/************************************************************************/
#pragma mark - Layer_Building类关闭所有升级时间更新函数
/************************************************************************/
void Layer_Building::CloseAllFloorTimeUpdate()
{
    map<int, Floor*>::iterator iter;
    for (iter = m_FloorList.begin(); iter != m_FloorList.end(); iter++)
        iter->second->SetHavestFloorTimeRefreshEnable(false);
}

/************************************************************************/
#pragma mark - Layer_Building类获得需要升级的楼层id函数
/************************************************************************/
int Layer_Building::GetLevelupFloorID()
{
    return m_iCurrentLevelFloorType;
}

/************************************************************************/
#pragma mark - Layer_Building类获得需要升级的楼层等级函数
/************************************************************************/
int Layer_Building::GetLevelupFloorLevel()
{
    map<int, Floor*>::iterator iter = m_FloorList.find(m_iCurrentLevelFloorType);
    if (iter != m_FloorList.end())
        return iter->second->GetFloorData()->Floor_Level;
    
    return NULL;
}

/************************************************************************/
#pragma mark - Layer_Building类是否有楼层处于活动中函数
/************************************************************************/
bool Layer_Building::IsFloorInAction()
{
    map<int, Floor*>::iterator iter = m_FloorList.find(m_iCurrentLevelFloorType);
    for (iter = m_FloorList.begin(); iter != m_FloorList.end(); iter++)
    {
        if (iter->second->GetFloorActionState() == true)
            return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_Building类完成楼层升级函数
/************************************************************************/
void Layer_Building::finishFloorHavest(int iType)
{
    map<int, Floor*>::iterator iter = m_FloorList.find(m_iCurrentLevelFloorType);
    if (iter != m_FloorList.end())
        iter->second->ProcessHavestSucessEvent();
}

/************************************************************************/
#pragma mark - Layer_Building类更新菜单收获状态函数
/************************************************************************/
bool Layer_Building::RefreshHavestMenu(bool bIsMoveTo)
{
    // 检测是否有可收获的楼层
    map<int, Floor*>::iterator iter = m_FloorList.find(m_iCurrentLevelFloorType);
    for (iter = m_FloorList.begin(); iter != m_FloorList.end(); iter++)
    {
        if (iter->second->GetHarvestFloorState() == true)
        {
            if (bIsMoveTo)
            {
                // 移动到该层
                CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.5f, ccp(0.0f, -iter->second->GetFloorData()->Floor_Type * 50.0f));
                if (pMoveTo != NULL)
                    runAction(pMoveTo);
            }
            
            return true;
        }
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_Building类获得可收获的楼层数函数
/************************************************************************/
int Layer_Building::GetEnableHavestFloorNum()
{
    int iNum = 0;
    
    map<int, Floor*>::iterator iter = m_FloorList.find(m_iCurrentLevelFloorType);
    for (iter = m_FloorList.begin(); iter != m_FloorList.end(); iter++)
    {
        if (iter->second->GetHarvestFloorState() == true)
            ++iNum;
    }
    
    return iNum;
}

/************************************************************************/
#pragma mark - Layer_Building类获得楼层对象函数
/************************************************************************/
Floor* Layer_Building::GetFloor(int iType)
{
    map<int, Floor*>::iterator iter = m_FloorList.find(iType);
    if (iter != m_FloorList.end())
        return iter->second;
    
    return NULL;
}

/************************************************************************/
#pragma mark - Layer_Building类显示提示箭头函数
/************************************************************************/
void Layer_Building::ShowGuideArrow(CCPoint pos, ARROW_DIRECTION eDirection)
{
    m_pGuideArrow->setIsVisible(true);
    m_pGuideArrow->stopAllActions();
    m_pGuideArrow->setPosition(pos);
    
    m_eDirection = eDirection;
    
    switch (eDirection) {
        case ARROW_UP:          // 上
            m_pGuideArrow->setRotation(0.0f);
            break;
        case ARROW_DOWN:        // 下
            m_pGuideArrow->setRotation(180.0f);
            break;
        case ARROW_LEFT:        // 左
            m_pGuideArrow->setRotation(-90.0f);
            break;
        case ARROW_RIGHT:       // 右
            m_pGuideArrow->setRotation(90.0f);
            break;
        default:
            break;
    }
    
    ProcessGuideArrowAnim(m_pGuideArrow);
}

/************************************************************************/
#pragma mark - Layer_Building类调整楼层遮罩和箭头位置函数
/************************************************************************/
void Layer_Building::AdjustFloorMaskAndArrowPosition(FLOORTYPE eType)
{
    // 找到楼层位置
    CCPoint pos(0.0f, 0.0f);
    
    map<int, Floor*>::iterator iter = m_FloorList.find(eType);
    if (iter != m_FloorList.end())
    {
        pos = iter->second->getPosition();
        
        if (eType == F_Player)
        {
            // 更新遮罩位置
            m_pFloorMaskUp->setPosition(ccp(pos.x - 160.0f, pos.y + 48.0f));
            m_pFloorMaskDown->setPosition(ccp(pos.x - 160.0f, pos.y - 44.0f - 480.0f));
            
            // 显示引导箭头
            ShowGuideArrow(ccp(pos.x, pos.y + 50.0f), ARROW_DOWN);
        }
        else if (eType == F_Platform)
        {
            // 更新遮罩位置
            m_pFloorMaskUp->setPosition(ccp(pos.x - 160.0f, pos.y + 48.0f));
            m_pFloorMaskDown->setPosition(ccp(pos.x - 160.0f, pos.y - 60.0f - 480.0f));
            
            // 显示引导箭头
            ShowGuideArrow(ccp(pos.x, pos.y + 70.0f), ARROW_DOWN);
        }
        else
        {
            // 更新遮罩位置
            m_pFloorMaskUp->setPosition(ccp(pos.x - 160.0f, pos.y + 26.0f));
            m_pFloorMaskDown->setPosition(ccp(pos.x - 160.0f, pos.y - 60.0f - 480.0f));
            
            // 显示引导箭头
            ShowGuideArrow(ccp(pos.x, pos.y + 50.0f), ARROW_DOWN);
        }
        
        m_pFloorMaskUp->setIsVisible(true);
        m_pFloorMaskDown->setIsVisible(true);
    }
}

/************************************************************************/
#pragma mark - Layer_Building类关闭新手引导图片函数
/************************************************************************/
void Layer_Building::closeAllGuide()
{
    // 关闭箭头
    m_pGuideArrow->stopAllActions();
    removeChild(m_pGuideArrow, true);
    
    // 关闭遮罩
    removeChild(m_pFloorMaskUp, true);
    removeChild(m_pFloorMaskDown, true);
}

/************************************************************************/
#pragma mark - Layer_Building类释放函数
/************************************************************************/
void Layer_Building::Destroy()
{
    // 释放所有楼层
    map<int, Floor*>::iterator iter;
    for (iter = m_FloorList.begin(); iter != m_FloorList.end(); iter++)
        removeChild(iter->second, true);
    m_FloorList.clear();
    
    removeChild(m_pBuildingBase, true);
    removeChild(m_pBuildingBack1, true);
    removeChild(m_pBuildingBack2, true);
    
    for (int i = 0; i < MAX_CLOUD_NUM; i++)
        removeChild(m_pCloud[i], true);
    
    removeChild(m_pFloorMaskUp, true);
    removeChild(m_pFloorMaskDown, true);
    
    // 移除事件处理
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - Layer_Building类回调函数 : 云层移动
/************************************************************************/
void Layer_Building::cloudMove(CCObject* pObject)
{
    // 获得云层对象
    CCSprite* pSprite = dynamic_cast<CCSprite*>(pObject);
    if (pSprite != NULL)
    {
        // 更新背景位置
        map<int, Floor*>::iterator iter = m_FloorList.find(F_Hint);
        if (iter != m_FloorList.end())
        {
            // 设置开始位置
            CCPoint startPos(0.0f, 0.0f);
            startPos.x = rand() % 100 + 320.0f;
            startPos.y = iter->second->getPosition().y + rand() % 300 + 100.0f;
            pSprite->setPosition(startPos);
            
            // 设置结束位置
            CCPoint destPos = iter->second->getPosition();
            destPos.x = -rand() % 200;
            destPos.y = startPos.y;
            
            CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(15.0f, destPos);
            CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Building::cloudMove));
            if (pMoveTo != NULL && pFuncN != NULL)
                pSprite->runAction(CCSequence::actions(pMoveTo, pFuncN, NULL));
        }
    }
}

/************************************************************************/
#pragma mark - Layer_Building类楼层更新函数
/************************************************************************/
void Layer_Building::floorUpdate(ccTime dt)
{
    // 更新背景位置
    map<int, Floor*>::iterator iter = m_FloorList.find(F_Hint);
    if (iter != m_FloorList.end())
    {
        m_pBuildingBack1->setPosition(ccp(160.0f, iter->second->getPosition().y + m_BuildingBack1.y + sss));
        m_pBuildingBack2->setPosition(ccp(160.0f, iter->second->getPosition().y + m_BuildingBack2.y + 80.0f));
    }
    
    // 更新云层位置
//    for (int i = 0; i < MAX_CLOUD_NUM; i++)
//    {
//        CCPoint pos = m_pCloud[i]->getPosition()
//    }
}

/************************************************************************/
#pragma mark - Layer_Building类回调函数 : 处理箭头动画函数
/************************************************************************/
void Layer_Building::ProcessGuideArrowAnim(CCObject* pObject)
{
    // 获得箭头精灵
    CCSprite* pArrow = dynamic_cast<CCSprite*>(pObject);
    if (pArrow == NULL)
        return;
    
    // 获得位置
    CCPoint pos = pArrow->getPosition();
    
    switch (m_eDirection) {
        case ARROW_UP:          // 上
        case ARROW_DOWN:        // 下
        {
            CCMoveTo* pMoveTo1 = CCMoveTo::actionWithDuration(0.5f, ccp(pos.x, pos.y + 5.0f));
            CCMoveTo* pMoveTo2 = CCMoveTo::actionWithDuration(1.0f, ccp(pos.x, pos.y - 10.0f));
            CCMoveTo* pMoveTo3 = CCMoveTo::actionWithDuration(0.5f, pos);
            CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Building::ProcessGuideArrowAnim));
            if (pMoveTo1 != NULL && pMoveTo2 != NULL && pMoveTo3 != NULL && pFuncN != NULL)
                pArrow->runAction(CCSequence::actions(pMoveTo1, pMoveTo2, pMoveTo3, pFuncN, NULL));
        }
            break;
        case ARROW_LEFT:        // 左
        case ARROW_RIGHT:       // 右
        {
            CCMoveTo* pMoveTo1 = CCMoveTo::actionWithDuration(0.5f, ccp(pos.x - 5.0f, pos.y));
            CCMoveTo* pMoveTo2 = CCMoveTo::actionWithDuration(1.0f, ccp(pos.x + 10.0f, pos.y));
            CCMoveTo* pMoveTo3 = CCMoveTo::actionWithDuration(0.5f, pos);
            CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Building::ProcessGuideArrowAnim));
            if (pMoveTo1 != NULL && pMoveTo2 != NULL && pMoveTo3 != NULL && pFuncN != NULL)
                pArrow->runAction(CCSequence::actions(pMoveTo1, pMoveTo2, pMoveTo3, pFuncN, NULL));
        }
            break;
        default:
            break;
    }
}