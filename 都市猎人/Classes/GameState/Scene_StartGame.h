//
//  Scene_StartGame.h
//  都市猎人
//
//  Created by 张 强 on 12-8-31.
//
//

#ifndef _____Scene_StartGame_h
#define _____Scene_StartGame_h

#include "cocos2d.h"
#include "GameController.h"


#define  LayerChild_Touch 0
#define  LayerChild_Layer 1
#define  LayerChild_Change1 2
#define  LayerChild_Change2 3

#define  ActionTag_Touch 0

class Scene_StartGame : public cocos2d::CCLayer
{
public:
    static cocos2d::CCScene * scene();
    
    bool init();
    virtual void onExit();
    
    virtual ~Scene_StartGame();
    LAYER_NODE_FUNC(Scene_StartGame);
    
protected:
    virtual bool ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    virtual void ccTouchMoved(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
    virtual void ccTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
    
    void    TouchOver(cocos2d::CCNode * sender);
    
protected:
    cocos2d::CCSize     m_WinSize;
    
    CCSprite *          pChange1;
    CCSprite *          pChange2;
    cocos2d::CCRect     m_ChangeRect;
    
    cocos2d::CCRect     m_TouchRect;
};


#endif
