//
//  Scene_LogIn.h
//  MidNightCity
//
//  Created by 强 张 on 12-3-30.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Scene_LogIn_h
#define MidNightCity_Scene_LogIn_h

#include "cocos2d.h"
#include "KNUIManager.h"
#include "SystemDataManage.h"
#include "ServerDataManage.h"
#include "PlayerDataManage.h"

USING_NS_CC;

// login场景ui层id
const int LOGIN_UI_ID = 1001;
// login场景系统ui层id
const int LOGIN_SYSTEM_UI_ID = 1001;

class Scene_LogIn : public CCLayer
{
public:
    virtual ~Scene_LogIn();
    
    #pragma mark - 初始化函数
    bool init();
    #pragma mark - 创建场景函数
    static CCScene* scene();
    
    #pragma mark - 退出函数
    virtual void onExit();
    
    #pragma mark - 连接基类初始化
    LAYER_NODE_FUNC(Scene_LogIn);
};

#endif
