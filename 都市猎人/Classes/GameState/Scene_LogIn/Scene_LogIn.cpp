//
//  Scene_LogIn.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-3-30.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Scene_LogIn.h"
#include "Wrapper.h"

/************************************************************************/
#pragma mark - Scene_LogIn类创建场景函数
/************************************************************************/
CCScene* Scene_LogIn::scene()
{
    // 创建场景
    CCScene* scene = CCScene::node();
    if (scene == NULL)
        return NULL;
    
    // 创建登陆图层
    Scene_LogIn* login = Scene_LogIn::node();
    if (login == NULL)
        return NULL;
    
    // 添加图层到场景
    scene->addChild(login);

    return scene;
}

Scene_LogIn::~Scene_LogIn()
{
    printf("Scene_LogIn:: 释放完成\n");
}

/************************************************************************/
#pragma mark - Scene_LogIn类初始化函数
/************************************************************************/
bool Scene_LogIn::init()
{
    // 初始化基类
    if(CCLayer::init() == false)
        return false;
    
    //播放背景音乐
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> playBackgroundMusic("title.mp3", true);
    
    // 添加资源 - login场景资源
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_login-hd.plist");
    
    // 初始化uimanager
    KNUIManager* pUIManager = KNUIManager::CreateManager();
    if (pUIManager != NULL && pUIManager->LoadUIConfigFromFile("loginUI_config.xml") == true)
        addChild(pUIManager, 0, LOGIN_UI_ID);

    return true;
}

/************************************************************************/
#pragma mark - Scene_LogIn类退出函数
/************************************************************************/
void Scene_LogIn::onExit()
{
    // 调用基类退出函数
    CCLayer::onExit();
    
    //停止背景音乐
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> stopBackgroundMusic();
    
    // 移除ui层
    removeChild(getChildByTag(LOGIN_UI_ID), true);
    
    // 移除所有节点
    removeAllChildrenWithCleanup(true);
    this -> removeFromParentAndCleanup(true);
    
    // 移除资源
    RemoveRes("game_login-hd.plist", "game_login-hd.pvr.ccz");
}






