//
//  Scene_Loading.h
//  MidNightCity
//
//  Created by 强 张 on 12-7-18.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Scene_Loading_h
#define MidNightCity_Scene_Loading_h

#include "cocos2d.h"
#include "GameController.h"

#define  ChildLayer    0
#define  ChildAnimation1   1
#define  ChildAnimation2   2
#define  ChildAnimation3   3
#define  ChildWords1            4
#define  ChildWords2            5

#define  LoadingStep_Failed 30

class Scene_Loading : public cocos2d::CCLayer
{
public:
    static cocos2d::CCScene * scene();
    static  Scene_Loading * ShareInstance();
   
protected:
    bool init();
    virtual void onExit();
    virtual ~Scene_Loading();
    
public:
    void    ReadyToGameScene();
    void    ReadyForLoading();
    void    ContinueLoading();
    void    StopLoading();
    
    void    loadingScene_BattleGuide();
    void    loadingScene_Game();
    void    LoadingScene_Battle();
    void    LoadingBackTo_SceneGame();
    
    void    LoadingAnimation();
    
public:
    void    PlayerInfoReqestFailedFunc();
    void    GameLoginReqestFailedFunc();
   
public:
    SS_GET(bool, m_IsLoading, IsLoading);
    
protected:
    cocos2d::CCSize     m_WinSize;
    int                 m_LoadingStep;
    
    bool                m_IsLoading;
    
private:
    static  Scene_Loading * m_Instance;

    int TextInfoIndex;
    const char * LoadingTextInfo[13][2];
};

#endif
