//
//  Scene_Loading.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-7-18.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Scene_Loading.h"
USING_NS_CC;

#include "Scene_Game.h"
#include "Scene_Battle.h"
#include "Scene_StartGame.h"


#pragma mark -
#pragma mark -
#pragma mark -
Scene_Loading * Scene_Loading::m_Instance = NULL;

CCScene * Scene_Loading::scene()
{
    //创建场景
    CCScene * scene = CCScene::node();
    if (scene == NULL)
        return NULL;
    
    //创建登陆图层
    m_Instance = new Scene_Loading();
    m_Instance -> autorelease();
    m_Instance -> init();
    
    scene -> addChild(m_Instance, 0, ChildLayer);
    
    return scene;
}

Scene_Loading * Scene_Loading::ShareInstance()
{
    if(m_Instance)
        return m_Instance;
    return NULL;
}

bool Scene_Loading::init()
{
    if(! CCLayer::init())   return false;
    
    TextInfoIndex = rand() % 13;
    
    LoadingTextInfo[0][0] = "一次消除5个同色方块来发动全屏攻击";
    LoadingTextInfo[0][1] = "那效果必是极好的";
    
    LoadingTextInfo[1][0] = "英雄的嫉妒心最重了";
    LoadingTextInfo[1][1] = "你摸哪个敌人,他们就会海扁之!";
    
    LoadingTextInfo[2][0] = "英雄达到满级时";
    LoadingTextInfo[2][1] = "他们会要求一些奇怪的陪练来转职";
    
    LoadingTextInfo[3][0] = "楼主?没错!就是叫你呐!";
    LoadingTextInfo[3][1] = "你升级可建造更高的楼层哦";
    
    LoadingTextInfo[4][0] = "五星英雄尽是些调皮的家伙";
    LoadingTextInfo[4][1] = "他们以乱入商店吓你一跳为乐";
    
    LoadingTextInfo[5][0] = "陪练不都是酱油!";
    LoadingTextInfo[5][1] = "相同技能的陪练能升级主英雄的技能";
    
    LoadingTextInfo[6][0] = "时钟它总是勤恳的工作";
    LoadingTextInfo[6][1] = "确保每5分钟回复1点行动力";
    
    LoadingTextInfo[7][0] = "自古二楼出英雄";
    LoadingTextInfo[7][1] = "记住这一句话找英雄时就不会跑错楼层了";
    
    LoadingTextInfo[8][0] = "盟友的英雄也能发动队长技能.";
    LoadingTextInfo[8][1] = "好盟友,好基友.";
    
    LoadingTextInfo[9][0] = "长期坐板凳的英雄恳求你解雇他们";
    LoadingTextInfo[9][1] = "你的善举会换得好报";
    
    LoadingTextInfo[10][0] = "有些英雄平时很宅";
    LoadingTextInfo[10][1] = "只有活动关卡能吸引到他们闪亮登场";
    
    LoadingTextInfo[11][0] = "英雄们很喜欢看COMBO的火花";
    LoadingTextInfo[11][1] = "能加成攻击和回复";
    
    LoadingTextInfo[12][0] = "顶楼总是被围观压力很大";
    LoadingTextInfo[12][1] = "他巴望着建造高些享受俯视感觉";
    
    m_WinSize = CCDirector::sharedDirector() -> getWinSize();
    m_LoadingStep = 0;
    m_IsLoading = false;

    CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("Loading_BG.png");
    pSprite -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2));
    this -> addChild(pSprite);
    
    CCSprite * pDian1 = CCSprite::spriteWithSpriteFrameName("LoadingDian.png");
    pDian1 -> setPosition(ccp(204, 30));
    pDian1 -> setOpacity(0);
    this -> addChild(pDian1, 0, ChildAnimation1);
    
    CCSprite * pDian2 = CCSprite::spriteWithSpriteFrameName("LoadingDian.png");
    pDian2 -> setPosition(ccp(204 + 20, 30));
    pDian2 -> setOpacity(0);
    this -> addChild(pDian2, 0, ChildAnimation2);
    
    CCSprite * pDian3 = CCSprite::spriteWithSpriteFrameName("LoadingDian.png");
    pDian3 -> setPosition(ccp(204 + 20 + 20, 30));
    pDian3 -> setOpacity(0);
    this -> addChild(pDian3, 0, ChildAnimation3);
    
    // 初始化systemmanager
    KNUIManager* pSystemUIManager = KNUIManager::CreateSystemManager();
    if (pSystemUIManager != NULL && pSystemUIManager->LoadUIConfigFromFile("LiveUI_config.xml") == true)
        addChild(pSystemUIManager);

    return true;
}

Scene_Loading::~Scene_Loading()
{
    printf("Scene_Loading:: 释放完成 \n");
}

void Scene_Loading::onExit()
{
    dynamic_cast<CCSprite*>(this -> getChildByTag(ChildAnimation1)) -> setOpacity(0.0);
    dynamic_cast<CCSprite*>(this -> getChildByTag(ChildAnimation2)) -> setOpacity(0.0);
    dynamic_cast<CCSprite*>(this -> getChildByTag(ChildAnimation3)) -> setOpacity(0.0);
    
    this -> removeChildByTag(ChildWords1, true);
    this -> removeChildByTag(ChildWords2, true);
    
    m_LoadingStep = 0;
    m_IsLoading = false;
}
#pragma mark -
#pragma mark -
#pragma mark -


void Scene_Loading::ReadyToGameScene()
{
    CCSize pTextSize = CCSizeMake(280, 20);
    
    TextInfoIndex = rand() % 13;
    
    CCLabelTTF * pText1 = CCLabelTTF::labelWithString(LoadingTextInfo[TextInfoIndex][0], pTextSize, CCTextAlignmentCenter, "黑体", 17);
    pText1 -> setPosition(ccp(m_WinSize.width / 2, 85));
    pText1 -> setColor(ccc3(255, 245, 209));
    this -> addChild(pText1, 0, ChildWords1);
    
    CCLabelTTF * pText2 = CCLabelTTF::labelWithString(LoadingTextInfo[TextInfoIndex][1], pTextSize, CCTextAlignmentCenter, "黑体", 17);
    pText2 -> setPosition(ccp(m_WinSize.width / 2, 68));
    pText2 -> setColor(ccc3(255, 245, 209));
    this -> addChild(pText2, 0, ChildWords2);
    
    this -> schedule(schedule_selector(Scene_Loading::LoadingAnimation), 0.5f);
    
    CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(1.2f);
    CCCallFunc * pCallFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Scene_Loading::ReadyForLoading));;
    
    this -> runAction(CCSequence::actions(pDelayTime, pCallFunc, NULL));
}


void Scene_Loading::ReadyForLoading()
{
    m_LoadingStep = 0;
    m_IsLoading = true;
    
    if(GameController::ShareInstance() -> GetGameNextState() == GameState_BattleGuide)
        this -> schedule(schedule_selector(Scene_Loading::loadingScene_BattleGuide));
    else if(GameController::ShareInstance() -> GetGameNextState() == GameState_Game)
        this -> schedule(schedule_selector(Scene_Loading::loadingScene_Game));
    else if(GameController::ShareInstance() -> GetGameNextState() == GameState_Battle)
        this -> schedule(schedule_selector(Scene_Loading::LoadingScene_Battle));
    else if(GameController::ShareInstance() -> GetGameNextState() == GameState_BackToGame)
        this -> schedule(schedule_selector(Scene_Loading::LoadingBackTo_SceneGame));
}

void Scene_Loading::ContinueLoading()
{
    m_LoadingStep ++;
}

void Scene_Loading::StopLoading()
{
    m_LoadingStep = LoadingStep_Failed;
}

void Scene_Loading::LoadingAnimation()  
{
    CCFadeIn * pFadeIn = CCFadeIn::actionWithDuration(0.4f);
    
    CCSprite * Dian1 = dynamic_cast<CCSprite*>(this -> getChildByTag(ChildAnimation1));
    CCSprite * Dian2 = dynamic_cast<CCSprite*>(this -> getChildByTag(ChildAnimation2));
    CCSprite * Dian3 = dynamic_cast<CCSprite*>(this -> getChildByTag(ChildAnimation3));
    
    if(Dian1 -> getOpacity() == 0.0)
        Dian1 -> runAction(pFadeIn);
    else if(Dian2 -> getOpacity() == 0.0)
        Dian2 -> runAction(pFadeIn);
    else if(Dian3 -> getOpacity() == 0.0)
        Dian3 -> runAction(pFadeIn);
    else
    {
        Dian1 -> setOpacity(0);
        Dian2 -> setOpacity(0);
        Dian3 -> setOpacity(0);
        
        Dian1 -> runAction(pFadeIn);
    }
}


#pragma mark -
#pragma mark -
#pragma mark -
void Scene_Loading::loadingScene_BattleGuide()
{
    switch(m_LoadingStep)
    {
        case 0://添加资源 - battle场景资源
        {
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("battle1-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("battle2-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("battle3-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("anim1-hd.plist");
            
            m_LoadingStep ++;
        }break;
            
        case 1://判断是否有玩家信息
        {
            if(strcmp(PlayerDataManage::ShareInstance() -> m_PlayerUserID, "") == 0)
            {
                ServerDataManage::ShareInstance() -> RequestPlayerInfo();
                
                m_LoadingStep ++;
            }else
                m_LoadingStep = 3;
        }break;
            
        case 2: break;
            
        case 3://请求服务器
        {
            //请求服务器登陆，取得登陆玩家信息并构建
            ServerDataManage::ShareInstance() -> RequestEnterGame();
            
            m_LoadingStep ++;
        }break;
            
        case 4: break;
            
        case 5://请求服务器
        {
            //请求好友小弟列表
            ServerDataManage::ShareInstance() -> RequestFriendList();
            
            m_LoadingStep ++;
        }break;
            
        case 6: break;
            
        case 7:
        {
            //请求好友申请列表
            ServerDataManage::ShareInstance() -> RequestApplyList();
            
            m_LoadingStep ++;
        }break;
            
        case 8: break;
            
        case 9://读取本地邮件列表
        {
            PlayerDataManage::ShareInstance() -> ReadMyMailList();
            
            m_LoadingStep ++;
        }break;
         
        case 10://为战斗引导添加假的好友小弟
        {
            Follower * F = Follower::node();
            F -> SetData(SystemDataManage::ShareInstance() -> GetData_ForFollower(1), false);
            
            PlayerDataManage::ShareInstance() -> AddFriendFollowerToTeam(F);
            
            m_LoadingStep ++;
        }break;
            
        case 11://初始化玩家的基础楼层
        {
            PlayerDataManage::ShareInstance() -> InitBaseFloor();
            
            m_LoadingStep ++;
        }break;
            
        case 12://生成引导战斗的战斗信息
        {
            ServerDataManage::ShareInstance() -> ReleaseBattleInfo();
            
            BattleInfo * bInfo = new BattleInfo();
            bInfo -> MapIndex = 1;
            bInfo -> SubMapIndex = 1;
            bInfo -> StepNum = 3;
            bInfo -> ClearLevelDropMoney = 50;
            bInfo -> ClearLevelDropExperience = 110;
            
            map <int, vector <Enemy_Data *> > pEnemyInfoList;
            
            ///////////////////////Step1
            vector <Enemy_Data *> pList1;
            Enemy_Data * BE1 = new Enemy_Data();
            BE1 -> Enemy_Profession = SWORD;
            BE1 -> Enemy_HP = BE1 -> Original_HP = 20;
            BE1 -> Enemy_Attack = BE1 -> Original_Attack = 10;
            BE1 -> Enemy_Defense = BE1 -> Original_Defense = 0;
            BE1 -> Enemy_CDRound = 2;
            memcpy(BE1 -> Enemy_IconName, "follower_fightIcon_401.png", CHARLENGHT);
            BE1 -> Enemy_DropMoney = 0;
            BE1 -> Enemy_DropFollower_ID = 23;
            
            const Follower_Data * FD = SystemDataManage::ShareInstance() -> GetData_ForFollower(23);
            BE1 -> Enemy_DropFollower_CharacterID = 7;
            BE1 -> Enemy_DropFollower_Attack = FD -> Follower_Attack;
            BE1 -> Enemy_DropFollower_Defense = FD -> Follower_Defense;
            BE1 -> Enemy_DropFollower_ComeBack = FD -> Follower_Comeback;
            BE1 -> Enemy_DropFollower_HP = FD -> Follower_HP;
            BE1 -> Enemy_DropFollower_Level = 1;
            BE1 -> Enemy_DropFollower_SkillLevel = 1;
            BE1 -> Enemy_DropFollower_LevelUpExp = 18;
            BE1 -> Enemy_DropFollower_BeAbsorbExp = 200;
            pList1.push_back(BE1);
            pEnemyInfoList[1] = pList1;
            
            ///////////////////////Step2
            vector <Enemy_Data *> pList2;
            for(int i = 0; i < 5; i ++)
            {
                Enemy_Data * BE2 = new Enemy_Data();
                BE2 -> Enemy_Profession = FIST;
                BE2 -> Enemy_HP = BE2 -> Original_HP = 20;
                BE2 -> Enemy_Attack = BE2 -> Original_Attack = 10;
                BE2 -> Enemy_Defense = BE2 -> Original_Defense = 0;
                BE2 -> Enemy_CDRound = 2;
                if(i == 0)           memcpy(BE2 -> Enemy_IconName, "follower_fightIcon_301.png", CHARLENGHT);
                else if(i == 1)      memcpy(BE2 -> Enemy_IconName, "follower_fightIcon_501.png", CHARLENGHT);
                else if(i == 2)      memcpy(BE2 -> Enemy_IconName, "follower_fightIcon_601.png", CHARLENGHT);
                else if(i == 3)      memcpy(BE2 -> Enemy_IconName, "follower_fightIcon_701.png", CHARLENGHT);
                else if(i == 4)      memcpy(BE2 -> Enemy_IconName, "follower_fightIcon_1301.png", CHARLENGHT);
                BE2 -> Enemy_DropMoney = 0;
                BE2 -> Enemy_DropFollower_ID = 0;
                pList2.push_back(BE2);
            }
            pEnemyInfoList[2] = pList2;
            
            ///////////////////////Step3
            vector <Enemy_Data *> pList3;
            Enemy_Data * BE3 = new Enemy_Data();
            BE3 -> Enemy_Profession = GUN;
            BE3 -> Enemy_HP = BE3 -> Original_HP = 20;
            BE3 -> Enemy_Attack = BE3 -> Original_Attack = 20;
            BE3 -> Enemy_Defense = BE3 -> Original_Defense = 0;
            BE3 -> Enemy_CDRound = 1;
            memcpy(BE3 -> Enemy_IconName, "follower_fightIcon_401.png", CHARLENGHT);
            BE3 -> Enemy_DropMoney = 0;
            BE3 -> Enemy_DropFollower_ID = 0;
            pList3.push_back(BE3);
            pEnemyInfoList[3] = pList3;
            
            bInfo -> EnemyInfo_List = pEnemyInfoList;
            ServerDataManage::ShareInstance() -> SetBattleInfo(bInfo);
            
            m_LoadingStep ++;
        }break;
            
        case 13://创建场景，切换场景
        {
            CCScene * pScene = Scene_Battle::scene();
            
            CCTransitionFade * pFade = CCTransitionFade::transitionWithDuration(0.6f, pScene);
            CCDirector::sharedDirector() -> pushScene(pFade);
            
            m_LoadingStep = 0;
            m_IsLoading = false;
            
            this -> unscheduleAllSelectors();
        }break;
            
        case LoadingStep_Failed://出错处理
        {
            CCDirector::sharedDirector() -> getRunningScene() -> removeChildByTag(MessageTag, true);
            
            CCScene * pStart = Scene_StartGame::scene();
            
            CCTransitionFade * pFade = CCTransitionFade::transitionWithDuration(0.6f, pStart);
            CCDirector::sharedDirector() -> pushScene(pFade);
            
            m_LoadingStep = 0;
            m_IsLoading = false;
            
            this -> unscheduleAllSelectors();
        }break;
    }
}

void Scene_Loading::loadingScene_Game()
{
    switch(m_LoadingStep)
    {
        case 0://添加资源 - game场景资源
        {
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_building-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_1-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_2-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_3-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_follower-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("npc-hd.plist");
            
            m_LoadingStep ++;
        }break;
            
        case 1://判断是否有玩家信息
        {
            if(strcmp(PlayerDataManage::ShareInstance() -> m_PlayerUserID, "") == 0)
            {
                ServerDataManage::ShareInstance() -> RequestPlayerInfo();
                
                m_LoadingStep ++;
            }else
                m_LoadingStep = 3;
        }break;
            
        case 2: break;
            
        case 3://请求服务器
        {
            //请求服务器登陆，取得登陆玩家信息并构建
            ServerDataManage::ShareInstance() -> RequestEnterGame();
             
            m_LoadingStep ++;
        }break;
            
        case 4: break;
            
        case 5://请求服务器
        {
            //请求好友小弟列表
            ServerDataManage::ShareInstance() -> RequestFriendList();
            
            m_LoadingStep ++;
        }break;
            
        case 6: break;
            
        case 7://读取本地公告
        {
            ServerDataManage::ShareInstance() -> RequestNotice();
            
            m_LoadingStep ++;
        }break;
            
        case 8: break;
            
        case 9:
        {
            //请求好友申请列表
            ServerDataManage::ShareInstance() -> RequestApplyList();
            
            m_LoadingStep ++;
        }break;
            
        case 10: break;
        
        case 11://读取本地邮件列表
        {
            PlayerDataManage::ShareInstance() -> ReadMyMailList();
            
            m_LoadingStep ++;
        }break;
            
        case 12://初始化玩家的基础楼层
        {
            PlayerDataManage::ShareInstance() -> InitBaseFloor();
            
            m_LoadingStep ++;
        }break;
            
        case 13://创建场景，切换场景
        {
            CCScene * pScene = Scene_Game::scene();
            
            CCTransitionFade * pFade = CCTransitionFade::transitionWithDuration(0.6f, pScene);
            CCDirector::sharedDirector() -> pushScene(pFade);
            
            m_LoadingStep = 0;
            m_IsLoading = false;
            
            this -> unscheduleAllSelectors();
        }break;
            
        case LoadingStep_Failed://出错处理
        {
            CCDirector::sharedDirector() -> getRunningScene() -> removeChildByTag(MessageTag, true);
            
            CCScene * pStart = Scene_StartGame::scene();
            
            CCTransitionFade * pFade = CCTransitionFade::transitionWithDuration(0.6f, pStart);
            CCDirector::sharedDirector() -> pushScene(pFade);
            
            m_LoadingStep = 0;
            m_IsLoading = false;
            
            this -> unscheduleAllSelectors();
        }break;
    }
}

void Scene_Loading::LoadingScene_Battle()
{
    switch(m_LoadingStep)
    {
        case 0://添加资源 - battle场景资源
        {
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("battle1-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("battle2-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("battle3-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("anim1-hd.plist");
            
            m_LoadingStep ++;
        }break;
            
        case 1://请求服务器进入副本，处理副本战斗信息
        {
            ServerDataManage::ShareInstance() -> RequestEnterBattle();
            
            m_LoadingStep ++;
        }break;
            
        case 2: break;
            
        case 3://创建场景，切换场景
        {
            CCScene * pScene = Scene_Battle::scene();
            
            CCTransitionFade * pFade = CCTransitionFade::transitionWithDuration(0.6f, pScene);
            CCDirector::sharedDirector() -> pushScene(pFade);
            
            m_LoadingStep = 0;
            m_IsLoading = false;
            
            this -> unscheduleAllSelectors();
        }break;
            
        case LoadingStep_Failed://出错处理
        {
            RemoveRes("battle1-hd.plist", "battle1-hd.pvr.ccz");
            RemoveRes("battle2-hd.plist", "battle2-hd.pvr.ccz");
            RemoveRes("battle3-hd.plist", "battle3-hd.pvr.ccz");
            RemoveRes("anim1-hd.plist", "anim1-hd.pvr.ccz");
            
            CCDirector::sharedDirector() -> getRunningScene() -> removeChildByTag(MessageTag, true);
        
            m_LoadingStep = 0;
            
            this -> unschedule(schedule_selector(Scene_Loading::LoadingScene_Battle));
            this -> schedule(schedule_selector(Scene_Loading::LoadingBackTo_SceneGame));
        }break;
    }
}

void Scene_Loading::LoadingBackTo_SceneGame()
{
    switch(m_LoadingStep)
    {
        case 0://添加资源 - game场景资源
        {
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_building-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_1-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_2-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_3-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_follower-hd.plist");
            CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("npc-hd.plist");
    
            m_LoadingStep ++;
        }break;
            
        case 1://留存的数据重新绑定图片资源
        {
            PlayerDataManage::ShareInstance() -> ReSetDataImage();
            
            m_LoadingStep ++;
        }break;
            
        case 2:
        {
            ServerDataManage::ShareInstance() -> RequestFriendList();
            
            m_LoadingStep ++;
        }break;
            
        case 3: break;
            
        case 4:
        {
            ServerDataManage::ShareInstance() -> RequestApplyList();
            
            m_LoadingStep ++;
        }break;
            
        case 5: break;
            
        case 6:
        {
            PlayerDataManage::ShareInstance() -> ReadMyMailList();
            
            m_LoadingStep ++;
        }break;
            
        case 7://创建场景，切换场景
        {
            CCScene * pScene = Scene_Game::scene();
            
            CCTransitionFade * pFade = CCTransitionFade::transitionWithDuration(0.6f, pScene);
            CCDirector::sharedDirector() -> pushScene(pFade);
            
            m_LoadingStep = 0;
            m_IsLoading = false;
            
            this -> unscheduleAllSelectors();
        }break;
            
        case LoadingStep_Failed://出错处理
        {
            CCDirector::sharedDirector() -> getRunningScene() -> removeChildByTag(MessageTag, true);
            
            m_LoadingStep = 7;
            
        }break;
    }
}
#pragma mark -
#pragma mark -
#pragma mark -






























