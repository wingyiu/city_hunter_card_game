//
//  Layer_SkillInfoBoard.cpp
//  都市猎人
//
//  Created by 张 强 on 12-9-29.
//
//

#include "Layer_SkillInfoBoard.h"
USING_NS_CC;

#include "Follower.h"
#include "Battle_Follower.h"
#include "SystemDataManage.h"
#include "PlayerDataManage.h"
#include "Layer_Display.h"
#include "Layer_GameGuide.h"

Layer_SkillInfoBoard * Layer_SkillInfoBoard::InitBoard(Battle_Follower * host)
{
    Layer_SkillInfoBoard * LS = new Layer_SkillInfoBoard();
    LS -> SetInfoHost(host);
    LS -> init();
    LS -> autorelease();
    return LS;
}

bool Layer_SkillInfoBoard::init()
{
    if(! CCLayer::init())
        return false;
    
    m_WinSize = CCDirector::sharedDirector() -> getWinSize();
    
    this -> setIsTouchEnabled(true);
    CCTouchDispatcher::sharedDispatcher() -> addTargetedDelegate(this, 0, true);
    
    this -> setScale(0.2f);
    
    //背景
    CCSprite * pBG = CCSprite::spriteWithSpriteFrameName("SkillTrigger.png");
    pBG -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2));
    this -> addChild(pBG, 0, BoardChildTag_BG);
    
    //释放按钮
    if(m_InfoHost -> GetSkillCDRecord() == 0)
    {
        m_CanbeTrigger = true;
        
        CCSprite * pBtn1 = CCSprite::spriteWithSpriteFrameName("button_yellow_normal.png");
        pBtn1 -> setPosition(ccp(94, 155));
        
        CCSprite * pBtn1_1 = CCSprite::spriteWithSpriteFrameName("button_yellow_select.png");
        pBtn1_1 -> setIsVisible(false);
        pBtn1_1 -> setPosition(ccp(94, 155));
        
        CCSprite * ShiFang = CCSprite::spriteWithSpriteFrameName("button_skill_shifang.png");
        ShiFang -> setPosition(ccp(94, 155));
        
        this -> addChild(pBtn1, 0, BoardChildTag_BTN_Left);
        this -> addChild(pBtn1_1, 0, BoardChildTag_BTN_Left_S);
        this -> addChild(ShiFang, 0, BoardChildTag_BTN_LetWord);
        
        m_Rect_ShiFang = CCRectMake(94 - pBtn1 -> getTextureRect().size.width / 2, 155 - pBtn1 -> getTextureRect().size.height / 2,
                                    pBtn1 -> getTextureRect().size.width, pBtn1 -> getTextureRect().size.height);
    }
    else
    {
        m_CanbeTrigger = false;
        
        CCSprite * pBtn1 = CCSprite::spriteWithSpriteFrameName("button_gray.png");
        pBtn1 -> setPosition(ccp(94, 155));
        
        CCSprite * ShiFang = CCSprite::spriteWithSpriteFrameName("button_skill_shifang.png");
        ShiFang -> setPosition(ccp(94, 155));
        
        this -> addChild(pBtn1, 0, BoardChildTag_BTN_Left);
        this -> addChild(ShiFang, 0, BoardChildTag_BTN_LetWord);
        
        m_Rect_ShiFang = CCRectMake(94 - pBtn1 -> getTextureRect().size.width / 2, 155 - pBtn1 -> getTextureRect().size.height / 2,
                                    pBtn1 -> getTextureRect().size.width, pBtn1 -> getTextureRect().size.height);
    }

    //取消按钮
    CCSprite * pBtn2 = CCSprite::spriteWithSpriteFrameName("button_green_normal.png");
    pBtn2 -> setPosition(ccp(226, 155));
    
    CCSprite * pBtn2_1 = CCSprite::spriteWithSpriteFrameName("button_green_select.png");
    pBtn2_1 -> setIsVisible(false);
    pBtn2_1 -> setPosition(ccp(226, 155));
    
    CCSprite * QuXiao = CCSprite::spriteWithSpriteFrameName("button_skill_quxiao.png");
    QuXiao -> setPosition(ccp(226, 155));
    
    this -> addChild(pBtn2, 0, BoardChildTag_BTN_Right);
    this -> addChild(pBtn2_1, 0, BoardChildTag_BTN_Right_S);
    this -> addChild(QuXiao, 0, BoardChildTag_BTN_RightWord);
    m_Rect_QuXiao = CCRectMake(226 - pBtn2 -> getTextureRect().size.width / 2, 155 - pBtn2 -> getTextureRect().size.height / 2,
                               pBtn2 -> getTextureRect().size.width, pBtn2 -> getTextureRect().size.height);
    
    ////////////////////////////////////////////////

    //小弟的名称
    CCSize pTextSize1 = CCSizeMake(170, 17);
    CCLabelTTF * pText1 = CCLabelTTF::labelWithString(m_InfoHost -> GetHost() -> GetData() -> Follower_Name, pTextSize1, CCTextAlignmentCenter, "AmericanTypewriter-Bold", 17);
    pText1 -> setPosition(ccp(m_WinSize.width / 2, 328));
    pText1 -> setColor(ccc3(255, 222, 0));
    this -> addChild(pText1, 0, BoardChildTag_Word_FollowerName);
    
    //技能名称
    int SkillType = m_InfoHost -> GetHost() -> GetData() -> Follower_CommonlySkillType;
    int SkillID = m_InfoHost -> GetHost() -> GetData() -> Follower_CommonlySkillID;
    const Skill_Data * SD = SystemDataManage::ShareInstance() -> GetData_ForSkill(SkillType, SkillID);
    
    CCSize pTextSize2 = CCSizeMake(170, 13);
    CCLabelTTF * pText2 = CCLabelTTF::labelWithString(SD -> Skill_Name, pTextSize2, CCTextAlignmentLeft, "AmericanTypewriter-Bold", 13);
    pText2 -> setAnchorPoint(ccp(0, 1));
    pText2 -> setPosition(ccp(89, 308));
    pText2 -> setColor(ccc3(138, 255, 0));
    this -> addChild(pText2, 0, BoardChildTag_Word_SkillName);
    
    //技能等级
    CCSize pTextSize3 = CCSizeMake(170, 13);
    char buff[32];
    sprintf(buff, "%d", m_InfoHost -> GetHost() -> GetData() -> Follower_SkillLevel);
    CCLabelTTF * pText3 = CCLabelTTF::labelWithString(buff, pTextSize3, CCTextAlignmentLeft, "AmericanTypewriter-Bold", 13);
    pText3 -> setAnchorPoint(ccp(0, 1));
    pText3 -> setPosition(ccp(89, 285));
    pText3 -> setColor(ccc3(255, 255, 255));
    this -> addChild(pText3, 0, BoardChildTag_Word_SkillLevel);
    
    //技能描述
    CCSize pTextSize4 = CCSizeMake(170, 13 * 4);
    CCLabelTTF * pText4 = CCLabelTTF::labelWithString(SD -> Skill_Depict, pTextSize4, CCTextAlignmentLeft, "AmericanTypewriter-Bold", 13);
    pText4 -> setAnchorPoint(ccp(0, 0.5));
    pText4 -> setPosition(ccp(89, 242));
    pText4 -> setColor(ccc3(126, 220, 16));
    this -> addChild(pText4, 0, BoardChildTag_Word_SkillInfo);
   
    //技能CD
    CCSize pTextSize5 = CCSizeMake(13 * 4, 13);
    sprintf(buff, "%d", m_InfoHost -> GetSkillCDRecord());
    CCLabelTTF * pText5 = CCLabelTTF::labelWithString(buff, pTextSize5, CCTextAlignmentCenter, "AmericanTypewriter-Bold", 13);
    pText5 -> setPosition(ccp(132, 200));
    pText5 -> setColor(ccc3(255, 255, 255));
    this -> addChild(pText5, 0, BoardChildTag_Word_SkillCD);
    
    //出现动作
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.2f, 1.0f);
    CCEaseBackOut * pBig = CCEaseBackOut::actionWithAction(pScaleTo);
    this -> runAction(pBig);
    
    return true;
}

Layer_SkillInfoBoard::~Layer_SkillInfoBoard()
{
    printf("Layer_SkillInfoBoard::释放完成\n");
}

void Layer_SkillInfoBoard::onExit()
{
    CCLayer::onExit();
    
    this -> removeAllChildrenWithCleanup(true);
    CCTouchDispatcher::sharedDispatcher() -> removeDelegate(this);
    
    m_InfoHost = NULL;
}

void Layer_SkillInfoBoard::Leave()
{
    //出现动作
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.2f, 0.0f);
    CCEaseBackIn * pSmall = CCEaseBackIn::actionWithAction(pScaleTo);
    CCCallFunc * pCallFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_SkillInfoBoard::LeaveFunc1));
    this -> runAction(CCSequence::actions(pSmall, pCallFunc, NULL));
}

void Layer_SkillInfoBoard::LeaveFunc1()
{
    this -> removeFromParentAndCleanup(true);
}



bool Layer_SkillInfoBoard::ccTouchBegan(CCTouch * pTouch, CCEvent * pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    //如果是战斗的引导
    if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
    {
        if(CCRect::CCRectContainsPoint(m_Rect_ShiFang, point))
        {
            if(m_CanbeTrigger)
            {
                this -> getChildByTag(BoardChildTag_BTN_Left_S) -> setIsVisible(true);
                this -> getChildByTag(BoardChildTag_BTN_Left) -> setIsVisible(false);
            }
        }
    }
    else
    {
        if(CCRect::CCRectContainsPoint(m_Rect_ShiFang, point))
        {
            if(m_CanbeTrigger)
            {
                this -> getChildByTag(BoardChildTag_BTN_Left_S) -> setIsVisible(true);
                this -> getChildByTag(BoardChildTag_BTN_Left) -> setIsVisible(false);
            }
        }
        else if(CCRect::CCRectContainsPoint(m_Rect_QuXiao, point))
        {
            this -> getChildByTag(BoardChildTag_BTN_Right_S) -> setIsVisible(true);
            this -> getChildByTag(BoardChildTag_BTN_Right) -> setIsVisible(false);
        }
    }
    return true;
}

void Layer_SkillInfoBoard::ccTouchMoved(CCTouch * pTouch, CCEvent * pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    //如果是战斗的引导
    if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
    {
        if(CCRect::CCRectContainsPoint(m_Rect_ShiFang, point))
        {
            if(m_CanbeTrigger)
            {
                this -> getChildByTag(BoardChildTag_BTN_Left_S) -> setIsVisible(true);
                this -> getChildByTag(BoardChildTag_BTN_Left) -> setIsVisible(false);
            }
        }else
        {
            if(m_CanbeTrigger)
            {
                this -> getChildByTag(BoardChildTag_BTN_Left_S) -> setIsVisible(false);
                this -> getChildByTag(BoardChildTag_BTN_Left) -> setIsVisible(true);
            }
        }
    }
    else
    {
        if(CCRect::CCRectContainsPoint(m_Rect_ShiFang, point))
        {
            if(m_CanbeTrigger)
            {
                this -> getChildByTag(BoardChildTag_BTN_Left_S) -> setIsVisible(true);
                this -> getChildByTag(BoardChildTag_BTN_Left) -> setIsVisible(false);
            }
        }else
        {
            if(m_CanbeTrigger)
            {
                this -> getChildByTag(BoardChildTag_BTN_Left_S) -> setIsVisible(false);
                this -> getChildByTag(BoardChildTag_BTN_Left) -> setIsVisible(true);
            }
        }
        
        if(CCRect::CCRectContainsPoint(m_Rect_QuXiao, point))
        {
            this -> getChildByTag(BoardChildTag_BTN_Right_S) -> setIsVisible(true);
            this -> getChildByTag(BoardChildTag_BTN_Right) -> setIsVisible(false);
        }else
        {
            this -> getChildByTag(BoardChildTag_BTN_Right_S) -> setIsVisible(false);
            this -> getChildByTag(BoardChildTag_BTN_Right) -> setIsVisible(true);
        }
    }
}

void Layer_SkillInfoBoard::ccTouchEnded(CCTouch * pTouch, CCEvent * pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    //如果是战斗的引导
    if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
    {
        if(CCRect::CCRectContainsPoint(m_Rect_ShiFang, point))
        {
            if(m_CanbeTrigger)
            {
                Layer_GameGuide::GetSingle() -> FinishBattleGuideStep6();
                
                this -> getChildByTag(BoardChildTag_BTN_Left) -> setIsVisible(true);
                this -> getChildByTag(BoardChildTag_BTN_Left_S) -> setIsVisible(false);
                
                Leave();
                Layer_Display::ShareInstance() -> OperationEnable(false);
                Layer_Display::ShareInstance() -> DisplaySkillStart(m_InfoHost);
            }
        }
    }
    else
    {
        if(CCRect::CCRectContainsPoint(m_Rect_ShiFang, point))
        {
            if(m_CanbeTrigger)
            {
                this -> getChildByTag(BoardChildTag_BTN_Left) -> setIsVisible(true);
                this -> getChildByTag(BoardChildTag_BTN_Left_S) -> setIsVisible(false);
                
                Leave();
                Layer_Display::ShareInstance() -> OperationEnable(false);
                Layer_Display::ShareInstance() -> DisplaySkillStart(m_InfoHost);
            }
        }
        else if(CCRect::CCRectContainsPoint(m_Rect_QuXiao, point))
        {
            this -> getChildByTag(BoardChildTag_BTN_Right_S) -> setIsVisible(false);
            this -> getChildByTag(BoardChildTag_BTN_Right) -> setIsVisible(true);
            
            Leave();
        }
    }
}

































