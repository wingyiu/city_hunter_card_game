//
//  Battle_Follower.h
//  MidNightCity
//
//  Created by 强 张 on 12-5-26.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Battle_Follower_h
#define MidNightCity_Battle_Follower_h

#include "cocos2d.h"
#include "MyConfigure.h"
#define FollowerTag_Icon  0
#define FollowerTag_IconFrame 1
#define FollowerTag_Label 2
#define FollowerTag_LightFrame 3
#define FollowerTag_CDFrame 4
#define FollowerTag_CD 5

struct DeBuff
{
    int Buff_StartRound;
    int Buff_KeepRound;
    int Buff_SkillType;
    int Buff_SkillID;
    int Buff_Value;
};

class Follower;
class Battle_Enemy;
class Battle_Follower : public cocos2d::CCLayer
{
    friend class Battle_FollowerManage;
    
public:
    virtual bool init();
    ~Battle_Follower();
    void   onExit();
    
    MY_LAYER_NODE_FUNC(Battle_Follower);
    virtual void Initialize();
  
public:
    void CalculateAttackEnemy();

public:
    void DisplayNumber();
    void DisplayNumberJump();
    void DisplayNumberJumpFun1();
    
public:
    void DisplayAttack();
    void RemoveLabelAttack(cocos2d::CCNode * sender);
    void RemoveAttackBall(cocos2d::CCNode * sender);
    void RemoveAllAttackBall(cocos2d::CCNode * sender);
    void DisplayAttackAllEnemy();
    
public:    
    bool CheckBeTouch(cocos2d::CCPoint point);
    void SkillAnimationOver(cocos2d::CCNode * sender);
    
public:
    void DecreaseSkillCDTime();
    void BeenSkillCDTime();
    void HideSkillCD();
    
    void GuideReSetSkillCD();
    
    void TriggerTeamSkill();
    void TriggerSkillAnimation();
    void CheckEnemySkillAnimationOver();
    void CheckFollowerSkillAnimationOver();
    void TriggerSkill();    
    
    void SkillAttackOneEnemy(Battle_Enemy * BE);
    
    void SkillAttackAllEnemy();
    void SkillAttackAllEnemyFunc1();
    
public:
    void DisplaySkillAnimation(Battle_Follower * sender);
    void RemoveSkillAnimation(cocos2d::CCNode * sender);
    
public:
    GET_SET(Follower *, m_Host, Host);
    GET_SET(bool, m_IsUnderShowAttackNumber, UnderShowAttackNumber);
    
public:
    SS_GET(Battle_Enemy*, m_Target, Target);
    SS_GET(bool, m_IsDisplaySkillAnimationOver, DisplaySkillAnimationOver);
    SS_GET(int, m_SkillCDRecord, SkillCDRecord);
    
public:   
    SS_SET(int, m_AddedAttack, AddedAttack);
    SS_SET(bool, m_IsAttackAllEnemy, IsAttackAllEnemy);
    
protected:
    cocos2d::CCRect             m_RealSize;
    cocos2d::CCPoint            m_TargetPosition;
    cocos2d::CCLabelAtlas *     m_LabelAttack;
    cocos2d::CCSize             m_NumSize_Attack;
  
private:
    //小弟的主体
    Follower    *       m_Host;
    //目标
    Battle_Enemy *      m_Target;
    
private:
    char                m_AttackBallName[CHARLENGHT];
    char                m_AttackNumberName[CHARLENGHT];
    
private:    
    int                 m_AddedAttack;
    int                 m_RealAttack;
    int                 m_LabelAttackString;
    
    int                 m_SkillCDRound;
    int                 m_SkillCDRecord;
    
    bool                m_IsAttackAllEnemy;
    bool                m_IsUnderShowAttackNumber;
    bool                m_IsDisplaySkillAnimationOver;
    
    list    <Battle_Enemy*>    m_TempEnemyList1; 
    vector  <Battle_Enemy*>    m_TempEnemyList2; 
};

#endif


















