//
//  Action_MyMove.h
//  都市猎人
//
//  Created by 张 强 on 12-9-26.
//
//

#ifndef _____Action_MyMove_h
#define _____Action_MyMove_h

#include "cocos2d.h"
    
class Action_MyMove : public cocos2d::CCActionInterval
{
public:
    bool initWithDuration(int times, int deep, cocos2d::ccTime speed);
    
    virtual void startWithTarget(cocos2d::CCNode * pTarget);
    virtual void update(cocos2d::ccTime time);
	virtual bool isDone(void);
    
public:
	static Action_MyMove * actionWithDuration(int times, int deep, cocos2d::ccTime speed);
    
private:
    void    MakeDestPoint();
    void    MoveOver(cocos2d::CCNode * sender);
    
protected:
    bool                    m_IsMoving;
    
    int                     m_MoveTimes;
    int                     m_MoveCount;
    int                     m_MoveDeep;
    cocos2d::ccTime         m_MoveDuration;
    cocos2d::ccTime         m_MoveSpeed;
    
    cocos2d::CCPoint        m_StartPosition;
    cocos2d::CCPoint        m_DestPosition;
};

#endif
