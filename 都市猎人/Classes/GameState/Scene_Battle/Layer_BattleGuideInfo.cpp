//
//  Layer_BattleGuideInfo.cpp
//  都市猎人
//
//  Created by 张 强 on 12-10-9.
//
//

#include "Layer_BattleGuideInfo.h"
USING_NS_CC;

#include "Layer_Enemy.h"
#include "Layer_Follower.h"
#include "Layer_Block.h"
#include "Layer_GameGuide.h"
#include "Layer_Display.h"


Layer_BattleGuideInfo * Layer_BattleGuideInfo::InitBoard(int step)
{
    Layer_BattleGuideInfo * LB = new Layer_BattleGuideInfo();
    LB -> init(step);
    LB -> autorelease();
    
    return LB;
}

bool Layer_BattleGuideInfo::init(int step)
{
    if(! CCLayer::init())
        return false;
    
    m_WinSize = CCDirector::sharedDirector() -> getWinSize();
    m_InfoStep = step;
    
    this -> setIsTouchEnabled(true);
    CCTouchDispatcher::sharedDispatcher() -> addTargetedDelegate(this, 0, true);
    
    this -> setScale(0.2f);
    
    //背景
    CCSprite * pBG = CCSprite::spriteWithSpriteFrameName("guide_back.png");
    pBG -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2));
    this -> addChild(pBG);
    
    //信息
    char buff[32];
    sprintf(buff, "BattleGuideInfo%d.png", step);
    CCSprite * pInfo = CCSprite::spriteWithSpriteFrameName(buff);
    if(step == 1)
        pInfo -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2));
    else
        pInfo -> setPosition(ccp(m_WinSize.width / 2 + 22, m_WinSize.height / 2));
    
    this -> addChild(pInfo);
    
    //出现动作
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.2f, 1.0f);
    CCEaseBackOut * pBig = CCEaseBackOut::actionWithAction(pScaleTo);
    this -> runAction(pBig);
    
    return true;
}

Layer_BattleGuideInfo::~Layer_BattleGuideInfo()
{
    printf("Layer_BattleGuideInfo::释放完成\n");
}

void Layer_BattleGuideInfo::onExit()
{
    CCLayer::onExit();
    
    this -> removeAllChildrenWithCleanup(true);
    CCTouchDispatcher::sharedDispatcher() -> removeDelegate(this);
}


bool Layer_BattleGuideInfo::ccTouchBegan(CCTouch * pTouch, CCEvent * pEvent)
{
    return true;
}

void Layer_BattleGuideInfo::ccTouchMoved(CCTouch * pTouch, CCEvent * pEvent)
{
    
}

void Layer_BattleGuideInfo::ccTouchEnded(CCTouch * pTouch, CCEvent * pEvent)
{
    //离开动作
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.2f, 0.0f);
    CCEaseBackIn * pSmall = CCEaseBackIn::actionWithAction(pScaleTo);
    CCCallFunc * pCallFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleGuideInfo::Leave));
    this -> runAction(CCSequence::actions(pSmall, pCallFunc, NULL));
}

void Layer_BattleGuideInfo::Leave()
{
    this -> removeFromParentAndCleanup(true);
    
    if(m_InfoStep == 1)
    {
        CCSprite * Guide1 = CCSprite::spriteWithSpriteFrameName("BattleGuide1.png");
        Guide1 -> setPosition(ccp(123, 445));
        
        CCSprite * Guide2 = CCSprite::spriteWithSpriteFrameName("BattleGuide2.png");
        Guide2 -> setPosition(ccp(216, 445));
        
        CCSprite * Guide3 = CCSprite::spriteWithSpriteFrameName("BattleGuide3.png");
        Guide3 -> setPosition(ccp(95, 381));
        
        CCSprite * Guide4 = CCSprite::spriteWithSpriteFrameName("BattleGuide4.png");
        Guide4 -> setPosition(ccp(275, 327));
        
        CCSprite * Guide5 = CCSprite::spriteWithSpriteFrameName("BattleGuide5.png");
        Guide5 -> setPosition(ccp(51, 327));
        
        Layer_Display::ShareInstance() -> addChild(Guide1, 0, Layer_DisplayChild_GuideInfo1);
        Layer_Display::ShareInstance() -> addChild(Guide2, 0, Layer_DisplayChild_GuideInfo2);
        Layer_Display::ShareInstance() -> addChild(Guide3, 0, Layer_DisplayChild_GuideInfo3);
        Layer_Display::ShareInstance() -> addChild(Guide4, 0, Layer_DisplayChild_GuideInfo4);
        Layer_Display::ShareInstance() -> addChild(Guide5, 0, Layer_DisplayChild_GuideInfo5);
    }
    else if(m_InfoStep == 2)
    {
        Layer_Block::ShareInstance() -> SetOperationEnable(true);
        Layer_Enemy::ShareInstance() -> SetOperationEnable(false);
        
        Layer_GameGuide::GetSingle() -> RunBattleGuideStep1();
        
        Layer_Enemy::ShareInstance() -> SetGuideStep(2);
    }
    else if(m_InfoStep == 3)
    {
        Layer_Block::ShareInstance() -> SetOperationEnable(true);
        Layer_Enemy::ShareInstance() -> SetOperationEnable(false);
        
        Layer_GameGuide::GetSingle() -> RunBattleGuideStep2();
        
        Layer_Enemy::ShareInstance() -> SetGuideStep(3);
    }
    else if(m_InfoStep == 4)
    {
        Layer_Block::ShareInstance() -> SetOperationEnable(true);
        Layer_Enemy::ShareInstance() -> SetOperationEnable(false);
        
        Layer_GameGuide::GetSingle() -> RunBattleGuideStep3();
        
        Layer_Enemy::ShareInstance() -> SetGuideStep(4);
    }
    else if(m_InfoStep == 5)
    {
        Layer_Block::ShareInstance() -> SetOperationEnable(false);
        Layer_Enemy::ShareInstance() -> SetOperationEnable(false);
        Layer_Follower::ShareInstance() -> SetOperationEnable(true);
        Layer_Follower::ShareInstance() -> GuideReSetSkillCD();
        
        Layer_GameGuide::GetSingle() -> RunBattleGuideStep5();
        
        Layer_Enemy::ShareInstance() -> SetGuideStep(6);
    }
}






















































