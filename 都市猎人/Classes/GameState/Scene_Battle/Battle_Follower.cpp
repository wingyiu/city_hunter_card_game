//
//  Battle_Follower.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-5-26.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Battle_Follower.h"
USING_NS_CC;

#include "Follower.h"
#include "Layer_Follower.h"

#include "Enemy.h"
#include "Battle_Enemy.h"
#include "Layer_Enemy.h"

#include "Layer_Display.h"
#include "Layer_SkillInfoBoard.h"

#include "Skill.h"
#include "SystemDataManage.h"


#pragma mark 战斗中小弟包装
#pragma mark -


/************************************************************************/
#pragma mark 战斗小弟初始化
/************************************************************************/
bool Battle_Follower::init()
{
    if(! CCLayer::init()) return false;
    
    m_Host = NULL;
    m_Target = NULL;
    
    memset(m_AttackBallName, 0, sizeof(m_AttackBallName));
    
    m_AddedAttack = 0;
    m_RealAttack = 0;
    m_LabelAttackString = 0;
    
    m_SkillCDRound = 0;
    m_SkillCDRecord = 0;
    
    m_IsAttackAllEnemy = false;
    m_IsUnderShowAttackNumber = false;
    m_IsDisplaySkillAnimationOver = true;
    
    m_RealSize = CCRectMake(0, 0, 0, 0);
    m_TargetPosition = CCPointMake(0, 0);
    m_LabelAttack = NULL;
    m_NumSize_Attack = CCSizeMake(0, 0);

    return true;
}

Battle_Follower::~Battle_Follower()
{
    printf("Battle_Follower:: 释放完成\n");
}

void Battle_Follower::onExit()
{
    this -> removeAllChildrenWithCleanup(true);
    m_LabelAttack -> release();
    m_TempEnemyList1.clear();
    m_TempEnemyList2.clear();
    
    m_Host = NULL;
    m_Target = NULL;
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 战斗小弟初始化数据
/************************************************************************/
void Battle_Follower::Initialize()
{
    ////////小弟图标////////
    CCSprite * Icon = CCSprite::spriteWithSpriteFrameName(m_Host -> GetData() -> Follower_IconName);
    Icon -> setPosition(ccp(0 + 4, 0));
    this -> addChild(Icon, 0, FollowerTag_Icon);
    
    ////////小弟图标的外框////////
    char buf[32];
    sprintf(buf, "FollowerIcon_%d.png", m_Host -> GetData() -> Follower_Profession);
    CCSprite * IconFrame = CCSprite::spriteWithSpriteFrameName(buf);
    m_RealSize.size.width = IconFrame -> getTextureRect().size.width;
    m_RealSize.size.height = IconFrame -> getTextureRect().size.height;
    this -> addChild(IconFrame, 0, FollowerTag_IconFrame);
    
    if(m_Host -> GetData() -> Follower_CommonlySkillType && m_Host -> GetData() -> Follower_CommonlySkillID)
    {
        ////////小弟CD进度条的外框////////
        sprintf(buf, "SkillCD_Frame%d.png", m_Host -> GetData() -> Follower_Profession);
        CCSprite * CDFrame = CCSprite::spriteWithSpriteFrameName(buf);
        CDFrame -> setPosition(ccp(0, 0 - Icon -> getTextureRect().size.height / 2 - 2));
        this -> addChild(CDFrame, 0, FollowerTag_CDFrame);
        
        ////////小弟CD进度条////////
        sprintf(buf, "SkillCD%d.png", m_Host -> GetData() -> Follower_Profession);
        CCSprite * CD = CCSprite::spriteWithFile(buf);
        CD -> setAnchorPoint(ccp(0, 0.5));
        CD -> setPosition(ccp(0 - CDFrame -> getTextureRect().size.width / 2 + 1.6, CDFrame -> getPosition().y));
        this -> addChild(CD, 0, FollowerTag_CD);
    }
    
    ////////小弟图标的发光外框////////
    CCSprite * pLightFrame = CCSprite::spriteWithSpriteFrameName("SkillFrame_1.png");
    pLightFrame -> setIsVisible(false);
    this -> addChild(pLightFrame, 0, FollowerTag_LightFrame);
    
    
    ////////显示攻击力的数字标签////////
    char buff1[32];
    sprintf(buff1, "Number_FollowerAttack%d-hd.png", m_Host -> GetData() -> Follower_Profession);
    memcpy(m_AttackNumberName, buff1, CHARLENGHT);
    
    CCSprite * NumImage = CCSprite::spriteWithFile(buff1);
    m_NumSize_Attack = CCSizeMake(NumImage -> getTextureRect().size.width / 12, NumImage -> getTextureRect().size.height);

    m_LabelAttack = CCLabelAtlas::labelWithString("0", buff1, m_NumSize_Attack.width, m_NumSize_Attack.height, '.');
    m_LabelAttack -> retain();
    
    
    ////////攻击球的图片名称////////
    char buff2[32];
    sprintf(buff2, "AttackBall%d.png", m_Host -> GetData() -> Follower_Profession);
    memcpy(m_AttackBallName, buff2, CHARLENGHT);
    
    
    ////////计算技能的CD时间////////
    int SkillType = m_Host -> GetData() -> Follower_CommonlySkillType;
    int SkillID = m_Host -> GetData() -> Follower_CommonlySkillID;
    if(SkillType != 0 && SkillID != 0)
    {
        const Skill_Data * SD = SystemDataManage::ShareInstance() -> GetData_ForSkill(SkillType, SkillID);
        
        m_SkillCDRound = SD -> Skill_CDRound;
        if(m_SkillCDRound - (m_Host -> GetData() -> Follower_SkillLevel - 1) <= 0)
            m_SkillCDRound = 1;
        else
            m_SkillCDRound -= (m_Host -> GetData() -> Follower_SkillLevel - 1);
        
        m_SkillCDRecord += m_SkillCDRound;
        
        CCSprite * pSprite = dynamic_cast<CCSprite*>(this -> getChildByTag(FollowerTag_CD));
        pSprite -> setScaleX(1.0 - float(1.0 / (float)m_SkillCDRound) * m_SkillCDRecord);
        pSprite -> setScaleY(0.8f);
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 先计算对敌人的伤害
/************************************************************************/
void Battle_Follower::CalculateAttackEnemy()
{
    m_RealAttack = m_Host -> GetData() -> Follower_Attack + m_AddedAttack;

    if(m_IsAttackAllEnemy)
    {
        Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
        for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
        {
            it -> second -> CalculateBeAttackByFollower(m_RealAttack, this);
            
            ///////战斗验证信息///////
            Layer_Display::ShareInstance() -> m_BattleAllAttack += m_RealAttack;
            if(Layer_Display::ShareInstance() -> m_BattleMaxAttack < m_RealAttack)
                Layer_Display::ShareInstance() -> m_BattleMaxAttack = m_RealAttack;
            ////////////////////////
        }
    }
    else 
    {
        m_Target = Layer_Enemy::ShareInstance() -> GetTargetEnemy();
        m_TargetPosition = m_Target -> GetWorldPoint();
        
        m_Target -> CalculateBeAttackByFollower(m_RealAttack, this);
        
        ///////战斗验证信息///////
        Layer_Display::ShareInstance() -> m_BattleAllAttack += m_RealAttack;
        if(Layer_Display::ShareInstance() -> m_BattleMaxAttack < m_RealAttack)
            Layer_Display::ShareInstance() -> m_BattleMaxAttack = m_RealAttack;
        ////////////////////////
    }
    
    m_AddedAttack = 0;
}  
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 关于攻击力数字
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/
/************************************************************************/
#pragma mark 显示攻击力数字
/************************************************************************/
void Battle_Follower::DisplayNumber()
{
    m_IsUnderShowAttackNumber = true;
    
    CCPoint pPoint = this -> getPosition();
    m_LabelAttackString = m_Host -> GetData() -> Follower_Attack;
    
    char buff[32];
    sprintf(buff, "%d", m_LabelAttackString);
    m_LabelAttack -> setString(buff);
    m_LabelAttack -> setOpacity(0);
    m_LabelAttack -> setPosition(ccp(pPoint.x - (m_NumSize_Attack.width * strlen(buff)) / 2, pPoint.y - m_NumSize_Attack.height / 2));
    
    CCFadeIn * pFadeIn = CCFadeIn::actionWithDuration(0.2f);
    m_LabelAttack -> runAction(pFadeIn);
    
    Layer_Display::ShareInstance() -> addChild(m_LabelAttack);
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示攻击力数字跳动加成
/************************************************************************/
void Battle_Follower::DisplayNumberJump()
{
    this -> schedule(schedule_selector(Battle_Follower::DisplayNumberJumpFun1));
}

void Battle_Follower::DisplayNumberJumpFun1()
{
    int count = 5;
    int Dis = (m_RealAttack - m_Host -> GetData() -> Follower_Attack) / count;
    
    if(m_LabelAttackString + Dis < m_RealAttack)
    {
        m_LabelAttackString += Dis;
    }else
    {
        m_LabelAttackString = m_RealAttack;
    }
    
    char buff[32];
    sprintf(buff, "%d", m_LabelAttackString);
    m_LabelAttack -> setString(buff);
    m_LabelAttack -> setPosition(ccp(this -> getPosition().x - (m_NumSize_Attack.width * strlen(buff)) / 2, m_LabelAttack -> getPosition().y));
    
    if(m_LabelAttackString == m_RealAttack)
    {
        this -> unschedule(schedule_selector(Battle_Follower::DisplayNumberJumpFun1));
        m_IsUnderShowAttackNumber = false;
        m_LabelAttackString = 0;
        m_RealAttack = 0;
    }
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示攻击
/************************************************************************/
void Battle_Follower::DisplayAttack()
{
    CCPoint pPoint = this -> getPosition();
    
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.2f);
    CCCallFuncN * pRemoveLabelAttack = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Follower::RemoveLabelAttack));
    m_LabelAttack -> runAction(CCSequence::actions(pFadeOut, pRemoveLabelAttack, NULL));
    
    if(! m_IsAttackAllEnemy)
    {
        //将目标敌人置于攻击
        m_Target -> SetUnderAttack(true);
        
        CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName(m_AttackBallName);
        pSprite -> setPosition(pPoint);
        pSprite -> setOpacity(0);
        
        //淡入
        CCFadeIn * pFadeIn = CCFadeIn::actionWithDuration(0.2f);
        //放大缩小
        CCScaleTo * pScaleToBig = CCScaleTo::actionWithDuration(0.1f, 3.5f);
        CCScaleTo * pScaleToSmall = CCScaleTo::actionWithDuration(0.1f, 1.0f);
        CCFiniteTimeAction * pScaleToBigAndSmall = CCSequence::actions(pScaleToBig, pScaleToSmall, NULL);
        //运动曲线
        ccBezierConfig pBezierConfig;
        pBezierConfig.controlPoint_1 = this -> getPosition();
        pBezierConfig.controlPoint_2 = CCPointMake(rand() % 320, this -> getPosition().y);
        pBezierConfig.endPosition = m_TargetPosition;
        CCBezierTo * pBezierTo = CCBezierTo::actionWithDuration(0.4f, pBezierConfig);
        CCEaseIn * pEaseIn = CCEaseIn::actionWithAction(pBezierTo, 1.2f);
        //结束函数
        CCCallFuncN * pRemoveAttackBall = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Follower::RemoveAttackBall));
        pSprite -> runAction(CCSequence::actions(pFadeIn, pScaleToBigAndSmall, pEaseIn, pRemoveAttackBall, NULL)); 
        
        //加入攻击的球
        Layer_Display::ShareInstance() -> addChild(pSprite);
    }
    else
    {
        Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
        for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
        {
            //将目标敌人置于攻击
            it -> second -> SetUnderAttack(true);
            
            m_TempEnemyList1.push_back(it -> second);
           
            CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName(m_AttackBallName);
            pSprite -> setPosition(pPoint);
            pSprite -> setOpacity(0);
            
            //淡入
            CCFadeIn * pFadeIn = CCFadeIn::actionWithDuration(0.2f);
            //放大缩小
            CCScaleTo * pScaleToBig = CCScaleTo::actionWithDuration(0.1f, 3.5f);
            CCScaleTo * pScaleToSmall = CCScaleTo::actionWithDuration(0.1f, 1.0f);
            CCFiniteTimeAction * pScaleToBigAndSmall = CCSequence::actions(pScaleToBig, pScaleToSmall, NULL);
            //运动曲线
            ccBezierConfig pBezierConfig;
            pBezierConfig.controlPoint_1 = this -> getPosition();
            pBezierConfig.controlPoint_2 = CCPointMake(rand() % 320, this -> getPosition().y);
            pBezierConfig.endPosition = it -> second -> getPosition();
            CCBezierTo * pBezierTo = CCBezierTo::actionWithDuration(0.4f, pBezierConfig);
            CCEaseIn * pEaseIn = CCEaseIn::actionWithAction(pBezierTo, 1.2f);
            //结束函数
            CCCallFuncN * pRemoveAllAttackBall = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Follower::RemoveAllAttackBall));
            pSprite -> runAction(CCSequence::actions(pFadeIn, pScaleToBigAndSmall, pEaseIn, pRemoveAllAttackBall, NULL));
            
            //加入攻击的球
            Layer_Display::ShareInstance() -> addChild(pSprite);
        }
        
        CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(0.8f);
        CCCallFunc * pDisplayAttackAllEnemy = CCCallFunc::actionWithTarget(this, callfunc_selector(Battle_Follower::DisplayAttackAllEnemy));
        this -> runAction(CCSequence::actions(pDelayTime, pDisplayAttackAllEnemy, NULL));
    }
    m_IsAttackAllEnemy = false;
}

void Battle_Follower::RemoveLabelAttack(CCNode * sender)
{
    m_LabelAttack -> removeFromParentAndCleanup(true);
}

void Battle_Follower::RemoveAttackBall(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
    m_Target -> DisplayBeAttack(true, 1, this);
    
    m_Target = NULL;
    m_TargetPosition.x = m_TargetPosition.y = 0;
}

void Battle_Follower::RemoveAllAttackBall(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
    
    list <Battle_Enemy*>::iterator it = m_TempEnemyList1.begin();
    (*it) -> DisplayBeAttack(false, 0, this);
    
    m_TempEnemyList1.erase(it);
    if(m_TempEnemyList1.empty())
    {
        m_Target = NULL;
        m_TargetPosition.x = m_TargetPosition.y = 0;
        m_TempEnemyList1.clear();
    }
}

void Battle_Follower::DisplayAttackAllEnemy()
{
    Layer_Display::ShareInstance() -> DisplayFollowerAttackAll(m_Host -> GetData() -> Follower_Profession);
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 减少CD回合
/************************************************************************/
void Battle_Follower::DecreaseSkillCDTime()
{
    if(m_SkillCDRecord == 0)   return;
    else if(m_SkillCDRecord - 1 <= 0)
    {
        m_SkillCDRecord = 0;
        
        CCSprite * pSprite = dynamic_cast<CCSprite*>(this -> getChildByTag(FollowerTag_LightFrame));
        pSprite -> setIsVisible(true);
        
        CCAnimation * pAnimation = CreateAnimation("SkillFrame_", 0.08f);
        CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
        CCRepeatForever * pAction = CCRepeatForever::actionWithAction(pAnimate);
        pSprite -> runAction(pAction);
    }
    else 
        m_SkillCDRecord -= 1;
    
    //CD条变化
    if(m_SkillCDRecord != 0)
    {
        CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.3f, 1.0 - float(1.0 / (float)m_SkillCDRound) * m_SkillCDRecord, 0.8);
        dynamic_cast<CCSprite*>(this -> getChildByTag(FollowerTag_CD)) -> runAction(pScaleTo);
    }
    else
    {
        CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.3f, 1.0 - float(1.0 / (float)m_SkillCDRound) * m_SkillCDRecord, 0.8);
        CCCallFunc * pCallFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Battle_Follower::HideSkillCD));
        dynamic_cast<CCSprite*>(this -> getChildByTag(FollowerTag_CD)) -> runAction(CCSequence::actions(pScaleTo, pCallFunc, NULL));
    }
}

void Battle_Follower::BeenSkillCDTime()
{
    CCSprite * pSprite = dynamic_cast<CCSprite*>(this -> getChildByTag(FollowerTag_LightFrame));
    pSprite -> stopAllActions();
    pSprite -> setIsVisible(false);
}

void Battle_Follower::HideSkillCD()
{
    dynamic_cast<CCSprite*>(this -> getChildByTag(FollowerTag_CD)) -> setIsVisible(false);
    dynamic_cast<CCSprite*>(this -> getChildByTag(FollowerTag_CDFrame)) -> setIsVisible(false);
}

void Battle_Follower::GuideReSetSkillCD()
{
    int SkillType = m_Host -> GetData() -> Follower_CommonlySkillType;
    int SkillID = m_Host -> GetData() -> Follower_CommonlySkillID;
    if(SkillType != 0 && SkillID != 0)
    {
        m_SkillCDRecord = 1;
        DecreaseSkillCDTime();
    }
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/


#pragma mark 关于技能
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/
#pragma mark 触发队长技能
/************************************************************************/
void Battle_Follower::TriggerTeamSkill()
{
    int SkillType = m_Host -> GetData() -> Follower_TeamSkillType;
    int SkillID = m_Host -> GetData() -> Follower_TeamSkillID;
    if(SkillType == 0 || SkillID == 0)  return;
    
    const Skill_Data * SD = SystemDataManage::ShareInstance() -> GetData_ForSkill(SkillType, SkillID);

    if(SkillType == 5)
    {
        //减少敌人每人百分之N的攻击力
        if(SkillID >= 12 && SkillID <= 15)
        {
            Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateDecreaseAttack(it -> second -> GetData() -> Original_Attack * SD -> Skill_Value);
            }
            printf("小弟的队长技能(5-(12-15))被触发，减少敌人每人百分之%.f的攻击力", SD -> Skill_Value);
        }
    }
    else if(SkillType == 7)
    {
        //小弟队伍防御力增加N点
        if(SkillID == 1)
        {
            Layer_Follower::ShareInstance() -> AddDefense(SD -> Skill_Value);
            printf("小弟的队长技能(7-1)被触发，小弟队伍防御力增加%.f点\n", SD -> Skill_Value);
        }
        //小弟队伍防御力增加百分之N
        else if(SkillID == 2)
        {
            Layer_Follower::ShareInstance() -> AddDefense(SD -> Skill_Value * Layer_Follower::ShareInstance() -> GetOriginalDefense());
            printf("小弟的队长技能(7-2)被触发，小弟队伍防御力增加百分之%.f\n", SD -> Skill_Value);
        }
    }
    else if(SkillType == 8)
    {
        Skill * pSkill = Skill::skillWithSpriteFrameName(SD -> Skill_IconName);
        pSkill -> SetData(SD);
        pSkill -> SetUser(this);
        
        //每回合恢复回复力百分之N的生命值
        if(SkillID >= 1 && SkillID <= 3)
        {
            Layer_Follower::ShareInstance() -> AddBuff(pSkill);
            printf("小弟的队长技能(8-%d)被触发，小弟队伍每回合恢复回复力百分之%.f的生命值\n", SkillID, SD -> Skill_Value);
        }
        //每回合恢复N点生命值
        else if(SkillID >= 4 && SkillID <= 6)
        {
            Layer_Follower::ShareInstance() -> AddBuff(pSkill);
            printf("小弟的队长技能(8-%d)被触发，小弟队伍每回合恢复%.f点的生命值\n", SkillID, SD -> Skill_Value);
        }
    }
    else if(SkillType == 9)
    {
        //增加队伍N点生命值上限
        if(SkillID == 1 || (SkillID >= 3 && SkillID <= 5))
        {
            Layer_Follower::ShareInstance() -> AddMaxHP(SD -> Skill_Value);
            printf("小弟的队长技能(9-1)被触发，小弟队伍增加%.f点生命值上限\n", SD -> Skill_Value);
        }
        //增加队伍百分之N的生命值上限
        else if(SkillID == 2 || (SkillID >= 6 && SkillID <= 8))
        {
            Layer_Follower::ShareInstance() -> AddMaxHP(Layer_Follower::ShareInstance() -> GetOriginalHP() * SD -> Skill_Value);
            printf("小弟的队长技能(9-2)被触发，小弟队伍增加生命值百分之%.f的生命值上限\n", SD -> Skill_Value);
        }
    }
    else if(SkillType == 10)
    {
        //百分之N的生命值时，被秒杀留一点生命值
        if(SkillID >= 1 && SkillID <= 2)
        {
            Layer_Follower::ShareInstance() -> SetAvoidDeadlyAttackPercent(SD -> Skill_Value);
            printf("小弟的队长技能(10-%d)被触发，小弟队伍剩下百分之%.f的生命值时，被秒杀留一点生命值\n", SkillID, SD -> Skill_Value);
        }
    }
    else if(SkillType == 11)
    {
        //对克制的伤害提升百分之N
        if( (SkillID >= 1 && SkillID <= 2) ||
            (SkillID >= 4 && SkillID <= 21) )
        {
            Layer_Follower::ShareInstance() -> SetOverComeAttackPercent(SD -> Skill_Value);
            Layer_Follower::ShareInstance() -> SetTeamOverComePro(m_Host -> GetData() -> Follower_Profession);
            Layer_Follower::ShareInstance() -> SetTeamBeOverComePro(m_Host -> GetData() -> BeOverCome_Profession);
            printf("小弟的队长技能(11-%d)被触发，小弟队伍对克制的伤害提升攻击力的百分之%.f\n", SkillID, SD -> Skill_Value);
        }
        //提升队伍伤害百分之N，受到伤害提升百分之N
        else if(SkillID == 3)
        {
            Layer_Follower::ShareInstance() -> SetAddAttackPercent(SD -> Skill_Value);
            printf("小弟的队长技能(11-3)被触发，小弟队伍整体提升伤害百分之%.f，受到伤害也提升百分之%.f\n", SD -> Skill_Value, SD -> Skill_Value);
        }
    }
    else if(SkillType == 12)
    {
        //面对刀系敌人的伤害减免百分之N
        if(SkillID == 1 || SkillID == 4)
        {
            Layer_Follower::ShareInstance() -> SetProDerate(SWORD);
            Layer_Follower::ShareInstance() -> SetProDerateAttackPercent(SD -> Skill_Value);
            printf("小弟的队长技能(12-%d)被触发，小弟队伍面对来自刀系敌人的伤害减免百分之%.f\n", SkillID, SD -> Skill_Value);
        }
        //面对拳系敌人的伤害减免百分之N
        else if(SkillID == 2 || SkillID == 5)
        {
            Layer_Follower::ShareInstance() -> SetProDerate(FIST);
            Layer_Follower::ShareInstance() -> SetProDerateAttackPercent(SD -> Skill_Value);
            printf("小弟的队长技能(12-%d)被触发，小弟队伍面对来自拳系敌人的伤害减免百分之%.f\n", SkillID, SD -> Skill_Value);
        }
        //面对枪系敌人的伤害减免百分之N
        else if(SkillID == 3 || SkillID == 6)
        {
            Layer_Follower::ShareInstance() -> SetProDerate(GUN);
            Layer_Follower::ShareInstance() -> SetProDerateAttackPercent(SD -> Skill_Value);
            printf("小弟的队长技能(12-%d)被触发，小弟队伍面对来自枪系敌人的伤害减免百分之%.f\n", SkillID, SD -> Skill_Value);
        }
    }
    else if(SkillType == 13)
    {
        //减免N点队伍受到的伤害
        if(SkillID == 1)
        {
            Layer_Follower::ShareInstance() -> SetDerateAttack(SD -> Skill_Value);
            printf("小弟的队长技能(13-1)被触发，小弟队伍直接减免伤害%.f点\n", SD -> Skill_Value);
        }
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 触发技能动画
/************************************************************************/
void Battle_Follower::TriggerSkillAnimation()
{ 
    int SkillType = m_Host -> GetData() -> Follower_CommonlySkillType;
    int SkillID = m_Host -> GetData() -> Follower_CommonlySkillID;
  
    if(SkillType == 1)
    {
        //技能类型1（1 － 12） （19 － 24）
        if((SkillID >= 1 && SkillID <= 12) || (SkillID >= 19 && SkillID <= 24) ||
           (SkillID >= 25 && SkillID <= 26) || (SkillID >= 28 && SkillID <= 42))
        {
            TriggerSkill();
        }
        //技能类型1（13 － 18 || 27）
        else if((SkillID >= 13 && SkillID <= 18) || SkillID == 27)
        {
            Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> DisplayBeAttackBySkill(this);
            }
            this -> schedule(schedule_selector(Battle_Follower::CheckEnemySkillAnimationOver));
        }
    }
    else if(SkillType == 2)
    {
        Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
        for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
        {
            it -> second -> DisplayBeAttackBySkill(this);
        }
        this -> schedule(schedule_selector(Battle_Follower::CheckEnemySkillAnimationOver));
    }
    else if(SkillType == 3 || SkillType == 5)
    {
        Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
        for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
        {
            it -> second -> DisplayBeAttackBySkill(this);
        }
        this -> schedule(schedule_selector(Battle_Follower::CheckEnemySkillAnimationOver));
    }
    else if(SkillType == 4)
    { 
        Layer_Enemy::ShareInstance() -> BGLight();
        Layer_Display::ShareInstance() -> OperationEnable(true);
        
        TriggerSkill();
    }
    else if(SkillType == 6)
    {
        Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
        for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
        {
            it -> second -> DisplayBeAttackBySkill(this);
        }
        this -> schedule(schedule_selector(Battle_Follower::CheckEnemySkillAnimationOver));
    }
}

void Battle_Follower::CheckEnemySkillAnimationOver()
{
    Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
    for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
    {
        if(! it -> second -> GetUnderShowSkillAnimation()) return;
    }
    this -> unschedule(schedule_selector(Battle_Follower::CheckEnemySkillAnimationOver));
    
    Layer_Enemy::ShareInstance() -> BGLight();
    Layer_Display::ShareInstance() -> OperationEnable(true);
    
    TriggerSkill();
}

void Battle_Follower::CheckFollowerSkillAnimationOver()
{
    /*Battle_FollowerList pList = *Layer_Follower::ShareInstance() -> GetFollowerList();
    for(Battle_FollowerList::iterator it = pList.begin(); it != pList.end(); ++ it)
    {
        if(! it -> second -> GetDisplaySkillAnimationOver()) return;
    }
    this -> unschedule(schedule_selector(Battle_Follower::CheckFollowerSkillAnimationOver));
    
    Layer_Enemy::ShareInstance() -> BGLight();
    Layer_Display::ShareInstance() -> OperationEnable(true);
    
    TriggerSkill();*/
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 触发技能
/************************************************************************/
void Battle_Follower::TriggerSkill()
{
    int SkillType = m_Host -> GetData() -> Follower_CommonlySkillType;
    int SkillID = m_Host -> GetData() -> Follower_CommonlySkillID;
    const Skill_Data * SD = SystemDataManage::ShareInstance() -> GetData_ForSkill(SkillType, SkillID);

    Battle_Enemy * pTarget = Layer_Enemy::ShareInstance() -> GetTargetEnemy();
    Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
    
    if(SkillType == 1)
    {
        //对单一目标造成（N点）的额外伤害
        if((SkillID >= 1 && SkillID <= 3) || (SkillID >= 25 && SkillID <= 26))
        {
            pTarget -> CalculateBeAttackByFollower(m_Host -> GetData() -> Follower_Attack + SD -> Skill_Value, this);
            SkillAttackOneEnemy(pTarget);
        }
        //对群体目标造成（N点）的额外伤害   
        else if((SkillID >= 4 && SkillID <= 6) || (SkillID >= 40 && SkillID <= 42))
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateBeAttackByFollower(m_Host -> GetData() -> Follower_Attack + SD -> Skill_Value, this);
            }
            SkillAttackAllEnemy();
        }
        //对单一目标造成攻击力（N倍）的伤害 
        else if(SkillID >= 7 && SkillID <= 9)
        {
            pTarget -> CalculateBeAttackByFollower(m_Host -> GetData() -> Follower_Attack * SD -> Skill_Value, this);
            SkillAttackOneEnemy(pTarget);
        }
        //对群体目标造成攻击力（N倍）的伤害 
        else if((SkillID >= 10 && SkillID <= 12) || (SkillID >= 28 && SkillID <= 39))
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateBeAttackByFollower(m_Host -> GetData() -> Follower_Attack * SD -> Skill_Value, this);
            }
            SkillAttackAllEnemy();
        }
        //对群体目标每回合造成（N点）的伤害
        //对群体目标每个回合造成攻击力百分之（N）的伤害 
        else if((SkillID >= 13 && SkillID <= 18) || SkillID == 27)
        {
            Skill * pSkill = Skill::skillWithSpriteFrameName(SD -> Skill_IconName);
            pSkill -> SetData(SD);
            pSkill -> SetUser(this);
            
            Layer_Enemy::ShareInstance() -> AddDeBuff(pSkill);
        }
        //对单一目标造成目标生命值百分之（N）的伤害 
        else if(SkillID >= 19 && SkillID <= 21)
        {
            pTarget -> CalculateBeAttackByFollower(pTarget -> GetData() -> Enemy_HP * SD -> Skill_Value, this);
            SkillAttackOneEnemy(pTarget);
        }
        //对群体目标造成目标生命值百分之（N）的伤害    
        else if(SkillID >= 22 && SkillID <= 24)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> CalculateBeAttackByFollower(it -> second -> GetData() -> Enemy_HP * SD -> Skill_Value, this);
            }
            SkillAttackAllEnemy();
        }
    }
    else if(SkillType == 2)
    {
        Skill * pSkill = Skill::skillWithSpriteFrameName(SD -> Skill_IconName);
        pSkill -> SetData(SD);
        pSkill -> SetUser(this);
        
        //对群体目标减少（N点）的防御值
        //对群体目标减少防御值百分之（N）的防御值      
        if(SkillID >= 1 && SkillID <= 7)
        {
            Layer_Enemy::ShareInstance() -> AddDeBuff(pSkill);
        }
    }
    else if(SkillType == 3)
    {
        Skill * pSkill = Skill::skillWithSpriteFrameName(SD -> Skill_IconName);
        pSkill -> SetData(SD);
        pSkill -> SetUser(this);
        
        //减少敌人N点攻击力
        //减少敌人百分之N的攻击力
        if(SkillID >= 1 && SkillID <= 6)
        {
            Layer_Enemy::ShareInstance() -> AddDeBuff(pSkill);
        }
    }
    else if(SkillType == 4)
    {
        //恢复（N点）的生命值 
        if(SkillID >= 1 && SkillID <= 3)
        {
            Layer_Follower::ShareInstance() -> DisplayAddHpBySkill(SD -> Skill_Value);
        }
        //恢复回复力（N）倍的生命值
        else if((SkillID >= 4 && SkillID <= 6) || SkillID == 13)
        {
            Layer_Follower::ShareInstance() -> DisplayAddHpBySkill(Layer_Follower::ShareInstance() -> GetFollowerAddedHp() * SD -> Skill_Value);
        }
        //每回合恢复回复力百分之（N）的生命值
        else if((SkillID >= 7 && SkillID <= 9) || (SkillID >= 14 && SkillID <= 15))
        {
            Skill * pSkill = Skill::skillWithSpriteFrameName(SD -> Skill_IconName);
            pSkill -> SetData(SD);
            pSkill -> SetUser(this);
        
            Layer_Follower::ShareInstance() -> AddBuff(pSkill);
        }
        //每回合恢复（N点）的生命值  
        else if((SkillID >= 10 && SkillID <= 12) || (SkillID >= 16 && SkillID <= 17))
        {
            Skill * pSkill = Skill::skillWithSpriteFrameName(SD -> Skill_IconName);
            pSkill -> SetData(SD);
            pSkill -> SetUser(this);
            
            Layer_Follower::ShareInstance() -> AddBuff(pSkill);
        }
    }
    else if(SkillType == 5)
    {
        Skill * pSkill = Skill::skillWithSpriteFrameName(SD -> Skill_IconName);
        pSkill -> SetData(SD);
        pSkill -> SetUser(this);
        
        //减少敌人每人（N点）攻击力
        //减少敌人每人百分之（N）攻击力
        if((SkillID >= 1 && SkillID <= 7) || (SkillID >= 10 && SkillID <= 11))
        {
            Layer_Enemy::ShareInstance() -> AddDeBuff(pSkill);
        }
    }
    else if(SkillType == 6)
    {
        //增加敌人出手N回合数
        if(SkillID >= 1 && SkillID <= 4)
        {
            for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
            {
                it -> second -> FollowerSkillAddCDRecord(SD -> Skill_Value);
            }
        }
    }
    
    //计算技能的CD时间
    m_SkillCDRecord += m_SkillCDRound;
    
    dynamic_cast<CCSprite*>(this -> getChildByTag(FollowerTag_CD)) -> setIsVisible(true);
    dynamic_cast<CCSprite*>(this -> getChildByTag(FollowerTag_CDFrame)) -> setIsVisible(true);
    
    //CD条变化
    CCSprite * pSprite = dynamic_cast<CCSprite*>(this -> getChildByTag(FollowerTag_CD));
    pSprite -> setScaleX(1.0 - float(1.0 / (float)m_SkillCDRound) * m_SkillCDRecord);
    pSprite -> setScaleY(0.8f);
    
    BeenSkillCDTime();
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示受到技能影响的动画
/************************************************************************/
void Battle_Follower::DisplaySkillAnimation(Battle_Follower * sender)
{
    /*m_IsDisplaySkillAnimationOver = false;
    
    int SkillType = sender -> GetHost() -> GetData() -> Follower_CommonlySkillType;
    int SkillID = sender -> GetHost() -> GetData() -> Follower_CommonlySkillID;
    
    if(SkillType == 3)
    {
        //技能类型3（1 － 3）
        if(SkillID >= 1 && SkillID <= 3)
        {
            CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("Animation30_1.png");
            
            CCAnimation * pAnimation = CreateAnimation("Animation30_", 0.08f);
            CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
            CCCallFuncN * pAniamtionOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Follower::RemoveSkillAnimation));
            pSprite -> runAction(CCSequence::actions(pAnimate, pAniamtionOver, NULL));
            
            this -> addChild(pSprite);
        }
        //技能类型3（4 － 6）
        else if(SkillID >= 4 && SkillID <= 6)
        {
            CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("Animation31_1.png");
            
            CCAnimation * pAnimation = CreateAnimation("Animation31_", 0.08f);
            CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
            CCCallFuncN * pAniamtionOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Follower::RemoveSkillAnimation));
            pSprite -> runAction(CCSequence::actions(pAnimate, pAniamtionOver, NULL));
            
            this -> addChild(pSprite);
        }
    }*/
}

void Battle_Follower::RemoveSkillAnimation(CCNode * sender)
{
    /*sender -> removeFromParentAndCleanup(true);
    m_IsDisplaySkillAnimationOver = true;*/
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示技能攻击 (打击一个敌人)
/************************************************************************/
void Battle_Follower::SkillAttackOneEnemy(Battle_Enemy * BE)
{
    m_Target = BE;
    m_TargetPosition = BE -> GetWorldPoint();
    
    //将目标敌人置于攻击
    m_Target -> SetUnderAttack(true);
    m_Target -> DisplayBeAttack(true, 2, this);
    
    Layer_Enemy::ShareInstance() -> CheckEnemyUnderAttackBySkill();
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示技能攻击 (打击所有的敌人)
/************************************************************************/
void Battle_Follower::SkillAttackAllEnemy()
{
    Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
    for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
    {
        m_TempEnemyList2.push_back(it -> second);
    }
    
    this -> schedule(schedule_selector(Battle_Follower::SkillAttackAllEnemyFunc1), 0.2f);
}

void Battle_Follower::SkillAttackAllEnemyFunc1()
{
    int index = rand() % m_TempEnemyList2.size();
    
    //将目标敌人置于攻击
    m_TempEnemyList2[index] -> SetUnderAttack(true);
    m_TempEnemyList2[index] -> DisplayBeAttack(true, 2, this);
    m_TempEnemyList2.erase(m_TempEnemyList2.begin() + index);
    
    if(m_TempEnemyList2.size() == 0)
    {
        m_TempEnemyList2.clear();
        this -> unschedule(schedule_selector(Battle_Follower::SkillAttackAllEnemyFunc1));

        Layer_Enemy::ShareInstance() -> CheckEnemyUnderAttackBySkill();
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 检测被点击
/************************************************************************/
bool Battle_Follower::CheckBeTouch(CCPoint point)
{
    m_RealSize.origin.x = this -> getPosition().x - m_RealSize.size.width / 2;
    m_RealSize.origin.y = this -> getPosition().y + m_RealSize.size.height / 2;
    
    if(point.x >= m_RealSize.origin.x && point.x <= m_RealSize.origin.x + m_RealSize.size.width &&
       point.y <= m_RealSize.origin.y && point.y >= m_RealSize.origin.y - m_RealSize.size.height)
    {
        //如果技能的CD时间为0，就是可以释放技能
        if(m_Host -> GetData() -> Follower_CommonlySkillType != 0 &&
           m_Host -> GetData() -> Follower_CommonlySkillID != 0)
        {
            Layer_SkillInfoBoard * LS = Layer_SkillInfoBoard::InitBoard(this);
            Layer_Display::ShareInstance() -> addChild(LS);
        }
        return true;
    }
    return false;
}
/************************************************************************/
#pragma mark -
/************************************************************************/
































