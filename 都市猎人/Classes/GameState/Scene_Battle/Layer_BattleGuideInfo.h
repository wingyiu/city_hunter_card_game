//
//  Layer_BattleGuideInfo.h
//  都市猎人
//
//  Created by 张 强 on 12-10-9.
//
//

#ifndef _____Layer_BattleGuideInfo_h
#define _____Layer_BattleGuideInfo_h

#include "cocos2d.h"



class Layer_BattleGuideInfo : public cocos2d::CCLayer
{
public:
    static Layer_BattleGuideInfo * InitBoard(int step);
    ~Layer_BattleGuideInfo();
    
protected:
    bool init(int step);
    void onExit();
    
    virtual bool ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    virtual void ccTouchMoved(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    virtual void ccTouchEnded(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    
protected:
    void  Leave();
    void  LeaveFunc1(cocos2d::CCNode * sender);
    
protected:
    cocos2d::CCSize             m_WinSize;
    int                         m_InfoStep;

};

#endif
