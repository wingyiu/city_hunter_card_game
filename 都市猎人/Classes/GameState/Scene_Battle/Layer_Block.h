//
//  Layer_Block.h
//  MidNightCity
//
//  Created by 强 张 on 12-6-1.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_Block_h
#define MidNightCity_Layer_Block_h

#include "cocos2d.h"
#include "Sprite_Block.h"
#include "Sprite_Cube.h"
#include "Sprite_Frame.h"
#include "Action_BlockChange.h"
#include "MyConfigure.h"
#include "Layer_GameGuide.h"
#include "PlayerDataManage.h"

USING_NS_CC;

// 拖动方块枚举
enum Direction
{
    Direction_None = 0,
    Direction_Horizontal,
    Direction_Vetical
};

// 横向方块间隔距离
const float HORIZON_BLOCK_DELTA = 53.0f;
// 竖向方块间隔距离
const float VETICAL_BLOCK_DELTA = 53.0f;

// 横向方块个数
const int HORIZON_BLOCK_NUM = 6;
// 竖向方块个数
const int VETICAL_BLOCK_NUM = 5;
// 能消除方块个数
const int CLEAR_BLOCK_NUM = 3;
// 终极方块消除个数
const int FINAL_CLEAR_BLOCK_NUM = 5;
// 终极方块淡入actionTag
const int FINAL_BLOCK_FADEOUT_ACTION_TAG = 1919;

// 方块起始点
const float HORIZON_START_POS = -HORIZON_BLOCK_DELTA * HORIZON_BLOCK_NUM / 2.0f + 26.5f + 160.0f;
const float VETICAL_START_POS = HORIZON_BLOCK_DELTA * VETICAL_BLOCK_NUM / 2 + 240.0f - 134.0f;

// 方块信息结构体
struct BLOCK_INFO
{
    Sprite_Block* pBlock;                       // 方块对象
    Sprite_Cube* pCube;                         // 方块底立方体对象
    
    Sprite_Frame* pFrame;                       // 方块外框
};

// 光阴移动状态枚举
enum LIGHT_STATE
{
    LIGHT_LIFTMOVE = 0,
    LIGHT_RIGHTMOVE,
    LIGHT_WAIT
};

// 方块图层类定义
class Layer_Block : public CCLayer
{
public:
    #pragma mark - 获得block实例对象
    static Layer_Block * ShareInstance();
    
    #pragma mark - 构造函数
    Layer_Block();
    #pragma mark - 析构函数
    virtual ~Layer_Block();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 退出函数
    virtual void onExit();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 设置可视范围
    void SetVisibleRange(float fPosX, float fPosY, float fWidth, float fHeight);
    
    #pragma mark - 设置操作标志函数
    void SetOperationEnable(bool bIsEnable)                                         {m_bIsOperationEnable = bIsEnable;}
    #pragma mark - 获得操作标志函数
    bool GetOperationEnable() const                                                 {return m_bIsOperationEnable;}
    
    #pragma mark - 设置layeralpha值函数
    void SetLayerAlpha(bool bIsOpenMask);
    
    #pragma mark - 结束方块函数
    void EndBlock();
    #pragma mark - 重开方块函数
    void RestartBlock();
    
    #pragma mark - 获得最大combo数量
    int GetMaxComboNum()                                                            {return m_iMaxComboNum;}
    
    #pragma mark - 使用cocos2d初始化函数
    LAYER_NODE_FUNC(Layer_Block);
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    #pragma mark - 事件函数 : 滑动
    virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
    #pragma mark - 事件函数 : 弹出
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
private:
    #pragma mark - opengl访问函数
    virtual void visit();
    
    #pragma mark - 在初始化时消除能clear的方块函数
    bool ClearBlockAtBeginning();
    
    #pragma mark - 方块消除函数
    void blockClear();
    
    #pragma mark - 方块消除事件函数
    void ProcessBlockClearEvent();
    
    #pragma mark - 加入消除列表函数
    bool AddTempListToClearList();
    
    #pragma mark - 拖动后对齐方块函数
    void AlignBlockAfterDrag();
    
    #pragma mark - 回调函数 : 刷新对齐后方块位置函数
    void RefreshBlockPositionAfterAlign();
    
    #pragma mark - 重置状态函数
    void resetState();
    
    #pragma mark - 计算战斗数据函数
    void caleBattleData();
    
    #pragma mark - 回调函数 : 播放动画函数
    void playAnim(CCObject* pObject);
    #pragma mark - 回调函数 : 移除动画函数
    void removeAnim(CCObject* pObject);
    
    #pragma mark - 是否是大消除函数
    bool IsBlockBigClear(BLOCK_TYPE eType);
    
    #pragma mark - 回调函数 : 逻辑更新函数
    void update(ccTime dt);
private:
    static Layer_Block * m_Instance;
    
    // 方块对象变量
    CCSpriteBatchNode* m_pBatchNode;                                // 精灵节点
    
    CCSprite* m_pLight;                                             // 光阴精灵
    CCSprite* m_pLine1;                                             // 线条精灵
    CCSprite* m_pLine2;                                             // 线条精灵
    CCSprite* m_pLine3;                                             // 线条精灵
    CCSprite* m_pLine4;                                             // 线条精灵
    
    CCLayerColor* m_pMaskLayer;                                     // 遮罩层
    
    Layer_GameGuide* m_pGuide;                                      // 新手引导
    
    // 方框属性变量
    int m_iSelectedRow;                                             // 被选中的行数
    int m_iSelectedCol;                                             // 被选中的列表
    int m_iMoveBlockNum;                                            // 拖动的跨数
    int m_iComboNum;                                                // combo数量
    int m_iMaxComboNum;                                             // 最大combo数量
    
    float m_fDragDistance;                                          // 拖动距离
    float m_fDistanceLimit;                                         // 新手引导 : 拖动距离限制
    
    CGFloat m_fCurrentSystemScale;                                  // 系统缩放系数
    
    bool m_bIsBeginningClear;                                       // 开始
    bool m_bIsClearEvent;                                           // 是否在消除状态中，在消除状态中，不能做任何操作
    bool m_bIsLineDraw;                                             // 提示横线绘制标志
    bool m_bIsBlockClear;                                           // 本次计算中是否有方块消除
    bool m_bIsOperationEnable;                                      // 是否允许操作
    bool m_bIsGuideBlockClear;
    
    CCRect m_visibleRange;                                          // 可视范围
    
    Direction m_eMoveDirection;                                     // 拖动状态
    LIGHT_STATE m_eLightState;                                      // 光影状态
    
    vector<BLOCK_INFO> m_vecMoveList;                               // 移动列表
    vector<Sprite_Block*> m_vecTempList;                            // 临时列表 : 在消除方块使用，来判断是否满几个，然后放入clearBlock列表中
    vector<Sprite_Block*> m_vecCreateList;
    
    set<Sprite_Block*> m_setClearBlock;                             // 消除列表
};

#endif