//
//  Scene_Battle.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-4-23.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Scene_Battle_h
#define MidNightCity_Scene_Battle_h

#include "KNUIManager.h"

#include "Layer_Enemy.h"
#include "Layer_Follower.h"
#include "Layer_Block.h"
#include "Layer_Display.h"

#define  SceneBattleChild_Layer_Enemy 0
#define  SceneBattleChild_Layer_Follower 1
#define  SceneBattleChild_Layer_Display 2
#define  SceneBattleChild_Layer_Block 3

// Scene场景ui层id
const int SCENE_UI_ID = 1003;

class Scene_Battle : public CCLayer
{
public:
    #pragma mark - 创建场景函数
    static CCScene* scene();
    ~Scene_Battle();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 退出函数
    virtual void onExit();

    #pragma mark - 连接基类初始化
    LAYER_NODE_FUNC(Scene_Battle); 
    
    static int m_MissionFriendFollowerID;
    static int m_MissionFriendValue;
    
    //virtual bool ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
};

#endif
