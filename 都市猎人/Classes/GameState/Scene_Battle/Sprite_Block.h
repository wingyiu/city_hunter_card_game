//
//  Sprite_Block.h
//  MidNightCity
//
//  Created by 强 张 on 12-6-1.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Sprite_Block_h
#define MidNightCity_Sprite_Block_h

#include "cocos2d.h"

USING_NS_CC;

// 方块类型枚举
enum BLOCK_TYPE
{
    BLOCK_TYPE1_SWORD = 1,
    BLOCK_TYPE2_FIST,
    BLOCK_TYPE3_GUN,
    BLOCK_TYPE4_HP,
    BLOCK_NONE = BLOCK_TYPE4_HP,                                     // 什么都不是- -|||
};

class Sprite_Block : public CCSprite
{
public:
    #pragma mark - 静态函数 : 从文件创建方块精灵函数
    static Sprite_Block* blockSpriteWithFile(const char* filename);
    #pragma mark - 静态函数 : 从缓存创建方块精灵函数
    static Sprite_Block* blockSpriteWithSpriteFrameName(const char* filename);
    
    #pragma mark - 设置方块类型函数
    void SetBlockType(BLOCK_TYPE eType)                 {m_eBlockType = eType;}
    #pragma mark - 获得方块类型函数
    BLOCK_TYPE GetBlockType() const                     {return m_eBlockType;}
    
    #pragma mark - 设置所在行数和列数
    void SetRowAndCol(int iRow, int iCol)               {m_iRow = iRow; m_iCol = iCol;}
    #pragma mark - 获得所在行数和列数
    void GetRowAndCol(int& iRow, int& iCol)             {iRow = m_iRow; iCol = m_iCol;}
    
    #pragma mark - 设置大爆炸标志函数
    void SetBigClearMark(bool bIsBigClear)              {m_bIsBigClear = bIsBigClear;}
    #pragma mark - 获得大爆炸标志函数
    bool GetBigClearMark() const                        {return m_bIsBigClear;}
    
    #pragma mark - 设置横向和竖向绘制标志
    void SetHorizonAndVeticalDraw(bool bIsHorizon, bool bIsVetical) {m_bIsHorizonDraw = bIsHorizon; m_bIsVeticalDraw = bIsVetical;}
private:
    #pragma mark - 构造函数
    Sprite_Block();
    #pragma mark - 析构函数
    virtual ~Sprite_Block();
    
    #pragma mark - 重写函数 : 访问函数
    virtual void visit();
private:
    // 方块属性变量
    BLOCK_TYPE m_eBlockType;                        // 方块类型
    
    int m_iRow;                                     // 所在的行数
    int m_iCol;                                     // 所在的列数
    
    bool m_bIsHorizonDraw;                          // 横向绘制
    bool m_bIsVeticalDraw;                          // 竖向绘制
    
    bool m_bIsBigClear;                             // 大爆炸
};

#endif
