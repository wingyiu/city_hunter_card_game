//
//  Layer_BattleResult.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-6-28.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_BattleResult.h"
#include "GameController.h"
#include "Layer_Enemy.h"
#include "Layer_BattleMessage.h"
#include "KNUIFunction.h"
#include "Scene_Battle.h"

/************************************************************************/
#pragma mark - Layer_BattleResult类构造函数
/************************************************************************/
Layer_BattleResult::Layer_BattleResult()
{
    // 为战斗结算对象变量赋初值
    m_pMaskLayer = NULL;  
    m_pTopMask = NULL;
    m_pBottomMask = NULL;
    m_pWordSprite = NULL;
    m_pMoneyBar = NULL;
    m_pExpBar = NULL;
    
    m_pExpProgressBack = NULL;
    m_pExpProgress = NULL;
    
    m_pFollowerBack = NULL;
    m_pRemainMark = NULL;
    
    m_pExpMask = NULL;
    m_pMoneyMask = NULL;
    
    m_pLevelMark = NULL;
    
    m_vecFollowerList.clear();
    
    m_pMoneyTotal = NULL;
    m_pExpTotal = NULL;
    m_pRemainExpTotal = NULL;
    m_pPlayerLevel = NULL;
    
    m_pAddFriendNormal = NULL;
    m_pAddFriendSelect = NULL;
    
    // 战斗结算属性变量
    m_fBaseAnimTime = 0.3f;
    
    m_iCurrentMoney = 0;
    m_iCurrentExp = 0;
    m_iCurrentRemainExp = 0;
    
    m_bIsExplosionEnable = false;
    m_bIsAnimOver = false;
    m_bIsExpEventFinish = false;
    m_bIsFrameEnable = true;
}

/************************************************************************/
#pragma mark - Layer_BattleResult类析构函数
/************************************************************************/
Layer_BattleResult::~Layer_BattleResult()
{
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_BattleResult类初始化函数
/************************************************************************/
bool Layer_BattleResult::init()
{
    this -> setIsTouchEnabled(true);
    
    //播放战斗胜利背景音乐
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> playBackgroundMusic("title.mp3", true);
    
    // 设置深度值
    // setVertexZ(2.0f);
    
    char szTemp[32] = {0};
    
    // 初始化背景遮罩
    m_pMaskLayer = CCLayerColor::layerWithColor(ccc4(0, 0, 0, 0));
    if (m_pMaskLayer != NULL)
        addChild(m_pMaskLayer);
    
    // 初始化顶部遮罩
    m_pTopMask = CCLayerColor::layerWithColorWidthHeight(ccc4(0, 0, 0, 255), 320.0f, 50.0f);
    if (m_pTopMask != NULL)
    {
        m_pTopMask->setPosition(ccp(0.0f, 480.0f));
        addChild(m_pTopMask);
    }
    
    // 初始化底部遮罩
    m_pBottomMask = CCLayerColor::layerWithColorWidthHeight(ccc4(0, 0, 0, 255), 320.0f, 50.0f);
    if (m_pBottomMask != NULL)
    {
        m_pBottomMask->setPosition(ccp(0.0f, -50.0f));
        addChild(m_pBottomMask);
    }
    
    // 初始化文字精灵
    m_pWordSprite = CCSprite::spriteWithSpriteFrameName("word.png");
    m_pWordSprite->setPosition(ccp(160.0f, 507.0f));
    addChild(m_pWordSprite);
    
    // 初始化金钱底条
    m_pMoneyBar = CCSprite::spriteWithSpriteFrameName("money_bar.png");
    if (m_pMoneyBar != NULL)
    {
        m_pMoneyBar->setPosition(ccp(475.0f, 360.0f));
        addChild(m_pMoneyBar);
    }
    
    // 初始化经验底条
    m_pExpBar = CCSprite::spriteWithSpriteFrameName("exp_bar.png");
    if (m_pExpBar != NULL)
    {
        m_pExpBar->setPosition(ccp(475.0f, 320.0f));
        addChild(m_pExpBar);
    }
    
    // 初始化经验进度底条
    m_pExpProgressBack = CCSprite::spriteWithSpriteFrameName("exp_progress_back.png");
    if (m_pExpProgressBack != NULL)
    {
        m_pExpProgressBack->setPosition(ccp(160.0f, 280.0f));
        m_pExpProgressBack->setOpacity(0);
        addChild(m_pExpProgressBack);
    }
    
    // 初始化经验进度条
    m_pExpProgress = KNProgress::node();
    UIInfo stInfo;
    
    // 文件名
    memcpy(stInfo.szNormalImage, "result_exp_progress.png", 32);
    // 百分比
    stInfo.fPercentage = KNUIFunction::GetSingle()->GetExpPercent();
    // 位置
    stInfo.point = ccp(160.0f, 280.0f);
    if (m_pExpProgress->init(stInfo) == true)
    {
        m_pExpProgress->SetVisibleRect(CCRectMake(160.0f - 140.0f, 0.0f, 320.0f, 480.0f));
        m_pExpProgress->GetProgressSprite()->setOpacity(0);
        addChild(m_pExpProgress);
    }
    
    // 初始化小弟底图
    m_pFollowerBack = CCSprite::spriteWithSpriteFrameName("followerget_back.png");
    if (m_pFollowerBack != NULL)
    {
        m_pFollowerBack->setPosition(ccp(480.0f, 145.0f));
        addChild(m_pFollowerBack);
    }
    
    // 初始化剩余纹理标记
    m_pRemainMark = CCSprite::spriteWithSpriteFrameName("lose.png");
    if (m_pRemainMark != NULL)
    {
        m_pRemainMark->setPosition(ccp(60.0f, 265.0f));
        addChild(m_pRemainMark);
        m_pRemainMark->setOpacity(0);
    }
    
    // 初始化经验遮罩
    m_pExpMask = CCSprite::spriteWithSpriteFrameName("exp_mask.png");
    if (m_pExpMask != NULL)
    {
        m_pExpMask->setPosition(ccp(161.0f, 320.0f));
        m_pExpMask->setOpacity(0);
        addChild(m_pExpMask);
    }
    
    // 初始化金钱遮罩
    m_pMoneyMask = CCSprite::spriteWithSpriteFrameName("money_mask.png");
    if (m_pMoneyMask != NULL)
    {
        m_pMoneyMask->setPosition(ccp(161.0f, 360.0f));
        m_pMoneyMask->setOpacity(0);
        addChild(m_pMoneyMask);
    }
    
    // 初始化等级标记
    m_pLevelMark = CCSprite::spriteWithSpriteFrameName("result_level.png");
    if (m_pLevelMark != NULL)
    {
        m_pLevelMark->setPosition(ccp(36.0f, 285.0f));
        m_pLevelMark->setOpacity(0);
        addChild(m_pLevelMark);
    }
    
    // 初始化金钱总数
    m_pMoneyTotal = CCLabelAtlas::labelWithString("0", "moneyNum-hd.png", 14, 19, '.');
    if (m_pMoneyTotal != NULL)
    {
        m_pMoneyTotal->setPosition(ccp(220.0f, 350.0f));
        addChild(m_pMoneyTotal);
        m_pMoneyTotal->setIsVisible(false);
    }
    
    // 初始化经验总数
    m_pExpTotal = CCLabelAtlas::labelWithString("0", "expNum-hd.png", 14, 19, '.');
    if (m_pExpTotal != NULL)
    {
        m_pExpTotal->setPosition(ccp(220.0f, 310.0f));
        addChild(m_pExpTotal);
        m_pExpTotal->setIsVisible(false);
    }
    
    // 初始化剩余经验总数
    m_pRemainExpTotal = CCLabelAtlas::labelWithString("0", "loseNum-hd.png", 15, 21, '.');
    if (m_pRemainExpTotal != NULL)
    {
        m_pRemainExpTotal->setPosition(ccp(140.0f, 255.0f));
        addChild(m_pRemainExpTotal);
        m_pRemainExpTotal->setIsVisible(false);
    }
    
    // 初始化友情点数字
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerLevel);
    m_pPlayerLevel = CCLabelAtlas::labelWithString(szTemp, "UINum-hd.png", 8, 14, '.');
    if (m_pPlayerLevel != NULL)
    {
        m_pPlayerLevel->setIsVisible(false);
        m_pPlayerLevel->setPosition(ccp(45.0f, 283.0f));
        addChild(m_pPlayerLevel);
    }
    
    // 初始化确认按钮
    m_pAddFriendNormal = CCSprite::spriteWithSpriteFrameName("button_liveok_normal.png");
    if (m_pAddFriendNormal != NULL)
    {
        m_pAddFriendNormal->setPosition(ccp(270.0f, 40.0f));
        m_pAddFriendNormal->setIsVisible(false);
        addChild(m_pAddFriendNormal);
    }
    m_pAddFriendSelect = CCSprite::spriteWithSpriteFrameName("button_liveok_select.png");
    if (m_pAddFriendSelect != NULL)
    {
        m_pAddFriendSelect->setPosition(ccp(270.0f, 40.0f));
        m_pAddFriendSelect->setIsVisible(false);
        addChild(m_pAddFriendSelect);
    }
    
    // 显示战斗统计界面
    show();
    
    // 播放烟花
    ProcessFrameEffect(NULL);
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_BattleResult类销毁函数
/************************************************************************/
void Layer_BattleResult::Destroy()
{
    CCTouchDispatcher::sharedDispatcher() -> removeDelegate(this);
    
    // 移除战斗对象
    removeChild(m_pMaskLayer, true);
    
    // 移除战斗胜利文字精灵
    removeChild(m_pWordSprite, true);
    // 移除金钱底条
    removeChild(m_pMoneyBar, true);
    // 移除经验底条
    removeChild(m_pExpBar, true);
    
    // 移除经验底条
    removeChild(m_pExpProgressBack, true);
    // 移除经验进度条
    removeChild(m_pExpProgress, true);
    
    // 移除小弟底图
    removeChild(m_pFollowerBack, true);
    // 移除剩余纹理标记
    removeChild(m_pRemainMark, true);
    
    // 移除经验遮罩
    removeChild(m_pExpMask, true);
    // 移除金钱遮罩
    removeChild(m_pMoneyMask, true);
    
    // 移除小弟icon
    vector<Follower*>::iterator iter;
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
        removeChild(*iter, false);
    
    // 移除金钱总数
    removeChild(m_pMoneyTotal, true);
    // 移除经验总数
    removeChild(m_pExpTotal, true);
    // 移除剩余总数
    removeChild(m_pRemainExpTotal, true);
    // 移出玩家等级
    removeChild(m_pPlayerLevel, true);
    
    // 停止烟花
    m_bIsFrameEnable = false;
    
    removeAllChildrenWithCleanup(true);
}

/************************************************************************/
#pragma mark - Layer_BattleResult类显示函数
/************************************************************************/
void Layer_BattleResult::show()
{
    AnimStep1(NULL);
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 动画步骤1
/************************************************************************/
void Layer_BattleResult::AnimStep1(CCObject* pObject)
{
    // 淡入背景遮罩
    CCFadeTo* pFadeTo = CCFadeTo::actionWithDuration(m_fBaseAnimTime, 100.0f);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleResult::AnimStep2));
    if (pFadeTo != NULL && pFunc != NULL)
        m_pMaskLayer->runAction(CCSequence::actions(pFadeTo, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 动画步骤2
/************************************************************************/
void Layer_BattleResult::AnimStep2(CCObject* pObject)
{
    CCMoveTo* pTopMove = CCMoveTo::actionWithDuration(m_fBaseAnimTime, ccp(0.0f, 480.0f - 50.0f));
    CCMoveTo* pBottomMove = CCMoveTo::actionWithDuration(m_fBaseAnimTime, ccp(0.0f, 0.0f));
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleResult::AnimStep3));
    if (pTopMove != NULL && pBottomMove != NULL && pFunc != NULL)
    {
        m_pTopMask->runAction(pTopMove);
        m_pBottomMask->runAction(CCSequence::actions(pBottomMove, pFunc, NULL));
    }
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 动画步骤3
/************************************************************************/
void Layer_BattleResult::AnimStep3(CCObject* pObject)
{
    // 进入文字
    CCPoint pos = m_pWordSprite->getPosition();
    CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(m_fBaseAnimTime, ccp(pos.x, 410.0f));
    CCMoveTo* pMoveTo1 = CCMoveTo::actionWithDuration(m_fBaseAnimTime / 2.0f, ccp(pos.x, 420.0f));
    CCMoveTo* pMoveTo2 = CCMoveTo::actionWithDuration(m_fBaseAnimTime / 2.0f, ccp(pos.x, 410.0f));
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleResult::AnimStep4));
    if (pMoveTo != NULL && pMoveTo1 != NULL && pMoveTo2 != NULL && pFunc != NULL)
        m_pWordSprite->runAction(CCSequence::actions(pMoveTo, pMoveTo1, pMoveTo2, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 动画步骤4
/************************************************************************/
void Layer_BattleResult::AnimStep4(CCObject* pObject)
{
    // 金钱条进入
    CCMoveTo* pMoveMoneyTo = CCMoveTo::actionWithDuration(m_fBaseAnimTime / 2.0f, ccp(160.0f, 360.0f));
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleResult::startMoneyNumberAdd));
    if (pMoveMoneyTo != NULL && pFunc != NULL)
        m_pMoneyBar->runAction(CCSequence::actions(pMoveMoneyTo, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 动画步骤5
/************************************************************************/
void Layer_BattleResult::AnimStep5(CCObject* pObject)
{
    // 这个时候能点击
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    // 经验条进入
    CCMoveTo* pMoveExpTo = CCMoveTo::actionWithDuration(m_fBaseAnimTime / 2.0f, ccp(160.0f, 320.0f));
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleResult::startExpNumberAdd));
    if (pMoveExpTo && pFunc != NULL)
        m_pExpBar->runAction(CCSequence::actions(pMoveExpTo, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 动画步骤6
/************************************************************************/
void Layer_BattleResult::AnimStep6(CCObject* pObject)
{
    // 进度条底条淡入
    CCFadeIn* pFadeIn = CCFadeIn::actionWithDuration(m_fBaseAnimTime);
    if (pFadeIn != NULL)
        m_pExpProgressBack->runAction(pFadeIn);
    
    // 剩余纹理淡入
    pFadeIn = CCFadeIn::actionWithDuration(m_fBaseAnimTime);
    if (pFadeIn != NULL)
        m_pRemainMark->runAction(pFadeIn);
    
    // 等级标记淡入
    pFadeIn = CCFadeIn::actionWithDuration(m_fBaseAnimTime);
    if (pFadeIn != NULL)
        m_pLevelMark->runAction(pFadeIn);
    
    // 等级进入
    m_pPlayerLevel->setIsVisible(true);
    
    // 计算剩余经验
    if (PlayerDataManage::m_GuideBattleMark)
        m_iCurrentRemainExp = 200;
    else
        m_iCurrentRemainExp = PlayerDataManage::m_PlayerLevelUpExperience - PlayerDataManage::m_PlayerExperience;
    
    // 进度条淡入
    pFadeIn = CCFadeIn::actionWithDuration(m_fBaseAnimTime);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleResult::startRemainExpNumberAdd));
    if (pFunc != NULL && pFadeIn != NULL)
        m_pExpProgress->GetProgressSprite()->runAction(CCSequence::actions(pFadeIn, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 动画步骤7
/************************************************************************/
void Layer_BattleResult::AnimStep7(CCObject* pObject)
{
    // 小弟底图进入
    CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(m_fBaseAnimTime, ccp(160.0f, 145.0f));
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleResult::AnimStep8));
    if (pMoveTo != NULL)
        m_pFollowerBack->runAction(CCSequence::actions(pMoveTo, pFunc, NULL));
    
    m_vecFollowerList.clear();
    list<Follower*> followerList = Layer_Enemy::ShareInstance()->m_PlayerBattleResList;
    list<Follower*>::iterator iter;
    int index = 0;
    float height = 230.0f;
    for (iter = followerList.begin(); iter != followerList.end(); iter++)
    {
//        if (IsNewFollowerInPackage(*iter) == true)
//            continue;
        
        if (index % 5 == 0)
            height -= 60.0f;
        
        // 生成小弟icon
        CCSprite* pIcon = CCSprite::spriteWithSpriteFrameName((*iter)->GetData()->Follower_IconName);
        if (pIcon != NULL)
        {
            pIcon->setIsVisible(true);
            pIcon->setOpacity(0);
            
            (*iter)->addChild(pIcon, 0, FollowerChild_Icon);
        }
        
        // 生成小弟边框
        char szTemp[32] = {0};
        sprintf(szTemp, "FollowerIcon_%d.png",  (*iter)->GetData()->Follower_Profession);
        CCSprite* pIconFrame = CCSprite::spriteWithSpriteFrameName(szTemp);
        if (pIconFrame != NULL)
        {
            pIconFrame->setIsVisible(true);
            pIconFrame->setOpacity(0);
            
            (*iter)->addChild(pIconFrame, 0, FollowerChild_IconFrame);
        }
        
        (*iter)->setPosition(ccp(40.0f + (index % 5) * 60.0f, height));
        
        addChild(*iter);
        
        m_vecFollowerList.push_back(*iter);
        
        ++index;
    }
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 动画步骤8
/************************************************************************/
void Layer_BattleResult::AnimStep8(CCObject* pObject)
{
    vector<Follower*>::iterator iter;
    int i = 0;
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
    {
        CCDelayTime* pTime1 = CCDelayTime::actionWithDuration(0.5f * i);
        CCDelayTime* pTime2 = CCDelayTime::actionWithDuration(0.5f * i);
        CCFadeIn* pFadeIn1 = CCFadeIn::actionWithDuration(0.1f);
        CCFadeIn* pFadeIn2 = CCFadeIn::actionWithDuration(0.1f);
        if (pTime1 != NULL && pTime2 != NULL && pFadeIn1 != NULL)
        {
            (*iter)->getChildByTag(FollowerChild_Icon)->runAction(CCSequence::actions(pTime1, pFadeIn1, NULL));
            (*iter)->getChildByTag(FollowerChild_IconFrame)->runAction(CCSequence::actions(pTime2, pFadeIn2, NULL));
        }
        
        ++i;
    }
    
    // 打开按钮
    m_pAddFriendNormal->setIsVisible(true);
    
    // 增加友情点
    PlayerDataManage::m_PlayerFriendValue += Scene_Battle::m_MissionFriendValue;
    
    // 动画播放结束
    m_bIsAnimOver = true;
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 打开金钱数量增加
/************************************************************************/
void Layer_BattleResult::startMoneyNumberAdd(CCObject* pObject)
{
    schedule(schedule_selector(Layer_BattleResult::MoneyNumberAdd), 0.01f);
    
    m_pMoneyTotal->setIsVisible(true);
}

char szTemp[16] = {0};
/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 打开经验数量增加
/************************************************************************/
void Layer_BattleResult::startExpNumberAdd(CCObject* pObject)
{
    schedule(schedule_selector(Layer_BattleResult::ExpNumberAdd));
    
    m_pExpTotal->setIsVisible(true);
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 打开剩余经验数量增加
/************************************************************************/
void Layer_BattleResult::startRemainExpNumberAdd(CCObject* pObject)
{
    schedule(schedule_selector(Layer_BattleResult::RemainNumberMinus));
    
    m_pRemainExpTotal->setIsVisible(true);
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 金钱数量增加
/************************************************************************/
void Layer_BattleResult::MoneyNumberAdd(ccTime dt)
{
    // test
    if (m_iCurrentMoney < Layer_Enemy::ShareInstance()->m_PlayerBattleMoney)
    {
        m_iCurrentMoney += 29;
        
        sprintf(szTemp, "%d", m_iCurrentMoney);
        
        m_pMoneyTotal->setString(szTemp);
    }
    else
    {
        sprintf(szTemp, "%d", Layer_Enemy::ShareInstance()->m_PlayerBattleMoney);
        m_pMoneyTotal->setString(szTemp);
        
        unschedule(schedule_selector(Layer_BattleResult::MoneyNumberAdd));
        CCFadeIn* pFadeIn = CCFadeIn::actionWithDuration(m_fBaseAnimTime);
        CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(m_fBaseAnimTime);
        CCScaleTo* pScaleTo1 = CCScaleTo::actionWithDuration(m_fBaseAnimTime, 1.2f);
        CCScaleTo* pScaleTo2 = CCScaleTo::actionWithDuration(m_fBaseAnimTime, 1.0f);
        CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleResult::AnimStep5));
        if (pFunc != NULL)
        {
            m_pMoneyMask->runAction(CCSequence::actions(pScaleTo1, pScaleTo2, NULL));
            m_pMoneyMask->runAction(CCSequence::actions(pFadeIn, pFadeOut, pFunc, NULL));
        }
    }
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 经验数量增加
/************************************************************************/
void Layer_BattleResult::ExpNumberAdd(ccTime dt)
{
    if (m_iCurrentExp < Layer_Enemy::ShareInstance()->m_PlayerBattleExperience)
    {
        m_iCurrentExp += 29;
        
        sprintf(szTemp, "%d", m_iCurrentExp);
        
        m_pExpTotal->setString(szTemp);
    }
    else
    {
        sprintf(szTemp, "%d", Layer_Enemy::ShareInstance()->m_PlayerBattleExperience);
        m_pExpTotal->setString(szTemp);
        
        unschedule(schedule_selector(Layer_BattleResult::ExpNumberAdd));
        CCFadeIn* pFadeIn = CCFadeIn::actionWithDuration(m_fBaseAnimTime);
        CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(m_fBaseAnimTime);
        CCScaleTo* pScaleTo1 = CCScaleTo::actionWithDuration(m_fBaseAnimTime, 1.2f);
        CCScaleTo* pScaleTo2 = CCScaleTo::actionWithDuration(m_fBaseAnimTime, 1.0f);
        CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleResult::AnimStep6));
        if (pFunc != NULL)
        {
            m_pExpMask->runAction(CCSequence::actions(pScaleTo1, pScaleTo2, NULL));
            m_pExpMask->runAction(CCSequence::actions(pFadeIn, pFadeOut, pFunc, NULL));
        }
    }
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 剩余经验减少
/************************************************************************/
void Layer_BattleResult::RemainNumberMinus(ccTime dt)
{
    if (Layer_Enemy::ShareInstance()->m_PlayerBattleExperience > 0 && !m_bIsExpEventFinish)
    {
        // 减少获得经验和剩余经验值
        Layer_Enemy::ShareInstance()->m_PlayerBattleExperience -= 1;
        
        // 增加玩家经验
        PlayerDataManage::m_PlayerExperience += 1;
        
        // 更新进度条
        if (PlayerDataManage::m_PlayerExperience >= PlayerDataManage::m_PlayerLevelUpExperience)
        {
            // 增加玩家等级
            PlayerDataManage::m_PlayerLevel += 1;
            
            // 重置经验
            PlayerDataManage::m_PlayerExperience = 0;
            
            // 计算下一升级所需经验
            PlayerDataManage::m_PlayerLevelUpExperience = KNUIFunction::GetSingle()->GetPlayerNeedExpForNextLevel();
            
            // 计算下一级行动力
            if (PlayerDataManage::m_PlayerLevel <= 40)
                PlayerDataManage::m_PlayerMaxActivity = 20 + (PlayerDataManage::m_PlayerLevel - 1) * 2.0f;
            else
                PlayerDataManage::m_PlayerMaxActivity = 98 + PlayerDataManage::m_PlayerLevel - 1;
            
            // 计算剩余经验
            m_iCurrentRemainExp = PlayerDataManage::m_PlayerLevelUpExperience - PlayerDataManage::m_PlayerExperience;
            
            // 计算最大行动力
            if (PlayerDataManage::m_PlayerLevel < 40)
                PlayerDataManage::m_PlayerMaxActivity = 20 + (PlayerDataManage::m_PlayerLevel - 1) * 2;
            else
                PlayerDataManage::m_PlayerMaxActivity = 98 + (PlayerDataManage::m_PlayerLevel - 1) * 1;
            
            // 恢复行动力
            PlayerDataManage::m_PlayerActivity = PlayerDataManage::m_PlayerMaxActivity;
            CCDirector::sharedDirector() -> setActionComeBack(false);
            PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime = 0;
            
            // 刷新界面
            char szTemp[32] = {0};
            sprintf(szTemp, "%d", PlayerDataManage::m_PlayerLevel);
            m_pPlayerLevel->setString(szTemp);
            
            // 升级效果
            CreateLevelUpEffect(NULL);
        }
        
        // 刷新获得经验
        sprintf(szTemp, "%d", Layer_Enemy::ShareInstance()->m_PlayerBattleExperience);
        m_pExpTotal->setString(szTemp);
        
        // 刷新到界面
        sprintf(szTemp, "%d", PlayerDataManage::m_PlayerExperience);
        m_pRemainExpTotal->setString(szTemp);
        
        // 设置进度条
        m_pExpProgress->SetPercent(KNUIFunction::GetSingle()->GetExpPercent());
    }
    else
    {
        unschedule(schedule_selector(Layer_BattleResult::RemainNumberMinus));
        CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleResult::AnimStep7));
        if (pFunc != NULL)
            runAction(pFunc);
    }
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 创建升级效果
/************************************************************************/
void Layer_BattleResult::CreateLevelUpEffect(CCObject* pObject)
{
    CCSprite* pSprite = CCSprite::spriteWithSpriteFrameName("result_levelup_1.png");
    pSprite->setOpacity(0);
    pSprite->setScale(2.0f);
    pSprite->setPosition(ccp(160.0f, 240.0f));
    addChild(pSprite);
    
    CCScaleTo* pScaleTo = CCScaleTo::actionWithDuration(0.5f, 1.0f);
    CCFadeIn* pFadeIn = CCFadeIn::actionWithDuration(0.5f);
    
    CCAnimation* pAnimation = CreateAnimation("result_levelup_", 0.12f);
    CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnimation);
    
    CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_BattleResult::PassLevelUpEffect));
    
    pSprite->runAction(pFadeIn);
    pSprite->runAction(CCSequence::actions(pScaleTo, pAnimate, pFuncN, NULL));
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 升级效果过渡
/************************************************************************/
void Layer_BattleResult::PassLevelUpEffect(CCObject* pObject)
{
    CCSprite* pSprite = dynamic_cast<CCSprite*>(pObject);
    if (pSprite == NULL)
        return;
    
    CCScaleTo* pScaleTo2 = CCScaleTo::actionWithDuration(0.5f, 2.0f);
    CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(0.5f);
    
    CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_BattleResult::EndLevelUpEffect));
    
    pSprite->runAction(pFadeOut);
    pSprite->runAction(CCSequence::actions(pScaleTo2, pFuncN, NULL));
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 结束升级效果
/************************************************************************/
void Layer_BattleResult::EndLevelUpEffect(CCObject* pObject)
{
    CCSprite* pSprite = dynamic_cast<CCSprite*>(pObject);
    if (pSprite == NULL)
        return;
    
    removeChild(pSprite, true);
}

/************************************************************************/
#pragma mark - Layer_BattleResult类回调函数 : 处理烟花效果函数
/************************************************************************/
void Layer_BattleResult::ProcessFrameEffect(CCObject* pObject)
{
    if (!m_bIsFrameEnable)
        return;
    
    CCParticleSystem* m_pLevelExplosion = CCParticleExplosion::node();
    if (m_pLevelExplosion != NULL)
    {
        m_pLevelExplosion->setTexture(CCTextureCache::sharedTextureCache()->addImage("explosion_star.png"));
        m_pLevelExplosion->setIsAutoRemoveOnFinish(true);
        
        m_pLevelExplosion->setLife(0.02f);
        m_pLevelExplosion->setSpeed(80.0f);
        m_pLevelExplosion->setEmissionRate(400);
        m_pLevelExplosion->setGravity(ccp(0.0f, -2.0f));
        
        m_pLevelExplosion->setPosition(ccp(rand() % 320, rand() % 480));
        
        addChild(m_pLevelExplosion, 10);
    }
    
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(1.0f);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_BattleResult::ProcessFrameEffect));
    
    runAction(CCSequence::actions(pTime, pFunc, NULL));
}

/************************************************************************/
#pragma mark - Layer_BattleResult类事件函数 : 点击
/************************************************************************/
bool Layer_BattleResult::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
    // 是否快速结束经验增加
    if (!m_bIsExpEventFinish)
    {
        FinishExpEvent();
        m_bIsExpEventFinish = true;
    }
    
    // 如果动画没有结束，则退出
    if (!m_bIsAnimOver)
        return false;
    
    // 加好友按钮
    if (m_pAddFriendNormal->getIsVisible() == true)
    {
        CCPoint point = convertTouchToNodeSpace(pTouch);
        
        CCPoint pos = m_pAddFriendNormal->getPosition();
        CCSize size = m_pAddFriendNormal->getContentSize();
        
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
        {
            m_pAddFriendNormal->setIsVisible(false);
            m_pAddFriendSelect->setIsVisible(true);
        }
    }
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_BattleResult类事件函数 : 滑动
/************************************************************************/
void Layer_BattleResult::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}

/************************************************************************/
#pragma mark - Layer_BattleResult类事件函数 : 弹出
/************************************************************************/
void Layer_BattleResult::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
    // 获得被点击的位置
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    // 检测被点击的小弟
    vector<Follower*>::iterator iter;
    for (iter = m_vecFollowerList.begin(); iter != m_vecFollowerList.end(); iter++)
    {
        CCPoint pos = (*iter)->getPosition();
        CCSize size = (*iter)->getChildByTag(FollowerChild_Icon)->getContentSize();
        
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
        {
            KNUIFunction::GetSingle()->ShowFollowerFullInfo(*iter, 10000);
            return;
        }
    }
    
    // 加好友按钮
    if (m_pAddFriendSelect->getIsVisible() == true)
    {
        CCPoint point = convertTouchToNodeSpace(pTouch);
        
        CCPoint pos = m_pAddFriendNormal->getPosition();
        CCSize size = m_pAddFriendNormal->getContentSize();
        
        m_pAddFriendNormal->setIsVisible(true);
        m_pAddFriendSelect->setIsVisible(false);
        
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
        {
            if (PlayerDataManage::m_GuideMark)
            {
                GameController::ShareInstance() -> ReadyToGameScene(GameState_BackToGame);
                return;
            }
            
            // 弹出界面
            Layer_BattleMessage * LB = Layer_BattleMessage::node();
            LB -> SetMessageState(MESSAGE_ADDFRIEND);
            LB -> setPosition(ccp(0, 50));
            addChild(LB);
        }
    }
}

/************************************************************************/
#pragma mark - Layer_BattleResult类获得的小弟是否是新类型函数
/************************************************************************/
bool Layer_BattleResult::IsNewFollowerInPackage(Follower *pFollower)
{
    PlayerFollowerDepot followerList = *PlayerDataManage::ShareInstance()->GetMyFollowerDepot();
    for (PlayerFollowerDepot::iterator iter = followerList.begin(); iter != followerList.end(); iter++)
    {
        list <Follower*>::iterator it2;
        for (it2 = iter->second.begin(); it2 != iter->second.end(); it2++)
        {
            if (pFollower->GetData()->Follower_ID == (*it2)->GetData()->Follower_ID)
                return true;
        }
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_BattleResult类快速结束经验增加函数
/************************************************************************/
void Layer_BattleResult::FinishExpEvent()
{
    // 把经验加到玩家经验上
    PlayerDataManage::m_PlayerExperience += Layer_Enemy::ShareInstance()->m_PlayerBattleExperience;
    
    // 进行升级操作
    while (PlayerDataManage::m_PlayerExperience >= PlayerDataManage::m_PlayerLevelUpExperience)
    {
        // 增加玩家升级
        PlayerDataManage::m_PlayerLevel += 1;
        
        // 减去升级的经验
        PlayerDataManage::m_PlayerExperience -= PlayerDataManage::m_PlayerLevelUpExperience;
        
        // 计算下一升级所需经验
        PlayerDataManage::m_PlayerLevelUpExperience = KNUIFunction::GetSingle()->GetPlayerNeedExpForNextLevel();
        
        // 计算下一级行动力
        if (PlayerDataManage::m_PlayerLevel <= 40)
            PlayerDataManage::m_PlayerMaxActivity = 20 + (PlayerDataManage::m_PlayerLevel - 1) * 2;
        else
            PlayerDataManage::m_PlayerMaxActivity = 98 + PlayerDataManage::m_PlayerLevel - 1;
        
        // 恢复行动力
        PlayerDataManage::m_PlayerActivity = PlayerDataManage::m_PlayerMaxActivity;
        CCDirector::sharedDirector() -> setActionComeBack(false);
        PlayerDataManage::ShareInstance() -> m_PlayerActivityComeBackTime = 0;
        
        // 升级动画效果
        CreateLevelUpEffect(NULL);
    }

    // 刷新界面－等级
    char szTemp[32] = {0};
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerLevel);
    m_pPlayerLevel->setString(szTemp);
    
    // 刷新到界面－经验
    sprintf(szTemp, "%d", PlayerDataManage::m_PlayerExperience);
    m_pRemainExpTotal->setString(szTemp);
    
    // 清空获得经验
    sprintf(szTemp, "%d", 0);
    m_pExpTotal->setString(szTemp);
    
    // 设置进度条
    m_pExpProgress->SetPercent(KNUIFunction::GetSingle()->GetExpPercent());
}