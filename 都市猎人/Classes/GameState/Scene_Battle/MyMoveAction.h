//
//  MyMoveAction.h
//  MidNightCity
//
//  Created by 强 张 on 12-6-20.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_MyMoveAction_h
#define MidNightCity_MyMoveAction_h

#include "cocos2d.h"

enum MyMove_Horizontal
{
    MyMove_Left = 0,
    MyMove_Right
};

enum MyMove_Vertical
{
    MyMove_Up = 0,
    MyMove_Down
};

class MyMoveAction : public cocos2d::CCActionInterval
{
public:
    bool initWithDuration(cocos2d::ccTime duration, float offset, float maxoffset);

    virtual void startWithTarget(cocos2d::CCNode * pTarget);
    virtual void update(cocos2d::ccTime time);
	virtual bool isDone(void);
           
public:
	static MyMoveAction * actionWithDuration(cocos2d::ccTime duration, float offset, float maxoffset);
    
protected:
    cocos2d::CCPoint        m_StartPosition;
    cocos2d::CCPoint        m_MovePosition;
    
    MyMove_Horizontal       m_Horizontal;
    MyMove_Vertical         m_Vertical;
    
    float                   m_Offset;
    float                   m_MaxOffset;
};

#endif
