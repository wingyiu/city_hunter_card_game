//
//  Layer_Follower.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-6-7.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//


#include "Layer_Follower.h"
USING_NS_CC;

#include "Follower.h"
#include "Battle_Follower.h"

#include "Battle_Enemy.h"
#include "Layer_Enemy.h"

#include "Layer_Block.h"
#include "Layer_Display.h"

#include "PlayerDataManage.h"
#include "ServerDataManage.h"
#include "SystemDataManage.h"

#include "MyMoveAction.h"
#include "Action_MyMove.h"

#include "KNUIFunction.h"

#include "Skill.h"

Layer_Follower * Layer_Follower::m_Instance = NULL;

/************************************************************************/
#pragma mark 单例
/************************************************************************/
Layer_Follower * Layer_Follower::ShareInstance()
{
    if(m_Instance == NULL)
    {
        return NULL;
    }
    return  m_Instance;
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 析构
/************************************************************************/
Layer_Follower::~Layer_Follower()
{
    m_Instance = NULL;
    
    printf("Layer_Follower::释放完成\n");
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 初始化
/************************************************************************/
bool Layer_Follower::init()
{
    if(! CCLayer::init())
        return false;
    
    this -> setIsTouchEnabled(true);
    CCTouchDispatcher::sharedDispatcher() -> addTargetedDelegate(this, 3, true);
    
    /////////////////////////////////////////////////
    m_WinSize = CCDirector::sharedDirector() -> getWinSize();
    
    //所有小弟血量
    m_FollowerHP = m_OriginalHP = m_LabelHPString = 0.0f;
    //所有小弟的防御
    m_FollowerDefense = m_OriginalDefense = 0.0f;
    //增加血量使用的
    m_FollowerAddedHp = m_AddedHp = m_LabelAddedHpString = 0.0f;
    //受击损失的血量
    m_FollowerHurtHp = 0.0f;

    ////////背景////////
    CCSprite * Background = CCSprite::spriteWithSpriteFrameName("battle_mask.png");
    CCAssert(Background != NULL, "Layer_Follower::init()");
    Background -> setPosition(ccp(m_WinSize.width / 2, 283));
    this -> addChild(Background, 0, LFChildTag_Background);
    
    ////////小弟////////
    CreateFollowerList();
    
    ////////血条框////////
    CCSprite * Hp_Background = CCSprite::spriteWithSpriteFrameName("PlayerHP_BG.png");
    CCAssert(Hp_Background != NULL, "Layer_Follower::init()");
    Hp_Background -> setPosition(ccp(m_WinSize.width / 2, 278));
    this -> addChild(Hp_Background, 0, LFChildTag_HPBackground);
    
    ////////血条////////
    CCSprite * Hp = CCSprite::spriteWithFile("PlayerHP-hd.png");
    CCAssert(Hp != NULL, "Layer_Follower::init()");
    Hp -> setAnchorPoint(ccp(0.0f, 0.5f));
    m_HpPoint = CCPointMake(26, 277);
    Hp -> setPosition(m_HpPoint);
    this -> addChild(Hp, 0, LFChildTag_HP);
    
    ////////血条遮罩////////
    CCSprite * HpMask = CCSprite::spriteWithSpriteFrameName("PlayerHP_Mask.png");
    CCAssert(HpMask != NULL, "Layer_Follower::init()");
    HpMask -> setPosition(ccp(m_WinSize.width / 2, 278));
    this -> addChild(HpMask, 0, LFChildTag_HPMask);
    
    ////////血量数字大小////////
    CCSprite * NumImage1 = CCSprite::spriteWithFile("Number_PlayerHP-hd.png");
    m_NumSize_PlayerHP = CCSizeMake(NumImage1 -> getTextureRect().size.width / 12, NumImage1 -> getTextureRect().size.height);
    ////////加血量数字大小////////
    CCSprite * NumImage2 = CCSprite::spriteWithFile("Number_AddHP-hd.png");
    m_NumSize_AddHP = CCSizeMake(NumImage2 -> getTextureRect().size.width / 12, NumImage2 -> getTextureRect().size.height);

    ////////血量数字显示////////
    char buff[32];
    sprintf(buff, "%d", (int)m_LabelHPString);
    CCLabelAtlas * pLabelAtlas = CCLabelAtlas::labelWithString(buff, "Number_PlayerHP-hd.png", m_NumSize_PlayerHP.width, m_NumSize_PlayerHP.height, '.');
    pLabelAtlas -> setPosition(ccp(277, 273));
    this -> addChild(pLabelAtlas, 0, LFChildTag_HPlable);
    
    m_IsFollowerAttack = false;
    m_OperationEnable = true;
    m_TriggerBuffing = false;
    m_CanBeDecreaseSkillCD = false;
    
    ////////关于技能的////////
    m_TeamOverComePro = 0;
    m_TeamBeOverComePro = 0;
    m_OverComeAttackPercent = 0.0;

    m_AddAttackPercent = 0.0;
    
    m_ProDerate = 0;
    m_ProDerateAttackPercent = 0.0;
    
    m_DerateAttack = 0;
    
    m_AvoidDeadlyAttackPercent = 0.0;
    
    ////////计算combo用的////////
    m_NoDispelCount = 0;
    m_AllCombo = 0;
    for(int i = 4; i < 31; i ++)
    {
        m_AllDispel[i] = 0;
    }
    
    m_FollowerCombo[BLOCK_TYPE1_SWORD] = 0;
    m_FollowerCombo[BLOCK_TYPE2_FIST] = 0;
    m_FollowerCombo[BLOCK_TYPE3_GUN] = 0;
    m_FollowerCombo[BLOCK_TYPE4_HP] = 0;
    
    m_FollowerDispel[BLOCK_TYPE1_SWORD] = 0;
    m_FollowerDispel[BLOCK_TYPE2_FIST] = 0;
    m_FollowerDispel[BLOCK_TYPE3_GUN] = 0;
    m_FollowerDispel[BLOCK_TYPE4_HP] = 0;
    
    m_Instance = this;
    
    return true;
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 退出释放
/************************************************************************/
void Layer_Follower::onExit()
{
    CCLayer::onExit();
    
    CCTouchDispatcher::sharedDispatcher() -> removeDelegate(this);
    
    this -> removeAllChildrenWithCleanup(true);
    
    for(Battle_FollowerList::iterator it = m_FollowerList.begin(); it != m_FollowerList.end(); )
    {
        it -> second -> release();
        m_FollowerList.erase(it ++);
    }
    m_FollowerFightList.clear();
    
    for(list<Skill*>::iterator it = m_FollowerBuffList.begin(); it != m_FollowerBuffList.end(); )
    {
        (*it) -> release();
        m_FollowerBuffList.erase(it ++);
    }
    
    m_ReadyProfession.clear();
    m_AllDispel.clear();
    m_FollowerCombo.clear();
    m_FollowerDispel.clear();
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 创建小弟
/************************************************************************/
void Layer_Follower::CreateFollowerList()
{
    PlayerFollowerTeam MyFollowerTeam = *PlayerDataManage::ShareInstance() -> GetMyFollowerTeam();
    
    for(PlayerFollowerTeam::iterator it = MyFollowerTeam.begin(); it != MyFollowerTeam.end(); ++ it)
    {
        Battle_Follower * BF = Battle_Follower::node();
        BF -> SetHost(it -> second);
        BF -> Initialize();
        
        m_FollowerList[it -> first] = BF;
        m_FollowerHP += BF -> GetHost() -> GetData() -> Follower_HP;
        m_FollowerDefense += BF -> GetHost() -> GetData() -> Follower_Defense;
        m_FollowerAddedHp += BF -> GetHost() -> GetData() -> Follower_Comeback;
    }
    m_OriginalHP = m_LabelHPString = m_FollowerHP;
    m_OriginalDefense = m_FollowerDefense;
    m_LabelAddedHpString = m_FollowerAddedHp;
    
    /*m_FollowerList[1] -> GetHost() -> GetData() -> Follower_TeamSkillType = 8;
    m_FollowerList[1] -> GetHost() -> GetData() -> Follower_TeamSkillID = 4;
    
    m_FollowerList[2] -> GetHost() -> GetData() -> Follower_CommonlySkillType = 3;
    m_FollowerList[2] -> GetHost() -> GetData() -> Follower_CommonlySkillID = 5;
    
    m_FollowerList[3] -> GetHost() -> GetData() -> Follower_CommonlySkillType = 4;
    m_FollowerList[3] -> GetHost() -> GetData() -> Follower_CommonlySkillID = 12;*/
    
    for(Battle_FollowerList::iterator it = m_FollowerList.begin(); it != m_FollowerList.end(); ++ it)
    {
        it -> second -> setPosition(ccp(55 + 53 * (it -> first - 1), 312));
        this -> addChild(it -> second);
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 减少CD时间
/************************************************************************/
void Layer_Follower::DecreaseSkillCDTime()
{
    if(m_CanBeDecreaseSkillCD)
    {
        for(Battle_FollowerList::iterator it = m_FollowerList.begin(); it != m_FollowerList.end(); ++ it)
        {
            it -> second -> DecreaseSkillCDTime();
        }
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 战斗引导中把第一个小弟的CD时间去掉
/************************************************************************/
void Layer_Follower::GuideReSetSkillCD()
{
    m_FollowerList.begin() -> second -> GuideReSetSkillCD();
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 触发队长技能
/************************************************************************/
void Layer_Follower::TriggerTeamSkill()
{
    Battle_Follower * BF1 = m_FollowerList.begin() -> second;
    Battle_Follower * BF2 = m_FollowerList.rbegin() -> second;
    
    if(BF1 -> GetHost() -> GetData() -> Follower_TeamSkillType == BF2 -> GetHost() -> GetData() -> Follower_TeamSkillType)
    {
        Battle_Follower * BF = (BF1 -> GetHost() -> GetData() -> Follower_TeamSkillID >= BF2 -> GetHost() -> GetData() -> Follower_TeamSkillID) ? BF1 : BF2;
        BF -> TriggerTeamSkill();
    }
    else
    {
        m_FollowerList.begin() -> second -> TriggerTeamSkill();
        m_FollowerList.rbegin() -> second -> TriggerTeamSkill();
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 点击
/************************************************************************/
bool Layer_Follower::ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent)
{
    if(! m_OperationEnable)
        return false;
    
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    //如果是战斗的引导
    if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
    {
        if(m_FollowerList.begin() -> second -> CheckBeTouch(point))
        {
            if(Layer_Enemy::ShareInstance() -> GetGuideStep() == 6)
            {
                Layer_GameGuide::GetSingle() -> FinishBattleGuideStep5();
                
                Layer_Block::ShareInstance() -> SetOperationEnable(false);
                Layer_Enemy::ShareInstance() -> SetOperationEnable(false);
                m_OperationEnable = false;
                
                Layer_GameGuide::GetSingle() -> RunBattleGuideStep6();
                
                Layer_Enemy::ShareInstance() -> SetGuideStep(7);
            }
            return true;
        }
    }
    else
    {
        for(Battle_FollowerList::iterator it = m_FollowerList.begin(); it != m_FollowerList.end(); ++ it)
        {
            if(it -> second -> CheckBeTouch(point))
            {
                return true;
            }
        }
    }
    return false;
}

void Layer_Follower::ccTouchMoved(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent)
{

}

void Layer_Follower::ccTouchEnded(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent)
{
  
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 关于攻击
#pragma mark {
/************************************************************************/
/************************************************************************/
#pragma mark 增加combo
/************************************************************************/
void Layer_Follower::AddFollowerCombo(int profession, int dispel, CCPoint startpoint)
{
    ReadyAttack(profession);
    
    if(dispel >= 4 && dispel <= 30)     
        m_AllDispel[dispel] += 1;
    
    map <int, int>::iterator it1 = m_FollowerCombo.find(profession);
    if(it1 != m_FollowerCombo.end())
    {
        it1 -> second += 1;
        if(it1 -> second == 1)
        {
            Layer_Display::ShareInstance() -> DisplayFollowerStartAttack(profession, startpoint);
        }
    }
    
    map <int, int>::iterator it2 = m_FollowerDispel.find(profession);
    if(it2 != m_FollowerDispel.end())
    {
        if(it2 -> second < dispel)
            it2 -> second = dispel;
    }
}

void Layer_Follower::DisplayFollowerCombo(int dispelCount)
{
    m_AllCombo += dispelCount;
    Layer_Display::ShareInstance() -> DisplayCombo(m_HpPoint, m_AllCombo);
}


/************************************************************************/
#pragma mark 某个职业开始准备战斗
/************************************************************************/
void Layer_Follower::ReadyAttack(int profession)
{
    for(list <int>::iterator it = m_ReadyProfession.begin(); it != m_ReadyProfession.end(); ++ it)
    {
        if((*it) == profession)     return;
    }
    m_ReadyProfession.push_back(profession);
    
    if(profession == BLOCK_TYPE1_SWORD)
    {
        for(Battle_FollowerList::iterator it = m_FollowerList.begin(); it != m_FollowerList.end(); ++ it)
        {
            if(it -> second -> GetHost() -> GetData() -> Follower_Profession == SWORD)
            {
                m_FollowerFightList.push_back(it -> second);
            }
        }
    }
    else if(profession == BLOCK_TYPE2_FIST)
    {
        for(Battle_FollowerList::iterator it = m_FollowerList.begin(); it != m_FollowerList.end(); ++ it)
        {
            if(it -> second -> GetHost() -> GetData() -> Follower_Profession == FIST)
            {
                m_FollowerFightList.push_back(it -> second);
            }
        }
    }
    else if(profession == BLOCK_TYPE3_GUN)
    {
        for(Battle_FollowerList::iterator it = m_FollowerList.begin(); it != m_FollowerList.end(); ++ it)
        {
            if(it -> second -> GetHost() -> GetData() -> Follower_Profession == GUN)
            {
                m_FollowerFightList.push_back(it -> second);
            }
        }
    }
    else if(profession == BLOCK_TYPE4_HP)
    {
        
    }
}

/************************************************************************/
#pragma mark 开始动作
/************************************************************************/
void Layer_Follower::StartAction()
{    
    //禁止操作
    Layer_Display::ShareInstance() -> OperationEnable(false);
    //敌人目标准心消失
    Layer_Enemy::ShareInstance() -> GetTargetEnemy() -> HidePost();
    
    //如果没有消除，就直接敌人攻击
    if(m_AllCombo == 0)
    {
        m_IsFollowerAttack = false;
        m_CanBeDecreaseSkillCD = false;
        Layer_Enemy::ShareInstance()-> StartAttack();
        
        m_NoDispelCount ++;
        if(m_NoDispelCount == 2)//2次未消除
        {
            Layer_Display::ShareInstance() -> DisplayBullShit(1);
        }else if(m_NoDispelCount == 3)//3次未消除
        {
            Layer_Display::ShareInstance() -> DisplayBullShit(2);
        }else if(m_NoDispelCount == 5)//5次未消除
        {
            Layer_Display::ShareInstance() -> DisplayBullShit(3);
            m_NoDispelCount = 0;
        }
    }
    else 
    {
        //未消除次数归0
        m_NoDispelCount = 0;
        //可以减少CD时间
        m_CanBeDecreaseSkillCD = true;
        
        //如果有消除攻击系的方块，有攻击小弟，显示攻击力数字跳动加成
        if(! m_FollowerFightList.empty())
        {
            m_IsFollowerAttack = true;
            
            //计算对敌人的伤害
            CalculateAttackEnemy();
            DisplayAttackNumberJump();
        }else
            m_IsFollowerAttack = false;
        
        //如果有加血，显示加血变化
        if(m_FollowerCombo[BLOCK_TYPE4_HP] != 0)
        {
            //计算额外的加血值
            CalculateAddHp();
            DisplayAddHpJump();
        }else 
        {
            if(! m_IsFollowerAttack)
            {
                Layer_Enemy::ShareInstance()-> StartAttack();
            }
        }
        
        if(m_AllCombo >= 2 && m_AllCombo <= 4)
        {
            Layer_Display::ShareInstance() -> DisplayBullShit(4);
        }else if(m_AllCombo >= 5 && m_AllCombo <= 9)
        {
            Layer_Display::ShareInstance() -> DisplayBullShit(5);
        }else if(m_AllCombo >= 10)
        {
            Layer_Display::ShareInstance() -> DisplayBullShit(6);
        }
    }
    
    //////清除计算的东西//////
    m_ReadyProfession.clear();
    
    m_AllCombo = 0;
    for(int i = 4; i < 31; i ++)
    {
        m_AllDispel[i] = 0;
    }
    
    m_FollowerCombo[BLOCK_TYPE1_SWORD] = 0;
    m_FollowerCombo[BLOCK_TYPE2_FIST] = 0;
    m_FollowerCombo[BLOCK_TYPE3_GUN] = 0;
    m_FollowerCombo[BLOCK_TYPE4_HP] = 0;
    
    m_FollowerDispel[BLOCK_TYPE1_SWORD] = 0;
    m_FollowerDispel[BLOCK_TYPE2_FIST] = 0;
    m_FollowerDispel[BLOCK_TYPE3_GUN] = 0;
    m_FollowerDispel[BLOCK_TYPE4_HP] = 0;
    ////////////////////////
}


/************************************************************************/
#pragma mark 计算对敌人的伤害 (攻击力加成)
/************************************************************************/
void Layer_Follower::CalculateAttackEnemy()
{
    int AllDis = 0;
    for(int i = 4; i < 31; i ++)
    {
        AllDis += m_AllDispel[i] * (i - 3);
    }

    //计算伤害加成
    for(list <Battle_Follower *>::iterator it = m_FollowerFightList.begin(); it != m_FollowerFightList.end(); ++ it)
    {
        float AddedAttack = 0;
        int FAttack = (*it) -> GetHost() -> GetData() -> Follower_Attack;
        
        if((*it) -> GetHost() -> GetData() -> Follower_Profession == SWORD)
        {
            AddedAttack = FAttack * (0.25 * (m_AllCombo - m_FollowerCombo[SWORD] + AllDis) + m_FollowerCombo[SWORD]);
            if(m_FollowerDispel[SWORD] > 4)     (*it) -> SetIsAttackAllEnemy(true);
        }
        else if((*it) -> GetHost() -> GetData() -> Follower_Profession == FIST)
        {
            AddedAttack = FAttack * (0.25 * (m_AllCombo - m_FollowerCombo[FIST] + AllDis) + m_FollowerCombo[FIST]);
            if(m_FollowerDispel[FIST] > 4)      (*it) -> SetIsAttackAllEnemy(true);
        }
        else if((*it) -> GetHost() -> GetData() -> Follower_Profession == GUN)
        {
            AddedAttack = FAttack * (0.25 * (m_AllCombo - m_FollowerCombo[GUN] + AllDis) + m_FollowerCombo[GUN]);
            if(m_FollowerDispel[GUN] > 4)       (*it) -> SetIsAttackAllEnemy(true);
        }
        
        (*it) -> SetAddedAttack(AddedAttack);
        (*it) -> CalculateAttackEnemy();
    }
}

/************************************************************************/
#pragma mark 计算加血的加成
/************************************************************************/
void Layer_Follower::CalculateAddHp()
{
    int AllDis = 0;
    for(int i = 4; i < 31; i ++)
    {
        AllDis += m_AllDispel[i] * (i - 3);
    }
    
    //计算血量加成
    m_AddedHp = m_FollowerAddedHp * (0.25 * (m_AllCombo - m_FollowerCombo[BLOCK_TYPE4_HP] + AllDis) + m_FollowerCombo[BLOCK_TYPE4_HP]);
    if(m_FollowerHP + (m_FollowerAddedHp + m_AddedHp) <= m_OriginalHP)
        m_FollowerHP += m_FollowerAddedHp + m_AddedHp;
    else
        m_FollowerHP = m_OriginalHP;
    
    //如果血量大于30％
    if(m_FollowerHP > m_OriginalHP / 100 * 30)
    {
        CCSprite * pSprite = dynamic_cast<CCSprite *>(this -> getChildByTag(LFChildTag_HP));
        pSprite -> stopAllActions();
        pSprite -> setOpacity(255);
    }
}

/************************************************************************/
#pragma mark 显示攻击力数字跳动加成 
/************************************************************************/
void Layer_Follower::DisplayAttackNumberJump()
{
    for(list <Battle_Follower *>::iterator it = m_FollowerFightList.begin(); it != m_FollowerFightList.end(); ++ it)
    {
        (*it) -> DisplayNumberJump();
    }
    schedule(schedule_selector(Layer_Follower::CheckDisplayAttackNumberJumpOver));
}

void Layer_Follower::CheckDisplayAttackNumberJumpOver()
{
    for(list <Battle_Follower *>::iterator it = m_FollowerFightList.begin(); it != m_FollowerFightList.end(); ++ it)
    {
        if((*it) -> GetUnderShowAttackNumber())  return;
    }
    
    unschedule(schedule_selector(Layer_Follower::CheckDisplayAttackNumberJumpOver));
    schedule(schedule_selector(Layer_Follower::DisplayAttack), 0.2f);
}

/************************************************************************/
#pragma mark 显示攻击 (结束小弟队伍这边的动作)
/************************************************************************/
void Layer_Follower::DisplayAttack()
{
    list <Battle_Follower *>::iterator it = m_FollowerFightList.begin();
    
    (*it) -> DisplayAttack();
    m_FollowerFightList.erase(it);
    
    //战斗队列都出手之后
    if(m_FollowerFightList.empty())
    {
        m_FollowerFightList.clear();
        unschedule(schedule_selector(Layer_Follower::DisplayAttack));

        Layer_Enemy::ShareInstance() -> CheckEnemyUnderAttack();
    }
}
#pragma mark }
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark 关于被攻击
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/
/************************************************************************/
#pragma mark 计算被伤害
/************************************************************************/
void Layer_Follower::CalculateBeAttackResult(int attack)
{
    if(m_FollowerHP - attack <= 0.0f)
    {
        //如果有防止被秒杀的技能
        if(m_AvoidDeadlyAttackPercent != 0.0)
        {
            if(m_FollowerHP >= m_OriginalHP * m_AvoidDeadlyAttackPercent)
            {
                m_FollowerHP = 10;
            }else
                m_FollowerHP = 0.0f;
        }else 
            m_FollowerHP = 0.0f;
    }
    else
    {
        m_FollowerHP -= attack;
        m_FollowerHurtHp += attack;
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示被攻击
/************************************************************************/
void Layer_Follower::DisplayBeAttack(float attack)
{
    if(m_LabelHPString <= 0.0f)    return;
    else if(m_LabelHPString - attack <= m_FollowerHP)
    {
        m_LabelHPString = m_FollowerHP;
    }
    else    
        m_LabelHPString -= attack;
    
    //血条数字变化
    char buff[32];
    sprintf(buff, "%d", (int)m_LabelHPString);
    CCLabelAtlas * pLabelAtlas = static_cast<CCLabelAtlas*>(this -> getChildByTag(LFChildTag_HPlable));
    pLabelAtlas -> setString(buff);
    
    //血条震动
    CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(LFChildTag_HP));
    /*if(! pSprite -> getActionByTag(ActionTag_HpShake))
    {
        Action_MyMove * pMyMove = Action_MyMove::actionWithDuration(5, 6, 0.005f);
        pMyMove -> setTag(ActionTag_HpShake);
        pSprite -> runAction(pMyMove);
    }*/
    
    //播放被攻击的音效
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> playEffect("6.wav");
    
    //血条缩短
    pSprite-> setTextureRect(CCRectMake(0, 0, m_LabelHPString / m_OriginalHP * 250.0, 10));
    
    //飘出被伤害的数字
    Layer_Display::ShareInstance() -> DisplayFollowerHurt(m_HpPoint, attack);

    //背景受击震动
    Layer_Enemy::ShareInstance() -> BGShake();
    
    //全屏效果
    Layer_Display::ShareInstance() -> DisplayFollowerBeAttack();
}

void Layer_Follower::DisplayBeAttackOver()
{
    CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(LFChildTag_HP));
    
    //血量低于30％的时候
    if(m_FollowerHP <= m_OriginalHP / 100 * 30)
    {
        CCFadeTo * pDark = CCFadeTo ::actionWithDuration(0.3f, 100);
        CCFadeTo * pLight = CCFadeTo ::actionWithDuration(0.3f, 255);
        CCFiniteTimeAction * pAction = CCSequence::actions(pDark, pLight, NULL);
        pSprite -> runAction(CCRepeat::actionWithAction(pAction, 10000));
    }
    
    if(m_FollowerHurtHp >= m_OriginalHP / 100 * 50)
    {
        Layer_Display::ShareInstance() -> DisplayBullShit(7);
    }
    else if(m_FollowerHurtHp >= m_OriginalHP / 100 * 30)
    {
        Layer_Display::ShareInstance() -> DisplayBullShit(8);
    }
    else if(m_FollowerHurtHp >= m_OriginalHP / 100 * 10)
    {
        Layer_Display::ShareInstance() -> DisplayBullShit(9);
    }
    m_FollowerHurtHp = 0.0f;
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 关于加血
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/
/************************************************************************/
#pragma mark 显示加血
/************************************************************************/
void Layer_Follower::DisplayAddHp()
{
    char buff[32];
    sprintf(buff, "%d", (int)m_LabelAddedHpString);
    CCLabelAtlas * pLabelAtlas = CCLabelAtlas::labelWithString(buff, "Number_AddHP-hd.png", m_NumSize_AddHP.width, m_NumSize_AddHP.height, '.');
    pLabelAtlas -> setPosition(ccp(m_WinSize.width / 2 - (strlen(buff) * m_NumSize_AddHP.width) / 2, m_HpPoint.y - m_NumSize_AddHP.height / 2));
    pLabelAtlas -> setOpacity(0);
    
    CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("JiaHao.png");
    pSprite -> setPosition(ccp(pLabelAtlas -> getPosition().x - 20, m_HpPoint.y));
    pSprite -> setOpacity(0);
    
    CCFadeIn * pFadeIn1 = CCFadeIn::actionWithDuration(0.5f);
    CCFadeIn * pFadeIn2 = CCFadeIn::actionWithDuration(0.5f);
    
    pLabelAtlas -> runAction(pFadeIn1);
    pSprite -> runAction(pFadeIn2);
    
    Layer_Display::ShareInstance() -> addChild(pLabelAtlas, 0, LFChildTag_AddHpLabel);
    Layer_Display::ShareInstance() -> addChild(pSprite, 0, LFChildTag_AddHpJiaHao);
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示加血变化
/************************************************************************/
void Layer_Follower::DisplayAddHpJump()
{
    this -> schedule(schedule_selector(Layer_Follower::DisplayAddHpJumpFun1));
}

//变化加血数字
void Layer_Follower::DisplayAddHpJumpFun1()
{
    float count = 5;
    float Dis = m_AddedHp / count;
    
    CCLabelAtlas * pLabelAtlas = static_cast<CCLabelAtlas*>(Layer_Display::ShareInstance() -> getChildByTag(LFChildTag_AddHpLabel));
    
    if(m_LabelAddedHpString + Dis < m_FollowerAddedHp + m_AddedHp)
        m_LabelAddedHpString += Dis;
    else 
        m_LabelAddedHpString = m_FollowerAddedHp + m_AddedHp;
    
    char buff[32];
    sprintf(buff, "%d", (int)m_LabelAddedHpString);
    pLabelAtlas -> setString(buff);

    if(m_LabelAddedHpString >= m_FollowerAddedHp + m_AddedHp)
    {
        this -> unschedule(schedule_selector(Layer_Follower::DisplayAddHpJumpFun1));
        DisplayAddHpJumpFun2();
    }
}

void Layer_Follower::DisplayAddHpJumpFun2()
{
    CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("AddHP_1.png");
    pSprite -> setPosition(ccp(m_WinSize.width / 2, m_HpPoint.y));
    
    CCAnimation * pAnimation = CreateAnimation("AddHP_", 0.04f);
    CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
    CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Follower::DisplayAddHpJumpFun4));
    
    pSprite -> runAction(CCSequence::actions(pAnimate, pCallFuncN, NULL));
    Layer_Display::ShareInstance() -> addChild(pSprite);
    
    this -> schedule(schedule_selector(Layer_Follower::DisplayAddHpJumpFun3));
    
    //播放加血的音效
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> playEffect("addhp.wav");
}

void Layer_Follower::DisplayAddHpJumpFun3()
{
    int count = 20;
    
    if(m_LabelHPString + (m_OriginalHP / count) < m_FollowerHP)
        m_LabelHPString += (m_OriginalHP / count);
    else 
        m_LabelHPString = m_FollowerHP;

    //血条数字变化
    char buff[32];
    sprintf(buff, "%d", (int)m_LabelHPString);
    CCLabelAtlas * pLabelAtlas = static_cast<CCLabelAtlas*>(this -> getChildByTag(LFChildTag_HPlable));
    pLabelAtlas -> setString(buff);
    
    //血条变长
    CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(LFChildTag_HP));
    pSprite-> setTextureRect(CCRectMake(0, 0, m_LabelHPString / m_OriginalHP * 250, 10));
    
    if(m_LabelHPString >= m_FollowerHP)
        this -> unschedule(schedule_selector(Layer_Follower::DisplayAddHpJumpFun3));
}

void Layer_Follower::DisplayAddHpJumpFun4(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
    
    m_LabelAddedHpString = m_FollowerAddedHp;
    m_AddedHp = 0.0f;
    
    CCFadeOut * pFadeOut1 = CCFadeOut::actionWithDuration(0.3f);
    CCFadeOut * pFadeOut2 = CCFadeOut::actionWithDuration(0.3f);
    CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Follower::DisplayAddHpJumpFun5));
    
    Layer_Display::ShareInstance() -> getChildByTag(LFChildTag_AddHpLabel) -> runAction(CCSequence::actions(pFadeOut1, pCallFuncN, NULL));
    Layer_Display::ShareInstance() -> getChildByTag(LFChildTag_AddHpJiaHao) -> runAction(CCSequence::actions(pFadeOut2, pCallFuncN, NULL));
}

void Layer_Follower::DisplayAddHpJumpFun5(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
    
    if(! m_IsFollowerAttack)
    {
        m_IsFollowerAttack = true;
        Layer_Enemy::ShareInstance() -> StartAttack();
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 显示复活加血
/************************************************************************/
void Layer_Follower::DisplayReliveAddHp()
{
    m_FollowerHP = m_OriginalHP;
    
    CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("AddHP_1.png");
    pSprite -> setPosition(ccp(m_WinSize.width / 2, m_HpPoint.y));
    
    CCAnimation * pAnimation = CreateAnimation("AddHP_", 0.04f);
    CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
    CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Follower::DisplayReliveAddHpCallBack1));
    pSprite -> runAction(CCSequence::actions(pAnimate, pCallFuncN, NULL));
    
    Layer_Display::ShareInstance() -> addChild(pSprite);
    
    this -> schedule(schedule_selector(Layer_Follower::DisplayReliveAddHpCallBack2));
    
    //播放加血的音效
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> playEffect("addhp.wav");
}

void Layer_Follower::DisplayReliveAddHpCallBack1(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
}

void Layer_Follower::DisplayReliveAddHpCallBack2()
{
    if(m_LabelHPString + 200 <= m_FollowerHP)
        m_LabelHPString += 200;
    else 
        m_LabelHPString = m_FollowerHP;
    
    //血条数字变化
    char buff[32];
    sprintf(buff, "%d", (int)m_LabelHPString);
    CCLabelAtlas * pLabelAtlas = static_cast<CCLabelAtlas*>(this -> getChildByTag(LFChildTag_HPlable));
    pLabelAtlas -> setString(buff);
    
    //血条变长
    CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(LFChildTag_HP));
    pSprite-> setTextureRect(CCRectMake(0, 0, m_LabelHPString / m_OriginalHP * 250, 10));
    
    if(m_LabelHPString >= m_FollowerHP)
    {
        this -> unschedule(schedule_selector(Layer_Follower::DisplayReliveAddHpCallBack2));
        //如果血量大于30％
        if(m_FollowerHP > m_OriginalHP / 100 * 30)
        {
            pSprite -> stopAllActions();
            pSprite -> setOpacity(255);
        }
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示技能加血
/************************************************************************/
void Layer_Follower::DisplayAddHpBySkill(int value)
{
    if(m_FollowerHP + value <= m_OriginalHP)
        m_FollowerHP += value;
    else 
        m_FollowerHP = m_OriginalHP;
    
    char buff[32];
    sprintf(buff, "%d", value);
    CCLabelAtlas * pLabelAtlas = CCLabelAtlas::labelWithString(buff, "Number_AddHP-hd.png", m_NumSize_AddHP.width, m_NumSize_AddHP.height, '.');
    pLabelAtlas -> setPosition(ccp(m_WinSize.width / 2 - (strlen(buff) * m_NumSize_AddHP.width) / 2, m_HpPoint.y - m_NumSize_AddHP.height / 2));
    pLabelAtlas -> setOpacity(0);
    
    CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("JiaHao.png");
    pSprite -> setPosition(ccp(pLabelAtlas -> getPosition().x - 20, m_HpPoint.y));
    pSprite -> setOpacity(0);
    
    CCFadeIn * pFadeIn1 = CCFadeIn::actionWithDuration(0.3f);
    CCFadeIn * pFadeIn2 = CCFadeIn::actionWithDuration(0.3f);
    
    pLabelAtlas -> runAction(pFadeIn1);
    pSprite -> runAction(pFadeIn2);
    
    Layer_Display::ShareInstance() -> addChild(pLabelAtlas, 0, LFChildTag_AddHpLabel2);
    Layer_Display::ShareInstance() -> addChild(pSprite, 0, LFChildTag_AddHpJiaHao2);
    
    CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(0.4f);
    CCCallFunc * pDisplayAddHpBySkillCallBack1 = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Follower::DisplayAddHpBySkillCallBack1));
    this -> runAction(CCSequence::actions(pDelayTime, pDisplayAddHpBySkillCallBack1, NULL));
}

void Layer_Follower::DisplayAddHpBySkillCallBack1()
{
    CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("AddHP_1.png");
    pSprite -> setPosition(ccp(m_WinSize.width / 2, m_HpPoint.y));
    
    CCAnimation * pAnimation = CreateAnimation("AddHP_", 0.04f);
    CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
    CCCallFuncN * pDisplayAddHpBySkillCallBack2 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Follower::DisplayAddHpBySkillCallBack2));
    
    pSprite -> runAction(CCSequence::actions(pAnimate, pDisplayAddHpBySkillCallBack2, NULL));
    Layer_Display::ShareInstance() -> addChild(pSprite);
    
    this -> schedule(schedule_selector(Layer_Follower::DisplayAddHpBySkillCallBack4));
    
    //播放加血的音效
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> playEffect("addhp.wav");
}

void Layer_Follower::DisplayAddHpBySkillCallBack2(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
    
    CCFadeOut * pFadeOut1 = CCFadeOut::actionWithDuration(0.3f);
    CCFadeOut * pFadeOut2 = CCFadeOut::actionWithDuration(0.3f);
    CCCallFuncN * pDisplayAddHpBySkillCallBack3 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Follower::DisplayAddHpBySkillCallBack3));
    
    Layer_Display::ShareInstance() -> getChildByTag(LFChildTag_AddHpLabel2) -> runAction(CCSequence::actions(pFadeOut1, pDisplayAddHpBySkillCallBack3, NULL));
    Layer_Display::ShareInstance() -> getChildByTag(LFChildTag_AddHpJiaHao2) -> runAction(CCSequence::actions(pFadeOut2, pDisplayAddHpBySkillCallBack3, NULL));
}

void Layer_Follower::DisplayAddHpBySkillCallBack3(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
}

void Layer_Follower::DisplayAddHpBySkillCallBack4()
{
    int count = 20;
    
    if(m_LabelHPString + (m_OriginalHP / count) < m_FollowerHP)
        m_LabelHPString += (m_OriginalHP / count);
    else
        m_LabelHPString = m_FollowerHP;
    
    //血条数字变化
    char buff[32];
    sprintf(buff, "%d", (int)m_LabelHPString);
    CCLabelAtlas * pLabelAtlas = static_cast<CCLabelAtlas*>(this -> getChildByTag(LFChildTag_HPlable));
    pLabelAtlas -> setString(buff);
    
    //血条变长
    CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(LFChildTag_HP));
    pSprite-> setTextureRect(CCRectMake(0, 0, m_LabelHPString / m_OriginalHP * 250, 10));
    
    if(m_LabelHPString >= m_FollowerHP)
    {
        this -> unschedule(schedule_selector(Layer_Follower::DisplayAddHpBySkillCallBack4));
        //如果血量大于30％
        if(m_FollowerHP > m_OriginalHP / 100 * 30)
        {
            pSprite -> stopAllActions();
            pSprite -> setOpacity(255);
        }
    }
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark 计算加减防御值
/************************************************************************/
void Layer_Follower::AddDefense(float defense)
{
    m_FollowerDefense += defense;
}

void Layer_Follower::DecreaseDefense(float defense)
{
    m_FollowerDefense -= defense;
    if(m_FollowerDefense <= m_OriginalDefense)
        m_FollowerDefense = m_OriginalDefense;
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 增加血量上限
/************************************************************************/
void Layer_Follower::AddMaxHP(int added)
{
    m_FollowerHP += added;
    m_OriginalHP = m_FollowerHP;
    m_LabelHPString = m_FollowerHP;
    
    //血条数字变化
    char buff[32];
    sprintf(buff, "%d", (int)m_LabelHPString);
    CCLabelAtlas * pLabelAtlas = static_cast<CCLabelAtlas*>(this -> getChildByTag(LFChildTag_HPlable));
    pLabelAtlas -> setString(buff);
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/


#pragma mark 关于Buff
#pragma mark {
/************************************************************************/
#pragma mark 加入Buff
/************************************************************************/
void Layer_Follower::AddBuff(Skill * buff)
{
    bool bExist = false;
    
    for(list <Skill*>::iterator it = m_FollowerBuffList.begin(); it != m_FollowerBuffList.end(); )
    {
        //如果已经有了相同类型的debuff
        if(buff -> GetData() -> Skill_Type == (*it) -> GetData() -> Skill_Type)
        {
            //如果新的buff的ID大于旧的buff的ID
            if(buff -> GetData() -> Skill_ID > (*it) -> GetData() -> Skill_ID)
            {
                //旧的buff关闭逻辑
                (*it) -> CloseSkill();
                //新的buff触发逻辑
                buff -> DisposalSkill();
                
                m_FollowerBuffList.push_back(buff);
                DisplayBuff();
                
                RemoveBuff(*it, false);
            }
            //如果新的debuff的ID小于等于旧的debuff的ID
            else 
            {
                buff -> release();
            }
            bExist = true;
            break;
        }
        else ++ it;
    }
    
    //如果没有相同类型的debuff
    if(! bExist)
    {
        //buff触发逻辑
        buff -> DisposalSkill();
        
        m_FollowerBuffList.push_back(buff);
        DisplayBuff();
    }
}

/************************************************************************/
#pragma mark 显示Buff
/************************************************************************/
void Layer_Follower::DisplayBuff()
{
    list<Skill*>::reverse_iterator it = m_FollowerBuffList.rbegin();
    
    (*it) -> setScale(3.0f);
    (*it) -> setPosition(ccp(305, 298 + 21 * (m_FollowerBuffList.size() - 1)));
    
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.5f, 1.0f);
    (*it) -> runAction(pScaleTo);
    
    this -> addChild(*it);
}

/************************************************************************/
#pragma mark  显示移除Buff
/************************************************************************/
void Layer_Follower::RemoveBuff(Skill * buff, bool needclose)
{
    for(list <Skill*>::iterator it = m_FollowerBuffList.begin(); it != m_FollowerBuffList.end(); ++ it)
    {
        if(buff == (*it))
        {
            CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.5f);
            CCCallFuncND * pCallFunc = CCCallFuncND::actionWithTarget(this, callfuncND_selector(Layer_Follower::ReSetPositionBuff), (void*)needclose);
            (*it) -> runAction(CCSequence::actions(pFadeOut, pCallFunc, NULL));
            break;
        }
    }
}

/************************************************************************/
#pragma mark 显示Buff重新设置位置
/************************************************************************/
void Layer_Follower::ReSetPositionBuff(CCNode * sender, void * data)
{
    sender -> removeFromParentAndCleanup(true);
    
    Skill * pSkill      = dynamic_cast<Skill*>(sender);
    bool    pneedclose  = static_cast<bool>(data);
    
    for(list <Skill*>::iterator it = m_FollowerBuffList.begin(); it != m_FollowerBuffList.end(); ++ it)
    {
        if((*it) == pSkill)
        {
            if(pneedclose)  (*it) -> CloseSkill();
            
            (*it) -> release();
            m_FollowerBuffList.erase(it);
            break;
        }
    }
    
    for(list <Skill*>::iterator it = m_FollowerBuffList.begin(); it != m_FollowerBuffList.end(); ++ it)
    {
        CCMoveTo * pMoveTo = CCMoveTo::actionWithDuration(0.5f, ccp(305, 298 + 21 * (m_FollowerBuffList.size() - 1)));
        (*it) -> runAction(pMoveTo);
    }
}

/************************************************************************/
#pragma mark 每回合触发Buff
/************************************************************************/
void Layer_Follower::TriggerBuff()
{
    m_TriggerBuffing = true;
    
    if(! m_FollowerBuffList.empty())
    {
        for(list <Skill*>::iterator it = m_FollowerBuffList.begin(); it != m_FollowerBuffList.end(); ++ it)
        {
            m_FollowerTempBuffList.push_back(*it);
        }
        this -> schedule(schedule_selector(Layer_Follower::TriggerBuffFun), 1.0f);
        
        CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(1.0f + 1.0f * m_FollowerBuffList.size());
        CCCallFunc * pCallFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Follower::TriggerOver));
        this -> runAction(CCSequence::actions(pDelayTime, pCallFunc, NULL));
    }
    else
        TriggerOver();
}

void Layer_Follower::TriggerBuffFun()
{
    list <Skill*>::iterator it = m_FollowerTempBuffList.begin();
    (*it) -> DisposalKeepSkill();
    m_FollowerTempBuffList.erase(it);
    
    if(m_FollowerTempBuffList.empty())
    {
        this -> unschedule(schedule_selector(Layer_Follower::TriggerBuffFun));
        m_FollowerTempBuffList.clear();
    }
}

void Layer_Follower::TriggerOver()
{
    for(list <Skill*>::iterator it = m_FollowerBuffList.begin(); it != m_FollowerBuffList.end(); ++ it)
    {
        if((*it) -> GetData() -> Skill_KeepRound <= 0)
        {
            RemoveBuff((*it), true);
        }
    }
    
    m_TriggerBuffing = false;
}
#pragma mark }
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/




































