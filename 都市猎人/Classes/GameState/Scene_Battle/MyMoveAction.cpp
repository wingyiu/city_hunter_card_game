//
//  MyMoveAction.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-6-20.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "MyMoveAction.h"
USING_NS_CC;

#include "CCActionInterval.h"


MyMoveAction * MyMoveAction::actionWithDuration(ccTime duration, float offset, float maxoffset)
{
    MyMoveAction * pMyMove = new MyMoveAction();
	pMyMove -> initWithDuration(duration, offset, maxoffset);
	pMyMove -> autorelease();
    
	return pMyMove;
}

bool MyMoveAction::initWithDuration(ccTime duration, float offset, float maxoffset)   
{
    if (CCActionInterval::initWithDuration(duration))
	{
        int Dir1 = rand() % 2;
        if(Dir1 == 0)        m_Horizontal = MyMove_Left;
        else if(Dir1 == 1)   m_Horizontal = MyMove_Right;
        
        int Dir2 = 2 + rand() % 2;
        if(Dir2 == 2)        m_Vertical = MyMove_Up;
        else if(Dir2 == 3)   m_Vertical = MyMove_Down;
        
        m_Offset = offset;
        m_MaxOffset = maxoffset;
        
		return true;
	}
    
	return false;
}

void MyMoveAction::startWithTarget(CCNode * pTarget)
{
    CCActionInterval::startWithTarget(pTarget);
	m_StartPosition = pTarget -> getPosition();
    m_MovePosition = m_StartPosition;
}

void MyMoveAction::update(cocos2d::ccTime time)
{
    switch(m_Horizontal)
    {
        case MyMove_Left:
            if(m_MovePosition.x - m_Offset > m_StartPosition.x - m_MaxOffset)
            {
                m_MovePosition.x -= m_Offset;
            }
            else
            {
                m_MovePosition.x = m_StartPosition.x - m_MaxOffset;
                m_Horizontal = MyMove_Right;
            }
            break;
            
        case MyMove_Right:
            if(m_MovePosition.x + m_Offset < m_StartPosition.x + m_MaxOffset)
            {
                m_MovePosition.x += m_Offset;
            }
            else 
            {
                m_MovePosition.x = m_StartPosition.x + m_MaxOffset;
                m_Horizontal = MyMove_Left;
            }
            break;
    }
    
    switch(m_Vertical)
    {
        case MyMove_Up:
            if(m_MovePosition.y + m_Offset < m_StartPosition.y + m_MaxOffset)
            {
                m_MovePosition.y += m_Offset;
            }
            else 
            {
                m_MovePosition.y = m_StartPosition.y + m_MaxOffset;
                m_Vertical = MyMove_Down;
            }
            break;
            
        case MyMove_Down:
            if(m_MovePosition.y - m_Offset > m_StartPosition.y - m_MaxOffset)
            {
                m_MovePosition.y -= m_Offset;
            }
            else 
            {
                m_MovePosition.y = m_StartPosition.y - m_MaxOffset;
                m_Vertical = MyMove_Up;
            }
            break;
    }
    m_pTarget -> setPosition(m_MovePosition);
}


bool MyMoveAction::isDone()
{
    if(m_elapsed >= m_fDuration)
    {
        m_pTarget -> setPosition(m_StartPosition);
        return true;
    }
    return false;
}
























