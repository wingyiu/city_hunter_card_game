//
//  Layer_BattleMessage.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-7-6.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_BattleMessage.h"
#include "Layer_Display.h"
#include "GameController.h"
#include "ServerDataManage.h"
#include "Scene_Battle.h"

/************************************************************************/
#pragma mark - Layer_BattleMessage类构造函数
/************************************************************************/
Layer_BattleMessage::Layer_BattleMessage()
{
    // 为战斗信息对象变量赋初值
    m_pFrameBack = NULL;
    
    for (int i = 0; i < MESSAGE_NUM; i++)
        m_pFrameLayer[i] = NULL;
    
    // 为战斗信息属性变量赋初值
    m_RealSize = CCSizeMake(0, 0);
    
    m_eState = MESSAGE_LIVE;
}

/************************************************************************/
#pragma mark - Layer_BattleMessage类析构函数
/************************************************************************/
Layer_BattleMessage::~Layer_BattleMessage()
{
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_BattleMesssage类初始化函数
/************************************************************************/
bool Layer_BattleMessage::init()
{
    // 初始化cclayer基类
    if (CCLayer::init() == false)
        return false;
    
    // 设置深度值
    // setVertexZ(3.0f);
    
    // 获得屏幕尺寸
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    // 初始化对话框背景
    m_pFrameBack = CCSprite::spriteWithSpriteFrameName("message_back1.png");
    if (m_pFrameBack != NULL)
    {
        m_pFrameBack->setPosition(ccp(winSize.width / 2.0f, winSize.height / 2.0f));
        addChild(m_pFrameBack);
        
        m_RealSize.width = m_pFrameBack -> getTextureRect().size.width;
        m_RealSize.height = m_pFrameBack -> getTextureRect().size.height;        
    }
    
    // 初始化复活对话框图层
    m_pFrameLayer[MESSAGE_LIVE] = CCLayer::node();
    if (m_pFrameLayer[MESSAGE_LIVE] != NULL)
    {
        m_pFrameLayer[MESSAGE_LIVE]->setPosition(ccp(winSize.width / 2.0f, winSize.height / 2.0f));
        addChild(m_pFrameLayer[MESSAGE_LIVE]);
        
        // 初始化对话框信息
        CCSprite* pMessageInfo = CCSprite::spriteWithSpriteFrameName("message_info.png");
        if (pMessageInfo != NULL)
        {
            pMessageInfo->setPosition(ccp(0.0f, 15.0f));
            m_pFrameLayer[MESSAGE_LIVE]->addChild(pMessageInfo, 0, LIVE_QUESTION_ID);
        }
        
        // 初始化放弃按钮
        CCSprite* pGiveupNormal = CCSprite::spriteWithSpriteFrameName("button_giveup_normal.png");
        if (pGiveupNormal != NULL)
        {
            pGiveupNormal->setPosition(ccp(-80.0f, -95.0f));
            m_pFrameLayer[MESSAGE_LIVE]->addChild(pGiveupNormal, 0, BUTTON_GIVEUP_NORMAL_ID);
        }
        CCSprite* pGiveupSelect = CCSprite::spriteWithSpriteFrameName("button_giveup_select.png");
        if (pGiveupSelect != NULL)
        {
            pGiveupSelect->setPosition(ccp(-80.0f, -95.0f));
            pGiveupSelect->setIsVisible(false);
            m_pFrameLayer[MESSAGE_LIVE]->addChild(pGiveupSelect, 0, BUTTON_GIVEUP_SELECT_ID);
        }
        
        // 初始化复活按钮
        CCSprite* pLiveNormal = CCSprite::spriteWithSpriteFrameName("button_live_normal.png");
        if (pLiveNormal != NULL)
        {
            pLiveNormal->setPosition(ccp(80.0f, -95.0f));
            m_pFrameLayer[MESSAGE_LIVE]->addChild(pLiveNormal, 0, BUTTON_LIVE_NORMAL_ID);
        }
        CCSprite* pLiveSelect = CCSprite::spriteWithSpriteFrameName("button_live_select.png");
        if (pLiveSelect != NULL)
        {
            pLiveSelect->setPosition(ccp(80.0f, -95.0f));
            pLiveSelect->setIsVisible(false);
            m_pFrameLayer[MESSAGE_LIVE]->addChild(pLiveSelect, 0, BUTTON_LIVE_SELECT_ID);
        }
        
        // 当前钻石
        CCLabelAtlas* pCurDiamond = CCLabelAtlas::labelWithString("0", "Number_EnemyCD-hd.png", 9, 15, '.');
        pCurDiamond->setPosition(ccp(50.0f, -47.0f));
        pCurDiamond->setScale(1.5f);
        m_pFrameLayer[MESSAGE_LIVE]->addChild(pCurDiamond, 0, LIVE_CUR_DIAMOND);
        
        // 需要钻石
        char szTemp[32] = {0};
        sprintf(szTemp, "%d", Price_ReLive);
        CCLabelAtlas* pNeedDiamond = CCLabelAtlas::labelWithString(szTemp, "number_type_15-hd.png", 13, 21, '.');
        pNeedDiamond->setPosition(ccp(50.0f, -15.0f));
        m_pFrameLayer[MESSAGE_LIVE]->addChild(pNeedDiamond, 0, LIVE_NEED_DIAMOND);
    }
    
    // 初始化放弃对话框
    m_pFrameLayer[MESSAGE_GIVEUP] = CCLayer::node();
    if (m_pFrameLayer[MESSAGE_GIVEUP] != NULL)
    {
        m_pFrameLayer[MESSAGE_GIVEUP]->setPosition(ccp(winSize.width / 2.0f, winSize.height / 2.0f));
        addChild(m_pFrameLayer[MESSAGE_GIVEUP]);
        
        // 初始化对话框询问
        CCSprite* pMessageQues = CCSprite::spriteWithSpriteFrameName("live_question.png");
        if (pMessageQues != NULL)
            m_pFrameLayer[MESSAGE_GIVEUP]->addChild(pMessageQues, 0, LIVE_QUESTION_ID);
        
        // 初始化确认按钮
        CCSprite* pConfirmNormal = CCSprite::spriteWithSpriteFrameName("button_liveok_normal.png");
        if (pConfirmNormal != NULL)
        {
            pConfirmNormal->setPosition(ccp(-80.0f, -95.0f));
            m_pFrameLayer[MESSAGE_GIVEUP]->addChild(pConfirmNormal, 0, BUTTON_CONFIRM_NORMAL_ID);
        }
        CCSprite* pConfirmSelect = CCSprite::spriteWithSpriteFrameName("button_liveok_select.png");
        if (pConfirmSelect != NULL)
        {
            pConfirmSelect->setPosition(ccp(-80.0f, -95.0f));
            pConfirmSelect->setIsVisible(false);
            m_pFrameLayer[MESSAGE_GIVEUP]->addChild(pConfirmSelect, 0, BUTTON_CONFIRM_SELECT_ID);
        }
        
        // 初始化返回按钮
        CCSprite* pBackNormal = CCSprite::spriteWithSpriteFrameName("button_liveback_normal.png");
        if (pBackNormal != NULL)
        {
            pBackNormal->setPosition(ccp(80.0f, -95.0f));
            m_pFrameLayer[MESSAGE_GIVEUP]->addChild(pBackNormal, 0, BUTTON_CANCEL_NORMAL_ID);
        }
        CCSprite* pBackSelect = CCSprite::spriteWithSpriteFrameName("button_liveback_select.png");
        if (pBackSelect != NULL)
        {
            pBackSelect->setPosition(ccp(80.0f, -95.0f));
            pBackSelect->setIsVisible(false);
            m_pFrameLayer[MESSAGE_GIVEUP]->addChild(pBackSelect, 0, BUTTON_CANCEL_SELECT_ID);
        }
    }
    
    // 初始化网络不稳定对话框
    m_pFrameLayer[MESSAGE_NETFAILURE] = CCLayer::node();
    if (m_pFrameLayer[MESSAGE_NETFAILURE] != NULL)
    {
        m_pFrameLayer[MESSAGE_NETFAILURE]->setPosition(ccp(winSize.width / 2.0f, winSize.height / 2.0f));
        addChild(m_pFrameLayer[MESSAGE_NETFAILURE]);
        
        // 初始化重试按钮
        CCSprite* pRetryNormal = CCSprite::spriteWithSpriteFrameName("button_retry_normal.png");
        if (pRetryNormal != NULL)
        {
            pRetryNormal->setPosition(ccp(-80.0f, -95.0f));
            m_pFrameLayer[MESSAGE_NETFAILURE]->addChild(pRetryNormal, 0, BUTTON_RETRY_NORMAL_ID);
        }
        CCSprite* pRetrySelect = CCSprite::spriteWithSpriteFrameName("button_retry_select.png");
        if (pRetrySelect != NULL)
        {
            pRetrySelect->setPosition(ccp(-80.0f, -95.0f));
            pRetrySelect->setIsVisible(false);
            m_pFrameLayer[MESSAGE_NETFAILURE]->addChild(pRetrySelect, 0, BUTTON_RETRY_SELECT_ID);
        }
        
        // 初始化文本
        CCLabelTTF* pTextLabel = CCLabelTTF::labelWithString("网络不稳定", "黑体", 15);
        if (pTextLabel != NULL)
            m_pFrameLayer[MESSAGE_NETFAILURE]->addChild(pTextLabel, 0, LABEL_INFORM_ID);
    }
    
    // 初始化丢失信息对话框
    m_pFrameLayer[MESSAGE_LOST] = CCLayer::node();
    if (m_pFrameLayer[MESSAGE_LOST] != NULL)
    {
        m_pFrameLayer[MESSAGE_LOST]->setPosition(ccp(winSize.width / 2.0f, winSize.height / 2.0f));
        addChild(m_pFrameLayer[MESSAGE_LOST]);
    }
    
    // 初始化添加好友图层
    m_pFrameLayer[MESSAGE_ADDFRIEND] = CCLayer::node();
    if (m_pFrameLayer[MESSAGE_ADDFRIEND] != NULL)
    {
        m_pFrameLayer[MESSAGE_ADDFRIEND]->setPosition(ccp(winSize.width / 2.0f, winSize.height / 2.0f));
        addChild(m_pFrameLayer[MESSAGE_ADDFRIEND]);
        
        // 大背景
        CCSprite* pFrameBack = CCSprite::spriteWithSpriteFrameName("frame_back.png");
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pFrameBack, 0, ADDFRIEND_FRAME_BACK_ID);
        
        // 标题背景
        CCSprite* pTitle = CCSprite::spriteWithSpriteFrameName("friend_edit_title.png");
        pTitle->setPosition(ccp(0.0f, 142.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pTitle, 0, ADDFRIEND_TITLE_ID);
        
        // 小弟头像
        CCSprite* pIcon = CCSprite::spriteWithSpriteFrameName("follower_Icon_1001.png");
        pIcon->setPosition(ccp(-105.0f, 90.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pIcon, 0, FRIENDFOLLOWER_ICON_ID);
        
        // 小弟边框
        CCSprite* pFrame = CCSprite::spriteWithSpriteFrameName("info_frame_1.png");
        pFrame->setPosition(ccp(0.0f, 90.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pFrame, 0, FRIENDFOLLOWER_FRAME_ID);
        
        // 等级标签
        CCSprite* pLevelMark = CCSprite::spriteWithSpriteFrameName("label_lv.png");
        pLevelMark->setPosition(ccp(-100.0f, 70.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pLevelMark, 1);
        
        // 等级背景
        CCSprite* pLevelBack = CCSprite::spriteWithSpriteFrameName("friend_level_mark.png");
        pLevelBack->setPosition(ccp(20.0f, 90.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pLevelBack, 1);
        
        // 好友姓名
        CCLabelTTF* pPlayerName = CCLabelTTF::labelWithString("上帝之手", "黑体", 13);
        pPlayerName->setAnchorPoint(ccp(0.0f, 0.0f));
        pPlayerName->setPosition(ccp(-65.0f, 82.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pPlayerName, 1, FRIENDFOLLOWER_NAME);
        
        // 好友小弟等级
        CCLabelAtlas* pFollowerLevel = CCLabelAtlas::labelWithString("1", "RoundNum-hd.png", 10, 15, '.');
        pFollowerLevel->setPosition(ccp(-94.0f, 64.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pFollowerLevel, 1, FRIENDFOLLOWER_VALUE_ID);
        
        // 好友等级
        CCLabelAtlas* pFriendLevel = CCLabelAtlas::labelWithString("1", "UINum-hd.png", 8, 14, '.');
        pFriendLevel->setPosition(ccp(25.0f, 82.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pFriendLevel, 1, FRIENDFOLLOWER_LEVEL_ID);
        
        // 编辑背景
        CCSprite* pEditBack = CCSprite::spriteWithSpriteFrameName("remove_friend_back.png");
        pEditBack->setPosition(ccp(0.0f, -20.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pEditBack, 1);
        
        // 提示文本
        CCLabelTTF* pHint1 = CCLabelTTF::labelWithString("亲爱的玩家:", "黑体", 15);
        pHint1->setAnchorPoint(ccp(0.0f, 0.0f));
        pHint1->setPosition(ccp(-120.0f, 30.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pHint1, 1);
        
        CCLabelTTF* pHint2 = CCLabelTTF::labelWithString("你和对方分别获得10点友情点", "黑体", 15);
        pHint2->setAnchorPoint(ccp(0.0f, 0.0f));
        pHint2->setPosition(ccp(-100.0f, 0.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pHint2, 1, LABEL_FRIEND_VALUE_HINT);
        
        CCLabelTTF* pHint3 = CCLabelTTF::labelWithString("是否要加对方为好友？", "黑体", 15);
        pHint3->setAnchorPoint(ccp(0.0f, 0.0f));
        pHint3->setPosition(ccp(-120.0f, -20.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pHint3, 1, LABEL_ADDFRIEND_HINT);
        
        // 初始化确认按钮
        CCSprite* pConfirmNormal = CCSprite::spriteWithSpriteFrameName("button_liveok_normal.png");
        if (pConfirmNormal != NULL)
        {
            pConfirmNormal->setPosition(ccp(0.0f, -70.0f));
            m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pConfirmNormal, 1, BUTTON_CONFIRM_NORMAL_ID);
        }
        CCSprite* pConfirmSelect = CCSprite::spriteWithSpriteFrameName("button_liveok_select.png");
        if (pConfirmSelect != NULL)
        {
            pConfirmSelect->setPosition(ccp(0.0f, -70.0f));
            pConfirmSelect->setIsVisible(false);
            m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pConfirmSelect, 1, BUTTON_CONFIRM_SELECT_ID);
        }
        
        // 拒绝按钮 － 普通
        CCSprite* pRefuseNormal = CCSprite::spriteWithSpriteFrameName("button_red_normal.png");
        pRefuseNormal->setPosition(ccp(-80.0f, -70.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pRefuseNormal, 1, BUTTON_REFUSE_NORMAL_ID);
        
        // 拒绝按钮 - 选择
        CCSprite* pRefuseSelect = CCSprite::spriteWithSpriteFrameName("button_red_select.png");
        pRefuseSelect->setPosition(ccp(-80.0f, -70.0f));
        pRefuseSelect->setIsVisible(false);
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pRefuseSelect, 1, BUTTON_REFUSE_SELECT_ID);
        
        // 加为好友 - 普通
        CCSprite* pAddNormal = CCSprite::spriteWithSpriteFrameName("button_yellow_normal.png");
        pAddNormal->setPosition(ccp(80.0f, -70.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pAddNormal, 1, BUTTON_ADD_NORMAL_ID);
        
        // 加为好友 - 选择
        CCSprite* pAddSelect = CCSprite::spriteWithSpriteFrameName("button_yellow_select.png");
        pAddSelect->setPosition(ccp(80.0f, -70.0f));
        pAddSelect->setIsVisible(false);
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pAddSelect, 1, BUTTON_ADD_SELECT_ID);
        
        // 文本 : 拒绝
        CCSprite* pRefuseMark = CCSprite::spriteWithSpriteFrameName("label_refuse.png");
        pRefuseMark->setPosition(ccp(-80.0f, -70.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pRefuseMark, 1, BUTTON_LABEL_REFUSE_ID);
        
        // 文本 : 加为好友
        CCSprite* pAddMark = CCSprite::spriteWithSpriteFrameName("label_addfriend.png");
        pAddMark->setPosition(ccp(80.0f, -70.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pAddMark, 1, BUTTON_LABEL_ADD_ID);
        
        // 关闭 - 普通
        CCSprite* pCloseNormal = CCSprite::spriteWithSpriteFrameName("return_button_normal.png");
        pCloseNormal->setPosition(ccp(0.0f, -130.0f));
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pCloseNormal, 1, BUTTON_CLOSE_NORMAL_ID);
        
        // 关闭 - 选择
        CCSprite* pCloseSelect = CCSprite::spriteWithSpriteFrameName("return_button_select.png");
        pCloseSelect->setPosition(ccp(0.0f, -130.0f));
        pCloseSelect->setIsVisible(false);
        m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pCloseSelect, 1, BUTTON_CLOSE_SELECT_ID);
    }
    
    // 注册事件消息
    CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this, 0, true);
    
    // 设置对话框状态
    SetMessageState(MESSAGE_LIVE);
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_BattleMessage类退出函数
/************************************************************************/
void Layer_BattleMessage::onExit()
{
    CCLayer::onExit();
    
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_BattleMessage类销毁函数
/************************************************************************/
void Layer_BattleMessage::Destroy()
{
    // 移除事件处理
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
    // 移出四个对话框
    
    for (int i = 0; i < MESSAGE_NUM; i++)
        removeChild(m_pFrameLayer[i], true);
}

/************************************************************************/
#pragma mark - Layer_BattleMessage类事件函数 : 点击
/************************************************************************/
bool Layer_BattleMessage::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    CCPoint point(0.0f, 0.0f);          // 鼠标点击位置
    CCPoint pos(0.0f, 0.0f);            // 控件位置
    CCSize size(0.0f, 0.0f);            // 控件尺寸
    
    // 复活对话框
    if (m_pFrameLayer[MESSAGE_LIVE]->getIsVisible() == true)
    {
        // 获得点击位置
        point = m_pFrameLayer[MESSAGE_LIVE]->convertTouchToNodeSpace(pTouch);
        
        // 复活按钮
        CCSprite* pLiveNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_LIVE]->getChildByTag(BUTTON_LIVE_NORMAL_ID));
        CCSprite* pLiveSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_LIVE]->getChildByTag(BUTTON_LIVE_SELECT_ID));
        
        pos = pLiveNormal->getPosition();
        size = pLiveNormal->getContentSize();
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
        {
            // 改变状态
            pLiveNormal->setIsVisible(false);
            pLiveSelect->setIsVisible(true);
            
            return true;
        }
        
        // 放弃按钮
        CCSprite* pGiveupNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_LIVE]->getChildByTag(BUTTON_GIVEUP_NORMAL_ID));
        CCSprite* pGiveupSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_LIVE]->getChildByTag(BUTTON_GIVEUP_SELECT_ID));
        
        pos = pGiveupNormal->getPosition();
        size = pGiveupNormal->getContentSize();
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
        {
            // 改变状态
            pGiveupNormal->setIsVisible(false);
            pGiveupSelect->setIsVisible(true);
            
            return true;
        }
    }
    
    // 放弃对话框
    if (m_pFrameLayer[MESSAGE_GIVEUP]->getIsVisible() == true)
    {
        
    }
    
    // 网络不稳定
    if (m_pFrameLayer[MESSAGE_NETFAILURE]->getIsVisible() == true)
    {
        
    }
    
    // 丢失提示
    if (m_pFrameLayer[MESSAGE_LOST]->getIsVisible() == true)
    {
        
    }
    
    // 加好友
    if (m_pFrameLayer[MESSAGE_ADDFRIEND]->getIsVisible() == true)
    {
        // 获得点击坐标
        point = m_pFrameLayer[MESSAGE_ADDFRIEND]->convertTouchToNodeSpace(pTouch);
        
        // 加入按钮
        CCSprite* pAddNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_ADD_NORMAL_ID));
        if (pAddNormal->getIsVisible() == true)
        {
            CCSprite* pAddSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_ADD_SELECT_ID));
            
            pos = pAddNormal->getPosition();
            size = pAddNormal->getContentSize();
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                // 改变状态
                pAddNormal->setIsVisible(false);
                pAddSelect->setIsVisible(true);
                
                return true;
            }
        }
        
        // 拒绝按钮
        CCSprite* pRefuseNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_REFUSE_NORMAL_ID));
        if (pRefuseNormal->getIsVisible() == true)
        {
            CCSprite* pRefuseSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_REFUSE_SELECT_ID));
            
            pos = pRefuseNormal->getPosition();
            size = pRefuseNormal->getContentSize();
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                // 改变状态
                pRefuseNormal->setIsVisible(false);
                pRefuseSelect->setIsVisible(true);
                
                return true;
            }
        }
        
        // 关闭按钮
        CCSprite* pCloseNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_CLOSE_NORMAL_ID));
        if (pCloseNormal->getIsVisible() == true)
        {
            CCSprite* pCloseSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_CLOSE_SELECT_ID));
            
            pos = pCloseNormal->getPosition();
            size = pCloseNormal->getContentSize();
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                // 改变状态
                pCloseNormal->setIsVisible(false);
                pCloseSelect->setIsVisible(true);
                
                return true;
            }
        }
        
        // 确认按钮
        CCSprite* pConfirmNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_CONFIRM_NORMAL_ID));
        if (pConfirmNormal->getIsVisible() == true)
        {
            CCSprite* pConfirmSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_CONFIRM_SELECT_ID));
            
            pos = pConfirmNormal->getPosition();
            size = pConfirmNormal->getContentSize();
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                // 改变状态
                pConfirmNormal->setIsVisible(false);
                pConfirmSelect->setIsVisible(true);
                
                return true;
            }
        }
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_BattleMessage类事件函数 : 弹起
/************************************************************************/
void Layer_BattleMessage::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    CCPoint point(0.0f, 0.0f);
    CCPoint pos(0.0f, 0.0f);
    CCSize size(0.0f, 0.0f);
    
    // 复活对话框
    if (m_pFrameLayer[MESSAGE_LIVE]->getIsVisible() == true)
    {
        // 获得点击坐标
        point = m_pFrameLayer[MESSAGE_LIVE]->convertTouchToNodeSpace(pTouch);
        
        // 复活按钮
        CCSprite* pLiveNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_LIVE]->getChildByTag(BUTTON_LIVE_NORMAL_ID));
        CCSprite* pLiveSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_LIVE]->getChildByTag(BUTTON_LIVE_SELECT_ID));
        
        pos = pLiveNormal->getPosition();
        size = pLiveNormal->getContentSize();
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
        {
            //发送复活请求
            ServerDataManage::ShareInstance() -> RequestRelive();
            return;
        }
        
        // 改变状态
        pLiveNormal->setIsVisible(true);
        pLiveSelect->setIsVisible(false);
        
        // 放弃按钮
        CCSprite* pGiveupNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_LIVE]->getChildByTag(BUTTON_GIVEUP_NORMAL_ID));
        CCSprite* pGiveupSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_LIVE]->getChildByTag(BUTTON_GIVEUP_SELECT_ID));
        
        pos = pGiveupNormal->getPosition();
        size = pGiveupNormal->getContentSize();
        if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
            point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
        {
            this -> removeFromParentAndCleanup(true);
            Layer_Enemy::ShareInstance() -> OutGame();
            return;
        }
        
        // 改变状态
        pGiveupNormal->setIsVisible(true);
        pGiveupSelect->setIsVisible(false);
    }
    
    // 放弃对话框
    if (m_pFrameLayer[MESSAGE_GIVEUP]->getIsVisible() == true)
    {
        
    }
    
    // 网络不稳定
    if (m_pFrameLayer[MESSAGE_NETFAILURE]->getIsVisible() == true)
    {
        
    }
    
    // 丢失提示
    if (m_pFrameLayer[MESSAGE_LOST]->getIsVisible() == true)
    {
        
    }
    
    // 加好友
    if (m_pFrameLayer[MESSAGE_ADDFRIEND]->getIsVisible() == true)
    {
        // 获得点击坐标
        point = m_pFrameLayer[MESSAGE_ADDFRIEND]->convertTouchToNodeSpace(pTouch);
        
        // 加入按钮
        CCSprite* pAddSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_ADD_SELECT_ID));
        if (pAddSelect->getIsVisible() == true)
        {
            CCSprite* pAddNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_ADD_NORMAL_ID));
            
            // 改变状态
            pAddNormal->setIsVisible(true);
            pAddSelect->setIsVisible(false);
            
            pos = pAddNormal->getPosition();
            size = pAddNormal->getContentSize();
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                if(Scene_Battle::m_MissionFriendFollowerID < 10000)
                {
                    ShowMessage(BType_Firm, "好友ID错误，无法加，请拒绝");
                }else
                {
                    //加对方为好友
                    ServerDataManage::ShareInstance()->RequestAddFriendByUserID(Scene_Battle::m_MissionFriendFollowerID);
                    this -> removeFromParentAndCleanup(true);
                    //退出战斗中
                    GameController::ShareInstance() -> ReadyToGameScene(GameState_BackToGame);
                }
                return;
            }
        }
        
        // 拒绝按钮
        CCSprite* pRefuseSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_REFUSE_SELECT_ID));
        if (pRefuseSelect->getIsVisible() == true)
        {
            CCSprite* pRefuseNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_REFUSE_NORMAL_ID));
            
            // 改变状态
            pRefuseNormal->setIsVisible(true);
            pRefuseSelect->setIsVisible(false);
            
            pos = pRefuseNormal->getPosition();
            size = pRefuseNormal->getContentSize();
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                this -> removeFromParentAndCleanup(true);
                //退出战斗中
                GameController::ShareInstance() -> ReadyToGameScene(GameState_BackToGame);
                return;
            }
        }
        
        // 关闭按钮
        CCSprite* pCloseSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_CLOSE_SELECT_ID));
        if (pCloseSelect->getIsVisible() == true)
        {
            CCSprite* pCloseNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_CLOSE_NORMAL_ID));
            
            // 改变状态
            pCloseNormal->setIsVisible(true);
            pCloseSelect->setIsVisible(false);
            
            pos = pCloseNormal->getPosition();
            size = pCloseNormal->getContentSize();
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                setIsVisible(false);
            }
        }
        
        // 确认按钮
        CCSprite* pConfirmSelect = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_CONFIRM_SELECT_ID));
        if (pConfirmSelect->getIsVisible() == true)
        {
            CCSprite* pConfirmNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_CONFIRM_NORMAL_ID));
            
            // 改变状态
            pConfirmNormal->setIsVisible(true);
            pConfirmSelect->setIsVisible(false);
            
            pos = pConfirmNormal->getPosition();
            size = pConfirmNormal->getContentSize();
            if (point.x > pos.x - size.width / 2.0f && point.x < pos.x + size.width / 2.0f &&
                point.y > pos.y - size.height / 2.0f && point.y < pos.y + size.height / 2.0f)
            {
                removeFromParentAndCleanup(true);
                GameController::ShareInstance()->ReadyToGameScene(GameState_BackToGame);
            }
        }
    }
}

/************************************************************************/
#pragma mark - Layer_BattleMessage类设置界面状态
/************************************************************************/
void Layer_BattleMessage::SetMessageState(MESSAGE_STATE eState)
{
    if (eState < 0 || eState >= MESSAGE_NUM)
        return;
    
    // 保存状态
    m_eState = eState;
    
    // 打开相应的界面
    for (int i = 0; i < MESSAGE_NUM; i++)
    {
        if (i == eState)
            m_pFrameLayer[i]->setIsVisible(true);
        else
            m_pFrameLayer[i]->setIsVisible(false);
    }
    
    // 打开背景
    m_pFrameBack->setIsVisible(true);
    
    // 特殊界面进行特殊处理
    switch (m_eState) {
        case MESSAGE_LIVE:
        {
            // 刷新钻石数量
            CCLabelAtlas* pCurDiamond = dynamic_cast<CCLabelAtlas*>(m_pFrameLayer[MESSAGE_LIVE]->getChildByTag(LIVE_CUR_DIAMOND));
            if (pCurDiamond != NULL)
            {
                char szTemp[32] = {0};
                sprintf(szTemp, "%d", PlayerDataManage::m_PlayerRMB);
                pCurDiamond->setString(szTemp);
            }
        }
            break;
        case MESSAGE_ADDFRIEND:         // 添加好友
        {
            // 关闭背景
            m_pFrameBack->setIsVisible(false);
            
            // 查找好友信息
            map<int, list<FriendInfo> > mapList = ServerDataManage::ShareInstance()->m_FriendInfoList;
            map<int, list<FriendInfo> >::iterator iter;
            
            int index = 0;
            for (iter = mapList.begin(); iter != mapList.end(); iter++)
            {
                list<FriendInfo> infoList = iter->second;
                list<FriendInfo>::iterator infoIter;
                for (infoIter = infoList.begin(); infoIter != infoList.end(); infoIter++)
                {
                    if (infoIter->Fri_UserID == Scene_Battle::m_MissionFriendFollowerID)
                    {
                        // 取得小弟新数据
                        const Follower_Data* pData = SystemDataManage::ShareInstance()->GetData_ForFollower(infoIter->Fri_FollowerID);
                        if (pData != NULL)
                        {
                            // 更新icon
                            CCSprite* pIcon = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(FRIENDFOLLOWER_ICON_ID));
                            if (pIcon != NULL)
                            {
                                // 记录icon位置
                                CCPoint pos = pIcon->getPosition();
                                
                                // 移出旧icon
                                m_pFrameLayer[MESSAGE_ADDFRIEND]->removeChild(pIcon, true);
                                
                                // 生成xinicon
                                pIcon = CCSprite::spriteWithSpriteFrameName(pData->Follower_IconName);
                                if (pIcon != NULL)
                                {
                                    pIcon->setPosition(pos);
                                    m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pIcon, 0, FRIENDFOLLOWER_ICON_ID);
                                }
                            }
                            
                            // 更新frame
                            CCSprite* pFrame = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(FRIENDFOLLOWER_FRAME_ID));
                            if (pFrame != NULL)
                            {
                                // 记录frame位置
                                CCPoint pos = pFrame->getPosition();
                                
                                // 移出旧frame
                                m_pFrameLayer[MESSAGE_ADDFRIEND]->removeChild(pFrame, true);
                                
                                // 生成新frame
                                char szTemp[32] = {0};
                                sprintf(szTemp, "info_frame_%d.png", pData->Follower_Profession);
                                pFrame = CCSprite::spriteWithSpriteFrameName(szTemp);
                                if (pFrame != NULL)
                                {
                                    pFrame->setPosition(pos);
                                    m_pFrameLayer[MESSAGE_ADDFRIEND]->addChild(pFrame, 0, FRIENDFOLLOWER_FRAME_ID);
                                }
                            }
                            
                            // 更新等级
                            CCLabelAtlas* pLevel = dynamic_cast<CCLabelAtlas*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(FRIENDFOLLOWER_LEVEL_ID));
                            if (pLevel != NULL)
                            {
                                char szTemp[32] = {0};
                                sprintf(szTemp, "%d", infoIter->Fri_UserLevel);
                                pLevel->setString(szTemp);
                            }
                            
                            // 更新友情点
                            CCLabelAtlas* pValue = dynamic_cast<CCLabelAtlas*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(FRIENDFOLLOWER_VALUE_ID));
                            if (pValue != NULL)
                            {
                                char szTemp[32] = {0};
                                sprintf(szTemp, "%d", infoIter->Fri_FollowerLevel);
                                pValue->setString(szTemp);
                            }
                            
                            // 更新好友姓名
                            CCLabelTTF* pPlayerName = dynamic_cast<CCLabelTTF*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(FRIENDFOLLOWER_NAME));
                            if (pPlayerName != NULL)
                                pPlayerName->setString(infoIter->Fri_UserName);
                            
                            // 是否为好友，进行不同的处理
                            if (index == 0)
                            {
                                // 加入按钮
                                CCSprite* pAddNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_ADD_NORMAL_ID));
                                if (pAddNormal != NULL)
                                    pAddNormal->setIsVisible(false);
                                // 拒绝按钮
                                CCSprite* pRefuseNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_REFUSE_NORMAL_ID));
                                if (pRefuseNormal != NULL)
                                    pRefuseNormal->setIsVisible(false);
                                // 确认按钮
                                CCSprite* pConfirmNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_CONFIRM_NORMAL_ID));
                                if (pConfirmNormal != NULL)
                                    pConfirmNormal->setIsVisible(true);
                                
                                // 刷新文本2
                                CCLabelTTF* pHint2 = dynamic_cast<CCLabelTTF*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(LABEL_FRIEND_VALUE_HINT));
                                if (pHint2 != NULL)
                                {
                                    char szTemp[256] = {0};
                                    sprintf(szTemp, "你和对方分别获得10点友情点");
                                    pHint2->setString(szTemp);
                                }
                                
                                // 刷新文本3
                                CCLabelTTF* pHint3 = dynamic_cast<CCLabelTTF*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(LABEL_ADDFRIEND_HINT));
                                if (pHint3 != NULL)
                                    pHint3->setIsVisible(false);
                                
                                // 文本 : 拒绝
                                CCSprite* pRefuseMark = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_LABEL_REFUSE_ID));
                                if (pRefuseMark != NULL)
                                    pRefuseMark->setIsVisible(false);
                                
                                // 文本 : 加为好友
                                CCSprite* pAddMark = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_LABEL_ADD_ID));
                                if (pAddMark != NULL)
                                    pAddMark->setIsVisible(false);
                            }
                            else
                            {
                                // 加入按钮
                                CCSprite* pAddNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_ADD_NORMAL_ID));
                                if (pAddNormal != NULL)
                                    pAddNormal->setIsVisible(true);
                                // 拒绝按钮
                                CCSprite* pRefuseNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_REFUSE_NORMAL_ID));
                                if (pRefuseNormal != NULL)
                                    pRefuseNormal->setIsVisible(true);
                                // 确认按钮
                                CCSprite* pConfirmNormal = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_CONFIRM_NORMAL_ID));
                                if (pConfirmNormal != NULL)
                                    pConfirmNormal->setIsVisible(false);
                                
                                // 刷新文本2
                                CCLabelTTF* pHint2 = dynamic_cast<CCLabelTTF*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(LABEL_FRIEND_VALUE_HINT));
                                if (pHint2 != NULL)
                                {
                                    char szTemp[256] = {0};
                                    sprintf(szTemp, "你和对方分别获得5点友情点");
                                    pHint2->setString(szTemp);
                                }
                                // 刷新文本3
                                CCLabelTTF* pHint3 = dynamic_cast<CCLabelTTF*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(LABEL_ADDFRIEND_HINT));
                                if (pHint3 != NULL)
                                    pHint3->setIsVisible(true);
                                
                                // 文本 : 拒绝
                                CCSprite* pRefuseMark = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_LABEL_REFUSE_ID));
                                if (pRefuseMark != NULL)
                                    pRefuseMark->setIsVisible(true);
                                
                                // 文本 : 加为好友
                                CCSprite* pAddMark = dynamic_cast<CCSprite*>(m_pFrameLayer[MESSAGE_ADDFRIEND]->getChildByTag(BUTTON_LABEL_ADD_ID));
                                if (pAddMark != NULL)
                                    pAddMark->setIsVisible(true);
                            }
                            
                            break;
                        }
                    }
                }
                
                ++index;
            }
        }
            break;
        default:
            break;
    }
}