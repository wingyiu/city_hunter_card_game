//
//  Layer_MapShake.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-5-23.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_MapShake.h"

/************************************************************************/
#pragma mark - Layer_MapShake类静态函数 : 创建震动函数
/************************************************************************/
Layer_MapShake* Layer_MapShake::actionWithDuration(ccTime fDuration, CCPoint oriPos, SHAKE_TYPE eType)
{
    Layer_MapShake* pShake = new Layer_MapShake();
    if (pShake != NULL)
    {
        if (pShake->initWithDuration(fDuration, oriPos, eType) == true)
        {
            pShake->autorelease();
            return pShake;
        }
    }
    
    delete pShake;
    pShake = NULL;
    
    return NULL;
}

/************************************************************************/
#pragma mark - Layer_MapShake类初始化震动函数
/************************************************************************/
bool Layer_MapShake::initWithDuration(ccTime fDuration, CCPoint oriPos, SHAKE_TYPE eType)
{
    if (CCActionInterval::initWithDuration(fDuration) == true)
    {
        m_oriPos = oriPos;
        m_shakePos = oriPos;
        
        m_eType = eType;
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_MapShake类逻辑更新函数
/************************************************************************/
void Layer_MapShake::update(ccTime time)
{
    switch (m_eType) {
        case SHAKE_RAND:
            ShakeWithRand();
            break;
        case SHAKE_UPDOWN:
            ShakeWithUpDown();
            break;
        case SHAKE_UPDOWN1:
            ShakeWithUpDown1();
            break;
        case SHAKE_LIFTRIGHT:
            ShakeWithLeftRight();
            break;
        default:
            break;
    }
}

/************************************************************************/
#pragma mark - Layer_MapShake类构造函数
/************************************************************************/
Layer_MapShake::Layer_MapShake()
{
    m_shakePos.x = m_shakePos.y = 0.0f;
    m_oriPos.x = m_oriPos.y = 0.0f;
    
    m_iShakeSpeed = 3;
}

/************************************************************************/
#pragma mark - Layer_MapShake类析构函数
/************************************************************************/
Layer_MapShake::~Layer_MapShake()
{
    
}

/************************************************************************/
#pragma mark - Layer_MapShake类随机抖动函数
/************************************************************************/
void Layer_MapShake::ShakeWithRand()
{
    // 进行震屏幕操作
    if (m_iShakeSpeed % 3 == 0)
    {
        int x = rand() % 12 - 6;
        int y = rand() % 10 - 5;
        
        m_shakePos = m_oriPos;
        
        m_shakePos.x += x;
        m_shakePos.y += y;
        
        m_pTarget->setPosition(m_shakePos);
    }
    
    // 结束则还原默认位置
    if (isDone())
        m_pTarget->setPosition(m_oriPos);
}

/************************************************************************/
#pragma mark - Layer_MapShake类上下抖动函数
/************************************************************************/
void Layer_MapShake::ShakeWithUpDown()
{
    // 移动方向
    static int way = 1;
    
    // 超过特定距离，方向反向
    if (m_shakePos.y > m_oriPos.y + 12)
        way = -1;
    else if (m_shakePos.y < m_oriPos.y - 12)
        way = 1;
    
    // 进行坐标变换
    m_shakePos.y += way * 3;
    m_pTarget->setPosition(m_shakePos);
    
    // 结束还原默认目标
    if (isDone())
        m_pTarget->setPosition(m_oriPos);
}

/************************************************************************/
#pragma mark - Layer_MapShake类上下抖动1函数
/************************************************************************/
void Layer_MapShake::ShakeWithUpDown1()
{
    // 移动方向
    static int way = 1;
    
    // 超过特定距离，方向反向
    if (m_shakePos.y > m_oriPos.y + 3)
        way = -1;
    else if (m_shakePos.y < m_oriPos.y - 3)
        way = 1;
    
    // 进行坐标变换
    m_shakePos.y += way;
    m_pTarget->setPosition(m_shakePos);
    
    // 结束还原默认目标
    if (isDone())
        m_pTarget->setPosition(m_oriPos);
}

/************************************************************************/
#pragma mark - Layer_MapShake类左右移动函数
/************************************************************************/
void Layer_MapShake::ShakeWithLeftRight()
{
    // 移动方向
    static int way = 1;
    
    // 超过特定距离，方向反向
    if (m_shakePos.x > m_oriPos.x + 6)
        way = -1;
    else if (m_shakePos.x < m_oriPos.x - 6)
        way = 1;
    
    // 进行坐标变换
    m_shakePos.x += way * 3;
    m_pTarget->setPosition(m_shakePos);
    
    // 结束还原默认目标
    if (isDone())
        m_pTarget->setPosition(m_oriPos);
}