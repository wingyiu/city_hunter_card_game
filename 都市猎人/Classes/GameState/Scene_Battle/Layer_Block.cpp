//
//  Layer_Block.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-6-1.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_Block.h"
#include "Layer_Follower.h"
#include "Layer_Enemy.h"
#include "SimpleAudioEngine.h"
#include "KNUIFunction.h"

// 方块阵列数组
BLOCK_INFO m_arrBlockItem[VETICAL_BLOCK_NUM][HORIZON_BLOCK_NUM];

Layer_Block * Layer_Block::m_Instance = NULL;

// 方块文件名
const char* g_BlockFilename[BLOCK_NONE] = {"block_1.png", "block_2.png", "block_3.png", "block_4.png"};
// 立方体文件名
const char* g_CubeFilename[BLOCK_NONE] = {"cube_11.png", "cube_22.png", "cube_33.png", "cube_44.png"};
// 动画文件名
const char* g_AnimFilename[BLOCK_NONE] = {"star_yellow_", "star_blue_", "star_purple_", "star_red_"};

// 战斗步骤1,方块布局
extern int g_blockStep1[5][6];

// 新手引导 : 每个步骤前加这个标志，方便查找

/************************************************************************/
#pragma mark - Layer_Block类获得block实例对象
/************************************************************************/
Layer_Block * Layer_Block::ShareInstance()
{
    if(m_Instance)
        return m_Instance;
    return NULL;
}

/************************************************************************/
#pragma mark - Layer_Block类构造函数
/************************************************************************/
Layer_Block::Layer_Block()
{
    // 为方块对象变量赋初值
    m_pBatchNode = NULL;
    
    m_pLight = NULL;
    m_pLine1 = NULL;
    m_pLine2 = NULL;
    m_pLine3 = NULL;
    m_pLine4 = NULL;
    
    m_pMaskLayer = NULL;
    
    m_pGuide = NULL;
    
    // 为方框属性变量赋初值
    //memset(&m_arrBlockItem, 0, sizeof(BLOCK_INFO) * HORIZON_BLOCK_NUM * VETICAL_BLOCK_NUM);
    
    m_iSelectedRow = m_iSelectedCol = 0;
    m_iMoveBlockNum = 0;
    m_iComboNum = 0;
    m_iMaxComboNum = 0;
    
    m_fDragDistance = 0.0f;
    m_fDistanceLimit = 0.0f;
    
    m_fCurrentSystemScale = 1.0f;
    
    m_bIsBeginningClear = true;
    m_bIsClearEvent = false;
    m_bIsLineDraw = false;
    m_bIsBlockClear = false;
    m_bIsOperationEnable = true;
    
    m_eMoveDirection = Direction_None;
    m_eLightState = LIGHT_LIFTMOVE;
    
    m_vecMoveList.clear();
    m_vecTempList.clear();
    
    m_setClearBlock.clear();
}

/************************************************************************/
#pragma mark - Layer_Block类析构函数
/************************************************************************/
Layer_Block::~Layer_Block()
{
    printf("Layer_Block:: 释放完成\n");
    
    Destroy();
}

/************************************************************************/
#pragma mark - Layer_Block类初始化函数
/************************************************************************/
bool Layer_Block::init()
{
    // 初始化基类
    if (CCLayer::init() == false)
        return false;
 
    this -> setIsTouchEnabled(true);
    CCTouchDispatcher::sharedDispatcher() -> addTargetedDelegate(this, 1, true);
    
    
    /////////////////////////////////////////////////
    // 获得缩放系数
    m_fCurrentSystemScale = CC_CONTENT_SCALE_FACTOR();
    
    // 初始化精灵节点
    m_pBatchNode = CCSpriteBatchNode::batchNodeWithFile("anim1-hd.pvr.ccz");
    if (m_pBatchNode != NULL)
        addChild(m_pBatchNode, 1);
    
    // 初始化光阴
    m_pLight = CCSprite::spriteWithSpriteFrameName("light.png");
    if (m_pLight != NULL)
    {
        m_pLight->setPosition(ccp(160.0f, 130.0f));
        m_pLight->setRotation(-40.0f);
        
        m_pBatchNode->addChild(m_pLight, 1);
    }
    
    // 初始化线条精灵
    m_pLine1 = CCSprite::spriteWithSpriteFrameName("line.png");
    if (m_pLine1 != NULL)
    {
        m_pLine1->setIsVisible(false);
        m_pBatchNode->addChild(m_pLine1, 3);
    }
    m_pLine2 = CCSprite::spriteWithSpriteFrameName("line.png");
    if (m_pLine2 != NULL)
    {
        m_pLine2->setIsVisible(false);
        m_pBatchNode->addChild(m_pLine2, 3);
    }
    m_pLine3 = CCSprite::spriteWithSpriteFrameName("line.png");
    if (m_pLine3 != NULL)
    {
        m_pLine3->setIsVisible(false);
        m_pBatchNode->addChild(m_pLine3, 3);
    }
    m_pLine4 = CCSprite::spriteWithSpriteFrameName("line.png");
    if (m_pLine4 != NULL)
    {
        m_pLine4->setIsVisible(false);
        m_pBatchNode->addChild(m_pLine4, 3);
    }
    
    // 初始化遮罩层
    m_pMaskLayer = CCLayerColor::layerWithColor(ccc4(0, 0, 0, 100));
    if (m_pMaskLayer != NULL)
    {
        m_pMaskLayer->setIsVisible(true);
        m_pMaskLayer->setVertexZ(2.1f);
        addChild(m_pMaskLayer, 5);
    }
    
    // 新手引导 : 初始化新手引导
    if (PlayerDataManage::m_GuideBattleMark)
    {
        m_pGuide = Layer_GameGuide::node();
        if (m_pGuide != NULL)
        {
            m_pGuide -> setVertexZ(3.0f);
            addChild(m_pGuide, 5);
        }
    }
    
    // 设置随机种子
    srand(time(0));
    
    // 初始化方块信息
    int iRand = 0;
    CCPoint blockPos(0.0f, 0.0f);
    for (int i = 0; i < VETICAL_BLOCK_NUM; ++i)
    {
        for (int j = 0; j < HORIZON_BLOCK_NUM; ++j)
        {
            // 新手引导 : 使用预设方块
            if (PlayerDataManage::m_GuideBattleMark)
            {
                switch (m_pGuide->GetBattleGuideStep()) {
                    case 1:
                        iRand = g_blockStep1[i][j];
                        break;
                    default:
                        break;
                }
            }
            else
            {
                // 随机方块属性
                iRand = rand() % 4 + 1;
            }
            
            // 初始化方块
            m_arrBlockItem[i][j].pBlock = Sprite_Block::blockSpriteWithSpriteFrameName(g_BlockFilename[iRand - 1]);
            if (m_arrBlockItem[i][j].pBlock != NULL)
            {
                // 设置方块类型
                m_arrBlockItem[i][j].pBlock->SetBlockType(static_cast<BLOCK_TYPE>(iRand));
                
                // 设置方块位置
                blockPos.x = HORIZON_START_POS + j * HORIZON_BLOCK_DELTA;
                blockPos.y = VETICAL_START_POS - i * VETICAL_BLOCK_DELTA;
                m_arrBlockItem[i][j].pBlock->setPosition(blockPos);
                
                // 设置方块行和列
                m_arrBlockItem[i][j].pBlock->SetRowAndCol(i, j);
                
                // 添加到精灵节点
                addChild(m_arrBlockItem[i][j].pBlock, 2);
            }
            
            // 初始化立方体
            m_arrBlockItem[i][j].pCube = Sprite_Cube::cubeWithSpriteFrameName(g_CubeFilename[iRand - 1]);
            if (m_arrBlockItem[i][j].pCube != NULL)
            {
                m_arrBlockItem[i][j].pCube->setPosition(blockPos);
                addChild(m_arrBlockItem[i][j].pCube, 1);
            }
            
            // 初始化外框
            m_arrBlockItem[i][j].pFrame = Sprite_Frame::frameWithSpriteFrameName("black_frame.png");
            if (m_arrBlockItem[i][j].pFrame != NULL)
            {
                m_arrBlockItem[i][j].pFrame->setPosition(blockPos);
                addChild(m_arrBlockItem[i][j].pFrame);
            }
        }
    }
    
    // 消除所有能消除的方块
    while (ClearBlockAtBeginning());
    
    // 因为消除过了，但是没有开始战争，所以这里重置下
    m_bIsBlockClear = false;
    
    // 注册逻辑更新函数
    schedule(schedule_selector(Layer_Block::update));

    m_Instance = this;
    
    return true;
}

/************************************************************************/
#pragma mark - Layer_Block类退出函数
/************************************************************************/
void Layer_Block::onExit()
{
    CCLayer::onExit();
    
    Destroy();
}
/************************************************************************/
#pragma mark - Layer_Block类销毁函数
/************************************************************************/
void Layer_Block::Destroy()
{
    if (m_pBatchNode == NULL)
        return;
    
    // 销毁所有
    for (int i = 0; i < VETICAL_BLOCK_NUM; ++i)
    {
        for (int j = 0; j < HORIZON_BLOCK_NUM; ++j)
        {
            removeChild(m_arrBlockItem[i][j].pBlock, true);
            removeChild(m_arrBlockItem[i][j].pCube, true);
            removeChild(m_arrBlockItem[i][j].pFrame, true);
        }
    }
    
    // 移除精灵节点
    removeChild(m_pBatchNode, true);
    m_pBatchNode = NULL;
    
    // 移除光阴精灵
    removeChild(m_pLight, true);
    removeChild(m_pLine1, true);
    removeChild(m_pLine2, true);
    removeChild(m_pLine3, true);
    removeChild(m_pLine4, true);
    
    // 移除遮罩层
    removeChild(m_pMaskLayer, true);
    
    // 新手引导 : 移出新手引导
    if (PlayerDataManage::m_GuideBattleMark)
        removeChild(m_pGuide, true);
    
    // 移除点击事件
    CCTouchDispatcher::sharedDispatcher()->removeDelegate(this);
}

/************************************************************************/
#pragma mark - Layer_Block类设置可视范围
/************************************************************************/
void Layer_Block::SetVisibleRange(float fPosX, float fPosY, float fWidth, float fHeight)
{
    // 获得图层位置
    CCPoint pos = getPosition();
    
    // 设置可视范围结构体
    m_visibleRange.origin.x = pos.x + fPosX;
    m_visibleRange.origin.y = pos.y + fPosY;
    m_visibleRange.size.width = fWidth;
    m_visibleRange.size.height = fHeight;
}

/************************************************************************/
#pragma mark - Layer_Block类设置layeralpha值函数
/************************************************************************/
void Layer_Block::SetLayerAlpha(bool bIsOpenMask)
{
    m_pMaskLayer->setIsVisible(bIsOpenMask);
}

/************************************************************************/
#pragma mark - Layer_Block类结束方块函数
/************************************************************************/
void Layer_Block::EndBlock()
{
    // 关闭操作
    m_bIsOperationEnable = false;
    
    // 进行翻转
    int index = 0;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < VETICAL_BLOCK_NUM; j++)
        {
            m_arrBlockItem[j][i].pCube->stopAllActions();
            
            CCDelayTime* pTime = CCDelayTime::actionWithDuration(index * 0.3f);
            CCDelayTime* pTime1 = CCDelayTime::actionWithDuration(index * 0.3f);
            CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(0.3f);
            Action_BlockChange* pAction = Action_BlockChange::actionWithBlockInfo(1.0f, j, i, BLOCK_LEFT, true);
            if (pAction != NULL)
            {
                pFadeOut->setTag(FINAL_BLOCK_FADEOUT_ACTION_TAG);
                m_arrBlockItem[j][i].pBlock->runAction(CCSequence::actions(pTime1, pFadeOut, NULL));
                m_arrBlockItem[j][i].pCube->runAction(CCSequence::actions(pTime, pAction, NULL));
            }
        }
        
        ++index;
    }
    
    index = 0;
    for (int i = HORIZON_BLOCK_NUM - 1; i >= 3; i--)
    {
        for (int j = 0; j < VETICAL_BLOCK_NUM; j++)
        {
            m_arrBlockItem[j][i].pCube->stopAllActions();
            
            CCDelayTime* pTime = CCDelayTime::actionWithDuration(index * 0.3f);
            CCDelayTime* pTime1 = CCDelayTime::actionWithDuration(index * 0.3f);
            CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(0.3f);
            Action_BlockChange* pAction = Action_BlockChange::actionWithBlockInfo(1.0f, j, i, BLOCK_RIGHT, true);
            if (pAction != NULL)
            {
                pFadeOut->setTag(FINAL_BLOCK_FADEOUT_ACTION_TAG);
                m_arrBlockItem[j][i].pBlock->runAction(CCSequence::actions(pTime1, pFadeOut, NULL));
                m_arrBlockItem[j][i].pCube->runAction(CCSequence::actions(pTime, pAction, NULL));
            }
        }
        
        ++index;
    }
}

/************************************************************************/
#pragma mark - Layer_Block类重开方块函数
/************************************************************************/
void Layer_Block::RestartBlock()
{
    // 关闭操作
    m_bIsOperationEnable = true;
    
    // 进行翻转
    int index = 0;
    for (int i = 2; i >= 0; i--)
    {
        for (int j = 0; j < VETICAL_BLOCK_NUM; j++)
        {
            m_arrBlockItem[j][i].pCube->stopAllActions();
            
            CCDelayTime* pTime = CCDelayTime::actionWithDuration(index * 0.3f);
            CCDelayTime* pTime1 = CCDelayTime::actionWithDuration(index * 0.3f + 0.5f);
            CCFadeIn* pFadeIn = CCFadeIn::actionWithDuration(0.3f);
            Action_BlockChange* pAction = Action_BlockChange::actionWithBlockInfo(1.0f, j, i, BLOCK_RIGHT, false);
            if (pAction != NULL)
            {
                m_arrBlockItem[j][i].pBlock->runAction(CCSequence::actions(pTime1, pFadeIn, NULL));
                m_arrBlockItem[j][i].pCube->runAction(CCSequence::actions(pTime, pAction, NULL));
            }
        }
        
        ++index;
    }
    
    index = 0;
    for (int i = 3; i < HORIZON_BLOCK_NUM; i++)
    {
        for (int j = 0; j < VETICAL_BLOCK_NUM; j++)
        {
            m_arrBlockItem[j][i].pCube->stopAllActions();
            
            CCDelayTime* pTime = CCDelayTime::actionWithDuration(index * 0.3f);
            CCDelayTime* pTime1 = CCDelayTime::actionWithDuration(index * 0.3f + 0.5f);
            CCFadeIn* pFadeIn = CCFadeIn::actionWithDuration(0.3f);
            Action_BlockChange* pAction = Action_BlockChange::actionWithBlockInfo(1.0f, j, i, BLOCK_LEFT, false);
            if (pAction != NULL)
            {
                m_arrBlockItem[j][i].pBlock->runAction(CCSequence::actions(pTime1, pFadeIn, NULL));
                m_arrBlockItem[j][i].pCube->runAction(CCSequence::actions(pTime, pAction, NULL));
            }
        }
        
        ++index;
    }
}

/************************************************************************/
#pragma mark - Layer_Block类事件函数 : 点击
/************************************************************************/
bool Layer_Block::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
    // 是否可操作状态
    if (!m_bIsOperationEnable)
        return false;
    
    // 如果在处理消除事件，不能操作
    if (m_bIsClearEvent)
        return false;
    
    // 取得鼠标点击位置
    CCPoint point = convertTouchToNodeSpace(pTouch);
    // 如果不在控件范围内，则退出，响应其他控件事件
    if (point.x < m_visibleRange.origin.x || point.x > m_visibleRange.origin.x + m_visibleRange.size.width ||
        point.y < m_visibleRange.origin.y || point.y > m_visibleRange.origin.y + m_visibleRange.size.height)
        return false;
    
    // 新手引导 : 限制点击区域
    if (PlayerDataManage::m_GuideBattleMark)
    {
        if (m_pGuide->LimitBlockTouchRange(point) == false)
            return false;
    }
    
    // 判断那个方块被点击
    CCPoint blockPos(0.0f, 0.0f);
    CCSize blockSize(0.0f, 0.0f);
    for (int i = 0; i < VETICAL_BLOCK_NUM; ++i)
    {
        for (int j = 0; j < HORIZON_BLOCK_NUM; ++j)
        {
            // 获得精灵位置
            blockPos = m_arrBlockItem[i][j].pBlock->getPosition();
            
            // 获得精灵尺寸
            blockSize = m_arrBlockItem[i][j].pBlock->getContentSize();
            
            // 是否被点击
            if (point.x > blockPos.x - blockSize.width / 2.0f && point.x < blockPos.x + blockSize.width / 2.0f &&
                point.y > blockPos.y - blockSize.height / 2.0f && point.y < blockPos.y + blockSize.height / 2.0f)
            {
                m_iSelectedRow = i;
                m_iSelectedCol = j;
                
                m_pLine1->setIsVisible(true);
                m_pLine2->setIsVisible(true);
                m_pLine3->setIsVisible(true);
                m_pLine4->setIsVisible(true);
                
                // row
                CCPoint pos = m_arrBlockItem[i][j].pBlock->getPosition();
                m_pLine1->setRotation(0.0f); 
                m_pLine2->setRotation(0.0f);
                
                m_pLine1->setPosition(ccp(160.0f, pos.y - 53.0f / 2.0f));
                m_pLine2->setPosition(ccp(160.0f, pos.y + 53.0f / 2.0f));
                
                // col
                m_pLine3->setRotation(90.0f); 
                m_pLine4->setRotation(90.0f);
                
                m_pLine3->setPosition(ccp(pos.x - 53.0f / 2.0f, 120.0f));
                m_pLine4->setPosition(ccp(pos.x + 53.0f / 2.0f, 120.0f));
                
                return true;
            }
        }
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_Block类事件函数 : 滑动
/************************************************************************/
void Layer_Block::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
    // 如果在处理消除事件，不能操作
    if (m_bIsClearEvent)
        return;
    
    // 点击坐标
    CCPoint curPoint = convertTouchToNodeSpace(pTouch);
    // 如果不在控件范围内，则退出，响应其他控件事件
    if (curPoint.x < m_visibleRange.origin.x || curPoint.x > m_visibleRange.origin.x + m_visibleRange.size.width ||
        curPoint.y < m_visibleRange.origin.y || curPoint.y > m_visibleRange.origin.y + m_visibleRange.size.height)
        return;
    
    // 上次点击坐标
    CCPoint lastPoint = pTouch->previousLocationInView(pTouch->view());
    lastPoint = CCDirector::sharedDirector()->convertToGL(lastPoint);
    lastPoint = convertToNodeSpace(lastPoint);
    
    // 偏移坐标
    CCPoint offsetPoint = ccpSub(curPoint, lastPoint);
    CCPoint temp = offsetPoint;
    
    // 新手引导只能让玩家向右移动一格
    if (PlayerDataManage::m_GuideBattleMark)
    {
        if (Layer_GameGuide::GetSingle()->GetBattleGuideStep() >= 3)
        {
            if (temp.y < 0.0f)
                return;
            
            if (temp.y < 0.0f)
                temp.y = -temp.y;
            
            m_fDistanceLimit += temp.y;
            if (m_fDistanceLimit > 53.0f)
                return;
        }
        else
        {
            if (temp.x < 0.0f)
                return;
            
            if (temp.x < 0.0f)
                temp.x = -temp.x;
            
            m_fDistanceLimit += temp.x;
            if (m_fDistanceLimit > 53.0f)
                return;
        }
    }
    
    // 修正移动值
    if (temp.x < 0.0f)
        temp.x = -temp.x;
    if (temp.y < 0.0f)
        temp.y = -temp.y;
        
    
    // 进行拖动
    if (m_eMoveDirection == Direction_None)                     // 判断是否横向还是竖向
    {
        // 判断拖动方向
        m_eMoveDirection = temp.x > temp.y ? Direction_Horizontal : Direction_Vetical;
        
        // 新手引导，限制其拖动
        if (PlayerDataManage::m_GuideBattleMark)
        {
            if (Layer_GameGuide::GetSingle()->GetBattleGuideStep() >= 3)
                m_eMoveDirection = Direction_Vetical;
            else
                m_eMoveDirection = Direction_Horizontal;
        }
        
        // 记录整排要拖动的方块
        if (m_eMoveDirection == Direction_Horizontal)
        {
            for (int i = 0; i < HORIZON_BLOCK_NUM; ++i)
            {
                m_vecMoveList.push_back(m_arrBlockItem[m_iSelectedRow][i]);
                m_arrBlockItem[m_iSelectedRow][i].pCube->SetHorizonAndVeticalDraw(true, false);
                m_arrBlockItem[m_iSelectedRow][i].pBlock->SetHorizonAndVeticalDraw(true, false);
                m_arrBlockItem[m_iSelectedRow][i].pFrame->SetHorizonAndVeticalDraw(true, false);
            }
        }
        else if (m_eMoveDirection == Direction_Vetical)
        {
            for (int i = 0; i < VETICAL_BLOCK_NUM; ++i)
            {
                m_vecMoveList.push_back(m_arrBlockItem[i][m_iSelectedCol]);
                m_arrBlockItem[i][m_iSelectedCol].pCube->SetHorizonAndVeticalDraw(false, true);
                m_arrBlockItem[i][m_iSelectedCol].pBlock->SetHorizonAndVeticalDraw(false, true);
                m_arrBlockItem[i][m_iSelectedCol].pFrame->SetHorizonAndVeticalDraw(false, true);
            }
        }
    }
    if (m_eMoveDirection == Direction_Horizontal)               // 横向拖动
    {
        // 拖动整排方块
        vector<BLOCK_INFO>::iterator iter;
        for (iter = m_vecMoveList.begin(); iter != m_vecMoveList.end(); ++iter)
        {
            CCPoint pos = (*iter).pBlock->getPosition();
            pos.x += offsetPoint.x;
            
            (*iter).pBlock->setPosition(pos);
            (*iter).pCube->setPosition(pos);
            (*iter).pFrame->setPosition(pos);
        }
        
        // 记录拖动距离
        m_fDragDistance += offsetPoint.x;
    }
    else if (m_eMoveDirection == Direction_Vetical)             // 竖向拖动
    {
        // 拖动整排方块
        vector<BLOCK_INFO>::iterator iter;
        for (iter = m_vecMoveList.begin(); iter != m_vecMoveList.end(); ++iter)
        {
            CCPoint pos = (*iter).pBlock->getPosition();
            pos.y += offsetPoint.y;
            
            (*iter).pBlock->setPosition(pos);
            (*iter).pCube->setPosition(pos);
            (*iter).pFrame->setPosition(pos);
        }
        
        // 记录拖动距离
        m_fDragDistance += offsetPoint.y;
    }
    
    if (m_eMoveDirection != Direction_None)
    {
        m_pLine3->setIsVisible(false);
        m_pLine4->setIsVisible(false);
        if (m_eMoveDirection == Direction_Horizontal)
        {
            m_pLine1->setRotation(0.0f); 
            m_pLine2->setRotation(0.0f);
            
            CCPoint pos = m_vecMoveList[0].pBlock->getPosition();
            m_pLine1->setPosition(ccp(160.0f, pos.y - 53.0f / 2.0f));
            m_pLine2->setPosition(ccp(160.0f, pos.y + 53.0f / 2.0f));
        }
        else if (m_eMoveDirection == Direction_Vetical)
        {
            m_pLine1->setRotation(90.0f); 
            m_pLine2->setRotation(90.0f);
            
            CCPoint pos = m_vecMoveList[0].pBlock->getPosition();
            m_pLine1->setPosition(ccp(pos.x - 53.0f / 2.0f, 120.0f));
            m_pLine2->setPosition(ccp(pos.x + 53.0f / 2.0f, 120.0f));
        }
    }
}

/************************************************************************/
#pragma mark - Layer_Block类事件函数 : 弹出
/************************************************************************/
void Layer_Block::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
    m_pLine1->setIsVisible(false);
    m_pLine2->setIsVisible(false);
    m_pLine3->setIsVisible(false);
    m_pLine4->setIsVisible(false);
    
    // 新手引导 : 恢复距离限制
    if (PlayerDataManage::m_GuideBattleMark)
        m_fDistanceLimit = 0.0f;

    // 如果没有发生任何的位移，则不处理
    if (m_eMoveDirection == Direction_None)
        return;
    
    // 设置处理事件标志为true
    m_bIsClearEvent = true;
    
    //                                                                                  ----> ProcessBlockClearEvent() 
    // 对齐方块，在函数中对齐后函数次序-> RefreshBlockPositionAfterAlign() -> blockClear() ->  |
    //                                                                                  ----> resetState()
    AlignBlockAfterDrag();
}

/************************************************************************/
#pragma mark - Layer_Block类opengl访问函数
/************************************************************************/
void Layer_Block::visit()
{
    // 打开opengl裁减
    glEnable(GL_SCISSOR_TEST);
    
    // 设置裁减区域，左下角为0 ,0，水平往右为x正方向，竖直向上为y正方向
    glScissor(m_visibleRange.origin.x * m_fCurrentSystemScale, m_visibleRange.origin.y * m_fCurrentSystemScale, m_visibleRange.size.width * m_fCurrentSystemScale, m_visibleRange.size.height * m_fCurrentSystemScale);
    
    // 调用基类方法
    CCLayer::visit();
    
    // 关闭opengl裁减
    glDisable(GL_SCISSOR_TEST);
}

/************************************************************************/
#pragma mark - Layer_Block类在初始化时消除能clear的方块函数
/************************************************************************/
bool Layer_Block::ClearBlockAtBeginning()
{
    m_bIsBeginningClear = false;
    
    // 遍历整个方块阵 - 横向
    for (int i = 0; i < VETICAL_BLOCK_NUM; ++i)
    {
        for (int j = 0; j < HORIZON_BLOCK_NUM; ++j)
        {
            // 临时列表没有，放入一个
            if (m_vecTempList.size() == 0)
            {
                m_vecTempList.push_back(m_arrBlockItem[i][j].pBlock);
                continue;
            }
            
            // 临时是否一致，一致就放入，不一致进行个数判断
            if (m_arrBlockItem[i][j].pBlock->GetBlockType() == m_vecTempList[0]->GetBlockType())
            {
                m_vecTempList.push_back(m_arrBlockItem[i][j].pBlock);
            }
            else
            {
                if (AddTempListToClearList() == true)
                    m_bIsBeginningClear = true;
                
                m_vecTempList.push_back(m_arrBlockItem[i][j].pBlock);
            }
        }
        
        // 一行遍历完成，则进行判断次
        if (AddTempListToClearList() == true)
            m_bIsBeginningClear = true;
    }
    
    // 遍历整个方块阵 - 横向
    for (int i = 0; i < HORIZON_BLOCK_NUM; ++i)
    {
        for (int j = 0; j < VETICAL_BLOCK_NUM; ++j)
        {
            // 临时列表没有，放入一个
            if (m_vecTempList.size() == 0)
            {
                m_vecTempList.push_back(m_arrBlockItem[j][i].pBlock);
                continue;
            }
            
            // 临时是否一致，一致就放入，不一致进行个数判断
            if (m_arrBlockItem[j][i].pBlock->GetBlockType() == m_vecTempList[0]->GetBlockType())
            {
                m_vecTempList.push_back(m_arrBlockItem[j][i].pBlock);
            }
            else
            {
                if (AddTempListToClearList() == true)
                    m_bIsBeginningClear = true;
                
                m_vecTempList.push_back(m_arrBlockItem[j][i].pBlock);
            }
        }
        
        // 一行遍历完成，则进行判断次
        if (AddTempListToClearList() == true)
            m_bIsBeginningClear = true;
    }
    
    for (set<Sprite_Block*>::iterator iter = m_setClearBlock.begin(); iter != m_setClearBlock.end(); ++iter)
    {
        int iRow = 0;
        int iCol = 0;
        
        (*iter)->GetRowAndCol(iRow, iCol);
        
        // 随机新方块
        int iRand = rand() % BLOCK_NONE + 1;
        
        char szCubeName[32] = {0};
        char szBlockName[32] = {0};
        
        sprintf(szCubeName, "cube_%d%d.png", iRand, iRand);
        sprintf(szBlockName, "block_%d.png", iRand);
        
        CCSpriteFrame* pCubeFrame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(szCubeName);
        CCSpriteFrame* pBlockFrame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(szBlockName);
        
        m_arrBlockItem[iRow][iCol].pCube->setDisplayFrame(pCubeFrame);
        m_arrBlockItem[iRow][iCol].pBlock->setDisplayFrame(pBlockFrame);
        m_arrBlockItem[iRow][iCol].pBlock->SetBlockType(static_cast<BLOCK_TYPE>(iRand));
    }
    
    m_setClearBlock.clear();
    
    return m_bIsBeginningClear;
}

/************************************************************************/
#pragma mark - Layer_Block类方块消除函数
/************************************************************************/
void Layer_Block::blockClear()
{
    // 方块消除标记 true - 表示这次有方块消除     false - 表示这次没有方块消除
    bool bIsClear = false;
    
    // 遍历整个方块阵 - 横向
    for (int i = 0; i < VETICAL_BLOCK_NUM; ++i)
    {
        for (int j = 0; j < HORIZON_BLOCK_NUM; ++j)
        {
            // 临时列表没有，放入一个
            if (m_vecTempList.size() == 0)
            {
                m_vecTempList.push_back(m_arrBlockItem[i][j].pBlock);
                continue;
            }
            
            // 临时是否一致，一致就放入，不一致进行个数判断
            if (m_arrBlockItem[i][j].pBlock->GetBlockType() == m_vecTempList[0]->GetBlockType())
            {
                m_vecTempList.push_back(m_arrBlockItem[i][j].pBlock);
            }
            else
            {
                if (AddTempListToClearList() == true)
                    bIsClear = true;
                
                m_vecTempList.push_back(m_arrBlockItem[i][j].pBlock);
            }
        }
        
        // 一行遍历完成，则进行判断次
        if (AddTempListToClearList() == true)
            bIsClear = true;
    }
    
    // 遍历整个方块阵 - 横向
    for (int i = 0; i < HORIZON_BLOCK_NUM; ++i)
    {
        for (int j = 0; j < VETICAL_BLOCK_NUM; ++j)
        {
            // 临时列表没有，放入一个
            if (m_vecTempList.size() == 0)
            {
                m_vecTempList.push_back(m_arrBlockItem[j][i].pBlock);
                continue;
            }
            
            // 临时是否一致，一致就放入，不一致进行个数判断
            if (m_arrBlockItem[j][i].pBlock->GetBlockType() == m_vecTempList[0]->GetBlockType())
            {
                m_vecTempList.push_back(m_arrBlockItem[j][i].pBlock);
            }
            else
            {
                if (AddTempListToClearList() == true)
                    bIsClear = true;
                
                m_vecTempList.push_back(m_arrBlockItem[j][i].pBlock);
            }
        }
        
        // 一行遍历完成，则进行判断次
        if (AddTempListToClearList() == true)
            bIsClear = true;
    }
    
    if (bIsClear)
    {
        if (!m_bIsBeginningClear)
            m_bIsBeginningClear = bIsClear;
    }
    
    // 如果有能消除的方块，则进行消除，否则重置状态，进行下一次操作
    if (bIsClear)
    {
        // 播放音效
        KNUIFunction::GetSingle()->PlayerEffect("14.wav");
        ProcessBlockClearEvent();
        
        // 新手引导
        if (PlayerDataManage::m_GuideBattleMark)
        {
            for (int i = 0; i < HORIZON_BLOCK_NUM; i++)
            {
                m_arrBlockItem[2][i].pBlock->setVertexZ(0.0f);
                m_arrBlockItem[2][i].pCube->setVertexZ(0.0f);
                m_arrBlockItem[2][i].pFrame->setVertexZ(0.0f);
            }
            
            for (int i = 0; i < VETICAL_BLOCK_NUM; i++)
            {
                m_arrBlockItem[i][2].pBlock->setVertexZ(0.0f);
                m_arrBlockItem[i][2].pCube->setVertexZ(0.0f);
                m_arrBlockItem[i][2].pFrame->setVertexZ(0.0f);
            }
        }
    }
    else
    {
        resetState();
    }
}

/************************************************************************/
#pragma mark - Layer_Block类方块消除事件函数
/************************************************************************/
void Layer_Block::ProcessBlockClearEvent()
{
    // 新手引导 : 关闭遮罩
    if (PlayerDataManage::m_GuideBattleMark)
    {
        // 显示拖动列
        for (int i = 0; i < HORIZON_BLOCK_NUM; i++)
        {
            /*m_arrBlockItem[2][i].pBlock->setVertexZ(0.0f);
            m_arrBlockItem[2][i].pCube->setVertexZ(0.0f);
            m_arrBlockItem[2][i].pFrame->setVertexZ(0.0f);*/
        }
        
        SetLayerAlpha(false);
        
        // 关闭提示箭头
        Layer_GameGuide::GetSingle()->CloseGuideArrow();
    }
    
    for (set<Sprite_Block*>::iterator iter = m_setClearBlock.begin(); iter != m_setClearBlock.end(); ++iter)
    {
        // 设置消除方式
        (*iter)->SetBigClearMark(IsBlockBigClear((*iter)->GetBlockType()));
        
        // 获得方块行和列
        int iRow = 0;
        int iCol = 0;
        (*iter)->GetRowAndCol(iRow, iCol);
        
        CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Block::playAnim));
        CCFadeOut* pFadeOut = CCFadeOut::actionWithDuration(0.5f);
        CCDelayTime* pTime = CCDelayTime::actionWithDuration(0.5f);
        Action_BlockChange* pChange = Action_BlockChange::actionWithBlockInfo(0.4f, iRow, iCol);
        
        if (pChange != NULL && pFuncN != NULL)
        {
            (*iter)->runAction(pFadeOut);
            (*iter)->runAction(CCSequence::actions(pFuncN, pTime, pChange, NULL));
        }
    }
    
    CCDelayTime* pTime = CCDelayTime::actionWithDuration(0.9f);
    CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Block::blockClear));
    runAction(CCSequence::actions(pTime, pFunc, NULL));
    
    // 计算战斗数据
    caleBattleData();
    
    // 清空消除列表
    m_setClearBlock.clear();
}

/************************************************************************/
#pragma mark - Layer_Block类加入消除列表函数
/************************************************************************/
bool Layer_Block::AddTempListToClearList()
{
    // 是否满足消除个数
    if (m_vecTempList.size() >= CLEAR_BLOCK_NUM)
    {
        vector<Sprite_Block*>::iterator iter;
        for (iter = m_vecTempList.begin(); iter != m_vecTempList.end(); ++iter)
        {
            // 是否列表里已经存在，不存在则插入进取
            if (m_setClearBlock.find(*iter) == m_setClearBlock.end())
                m_setClearBlock.insert(*iter);
        }
        
        // 清空临时列表
        m_vecTempList.clear();
        
        m_bIsBlockClear = true;
        
        ++m_iComboNum;
        
        return true;
    }
    
    // 清空临时列表
    m_vecTempList.clear();
    
    return false;
}

/************************************************************************/
#pragma mark - Layer_Block类拖动后对齐方块函数
/************************************************************************/
void Layer_Block::AlignBlockAfterDrag()
{
    int temp = static_cast<int>(m_fDragDistance);
    int index = 0;
    
    int a = temp / 53;
    float fOffset = m_fDragDistance - a * 53;
    
    // 复原拖动列位置
    if (m_eMoveDirection == Direction_Horizontal)
    {
        vector<BLOCK_INFO>::iterator iter;
        for (iter = m_vecMoveList.begin(); iter != m_vecMoveList.end(); ++iter)
        {
            CCPoint pos = (*iter).pBlock->getPosition();
            
            // 计算偏移位置
            float fBlockOffset = 0.0f;
            if (fOffset <= -HORIZON_BLOCK_DELTA / 2.0f)                              // 超过方块尺寸的一半，就算拖动一格
            {
                fBlockOffset = fOffset + HORIZON_BLOCK_DELTA;
                fBlockOffset *= -1;
                m_iMoveBlockNum = m_fDragDistance / HORIZON_BLOCK_DELTA - 1;
                
                pos.x += fBlockOffset;
            }
            else if (fOffset > -HORIZON_BLOCK_DELTA / 2.0f && fOffset < 0.0f)        // 在方块尺寸的一半内，不算成功
            {
                fBlockOffset = fOffset;
                m_iMoveBlockNum = m_fDragDistance / HORIZON_BLOCK_DELTA;
                
                pos.x -= fBlockOffset;
            }
            else if (fOffset >= 0.0f && fOffset < HORIZON_BLOCK_DELTA / 2.0f)         // 在方块尺寸的一半内，不算成功
            {
                fBlockOffset = fOffset;
                m_iMoveBlockNum = m_fDragDistance / HORIZON_BLOCK_DELTA;
                
                pos.x -= fBlockOffset;
            }
            else if (fOffset >= HORIZON_BLOCK_DELTA / 2.0f)                          // 超过方块尺寸的一半，就算拖动一格
            {
                fBlockOffset = HORIZON_BLOCK_DELTA - fOffset;
                m_iMoveBlockNum = m_fDragDistance / HORIZON_BLOCK_DELTA + 1;
                pos.x += fBlockOffset;
            }
            
            // 移动方块
            CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.2f, pos);
            if (pMoveTo != NULL)
                (*iter).pBlock->runAction(pMoveTo);
            else
                (*iter).pBlock->setPosition(pos);
            
            // 移动外边框
            pMoveTo = CCMoveTo::actionWithDuration(0.2f, pos);
            if (pMoveTo != NULL)
                (*iter).pFrame->runAction(pMoveTo);
            else
                (*iter).pFrame->setPosition(pos);
            
            ++index;
            if (index == m_vecMoveList.size())
            {
                // 等待位置对齐后进行数组对齐
                pMoveTo = CCMoveTo::actionWithDuration(0.2f, pos);
                CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Block::RefreshBlockPositionAfterAlign));
                if (pFunc != NULL)
                    (*iter).pCube->runAction(CCSequence::actions(pMoveTo, pFunc, NULL));
            }
            else
            {
                // 移动立方体
                pMoveTo = CCMoveTo::actionWithDuration(0.2f, pos);
                if (pMoveTo != NULL)
                    (*iter).pCube->runAction(pMoveTo);
                else
                    (*iter).pCube->setPosition(pos);
            }
        }
    }
    else if (m_eMoveDirection == Direction_Vetical)
    {
        vector<BLOCK_INFO>::iterator iter;
        for (iter = m_vecMoveList.begin(); iter != m_vecMoveList.end(); ++iter)
        {
            CCPoint pos = (*iter).pBlock->getPosition();
            
            // 计算偏移位置
            float fBlockOffset = 0.0f;
            if (fOffset <= -VETICAL_BLOCK_DELTA / 2.0f)                          // 超过方块尺寸的一半，就算拖动一格
            {
                fBlockOffset = fOffset + VETICAL_BLOCK_DELTA;
                fBlockOffset *= -1;
                m_iMoveBlockNum = m_fDragDistance / VETICAL_BLOCK_DELTA - 1;
                
                pos.y += fBlockOffset;
            }
            else if (fOffset > -VETICAL_BLOCK_DELTA / 2.0f && fOffset < 0.0f)    // 在方块尺寸的一半内，不算成功
            {
                fBlockOffset = fOffset;
                m_iMoveBlockNum = m_fDragDistance / VETICAL_BLOCK_DELTA;
                
                pos.y -= fBlockOffset;
            }
            else if (fOffset >= 0.0f && fOffset < VETICAL_BLOCK_DELTA / 2.0f)     // 在方块尺寸的一半内，不算成功
            {
                fBlockOffset = fOffset;
                m_iMoveBlockNum = m_fDragDistance / VETICAL_BLOCK_DELTA;
                
                pos.y -= fBlockOffset;
            }
            else if (fOffset >= VETICAL_BLOCK_DELTA / 2.0f)                      // 超过方块尺寸的一半，就算拖动一格
            {
                fBlockOffset = VETICAL_BLOCK_DELTA - fOffset;
                m_iMoveBlockNum = m_fDragDistance / VETICAL_BLOCK_DELTA + 1;
                pos.y += fBlockOffset;
            }
            
            // 移动方块
            CCMoveTo* pMoveTo = CCMoveTo::actionWithDuration(0.2f, pos);
            if (pMoveTo != NULL)
                (*iter).pBlock->runAction(pMoveTo);
            else
                (*iter).pBlock->setPosition(pos);
            
            // 移动外边框
            pMoveTo = CCMoveTo::actionWithDuration(0.2f, pos);
            if (pMoveTo != NULL)
                (*iter).pFrame->runAction(pMoveTo);
            else
                (*iter).pFrame->setPosition(pos);
            
            ++index;
            if (index == m_vecMoveList.size())
            {
                // 等待位置对齐后进行数组对齐
                pMoveTo = CCMoveTo::actionWithDuration(0.2f, pos);
                CCCallFunc* pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Block::RefreshBlockPositionAfterAlign));
                if (pFunc != NULL)
                    (*iter).pCube->runAction(CCSequence::actions(pMoveTo, pFunc, NULL));
            }
            else
            {
                // 移动立方体
                pMoveTo = CCMoveTo::actionWithDuration(0.2f, pos);
                if (pMoveTo != NULL)
                    (*iter).pCube->runAction(pMoveTo);
                else
                    (*iter).pCube->setPosition(pos);
            }
        }
    }
}

/************************************************************************/
#pragma mark - Layer_Block类回调函数 : 刷新对齐后方块位置函数
/************************************************************************/
void Layer_Block::RefreshBlockPositionAfterAlign()
{
    if (m_eMoveDirection == Direction_Horizontal)
    {
        if (m_iMoveBlockNum > 0)
        {
            for (int i = 0; i < m_iMoveBlockNum; ++i)
            {
                m_arrBlockItem[m_iSelectedRow][i] = m_vecMoveList[HORIZON_BLOCK_NUM - m_iMoveBlockNum + i];
                
                CCPoint pos = m_arrBlockItem[m_iSelectedRow][i].pBlock->getPosition();
                pos.x = HORIZON_START_POS + i * HORIZON_BLOCK_DELTA;
                
                m_arrBlockItem[m_iSelectedRow][i].pBlock->setPosition(pos);
                m_arrBlockItem[m_iSelectedRow][i].pBlock->SetRowAndCol(m_iSelectedRow, i);
                m_arrBlockItem[m_iSelectedRow][i].pCube->setPosition(pos);
                m_arrBlockItem[m_iSelectedRow][i].pFrame->setPosition(pos);
            }
            
            // 移动拖出去的方块
            int index = 0;
            for (int i = m_iMoveBlockNum; i < HORIZON_BLOCK_NUM; ++i)
            {
                m_arrBlockItem[m_iSelectedRow][i] = m_vecMoveList[index];
                
                m_arrBlockItem[m_iSelectedRow][i].pBlock->SetRowAndCol(m_iSelectedRow, i);
                
                index++;
            }
        }
        else if (m_iMoveBlockNum < 0)
        {
            m_iMoveBlockNum *= -1;
            for (int i = 0; i < HORIZON_BLOCK_NUM - m_iMoveBlockNum; ++i)
            {
                m_arrBlockItem[m_iSelectedRow][i] = m_vecMoveList[m_iMoveBlockNum + i];
                m_arrBlockItem[m_iSelectedRow][i].pBlock->SetRowAndCol(m_iSelectedRow, i);
            }
            
            // 移动拖出去的方块
            int index = 0;
            for (int i = HORIZON_BLOCK_NUM - m_iMoveBlockNum; i < HORIZON_BLOCK_NUM; ++i)
            {
                m_arrBlockItem[m_iSelectedRow][i] = m_vecMoveList[index];
                
                CCPoint pos = m_arrBlockItem[m_iSelectedRow][i].pBlock->getPosition();
                pos.x = HORIZON_START_POS + i * HORIZON_BLOCK_DELTA;
                
                m_arrBlockItem[m_iSelectedRow][i].pBlock->SetRowAndCol(m_iSelectedRow, i);
                
                m_arrBlockItem[m_iSelectedRow][i].pBlock->setPosition(pos);
                m_arrBlockItem[m_iSelectedRow][i].pCube->setPosition(pos);
                m_arrBlockItem[m_iSelectedRow][i].pFrame->setPosition(pos);
                
                index++;
            }
        }
        
        // 隐藏不显示
        for (int i = 0; i < HORIZON_BLOCK_NUM; i++)
        {
            m_arrBlockItem[m_iSelectedRow][i].pCube->SetHorizonAndVeticalDraw(false, false);
            m_arrBlockItem[m_iSelectedRow][i].pBlock->SetHorizonAndVeticalDraw(false, false);
            m_arrBlockItem[m_iSelectedRow][i].pFrame->SetHorizonAndVeticalDraw(false, false);
        }
    }
    else if (m_eMoveDirection == Direction_Vetical)
    {
        if (m_iMoveBlockNum > 0)
        {
            for (int i = 0; i < VETICAL_BLOCK_NUM - m_iMoveBlockNum; ++i)
            {
                m_arrBlockItem[i][m_iSelectedCol] = m_vecMoveList[m_iMoveBlockNum + i];
                
                m_arrBlockItem[i][m_iSelectedCol].pBlock->SetRowAndCol(i, m_iSelectedCol);
            }
            
            // 移动拖出去的方块
            int index = 0;
            for (int i = VETICAL_BLOCK_NUM - m_iMoveBlockNum; i < VETICAL_BLOCK_NUM; ++i)
            {
                m_arrBlockItem[i][m_iSelectedCol] = m_vecMoveList[index];
                
                CCPoint pos = m_arrBlockItem[i][m_iSelectedCol].pBlock->getPosition();
                pos.y = VETICAL_START_POS - i * VETICAL_BLOCK_DELTA;
                
                m_arrBlockItem[i][m_iSelectedCol].pBlock->setPosition(pos);
                m_arrBlockItem[i][m_iSelectedCol].pCube->setPosition(pos);
                m_arrBlockItem[i][m_iSelectedCol].pFrame->setPosition(pos);
                
                m_arrBlockItem[i][m_iSelectedCol].pBlock->SetRowAndCol(i, m_iSelectedCol);
                
                index++;
            }
        }
        else if (m_iMoveBlockNum < 0)
        {
            m_iMoveBlockNum *= -1;
            for (int i = 0; i < m_iMoveBlockNum; ++i)
            {
                m_arrBlockItem[i][m_iSelectedCol] = m_vecMoveList[VETICAL_BLOCK_NUM - m_iMoveBlockNum + i];
                
                CCPoint pos = m_arrBlockItem[i][m_iSelectedCol].pBlock->getPosition();
                pos.y = VETICAL_START_POS - i * VETICAL_BLOCK_DELTA;
                
                m_arrBlockItem[i][m_iSelectedCol].pBlock->setPosition(pos);
                m_arrBlockItem[i][m_iSelectedCol].pCube->setPosition(pos);
                m_arrBlockItem[i][m_iSelectedCol].pFrame->setPosition(pos);
                
                m_arrBlockItem[i][m_iSelectedCol].pBlock->SetRowAndCol(i, m_iSelectedCol);
            }
            
            // 移动拖出去的方块
            int index = 0;
            for (int i = m_iMoveBlockNum; i < VETICAL_BLOCK_NUM; ++i)
            {
                m_arrBlockItem[i][m_iSelectedCol] = m_vecMoveList[index];
                
                m_arrBlockItem[i][m_iSelectedCol].pBlock->SetRowAndCol(i, m_iSelectedCol);
                index++;
            }
        }
        
        for (int i = 0; i < VETICAL_BLOCK_NUM; i++)
        {
            m_arrBlockItem[i][m_iSelectedCol].pCube->SetHorizonAndVeticalDraw(false, false);
            m_arrBlockItem[i][m_iSelectedCol].pBlock->SetHorizonAndVeticalDraw(false, false);
            m_arrBlockItem[i][m_iSelectedCol].pFrame->SetHorizonAndVeticalDraw(false, false);
        }
    }
    
    // 进行方块消除
    blockClear();
}

/************************************************************************/
#pragma mark - Layer_Block类重置状态函数
/************************************************************************/
void Layer_Block::resetState()
{
    // 新手引导
    if (PlayerDataManage::m_GuideBattleMark)
    {
        if (Layer_GameGuide::GetSingle()->GetBattleGuideStep() == 1 && m_bIsBeginningClear)
            Layer_GameGuide::GetSingle()->FinishBattleGuideStep1();
        else if (Layer_GameGuide::GetSingle()->GetBattleGuideStep() == 2 && m_bIsBeginningClear)
            Layer_GameGuide::GetSingle()->FinishBattleGuideStep2();
        else if (Layer_GameGuide::GetSingle()->GetBattleGuideStep() == 3 && m_iMoveBlockNum != 0)
            Layer_GameGuide::GetSingle()->FinishBattleGuideStep3();
        else if (Layer_GameGuide::GetSingle()->GetBattleGuideStep() == 4 && m_iMoveBlockNum != 0)
            Layer_GameGuide::GetSingle()->FinishBattleGuideStep4();
    }
    
    m_bIsBeginningClear = false;
    
    // 还原拖动方向，进行下一次拖动判断
    m_eMoveDirection = Direction_None;
    
    // 清空拖动列表
    m_vecMoveList.clear();
    
    // 重置拖动的距离
    m_fDragDistance = 0.0f;
    
    // 清空消除列表
    m_setClearBlock.clear();
    
    // 重置消除事件
    m_bIsClearEvent = false;
    
    // 记录最大combo数量
    if (m_iComboNum > m_iMaxComboNum)
        m_iMaxComboNum = m_iComboNum;
    
    // 清空combo数
    m_iComboNum = 0;
    
    // 是否有方块消除，有消除则进行战斗
    if (m_iMoveBlockNum != 0)
    {
        Layer_Follower::ShareInstance() -> StartAction();
        
        // 清空拖动格数
        m_iMoveBlockNum = 0;
    }
}

/************************************************************************/
#pragma mark - Layer_Block类计算战斗数据函数
/************************************************************************/
void Layer_Block::caleBattleData()
{
    // 临时列表，用于临时随机一格相同方块的位置
    vector<Sprite_Block*> vecTempList;
    
    int dispelCount = 0;
    
    // 计算
    for (int i = BLOCK_TYPE1_SWORD; i <= BLOCK_NONE; i++)
    {
        int num = 0;
        set<Sprite_Block*>::iterator iter;
        for (iter = m_setClearBlock.begin(); iter != m_setClearBlock.end(); iter++)
        {
            if ((*iter)->GetBlockType() == i)
            {
                ++num;
                vecTempList.push_back(*iter);
            }
        }
        
        // 有方块消除，则进行combo计算
        if (num != 0)
        {
            dispelCount ++;
            
            // 随机一个方块
            int iRand = rand() % num;
            CCPoint pos = vecTempList[iRand]->getPosition();
            
            SetOperationEnable(false);
            Layer_Follower::ShareInstance() -> AddFollowerCombo(i, num, pos);
            
            // 清空临时列表，开始下一个方块类型的随机
            vecTempList.clear();
        }
    }
    
    Layer_Follower::ShareInstance() -> DisplayFollowerCombo(dispelCount);
}

/************************************************************************/
#pragma mark - Layer_Block类回调函数 : 播放动画函数
/************************************************************************/
void Layer_Block::playAnim(CCObject* pObject)
{
    // 取得方块对象
    Sprite_Block* pSprite = dynamic_cast<Sprite_Block*>(pObject);
    if (pSprite != NULL)
    {
        char szTemp[32] = {0};
        CCAnimation* pAnimation = NULL;
        
        if (pSprite->GetBigClearMark())
        {
            sprintf(szTemp, "bigblock_clear_");
            pAnimation = CreateAnimation(szTemp, 0.03f);
        }
        else
        {
            int type = pSprite->GetBlockType();
            sprintf(szTemp, "%s", g_AnimFilename[type - 1]);
            pAnimation = CreateAnimation(g_AnimFilename[type - 1], 0.03f);
        }
        
        CCAnimate* pAnimate = CCAnimate::actionWithAnimation(pAnimation);
        CCCallFuncN* pFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Block::removeAnim));
        
        // 创建动画对象
        sprintf(szTemp, "%s%d.png", szTemp, 1);
        Sprite_Frame* pAnim = Sprite_Frame::frameWithSpriteFrameName(szTemp);
        if (pAnim != NULL && pAnimate != NULL && pFuncN != NULL)
        {
            pAnim->setPosition(pSprite->getPosition());
            //pAnim->setVertexZ(1.0f);
            addChild(pAnim, 5);
            pAnim->runAction(CCSequence::actions(pAnimate, pFuncN, NULL));
        }
    }
}

/************************************************************************/
#pragma mark - Layer_Block类回调函数 : 移除动画函数
/************************************************************************/
void Layer_Block::removeAnim(CCObject* pObject)
{
    // 取得动画对象
    Sprite_Frame* pSprite = dynamic_cast<Sprite_Frame*>(pObject);
    if (pSprite != NULL)
        removeChild(pSprite, true);
}

/************************************************************************/
#pragma mark - Layer_Block类是否是大消除函数
/************************************************************************/
bool Layer_Block::IsBlockBigClear(BLOCK_TYPE eType)
{
    int num = 0;
    set<Sprite_Block*>::iterator numIter;
    for (numIter = m_setClearBlock.begin(); numIter != m_setClearBlock.end(); numIter++)
    {
        if (eType == (*numIter)->GetBlockType())
            num++;
    }
    
    return num >= FINAL_CLEAR_BLOCK_NUM;
}

/************************************************************************/
#pragma mark - Layer_Block类回调函数 : 逻辑更新函数
/************************************************************************/
void Layer_Block::update(ccTime dt)
{
    switch (m_eLightState) {
        case LIGHT_LIFTMOVE:
        {
            CCPoint pos = m_pLight->getPosition();
            
            if (pos.x < 880.0f)
                pos.x += 20.0f;
            else
                m_eLightState = LIGHT_RIGHTMOVE;
            
            m_pLight->setPosition(pos);
        }
            break;
        case LIGHT_RIGHTMOVE:
        {
            CCPoint pos = m_pLight->getPosition();
            
            if (pos.x > - 480.0f)
                pos.x -= 20.0f;
            else
                m_eLightState = LIGHT_WAIT;
            
            m_pLight->setPosition(pos);
        }
            break;
        case LIGHT_WAIT:
        {
            static int time = 1;
            if (++time % 1200 == 0)
                m_eLightState = LIGHT_LIFTMOVE;
        }
            break;
        default:
            break;
    }
}