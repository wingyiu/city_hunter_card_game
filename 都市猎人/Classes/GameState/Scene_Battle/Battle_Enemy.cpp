//
//  Battle_Enemy.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-5-26.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Battle_Enemy.h"
USING_NS_CC;

#include "Follower.h"
#include "Battle_Follower.h"
#include "Skill.h"
#include "Layer_Enemy.h"
#include "Layer_Follower.h"
#include "Layer_Display.h"
#include "SystemDataManage.h"
#include "MyMoveAction.h"
#include "Action_MyMove.h"
#include "KNUIFunction.h"

#pragma mark 战斗中敌人包装
#pragma mark -


#define  TEST_Attack   0 

/************************************************************************/
#pragma mark 战斗敌人的初始化
/************************************************************************/
bool Battle_Enemy::init()
{
    if(! CCLayer::init())   return false;

    m_IsDead = false;
    m_IsUnderAttack = false;
    m_IsUnderShowDead = false;
    m_IsUnderShowAttack = false;
    m_IsUnderShowSkillAnimation = false;
    
    m_RealAttack = 0;
    
    m_CDRecord = 1;
    m_CDColor = 255;
    m_CDColorDir = 2;
    m_CDColorChange = 0;
    
    m_WorldPoint = CCPointMake(0, 0);
    
    m_IconSize = CCSizeMake(0, 0);
    m_IconCenterPoint = CCPointMake(0, 0);
    m_IconBottomPoint = CCPointMake(0, 0);
    
    m_NumSize_Attack1 = CCSizeMake(0, 0); 
    m_NumSize_Attack2 = CCSizeMake(0, 0); 
    m_NumSize_Attack3 = CCSizeMake(0, 0); 
    m_NumSize_Attack4 = CCSizeMake(0, 0);
    m_NumSize_CD = CCSizeMake(0, 0); 
    
    return true;
}

Battle_Enemy::~Battle_Enemy()
{    
    printf("Battle_Enemy:: 释放完成\n");
}

void Battle_Enemy::SetData(Enemy_Data * Data)
{
    memcpy(&m_Data, Data, sizeof(Enemy_Data));
}

void Battle_Enemy::onExit()
{
    this -> removeAllChildrenWithCleanup(true);
    
    for(EnemyHurtlist::iterator it = m_HurtValueList.begin(); it != m_HurtValueList.end(); )
    {
        SAFE_DELETE(*it);
        m_HurtValueList.erase(it ++);
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 战斗敌人初始化数据
/************************************************************************/
void Battle_Enemy::Initialize()
{
    /////////////加入Icon图片///////////////
    CCSprite * Icon = CCSprite::spriteWithSpriteFrameName(m_Data.Enemy_IconName);
    Icon -> setAnchorPoint(ccp(0.5, 0));
    
    m_IconSize.width = Icon -> getTextureRect().size.width;
    m_IconSize.height = Icon -> getTextureRect().size.height;
    
    m_IconCenterPoint.x = 0;
    m_IconCenterPoint.y = 0 + m_IconSize.height / 2;
    
    m_IconBottomPoint.x = 0;
    m_IconBottomPoint.y = 0;
    
    m_WorldPoint.x = this -> getPosition().x;
    m_WorldPoint.y = this -> getPosition().y + m_IconSize.height / 2;
    
    Icon -> setPosition(ccp(0, 0));
    Icon -> setOpacity(0);
    
    this -> addChild(Icon, 0, EnemyTag_Icon);
    ////////////////////////////////////
    
    
    ////////////加入Hp背景图片    加入Hp图片/////////////
    const char * HpBGName = NULL;
    const char * HpName = NULL;
    if(Icon -> getContentSize().width * 2 < 200)
    {
        HpBGName = "EnemyHP1_BG.png";
        HpName = "EnemyHP1.png";
    }
    else if(Icon -> getContentSize().width * 2 < 300)
    {
        HpBGName = "EnemyHP2_BG.png";
        HpName = "EnemyHP2.png";
    }
    else
    {
        HpBGName = "EnemyHP3_BG.png";
        HpName = "EnemyHP3.png";
    }
    
    ////////////////////////////////////
    CCSprite * HpBG = CCSprite::spriteWithSpriteFrameName(HpBGName);

    HpBG -> setPosition(ccp(m_IconCenterPoint.x, m_IconCenterPoint.y + m_IconSize.height / 2));
    HpBG -> setOpacity(0);
    
    this -> addChild(HpBG, 0, EnemyTag_HpBg);
    
    ////////////////////////////////////
    CCSprite * Hp = CCSprite::spriteWithSpriteFrameName(HpName);
    
    Hp -> setAnchorPoint(ccp(0.0f, 0.5f));
    if(Icon -> getContentSize().width * 2 >= 300)
        Hp -> setPosition(ccp(m_IconCenterPoint.x - HpBG -> getTextureRect().size.width / 2 + 21.7, m_IconCenterPoint.y + m_IconSize.height / 2 + 1));
    else
        Hp -> setPosition(ccp(m_IconCenterPoint.x - HpBG -> getTextureRect().size.width / 2 + 14, m_IconCenterPoint.y + m_IconSize.height / 2));
    Hp -> setOpacity(0);
    
    this -> addChild(Hp, 0, EnemyTag_Hp);
    ////////////////////////////////////
    
    
    ////////////加入职业Icon////////////
    char buff[32];
    sprintf(buff, "EnemyProIcon%d.png", m_Data.Enemy_Profession);
    
    CCSprite * ProIcon = CCSprite::spriteWithSpriteFrameName(buff);
    
    ProIcon -> setAnchorPoint(ccp(0.0f, 0.5f));
    if(Icon -> getContentSize().width * 2 >= 300)
        ProIcon -> setPosition(ccp(HpBG -> getPosition().x - HpBG -> getTextureRect().size.width / 2 + 8.5, HpBG -> getPosition().y + 1));
    else
        ProIcon -> setPosition(ccp(HpBG -> getPosition().x - HpBG -> getTextureRect().size.width / 2 + 1, HpBG -> getPosition().y));
    ProIcon -> setOpacity(0);
    
    this -> addChild(ProIcon, 0, EnemyTag_ProIcon);
    ////////////////////////////////////
    
    
    ////////////加入准星图片////////////
    CCSprite * Post = CCSprite::spriteWithSpriteFrameName("Post.png");

    Post -> setPosition(m_IconCenterPoint);
    Post -> setOpacity(0);

    this -> addChild(Post, 0, EnemyTag_Post);
    ////////////////////////////////////
    
    
    ////////////减血数字的大小////////////
    CCSprite * NumImage1 = CCSprite::spriteWithFile("Number_FollowerAttack1-hd.png");
    m_NumSize_Attack1 = CCSizeMake(NumImage1 -> getTextureRect().size.width / 12, NumImage1 -> getTextureRect().size.height);
    CCSprite * NumImage2 = CCSprite::spriteWithFile("Number_FollowerAttack2-hd.png");
    m_NumSize_Attack2 = CCSizeMake(NumImage2 -> getTextureRect().size.width / 12, NumImage2 -> getTextureRect().size.height);
    CCSprite * NumImage3 = CCSprite::spriteWithFile("Number_FollowerAttack3-hd.png");
    m_NumSize_Attack3 = CCSizeMake(NumImage3 -> getTextureRect().size.width / 12, NumImage3 -> getTextureRect().size.height);
    CCSprite * NumImage4 = CCSprite::spriteWithFile("Number_DecreaseHP-hd.png");
    m_NumSize_Attack4 = CCSizeMake(NumImage4 -> getTextureRect().size.width / 12, NumImage4 -> getTextureRect().size.height);
    ////////////////////////////////////
    
    
    ////////////CD数字的大小////////////
    CCSprite * NumImage5 = CCSprite::spriteWithFile("Number_EnemyCD-hd.png");
    m_NumSize_CD = CCSizeMake(NumImage5 -> getTextureRect().size.width / 12, NumImage5 -> getTextureRect().size.height);
    ////////////////////////////////////
    
    /////////////计算CD时间///////////////
    m_CDRecord += m_Data.Enemy_CDRound - 1;
    ////////////////////////////////////
}
/************************************************************************/
#pragma mark-
/************************************************************************/



/************************************************************************/
#pragma mark 显示敌人出现
/************************************************************************/
void Battle_Enemy::DisplaySelf()
{
    //出现的动画
    CCAnimation * pAnimation = CreateAnimation("enmey_in_", 0.05f);
    CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
    CCCallFuncN * pRemoveAppearAnimation = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveAppearAnimation));
    
    CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("enmey_in_1.png");
    pSprite -> setAnchorPoint(ccp(0.5, 0));
    pSprite -> setPosition(m_IconBottomPoint);
    pSprite -> runAction(CCSequence::actions(pAnimate, pRemoveAppearAnimation, NULL));
    
    this -> addChild(pSprite);
    
    //元素淡入
    CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(0.05f * 3);
    CCCallFunc * pShowSelf = CCCallFunc::actionWithTarget(this, callfunc_selector(Battle_Enemy::ShowSelf));
    this -> runAction(CCSequence::actions(pDelayTime, pShowSelf, NULL));
}

void Battle_Enemy::RemoveAppearAnimation(CCNode * sender)
{
    this -> removeChild(sender, true);
}

void Battle_Enemy::ShowSelf()
{
    CCObject * pObject = NULL;
    CCARRAY_FOREACH(getChildren(), pObject)
    {
        CCNode * pChild = dynamic_cast<CCNode*>(pObject);
        if(pChild -> getTag() != EnemyTag_Post)
        {
            CCFadeIn * pFadeIn = CCFadeIn::actionWithDuration(0.2f);
            pChild -> runAction(pFadeIn);
        }
    }  
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示准星
/************************************************************************/
void Battle_Enemy::DisplayPost()
{
    if(! m_IsDead)
    {
        CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(EnemyTag_Post));
        
        CCFadeTo * pFadeIn = CCFadeTo::actionWithDuration(0.3f, 255);
        
        pSprite -> stopAllActions();
        pSprite -> runAction(pFadeIn);
    }
}

void Battle_Enemy::HidePost()
{
    CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(EnemyTag_Post));
    
    if(pSprite -> getOpacity() != 0)
    {
        CCFadeTo * pFadeOut = CCFadeTo::actionWithDuration(0.3f, 0);
        
        pSprite -> stopAllActions();
        pSprite -> runAction(pFadeOut);
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示CD时间
/************************************************************************/
void Battle_Enemy::DisplayCDTime()
{
    CCSprite * HpBG = dynamic_cast<CCSprite*>(this -> getChildByTag(EnemyTag_HpBg));
    
    char buff[16];
    sprintf(buff, "%d", m_CDRecord);
    CCLabelAtlas * pLabelAtlas = CCLabelAtlas::labelWithString(buff, "Number_EnemyCD-hd.png", m_NumSize_CD.width, m_NumSize_CD.height, '.');
    pLabelAtlas -> setPosition(ccp(0 + HpBG -> getTextureRect().size.width / 2, 0));
    pLabelAtlas -> setOpacity(0);
    this -> addChild(pLabelAtlas, 0, EnemyTag_CDNum);
    
    CCMoveTo * pMoveTo = CCMoveTo::actionWithDuration(0.2f, ccp(0 + HpBG -> getTextureRect().size.width / 2, HpBG -> getPosition().y - m_NumSize_CD.height / 2));
    CCFadeIn * pFadeIn = CCFadeIn::actionWithDuration(0.2f);
    pLabelAtlas -> runAction(CCSpawn::actions(pMoveTo, pFadeIn, NULL));
    
    m_CDColorChange = 30 - 10 * (m_CDRecord - 1);
    if(m_CDColorChange <= 5) m_CDColorChange = 5;
    
    unschedule(schedule_selector(Battle_Enemy::DisplayCDTimeColor));
    schedule(schedule_selector(Battle_Enemy::DisplayCDTimeColor));
}

void Battle_Enemy::DisplayCDTimeCallBack1(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
}

void Battle_Enemy::DisplayCDTimeColor()
{
    if(m_CDColorDir == 1)
    {
        if(m_CDColor - m_CDColorChange >= 0)    m_CDColor -= m_CDColorChange;
        else    {m_CDColor = 0;  m_CDColorDir = 2;}
    }
    else if(m_CDColorDir == 2)
    {
        if(m_CDColor + m_CDColorChange <= 255) m_CDColor += m_CDColorChange;
        else    {m_CDColor = 255;  m_CDColorDir = 1;}
    }
    
    CCLabelAtlas * pLabelAtlas = static_cast<CCLabelAtlas*>(this -> getChildByTag(EnemyTag_CDNum));
    pLabelAtlas -> setColor(ccc3(255, m_CDColor, m_CDColor));
}

void Battle_Enemy::DisplayAddCDTime()
{
    m_CDRecord += m_Data.Enemy_CDRound - 1;
    char buff[16];
    sprintf(buff, "%d", m_CDRecord);
    
    CCLabelAtlas * pLabelAtlas = static_cast<CCLabelAtlas*>(this -> getChildByTag(EnemyTag_CDNum));
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.2f);
    CCCallFuncN * pDisplayCDTimeCallBack1 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::DisplayCDTimeCallBack1));
    pLabelAtlas -> runAction(CCSequence::actions(pFadeOut, pDisplayCDTimeCallBack1, NULL));
    
    DisplayCDTime();
}

void Battle_Enemy::DisplayReduceCDTime()
{
    m_CDRecord -= 1;
    if(m_CDRecord <= 1) m_CDRecord = 1;
    char buff[16];
    sprintf(buff, "%d", m_CDRecord);
    
    CCLabelAtlas * pLabelAtlas = static_cast<CCLabelAtlas*>(this -> getChildByTag(EnemyTag_CDNum));
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.2f);
    CCCallFuncN * pDisplayCDTimeCallBack1 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::DisplayCDTimeCallBack1));
    pLabelAtlas -> runAction(CCSequence::actions(pFadeOut, pDisplayCDTimeCallBack1, NULL));
    
    DisplayCDTime();
}
/************************************************************************/
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark 计算对小弟的伤害
/************************************************************************/
void Battle_Enemy::CalculateAttackFollower()
{
    m_RealAttack = m_Data.Enemy_Attack;
    
    //如果有克制伤害加成
    if(Layer_Follower::ShareInstance() -> GetOverComeAttackPercent() != 0.0)
    {
        if(m_Data.Enemy_Profession == Layer_Follower::ShareInstance() -> GetTeamBeOverComePro())
        {
            m_RealAttack += Layer_Follower::ShareInstance() -> GetOverComeAttackPercent() * m_RealAttack;
        }
    }
    
    //如果有提升伤害百分比
    if(Layer_Follower::ShareInstance() -> GetAddAttackPercent() != 0.0)
    {
        m_RealAttack += Layer_Follower::ShareInstance() -> GetAddAttackPercent() * m_RealAttack;
    }
    
    //如果小弟有某职业伤害减免
    if(Layer_Follower::ShareInstance() -> GetProDerateAttackPercent() != 0.0)
    {
        if(m_Data.Enemy_Profession == Layer_Follower::ShareInstance() -> GetProDerate())
        {
            m_RealAttack -= Layer_Follower::ShareInstance() -> GetProDerateAttackPercent() * m_RealAttack;
            if(m_RealAttack <= 0)   m_RealAttack = 1;
        }
    }
    
    //如果小弟有直接减免伤害值
    if(Layer_Follower::ShareInstance() -> GetDerateAttack() != 0)
    {
        m_RealAttack -= Layer_Follower::ShareInstance() -> GetDerateAttack();
        if(m_RealAttack <= 0)   m_RealAttack = 1;
    }
    
    m_RealAttack -= Layer_Follower::ShareInstance() -> GetFollowerDefense();
    if(m_RealAttack <= 0)   m_RealAttack = 1;
    
    Layer_Follower::ShareInstance() -> CalculateBeAttackResult(m_RealAttack);
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 计算被伤害
/************************************************************************/
void Battle_Enemy::CalculateBeAttackByFollower(int value, Battle_Follower * Sender)
{    
    int RealHurt = value;
    
    //如果有克制伤害加成
    if(Layer_Follower::ShareInstance() -> GetOverComeAttackPercent() != 0.0f)
    {
        //如果该小弟的职业和发出克制技能的职业相同
        if(Sender -> GetHost() -> GetData() -> Follower_Profession == Layer_Follower::ShareInstance() -> GetTeamOverComePro())
        {
            //如果目标的职业是该小弟的对应克制职业
            if(m_Data.Enemy_Profession == Sender -> GetHost() -> GetData() -> OverCome_Profession)
            {
                RealHurt += Layer_Follower::ShareInstance() -> GetOverComeAttackPercent() * RealHurt;
            }
        }
    }
    
    //如果有提升队伍伤害百分比
    if(Layer_Follower::ShareInstance() -> GetAddAttackPercent() != 0)
    {
        RealHurt += Layer_Follower::ShareInstance() -> GetAddAttackPercent() * RealHurt;
    }
    
    //最后再减去敌人的防御值
    RealHurt = RealHurt - m_Data.Enemy_Defense;
    if(RealHurt <= 0) RealHurt = 1;
    
    HurtValue * pHurtValue = new HurtValue(value, RealHurt);
    m_HurtValueList.push_back(pHurtValue);
    
    if(! m_IsDead)
    {
        CalculateDecreaseHP(RealHurt);
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 受击减血
/************************************************************************/
void Battle_Enemy::CalculateDecreaseHP(float value)
{
    if(m_Data.Enemy_HP - value >= 0)
    {
        m_Data.Enemy_HP -= value;
    }else
    {
        m_Data.Enemy_HP = 0;
        m_IsDead = true;
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示被攻击
/************************************************************************/
void Battle_Enemy::DisplayBeAttack(bool isAnimation, int mode, Battle_Follower * sender)
{
    //隐藏准星
    HidePost();
    
    //Icon受击震动
    CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(EnemyTag_Icon));
    if(! pSprite -> getActionByTag(ActionTag_IconShake))
    {
        Action_MyMove * pMyMove = Action_MyMove::actionWithDuration(5, 8, 0.01f);
        pMyMove -> setTag(ActionTag_IconShake);
        pSprite -> runAction(pMyMove);
    }
    
    //播放受击音效
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> playEffect("5.wav");
    
    int pro = sender -> GetHost() -> GetData() -> Follower_Profession;
    //播放受击动画
    if(isAnimation)
    {
        const char * filename = NULL;
        CCSprite * pAni = NULL;

        //普通攻击动画
        if(mode == 1)
        {
            if(pro == SWORD)
                filename = "SwordAttack_";
            else if(pro == FIST)
                filename = "FistAttack_";
            else if(pro == GUN)
                filename = "GunAttack_";
            
            char buff[32];
            sprintf(buff, "%s%d.png", filename, 1);
            pAni = CCSprite::spriteWithSpriteFrameName(buff);
            
            if(pro == GUN)
            {
                pAni -> setAnchorPoint(ccp(0.5, 0));
                pAni -> setPosition(m_IconBottomPoint);
            }else if(pro == FIST)
            {
                pAni -> setPosition(m_IconCenterPoint);
            }else if(pro == SWORD)
            {
                pAni -> setAnchorPoint(ccp(0.5, 0));
                pAni -> setPosition(m_IconBottomPoint);
            }
        }
        //技能攻击动画
        else if(mode == 2)
        {
            if(pro == SWORD)
                filename = "SkillSwordAttackAll_";
            else if(pro == FIST)
                filename = "SkillFistAttackAll_";
            else if(pro == GUN)
                filename = "SkillGunAttackAll_";
            
            char buff[32];
            sprintf(buff, "%s%d.png", filename, 1);
            pAni = CCSprite::spriteWithSpriteFrameName(buff);
            pAni -> setAnchorPoint(ccp(0.5, 0));
            pAni -> setPosition(m_IconBottomPoint);
        }
        
        CCAnimation * pAnimation = CreateAnimation(filename, 0.06f);
        CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
        CCCallFuncN * pAniOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveAnimation));
        
        pAni -> runAction(CCSequence::actions(pAnimate, pAniOver, NULL));
        this -> addChild(pAni);
    }
    
    //飘动出受到的伤害的数字
    EnemyHurtlist::iterator it = m_HurtValueList.begin();
    if(it != m_HurtValueList.end())
    {
        //伤害数字
        char buff[32];
        sprintf(buff, "%d", (*it) -> Hurt);
        
        CCLabelAtlas * pLabelAtlas = NULL;
        if(pro == SWORD)
        {
            pLabelAtlas = CCLabelAtlas::labelWithString(buff, "Number_FollowerAttack1-hd.png", m_NumSize_Attack1.width, m_NumSize_Attack1.height, '.');
            pLabelAtlas -> setPosition(ccp(m_WorldPoint.x - (m_NumSize_Attack1.width * strlen(buff)), m_WorldPoint.y));
        }else if(pro == FIST)
        {
            pLabelAtlas = CCLabelAtlas::labelWithString(buff, "Number_FollowerAttack2-hd.png", m_NumSize_Attack2.width, m_NumSize_Attack2.height, '.');
            pLabelAtlas -> setPosition(ccp(m_WorldPoint.x - (m_NumSize_Attack2.width * strlen(buff)), m_WorldPoint.y));
        }else if(pro == GUN)
        {
            pLabelAtlas = CCLabelAtlas::labelWithString(buff, "Number_FollowerAttack3-hd.png", m_NumSize_Attack3.width, m_NumSize_Attack3.height, '.');
            pLabelAtlas -> setPosition(ccp(m_WorldPoint.x - (m_NumSize_Attack3.width * strlen(buff)), m_WorldPoint.y));
        }

        //移动
        CCMoveTo * pMoveTo = CCMoveTo::actionWithDuration(0.7f, ccp(m_WorldPoint.x - 20 + rand() % 40, m_WorldPoint.y - 50 + rand() % 100));
        CCCallFuncN * pMoveOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::NumMoveOver));
        pLabelAtlas -> runAction(CCSequence::actions(pMoveTo, pMoveOver, NULL));
        
        //伤害数字变化
        if((*it) -> Hurt != (*it) -> RealHurt)
        {
            CCCallFuncND * pNumChange = CCCallFuncND::actionWithTarget(this, callfuncND_selector(Battle_Enemy::NumChange), *it);
            CCRepeat * pRepeat = CCRepeat::actionWithAction(pNumChange, 100);
            pRepeat -> setTag(999);
            pLabelAtlas -> runAction(pRepeat);
            
            m_HurtValueList.erase(it);
        }
        else
        {
            SAFE_DELETE((*it));
            m_HurtValueList.erase(it);
        }
        
        Layer_Display::ShareInstance() -> addChild(pLabelAtlas);
    }
    
    //血条变化
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.2, m_Data.Enemy_HP / m_Data.Original_HP, 1.0);
    this -> getChildByTag(EnemyTag_Hp) -> runAction(pScaleTo);
}

void Battle_Enemy::RemoveAnimation(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
}

void Battle_Enemy::NumMoveOver(cocos2d::CCNode * sender)
{
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.7f);
    CCCallFuncN * pRemoveNum = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveNum));
    
    sender -> runAction(CCSequence::actions(pFadeOut, pRemoveNum, NULL));
}

void Battle_Enemy::RemoveNum(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
    
    //如果敌人死亡，直接播放死亡动画
    if(m_IsDead)
    {
        DisplayDead();
    }
    else
    {
        m_IsUnderAttack = false;
    }
}

void Battle_Enemy::NumChange(CCNode * sender, void * data)
{
    HurtValue * hurt = static_cast<HurtValue*>(data);
    
    if(hurt -> Hurt + 5 < hurt -> RealHurt)     
        hurt -> Hurt += 5;
    else 
        hurt -> Hurt = hurt -> RealHurt;
    
    CCLabelAtlas * label = static_cast<CCLabelAtlas*>(sender);
    
    char buff[32];
    sprintf(buff, "%d", hurt -> Hurt);
    label -> setString(buff);
    
    if(hurt -> Hurt >= hurt -> RealHurt)  
    {
        sender -> stopActionByTag(999);
        SAFE_DELETE(hurt);
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 显示被技能攻击
/************************************************************************/
void Battle_Enemy::DisplayBeAttackBySkill(Battle_Follower * sender)
{
    m_IsUnderShowSkillAnimation = true;
    
    int SkillType = sender -> GetHost() -> GetData() -> Follower_CommonlySkillType;
    int SkillID = sender -> GetHost() -> GetData() -> Follower_CommonlySkillID;
    
    if(SkillType == 1)
    {
        //技能类型1（13 － 15）
        if(SkillID >= 13 && SkillID <= 15)
        {
            CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("Animation17_1.png");
            pSprite -> setPosition(m_IconCenterPoint);
            
            CCAnimation * pAnimation = CreateAnimation("Animation17_", 0.3f);
            CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
            CCCallFuncN * pAniamtionOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveSkillAnimation));
            pSprite -> runAction(CCSequence::actions(pAnimate, pAniamtionOver, NULL));
            
            this -> addChild(pSprite);
        }
        //技能类型1（16 － 18）
        else if(SkillID >= 16 && SkillID <= 18)
        {
            CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("Animation19_1.png");
            pSprite -> setPosition(m_IconCenterPoint);
            
            CCAnimation * pAnimation = CreateAnimation("Animation19_", 0.3f);
            CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
            CCCallFuncN * pAniamtionOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveSkillAnimation));
            pSprite -> runAction(CCSequence::actions(pAnimate, pAniamtionOver, NULL));
            
            this -> addChild(pSprite);
        }
        //技能类型1（27）
        else if(SkillID == 27)
        {
            CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("Animation18_1.png");
            pSprite -> setPosition(m_IconCenterPoint);
            
            CCAnimation * pAnimation = CreateAnimation("Animation18_", 0.3f);
            CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
            CCCallFuncN * pAniamtionOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveSkillAnimation));
            pSprite -> runAction(CCSequence::actions(pAnimate, pAniamtionOver, NULL));
            
            this -> addChild(pSprite);
        }
    }
    else if(SkillType == 2)
    {
        //技能类型2（1 － 3）
        if((SkillID >= 1 && SkillID <= 3) || SkillID == 7)
        {
            CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("Animation29_1.png");
            pSprite -> setPosition(m_IconCenterPoint);
            
            CCAnimation * pAnimation = CreateAnimation("Animation29_", 0.08f);
            CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
            CCCallFuncN * pAniamtionOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveSkillAnimation));
            pSprite -> runAction(CCSequence::actions(pAnimate, pAniamtionOver, NULL));
            
            this -> addChild(pSprite);
        }
        //技能类型2（4 － 6）
        else if(SkillID >= 4 && SkillID <= 6)
        {
            CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("Animation28_1.png");
            pSprite -> setPosition(m_IconCenterPoint);
            
            CCAnimation * pAnimation = CreateAnimation("Animation28_", 0.08f);
            CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
            CCCallFuncN * pAniamtionOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveSkillAnimation));
            pSprite -> runAction(CCSequence::actions(pAnimate, pAniamtionOver, NULL));
            
            this -> addChild(pSprite);    
        }
    }
    //技能类型3（1 － 6）
    //技能类型5（1 － 7  10 - 11）
    else if((SkillType == 3 && (SkillID >= 1 && SkillID <= 6)) ||
            (SkillType == 5 && ((SkillID >= 1 && SkillID <= 7) || (SkillID >= 10 && SkillID <= 11))))
    {
        CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("Animation101_1.png");
        pSprite -> setPosition(m_IconCenterPoint);
        
        CCAnimation * pAnimation = CreateAnimation("Animation101_", 0.08f);
        CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
        CCRepeat * pRepeat = CCRepeat::actionWithAction(pAnimate, 2);
        CCCallFuncN * pAniamtionOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveSkillAnimation));
        pSprite -> runAction(CCSequence::actions(pRepeat, pAniamtionOver, NULL));
        
        this -> addChild(pSprite);
    }
    else if(SkillType == 6)
    {
        //技能类型6（1 － 2）
        CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("Animation34_1.png");
        pSprite -> setPosition(m_IconCenterPoint);
        
        CCAnimation * pAnimation = CreateAnimation("Animation34_", 0.1f);
        CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
        CCCallFuncN * pAniamtionOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveSkillAnimation));
        pSprite -> runAction(CCSequence::actions(pAnimate, pAniamtionOver, NULL));
        
        this -> addChild(pSprite);
    }
}

void Battle_Enemy::RemoveSkillAnimation(cocos2d::CCNode * sender)
{
    this -> removeChild(sender, true);
    m_IsUnderShowSkillAnimation = false;
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示被持续性技能攻击
/************************************************************************/
void Battle_Enemy::DisplayBeAttackByKeepSkill()
{
    m_IsUnderAttack = true;
    
    //Icon受击震动
    CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(EnemyTag_Icon));
    if(! pSprite -> getActionByTag(ActionTag_IconShake))
    {
        Action_MyMove * pMyMove = Action_MyMove::actionWithDuration(5, 8, 0.01f);
        pMyMove -> setTag(ActionTag_IconShake);
        pSprite -> runAction(pMyMove);
    }
    
    //飘动出受到的伤害的数字
    EnemyHurtlist::iterator it = m_HurtValueList.begin();
    if(it != m_HurtValueList.end())
    {
        //伤害数字
        char buff[32];
        sprintf(buff, "%d", (*it) -> Hurt);
        CCLabelAtlas * pLabelAtlas = CCLabelAtlas::labelWithString(buff, "Number_DecreaseHP-hd.png", m_NumSize_Attack4.width, m_NumSize_Attack4.height, '.');
        pLabelAtlas -> setPosition(ccp(m_WorldPoint.x - (m_NumSize_Attack4.width * strlen(buff)), m_WorldPoint.y));
      
        //移动
        CCMoveTo * pMoveTo = CCMoveTo::actionWithDuration(0.7f, ccp(m_WorldPoint.x - 20 + rand() % 40, m_WorldPoint.y - 50 + rand() % 100));
        CCCallFuncN * pNumMoveOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::NumMoveOver));
        pLabelAtlas -> runAction(CCSequence::actions(pMoveTo, pNumMoveOver, NULL));
        
        if((*it) -> Hurt != (*it) -> RealHurt)
        {
            CCCallFuncND * pNumChange = CCCallFuncND::actionWithTarget(this, callfuncND_selector(Battle_Enemy::NumChange), *it);
            CCRepeat * pRepeat = CCRepeat::actionWithAction(pNumChange, 100);
            pRepeat -> setTag(999);
            pLabelAtlas -> runAction(pRepeat);
            
            m_HurtValueList.erase(it);
        }
        else
        {
            SAFE_DELETE((*it));
            m_HurtValueList.erase(it);
        }
        
        Layer_Display::ShareInstance() -> addChild(pLabelAtlas);
    }
    
    //血条变化
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.6, m_Data.Enemy_HP / m_Data.Original_HP, 1.0);
    this -> getChildByTag(EnemyTag_Hp) -> runAction(pScaleTo);
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示攻击
/************************************************************************/
void Battle_Enemy::DisplayAttack()
{
    m_IsUnderShowAttack = true;
    
    CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("enemy_attack_1.png");
    pSprite -> setPosition(m_WorldPoint);
    
    CCAnimation * pAnimation = CreateAnimation("enemy_attack_", 0.07f);
    CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
    CCCallFuncN * pDisplayAttackCallBack1 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::DisplayAttackCallBack1));
    
    pSprite -> runAction(CCSequence::actions(pAnimate, pDisplayAttackCallBack1, NULL));
    if(m_Data.Enemy_Profession == SWORD)
        pSprite -> setColor(ccc3(234, 253, 22));
    else if(m_Data.Enemy_Profession == FIST)
        pSprite -> setColor(ccc3(21, 206, 253));
    else if(m_Data.Enemy_Profession == GUN)
        pSprite -> setColor(ccc3(208, 11, 253));
    
    Layer_Display::ShareInstance() -> addChild(pSprite);
}

void Battle_Enemy::DisplayAttackCallBack1(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
    
    char buff[16];
    sprintf(buff, "AttackBall%d.png", m_Data.Enemy_Profession);
    CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName(buff);
    pSprite -> setPosition(m_WorldPoint);
    pSprite -> setOpacity(0);
    
    CCFadeIn * pFadeIn = CCFadeIn::actionWithDuration(0.15f);
    CCCallFuncN * pDisplayAttackCallBack2 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::DisplayAttackCallBack2));
    pSprite -> runAction(CCSequence::actions(pFadeIn, pDisplayAttackCallBack2, NULL));
    
    Layer_Display::ShareInstance() -> addChild(pSprite);
}

void Battle_Enemy::DisplayAttackCallBack2(CCNode * sender)
{
    sender -> stopAllActions();
    
    ccBezierConfig pBezierConfig;
    pBezierConfig.controlPoint_1 = m_WorldPoint;
    pBezierConfig.controlPoint_2 = CCPointMake(rand() % 320, m_WorldPoint.y);
    CCSprite * pSprite = static_cast<CCSprite*>(Layer_Follower::ShareInstance() -> getChildByTag(LFChildTag_HP));
    pBezierConfig.endPosition = ccp(pSprite -> getPosition().x + pSprite -> getTextureRect().size.width / 2, pSprite -> getPosition().y);
    
    CCBezierTo * pBezierTo = CCBezierTo::actionWithDuration(0.4f, pBezierConfig);
    CCEaseIn * pEaseIn = CCEaseIn::actionWithAction(pBezierTo, 1.4f);
    CCCallFuncN * pDisplayAttackCallBack3 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::DisplayAttackCallBack3));
    
    sender -> runAction(CCSequence::actions(pEaseIn, pDisplayAttackCallBack3, NULL));
}

void Battle_Enemy::DisplayAttackCallBack3(cocos2d::CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
    
    Layer_Follower::ShareInstance() -> DisplayBeAttack(m_RealAttack);
    
    m_IsUnderShowAttack = false;
    m_RealAttack = 0;
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示敌人死亡
/************************************************************************/
void Battle_Enemy::DisplayDead()
{
    if(! m_IsUnderShowDead)
        m_IsUnderShowDead = true;
    else
        return;
    
    //先把准星去掉
    this -> removeChildByTag(EnemyTag_Post, true);
    
    //显示死亡动画
    CCAnimation * pAnimation = CreateAnimation("enemy_out_", 0.07f);
    CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
    CCCallFuncN * pRemoveDeadAnimation = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveDeadAnimation));
    
    CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("enemy_out_1.png");
    pSprite -> setAnchorPoint(ccp(0.5, 0));
    pSprite -> setPosition(ccp(0, m_IconBottomPoint.y));
    pSprite -> runAction(CCSequence::actions(pAnimate, pRemoveDeadAnimation, NULL));
    
    this -> addChild(pSprite);
    
    //淡出元素
    CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(0.07f * 2);
    CCCallFunc * pDisappearSelf = CCCallFunc::actionWithTarget(this, callfunc_selector(Battle_Enemy::DisappearSelf));
    this -> runAction(CCSequence::actions(pDelayTime, pDisappearSelf, NULL));
}

void Battle_Enemy::RemoveDeadAnimation(CCNode * sender)
{
    this -> removeChild(sender, true);
}

void Battle_Enemy::DisappearSelf()
{
    this -> unschedule(schedule_selector(Battle_Enemy::DisplayCDTimeColor));
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.2f);
    
    CCObject * pObject = NULL;
    CCARRAY_FOREACH(getChildren(), pObject)
    {
        CCNode * pChild = dynamic_cast<CCNode *>(pObject);
        
        CCCallFuncN * pRemoveBEChild = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::RemoveBEChild));
        pChild -> runAction(CCSequence::actions(dynamic_cast<CCFadeOut*>(pFadeOut -> copy()), pRemoveBEChild, NULL));
    }
    
    this -> schedule(schedule_selector(Battle_Enemy::DisappearSelfOver));
}

void Battle_Enemy::RemoveBEChild(CCNode * sender)
{
    this -> removeChild(sender, true);
}

void Battle_Enemy::DisappearSelfOver(CCNode * sender)
{
    if(this -> getChildrenCount() <= 0)
    {
        this -> unschedule(schedule_selector(Battle_Enemy::DisappearSelfOver));
        
        //如果该敌人有掉落金钱
        if(m_Data.Enemy_DropMoney != 0)
        {
            Layer_Enemy::ShareInstance() -> m_PlayerBattleMoney += m_Data.Enemy_DropMoney;
            Layer_Display::ShareInstance() -> DisplayEnemyDropMoney(m_WorldPoint, this);
        }
        
        //如果该敌人有掉落小弟
        if(m_Data.Enemy_DropFollower_ID != 0)
        {
            Layer_Enemy::ShareInstance() -> m_PlayerBattleRes += 1;
            Layer_Display::ShareInstance() -> DisplayEnemyDropRes(m_WorldPoint, this);
            
            Follower * pFollower = Follower::node();
            pFollower -> SetData(SystemDataManage::ShareInstance() -> GetData_ForFollower(m_Data.Enemy_DropFollower_ID), false);
            
            pFollower -> GetData() -> Follower_ServerID = m_Data.Enemy_DropFollower_ServerID;
            pFollower -> GetData() -> Follower_Experience = 0;
            pFollower -> GetData() -> Follower_Level = m_Data.Enemy_DropFollower_Level;
            pFollower -> GetData() -> Follower_SkillLevel = 1;
            pFollower -> GetData() -> Follower_LevelUpExperience = m_Data.Enemy_DropFollower_LevelUpExp;
            pFollower -> GetData() -> Follower_BeAbsorbExperience = m_Data.Enemy_DropFollower_BeAbsorbExp;
            pFollower -> GetData() -> Follower_HP = m_Data.Enemy_DropFollower_HP;
            pFollower -> GetData() -> Follower_Attack = m_Data.Enemy_DropFollower_Attack;
            pFollower -> GetData() -> Follower_Defense = m_Data.Enemy_DropFollower_Defense;
            pFollower -> GetData() -> Follower_Comeback = m_Data.Enemy_DropFollower_ComeBack;
            
            Layer_Enemy::ShareInstance() -> m_PlayerBattleResList.push_back(pFollower);
        }
        
        if(m_Data.Enemy_DropMoney == 0 && m_Data.Enemy_DropFollower_ID == 0)
        {
            SetDisplayDeadOver();
        }
    }
}

void Battle_Enemy::SetDisplayDeadOver()
{
    if(m_IsUnderAttack)
    {
        m_IsUnderAttack = false;
        m_IsUnderShowDead = false;
        
        this -> removeFromParentAndCleanup(true);
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 检测点击
/************************************************************************/
bool Battle_Enemy::CheckBeTouch(CCPoint point)
{
    float x = this -> getPosition().x - m_IconSize.width / 2;
    float y = this -> getPosition().y;
    
    if(point.x >= x && point.x <= x + m_IconSize.width &&
       point.y >= y && point.y <= y + m_IconSize.height)
    {
        return true;
    }
    return false;
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/


#pragma mark 被小弟的技能影响的函数
#pragma mark -
#pragma mark -
#pragma mark -

/************************************************************************/
#pragma mark 被小弟的技能增加出手CD时间
/************************************************************************/
void Battle_Enemy::FollowerSkillAddCDRecord(int count)
{
    m_CDRecord += count;
    char buff[16];
    sprintf(buff, "%d", m_CDRecord);
    
    CCLabelAtlas * pLabelAtlas = static_cast<CCLabelAtlas*>(this -> getChildByTag(EnemyTag_CDNum));
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.3f);
    CCCallFuncN * pDisplayCDTimeCallBack1 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Battle_Enemy::DisplayCDTimeCallBack1));
    pLabelAtlas -> runAction(CCSequence::actions(pFadeOut, pDisplayCDTimeCallBack1, NULL));
    
    DisplayCDTime();
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 减防御 & 加回防御
/************************************************************************/
void Battle_Enemy::CalculateDecreaseDefense(float value)
{
    m_Data.Enemy_Defense -= value;
    if(m_Data.Enemy_Defense <= 0.0f) 
        m_Data.Enemy_Defense = 1.0f;
}

void Battle_Enemy::CalculatePlusDefense(float value)
{
    m_Data.Enemy_Defense += value;
    if(m_Data.Enemy_Defense >= m_Data.Original_Defense)
        m_Data.Enemy_Defense = m_Data.Original_Defense;
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark  减攻击力 & 加回攻击力
/************************************************************************/
void Battle_Enemy::CalculateDecreaseAttack(float value)
{
    m_Data.Enemy_Attack -= value;
    if(m_Data.Enemy_Attack <= 0.0f) 
        m_Data.Enemy_Attack = 1.0f;
}

void Battle_Enemy::CalculatePlusAttack(float value)
{
    m_Data.Enemy_Attack += value;
    if(m_Data.Enemy_Attack >= m_Data.Original_Attack) 
        m_Data.Enemy_Attack = m_Data.Original_Attack;
}
/************************************************************************/
#pragma mark -
/************************************************************************/










 
