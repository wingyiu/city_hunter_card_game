//
//  Layer_Enemy.h
//  MidNightCity
//
//  Created by 强 张 on 12-6-6.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_Enemy_h
#define MidNightCity_Layer_Enemy_h

#include "cocos2d.h"
#include "MyConfigure.h"
class Battle_Enemy;
class Skill;
class Follower;

typedef map <int, Battle_Enemy*>            Battle_EnemyList;

#define  LEChildTag_Background 0
#define  LEChildTag_UIBackground 1
#define  LEChildTag_AnimBackground 2
#define  LEChildTag_UIStepIcon 3
#define  LEChildTag_UIStepSpace 4
#define  LEChildTag_UIMoneyIcon 5
#define  LEChildTag_UIResIcon  6
#define  LEChildTag_OutBattle1 7
#define  LEChildTag_OutBattle2 8


#define  ActionTag_BgShake 0

class Layer_Enemy : public cocos2d::CCLayer
{
    friend class Layer_BattleAction;
    friend class Scene_Battle;
    
public:
    static Layer_Enemy * ShareInstance();
    
public:
    void OutGame();
    
public:
    bool    init();
    void    onExit();
    ~Layer_Enemy();
    
    //开始显示场景和敌人
    void DisplayScene();
    void DisplayEnemy();       
    void DisplayEnemyOneByOne();
    void DisplayEnemyOneByOneUpdate();
    void DisplayEnemyOneByOneOver();
    
    //显示副本开始的动画
    void DisplayStart();
    void StartAnimationOver(cocos2d::CCNode * sender);
    void DisplayColorLayerAndAction();
    void DisplayColorLayerOver(cocos2d::CCNode * sender);
    void DisplayActionOver(cocos2d::CCNode * sender);
    
public:
    //创建所有的敌人列表
    void CreateEnemyList();
    
public:    
    //寻找血量最少的敌人
    int  FindEnemyByHP();
    //得到目标敌人
    Battle_Enemy * GetTargetEnemy();
    //得到敌人数量
    int            GetEnemyNum() {return m_EnemyList.size();} 
    
public:   
    void    AddDeBuff(Skill * debuff);
    void    DisplayDeBuff();
    void    RemoveDeBuff(Skill * debuff, bool needclose);
    void    ReSetPositionDeBuff(cocos2d::CCNode * sender, void * data);
    
    void    TriggerDeBuff();
    
    void    EnterBattleSceneAndDisplayWarning(const char * mapFilename);    
    void    EnterBattleSceneAndDisplayWarningCallBack1();
    void    EnterBattleScene(const char * mapFilename);
    void    EnterBattleSceneOver(cocos2d::CCNode * sender);
    
public:
    void    StartAction();
    
public:
    void    CheckEnemyUnderAttack();
    void    CheckEnemyUnderAttackBySkill();
    void    CheckEnemyUnderAttackByDeBuff();
    void    CheckEnemyUnderAttackOver();
    
public:
    void    DisposalEnemyDead();
    void    PassBattle();

public:
    void    StartAttack();
    void    CalculateAttackResultToFollower();
    void    DisplayAttack();
    void    CheckDisplayAttackOver();
    void    AttackOver();
    void    AttackOverFunc();
    
public:
    void    DisplayChangeBattleMoney();
    void    DisplayChangeBattleMoneyCallBack1();
    void    DisplayChangeBattleRes();
    void    DisplayChangeBattleResCallBack1();
    
public:
    virtual bool ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    virtual void ccTouchMoved(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    virtual void ccTouchEnded(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    
public:
    void    BGShake();
    void    BGDark();
    void    BGLight();
   
public:
    ////关于掉落的 和上方UI显示的东西////
    
    cocos2d::CCLabelAtlas *     m_LabelPlayerMoney;
    int                         m_LabelPlayerMoneyString;
    
    cocos2d::CCLabelAtlas *     m_LabelPlayerRes;
    int                         m_LabelPlayerResString;
    
    int                         m_PlayerBattleMoney;
    int                         m_PlayerBattleExperience;
    int                         m_PlayerBattleRes;
    list                        <Follower *> m_PlayerBattleResList;
    
    bool                        m_bAllClearLevel;
    
    /////////////////
    
    cocos2d::CCLabelAtlas   *       m_LabelCurrentStep;
    cocos2d::CCLabelAtlas   *       m_LabelAllStep;
    cocos2d::CCSprite       *       m_OutBattle1;
    cocos2d::CCSprite       *       m_OutBattle2;
    
    
public: 
    SS_GET(Battle_EnemyList*, &m_EnemyList, EnemyList);
    SS_GET(cocos2d::CCPoint, m_BgPoint, BgPoint);
    
public:
    GET_SET(bool, m_OperationEnable, OperationEnable);
    GET_SET(int, m_GuideStep, GuideStep);
    
private:
    static Layer_Enemy *    m_Instance;
    LAYER_NODE_FUNC(Layer_Enemy);
    
protected:
    cocos2d::CCSize             m_RealSize;
    cocos2d::CCSize             m_WinSize;
    cocos2d::CCPoint            m_BgPoint;
    cocos2d::CCSize             m_NumSize;
    
private:
    int                     m_TargetIndex;
    int                     m_GuideStep;
    
    Battle_EnemyList        m_EnemyList;
    
    list <Battle_Enemy*>    m_EnemyListTemporary;
    list <Battle_Enemy*>    m_EnemyFightList;
    
    list <Skill*>           m_EnemyDeBuffList;

    const char *    m_BGImageName;
    bool            m_IsFirstBattle;
    int             m_IsUnderAttackBySkill;
    bool            m_OperationEnable;
};

#endif
