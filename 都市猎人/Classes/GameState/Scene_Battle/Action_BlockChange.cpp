//
//  Action_BlockChange.cpp
//  Test
//
//  Created by 惠伟 孙 on 12-6-13.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Action_BlockChange.h"
#include "Layer_Block.h"

extern BLOCK_INFO m_arrBlockItem[VETICAL_BLOCK_NUM][HORIZON_BLOCK_NUM];
extern int g_blockStep1Clear[5][6];
extern int g_blockStep2[5][6];
extern int g_blockStep3[5][6];

/************************************************************************/
#pragma mark - Action_BlockChange类静态函数 : 创建方块变换事件
/************************************************************************/
Action_BlockChange* Action_BlockChange::actionWithBlockInfo(float fDuration, int iRow, int iCol, BLOCK_ROTATE_STATE eState, bool bIsEnded)
{
    Action_BlockChange* pAction = new Action_BlockChange();
    if (pAction != NULL)
    {
        if (pAction->initWithDuration(fDuration, iRow, iCol) == true)
        {
            pAction->m_eState = eState;
            pAction->m_bIsEnded = bIsEnded;
            
            pAction->autorelease();
            return pAction;
        }
    }   
       
    delete pAction;
    pAction = NULL;
    
    return NULL;
}

/************************************************************************/
#pragma mark - Action_BlockChange类初始化震动函数
/************************************************************************/
bool Action_BlockChange::initWithDuration(float fDuration, int iRow, int iCol)
{
    if (CCActionInterval::initWithDuration(fDuration) == true)
    {
        m_eChangeState = ROTATE_OUT;
        
        m_iRow = iRow;
        m_iCol = iCol;
        
        m_fRotateSpeed = 360.0f / fDuration * 0.1f;
        
        return true;
    }
    
    return false;
}

/************************************************************************/
#pragma mark - Action_BlockChange类构造函数
/************************************************************************/
Action_BlockChange::Action_BlockChange()
{
    m_eChangeState = ROTATE_OUT;
    
    m_iRow = 0;
    m_iCol = 0;
    
    m_fRotateSpeed = 0.0f;
    
    m_bIsEnded = false;
}

/************************************************************************/
#pragma mark - Action_BlockChange类析构函数
/************************************************************************/
Action_BlockChange::~Action_BlockChange()
{
    
}

/************************************************************************/
#pragma mark - Action_BlockChange类逻辑更新函数
/************************************************************************/
void Action_BlockChange::update(ccTime time)
{
    switch (m_eState) {
        case BLOCK_NORMAL:
            updateByNormal(time);
            break;
        case BLOCK_LEFT:
            updateByLeft(time);
            break;
        case BLOCK_RIGHT:
            updateByRight(time);
            break;
        default:
            break;
    }
}

/************************************************************************/
#pragma mark - Action_BlockChange类正常翻转更新函数
/************************************************************************/
void Action_BlockChange::updateByNormal(ccTime time)
{
    switch (m_eChangeState) {
        case ROTATE_OUT:
        {
            float fRotateX = 0.0f;
            float fRotateY = 0.0f;
            float fRotateZ = 0.0f;
            
            m_arrBlockItem[m_iRow][m_iCol].pCube->GetCubeRotation(fRotateX, fRotateY, fRotateZ);
            
            fRotateX += m_fRotateSpeed * time;
            if (fRotateX < 180.0f)
                m_arrBlockItem[m_iRow][m_iCol].pCube->SetCubeRotation(fRotateX, fRotateY, fRotateZ);
            else
                m_eChangeState = REINIT;
            
            int iRand = 0;
            
            // 新手引导 : 指定翻出的方块
            if (PlayerDataManage::m_GuideBattleMark)
            {
                if (Layer_GameGuide::GetSingle()->GetBattleGuideStep() == 1)
                    iRand = g_blockStep1Clear[m_iRow][m_iCol];
                else if (Layer_GameGuide::GetSingle()->GetBattleGuideStep() == 2)
                    iRand = g_blockStep2[m_iRow][m_iCol];
                else if (Layer_GameGuide::GetSingle()->GetBattleGuideStep() == 4)
                    iRand = g_blockStep3[m_iRow][m_iCol];
                else
                    iRand = rand() % BLOCK_NONE + 1;
            }
            else
            {
                // 随机新方块
                iRand = rand() % BLOCK_NONE + 1;
            }
            
            m_arrBlockItem[m_iRow][m_iCol].pBlock->SetBlockType(static_cast<BLOCK_TYPE>(iRand));
        }
            break;
        case REINIT:
        {
            int iRand = m_arrBlockItem[m_iRow][m_iCol].pBlock->GetBlockType();
            
            char szCubeName[32] = {0};
            sprintf(szCubeName, "cube_%d%d.png", iRand, iRand);
            
            // 更新立方体纹理
            CCSpriteFrame* pFrame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(szCubeName);
            if (pFrame != NULL)
            {
                // 设置立方体新纹理
                UpdateCubeInfoByRowAndCol(pFrame, m_iRow, m_iCol, iRand);
                m_arrBlockItem[m_iRow][m_iCol].pBlock->SetBlockType(static_cast<BLOCK_TYPE>(iRand));
            }
            
            // 切换状态
            m_eChangeState = ROTATE_IN;
        }
            break;
        case ROTATE_IN:
        {
            float fRotateX = 0.0f;
            float fRotateY = 0.0f;
            float fRotateZ = 0.0f;
            
            m_arrBlockItem[m_iRow][m_iCol].pCube->GetCubeRotation(fRotateX, fRotateY, fRotateZ);
            
            fRotateX += m_fRotateSpeed * time;
            if (fRotateX < 350.0f)
            {
                m_arrBlockItem[m_iRow][m_iCol].pCube->SetCubeRotation(fRotateX, fRotateY, fRotateZ);
            }
            else
            {
                m_eChangeState = RESET;
                m_arrBlockItem[m_iRow][m_iCol].pCube->SetCubeRotation(0.0f, 0.0f, 0.0f);
            }
        }
            break;
        case RESET:
        {
            char szBlockName[32] = {0};
            int iRand = m_arrBlockItem[m_iRow][m_iCol].pCube->GetBlockTempType();
            
            // 更新方块纹理
            sprintf(szBlockName, "block_%d.png", iRand);
            CCSpriteFrame* pFrame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(szBlockName);
            if (pFrame != NULL)
            {
                // 设置方块新纹理
                UpdateBlockInfoByRowAndCol(pFrame, m_iRow, m_iCol, iRand);
            }
            
            m_elapsed = m_fDuration;
            
            m_eChangeState = FINISH;
        }
            break;
        case FINISH:
            break;
        default:
            break;
    }
}

/************************************************************************/
#pragma mark - Action_BlockChange类左翻转更新函数
/************************************************************************/
void Action_BlockChange::updateByLeft(ccTime time)
{
    switch (m_eChangeState) {
        case ROTATE_OUT:
        {
            float fRotateX = 0.0f;
            float fRotateY = 0.0f;
            float fRotateZ = 0.0f;
            
            m_arrBlockItem[m_iRow][m_iCol].pCube->GetCubeRotation(fRotateX, fRotateY, fRotateZ);
            
            fRotateY += m_fRotateSpeed * time;
            if (fRotateY < 180.0f)
                m_arrBlockItem[m_iRow][m_iCol].pCube->SetCubeRotation(fRotateX, fRotateY, fRotateZ);
            else
                m_eChangeState = REINIT;
        }
            break;
        case REINIT:
        {
            // 随机新方块
            char szCubeName[32] = {0};
            
            int iRand = 0;
            
            if (m_bIsEnded)
                iRand = 8;
            else
                iRand = m_arrBlockItem[m_iRow][m_iCol].pBlock->GetBlockType();
            
            sprintf(szCubeName, "cube_%d%d.png", iRand, iRand);
            
            // 更新立方体纹理
            CCSpriteFrame* pFrame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(szCubeName);
            if (pFrame != NULL)
            {
                // 设置立方体新纹理
                UpdateCubeInfoByRowAndCol(pFrame, m_iRow, m_iCol, iRand);
            }
            
            // 切换状态
            m_eChangeState = ROTATE_IN;
        }
            break;
        case ROTATE_IN:
        {
            float fRotateX = 0.0f;
            float fRotateY = 0.0f;
            float fRotateZ = 0.0f;
            
            m_arrBlockItem[m_iRow][m_iCol].pCube->GetCubeRotation(fRotateX, fRotateY, fRotateZ);
            
            fRotateY += m_fRotateSpeed * time;
            if (fRotateY < 350.0f)
            {
                m_arrBlockItem[m_iRow][m_iCol].pCube->SetCubeRotation(fRotateX, fRotateY, fRotateZ);
            }
            else
            {
                m_eChangeState = RESET;
                m_arrBlockItem[m_iRow][m_iCol].pCube->SetCubeRotation(0.0f, 0.0f, 0.0f);
            }
        }
            break;
        case RESET:
        {
            m_elapsed = m_fDuration;
            m_eChangeState = FINISH;
        }
            break;
        case FINISH:
            break;
        default:
            break;
    }
}

/************************************************************************/
#pragma mark - Action_BlockChange类右翻转更新函数
/************************************************************************/
void Action_BlockChange::updateByRight(ccTime time)
{
    switch (m_eChangeState) {
        case ROTATE_OUT:
        {
            float fRotateX = 0.0f;
            float fRotateY = 0.0f;
            float fRotateZ = 0.0f;
            
            m_arrBlockItem[m_iRow][m_iCol].pCube->GetCubeRotation(fRotateX, fRotateY, fRotateZ);
            
            fRotateY -= m_fRotateSpeed * time;
            if (fRotateY > -180.0f)
                m_arrBlockItem[m_iRow][m_iCol].pCube->SetCubeRotation(fRotateX, fRotateY, fRotateZ);
            else
                m_eChangeState = REINIT;
        }
            break;
        case REINIT:
        {
            // 随机新方块
            char szCubeName[32] = {0};
            int iRand = 0;
            
            if (m_bIsEnded)
                iRand = 8;
            else
                iRand = m_arrBlockItem[m_iRow][m_iCol].pBlock->GetBlockType();
            
            sprintf(szCubeName, "cube_%d%d.png", iRand, iRand);
            
            // 更新立方体纹理
            CCSpriteFrame* pFrame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(szCubeName);
            if (pFrame != NULL)
            {
                // 设置立方体新纹理
                UpdateCubeInfoByRowAndCol(pFrame, m_iRow, m_iCol, iRand);
            }
            
            // 切换状态
            m_eChangeState = ROTATE_IN;
        }
            break;
        case ROTATE_IN:
        {
            float fRotateX = 0.0f;
            float fRotateY = 0.0f;
            float fRotateZ = 0.0f;
            
            m_arrBlockItem[m_iRow][m_iCol].pCube->GetCubeRotation(fRotateX, fRotateY, fRotateZ);
            
            fRotateY -= m_fRotateSpeed * time;
            if (fRotateY > -360.0f)
            {
                m_arrBlockItem[m_iRow][m_iCol].pCube->SetCubeRotation(fRotateX, fRotateY, fRotateZ);
            }
            else
            {
                m_eChangeState = RESET;
                m_arrBlockItem[m_iRow][m_iCol].pCube->SetCubeRotation(0.0f, 0.0f, 0.0f);
            }
        }
            break;
        case RESET:
        {
            m_elapsed = m_fDuration;
            m_eChangeState = FINISH;
        }
            break;
        case FINISH:
            break;
        default:
            break;
    }
}

/************************************************************************/
#pragma mark - Action_BlockChange类更新立方体信息函数
/************************************************************************/
void Action_BlockChange::UpdateCubeInfoByRowAndCol(CCSpriteFrame* pFrame, int iRow, int iCol, int iType)
{
    if (pFrame == NULL)
        return;
    
    m_arrBlockItem[iRow][iCol].pCube->setDisplayFrame(pFrame);
    m_arrBlockItem[iRow][iCol].pCube->SetBlockTempType(iType);
}

/************************************************************************/
#pragma mark - Action_BlockChange类更新方块信息函数
/************************************************************************/
void Action_BlockChange::UpdateBlockInfoByRowAndCol(CCSpriteFrame* pFrame, int iRow, int iCol, int iType)
{
    if (pFrame == NULL)
        return;
    
    m_arrBlockItem[iRow][iCol].pBlock->setDisplayFrame(pFrame);
    //m_arrBlockItem[iRow][iCol].pBlock->SetBlockType(static_cast<BLOCK_TYPE>(iType));
    
    CCAction* pAction = m_arrBlockItem[iRow][iCol].pBlock->getActionByTag(FINAL_BLOCK_FADEOUT_ACTION_TAG);
    if (pAction != NULL)
        m_arrBlockItem[iRow][iCol].pBlock->stopAction(pAction);
    m_arrBlockItem[iRow][iCol].pBlock->setScale(1.0f);
    
    m_arrBlockItem[iRow][iCol].pBlock->setIsVisible(true);
    m_arrBlockItem[iRow][iCol].pBlock->setOpacity(255);
}