//
//  Sprite_Cube.cpp
//  Test
//
//  Created by 惠伟 孙 on 12-6-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Sprite_Cube.h"

void CGAffineToGL(const CCAffineTransform *t, GLfloat *m)
{
	// | m[0] m[4] m[8]  m[12] |     | m11 m21 m31 m41 |     | a c 0 tx |
	// | m[1] m[5] m[9]  m[13] |     | m12 m22 m32 m42 |     | b d 0 ty |
	// | m[2] m[6] m[10] m[14] | <=> | m13 m23 m33 m43 | <=> | 0 0 1  0 |
	// | m[3] m[7] m[11] m[15] |     | m14 m24 m34 m44 |     | 0 0 0  1 |
	
	m[2] = m[3] = m[6] = m[7] = m[8] = m[9] = m[11] = m[14] = 0.0f;
	m[10] = m[15] = 1.0f;
	m[0] = t->a; m[4] = t->c; m[12] = t->tx;
	m[1] = t->b; m[5] = t->d; m[13] = t->ty;
}

/************************************************************************/
#pragma mark - Sprite_Cube类静态函数 : 立方体初始化函数
/************************************************************************/
Sprite_Cube* Sprite_Cube::cubeWithSpriteFrameName(const char *filename)
{
    Sprite_Cube* pCube = new Sprite_Cube();
    if (pCube != NULL)
    {
        if (pCube->initWithSpriteFrameName(filename) == true)
        {
            pCube->m_pCubeFrame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename);
            //pCube->m_pSideFrame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("side.png");
            
            //pCube->InitCubeOhterface();
            pCube->autorelease();
            return pCube;
        }
    }
    
    delete pCube;
    pCube = NULL;
    
    return NULL;
}

/************************************************************************/
#pragma mark - Sprite_Cube类初始化方块纹理桢函数
/************************************************************************/
void Sprite_Cube::InitBlockFrame(CCSpriteFrame* pFrame)
{
    m_pBlockFrame = pFrame;
}

/************************************************************************/
#pragma mark - Sprite_Cube类初始化立方体纹理函数
/************************************************************************/
void Sprite_Cube::InitCubeFrame(CCSpriteFrame* pFrame)
{
    m_pCubeFrame = pFrame;
}

/************************************************************************/
#pragma mark - Sprite_Cube类重写位置设置函数
/************************************************************************/
//void Sprite_Cube::setPosition(const CCPoint& pos)
//{
//    m_oriPosition = pos;
//	if (CC_CONTENT_SCALE_FACTOR() == 1)
//		m_Position = m_oriPosition;
//	else
//		m_Position = ccpMult(m_oriPosition, CC_CONTENT_SCALE_FACTOR());
//}

/************************************************************************/
#pragma mark - Sprite_Cube类构造函数
/************************************************************************/
Sprite_Cube::Sprite_Cube()
{
    // 为立方体对象变量赋初值
    m_pCubeFrame = NULL;
    m_pBlockFrame = NULL;
    m_pSideFrame = NULL;
    
    // 为立方体属性变量赋初值
    m_Position.x = m_Position.y = 0.0f;
    
    m_fRotationX = m_fRotationY = m_fRotationZ = 0.0f;
    
    m_fThickness = 4.0f * CC_CONTENT_SCALE_FACTOR();
    
    m_iBlockType = -1;
    
    m_bIsHorizonDraw = false;
    m_bIsVeticalDraw = false;
    
    m_bAlpha = 255;
    
    m_stVertexColor = ccc4(255, 255, 255, 255);
    
    memset(m_CubeSideVertices, 0, sizeof(ccV3F_C4B_T2F) * 4);
    memset(m_CubeBottomVertices, 0, sizeof(ccV3F_C4B_T2F) * 4);
}

/************************************************************************/
#pragma mark - Sprite_Cube类析构函数
/************************************************************************/
Sprite_Cube::~Sprite_Cube()
{
    
}

/************************************************************************/
#pragma mark - Sprite_Cube类重写函数 : 访问函数
/************************************************************************/
void Sprite_Cube::visit()
{
//    if (m_fRotationX < 360.0f)
//        m_fRotationX += 0.5f;
//    else
//        m_fRotationX = 0.0f;
    
    // 因为用到3D,避免盖住其他图层，关闭深度测试
    glDisable(GL_DEPTH_TEST);
    
    // 绘制自身
    CubeVisit();
    
    // 得到原位置
    CCPoint pos = getPosition();
    
    // 绘制水平临时立方体
    if (m_bIsHorizonDraw)
    {
        setPosition(ccp(pos.x - 53.0f * 6, pos.y));
        CubeVisit();
        
        setPosition(ccp(pos.x + 53.0f * 6, pos.y));
        CubeVisit();
    }
    
    // 绘制竖直临时立方体
    if (m_bIsVeticalDraw)
    {
        setPosition(ccp(pos.x, pos.y - 53.0f * 5));
        CubeVisit();
        
        setPosition(ccp(pos.x, pos.y + 53.0f * 5));
        CubeVisit();
    }
    
    setPosition(pos);
    
    // 还原深度测试
    glEnable(GL_DEPTH_TEST);
}

/************************************************************************/
#pragma mark - Sprite_Cube类重写函数 : 绘制函数
/************************************************************************/
void Sprite_Cube::draw()
{
    CCNode::draw();
    
	CCAssert(! m_bUsesBatchNode, "");
    
	// Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
	// Needed states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
	// Unneeded states: -
	bool newBlend = m_sBlendFunc.src != CC_BLEND_SRC || m_sBlendFunc.dst != CC_BLEND_DST;
	if (newBlend)
		glBlendFunc(m_sBlendFunc.src, m_sBlendFunc.dst);
    
    // 在旋转到背面的时候，使不可见
    if (m_fRotationX > 90.0f && m_fRotationX < 270.0f)
    {
        glBindTexture(GL_TEXTURE_2D, 0);
        
        m_stVertexColor = ccc4(100, 100, 100, m_bAlpha);
    }
    else
    {
        if (m_pobTexture)
            glBindTexture(GL_TEXTURE_2D, m_pobTexture->getName());
        else
            glBindTexture(GL_TEXTURE_2D, 0);
        
        m_stVertexColor = ccc4(255, 255, 255, m_bAlpha);
    }
    
    // 更新顶点颜色
    m_sQuad.tl.colors = m_stVertexColor;
    m_sQuad.tr.colors = m_stVertexColor;
    m_sQuad.bl.colors = m_stVertexColor;
    m_sQuad.br.colors = m_stVertexColor;
    
	long offset = (long)&m_sQuad;
    
	// vertex
	int diff = offsetof(ccV3F_C4B_T2F, vertices);
	glVertexPointer(3, GL_FLOAT, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
    
	// color
	diff = offsetof( ccV3F_C4B_T2F, colors);
	glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
	
	// tex coords
	diff = offsetof( ccV3F_C4B_T2F, texCoords);
	glTexCoordPointer(2, GL_FLOAT, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
	
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	
	if(newBlend)
		glBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
}

/************************************************************************/
#pragma mark - Sprite_Cube类重写访问函数
/************************************************************************/
/*
void Sprite_Cube::visit()
{
    // 不可见，则跳过
    if (!m_bIsVisible)
		return;
    
    // test
//    if (a == 0)
//    {
//        if (m_fRotationY < 90.0f)
//            m_fRotationY += 2.0f;
//        else
//        {
//            a = 1;
//            m_fRotationY = -90.0f;
//        }
//    }
//    else if (a == 1)
//    {
//        if (m_fRotationY < 0.0f)
//            m_fRotationY += 2.0f;
//        else
//        {
//            a = -1;
//        }
//    }
//    if (m_fRotationX < 360.0f)
//        m_fRotationX += 2.0;
//    else
//        m_fRotationX = 0.0f;
//    if (a == 0)
//    {
//        if (m_fRotationY < 180.0f)
//            m_fRotationY += 2.0f;
//        else
//        {
//            m_fRotationY = 0.0f;
//            a = -1;
//        }
//    }
    
    // 关闭深度测试，避免与其他图形产生遮挡
    //glDisable(GL_DEPTH_TEST);
    
    // 压入世界变换矩阵
	glPushMatrix();
    
    // 设置移动和旋转矩阵
    glTranslatef(m_Position.x, m_Position.y + 2, 0.0f);
    glRotatef(m_fRotationX, -1.0f, 0.0f, 0.0f);
    
    // cocos内部转换
    transform();
    
    // 绘制该精灵
    draw();
    
    // 弹出世界变换矩阵
    glPopMatrix();
    
    // 绘制横向
    if (m_bIsHorizonDraw)
    {
        glPushMatrix();
        
        glTranslatef(m_Position.x - 6 * 53.0f * CC_CONTENT_SCALE_FACTOR(), m_Position.y + 2, 0.0f);
        glRotatef(m_fRotationY, 0.0f, 1.0f, 0.0f);
        
        transform();
        
        draw();
        
        glPopMatrix();
        
        glPushMatrix();
        
        glTranslatef(m_Position.x + 6 * 53.0f * CC_CONTENT_SCALE_FACTOR(), m_Position.y + 2, 0.0f);
        glRotatef(m_fRotationY, 0.0f, 1.0f, 0.0f);
        
        transform();
        
        draw();
        
        glPopMatrix();
    }
    
    // 绘制竖向
    if (m_bIsVeticalDraw)
    {
        glPushMatrix();
        
        glTranslatef(m_Position.x, m_Position.y - 5 * 53.0f * CC_CONTENT_SCALE_FACTOR() + 2, 0.0f);
        glRotatef(m_fRotationY, 0.0f, 1.0f, 0.0f);
        
        transform();
        
        draw();
        
        glPopMatrix();
        
        glPushMatrix();
        
        glTranslatef(m_Position.x, m_Position.y + 5 * 53.0f * CC_CONTENT_SCALE_FACTOR() + 2, 0.0f);
        glRotatef(m_fRotationY, 0.0f, 1.0f, 0.0f);
        
        transform();
        
        draw();
        
        glPopMatrix();
    }
    
    // 还原 : 打开深度测试
    //glEnable(GL_DEPTH_TEST);
}
*/

/************************************************************************/
#pragma mark - Sprite_Cube类重写绘制函数
/************************************************************************/
/*
void Sprite_Cube::draw()
{
    // 是否要进行alpha混合
	bool newBlend = m_sBlendFunc.src != CC_BLEND_SRC || m_sBlendFunc.dst != CC_BLEND_DST;
//	if (newBlend)
//		glBlendFunc(m_sBlendFunc.src, m_sBlendFunc.dst);
    
    // 设置立方体纹理
    glBindTexture(GL_TEXTURE_2D, 0);
    
    CCSize size = getContentSize();
    
    size.width *= CC_CONTENT_SCALE_FACTOR();
    size.height *= CC_CONTENT_SCALE_FACTOR();
    
// -----------------------------------------绘制侧面-----------------------------------------
    
    // 获得侧面顶点内存地址
    long offset = (long)&m_CubeSideVertices;
    
	// 设置顶点位置
	int diff = offsetof(ccV3F_C4B_T2F, vertices);
	glVertexPointer(3, GL_FLOAT, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
    
	// 设置顶点颜色
	diff = offsetof( ccV3F_C4B_T2F, colors);
	glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
	
	// 设置纹理坐标
	diff = offsetof( ccV3F_C4B_T2F, texCoords);
	glTexCoordPointer(2, GL_FLOAT, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
    
    // 绘制右侧面
    glTranslatef(0.0f, 0.0f, 0.0f);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    // 绘制左侧面
    glTranslatef(-size.width, 0.0f, 0.0f);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    // 还原位移
    glTranslatef(size.width, 0.0f, 0.0f);
    
// -----------------------------------------绘制底面-----------------------------------------
    
    // 获得底面顶点内存地址
    offset = (long)&m_CubeBottomVertices;
    
    // 设置顶点位置
    diff = offsetof(ccV3F_C4B_T2F, vertices);
	glVertexPointer(3, GL_FLOAT, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
    
	// 设置顶点颜色
	diff = offsetof( ccV3F_C4B_T2F, colors);
	glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
	
	// 设置纹理坐标
	diff = offsetof( ccV3F_C4B_T2F, texCoords);
	glTexCoordPointer(2, GL_FLOAT, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
    
    // 绘制底面
    glTranslatef(0.0f, 0.0f, 0.0f);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    // 绘制顶面
    glTranslatef(0.0f, size.height, 0.0f);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    // 还原位移
    glTranslatef(0.0f, -size.height, 0.0f);
	
    // 还原alpha混合
	if(newBlend)
		glBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
    
// -----------------------------------------绘制前面-----------------------------------------
    
    // 获得前面顶点内存地址 : 使用原精灵顶点
    offset = (long)&m_sQuad;
    
	// 设置顶点位置
    diff = offsetof(ccV3F_C4B_T2F, vertices);
	glVertexPointer(3, GL_FLOAT, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
    
	// 设置顶点颜色
	diff = offsetof( ccV3F_C4B_T2F, colors);
	glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
	
	// 设置纹理坐标
	diff = offsetof( ccV3F_C4B_T2F, texCoords);
	glTexCoordPointer(2, GL_FLOAT, sizeof(ccV3F_C4B_T2F), (void*)(offset + diff));
    
//    if (m_pCubeFrame == NULL)
//    {
//        glBindTexture(GL_TEXTURE_2D, 0);
//    }
//    else
//    {
//        setDisplayFrame(m_pCubeFrame);
//        
//        if (m_pobTexture != NULL)
//            glBindTexture(GL_TEXTURE_2D, m_pobTexture->getName());
//        else
//            glBindTexture(GL_TEXTURE_2D, 0);
//    }
    
    // 设置立方体纹理
    if (m_pobTexture != NULL)
        glBindTexture(GL_TEXTURE_2D, m_pobTexture->getName());
    else
        glBindTexture(GL_TEXTURE_2D, 0);
    
    ccColor4B color = ccc4(m_bAlpha, m_bAlpha, m_bAlpha, m_bAlpha);
    m_sQuad.tl.colors = color;
    m_sQuad.tr.colors = color;
    m_sQuad.bl.colors = color;
    m_sQuad.br.colors = color;
    
    // 绘制前面
    glTranslatef(0.0f, 0.0f, m_fThickness);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    // 绘制后面
    color = ccc4(100, 100, 100, 255);
    m_sQuad.tl.colors = color;
    m_sQuad.tr.colors = color;
    m_sQuad.bl.colors = color;
    m_sQuad.br.colors = color;
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glTranslatef(0.0f, 0.0f, -m_fThickness * 2.0f);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
//    if (m_pBlockFrame != NULL)
//    {
//        // 设置方块纹理
//        setDisplayFrame(m_pBlockFrame);
//        
//        color = ccc4(255, 255, 255, 255);
//        m_sQuad.tl.colors = color;
//        m_sQuad.tr.colors = color;
//        m_sQuad.bl.colors = color;
//        m_sQuad.br.colors = color;
//        
//        // 绑定纹理
//        if (m_pobTexture != NULL)
//            glBindTexture(GL_TEXTURE_2D, m_pobTexture->getName());
//        else
//            glBindTexture(GL_TEXTURE_2D, 0);
//        
//        glTranslatef(0.0f, 0.0f, m_fThickness * 2.0f + 1.0f);
//        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
//    }
    
    if(newBlend)
		glBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
}
*/

/************************************************************************/
#pragma mark - 初始化侧面函数
/************************************************************************/
/*
void Sprite_Cube::InitCubeOhterface()
{
    // 获得纹理尺寸
    CCSize size = getContentSize();
    
    size.width *= CC_CONTENT_SCALE_FACTOR();
    size.height *= CC_CONTENT_SCALE_FACTOR();
    
    ccColor4B color = ccc4(100, 100, 100, 255);
    
    // 设置侧面顶点信息
    m_CubeSideVertices[0].vertices = vertex3(size.width, size.height, m_fThickness);
    m_CubeSideVertices[1].vertices = vertex3(size.width, 0.0f, m_fThickness);
    m_CubeSideVertices[2].vertices = vertex3(size.width, size.height, -m_fThickness);
    m_CubeSideVertices[3].vertices = vertex3(size.width, 0.0f, -m_fThickness);
    
    m_CubeSideVertices[0].colors = color;
    m_CubeSideVertices[1].colors = color;
    m_CubeSideVertices[2].colors = color;
    m_CubeSideVertices[3].colors = color;
    
    m_CubeSideVertices[0].texCoords = m_sQuad.tl.texCoords;
    m_CubeSideVertices[1].texCoords = m_sQuad.bl.texCoords;
    m_CubeSideVertices[2].texCoords = m_sQuad.tr.texCoords;
    m_CubeSideVertices[3].texCoords = m_sQuad.br.texCoords;
    
    // 设置底面顶点信息
    m_CubeBottomVertices[0].vertices = vertex3(0.0f, 0.0f, m_fThickness);
    m_CubeBottomVertices[1].vertices = vertex3(0.0f, 0.0f, -m_fThickness);
    m_CubeBottomVertices[2].vertices = vertex3(size.width, 0.0f, m_fThickness);
    m_CubeBottomVertices[3].vertices = vertex3(size.width, 0.0f, -m_fThickness);
    
    m_CubeBottomVertices[0].colors = color;
    m_CubeBottomVertices[1].colors = color;
    m_CubeBottomVertices[2].colors = color;
    m_CubeBottomVertices[3].colors = color;
    
    m_CubeBottomVertices[0].texCoords = m_sQuad.tl.texCoords;
    m_CubeBottomVertices[1].texCoords = m_sQuad.bl.texCoords;
    m_CubeBottomVertices[2].texCoords = m_sQuad.tr.texCoords;
    m_CubeBottomVertices[3].texCoords = m_sQuad.br.texCoords;
}
*/

/************************************************************************/
#pragma mark - Sprite_Cube类立方体访问函数
/************************************************************************/
void Sprite_Cube::CubeVisit()
{
    // 将变换矩阵压入堆栈
    glPushMatrix();
    
    // 重置变换矩阵
    m_tTransform = CCAffineTransformIdentity;
    
    // 变换位置
    glTranslatef(m_tPositionInPixels.x, m_tPositionInPixels.y, 0.0f);
    // 旋转角度
    glRotatef(m_fRotationX, -1.0f, 0.0f, 0.0f);
    glRotatef(m_fRotationY, 0.0f, 1.0f, 0.0f);
    
    // 对齐到铆点
    if(CCPoint::CCPointEqualToPoint(m_tAnchorPointInPixels, CCPointZero) == false)
        m_tTransform = CCAffineTransformTranslate(m_tTransform, -m_tAnchorPointInPixels.x, -m_tAnchorPointInPixels.y);
    
    // 对齐到opengl空间
    CGAffineToGL(&m_tTransform, m_pTransformGL);
    glMultMatrixf(m_pTransformGL);
    
    // 绘制精灵
    draw();
    
    // 将变换矩阵弹出堆栈
    glPopMatrix();
}