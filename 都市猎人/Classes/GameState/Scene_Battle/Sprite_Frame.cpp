//
//  Sprite_Frame.cpp
//  Test
//
//  Created by 惠伟 孙 on 12-6-15.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Sprite_Frame.h"

/************************************************************************/
#pragma mark - Sprite_Frame类静态函数 : 创建外框对象
/************************************************************************/
Sprite_Frame* Sprite_Frame::frameWithSpriteFrameName(const char* filename)
{
    Sprite_Frame* pFrame = new Sprite_Frame();
    if (pFrame != NULL)
    {
        if (pFrame->initWithSpriteFrameName(filename) == true)
        {
            pFrame->autorelease();
            return pFrame;
        }
    }
    
    delete pFrame;
    pFrame = NULL;
    
    return NULL;
}

/************************************************************************/
#pragma mark - Sprite_Frame类构造函数
/************************************************************************/
Sprite_Frame::Sprite_Frame()
{
    m_bIsHorizonDraw = false;
    m_bIsVeticalDraw = false;
}

/************************************************************************/
#pragma mark - Sprite_Frame类析构函数
/************************************************************************/
Sprite_Frame::~Sprite_Frame()
{
    
}

/************************************************************************/
#pragma mark - Sprite_Frame类重写函数 : 访问函数
/************************************************************************/
void Sprite_Frame::visit()
{
    // 关闭深度缓冲
    //glDisable(GL_DEPTH_TEST);
    
    // 调用基类访问函数
    CCSprite::visit();
    
    CCPoint pos = getPosition();
    
    if (m_bIsHorizonDraw)
    {
        setPosition(ccp(pos.x - 53.0f * 6, pos.y));
        CCSprite::visit();
        
        setPosition(ccp(pos.x + 53.0f * 6, pos.y));
        CCSprite::visit();
    }
    
    if (m_bIsVeticalDraw)
    {
        setPosition(ccp(pos.x, pos.y - 53.0f * 5));
        CCSprite::visit();
        
        setPosition(ccp(pos.x, pos.y + 53.0f * 5));
        CCSprite::visit();
    }
    
    setPosition(pos);
    
    // 打开深度缓冲
    //glEnable(GL_DEPTH_TEST);
}