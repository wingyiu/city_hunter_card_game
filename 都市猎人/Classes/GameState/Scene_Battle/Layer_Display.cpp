//
//  Layer_Display.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-6-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_Display.h"
USING_NS_CC;

#include "Enemy.h"
#include "Battle_Enemy.h"
#include "Layer_Enemy.h"

#include "Follower.h"
#include "Battle_Follower.h"
#include "Layer_Follower.h"
#include "Layer_BattleMessage.h"

#include "Layer_Block.h"

#include "Action_MyMove.h"

#include "GameController.h"
#include "SystemDataManage.h"

Layer_Display * Layer_Display::m_Instance = NULL;

/************************************************************************/
#pragma mark 单例
/************************************************************************/
Layer_Display * Layer_Display::ShareInstance()
{
    if(m_Instance == NULL)
    {
        return NULL;
    }
    return m_Instance;
}
/************************************************************************/
#pragma mark -
/************************************************************************/

Layer_Display::~Layer_Display()
{
    m_Instance = NULL;
    
    printf("Layer_Display:: 释放完成\n");
}

/************************************************************************/
#pragma mark 初始化
/************************************************************************/
bool Layer_Display::init()
{
    if(! CCLayer::init())
        return false;
    
    m_WinSize = CCDirector::sharedDirector() -> getWinSize();
    
    m_Round = 0;
    m_StepRecord = 1;
    
    m_BattleAllAttack = 0;
    m_BattleMaxAttack = 0;
    
    m_Count = 0;
    
    m_SA_Icon1 = NULL;
    m_SA_Icon1_1 = NULL;
    m_SA_Icon2 = NULL;
    m_SA_Icon2_1 = NULL;
    m_SA_Icon3Left = NULL;
    m_SA_Icon3Right = NULL;
    
    //Combo数字大小
    CCSprite * pSprite1 = CCSprite::spriteWithFile("ComboNum-hd.png");
    m_NumSize_Combo = CCSizeMake(pSprite1 -> getTextureRect().size.width / 12, pSprite1 -> getTextureRect().size.height);
    //Great数字大小
    CCSprite * pSprite2 = CCSprite::spriteWithFile("GreatNum-hd.png");
    m_NumSize_Great = CCSizeMake(pSprite2 -> getTextureRect().size.width / 12, pSprite2 -> getTextureRect().size.height);
    //Perfect数字大小
    CCSprite * pSprite3 = CCSprite::spriteWithFile("PerfectNum-hd.png");
    m_NumSize_Perfect = CCSizeMake(pSprite3 -> getTextureRect().size.width / 12, pSprite3 -> getTextureRect().size.height);
    
    //减血数字大小
    CCSprite * pSprite4 = CCSprite::spriteWithFile("Number_DecreaseHP-hd.png");
    m_NumSize_DecreaseHP = CCSizeMake(pSprite4 -> getTextureRect().size.width / 12, pSprite4 -> getTextureRect().size.height);
    
    //呼叫者
    m_Caller = NULL;
    
    for(int i = 0; i < 4; i ++)
    {
        //////////////颜色//////////////////
        int deep = 150;
        CCSize RTSize = CCSizeMake(640, 960);
        CCRenderTexture * RT = CCRenderTexture::renderTextureWithWidthAndHeight(RTSize.width / 2, RTSize.height / 2);
        RT -> beginWithClear(255, 0, 0, 0);
        
        glDisable(GL_TEXTURE_2D);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        
        CCPoint vertices[16];
        ccColor4F colors[16];
        int nVertices = 0;
        GLfloat inner = 0.8f;
        GLfloat outer = 0.1f;
        
        //1
        vertices[nVertices] = CCPointMake(0, 0);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, inner};
        //2
        vertices[nVertices] = CCPointMake(0 + deep, 0 + deep);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, outer};
        //3
        vertices[nVertices] = CCPointMake(RTSize.width / 2, 0);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, inner};
        //4
        vertices[nVertices] = CCPointMake(RTSize.width - deep, 0 + deep);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, outer};
        //5
        vertices[nVertices] = CCPointMake(RTSize.width, 0);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, inner};
        //6
        vertices[nVertices] = CCPointMake(RTSize.width - deep, RTSize.height / 2);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, outer};
        //7
        vertices[nVertices] = CCPointMake(RTSize.width, RTSize.height);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, inner};
        //8
        vertices[nVertices] = CCPointMake(RTSize.width - deep, RTSize.height - deep);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, outer};
        //9
        vertices[nVertices] = CCPointMake(RTSize.width / 2, RTSize.height);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, inner};
        //10
        vertices[nVertices] = CCPointMake(RTSize.width / 2, RTSize.height - deep);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, outer};
        //11
        vertices[nVertices] = CCPointMake(0, RTSize.height);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, inner};
        //12
        vertices[nVertices] = CCPointMake(0 + deep, RTSize.height - deep);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, outer};
        //13
        vertices[nVertices] = CCPointMake(0, RTSize.height / 2);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, inner};
        //14
        vertices[nVertices] = CCPointMake(0 + deep, RTSize.height / 2);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, outer};
        //15
        vertices[nVertices] = CCPointMake(0, 0);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, inner};
        //16
        vertices[nVertices] = CCPointMake(0 + deep,  0 + deep);
        colors[nVertices ++] = (ccColor4F){0, 0, 0, outer};
        
        glVertexPointer(2, GL_FLOAT, 0, vertices);
        glColorPointer(4, GL_FLOAT, 0, colors);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)nVertices);
        
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glEnable(GL_TEXTURE_2D);
        
        RT -> end();
        ////////////////////////////////////////////////
        
        CCSprite * pSprite = CCSprite::spriteWithTexture(RT -> getSprite() -> getTexture());
        pSprite -> setOpacity(0);
        pSprite -> setIsVisible(false);
        pSprite -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2));
        
        if(i == 0)
            this -> addChild(pSprite, 0, Layer_DisplayChild_HurtMask1);
        else if(i == 1)
            this -> addChild(pSprite, 0, Layer_DisplayChild_HurtMask2);
        else if(i == 2)
            this -> addChild(pSprite, 0, Layer_DisplayChild_HurtMask3);
        else if(i == 3)
            this -> addChild(pSprite, 0, Layer_DisplayChild_HurtMask4);
    }

    m_Instance = this;
    
    return true;
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 退出释放
/************************************************************************/
void Layer_Display::onExit()
{
    CCLayer::onExit();
    
    this -> removeAllChildrenWithCleanup(true);
    
    m_Caller = NULL;
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 禁止操作
/************************************************************************/
void Layer_Display::OperationEnable(bool enable)
{
    if(! enable)
    {
        Layer_Block::ShareInstance() -> SetLayerAlpha(true);
        Layer_Block::ShareInstance() -> SetOperationEnable(false);
        Layer_Enemy::ShareInstance() -> SetOperationEnable(false);
        Layer_Follower::ShareInstance() -> SetOperationEnable(false);
    }else
    {
        Layer_Block::ShareInstance() -> SetLayerAlpha(false);
        Layer_Block::ShareInstance() -> SetOperationEnable(true);
        Layer_Enemy::ShareInstance() -> SetOperationEnable(true);
        Layer_Follower::ShareInstance() -> SetOperationEnable(true);
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 显示出Combo动画
/************************************************************************/
void Layer_Display::DisplayCombo(CCPoint point, int combo)
{
    const char * IconName = NULL;
    const char * NumName = NULL;
    CCSize NumSize = CCSizeMake(0, 0);
    
    if(combo >= 1 && combo <= 4)
    {
        IconName = "ComboIcon.png";
        NumName = "ComboNum-hd.png";
        NumSize = m_NumSize_Combo;
    }else if(combo >= 5 && combo <= 9)
    {
        IconName = "GreatIcon.png";
        NumName = "GreatNum-hd.png";
        NumSize = m_NumSize_Great;
    }else if(combo >= 10)
    {
        IconName = "PerfectIcon.png";
        NumName = "PerfectNum-hd.png";
        NumSize = m_NumSize_Perfect;
    }
    
    //Combo图标
    CCSprite * ComboIcon = CCSprite::spriteWithSpriteFrameName(IconName);
    ComboIcon -> setPosition(ccp(0 - ComboIcon -> getTextureRect().size.width / 2, point.y));

    //Combo的数量
    char buff[16];
    sprintf(buff, "%d", combo);
    CCLabelAtlas * ComboNum = CCLabelAtlas::labelWithString(buff, NumName, NumSize.width, NumSize.height, '.');
    ComboNum -> setPosition(ccp(320 + ComboIcon -> getTextureRect().size.width / 2, point.y - 18));
    ComboNum -> setAnchorPoint(ccp(0.5, 0.5));
    
    //延时淡出
    CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(0.4f);
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.4f);
    CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayComboCallBack2));
    
    //Combo图标移动
    CCMoveTo * pMoveTo1 = CCMoveTo::actionWithDuration(0.2f, ccp(110, point.y));
    CCEaseIn * ComboIconMove = CCEaseIn::actionWithAction(pMoveTo1, 3.0f);
    ComboIcon -> runAction(CCSequence::actions(ComboIconMove, CCSequence::actions(pDelayTime, pFadeOut, NULL), pCallFuncN, NULL));
    
    //Combo的数量MoveTo
    CCMoveTo * pMoveTo2 = CCMoveTo::actionWithDuration(0.2f, ccp(225, point.y + 7));
    CCEaseIn * ComboNumMove = CCEaseIn::actionWithAction(pMoveTo2, 3.0f);
    CCCallFuncN * pDisplayComboCallBack1 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayComboCallBack1));
    ComboNum -> runAction(CCSequence::actions(ComboNumMove, pDisplayComboCallBack1, NULL));
    
    this -> addChild(ComboIcon);
    this -> addChild(ComboNum);
}

void Layer_Display::DisplayComboCallBack1(CCNode * sender)
{
    //Combo的数量放大缩小
    CCScaleTo * pScaleToBig = CCScaleTo::actionWithDuration(0.4f, 4.0f);
    CCEaseOut * pBig = CCEaseOut::actionWithAction(pScaleToBig, 10.0f);
    CCScaleTo * pScaleToSmall = CCScaleTo::actionWithDuration(0.4f, 1.0f);
    CCEaseOut * pSmall = CCEaseOut::actionWithAction(pScaleToSmall, 10.0f);
    CCCallFuncN * pDisplayComboCallBack2 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayComboCallBack2));
    
    sender -> runAction(CCSequence::actions(pBig, pSmall, pDisplayComboCallBack2, NULL));
}

void Layer_Display::DisplayComboCallBack2(CCNode * sender)
{
    this -> removeChild(sender, true);
}

void Layer_Display::DisplayComboCallBack3(CCNode * sender)
{
    
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示出小弟受击的动画 血条飘出被伤害的数字
/************************************************************************/
void Layer_Display::DisplayFollowerHurt(CCPoint point, int hurt)
{
    CCSprite * pJianHao = CCSprite::spriteWithSpriteFrameName("JianHao.png");

    char buff[32];
    sprintf(buff, "%d", hurt);
    CCLabelAtlas * pLabelAtlas = CCLabelAtlas::labelWithString(buff, "Number_DecreaseHP-hd.png", m_NumSize_DecreaseHP.width, m_NumSize_DecreaseHP.height, '.');
    
    CCSprite * pSprite = static_cast<CCSprite*>(Layer_Follower::ShareInstance() -> getChildByTag(LFChildTag_HP));
    CCPoint pPoint1 = CCPointMake(point.x + pSprite -> getTextureRect().size.width, point.y);
    CCPoint pPoint2 = CCPointMake(point.x + pSprite -> getTextureRect().size.width + 8, point.y - m_NumSize_DecreaseHP.height / 2);
    
    pJianHao -> setPosition(pPoint1);
    pLabelAtlas -> setPosition(pPoint2);
    
    CCCallFuncN * pDisplayFollowerHurtCallBack1 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayFollowerHurtCallBack1));
    ////////////////////////
    CCMoveTo * pMoveTo1 = CCMoveTo::actionWithDuration(1.2f, ccp(pPoint1.x, pPoint1.y + 90));
    CCEaseOut * pEaseOut1 = CCEaseOut::actionWithAction(pMoveTo1, 1.7f);
    CCFadeOut * pFadeOut1 = CCFadeOut::actionWithDuration(1.5f);
    CCFiniteTimeAction * pSequence1 = CCSequence::actions(pFadeOut1, pDisplayFollowerHurtCallBack1, NULL);
    
    pJianHao -> runAction(CCSpawn::actions(pEaseOut1, pSequence1, NULL));
    this -> addChild(pJianHao);
    ////////////////////////
    CCMoveTo * pMoveTo2 = CCMoveTo::actionWithDuration(1.2f, ccp(pPoint2.x, pPoint2.y + 90));
    CCEaseOut * pEaseOut2 = CCEaseOut::actionWithAction(pMoveTo2, 1.7f);
    CCFadeOut * pFadeOut2 = CCFadeOut::actionWithDuration(1.5f);
    CCFiniteTimeAction * pSequence2 = CCSequence::actions(pFadeOut2, pDisplayFollowerHurtCallBack1, NULL);
    
    pLabelAtlas -> runAction(CCSpawn::actions(pEaseOut2, pSequence2, NULL));
    this -> addChild(pLabelAtlas);
}

void Layer_Display::DisplayFollowerHurtCallBack1(CCNode * sender)
{
    this -> removeChild(sender, true);
}
/************************************************************************/
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 小弟被攻击时的全屏效果
/************************************************************************/
void Layer_Display::DisplayFollowerBeAttack()
{
    CCSprite * pSprite1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Layer_DisplayChild_HurtMask1));
    CCSprite * pSprite2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Layer_DisplayChild_HurtMask2));
    CCSprite * pSprite3 = dynamic_cast<CCSprite*>(this -> getChildByTag(Layer_DisplayChild_HurtMask3));
    CCSprite * pSprite4 = dynamic_cast<CCSprite*>(this -> getChildByTag(Layer_DisplayChild_HurtMask4));
    
    if(! pSprite1)  return;
    
    CCFadeIn * pFadeIn = CCFadeIn::actionWithDuration(0.05f);
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.15f);
    CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::FlashOver));
    CCFiniteTimeAction * pAction = CCSequence::actions(pFadeIn, pFadeOut, pCallFuncN, NULL);
    pAction -> setTag(ActionTag_BaAttackFlash);
    
    if(! pSprite1 -> getActionByTag(ActionTag_BaAttackFlash))
    {
        pSprite1 -> setIsVisible(true);
        pSprite1 -> runAction(dynamic_cast<CCFiniteTimeAction*>(pAction -> copy()));
    }
    else if(! pSprite2 -> getActionByTag(ActionTag_BaAttackFlash))
    {
        pSprite2 -> setIsVisible(true);
        pSprite2 -> runAction(dynamic_cast<CCFiniteTimeAction*>(pAction -> copy()));
    }
    else if(! pSprite3 -> getActionByTag(ActionTag_BaAttackFlash))
    {
        pSprite3 -> setIsVisible(true);
        pSprite3 -> runAction(dynamic_cast<CCFiniteTimeAction*>(pAction -> copy()));
    }
    else if(! pSprite4 -> getActionByTag(ActionTag_BaAttackFlash))
    {
        pSprite4 -> setIsVisible(true);
        pSprite4 -> runAction(dynamic_cast<CCFiniteTimeAction*>(pAction -> copy()));
    }
    
    if(! this -> getChildByTag(Layer_DisplayChild_HurtMask5))
    {
        int r = rand() % 3 + 1;
        char buff[32];
        sprintf(buff, "FollowerBaAttack%d.png", r);
        
        CCSprite * pBreak = CCSprite::spriteWithSpriteFrameName(buff);
        pBreak -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2));
        
        Action_MyMove * pMyMove = Action_MyMove::actionWithDuration(6, 8, 0.004f);
        pMyMove -> setTag(ActionTag_BreakShake);
        pBreak -> runAction(pMyMove);
        this -> schedule(schedule_selector(Layer_Display::DisplayFollowerBeAttackOver));
        
        this -> addChild(pBreak, 0, Layer_DisplayChild_HurtMask5);
    }
}

void Layer_Display::FlashOver(CCNode * sender)
{
    sender -> setIsVisible(false);
}

void Layer_Display::DisplayFollowerBeAttackOver()
{
    CCSprite * pSprite = dynamic_cast<CCSprite*>(this -> getChildByTag(Layer_DisplayChild_HurtMask5));
    
    if(! pSprite -> getActionByTag(ActionTag_BreakShake))
    {
        this -> unschedule(schedule_selector(Layer_Display::DisplayFollowerBeAttackOver));
        
        CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(1.0f);
        CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayFollowerBeAttackOverFunc1));
        pSprite -> runAction(CCSequence::actions(pFadeOut, pCallFuncN, NULL));
    }
}

void Layer_Display::DisplayFollowerBeAttackOverFunc1(CCNode * sender)
{
    this -> removeChild(sender, true);
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示消掉方块后飘动的球
/************************************************************************/
void Layer_Display::DisplayFollowerStartAttack(int profession, CCPoint startpoint)
{
    //如果是加血的
    if(profession == BLOCK_TYPE4_HP)
    {
        //球
        CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("AddHpBall.png");
        pSprite -> setPosition(startpoint);
        
        CCPoint destPoint = CCPointMake(m_WinSize.width / 2, Layer_Follower::ShareInstance() -> GetHpPoint().y);
        
        //运动曲线
        ccBezierConfig pBezierConfig;
        pBezierConfig.controlPoint_1 = startpoint;
        pBezierConfig.controlPoint_2 = CCPointMake(rand() % 320, startpoint.y + rand() % (int)(destPoint.y - startpoint.y));
        pBezierConfig.endPosition = destPoint;
        CCBezierTo * pBezierTo = CCBezierTo::actionWithDuration(0.6f, pBezierConfig);
        
        CCCallFuncN * pAddHpBallArrival = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::AddHpBallArrival));
        
        pSprite -> runAction(CCSequence::actions(pBezierTo, pAddHpBallArrival, NULL));
        this -> addChild(pSprite);
    }
    //如果是攻击系的
    else 
    {
        char buff[16];
        sprintf(buff, "AttackBall%d.png", profession);
        
        Battle_FollowerList pList = *Layer_Follower::ShareInstance() -> GetFollowerList();
        for(Battle_FollowerList::iterator it = pList.begin(); it != pList.end(); ++ it)
        {
            if(it -> second -> GetHost() -> GetData() -> Follower_Profession == profession)
            {
                CCPoint destPoint = it -> second -> getPosition();
                
                //球
                CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName(buff);
                pSprite -> setPosition(startpoint);
                
                //运动曲线
                ccBezierConfig pBezierConfig;
                pBezierConfig.controlPoint_1 = startpoint;
                pBezierConfig.controlPoint_2 = CCPointMake(rand() % 320, startpoint.y + rand() % (int)(destPoint.y - startpoint.y));
                pBezierConfig.endPosition = destPoint;
                CCBezierTo * pBezierTo = CCBezierTo::actionWithDuration(0.6f, pBezierConfig);
                
                CCCallFuncND * pAttackBallArrival = CCCallFuncND::actionWithTarget(this, callfuncND_selector(Layer_Display::AttackBallArrival), it -> second);
            
                pSprite -> runAction(CCSequence::actions(pBezierTo, pAttackBallArrival, NULL));
                this -> addChild(pSprite);
            }
        }
    }
}

void Layer_Display::AddHpBallArrival(CCNode * sender)
{
    Layer_Follower::ShareInstance() -> DisplayAddHp();
    
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.6f);
    CCCallFuncN * pBallDispel = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::BallDispel));
    sender -> runAction(CCSequence::actions(pFadeOut, pBallDispel, NULL));
}

void Layer_Display::AttackBallArrival(CCNode * sender, void * data)
{
    Battle_Follower * BF = static_cast<Battle_Follower*>(data);
    BF -> DisplayNumber();
    
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.6f);
    CCCallFuncN * pBallDispel = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::BallDispel));
    sender -> runAction(CCSequence::actions(pFadeOut, pBallDispel, NULL));
}

void Layer_Display::BallDispel(CCNode * sender)
{
    this -> removeChild(sender, true);
}
/************************************************************************/
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark 显示小弟攻击所有的敌人特效
/************************************************************************/
void Layer_Display::DisplayFollowerAttackAll(int profession)
{
    const char * filename = NULL;
    char buff[32];
    
    if(profession == SWORD)
    {
        filename = "SwordAttackAll_";
    
        sprintf(buff, "%s%d.png", filename, 1);
        CCSprite * pAni = CCSprite::spriteWithSpriteFrameName(buff);
        pAni -> setPosition(ccp(160, 380));
        
        CCAnimation * pAnimation = CreateAnimation(filename, 0.06f);
        CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
        CCCallFuncN * pAniOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayFollowerAttackRemove));
        
        pAni -> runAction(CCSequence::actions(pAnimate, pAniOver, NULL));
        this -> addChild(pAni);
    }
    else if(profession == FIST)
    {
        filename = "FistAttack_";
        sprintf(buff, "%s%d.png", filename, 1);
        
        Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
        for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
        {
            CCSprite * pAni = CCSprite::spriteWithSpriteFrameName(buff);
            pAni -> setPosition(it -> second -> GetWorldPoint());
            
            CCAnimation * pAnimation = CreateAnimation(filename, 0.05f);
            CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
            CCCallFuncN * pAniOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayFollowerAttackRemove));
            
            pAni -> runAction(CCSequence::actions(pAnimate, pAniOver, NULL));
            this -> addChild(pAni);
        }
    }
    else if(profession == GUN)
    {
        filename = "GunAttackAll_";
        sprintf(buff, "%s%d.png", filename, 1);
        
        Battle_EnemyList pList = *Layer_Enemy::ShareInstance() -> GetEnemyList();
        for(Battle_EnemyList::iterator it = pList.begin(); it != pList.end(); ++ it)
        {
            CCSprite * pAni = CCSprite::spriteWithSpriteFrameName(buff);
            pAni -> setPosition(it -> second -> GetWorldPoint());
            
            CCAnimation * pAnimation = CreateAnimation(filename, 0.05f);
            CCAnimate * pAnimate = CCAnimate::actionWithAnimation(pAnimation);
            CCCallFuncN * pAniOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayFollowerAttackRemove));
            
            pAni -> runAction(CCSequence::actions(pAnimate, pAniOver, NULL));
            this -> addChild(pAni);
        }
    }
}

void Layer_Display::DisplayFollowerAttackRemove(CCNode * sender)
{
    this -> removeChild(sender, true);
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示敌人死亡后的掉落金币
/************************************************************************/
void Layer_Display::DisplayEnemyDropMoney(CCPoint startpoint, Battle_Enemy * BE)
{
    //金币Icon
    CCSprite * pDropMoney = CCSprite::spriteWithSpriteFrameName("DropMoneyIcon.png");
    pDropMoney -> setPosition(startpoint);
    pDropMoney -> setScale(0.3f);
    
    //放大
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.4f, 1.0f);
    CCCallFuncND * pDisplayEnemyDropMoneyCallBack1 = CCCallFuncND::actionWithTarget(this, callfuncND_selector(Layer_Display::DisplayEnemyDropMoneyCallBack1), BE);
    
    pDropMoney -> runAction(CCSequence::actions(pScaleTo, pDisplayEnemyDropMoneyCallBack1, NULL));
    
    this -> addChild(pDropMoney);
}

void Layer_Display::DisplayEnemyDropMoneyCallBack1(CCNode * sender, void * data)
{
    CCPoint destPoint = CCPointMake(123, 470);
    
    //运动曲线
    CCMoveTo * pMoveTo = CCMoveTo::actionWithDuration(0.3f, destPoint);
    CCEaseOut * pEaseOut = CCEaseOut::actionWithAction(pMoveTo, 1.5f);
    
    //缩小
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.3f, 0.1f);
    
    //回调函数
    CCCallFuncND * pDisplayEnemyDropMoneyCallBack2 = CCCallFuncND::actionWithTarget(this, callfuncND_selector(Layer_Display::DisplayEnemyDropMoneyCallBack2), data);
    
    sender -> runAction(CCSequence::actions(CCSpawn::actions(pEaseOut, pScaleTo, NULL), pDisplayEnemyDropMoneyCallBack2, NULL));
}

void Layer_Display::DisplayEnemyDropMoneyCallBack2(CCNode * sender, void * data)
{
    this -> removeChild(sender, true);
    Layer_Enemy::ShareInstance() -> DisplayChangeBattleMoney();
    
    static_cast<Battle_Enemy*>(data) -> SetDisplayDeadOver();
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示敌人死亡后的掉落小弟
/************************************************************************/
void Layer_Display::DisplayEnemyDropRes(CCPoint startpoint, Battle_Enemy * BE)
{
    const char * IconName = SystemDataManage::ShareInstance() -> GetData_ForFollower(BE -> GetData() -> Enemy_DropFollower_ID) -> Follower_DropIconName;
    
    //物品Icon
    CCSprite * pDropRes = CCSprite::spriteWithSpriteFrameName(IconName);
    pDropRes -> setPosition(startpoint);
    pDropRes -> setScale(0.3f);
    
    //放大
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.4f, 1.0f);
    CCCallFuncND * pDisplayEnemyDropResCallBack1 = CCCallFuncND::actionWithTarget(this, callfuncND_selector(Layer_Display::DisplayEnemyDropresCallBack1), BE);
    
    pDropRes -> runAction(CCSequence::actions(pScaleTo, pDisplayEnemyDropResCallBack1, NULL));
    
    this -> addChild(pDropRes);
}

void Layer_Display::DisplayEnemyDropresCallBack1(cocos2d::CCNode * sender, void * data)
{
    CCPoint destPoint = CCPointMake(212, 470);
    
    //运动曲线
    CCMoveTo * pMoveTo = CCMoveTo::actionWithDuration(0.3f, destPoint);
    CCEaseOut * pEaseOut = CCEaseOut::actionWithAction(pMoveTo, 1.5f);
    
    //缩小
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.3f, 0.1f);
    
    //回调函数
    CCCallFuncND * pDisplayEnemyDropResCallBack2 = CCCallFuncND::actionWithTarget(this, callfuncND_selector(Layer_Display::DisplayEnemyDropResCallBack2), data);
    
    sender -> runAction(CCSequence::actions(CCSpawn::actions(pEaseOut, pScaleTo, NULL), pDisplayEnemyDropResCallBack2, NULL));
}

void Layer_Display::DisplayEnemyDropResCallBack2(cocos2d::CCNode * sender, void * data)
{
    this -> removeChild(sender, true);
    Layer_Enemy::ShareInstance() -> DisplayChangeBattleRes();
    
    static_cast<Battle_Enemy*>(data) -> SetDisplayDeadOver();
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示技能开始的效果
/************************************************************************/
void Layer_Display::DisplaySkillStart(Battle_Follower * BF)
{
    m_Caller = BF;
    m_DestPoint = m_Caller -> getPosition();
    
    this -> schedule(schedule_selector(Layer_Display::DisplaySkillStartFunc1));
    Layer_Enemy::ShareInstance() -> BGDark();
    
    CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(1.0f);
    CCCallFunc * pFun = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Display::DisplaySkillStartOver));
    this -> runAction(CCSequence::actions(pDelayTime, pFun, NULL));
}

void Layer_Display::DisplaySkillStartOver()
{
    this -> unschedule(schedule_selector(Layer_Display::DisplaySkillStartFunc1));
    
    int SkillType = m_Caller -> GetHost() -> GetData() -> Follower_CommonlySkillType;
    int SkillID = m_Caller -> GetHost() -> GetData() -> Follower_CommonlySkillID;
    
    if( SkillType == 1 && ((SkillID >= 1 && SkillID <= 12) || (SkillID >= 19 && SkillID <= 24) || (SkillID >= 25 && SkillID <= 26) || (SkillID >= 28 && SkillID <= 42)) )
    {
        if(m_Caller -> GetHost() -> GetData() -> Follower_Profession == SWORD)
        {
            m_SA_Icon1 = "SA_Sword_Icon1.png";
            m_SA_Icon1_1 = "SA_Sword_Icon1_1.png";
            m_SA_Icon2 = "SA_Sword_Icon2.png";
            m_SA_Icon2_1 = "SA_Sword_Icon2_1.png";
            m_SA_Icon3Left = "SA_Sword_Icon3Left.png";
            m_SA_Icon3Right = "SA_Sword_Icon3Right.png";
        }
        else if(m_Caller -> GetHost() -> GetData() -> Follower_Profession == FIST)
        {
            m_SA_Icon1 = "SA_Fist_Icon1.png";
            m_SA_Icon1_1 = "SA_Fist_Icon1_1.png";
            m_SA_Icon2 = "SA_Fist_Icon2.png";
            m_SA_Icon2_1 = "SA_Fist_Icon2_1.png";
            m_SA_Icon3Left = "SA_Fist_Icon3Left.png";
            m_SA_Icon3Right = "SA_Fist_Icon3Right.png";
        }
        else if(m_Caller -> GetHost() -> GetData() -> Follower_Profession == GUN)
        {
            m_SA_Icon1 = "SA_Gun_Icon1.png";
            m_SA_Icon1_1 = "SA_Gun_Icon1_1.png";
            m_SA_Icon2 = "SA_Gun_Icon2.png";
            m_SA_Icon2_1 = "SA_Gun_Icon2_1.png";
            m_SA_Icon3Left = "SA_Gun_Icon3Left.png";
            m_SA_Icon3Right = "SA_Gun_Icon3Right.png";
        }
       
        DisplaySkillAnimation();
    }
    else
    {
        m_Caller -> TriggerSkillAnimation();
        m_Caller = NULL;
    }
}

void Layer_Display::DisplaySkillStartFunc1()
{
    CCMoveTo * pMoveTo = CCMoveTo::actionWithDuration(0.1f, m_DestPoint);
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.1f, 0.0f);
    CCCallFuncN * pFunc2 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplaySkillStartFunc2));
    CCFiniteTimeAction * pAction = CCSequence::actions(CCSpawn::actions(pMoveTo, pScaleTo, NULL), pFunc2, NULL);
    
    for(int i = 0; i < 3; ++ i)
    {
        CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName("SkillStartBall.png");
        pSprite -> setPosition(ccp((m_DestPoint.x - 100) + (rand() % 201), (m_DestPoint.y - 100) + (rand() % 201)));
        
        pSprite -> runAction(dynamic_cast<CCFiniteTimeAction*>(pAction -> copy()));
        this -> addChild(pSprite);
    }
}

void Layer_Display::DisplaySkillStartFunc2(cocos2d::CCNode * sender)
{
    this -> removeChild(sender, true);
}



void Layer_Display::DisplaySkillAnimation()
{
    CCSprite * pRight = CCSprite::spriteWithSpriteFrameName(m_SA_Icon3Right);
    pRight -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2));
    CCSprite * pLeft = CCSprite::spriteWithSpriteFrameName(m_SA_Icon3Left);
    pLeft -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2));
    
    CCSprite * pIcon1_1 = CCSprite::spriteWithSpriteFrameName(m_SA_Icon1_1);
    pIcon1_1 -> setPosition(ccp(280, 389.5));
    CCSprite * pIcon2_1 = CCSprite::spriteWithSpriteFrameName(m_SA_Icon2_1);
    pIcon2_1 -> setPosition(ccp(85, 405));
    
    CCMoveTo * pIcon1_1Move = CCMoveTo::actionWithDuration(0.2f, ccp(65.5f, pIcon1_1 -> getPosition().y));
    CCMoveTo * pIcon2_1Move = CCMoveTo::actionWithDuration(0.2f, ccp(230.0f, pIcon2_1 -> getPosition().y));
    CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplaySkillAnimationFunc1));
    pIcon1_1 -> runAction(CCSequence::actions(pIcon1_1Move, pCallFuncN, NULL));
    pIcon2_1 -> runAction(CCSequence::actions(pIcon2_1Move, pCallFuncN, NULL));
    
    this -> addChild(pRight, 0, Layer_DisplayChild_SA_Right);
    this -> addChild(pLeft, 0, Layer_DisplayChild_SA_Left);
    this -> addChild(pIcon1_1, 0, Layer_DisplayChild_SA_Icon1_1);
    this -> addChild(pIcon2_1, 0, Layer_DisplayChild_SA_Icon2_1);
}

void Layer_Display::DisplaySkillAnimationFunc1(CCNode * sender)
{
    this -> removeChild(sender, true);
    
    if(! this -> getChildByTag(Layer_DisplayChild_SA_Icon1_1) && ! this -> getChildByTag(Layer_DisplayChild_SA_Icon2_1))
    {
        CCLayerColor * ColorLayer = CCLayerColor::layerWithColorWidthHeight(ccc4(255, 255, 255, 255), m_WinSize.width, m_WinSize.height);
        
        CCDelayTime * pDelay = CCDelayTime::actionWithDuration(0.1f);
        CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplaySkillAnimationFunc2));
        ColorLayer -> runAction(CCSequence::actions(pDelay, pCallFuncN, NULL));
        
        this -> addChild(ColorLayer);
    }
}

void Layer_Display::DisplaySkillAnimationFunc2(CCNode * sender)
{
    this -> removeChild(sender, true);
    
    CCSprite * pIcon1 = CCSprite::spriteWithSpriteFrameName(m_SA_Icon1);
    pIcon1 -> setPosition(ccp(65.5f, 389.5));
    CCSprite * pIcon2 = CCSprite::spriteWithSpriteFrameName(m_SA_Icon2);
    pIcon2 -> setPosition(ccp(230.0f, 405));
    
    CCMoveTo * pIcon1Move = CCMoveTo::actionWithDuration(0.7f, ccp(33, pIcon1 -> getPosition().y));
    CCMoveTo * pIcon2Move = CCMoveTo::actionWithDuration(0.7f, ccp(275, pIcon2 -> getPosition().y));
    CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplaySkillAnimationFunc3));
    pIcon1 -> runAction(CCSequence::actions(pIcon1Move, pCallFuncN, NULL));
    pIcon2 -> runAction(CCSequence::actions(pIcon2Move, pCallFuncN, NULL));
    
    this -> addChild(pIcon1, 0, Layer_DisplayChild_SA_Icon1);
    this -> addChild(pIcon2, 0, Layer_DisplayChild_SA_Icon2);
}

void Layer_Display::DisplaySkillAnimationFunc3(CCNode * sender)
{
    CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplaySkillAnimationFunc4));
    
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.7f);
    
    CCSprite * pIcon1 = dynamic_cast<CCSprite*>(this -> getChildByTag(Layer_DisplayChild_SA_Icon1));
    CCMoveTo * pIcon1Move = CCMoveTo::actionWithDuration(0.7f, ccp(pIcon1 -> getPosition().x, pIcon1 -> getPosition().y + 100));
    pIcon1 -> runAction(CCSequence::actions(CCSpawn::actions(pIcon1Move, pFadeOut, NULL), pCallFuncN, NULL));
    
    CCSprite * pIcon2 = dynamic_cast<CCSprite*>(this -> getChildByTag(Layer_DisplayChild_SA_Icon2));
    CCMoveTo * pIcon2Move = CCMoveTo::actionWithDuration(0.7f, ccp(pIcon2 -> getPosition().x, pIcon2 -> getPosition().y - 100));
    pIcon2 -> runAction(CCSequence::actions(CCSpawn::actions(pIcon2Move, dynamic_cast<CCFadeOut*>(pFadeOut -> copy()), NULL), pCallFuncN, NULL));
    
    CCSprite * pRight = dynamic_cast<CCSprite*>(this -> getChildByTag(Layer_DisplayChild_SA_Right));
    CCMoveTo * pRightMove = CCMoveTo::actionWithDuration(0.7f, ccp(pRight -> getPosition().x, pRight -> getPosition().y - 100));
    pRight -> runAction(CCSequence::actions(CCSpawn::actions(pRightMove, dynamic_cast<CCFadeOut*>(pFadeOut -> copy()), NULL), pCallFuncN, NULL));
    
    CCSprite * pLeft = dynamic_cast<CCSprite*>(this -> getChildByTag(Layer_DisplayChild_SA_Left));
    CCMoveTo * pLeftMove = CCMoveTo::actionWithDuration(0.7f, ccp(pLeft -> getPosition().x, pLeft -> getPosition().y + 100));
    pLeft -> runAction(CCSequence::actions(CCSpawn::actions(pLeftMove, dynamic_cast<CCFadeOut*>(pFadeOut -> copy()), NULL), pCallFuncN, NULL));
}

void Layer_Display::DisplaySkillAnimationFunc4(CCNode * sender)
{
    this -> removeChild(sender, true);
        
    if(! this -> getChildByTag(Layer_DisplayChild_SA_Icon1) && ! this -> getChildByTag(Layer_DisplayChild_SA_Icon2) &&
       ! this -> getChildByTag(Layer_DisplayChild_SA_Right) && ! this -> getChildByTag(Layer_DisplayChild_SA_Left))
    {
        m_Caller -> TriggerSkillAnimation();
        m_Caller = NULL;
    }
}
/************************************************************************/
#pragma mark -
/************************************************************************/




/************************************************************************/
#pragma mark 显示屏幕警告
/************************************************************************/
void Layer_Display::DisplayWarning()
{
    //////////////颜色//////////////////
    CCSize RTSize = CCSizeMake(640, 428);
    CCRenderTexture * RT = CCRenderTexture::renderTextureWithWidthAndHeight(RTSize.width / 2, RTSize.height / 2);
    RT -> beginWithClear(255, 0, 0, 0);
    
    glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
    CCPoint vertices[10];
    ccColor4F colors[10];
    int nVertices = 0;
    
    vertices[nVertices] = CCPointMake(0, 0);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0.8};
    vertices[nVertices] = CCPointMake(RTSize.width, 0);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0.8};
    
    vertices[nVertices] = CCPointMake(0, 0 + RTSize.height / 4 * 1);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0.5};
    vertices[nVertices] = CCPointMake(RTSize.width, 0 + RTSize.height / 4 * 1);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0.5};
    
    vertices[nVertices] = CCPointMake(0, 0 + RTSize.height / 4 * 2);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0.0};
    vertices[nVertices] = CCPointMake(RTSize.width, 0 + RTSize.height / 4 * 2);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0.0};
    
    vertices[nVertices] = CCPointMake(0, 0 + RTSize.height / 4 * 3);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0.5};
    vertices[nVertices] = CCPointMake(RTSize.width, 0 + RTSize.height / 4 * 3);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0.5};
    
    vertices[nVertices] = CCPointMake(0, 0 + RTSize.height / 4 * 4);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0.8};
    vertices[nVertices] = CCPointMake(RTSize.width, 0 + RTSize.height / 4 * 4);
    colors[nVertices++] = (ccColor4F){0, 0, 0, 0.8};
    
    glVertexPointer(2, GL_FLOAT, 0, vertices);
    glColorPointer(4, GL_FLOAT, 0, colors);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)nVertices);
    
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnable(GL_TEXTURE_2D);
    
    RT -> end();
     
    CCSprite * pSprite = CCSprite::spriteWithTexture(RT -> getSprite() -> getTexture());
    pSprite -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height - (428 / 4)));
    pSprite -> setOpacity(0.0);
    
    CCFadeIn * pFadeIn = CCFadeIn::actionWithDuration(0.5f);
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.5f);
    CCFiniteTimeAction * pFiniteTimeAction = CCSequence::actions(pFadeIn, pFadeOut, NULL);
    
    CCRepeat * pRepeat = CCRepeat::actionWithAction(pFiniteTimeAction, 100);
    pSprite -> runAction(pRepeat);
    ////////////////////////////////////////////////
    
    CCSprite * pWarningTitle1 = CCSprite::spriteWithSpriteFrameName("WarningTitle.png");
    pWarningTitle1 -> setPosition(ccp(m_WinSize.width / 2, 378 + 43));
    
    CCSprite * pWarningTitle2 = CCSprite::spriteWithSpriteFrameName("WarningTitle.png");
    pWarningTitle2 -> setPosition(ccp(m_WinSize.width / 2, 378 - 43));
    
    CCMoveBy * pWarningMove = CCMoveBy::actionWithDuration(0.002f, ccp(-6, 0));
    
    CCSprite * pWarning1 = CCSprite::spriteWithSpriteFrameName("Warning.png");
    pWarning1 -> setPosition(ccp(m_WinSize.width / 2, 378));
    pWarning1 -> runAction(CCRepeatForever::actionWithAction(dynamic_cast<CCMoveBy*>(pWarningMove -> copy())));
    
    CCSprite * pWarning2 = CCSprite::spriteWithSpriteFrameName("Warning.png");
    pWarning2 -> setPosition(ccp(m_WinSize.width + m_WinSize.width / 2, 378));
    pWarning2 -> runAction(CCRepeatForever::actionWithAction(dynamic_cast<CCMoveBy*>(pWarningMove -> copy())));
    
    this -> schedule(schedule_selector(Layer_Display::DisplayWarningCallBack1), 0.005);
    
    this -> addChild(pSprite, 0, Layer_DisplayChild_WarningColor);
    this -> addChild(pWarning1, 1, Layer_DisplayChild_Warning1);
    this -> addChild(pWarning2, 2, Layer_DisplayChild_Warning2);
    this -> addChild(pWarningTitle1, 3, Layer_DisplayChild_WarningTitle1);
    this -> addChild(pWarningTitle2, 4, Layer_DisplayChild_WarningTitle2);
}

void Layer_Display::DisplayWarningCallBack1()
{
    CCMoveBy * pWarningMove = CCMoveBy::actionWithDuration(0.002f, ccp(-6, 0));
    
    if(this -> getChildByTag(Layer_DisplayChild_Warning1))
    {
        if(this -> getChildByTag(Layer_DisplayChild_Warning1) -> getPosition().x <= 0 - m_WinSize.width / 2)
        {
            this -> getChildByTag(Layer_DisplayChild_Warning1) -> stopAllActions();
            this -> getChildByTag(Layer_DisplayChild_Warning1) -> setPosition(ccp(m_WinSize.width + m_WinSize.width / 2, 378));
            this -> getChildByTag(Layer_DisplayChild_Warning1) -> runAction(CCRepeatForever::actionWithAction(dynamic_cast<CCMoveBy*>(pWarningMove -> copy())));
        }
    }
    
    if(this -> getChildByTag(Layer_DisplayChild_Warning2))
    {
        if(this -> getChildByTag(Layer_DisplayChild_Warning2) -> getPosition().x <= 0 - m_WinSize.width / 2)
        {
            this -> getChildByTag(Layer_DisplayChild_Warning2) -> stopAllActions();
            this -> getChildByTag(Layer_DisplayChild_Warning2) -> setPosition(ccp(m_WinSize.width + m_WinSize.width / 2, 378));
            this -> getChildByTag(Layer_DisplayChild_Warning2) -> runAction(CCRepeatForever::actionWithAction(dynamic_cast<CCMoveBy*>(pWarningMove -> copy())));
        }
    }
}

void Layer_Display::DisplayWarningOver()
{
    CCFadeOut * pFadeOut1 = CCFadeOut::actionWithDuration(0.2f);
    CCCallFuncN * pDisplayWarningOverCallBack1 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayWarningOverCallBack1));
    this -> getChildByTag(Layer_DisplayChild_WarningColor) -> stopAllActions();
    this -> getChildByTag(Layer_DisplayChild_WarningColor) -> runAction(CCSequence::actions(dynamic_cast<CCFadeOut*>(pFadeOut1 -> copy()), pDisplayWarningOverCallBack1, NULL));
    this -> getChildByTag(Layer_DisplayChild_WarningTitle1) -> runAction(CCSequence::actions(dynamic_cast<CCFadeOut*>(pFadeOut1 -> copy()), pDisplayWarningOverCallBack1, NULL));
    this -> getChildByTag(Layer_DisplayChild_WarningTitle2) -> runAction(CCSequence::actions(dynamic_cast<CCFadeOut*>(pFadeOut1 -> copy()), pDisplayWarningOverCallBack1, NULL));
    
    CCFadeOut * pFadeOut2 = CCFadeOut::actionWithDuration(0.1f);
    CCCallFuncN * pDisplayWarningOverCallBack2 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayWarningOverCallBack2));
    this -> getChildByTag(Layer_DisplayChild_Warning1) -> runAction(CCSequence::actions(dynamic_cast<CCFadeOut*>(pFadeOut2 -> copy()), pDisplayWarningOverCallBack2, NULL));
    this -> getChildByTag(Layer_DisplayChild_Warning2) -> runAction(CCSequence::actions(dynamic_cast<CCFadeOut*>(pFadeOut2 -> copy()), pDisplayWarningOverCallBack2, NULL));
}

void Layer_Display::DisplayWarningOverCallBack1(CCNode * sender)
{
    this -> removeChild(sender, true);
}

void Layer_Display::DisplayWarningOverCallBack2(CCNode * sender)
{
    this -> removeChild(sender, true);
    this -> unschedule(schedule_selector(Layer_Display::DisplayWarningCallBack1));
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示GameOver
/************************************************************************/
void Layer_Display::DisplayGameOver()
{
    Layer_Block::ShareInstance() -> EndBlock();
  
    CCDelayTime * pDelay = CCDelayTime::actionWithDuration(2.0f);
    CCCallFuncN * pDisplayGameOverCallBack1 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayGameOverCallBack1));
    this -> runAction(CCSequence::actions(pDelay, pDisplayGameOverCallBack1, NULL));
}

void Layer_Display::DisplayGameOverCallBack1()
{
    CCSprite * pGame = CCSprite::spriteWithSpriteFrameName("Game.png");
    pGame -> setPosition(ccp(0 - pGame -> getTextureRect().size.width / 2, 133));
    
    CCSprite * pOver = CCSprite::spriteWithSpriteFrameName("Over.png");
    pOver -> setPosition(ccp(m_WinSize.width + pOver -> getTextureRect().size.width / 2, 133));
    
    CCMoveTo * pGMove = CCMoveTo::actionWithDuration(0.2f, ccp(m_WinSize.width / 2 - pGame -> getTextureRect().size.width / 2, 133));
    CCMoveTo * pOMove = CCMoveTo::actionWithDuration(0.2f, ccp(m_WinSize.width / 2 + pOver -> getTextureRect().size.width / 2, 133));
    CCEaseIn * pGameMove = CCEaseIn::actionWithAction(pGMove, 2.0f);
    CCEaseIn * pOverMove = CCEaseIn::actionWithAction(pOMove, 2.0f);
    pGame -> runAction(pGameMove);
    pOver -> runAction(pOverMove);
    
    CCDelayTime * pDelay = CCDelayTime::actionWithDuration(0.2f);
    CCCallFuncN * pDisplayGameOverCallBack2 = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayGameOverCallBack2));
    this -> runAction(CCSequence::actions(pDelay, pDisplayGameOverCallBack2, NULL));

    this -> addChild(pGame, 10, Layer_DisplayChild_Game);
    this -> addChild(pOver, 10, Layer_DisplayChild_Over);
}

void Layer_Display::DisplayGameOverCallBack2()
{
    CCSprite * pGame = dynamic_cast<CCSprite *>(this -> getChildByTag(Layer_DisplayChild_Game));
    CCSprite * pOver = dynamic_cast<CCSprite *>(this -> getChildByTag(Layer_DisplayChild_Over));
    
    CCScaleTo * Big = CCScaleTo::actionWithDuration(0.1f, 1.0f, 2.0f);
    CCScaleTo * Small = CCScaleTo::actionWithDuration(0.1f, 1.0f, 1.0f);
    
    CCFiniteTimeAction * pGameBigAndSmall = CCSequence::actions(Big, Small, NULL);
    CCMoveTo * GameMove = CCMoveTo::actionWithDuration(0.2f, ccp(0 + m_WinSize.width / 4, 133));
    pGame -> runAction(CCSequence::actions(pGameBigAndSmall, GameMove, NULL));
    
    CCFiniteTimeAction * pOverBigAndSmall = CCSequence::actions(dynamic_cast<CCScaleTo*>(Big -> copy()), dynamic_cast<CCScaleTo*>(Small -> copy()), NULL);
    CCMoveTo * OverMove = CCMoveTo::actionWithDuration(0.2f, ccp(m_WinSize.width / 2 + m_WinSize.width / 4, 133));
    pOver -> runAction(CCSequence::actions(pOverBigAndSmall, OverMove, NULL)); 
    
    CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(0.6f);
    CCCallFunc * pDisplayGameOverCallBack3 = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Display::DisplayGameOverCallBack3));
    this -> runAction(CCSequence::actions(pDelayTime, pDisplayGameOverCallBack3, NULL));
}

void Layer_Display::DisplayGameOverCallBack3()
{
    Layer_BattleMessage * LB = Layer_BattleMessage::node();
    LB -> SetMessageState(MESSAGE_LIVE);
    LB -> setPosition(ccp(0, 50));
    
    this -> addChild(LB, 0, Layer_DisplayChild_Message);
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示GameOver结束
/************************************************************************/
void Layer_Display::DisplayGameOverRemove()
{
    CCSprite * pGame = dynamic_cast<CCSprite*>(this -> getChildByTag(Layer_DisplayChild_Game));
    CCSprite * pOver = dynamic_cast<CCSprite*>(this -> getChildByTag(Layer_DisplayChild_Over));
    
    CCMoveTo * pGMove = CCMoveTo::actionWithDuration(0.2f, ccp(0 - pGame -> getTextureRect().size.width / 2, 133));
    CCMoveTo * pOMove = CCMoveTo::actionWithDuration(0.2f, ccp(m_WinSize.width + pOver -> getTextureRect().size.width / 2, 133));
    CCEaseIn * pGameMove = CCEaseIn::actionWithAction(pGMove, 2.0f);
    CCEaseIn * pOverMove = CCEaseIn::actionWithAction(pOMove, 2.0f);
    pGame -> runAction(pGameMove);
    pOver -> runAction(pOverMove);
    
    CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(0.3f);
    CCCallFunc * pCallFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Display::DisplayGameOverRemoveCallBack1));
    this -> runAction(CCSequence::actions(pDelayTime, pCallFunc, NULL));
}

void Layer_Display::DisplayGameOverRemoveCallBack1()
{
    this -> removeChildByTag(Layer_DisplayChild_Game, true);
    this -> removeChildByTag(Layer_DisplayChild_Over, true);
}
/************************************************************************/
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 显示废话
/************************************************************************/
void Layer_Display::DisplayBullShit(int index)
{
    char buff[32];
    sprintf(buff, "bullshit_%d.png", index);
    CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName(buff);
    pSprite -> setPosition(ccp(0 - pSprite -> getTextureRect().size.width / 2, 130));
    
    //进来的时候
    CCMoveTo * pMoveTo1 = CCMoveTo::actionWithDuration(0.3f, ccp(m_WinSize.width / 2, 130));
    CCEaseElasticOut * pEaseElasticOut = CCEaseElasticOut::actionWithAction(pMoveTo1);
    
    //离开的时候
    CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(0.8f);
    CCMoveTo * pMoveTo2 = CCMoveTo::actionWithDuration(0.3f, ccp(m_WinSize.width + pSprite -> getTextureRect().size.width / 2, 130));
    CCEaseElasticIn * pEaseElasticIn = CCEaseElasticIn::actionWithAction(pMoveTo2);
    CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Display::DisplayBullShitFunc1));
    
    pSprite -> runAction(CCSequence::actions(pEaseElasticOut, CCSequence::actions(pDelayTime, pEaseElasticIn, NULL), pCallFuncN, NULL));
    this -> addChild(pSprite);
}

void Layer_Display::DisplayBullShitFunc1(CCNode * sender)
{
    this -> removeChild(sender, true);
}
/************************************************************************/
#pragma mark -
/************************************************************************/







void Layer_Display::DisplayTest()
{
    /*CCSprite * pDropMoney = CCSprite::spriteWithSpriteFrameName("DropMoneyIcon.png");
    pDropMoney -> setPosition(ccp(160, 240));
    
    MyCircularityConfig MC;
    MC.CenterPosition = CCPointMake(160,  240);
    MC.aLength = 100;
    MC.cLength = 50;
    MyCircularityAction * pA = MyCircularityAction::actionWithDuration(1.0f, MC);
    pDropMoney -> runAction(CCRepeat::actionWithAction(pA, 300));
    
    this -> addChild(pDropMoney);*/
    
    
    //CCRipple3D * asd = CCRipple3D::actionWithPosition(CCPointMake(64, 380), 64, 2, 30, ccg(32, 24), 10.0f);//水波 参数是中心点，半径，波浪数，振幅，格，时间
    //this -> runAction(asd);
}





























