//
//  Layer_BattleResult.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-6-28.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_BattleResult_h
#define MidNightCity_Layer_BattleResult_h

#include "cocos2d.h"
#include "KNProgress.h"
#include "Follower.h"
#include "Layer_Display.h"
#include "PlayerDataManage.h"

USING_NS_CC;

class Layer_BattleResult : public CCLayer
{
public:
    #pragma mark - 构造函数
    Layer_BattleResult();
    #pragma mark - 析构函数
    virtual ~Layer_BattleResult();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 显示函数
    void show();
    
    #pragma mark - 连接cocos2d初始化方式
    LAYER_NODE_FUNC(Layer_BattleResult);
private:
    #pragma mark - 回调函数 : 动画步骤1
    void AnimStep1(CCObject* pObject);
    #pragma mark - 回调函数 : 动画步骤2
    void AnimStep2(CCObject* pObject);
    #pragma mark - 回调函数 : 动画步骤3
    void AnimStep3(CCObject* pObject);
    #pragma mark - 回调函数 : 动画步骤4
    void AnimStep4(CCObject* pObject);
    #pragma mark - 回调函数 : 动画步骤5
    void AnimStep5(CCObject* pObject);
    #pragma mark - 回调函数 : 动画步骤6
    void AnimStep6(CCObject* pObject);
    #pragma mark - 回调函数 : 动画步骤7
    void AnimStep7(CCObject* pObject);
    #pragma mark - 回调函数 : 动画步骤8
    void AnimStep8(CCObject* pObject);
    
    #pragma mark - 回调函数 : 打开金钱数量增加
    void startMoneyNumberAdd(CCObject* pObject);
    #pragma mark - 回调函数 : 打开经验数量增加
    void startExpNumberAdd(CCObject* pObject);
    #pragma mark - 回调函数 : 打开剩余经验数量增加
    void startRemainExpNumberAdd(CCObject* pObject);
    
    #pragma mark - 回调函数 : 金钱数量增加
    void MoneyNumberAdd(ccTime dt);
    #pragma mark - 回调函数 : 经验数量增加
    void ExpNumberAdd(ccTime dt);
    #pragma mark - 回调函数 : 剩余经验减少
    void RemainNumberMinus(ccTime dt);
    
    #pragma mark - 回调函数 : 创建升级效果
    void CreateLevelUpEffect(CCObject* pObject);
    #pragma mark - 回调函数 : 升级效果过渡
    void PassLevelUpEffect(CCObject* pObject);
    #pragma mark - 回调函数 : 结束升级效果
    void EndLevelUpEffect(CCObject* pObject);
    #pragma mark - 回调函数 : 处理烟花效果函数
    void ProcessFrameEffect(CCObject* pObject);
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    #pragma mark - 事件函数 : 滑动
    virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
    #pragma mark - 事件函数 : 弹出
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
private:
    #pragma mark - 获得的小弟是否是新类型函数
    bool IsNewFollowerInPackage(Follower* pFollower);
    #pragma mark - 快速结束经验增加函数
    void FinishExpEvent();
private:
    // 战斗结算对象变量
    CCLayerColor* m_pMaskLayer;                                     // 背景遮罩
    CCLayerColor* m_pTopMask;                                       // 顶部遮罩
    CCLayerColor* m_pBottomMask;                                    // 底部遮罩
    
    CCSprite* m_pWordSprite;                                        // 通关胜利
    CCSprite* m_pMoneyBar;                                          // 金钱地条
    CCSprite* m_pExpBar;                                            // 经验地条
    
    CCSprite* m_pExpProgressBack;                                   // 经验进度底条
    
    KNProgress* m_pExpProgress;                                     // 经验进度条
    
    CCSprite* m_pFollowerBack;                                      // 小弟背景
    CCSprite* m_pRemainMark;                                        // 剩余纹理标记
    
    CCSprite* m_pExpMask;                                           // 经验遮罩
    CCSprite* m_pMoneyMask;                                         // 金钱遮罩
    
    CCSprite* m_pLevelMark;                                         // 等级标记
    
    vector<Follower*> m_vecFollowerList;                            // 战斗获得小弟列表
    
    CCLabelAtlas* m_pMoneyTotal;                                    // 金钱总数
    CCLabelAtlas* m_pExpTotal;                                      // 经验总数
    CCLabelAtlas* m_pRemainExpTotal;                                // 剩余经验总数
    CCLabelAtlas* m_pPlayerLevel;                                   // 玩家等级
    
    CCSprite* m_pAddFriendNormal;                                   // 好友确定 - 普通
    CCSprite* m_pAddFriendSelect;                                   // 好友确定 - 点击
    
    // 战斗结算属性变量
    float m_fBaseAnimTime;                                          // 动画播放基本时间
    
    int m_iCurrentMoney;                                            // 当前金钱数量
    int m_iCurrentExp;                                              // 当前经验数量
    int m_iCurrentRemainExp;                                        // 剩余经验数量
    
    bool m_bIsExplosionEnable;                                      // 爆炸效果开启
    bool m_bIsAnimOver;                                             // 动画是否结束
    bool m_bIsExpEventFinish;                                       // 经验处理结束
    bool m_bIsFrameEnable;                                          // 是否播放烟花
};

#endif
