//
//  Layer_BattleMessage.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-7-6.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_BattleMessage_h
#define MidNightCity_Layer_BattleMessage_h

#include "cocos2d.h"

USING_NS_CC;

enum MESSAGE_STATE
{
    MESSAGE_LIVE = 0,               // 复活对话框
    MESSAGE_GIVEUP,                 // 放弃对话框
    MESSAGE_NETFAILURE,             // 网络不稳定
    MESSAGE_LOST,                   // 丢失提示
    MESSAGE_ADDFRIEND,              // 加好友
    MESSAGE_NUM,
};

// 应用好友信息
struct FriendInfo;

// 图层控件标识id
const int BUTTON_CONFIRM_NORMAL_ID  = 1000;         // 确认按钮 - 普通
const int BUTTON_CONFIRM_SELECT_ID  = 1001;         // 确认按钮 - 选择

const int BUTTON_CANCEL_NORMAL_ID   = 1002;         // 取消按钮 - 普通
const int BUTTON_CANCEL_SELECT_ID   = 1003;         // 取消按钮 - 选择

const int BUTTON_RETRY_NORMAL_ID    = 1004;         // 重试按钮 - 普通
const int BUTTON_RETRY_SELECT_ID    = 1005;         // 重试按钮 - 选择

const int BUTTON_GIVEUP_NORMAL_ID   = 1006;         // 放弃按钮 - 普通
const int BUTTON_GIVEUP_SELECT_ID   = 1007;         // 放弃按钮 - 选择

const int FRIENDFOLLOWER_ICON_ID    = 1008;         // 好友小弟 - 头像
const int FRIENDFOLLOWER_FRAME_ID   = 1009;         // 好友小弟 - 边框

const int BUTTON_ADD_NORMAL_ID      = 1010;         // 好友小弟 - 添加按钮 - 普通
const int BUTTON_ADD_SELECT_ID      = 1011;         // 好友小弟 - 添加按钮 - 选择
const int BUTTON_LABEL_ID           = 1019;         // 好友小弟 - 添加按钮 - 标签

const int FRIENDFOLLOWER_LEVEL_ID   = 1012;         // 好友小弟 - 等级
const int FRIENDFOLLOWER_VALUE_ID   = 1013;         // 好友小弟 - 友情点

const int LIVE_QUESTION_ID          = 1014;         // 复活提问
const int GIVEUP_QUESTION_ID        = 1015;         // 放弃提问

const int BUTTON_LIVE_NORMAL_ID     = 1016;         // 复活按钮 - 普通
const int BUTTON_LIVE_SELECT_ID     = 1017;         // 复活按钮 - 选择

const int LABEL_INFORM_ID           = 1018;         // 提示文本

const int ADDFRIEND_FRAME_BACK_ID   = 1019;

const int ADDFRIEND_TITLE_ID        = 1020;
const int ADDFRIEND_ICON_ID         = 1021;

const int BUTTON_REFUSE_NORMAL_ID   = 1022;
const int BUTTON_REFUSE_SELECT_ID   = 1023;

const int BUTTON_CLOSE_NORMAL_ID    = 1024;
const int BUTTON_CLOSE_SELECT_ID    = 1025;

const int FRIENDFOLLOWER_NAME       = 1026;

const int LIVE_CUR_DIAMOND          = 1027;
const int LIVE_NEED_DIAMOND         = 1028;

const int LABEL_FRIEND_VALUE_HINT   = 1029;
const int LABEL_ADDFRIEND_HINT      = 1030;

const int BUTTON_LABEL_ADD_ID       = 1031;
const int BUTTON_LABEL_REFUSE_ID    = 1032;

class Layer_BattleMessage : public CCLayer
{
public:
    #pragma mark - 构造函数
    Layer_BattleMessage();
    #pragma mark - 析构函数
    virtual ~Layer_BattleMessage();
    
    #pragma mark - 初始化函数
    bool init();
    
    #pragma mark - 退出函数
    virtual void onExit();
    
    #pragma mark - 销毁函数
    void Destroy();
    
    #pragma mark - 设置界面状态
    void SetMessageState(MESSAGE_STATE eState);
    
    #pragma mark - 刷新好友信息
    void RefreshFriendInfo(FriendInfo& stInfo);
    
    #pragma mark - 连接cocos2d初始化方法
    LAYER_NODE_FUNC(Layer_BattleMessage);
   
public:
    CCSize   m_RealSize;
protected:
    #pragma mark - 事件函数 : 点击
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    #pragma mark - 事件函数 : 弹起
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
private:
    // 战斗信息对象变量
    CCSprite* m_pFrameBack;                     // 对话框背景
    
    CCLayer* m_pFrameLayer[MESSAGE_NUM];        // 对话框图层
    
    // 战斗信息属性变量
    MESSAGE_STATE m_eState;                     // 对话框状态
};

#endif
