//
//  MyCircularityAction.h
//  MidNightCity
//
//  Created by 强 张 on 12-7-4.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_MyCircularityAction_h
#define MidNightCity_MyCircularityAction_h

#include "cocos2d.h"

// 定义一个结构来包含确定椭圆的参数  
struct MyCircularityConfig
{  
    //中心点坐标  
    cocos2d::CCPoint CenterPosition;  
    //椭圆a长，三角斜边  
    float aLength;  
    //椭圆c长，三角底边  
    float cLength;  
};

class MyCircularityAction : public cocos2d::CCActionInterval
{
public:  
    bool initWithDuration(cocos2d::ccTime t, const MyCircularityConfig & c);  
    virtual void update(cocos2d::ccTime time);
    
public:  
    static MyCircularityAction * actionWithDuration(cocos2d::ccTime t, const MyCircularityConfig & c);  
    
protected:
    float CircularityXat(float a, float bx, float c, cocos2d::ccTime t );//返回X坐标  
    float CircularityYat(float a, float by, float c, cocos2d::ccTime t );//返回Y坐标  
  
protected:  
    MyCircularityConfig m_CircularityConfig;  
    
    cocos2d::CCPoint m_startPosition;  
    cocos2d::CCPoint s_startPosition;  
};

#endif
