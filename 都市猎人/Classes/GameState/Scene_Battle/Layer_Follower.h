//
//  Layer_Follower.h
//  MidNightCity
//
//  Created by 强 张 on 12-6-7.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_Follower_h
#define MidNightCity_Layer_Follower_h

#include "cocos2d.h"
#include "Follower_Configure.h"

class Battle_Follower;
class Skill;

typedef map <int, Battle_Follower*>            Battle_FollowerList;


#define  LFChildTag_Background 0
#define  LFChildTag_HPBackground 1
#define  LFChildTag_HP 2
#define  LFChildTag_HPMask 3
#define  LFChildTag_HPlable 4

#define  LFChildTag_AddHpLabel 5
#define  LFChildTag_AddHpJiaHao 6

#define  LFChildTag_AddHpLabel2 7
#define  LFChildTag_AddHpJiaHao2 8

#define  LFChildTag_AddHpLabel3 9
#define  LFChildTag_AddHpJiaHao3 10


#define  ActionTag_HpShake 0

class Sprite_Frame;
class Layer_Follower : public cocos2d::CCLayer
{
    friend class Layer_BattleAction;
    friend class Scene_Battle;
    
public:
    static Layer_Follower * ShareInstance();
  
protected:
    bool   init();
    void   onExit();
    ~Layer_Follower();
    LAYER_NODE_FUNC(Layer_Follower);
    
public:
    void CreateFollowerList();
  
public:    
    void ReadyAttack(int profession);
    void StartAction();

public:    
    void CalculateAttackEnemy();
    void CalculateAddHp();
    void CalculateBeAttackResult(int attack);
    
public:    
    void DisplayAttackNumberJump();
    void CheckDisplayAttackNumberJumpOver();
    
public:    
    void DisplayAttack();

public:    
    void DisplayBeAttack(float attack);
    void DisplayBeAttackOver();
    
public:    
    void DisplayAddHp();
    void DisplayAddHpJump();
    void DisplayAddHpJumpFun1();
    void DisplayAddHpJumpFun2();    
    void DisplayAddHpJumpFun3();      
    void DisplayAddHpJumpFun4(cocos2d::CCNode * sender);
    void DisplayAddHpJumpFun5(cocos2d::CCNode * sender);
 
public:
    void DisplayAddHpBySkill(int value);
    void DisplayAddHpBySkillCallBack1();
    void DisplayAddHpBySkillCallBack2(cocos2d::CCNode * sender);
    void DisplayAddHpBySkillCallBack3(cocos2d::CCNode * sender);
    void DisplayAddHpBySkillCallBack4();
    
public:
    void DisplayReliveAddHp();
    void DisplayReliveAddHpCallBack1(cocos2d::CCNode * sender);
    void DisplayReliveAddHpCallBack2();
    
public:
    void GuideReSetSkillCD();
    
public:
    virtual bool ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    virtual void ccTouchMoved(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    virtual void ccTouchEnded(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);

public:    
    void    AddFollowerCombo(int profession, int dispel, cocos2d::CCPoint startpoint);
    void    DisplayFollowerCombo(int dispelCount);
    
public: 
    void AddDefense(float defense);
    void DecreaseDefense(float defense);
    
    void AddMaxHP(int added);
    
public:
    void    AddBuff(Skill * buff);
    void    DisplayBuff();
    void    RemoveBuff(Skill * debuff, bool needclose);
    void    ReSetPositionBuff(cocos2d::CCNode * sender, void * data);
    
    void    TriggerBuff();
    void    TriggerBuffFun();
    void    TriggerOver();
    
public:
    void DecreaseSkillCDTime();
    void TriggerTeamSkill();
    
public:
    SS_GET(Battle_Follower*, m_FollowerList[0], CaptainFollower);
    
    SS_GET(Battle_FollowerList*, &m_FollowerList, FollowerList);
    SS_GET(list<Battle_Follower*>*, &m_FollowerFightList, FollowerFightList);
    SS_GET(cocos2d::CCPoint, m_HpPoint, HpPoint);

    SS_GET(float, m_FollowerHP, FollowerHP);
    SS_GET(float, m_OriginalDefense, OriginalDefense);
    SS_GET(float, m_FollowerDefense, FollowerDefense);
    SS_GET(float, m_FollowerAddedHp, FollowerAddedHp);
    SS_GET(float, m_OriginalHP, OriginalHP);
    SS_GET(bool, m_TriggerBuffing, TriggerBuffing);
    
public:
    GET_SET(int, m_TeamOverComePro, TeamOverComePro);
    GET_SET(int, m_TeamBeOverComePro, TeamBeOverComePro);
    GET_SET(float, m_OverComeAttackPercent, OverComeAttackPercent);
    GET_SET(float, m_AddAttackPercent, AddAttackPercent);
    GET_SET(int, m_ProDerate, ProDerate);
    GET_SET(float, m_ProDerateAttackPercent, ProDerateAttackPercent);
    GET_SET(int, m_DerateAttack, DerateAttack);
    GET_SET(float, m_AvoidDeadlyAttackPercent, AvoidDeadlyAttackPercent);
    GET_SET(bool, m_OperationEnable, OperationEnable);
    
private:
    static  Layer_Follower * m_Instance;
    
protected:
    cocos2d::CCSize             m_RealSize;
    cocos2d::CCSize             m_WinSize;
    cocos2d::CCPoint            m_HpPoint;
    cocos2d::CCPoint            m_AddHpLabelPoint;
    cocos2d::CCSize             m_NumSize_PlayerHP;
    cocos2d::CCSize             m_NumSize_AddHP;      
    
private:
    Battle_FollowerList         m_FollowerList;
    list <Battle_Follower *>    m_FollowerFightList;
    
    float                       m_FollowerHP;
    float                       m_OriginalHP;
    float                       m_LabelHPString;
    
    float                       m_FollowerHurtHp;
    
    float                       m_FollowerAddedHp;
    float                       m_AddedHp;
    float                       m_LabelAddedHpString;
    
    float                       m_FollowerDefense;
    float                       m_OriginalDefense;
  
    bool                        m_IsFollowerAttack;
    bool                        m_OperationEnable;
    bool                        m_TriggerBuffing;
    bool                        m_CanBeDecreaseSkillCD;
    
    
    list <Skill*>               m_FollowerBuffList;
    list <Skill*>               m_FollowerTempBuffList;
    
    ////////关于技能的////////
    //职业相克的
    int                         m_TeamOverComePro;
    int                         m_TeamBeOverComePro;
    float                       m_OverComeAttackPercent;
    
    //攻击加成的
    float                       m_AddAttackPercent;
    
    //对不同职业敌人的减免伤害的
    int                         m_ProDerate;
    float                       m_ProDerateAttackPercent;
    
    //直接减免队伍受到的伤害的
    int                         m_DerateAttack;
    
    //防止被秒杀的
    float                       m_AvoidDeadlyAttackPercent;
    
    ////////计算combo用的////////
    list <int>                  m_ReadyProfession;
    
    int                         m_NoDispelCount;
    
    int                         m_AllCombo;
    map <int, int>              m_AllDispel;
    
    map <int, int>              m_FollowerCombo;
    map <int, int>              m_FollowerDispel;
};

#endif






























