//
//  Layer_SkillInfoBoard.h
//  都市猎人
//
//  Created by 张 强 on 12-9-29.
//
//

#ifndef _____Layer_SkillInfoBoard_h
#define _____Layer_SkillInfoBoard_h

#include "cocos2d.h"
#include "MyConfigure.h"


#define  BoardChildTag_BG 0

#define  BoardChildTag_BTN_Left  1
#define  BoardChildTag_BTN_Left_S  2
#define  BoardChildTag_BTN_LetWord 3

#define  BoardChildTag_BTN_Right  4
#define  BoardChildTag_BTN_Right_S  5
#define  BoardChildTag_BTN_RightWord 6

#define  BoardChildTag_Word_FollowerName 7
#define  BoardChildTag_Word_SkillName 8
#define  BoardChildTag_Word_SkillLevel 9
#define  BoardChildTag_Word_SkillInfo 10
#define  BoardChildTag_Word_SkillCD 11

class Battle_Follower;
class Layer_SkillInfoBoard : public cocos2d::CCLayer
{
public:
    static Layer_SkillInfoBoard * InitBoard(Battle_Follower * host);
    ~Layer_SkillInfoBoard();
    
public:
    SS_SET(Battle_Follower*, m_InfoHost, InfoHost);
   
protected:
    bool init();
    void onExit();

    virtual bool ccTouchBegan(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    virtual void ccTouchMoved(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);
    virtual void ccTouchEnded(cocos2d::CCTouch * pTouch, cocos2d::CCEvent * pEvent);

protected:
    void  Leave();
    void  LeaveFunc1();
    
protected:
    cocos2d::CCSize             m_WinSize;
    cocos2d::CCRect             m_Rect_ShiFang;
    cocos2d::CCRect             m_Rect_QuXiao;
    
private:
    Battle_Follower *           m_InfoHost;
    bool                        m_CanbeTrigger;
};

#endif
