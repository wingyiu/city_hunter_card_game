//
//  Sprite_Block.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-6-7.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Sprite_Block.h"

/************************************************************************/
#pragma mark - Sprite_Block类静态函数 : 从文件创建方块精灵函数
/************************************************************************/
Sprite_Block* Sprite_Block::blockSpriteWithFile(const char* filename)
{
    Sprite_Block* pSprite = new Sprite_Block();
    if (pSprite != NULL)
    {
        if (pSprite->initWithFile(filename) == true)
        {
            pSprite->autorelease();
            return pSprite;
        }
    }
    
    delete pSprite;
    pSprite = NULL;
    
    return NULL;
}

/************************************************************************/
#pragma mark - Sprite_Block类静态函数 : 从缓存创建方块精灵函数
/************************************************************************/
Sprite_Block* Sprite_Block::blockSpriteWithSpriteFrameName(const char* filename)
{
    Sprite_Block* pSprite = new Sprite_Block();
    if (pSprite != NULL)
    {
        if (pSprite->initWithSpriteFrameName(filename) == true)
        {
            pSprite->autorelease();
            
            return pSprite;
        }
    }
    
    delete pSprite;
    pSprite = NULL;
    
    return NULL;
}

/************************************************************************/
#pragma mark - Sprite_Block类构造函数
/************************************************************************/
Sprite_Block::Sprite_Block()
{
    // 为方块属性变量赋初值
    m_eBlockType = BLOCK_NONE;
    
    m_iRow = 0;
    m_iCol = 0;
    
    m_bIsHorizonDraw = false;
    m_bIsVeticalDraw = false;
    
    m_bIsBigClear = false;
}

/************************************************************************/
#pragma mark - Sprite_Block类析构函数
/************************************************************************/
Sprite_Block::~Sprite_Block()
{
    
}

/************************************************************************/
#pragma mark - Sprite_Block类重写函数 : 访问函数
/************************************************************************/
void Sprite_Block::visit()
{
    // 不可见，则跳过
    if (!m_bIsVisible)
		return;
    
    //glDisable(GL_DEPTH_TEST);
    
    // 调用基类visit函数
    CCSprite::visit();
    
    // 获得原先坐标
    CCPoint pos = getPosition();
    
    // 绘制横向
    if (m_bIsHorizonDraw)
    {
        setPosition(ccp(pos.x - 53.0f * 6, pos.y));
        CCSprite::visit();
        
        setPosition(ccp(pos.x + 53.0f * 6, pos.y));
        CCSprite::visit();
    }
    
    // 绘制竖向
    if (m_bIsVeticalDraw)
    {
        setPosition(ccp(pos.x, pos.y - 53.0f * 5));
        CCSprite::visit();
        
        setPosition(ccp(pos.x, pos.y + 53.0f * 5));
        CCSprite::visit();
    }
    
    // 还原坐标
    setPosition(pos);
    
    //glEnable(GL_DEPTH_TEST);
}