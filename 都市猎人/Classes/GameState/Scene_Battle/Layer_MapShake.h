//
//  Layer_MapShake.h
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-5-23.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_MapShake_h
#define MidNightCity_Layer_MapShake_h

#include "cocos2d.h"

USING_NS_CC;

// shake类型枚举
enum SHAKE_TYPE
{
    SHAKE_RAND = 0,                 // 随机抖动
    SHAKE_UPDOWN,                   // 上下移动
    SHAKE_UPDOWN1,                  // 上下移动1
    SHAKE_LIFTRIGHT,                // 左右移动
};

class Layer_MapShake : public CCActionInterval
{
public:
    #pragma mark - 静态函数 : 创建震动函数
    static Layer_MapShake* actionWithDuration(ccTime fDuration, CCPoint oriPos, SHAKE_TYPE eType = SHAKE_RAND);
public:
    #pragma mark - 初始化震动函数
    bool initWithDuration(ccTime fDuration, CCPoint oriPos, SHAKE_TYPE eType);
    
    #pragma mark - 获得原始位置函数
    CCPoint GetOriginPos() const        {return m_oriPos;}
    
    #pragma mark - 逻辑更新函数
    virtual void update(ccTime time);
private:
    #pragma mark - 构造函数
    Layer_MapShake();
    #pragma mark - 析构函数
    virtual ~Layer_MapShake();
    
    #pragma mark - 随机抖动函数
    void ShakeWithRand();
    #pragma mark - 上下抖动函数
    void ShakeWithUpDown();
    #pragma mark - 上下抖动1函数
    void ShakeWithUpDown1();
    #pragma mark - 左右移动函数
    void ShakeWithLeftRight();
private:
    CCPoint m_shakePos;                 // 震动位置
    CCPoint m_oriPos;                   // 原始位置
    
    int m_iShakeSpeed;                  // 震动频率
    
    SHAKE_TYPE m_eType;                 // 震动类型
};

#endif
