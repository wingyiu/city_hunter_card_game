//
//  Layer_Display.h
//  MidNightCity
//
//  Created by 强 张 on 12-6-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Layer_Display_h
#define MidNightCity_Layer_Display_h

#include "cocos2d.h"

#define  Layer_DisplayChild_Game  0
#define  Layer_DisplayChild_Over   1
#define Layer_DisplayChild_Warning1 2
#define Layer_DisplayChild_Warning2 3
#define Layer_DisplayChild_WarningTitle1 4
#define Layer_DisplayChild_WarningTitle2 5
#define Layer_DisplayChild_WarningColor 6
#define Layer_DisplayChild_IconMask 7
#define Layer_DisplayChild_Message 8

#define Layer_DisplayChild_HurtMask1 9
#define Layer_DisplayChild_HurtMask2 10
#define Layer_DisplayChild_HurtMask3 11
#define Layer_DisplayChild_HurtMask4 12
#define Layer_DisplayChild_HurtMask5 13

#define Layer_DisplayChild_SA_Left 14
#define Layer_DisplayChild_SA_Right 15
#define Layer_DisplayChild_SA_Icon1 16
#define Layer_DisplayChild_SA_Icon1_1 17
#define Layer_DisplayChild_SA_Icon2 18
#define Layer_DisplayChild_SA_Icon2_1 19

#define Layer_DisplayChild_GuideInfo1  21
#define Layer_DisplayChild_GuideInfo2  22
#define Layer_DisplayChild_GuideInfo3  23
#define Layer_DisplayChild_GuideInfo4  24
#define Layer_DisplayChild_GuideInfo5  25


#define  ActionTag_BaAttackFlash  0
#define  ActionTag_BreakShake 1

class Battle_Follower;
class Follower;
class Battle_Enemy;
class Layer_Display : public cocos2d::CCLayer
{
    friend class Scene_Battle;
    
public:
    static Layer_Display * ShareInstance();
    
protected:
    bool init();
    void onExit();
    ~Layer_Display();
    LAYER_NODE_FUNC(Layer_Display);
    
public:
    int     m_Round;
    int     m_StepRecord;    

public:
    int     m_BattleAllAttack;
    int     m_BattleMaxAttack;
   
public:
    void    OperationEnable(bool enable); 
    
public:
    void    DisplayCombo(cocos2d::CCPoint point, int combo);
    void    DisplayComboCallBack1(cocos2d::CCNode * sender);
    void    DisplayComboCallBack2(cocos2d::CCNode * sender);
    void    DisplayComboCallBack3(cocos2d::CCNode * sender);    
 
public:
    void    DisplayFollowerHurt(cocos2d::CCPoint point, int hurt);
    void    DisplayFollowerHurtCallBack1(cocos2d::CCNode * sender);

public:
    void    DisplayFollowerBeAttack();
    void    FlashOver(cocos2d::CCNode * sender);
    void    DisplayFollowerBeAttackOver();
    void    DisplayFollowerBeAttackOverFunc1(cocos2d::CCNode * sender);
    
public:
    void    DisplayFollowerStartAttack(int profession, cocos2d::CCPoint startpoint);
    void    AttackBallArrival(cocos2d::CCNode * sender, void * data);
    void    AddHpBallArrival(cocos2d::CCNode * sender);
    void    BallDispel(CCNode * sender);    

public:
    void    DisplayFollowerAttackAll(int profession);
    void    DisplayFollowerAttackRemove(cocos2d::CCNode * sender);
    
public:
    void    DisplayEnemyDropMoney(cocos2d::CCPoint startpoint, Battle_Enemy * BE);
    void    DisplayEnemyDropMoneyCallBack1(cocos2d::CCNode * sender, void * data);
    void    DisplayEnemyDropMoneyCallBack2(cocos2d::CCNode * sender, void * data);    

public:
    void    DisplayEnemyDropRes(cocos2d::CCPoint startpoint, Battle_Enemy * BE);
    void    DisplayEnemyDropresCallBack1(cocos2d::CCNode * sender, void * data);
    void    DisplayEnemyDropResCallBack2(cocos2d::CCNode * sender, void * data); 
    
public:
    void    DisplaySkillStart(Battle_Follower * BF);
    void    DisplaySkillStartOver();
    void    DisplaySkillStartFunc1();
    void    DisplaySkillStartFunc2(cocos2d::CCNode * sender);
    
    void    DisplaySkillAnimation();
    void    DisplaySkillAnimationFunc1(cocos2d::CCNode * sender);
    void    DisplaySkillAnimationFunc2(cocos2d::CCNode * sender);
    void    DisplaySkillAnimationFunc3(cocos2d::CCNode * sender);
    void    DisplaySkillAnimationFunc4(cocos2d::CCNode * sender);
    
protected:
    cocos2d::CCPoint        m_DestPoint;
    int                     m_Count;
    const char *            m_SA_Icon1;
    const char *            m_SA_Icon1_1;
    const char *            m_SA_Icon2;
    const char *            m_SA_Icon2_1;
    const char *            m_SA_Icon3Left;
    const char *            m_SA_Icon3Right;
    
public:
    void    DisplayWarning();
    void    DisplayWarningCallBack1();
    void    DisplayWarningOver();
    void    DisplayWarningOverCallBack1(cocos2d::CCNode * sender);    
    void    DisplayWarningOverCallBack2(cocos2d::CCNode * sender);
    
public:
    void    DisplayGameOver();
    void    DisplayGameOverCallBack1();
    void    DisplayGameOverCallBack2();
    void    DisplayGameOverCallBack3();
   
public:
    void    DisplayGameOverRemove();
    void    DisplayGameOverRemoveCallBack1();
    
public:
    void    DisplayBullShit(int index);
    void    DisplayBullShitFunc1(cocos2d::CCNode * sender);
    
    
    void    DisplayTest();
    
protected:
    cocos2d::CCSize         m_WinSize;
    cocos2d::CCSize         m_NumSize_Combo;
    cocos2d::CCSize         m_NumSize_Great;
    cocos2d::CCSize         m_NumSize_Perfect;
    cocos2d::CCSize         m_NumSize_DecreaseHP;

    
private:
    static Layer_Display *          m_Instance;
    
    Battle_Follower *               m_Caller;
};

#endif

















