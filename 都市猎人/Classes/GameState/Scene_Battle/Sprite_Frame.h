//
//  Sprite_Frame.h
//  Test
//
//  Created by 惠伟 孙 on 12-6-15.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef Test_Sprite_Frame_h
#define Test_Sprite_Frame_h

#include "cocos2d.h"

USING_NS_CC;

class Sprite_Frame : public CCSprite
{
public:
    #pragma mark - 静态函数 : 创建外框对象
    static Sprite_Frame* frameWithSpriteFrameName(const char* filename);
    
    #pragma mark - 设置横向和竖向绘制标志
    void SetHorizonAndVeticalDraw(bool bIsHorizon, bool bIsVetical)         {m_bIsHorizonDraw = bIsHorizon; m_bIsVeticalDraw = bIsVetical;}
private:
    #pragma mark - 构造函数
    Sprite_Frame();
    #pragma mark - 析构函数
    ~Sprite_Frame();
    
    #pragma mark - 重写函数 : 访问函数
    virtual void visit();
private:
    bool m_bIsHorizonDraw;                          // 横向绘制
    bool m_bIsVeticalDraw;                          // 竖向绘制
};

#endif
