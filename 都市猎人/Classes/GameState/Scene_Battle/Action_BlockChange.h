//
//  Action_BlockChange.h
//  Test
//
//  Created by 惠伟 孙 on 12-6-13.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef Test_Action_BlockChange_h
#define Test_Action_BlockChange_h

#include "cocos2d.h"

USING_NS_CC;

// 消除状态枚举
enum CHANGE_STATE
{     
    ROTATE_OUT = 0,  
    REINIT,
    ROTATE_IN,
    RESET,
    FINISH,
};

// 方块旋转枚举
enum BLOCK_ROTATE_STATE
{
    BLOCK_NORMAL = 0,                       // 正常方块消除
    BLOCK_LEFT,                             // 方块从左往右转
    BLOCK_RIGHT,                            // 方块从右往左转
};

// 方块消除action
class Action_BlockChange : public CCActionInterval
{
public:
    #pragma mark - 静态函数 : 创建方块变换事件
    static Action_BlockChange* actionWithBlockInfo(float fDuration, int iRow, int iCol, BLOCK_ROTATE_STATE eState = BLOCK_NORMAL, bool bIsEnded = false);
    #pragma mark - 初始化震动函数
    bool initWithDuration(float fDuration, int iRow, int iCol);
private:
    #pragma mark - 构造函数
    Action_BlockChange();
    #pragma mark - 析构函数
    ~Action_BlockChange();
    
    #pragma mark - 逻辑更新函数
    virtual void update(ccTime time);
    
    #pragma mark - 正常翻转更新函数
    void updateByNormal(ccTime time);
    #pragma mark - 左翻转更新函数
    void updateByLeft(ccTime time);
    #pragma mark - 右翻转更新函数
    void updateByRight(ccTime time);
    
    #pragma mark - 更新立方体信息函数
    void UpdateCubeInfoByRowAndCol(CCSpriteFrame* pFrame, int iRow, int iCol, int iType);
    #pragma mark - 更新方块信息函数
    void UpdateBlockInfoByRowAndCol(CCSpriteFrame* pFrame, int iRow, int iCol, int iType);
private:
    // 方块消除属性变量
    CHANGE_STATE m_eChangeState;            // 方块消除状态
    
    int m_iRow;                             // 方块所在行
    int m_iCol;                             // 方块所在列
    
    float m_fRotateSpeed;                   // 立方体旋转速度
    
    bool m_bIsEnded;                        // 是否结束，true :全部为黑色  false : 重开方块
    
    BLOCK_ROTATE_STATE m_eState;            // 方块状态
};

#endif
