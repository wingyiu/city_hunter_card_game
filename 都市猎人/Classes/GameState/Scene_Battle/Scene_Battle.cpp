//
//  Scene_Battle.cpp
//  MidNightCity
//
//  Created by 惠伟 孙 on 12-4-23.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Scene_Battle.h"
#include "GameController.h"
//#include "Layer_BattleResult.h"

int Scene_Battle::m_MissionFriendFollowerID = 0;
int Scene_Battle::m_MissionFriendValue = 0;

/************************************************************************/
#pragma mark - Scene_Battle类创建场景函数
/************************************************************************/
CCScene* Scene_Battle::scene()
{
    CCScene* pScene = CCScene::node();
    if (pScene == NULL)
        return NULL;
    
    Scene_Battle* pLayer = Scene_Battle::node();
    if (pLayer != NULL)
        pScene->addChild(pLayer);
    
    return pScene;
}

Scene_Battle::~Scene_Battle()
{
    printf("Scene_Battle:: 释放完成\n");
}

/************************************************************************/
#pragma mark - Scene_Battle类初始化函数
/************************************************************************/
bool Scene_Battle::init()
{
    // 初始化基类
    if (CCLayer::init() == false)
        return false;
    
    // 初始化战斗显示层
    Layer_Display * LD = Layer_Display::node();
    LD -> setVertexZ(6.0f);
    this -> addChild(LD, 1, SceneBattleChild_Layer_Display);
    
    // 初始化敌人层
    Layer_Enemy * LE = Layer_Enemy::node();
    LE -> setVertexZ(0.0f);
    this -> addChild(LE, 0, SceneBattleChild_Layer_Enemy);
    
    // 初始化小弟层
    Layer_Follower * LF = Layer_Follower::node();
    LF -> setVertexZ(1.0f);
    this -> addChild(LF, 0, SceneBattleChild_Layer_Follower);
    
    // 初始化方块层
    Layer_Block * LB = Layer_Block::node();
    LB -> SetVisibleRange(0.0f, 0.0f, 320.0f, 265.0f);
    LB -> setVertexZ(2.0f);
    this -> addChild(LB, 0, SceneBattleChild_Layer_Block);
    
    LD -> OperationEnable(false);

    // 初始化uimanager
    KNUIManager * pUIManager = KNUIManager::CreateManager();
    if (pUIManager->LoadUIConfigFromFile("battleUI_config.xml") == true)
    {
        pUIManager->setVertexZ(7.0f);
        this -> addChild(pUIManager, 1, SCENE_UI_ID);
    }
    
//    Layer_BattleResult* pResult = Layer_BattleResult::node();
//    pResult->setVertexZ(10.0f);
//    addChild(pResult);
    
    //this -> setIsTouchEnabled(true);
    //CCTouchDispatcher::sharedDispatcher() -> addTargetedDelegate(this, 0, true);
    
    // 预先加载音效
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("14.wav");
    
    return true;
}

/************************************************************************/
#pragma mark - Scene_Battle类退出函数
/************************************************************************/
void Scene_Battle::onExit()
{
    // 调用基类退出函数
    CCLayer::onExit();
    
    // 移除战斗显示层
    Layer_Display * LD = dynamic_cast<Layer_Display*>(this -> getChildByTag(SceneBattleChild_Layer_Display));
    this -> removeChild(LD, true);
    
    // 移除敌人层
    Layer_Enemy * LE = dynamic_cast<Layer_Enemy*>(this -> getChildByTag(SceneBattleChild_Layer_Enemy));
    this -> removeChild(LE, true);
    
    // 移除小弟层
    Layer_Follower * LF = dynamic_cast<Layer_Follower*>(this -> getChildByTag(SceneBattleChild_Layer_Follower));
    this -> removeChild(LF, true);
    
    // 移除方块层
    Layer_Block * LB = dynamic_cast<Layer_Block*>(this -> getChildByTag(SceneBattleChild_Layer_Block));
    this -> removeChild(LB, true);
    
    // 释放副本信息
    ServerDataManage::ShareInstance() -> ReleaseBattleInfo();
    
    // 移出ui层
    this -> removeChild(getChildByTag(SCENE_UI_ID), true);
    
    // 移除场景所有节点，再做一次清除
    removeAllChildrenWithCleanup(true);
    this -> removeFromParentAndCleanup(true);
    
    //移除队伍中的好友小弟
    PlayerDataManage::ShareInstance() -> RemoveFriendFollowerFromTeam();
    
    // 移除资源 - battle场景资源
    RemoveRes("battle1-hd.plist", "battle1-hd.pvr.ccz");
    RemoveRes("battle2-hd.plist", "battle2-hd.pvr.ccz");
    RemoveRes("battle3-hd.plist", "battle3-hd.pvr.ccz");
    RemoveRes("anim1-hd.plist", "anim1-hd.pvr.ccz");
}


/*bool Scene_Battle::ccTouchBegan(CCTouch * pTouch, CCEvent * pEvent)
{
    GameController::ShareInstance() -> ReadyToGameScene(GameState_BackToGame);
    return true;
}*/





















