//
//  Sprite_Cube.h
//  Test
//
//  Created by 惠伟 孙 on 12-6-11.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef Test_Sprite_Cube_h
#define Test_Sprite_Cube_h

#include "cocos2d.h"

USING_NS_CC;

class Sprite_Cube : public CCSprite
{
public:
    #pragma mark - 静态函数 : 立方体初始化函数
    static Sprite_Cube* cubeWithSpriteFrameName(const char* filename);
    
    #pragma mark - 初始化方块纹理桢函数
    void InitBlockFrame(CCSpriteFrame* pFrame);
    #pragma mark - 初始化立方体纹理函数
    void InitCubeFrame(CCSpriteFrame* pFrame);
    
    #pragma mark - 重写位置设置函数
    //virtual void setPosition(const CCPoint& pos);
    #pragma mark - 重写获得位置函数
    //const CCPoint& getPosition()                                        {return m_oriPosition;}
    
    #pragma mark - 设置旋转函数
    void SetCubeRotation(float x, float y, float z)                     {m_fRotationX = x; m_fRotationY = y; m_fRotationZ = z;}
    #pragma mark - 获得旋转函数
    void GetCubeRotation(float& x, float& y, float& z)                  {x = m_fRotationX; y = m_fRotationY; z = m_fRotationZ;}
    
    #pragma mark - 设置方块类型函数
    void SetBlockTempType(int iType)                                    {m_iBlockType = iType;}
    #pragma mark - 获得方块类型函数
    int GetBlockTempType() const                                        {return m_iBlockType;}
    
    void setOpacity(GLubyte opacity)                                    {m_bAlpha = opacity;}
    
    #pragma mark - 设置横和竖绘制标志函数
    void SetHorizonAndVeticalDraw(bool bIsHorizon, bool bIsVetical)     {m_bIsHorizonDraw = bIsHorizon; m_bIsVeticalDraw = bIsVetical;}
private:
    #pragma mark - 构造函数
    Sprite_Cube();
    #pragma mark - 析构函数
    ~Sprite_Cube();
    
    #pragma mark - 初始化侧面函数
    //void InitCubeOhterface();
    
    #pragma mark - 重写访问函数
    virtual void visit();
    #pragma mark - 重写绘制函数
    virtual void draw();
    
    #pragma mark - 立方体访问函数
    void CubeVisit();
private:
    // 立方体对象变量
    CCSpriteFrame* m_pCubeFrame;                                        // 立方体纹理桢
    CCSpriteFrame* m_pBlockFrame;                                       // 方块纹理桢
    CCSpriteFrame* m_pSideFrame;                                        // 侧面纹理桢
    
    // 立方体属性变量
    CCPoint m_oriPosition;                                              // 立方体原位置
    CCPoint m_Position;                                                 // 立方体位置
    
    float m_fRotationX;                                                 // 立方体x轴旋转
    float m_fRotationY;                                                 // 立方体y轴旋转
    float m_fRotationZ;                                                 // 立方体z轴旋转
    
    float m_fThickness;                                                 // 立方体厚度值
    
    int m_iBlockType;                                                   // 记录方块类型，在最后初始化精灵用
    
    bool m_bIsHorizonDraw;                                              // 是否绘制横向的两边
    bool m_bIsVeticalDraw;                                              // 是否绘制竖向的两边
    
    GLubyte m_bAlpha;
    
    ccColor4B m_stVertexColor;
    
    ccV3F_C4B_T2F m_CubeSideVertices[4];                                // 立方体侧面顶点
    ccV3F_C4B_T2F m_CubeBottomVertices[4];                              // 立方体底面顶点
};

#endif
