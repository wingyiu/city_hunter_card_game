//
//  MyCircularityAction.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-7-4.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "MyCircularityAction.h"
USING_NS_CC;

MyCircularityAction * MyCircularityAction::actionWithDuration(ccTime t, const MyCircularityConfig & c)
{  
    MyCircularityAction * pMyCircularityAction = new MyCircularityAction();  
    pMyCircularityAction -> initWithDuration(t, c);  
    pMyCircularityAction -> autorelease();  
    
    return pMyCircularityAction;  
}  

bool MyCircularityAction::initWithDuration(ccTime t, const MyCircularityConfig & c)
{  
    if (CCActionInterval::initWithDuration(t))  
    {  
        m_CircularityConfig = c;  
        return true;  
    }  
    
    return false;  
}  

//返回X坐标  
float MyCircularityAction::CircularityXat( float a, float bx, float c, ccTime t )
{  
    return -a * cos(2 * 3.1415926 * t) + a;  
}  

//返回Y坐标  
float MyCircularityAction::CircularityYat( float a, float by, float c, ccTime t )
{  
    float b = sqrt(powf(a, 2) - powf(c, 2));
    return b * sin(2 * 3.1415926 * t);  
}  

void MyCircularityAction::update(ccTime time)  
{  
    if (m_pTarget)  
    {  
        CCPoint s_startPosition = m_CircularityConfig.CenterPosition;//中心点坐标  
        float a = m_CircularityConfig.aLength;  
        float bx = m_CircularityConfig.CenterPosition.x;  
        float by = m_CircularityConfig.CenterPosition.y;  
        float c = m_CircularityConfig.cLength;  
        float x = CircularityXat(a, bx, c, time);//调用之前的坐标计算函数来计算出坐标值  
        float y = CircularityYat(a, by, c, time);  
        m_pTarget -> setPosition(ccpAdd(s_startPosition, ccp(x-a, y)));//由于我们画计算出的椭圆你做值是以原点为中心的，所以需要加上我们设定的中心点坐标  
    }  
}  










