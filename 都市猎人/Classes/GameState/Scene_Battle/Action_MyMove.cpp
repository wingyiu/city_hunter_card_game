//
//  Action_MyMove.cpp
//  都市猎人
//
//  Created by 张 强 on 12-9-26.
//
//

#include "Action_MyMove.h"
USING_NS_CC;

    
Action_MyMove * Action_MyMove::actionWithDuration(int times, int deep, ccTime speed)
{
    Action_MyMove * pMyMove = new Action_MyMove();
	pMyMove -> initWithDuration(times, deep, speed);
	pMyMove -> autorelease();
    
	return pMyMove;
}

bool Action_MyMove::initWithDuration(int times, int deep, ccTime speed)
{
    if (CCActionInterval::initWithDuration(100))
	{
        m_IsMoving = false;
        m_MoveTimes = times;
        m_MoveCount = 0;
        m_MoveDeep = deep + 1;
        m_MoveSpeed = speed;
        
		return true;
	}
    
	return false;
}

void Action_MyMove::startWithTarget(CCNode * pTarget)
{
    CCActionInterval::startWithTarget(pTarget);
    
	m_StartPosition = pTarget -> getPosition();
    MakeDestPoint();
}

void Action_MyMove::MakeDestPoint()
{
    float destx = ((rand() % 2) == 0) ? (m_StartPosition.x - rand() % m_MoveDeep) : (m_StartPosition.x + rand() % m_MoveDeep);
    float desty = ((rand() % 2) == 0) ? (m_StartPosition.y - rand() % m_MoveDeep) : (m_StartPosition.y + rand() % m_MoveDeep);
    m_DestPosition = CCPointMake(destx, desty);
    
    float dx = (destx > m_pTarget -> getPosition().x) ? (destx - m_pTarget -> getPosition().x) : (m_pTarget -> getPosition().x - destx);
	float dy = (desty > m_pTarget -> getPosition().y) ? (desty - m_pTarget -> getPosition().y) : (m_pTarget -> getPosition().y - desty);
	float dist = sqrt(dx * dx + dy * dy);
    m_MoveDuration = m_MoveSpeed * dist;
}

void Action_MyMove::update(cocos2d::ccTime time)
{
    if(! m_IsMoving)
    {
        m_IsMoving = true;
        
        CCMoveTo * pMove = CCMoveTo::actionWithDuration(m_MoveDuration, m_DestPosition);
        CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Action_MyMove::MoveOver));
        m_pTarget -> runAction(CCSequence::actions(pMove, pCallFuncN, NULL));
    }
}

void Action_MyMove::MoveOver(CCNode * sender)
{
    if(m_MoveCount + 1 < m_MoveTimes)
    {
        m_MoveCount += 1;
        MakeDestPoint();
        m_IsMoving = false;
    }
    else
        m_MoveCount = m_MoveTimes;
}

bool Action_MyMove::isDone()
{
    if(m_MoveCount >= m_MoveTimes)
    {
        float dx = (m_StartPosition.x > m_pTarget -> getPosition().x) ? (m_StartPosition.x - m_pTarget -> getPosition().x) : (m_pTarget -> getPosition().x - m_StartPosition.x);
        float dy = (m_StartPosition.y > m_pTarget -> getPosition().y) ? (m_StartPosition.y - m_pTarget -> getPosition().y) : (m_pTarget -> getPosition().y - m_StartPosition.y);
        float dist = sqrt(dx * dx + dy * dy);
        m_MoveDuration = m_MoveSpeed * dist;
        
        CCMoveTo * pMove = CCMoveTo::actionWithDuration(m_MoveDuration, m_StartPosition);
        m_pTarget -> runAction(pMove);
        
        return true;
    }
    return false;
}





















