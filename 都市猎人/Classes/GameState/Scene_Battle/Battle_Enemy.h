//
//  Battle_Enemy.h
//  MidNightCity
//
//  Created by 强 张 on 12-5-26.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Battle_Enemy_h
#define MidNightCity_Battle_Enemy_h

#include "cocos2d.h"
#include "Enemy_Configure.h"

#define  EnemyTag_Icon      0
#define  EnemyTag_HpBg        1
#define  EnemyTag_Hp        2
#define  EnemyTag_ProIcon        3
#define  EnemyTag_Post      4
#define  EnemyTag_CDNum       5


#define  ActionTag_IconShake   0

struct HurtValue
{
    HurtValue(int hurt, int realhurt) {Hurt = hurt; RealHurt = realhurt;}
    int Hurt;
    int RealHurt;
};
typedef list <HurtValue*> EnemyHurtlist;


class Skill;
class Battle_Follower;
class Battle_Enemy : public cocos2d::CCLayer
{
    friend class Battle_EnemyManage;
    
public:
    virtual bool init();
    ~Battle_Enemy();
    void   onExit();
    
    void SetData(Enemy_Data * Data);
    
    MY_LAYER_NODE_FUNC(Battle_Enemy);
    virtual void Initialize();
    
public:
    void    DisplaySelf();
    void    RemoveAppearAnimation(cocos2d::CCNode * sender);
    void    ShowSelf();

public:
    void    DisplayDead();
    void    RemoveDeadAnimation(cocos2d::CCNode * sender);
    void    DisappearSelf();
    void    RemoveBEChild(cocos2d::CCNode * sender);
    void    DisappearSelfOver(cocos2d::CCNode * sender);
    
    void    DisplayDeadCallBack2(cocos2d::CCNode * sender);  
    void    SetDisplayDeadOver();
    
public:
    void    CalculateAttackFollower();
    void    CalculateBeAttackByFollower(int value, Battle_Follower * Sender);
    
public:    
    void    CalculateDecreaseDefense(float value);
    void    CalculatePlusDefense(float value);
    
    void    CalculateDecreaseAttack(float value);
    void    CalculatePlusAttack(float value);
    
    void    CalculateDecreaseHP(float value);
    
public:
    void    DisplayPost();
    void    HidePost();
    
public:    
    void    DisplayBeAttack(bool isAnimation, int mode, Battle_Follower * sender);
    void    RemoveAnimation(cocos2d::CCNode * sender);
    void    NumMoveOver(cocos2d::CCNode * sender);
    void    RemoveNum(cocos2d::CCNode * sender);
    void    NumChange(cocos2d::CCNode * sender, void * data);
   
public:
    void    DisplayBeAttackBySkill(Battle_Follower * sender);
    void    RemoveSkillAnimation(cocos2d::CCNode * sender);
    
public: 
    void    DisplayBeAttackByKeepSkill();
    
public:  
    void    DisplayAttack();
    void    DisplayAttackCallBack1(cocos2d::CCNode * sender);
    void    DisplayAttackCallBack2(cocos2d::CCNode * sender);    
    void    DisplayAttackCallBack3(cocos2d::CCNode * sender);
  
public:
    void    DisplayCDTime();
    void    DisplayCDTimeCallBack1(cocos2d::CCNode * sender);
    void    DisplayCDTimeColor();
    void    DisplayAddCDTime();
    void    DisplayReduceCDTime();
    
public:
    bool    CheckBeTouch(cocos2d::CCPoint point);
    void    FollowerSkillAddCDRecord(int count);
    
public:
    SS_GET(Enemy_Data *, &m_Data, Data);
    SS_GET(cocos2d::CCPoint, m_WorldPoint, WorldPoint);
    SS_GET(int, m_CDRecord, CDRecord);

    SS_GET(bool, m_IsDead, IsDead);
    SS_GET(bool, m_IsUnderShowDead, UnderShowDead);
    SS_GET(bool, m_IsUnderShowAttack, UnderShowAttack);
    SS_GET(bool, m_IsUnderShowSkillAnimation, UnderShowSkillAnimation);
  
public:
    GET_SET(bool, m_IsUnderAttack, UnderAttack);
    
protected:
    cocos2d::CCPoint        m_WorldPoint;
    
    cocos2d::CCSize         m_IconSize;
    cocos2d::CCPoint        m_IconCenterPoint;
    cocos2d::CCPoint        m_IconBottomPoint;
    
    cocos2d::CCSize         m_NumSize_Attack1;
    cocos2d::CCSize         m_NumSize_Attack2;
    cocos2d::CCSize         m_NumSize_Attack3;
    cocos2d::CCSize         m_NumSize_Attack4;
    cocos2d::CCSize         m_NumSize_CD;
    
private:
    Enemy_Data              m_Data;
    
    bool                    m_IsDead;
    bool                    m_IsUnderAttack;
    bool                    m_IsUnderShowDead;
    bool                    m_IsUnderShowAttack;
    bool                    m_IsUnderShowSkillAnimation;

    float                   m_RealAttack;
    
    int                     m_CDRecord;
    GLubyte                 m_CDColor;
    int                     m_CDColorDir;
    int                     m_CDColorChange;
    
private:
    EnemyHurtlist          m_HurtValueList;
    
};

#endif























