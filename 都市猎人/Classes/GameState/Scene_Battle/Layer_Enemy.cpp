//
//  Layer_Enemy.cpp
//  MidNightCity
//
//  Created by 强 张 on 12-6-6.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#include "Layer_Enemy.h"
USING_NS_CC;

#include "Enemy.h"
#include "Battle_Enemy.h"

#include "Layer_Follower.h"
#include "Layer_Block.h"
#include "Layer_Display.h"
#include "Layer_MapShake.h"

#include "Layer_BattleResult.h"
#include "Layer_BattleGuideInfo.h"

#include "PlayerDataManage.h"
#include "ServerDataManage.h"
#include "SystemDataManage.h"

#include "GameController.h"

#include "MyMoveAction.h"
#include "Action_MyMove.h"

#include "KNUIFunction.h"
#include "Wrapper.h"

#include "Skill.h"

Layer_Enemy * Layer_Enemy::m_Instance = NULL;


/************************************************************************/
#pragma mark 单例
/************************************************************************/
Layer_Enemy * Layer_Enemy::ShareInstance()
{
    if(m_Instance == NULL)
    {
        return NULL;
    }
    return  m_Instance;
}
/************************************************************************/
#pragma mark -
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 析构
/************************************************************************/
Layer_Enemy::~Layer_Enemy()
{
    m_Instance = NULL;
    
    printf("Layer_Enemy:: 释放完成\n");
}
/************************************************************************/
#pragma mark -
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 初始化
/************************************************************************/
bool Layer_Enemy::init()
{
    if(! CCLayer::init())
        return false;

    this -> setIsTouchEnabled(true);
    CCTouchDispatcher::sharedDispatcher() -> addTargetedDelegate(this, 2, true);
    
    //开场动画显示完毕之后，播放战斗背景音乐
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> playBackgroundMusic("battle.mp3", true);
    
    
    /////////////////////////////////////////////////
    m_IsFirstBattle = true;
    m_IsUnderAttackBySkill = 0;
    m_OperationEnable = true;
    m_WinSize = CCDirector::sharedDirector() -> getWinSize();

    
    ////////UI数字大小////////
    CCSprite * NumImage1 = CCSprite::spriteWithFile("UINum-hd.png");
    m_NumSize = CCSizeMake(NumImage1 -> getTextureRect().size.width / 12, NumImage1 -> getTextureRect().size.height);
    
    
    int TempMapIndex = ServerDataManage::ShareInstance() -> GetSubMapIndex();
    BattleAfficheInfoList TempBattleAfficheInfoList = *ServerDataManage::ShareInstance() -> GetBattleAfficheInfoList();
    BattleAfficheInfoList::iterator it = TempBattleAfficheInfoList.find(TempMapIndex);
    if(it != TempBattleAfficheInfoList.end())
    {
        ////////加入背景图片////////
        if(strcmp(it -> second -> MapImageFilename, "mission1.png") == 0)       m_BGImageName = "BattleBG_1.png";
        else if(strcmp(it -> second -> MapImageFilename, "mission2.png") == 0)  m_BGImageName = "BattleBG_2.png";
        else if(strcmp(it -> second -> MapImageFilename, "mission3.png") == 0)  m_BGImageName = "BattleBG_3.png";
        else if(strcmp(it -> second -> MapImageFilename, "mission4.png") == 0)  m_BGImageName = "BattleBG_4.png";
        else if(strcmp(it -> second -> MapImageFilename, "mission5.png") == 0)  m_BGImageName = "BattleBG_5.png";
        CCSprite * Background = CCSprite::spriteWithSpriteFrameName(m_BGImageName);
        m_BgPoint = CCPointMake(m_WinSize.width / 2, m_WinSize.height - Background -> getTextureRect().size.height / 2);
        Background -> setPosition(m_BgPoint);
        Background -> setOpacity(0);
        Background -> setVertexZ(0.1f);
        this -> addChild(Background, 0, LEChildTag_Background);
        
        
        ////////加入敌人////////
        CreateEnemyList();
        
        
        ////////UI框////////
        CCSprite * UIBackground = CCSprite::spriteWithSpriteFrameName("TopUI.png");
        UIBackground -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height - UIBackground -> getTextureRect().size.height / 2));
        UIBackground -> setOpacity(0);
        UIBackground -> setVertexZ(0.5f);
        this -> addChild(UIBackground, 0, LEChildTag_UIBackground);
        
        CCSprite * pStepIcon = CCSprite::spriteWithSpriteFrameName("StepIcon.png");
        pStepIcon -> setPosition(ccp(43, 470));
        pStepIcon -> setOpacity(0);
        pStepIcon -> setVertexZ(0.6f);
        this -> addChild(pStepIcon, 0, LEChildTag_UIStepIcon);
        
        CCSprite * pStepSpace = CCSprite::spriteWithSpriteFrameName("StepSpace.png");
        pStepSpace -> setPosition(ccp(80, 470));
        pStepSpace -> setOpacity(0);
        pStepSpace -> setVertexZ(0.6f);
        this -> addChild(pStepSpace, 0, LEChildTag_UIStepSpace);
        
        CCSprite * pMoneyIcon = CCSprite::spriteWithSpriteFrameName("DropMoneyIcon.png");
        pMoneyIcon -> setPosition(ccp(123, 470));
        pMoneyIcon -> setOpacity(0);
        pMoneyIcon -> setVertexZ(0.6f);
        this -> addChild(pMoneyIcon, 0, LEChildTag_UIMoneyIcon);
        
        CCSprite * pResIcon = CCSprite::spriteWithSpriteFrameName("DropAnimation_1.png");
        pResIcon -> setPosition(ccp(212, 470));
        pResIcon -> setOpacity(0);
        pResIcon -> setVertexZ(0.6f);
        this -> addChild(pResIcon, 0, LEChildTag_UIResIcon);
        
        
        ////////逃跑按钮////////
        m_OutBattle1 = CCSprite::spriteWithSpriteFrameName("runaway_button.png");
        m_OutBattle1 -> setPosition(ccp(305, 469));
        m_OutBattle1 -> setVertexZ(0.6f);
        this -> addChild(m_OutBattle1, 0, LEChildTag_OutBattle1);
        
        m_OutBattle2 = CCSprite::spriteWithSpriteFrameName("runaway_button_select.png");
        m_OutBattle2 -> setPosition(ccp(305, 469));
        m_OutBattle2 -> setOpacity(0);
        m_OutBattle2 -> setVertexZ(0.6f);
        this -> addChild(m_OutBattle2, 0, LEChildTag_OutBattle2);
        
        
        ////////战斗中玩家获得的钱和物品////////
        m_PlayerBattleMoney = 0;
        m_PlayerBattleExperience = 0;
        m_PlayerBattleRes = 0;
        
        m_LabelPlayerMoneyString = m_PlayerBattleMoney;
        m_LabelPlayerResString = m_PlayerBattleRes;
        
        char buff[32];
        sprintf(buff, "%d", m_LabelPlayerMoneyString);
        m_LabelPlayerMoney = CCLabelAtlas::labelWithString(buff, "UINum-hd.png", m_NumSize.width, m_NumSize.height, '.');
        m_LabelPlayerMoney -> setPosition(ccp(137, 463));
        m_LabelPlayerMoney -> setOpacity(0.0);
        m_LabelPlayerMoney -> setVertexZ(0.6f);
        this -> addChild(m_LabelPlayerMoney);
        
        sprintf(buff, "%d", m_LabelPlayerResString);
        m_LabelPlayerRes = CCLabelAtlas::labelWithString(buff, "UINum-hd.png", m_NumSize.width, m_NumSize.height, '.');
        m_LabelPlayerRes -> setPosition(ccp(226, 463));
        m_LabelPlayerRes -> setOpacity(0.0);
        m_LabelPlayerRes -> setVertexZ(0.6f);
        this -> addChild(m_LabelPlayerRes);
        
        sprintf(buff, "%d", Layer_Display::ShareInstance() -> m_StepRecord);
        m_LabelCurrentStep = CCLabelAtlas::labelWithString(buff, "UINum-hd.png", m_NumSize.width, m_NumSize.height, '.');
        m_LabelCurrentStep -> setPosition(ccp(80 - 8 - strlen(buff) * m_NumSize.width, 463));
        m_LabelCurrentStep -> setOpacity(0.0);
        m_LabelCurrentStep -> setVertexZ(0.6f);
        this -> addChild(m_LabelCurrentStep);
        
        sprintf(buff, "%d", ServerDataManage::ShareInstance() -> GetStepNum());
        m_LabelAllStep = CCLabelAtlas::labelWithString(buff, "UINum-hd.png", m_NumSize.width, m_NumSize.height, '.');
        m_LabelAllStep -> setPosition(ccp(80 + 8, 463));
        m_LabelAllStep -> setOpacity(0.0);
        m_LabelAllStep -> setVertexZ(0.6f);
        this -> addChild(m_LabelAllStep);
        
        
        //是否通过了该副本
        m_bAllClearLevel = false;
        
        ////////这个layer的真实大小////////
        m_RealSize = CCSizeMake(Background -> getTextureRect().size.width, Background -> getTextureRect().size.height);
        
        ////////引导的步骤纪录////////
        m_GuideStep = 0;
        
        DisplayScene();
    }
    else
    {
        CCCallFunc * pFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Enemy::OutGame));
        pFunc -> retain();
        ShowMessage(BType_Firm, "副本战斗背景生成失败，没有找到这个副本的信息", pFunc);
    }
    
    m_Instance = this;

    return true;
}
/************************************************************************/
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark 退出释放
/************************************************************************/
void Layer_Enemy::onExit()
{
    CCLayer::onExit();
    
    CCTouchDispatcher::sharedDispatcher() -> removeDelegate(this);
    
    this -> removeAllChildrenWithCleanup(true);
    
    for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); )
    {
        it -> second -> release();
        m_EnemyList.erase(it ++);
    }
    
    for(list <Skill*>::iterator it = m_EnemyDeBuffList.begin(); it != m_EnemyDeBuffList.end(); )
    {
        (*it) -> release();
        m_EnemyDeBuffList.erase(it ++);
    }
    
    if(m_bAllClearLevel)
    {
        m_PlayerBattleResList.clear();
    }else
    {
        for(list <Follower*>::iterator it = m_PlayerBattleResList.begin(); it != m_PlayerBattleResList.end(); )
        {
            (*it) -> release();
            m_PlayerBattleResList.erase(it ++);
        }
        m_PlayerBattleResList.clear();
    }
    
    m_EnemyListTemporary.clear();
    m_EnemyFightList.clear();
}
/************************************************************************/
#pragma mark -
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark 退出战斗
/************************************************************************/
void Layer_Enemy::OutGame()
{
    m_bAllClearLevel = false;
    GameController::ShareInstance() -> ReadyToGameScene(GameState_BackToGame);
}
/************************************************************************/
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark *********************************开始出现场景和敌人*********************************
/************************************************************************/
void Layer_Enemy::DisplayScene()
{
    CCFadeIn * pFadeIn = CCFadeIn::actionWithDuration(1.5f);
    
    this -> getChildByTag(LEChildTag_Background) -> runAction(pFadeIn);
    this -> getChildByTag(LEChildTag_UIBackground) -> runAction(dynamic_cast<CCFadeIn*>(pFadeIn -> copy()));
    this -> getChildByTag(LEChildTag_UIStepIcon) -> runAction(dynamic_cast<CCFadeIn*>(pFadeIn -> copy()));
    this -> getChildByTag(LEChildTag_UIStepSpace) -> runAction(dynamic_cast<CCFadeIn*>(pFadeIn -> copy()));    
    this -> getChildByTag(LEChildTag_UIMoneyIcon) -> runAction(dynamic_cast<CCFadeIn*>(pFadeIn -> copy()));
    this -> getChildByTag(LEChildTag_UIResIcon) -> runAction(dynamic_cast<CCFadeIn*>(pFadeIn -> copy()));
    m_OutBattle1 -> runAction(dynamic_cast<CCFadeIn*>(pFadeIn -> copy()));
    
    m_LabelPlayerMoney -> runAction(dynamic_cast<CCFadeIn*>(pFadeIn -> copy()));
    m_LabelPlayerRes -> runAction(dynamic_cast<CCFadeIn*>(pFadeIn -> copy()));
    m_LabelCurrentStep -> runAction(dynamic_cast<CCFadeIn*>(pFadeIn -> copy()));
    m_LabelAllStep -> runAction(dynamic_cast<CCFadeIn*>(pFadeIn -> copy()));
    
    //加入敌人并延时让敌人出现
    for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); ++ it)
    {
        m_EnemyListTemporary.push_back(it -> second);
    }
    
    CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(1.6f);
    CCCallFunc * pDisplayEnemyOneByOne = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Enemy::DisplayEnemyOneByOne));
    this -> runAction(CCSequence::actions(pDelayTime, pDisplayEnemyOneByOne, NULL));
}

void Layer_Enemy::DisplayEnemy()
{
    for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); ++ it)
    {
        m_EnemyListTemporary.push_back(it -> second);
    }
    DisplayEnemyOneByOne();
}

void Layer_Enemy::DisplayEnemyOneByOne()
{
    schedule(schedule_selector(Layer_Enemy::DisplayEnemyOneByOneUpdate), 0.4f);
}

void Layer_Enemy::DisplayEnemyOneByOneUpdate()
{
    list <Battle_Enemy *>::iterator it = m_EnemyListTemporary.begin();
    (*it) -> DisplaySelf();
    m_EnemyListTemporary.erase(it);
    
    if(m_EnemyListTemporary.empty())
    {
        for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); ++ it)
        {
            m_EnemyListTemporary.push_back(it -> second);
        }
        
        unschedule(schedule_selector(Layer_Enemy::DisplayEnemyOneByOneUpdate));
        schedule(schedule_selector(Layer_Enemy::DisplayEnemyOneByOneOver), 0.2f);
    }
}

void Layer_Enemy::DisplayEnemyOneByOneOver()
{
    list <Battle_Enemy *>::iterator it = m_EnemyListTemporary.begin();
    (*it) -> DisplayCDTime();
    m_EnemyListTemporary.erase(it);
    
    if(m_EnemyListTemporary.empty())
    {
        unschedule(schedule_selector(Layer_Enemy::DisplayEnemyOneByOneOver));
        m_EnemyListTemporary.clear();
        
        //变化上方UI的Step数
        char buff[32];
        sprintf(buff, "%d", Layer_Display::ShareInstance() -> m_StepRecord);
        m_LabelCurrentStep -> setString(buff);
        
        //如果是刚进入副本，就显示开场动画
        if(m_IsFirstBattle)
        {
            CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(1.0f);
            CCCallFunc * pDisplayStart = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Enemy::DisplayStart));
            this -> runAction(CCSequence::actions(pDelayTime, pDisplayStart, NULL));
            m_IsFirstBattle = false;
        }else
        {
            //默认的目标显示准星
            GetTargetEnemy() -> DisplayPost();
            
            //引导战斗
            if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
            {
                m_OperationEnable = true;
                
                if(m_GuideStep == 2)
                {
                    Layer_BattleGuideInfo * LB = Layer_BattleGuideInfo::InitBoard(3);
                    Layer_Display::ShareInstance() -> addChild(LB);
                }
                else if(m_GuideStep == 3)
                {
                    Layer_BattleGuideInfo * LB = Layer_BattleGuideInfo::InitBoard(4);
                    Layer_Display::ShareInstance() -> addChild(LB);
                }
            }
            //正常战斗
            else
            {
                //可以操作
                Layer_Display::ShareInstance() -> OperationEnable(true);
            }
        }
    }
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark *********************************显示副本开始的动画*********************************
/************************************************************************/
void Layer_Enemy::DisplayStart()
{
    CCSprite * pStartSprite = CCSprite::spriteWithSpriteFrameName("StartBattle_Ready_1.png");
    pStartSprite -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2));
    
    CCAnimation * pStartAnimation = CreateAnimation("StartBattle_Ready_", 0.06f);
    CCAnimate * pStartAnimate = CCAnimate::actionWithAnimation(pStartAnimation);
    CCCallFuncN * pStartAnimationOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Enemy::StartAnimationOver));
    pStartSprite -> runAction(CCSequence::actions(pStartAnimate, pStartAnimationOver, NULL));
    
    Layer_Display::ShareInstance() -> addChild(pStartSprite);
}

void Layer_Enemy::StartAnimationOver(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
    
    DisplayColorLayerAndAction();
}

void Layer_Enemy::DisplayColorLayerAndAction()
{
    //闪动的Layer
    CCLayerColor * pLayerColor = CCLayerColor::layerWithColorWidthHeight(ccc4(255, 255, 255, 150), m_WinSize.width, 214);
    pLayerColor -> setPosition(ccp(0, m_WinSize.height - 214));
    pLayerColor -> setVertexZ(0.9f);
    
    CCFadeTo * pFadeTo1 = CCFadeTo::actionWithDuration(0.2f, 255);
    CCFadeTo * pFadeTo2 = CCFadeTo::actionWithDuration(0.2f, 150);
    CCFiniteTimeAction * pBlink = CCSequence::actions(pFadeTo1, pFadeTo2, NULL);
    CCCallFuncN * pDisplayColorLayerOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Enemy::DisplayColorLayerOver));
    
    pLayerColor -> runAction(CCSequence::actions(pBlink, pDisplayColorLayerOver, NULL));
    this -> addChild(pLayerColor);
    
    //Action动画
    CCSprite * pAction = CCSprite::spriteWithSpriteFrameName("StartBattle_Action.png");
    pAction -> setScale(0.5f);
    pAction -> setPosition(ccp(m_WinSize.width / 2, m_BgPoint.y + 20));
    
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.8f, 2.0f);
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(1.0f);
    CCFiniteTimeAction * pSpawn = CCSpawn::actions(pScaleTo, pFadeOut, NULL);
    CCCallFuncN * pDisplayActionOver = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Enemy::DisplayActionOver));
    
    pAction -> runAction(CCSequence::actions(pSpawn, pDisplayActionOver, NULL));
    Layer_Display::ShareInstance() -> addChild(pAction);
}

void Layer_Enemy::DisplayColorLayerOver(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
}

void Layer_Enemy::DisplayActionOver(CCNode * sender)
{
    sender -> removeFromParentAndCleanup(true);
    
    //默认的目标显示准星
    GetTargetEnemy() -> DisplayPost();
    
    //引导战斗
    if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
    {
        m_OperationEnable = true;
        Layer_BattleGuideInfo * LB = Layer_BattleGuideInfo::InitBoard(1);
        Layer_Display::ShareInstance() -> addChild(LB);
    }
    //正常战斗
    else
    {
        //可以操作
        Layer_Display::ShareInstance() -> OperationEnable(true);
    
        //先触发队长技能
        Layer_Follower::ShareInstance() -> TriggerTeamSkill();
    }
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark *************************************创建敌人*************************************
/************************************************************************/
void Layer_Enemy::CreateEnemyList()
{
    vector <Enemy_Data *> pEnemyList = *ServerDataManage::ShareInstance() -> GetEnemyInfoList(Layer_Display::ShareInstance() -> m_StepRecord);
    
    int index = 0;
    for(vector <Enemy_Data *>::iterator it = pEnemyList.begin(); it != pEnemyList.end(); ++ it)
    {
        Battle_Enemy * BE = Battle_Enemy::node();
        BE -> SetData(*it);
        
        m_EnemyList[index] = BE;
        index ++;
        
        /*////打印
        char buf[64];
    
        Layer_Display::ShareInstance() -> removeAllChildrenWithCleanup(true);

        sprintf(buf, "Att =  %.0f", BE -> GetData() -> Enemy_Attack);
        CCLabelTTF * pAttack = CCLabelTTF::labelWithString(buf, "宋体", 10);
        pAttack -> setColor(ccc3(0, 255, 0));
        pAttack -> setPosition(ccp(30, 430 + (index * 12)));
        Layer_Display::ShareInstance() -> addChild(pAttack);
        
        sprintf(buf, "Def =  %.0f", BE -> GetData() -> Enemy_Defense);
        CCLabelTTF * pDefense = CCLabelTTF::labelWithString(buf, "宋体", 10);
        pDefense -> setColor(ccc3(0, 255, 0));
        pDefense -> setPosition(ccp(120, 430 + (index * 12)));
        Layer_Display::ShareInstance() -> addChild(pDefense);
        
        sprintf(buf, "HP =  %.0f", BE -> GetData() -> Enemy_HP);
        CCLabelTTF * pHP = CCLabelTTF::labelWithString(buf, "宋体",10);
        pHP -> setColor(ccc3(0, 255, 0));
        pHP -> setPosition(ccp(220, 430 + (index * 12)));
        Layer_Display::ShareInstance() -> addChild(pHP);*/
    }
    
    int RectX = m_WinSize.width / m_EnemyList.size();
    for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); ++ it)
    {
        CCSprite * pSprite = CCSprite::spriteWithSpriteFrameName(it -> second -> GetData() -> Enemy_IconName);
        if(pSprite -> getContentSize().height * 2 == 140 ||
           pSprite -> getContentSize().height * 2 == 150 ||
           pSprite -> getContentSize().height * 2 == 170 ||
           pSprite -> getContentSize().height * 2 == 200)
        {
            it -> second -> setPosition(ccp(RectX / 2 + (RectX * it -> first), 345));
        }
        else if(pSprite -> getContentSize().height * 2 == 250)
        {
            it -> second -> setPosition(ccp(RectX / 2 + (RectX * it -> first), 335));
        }
        else if(pSprite -> getContentSize().height * 2 == 300)
        {
            it -> second -> setPosition(ccp(RectX / 2 + (RectX * it -> first), 330));
        }
        
        it -> second -> Initialize();
        it -> second -> setVertexZ(0.4f);
        this -> addChild(it -> second);
    }
    
    //自动选择的目标
    m_TargetIndex = FindEnemyByHP();
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark *************************************检查点击*************************************
/************************************************************************/
bool Layer_Enemy::ccTouchBegan(CCTouch * pTouch, CCEvent * pEvent)
{
    if(! m_OperationEnable)
        return false;
    
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
    {
        m_GuideStep ++;
        
        if(m_GuideStep == 1)
        {
            Layer_Display::ShareInstance() -> removeChildByTag(Layer_DisplayChild_GuideInfo1, true);
            Layer_Display::ShareInstance() -> removeChildByTag(Layer_DisplayChild_GuideInfo2, true);
            Layer_Display::ShareInstance() -> removeChildByTag(Layer_DisplayChild_GuideInfo3, true);
            Layer_Display::ShareInstance() -> removeChildByTag(Layer_DisplayChild_GuideInfo4, true);
            Layer_Display::ShareInstance() -> removeChildByTag(Layer_DisplayChild_GuideInfo5, true);
            
            Layer_BattleGuideInfo * LB = Layer_BattleGuideInfo::InitBoard(2);
            Layer_Display::ShareInstance() -> addChild(LB);
        }
    }
    else
    {
        for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); ++ it)
        {
            if(it -> second -> CheckBeTouch(point))
            {
                if(m_TargetIndex != it -> first)
                {
                    //将目标定为点击选择的敌人
                    m_TargetIndex = it -> first;
                    //目标显示准星
                    it -> second -> DisplayPost();
                    //不是目标的就隐藏准星
                    for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); ++ it)
                    {
                        if(it -> first != m_TargetIndex)    it -> second -> HidePost();
                    }
                }
                return true;
            }
        }
        
        CCPoint bPoint = m_OutBattle1 -> getPosition();
        CCSize  bSize = m_OutBattle1 -> getTextureRect().size;
        if(point.x >= bPoint.x - bSize.width / 2 && point.x <= bPoint.x + bSize.width / 2 &&
           point.y >= bPoint.y - bSize.height / 2 && point.y <= bPoint.y + bSize.height / 2)
        {
            m_OutBattle1 -> setOpacity(0);
            m_OutBattle2 -> setOpacity(255);
            return true;
        }
    }
    
    return false;
}

void Layer_Enemy::ccTouchMoved(CCTouch * pTouch, CCEvent * pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    CCPoint bPoint = m_OutBattle1 -> getPosition();
    CCSize  bSize = m_OutBattle1 -> getTextureRect().size;
    if(point.x >= bPoint.x - bSize.width / 2 && point.x <= bPoint.x + bSize.width / 2 &&
       point.y >= bPoint.y - bSize.height / 2 && point.y <= bPoint.y + bSize.height / 2)
    {
        m_OutBattle1 -> setOpacity(0);
        m_OutBattle2 -> setOpacity(255);
    }else
    {
        m_OutBattle1 -> setOpacity(255);
        m_OutBattle2 -> setOpacity(0);
    }
}

void Layer_Enemy::ccTouchEnded(CCTouch * pTouch, CCEvent * pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    CCPoint bPoint = m_OutBattle1 -> getPosition();
    CCSize  bSize = m_OutBattle1 -> getTextureRect().size;
    if(point.x >= bPoint.x - bSize.width / 2 && point.x <= bPoint.x + bSize.width / 2 &&
       point.y >= bPoint.y - bSize.height / 2 && point.y <= bPoint.y + bSize.height / 2)
    {
        m_OutBattle1 -> setOpacity(255);
        m_OutBattle2 -> setOpacity(0);
        
        OutGame();
    }
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark **********************************关于DeBuff**********************************
#pragma mark 加入DeBuff
/************************************************************************/
void Layer_Enemy::AddDeBuff(Skill * debuff)
{
    bool bExist = false;
    
    for(list <Skill*>::iterator it = m_EnemyDeBuffList.begin(); it != m_EnemyDeBuffList.end(); )
    {
        //如果已经有了相同类型的debuff
        if(debuff -> GetData() -> Skill_Type == (*it) -> GetData() -> Skill_Type)
        {
            //如果新的debuff的ID大于旧的debuff的ID
            if(debuff -> GetData() -> Skill_ID > (*it) -> GetData() -> Skill_ID)
            {
                //旧的debuff关闭逻辑
                (*it) -> CloseSkill();
                //新的debuff触发逻辑
                debuff -> DisposalSkill();
                
                m_EnemyDeBuffList.push_back(debuff);
                DisplayDeBuff();
                
                RemoveDeBuff(*it, false);
            }
            //如果新的debuff的ID小于等于旧的debuff的ID
            else
            {
                debuff -> release();
            }
            bExist = true;
            break;
        }
        else ++ it;
    }
    
    //如果没有相同类型的debuff
    if(! bExist)
    {
        //debuff触发逻辑
        debuff -> DisposalSkill();
        
        m_EnemyDeBuffList.push_back(debuff);
        DisplayDeBuff();
    }
}

/************************************************************************/
#pragma mark 显示DeBuff
/************************************************************************/
void Layer_Enemy::DisplayDeBuff()
{
    list<Skill*>::reverse_iterator it = m_EnemyDeBuffList.rbegin();
    
    (*it) -> setScale(3.0f);
    (*it) -> setPosition(ccp(25 + 20 * (m_EnemyDeBuffList.size() - 1), 450));

    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.5f, 1.0f);
    (*it) -> runAction(pScaleTo);
    
    (*it) -> setVertexZ(0.3f);
    this -> addChild(*it);
}

/************************************************************************/
#pragma mark 显示移除DeBuff
/************************************************************************/
void Layer_Enemy::RemoveDeBuff(Skill * debuff, bool needclose)
{
    for(list <Skill*>::iterator it = m_EnemyDeBuffList.begin(); it != m_EnemyDeBuffList.end(); ++ it)
    {
        if(debuff == (*it))
        {
            CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.5f);
            CCCallFuncND * pCallFunc = CCCallFuncND::actionWithTarget(this, callfuncND_selector(Layer_Enemy::ReSetPositionDeBuff), (void*)needclose);
            (*it) -> runAction(CCSequence::actions(pFadeOut, pCallFunc, NULL));
            break;
        }
    }
}

/************************************************************************/
#pragma mark 显示DeBuff重新设置位置
/************************************************************************/
void Layer_Enemy::ReSetPositionDeBuff(CCNode * sender, void * data)
{
    Skill * pSkill = dynamic_cast<Skill*>(sender);
    bool    pneedclose = static_cast<bool>(data);
    
    for(list <Skill*>::iterator it = m_EnemyDeBuffList.begin(); it != m_EnemyDeBuffList.end(); ++ it)
    {
        if((*it) == pSkill)
        {
            if(pneedclose)  (*it) -> CloseSkill();
            
            (*it) -> release();
            m_EnemyDeBuffList.erase(it);
            break;
        }
    }
    
    int index = 0;
    for(list <Skill*>::iterator it = m_EnemyDeBuffList.begin(); it != m_EnemyDeBuffList.end(); ++ it)
    {
        CCMoveTo * pMoveTo = CCMoveTo::actionWithDuration(0.5f, ccp(25 + 20 * index, 450));
        (*it) -> runAction(pMoveTo);
        
        ++ index;
    }
}

/************************************************************************/
#pragma mark 触发DeBuff
/************************************************************************/
void Layer_Enemy::TriggerDeBuff()
{
    for(list <Skill*>::iterator it = m_EnemyDeBuffList.begin(); it != m_EnemyDeBuffList.end(); ++ it)
    {
        (*it) -> DisposalKeepSkill();
    }
    
    CheckEnemyUnderAttackByDeBuff();
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark ************************************关于敌人被攻击************************************
#pragma mark 检查敌人是否处在被攻击中 (被普通攻击)
/************************************************************************/
void Layer_Enemy::CheckEnemyUnderAttack()
{
    this -> schedule(schedule_selector(Layer_Enemy::CheckEnemyUnderAttackOver));
    m_IsUnderAttackBySkill = 0;
}

/************************************************************************/
#pragma mark 检查敌人是否处在被攻击中 (被技能攻击)
/************************************************************************/
void Layer_Enemy::CheckEnemyUnderAttackBySkill()
{
    this -> schedule(schedule_selector(Layer_Enemy::CheckEnemyUnderAttackOver));
    m_IsUnderAttackBySkill = 1;
}

/************************************************************************/
#pragma mark 检查敌人是否处在被攻击中 (被DeBuff攻击)
/************************************************************************/
void Layer_Enemy::CheckEnemyUnderAttackByDeBuff()
{
    this -> schedule(schedule_selector(Layer_Enemy::CheckEnemyUnderAttackOver));
    m_IsUnderAttackBySkill = 2;
}

/************************************************************************/
#pragma mark 检查敌人被攻击结束
/************************************************************************/
void Layer_Enemy::CheckEnemyUnderAttackOver()
{
    for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); ++ it)
    {
        if(it -> second -> GetUnderAttack())    return;
    }
    if(m_IsUnderAttackBySkill == 2)
    {
        if(Layer_Follower::ShareInstance() -> GetTriggerBuffing())  return;
    }
    this -> unschedule(schedule_selector(Layer_Enemy::CheckEnemyUnderAttackOver));
    
    //检查死亡的敌人
    for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); )
    {
        if(it -> second -> GetIsDead())
        {
            it -> second -> release();
            m_EnemyList.erase(it ++);
        }
        else    ++it;
    }
    
    //重新设定默认目标
    m_TargetIndex = FindEnemyByHP();
    
    DisposalEnemyDead();
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark **************************************关于敌人的死亡**************************************
#pragma mark 死亡敌人之后的处理
/************************************************************************/
void Layer_Enemy::DisposalEnemyDead()
{
    //如果还有敌人存在
    if(! m_EnemyList.empty())
    {
        //如果是普通攻击打的
        if(m_IsUnderAttackBySkill == 0)
        {
            StartAttack();
        }
        //如果是技能打的
        else if(m_IsUnderAttackBySkill == 1)
        {
            Layer_Enemy::ShareInstance() -> BGLight();
            Layer_Display::ShareInstance() -> OperationEnable(true);
        }
        //如果是DeBuff打的
        else if(m_IsUnderAttackBySkill == 2)
        {
            //检查到期的DeBuff，并移除
            for(list <Skill*>::iterator it = m_EnemyDeBuffList.begin(); it != m_EnemyDeBuffList.end(); ++ it)
            {
                if((*it) -> GetData() -> Skill_KeepRound <= 0)
                {
                    RemoveDeBuff((*it), true);
                }
            }
            
            Layer_Follower::ShareInstance() -> DecreaseSkillCDTime();
            Layer_Display::ShareInstance() -> m_Round += 1;
            
            //目标重新把准星显示出来
            GetTargetEnemy() -> DisplayPost();
            Layer_Enemy::ShareInstance() -> BGLight();
            Layer_Display::ShareInstance() -> OperationEnable(true);
        }
    }
    //如果敌人都已经死亡了
    else
    {
        Layer_Display::ShareInstance() -> m_StepRecord += 1;
        
        //如果是普通攻击打的
        if(m_IsUnderAttackBySkill == 0)
        {
            Layer_Follower::ShareInstance() -> DecreaseSkillCDTime();
            Layer_Display::ShareInstance() -> m_Round += 1;
        }
        //如果是技能打的
        else if(m_IsUnderAttackBySkill == 1)
        {
            Layer_Enemy::ShareInstance() -> BGLight();
        }
        //如果是DeBuff打的
        else if(m_IsUnderAttackBySkill == 2)
        {
            //检查到期的DeBuff，并移除
            for(list <Skill*>::iterator it = m_EnemyDeBuffList.begin(); it != m_EnemyDeBuffList.end(); ++ it)
            {
                if((*it) -> GetData() -> Skill_KeepRound <= 0)
                {
                    RemoveDeBuff((*it), true);
                }
            }
            
            Layer_Follower::ShareInstance() -> DecreaseSkillCDTime();
            Layer_Display::ShareInstance() -> m_Round += 1;
        }
        
        if(Layer_Display::ShareInstance() -> m_StepRecord == ServerDataManage::ShareInstance() -> GetStepNum())
        {
            EnterBattleSceneAndDisplayWarning(m_BGImageName);
            
            //最后一个步骤，放Boss背景音乐
            CocosDenshion::SimpleAudioEngine::sharedEngine() -> playBackgroundMusic("boss.mp3", true);
        }
        else if(Layer_Display::ShareInstance() -> m_StepRecord > ServerDataManage::ShareInstance() -> GetStepNum())
        {
            PassBattle();
        }
        else
        {
            EnterBattleScene(m_BGImageName);
            CreateEnemyList();
            DisplayEnemy();
        }
    }
}

/************************************************************************/
#pragma mark 通过了副本的处理
/************************************************************************/
void Layer_Enemy::PassBattle()
{
    CCDirector::sharedDirector() -> getRunningScene() -> removeChildByTag(MessageTag, true);
    
    //如果是过了引导关
    //如果是过了引导，掉落的小弟就不加入背包，因为登陆的时候服务器已经给过了
    if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
    {
        //加钱
        Layer_Enemy::ShareInstance() -> m_PlayerBattleMoney += ServerDataManage::ShareInstance() -> GetClearLevelDropMoney();
        PlayerDataManage::m_PlayerMoney += Layer_Enemy::ShareInstance() -> m_PlayerBattleMoney;
        
        //加经验
        Layer_Enemy::ShareInstance() -> m_PlayerBattleExperience += ServerDataManage::ShareInstance() -> GetClearLevelDropExperience();
        
        Layer_BattleResult * pBattleResult = Layer_BattleResult::node();
        Layer_Display::ShareInstance() -> addChild(pBattleResult);
        
        m_bAllClearLevel = false;
        
        PlayerDataManage::m_GuideBattleMark = false;
        PlayerDataManage::m_GuideStepMark += 1;
        SaveBattleGuideInfo(PlayerDataManage::m_GuideBattleMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
        SaveGameGuideInfo(PlayerDataManage::m_GuideStepMark, PlayerDataManage::ShareInstance() -> m_Player91ID);
        ServerDataManage::ShareInstance() -> RequestGuideDoneReqest(PlayerDataManage::m_GuideStepMark);
    }
    //如果是过了正常的副本
    else
    {
        //先发送战斗验证
        ServerDataManage::ShareInstance() -> VerificationBattleInfo();
    }
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark **************************************关于敌人的攻击**************************************
#pragma mark 开始攻击
/************************************************************************/
void Layer_Enemy::StartAttack()
{
    for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); ++ it)
    {
        if(it -> second -> GetCDRecord() == 1)
        {
            m_EnemyFightList.push_back(it -> second);
        }else
        {
            it -> second -> DisplayReduceCDTime();
        }
    }
    
    if(! m_EnemyFightList.empty())
    {
        CalculateAttackResultToFollower();
    }else
    {
        AttackOver();
    }
}

/************************************************************************/
#pragma mark 先计算对所有小弟的伤害
/************************************************************************/
void Layer_Enemy::CalculateAttackResultToFollower()
{
    for(list<Battle_Enemy*>::iterator it = m_EnemyFightList.begin(); it != m_EnemyFightList.end(); ++ it)
    {
        (*it) -> CalculateAttackFollower();
    }
    
    this -> schedule(schedule_selector(Layer_Enemy::DisplayAttack), 0.15f);
}

/************************************************************************/
#pragma mark 显示攻击
/************************************************************************/
void Layer_Enemy::DisplayAttack()
{
    list <Battle_Enemy *>::iterator it = m_EnemyFightList.begin();
    
    (*it) -> DisplayAttack();
    (*it) -> DisplayAddCDTime();
    m_EnemyFightList.erase(it);
    
    //敌人攻击列表清空
    if(m_EnemyFightList.empty())
    {
        m_EnemyFightList.clear();
        this -> unschedule(schedule_selector(Layer_Enemy::DisplayAttack));
        
        this -> schedule(schedule_selector(Layer_Enemy::CheckDisplayAttackOver));
    }
}

/************************************************************************/
#pragma mark 检查敌人攻击是否结束
/************************************************************************/
void Layer_Enemy::CheckDisplayAttackOver()
{
    for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); ++ it)
    {
        if(it -> second -> GetUnderShowAttack())    return;
    }
    this -> unschedule(schedule_selector(Layer_Enemy::CheckDisplayAttackOver));
    
    //小弟那边结束被攻击时的处理
    Layer_Follower::ShareInstance() -> DisplayBeAttackOver();
    //敌人结束攻击
    AttackOver();
}


/************************************************************************/
#pragma mark 敌人攻击结束之后的处理
/************************************************************************/
void Layer_Enemy::AttackOver()
{
    if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
    {
        if(m_GuideStep == 4)
        {
            Layer_GameGuide::GetSingle() -> RunBattleGuideStep4();
            Layer_Display::ShareInstance() -> OperationEnable(true);
            
            m_GuideStep = 5;
            return;
        }
        else if(m_GuideStep == 5)
        {
            Layer_BattleGuideInfo * LB = Layer_BattleGuideInfo::InitBoard(5);
            Layer_Display::ShareInstance() -> addChild(LB);
            return;
        }
    }
    
    //如果玩家死亡了
    if(Layer_Follower::ShareInstance() -> GetFollowerHP() <= 0.0f)
    {
        Layer_Display::ShareInstance() -> DisplayGameOver();
        
        //玩家死亡，播放音效
        CocosDenshion::SimpleAudioEngine::sharedEngine() -> stopBackgroundMusic();
        CocosDenshion::SimpleAudioEngine::sharedEngine() -> playEffect("fail.mp3");
    }
    else
    {
        CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(1.0f);
        CCCallFunc * pCallFunc = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Enemy::AttackOverFunc));
        this -> runAction(CCSequence::actions(pDelayTime, pCallFunc, NULL));
    }
}

void Layer_Enemy::AttackOverFunc()
{
    //敌人攻击结束，触发DeBuff和Buff
    TriggerDeBuff();
    Layer_Follower::ShareInstance() -> TriggerBuff();
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark ***********************************进入战斗场景***********************************
/************************************************************************/
void Layer_Enemy::EnterBattleSceneAndDisplayWarning(const char * mapFilename)
{
    Layer_Display::ShareInstance() -> DisplayWarning();
    EnterBattleScene(mapFilename);
    
    CCDelayTime * pDelayTime = CCDelayTime::actionWithDuration(1.6f);
    CCCallFunc * pEnterBattleSceneAndDisplayWarningCallBack1 = CCCallFunc::actionWithTarget(this, callfunc_selector(Layer_Enemy::EnterBattleSceneAndDisplayWarningCallBack1));
    this -> runAction(CCSequence::actions(pDelayTime, pEnterBattleSceneAndDisplayWarningCallBack1, NULL));
}

void Layer_Enemy::EnterBattleSceneAndDisplayWarningCallBack1()
{
    Layer_Display::ShareInstance() -> DisplayWarningOver();
    
    CreateEnemyList();
    DisplayEnemy();
}

void Layer_Enemy::EnterBattleScene(const char* mapFilename)
{
    // 创建动画背景图片
    CCSprite * AnimBackground = CCSprite::spriteWithSpriteFrameName(mapFilename);
    AnimBackground -> setPosition(m_BgPoint);
    AnimBackground -> setVertexZ(0.2f);
    
    this -> addChild(AnimBackground, 0, LEChildTag_AnimBackground);
    
    CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(1.5f, 2.0f);
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(1.5f);
    Layer_MapShake * pShake = Layer_MapShake::actionWithDuration(1.2f, m_BgPoint, SHAKE_UPDOWN);
    CCCallFuncN * pCallFuncN = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Layer_Enemy::EnterBattleSceneOver));
    
    AnimBackground -> runAction(CCSequence::actions(CCSpawn::actions(pScaleTo, pFadeOut, pShake, NULL), pCallFuncN, NULL));
}

void Layer_Enemy::EnterBattleSceneOver(CCNode * sender)
{
    this -> removeChild(sender, true);
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark ***********************************寻找血最少的敌人***********************************
/************************************************************************/
int Layer_Enemy::FindEnemyByHP()
{
    CCPoint value = CCPointMake(-1, 10000000);
    
    int targetIndex = value.x;
    for(Battle_EnemyList::iterator it = m_EnemyList.begin(); it != m_EnemyList.end(); ++ it)
    {
        if(! it -> second -> GetIsDead())
        {
            if(it -> second -> GetData() -> Enemy_HP < value.y)
            {
                value.x = it -> first;
                value.y = it -> second -> GetData() -> Enemy_HP;
            }
        }
    }
    targetIndex = value.x;
    
    if(targetIndex == -1)
        targetIndex = m_TargetIndex;
    
    return targetIndex;
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark  **************************************获取目标**************************************
/************************************************************************/
Battle_Enemy * Layer_Enemy::GetTargetEnemy()
{
    Battle_EnemyList::iterator it = m_EnemyList.find(m_TargetIndex);
    CCAssert(it != m_EnemyList.end(), "Layer_Enemy::GetTargetEnemy()");
    
    if(it -> second -> GetIsDead())
    {
        m_TargetIndex = FindEnemyByHP();
        it = m_EnemyList.find(m_TargetIndex);
    }
    return it -> second;
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark  **************************************改变UI显示的值**************************************
/************************************************************************/
void Layer_Enemy::DisplayChangeBattleMoney()
{
    schedule(schedule_selector(Layer_Enemy::DisplayChangeBattleMoneyCallBack1), 0.02f);
}

void Layer_Enemy::DisplayChangeBattleMoneyCallBack1()
{
    if(m_LabelPlayerMoneyString + 10 < m_PlayerBattleMoney)
        m_LabelPlayerMoneyString += 10;
    else
        m_LabelPlayerMoneyString = m_PlayerBattleMoney;
    
    char buff[32];
    sprintf(buff, "%d", m_LabelPlayerMoneyString);
    m_LabelPlayerMoney -> setString(buff);
    
    if(m_LabelPlayerMoneyString >= m_PlayerBattleMoney)
    {
        unschedule(schedule_selector(Layer_Enemy::DisplayChangeBattleMoneyCallBack1));
    }
}

void Layer_Enemy::DisplayChangeBattleRes()
{
    if(m_LabelPlayerResString < m_PlayerBattleRes)
        m_LabelPlayerResString = m_PlayerBattleRes;
    
    char buff[32];
    sprintf(buff, "%d", m_LabelPlayerResString);
    m_LabelPlayerRes -> setString(buff);
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/


/************************************************************************/
#pragma mark ***********************************背景震动***********************************
/************************************************************************/
void Layer_Enemy::BGShake()
{
    CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(LEChildTag_Background));
    if(! pSprite -> getActionByTag(ActionTag_BgShake))
    {
        Action_MyMove * pMyMove = Action_MyMove::actionWithDuration(6, 10, 0.004f);
        pMyMove -> setTag(ActionTag_BgShake);
        pSprite -> runAction(pMyMove);
    }
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/



/************************************************************************/
#pragma mark ***********************************背景变暗 背景变亮***********************************
/************************************************************************/
void Layer_Enemy::BGDark()
{
    CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(LEChildTag_Background));
    
    CCFadeTo * pFadeTo = CCFadeTo::actionWithDuration(0.8f, 100);
    pSprite -> runAction(pFadeTo);
}

void Layer_Enemy::BGLight()
{
    CCSprite * pSprite = static_cast<CCSprite*>(this -> getChildByTag(LEChildTag_Background));
    
    if(pSprite -> getOpacity() != 255)
    {
        CCFadeTo * pFadeTo = CCFadeTo::actionWithDuration(0.8f, 255);
        pSprite -> runAction(pFadeTo);
    }
}
/************************************************************************/
#pragma mark -
#pragma mark -
#pragma mark -
/************************************************************************/











































































