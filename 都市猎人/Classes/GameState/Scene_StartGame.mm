//
//  Scene_StartGame.cpp
//  都市猎人
//
//  Created by 张 强 on 12-8-31.
//
//



#include "Scene_StartGame.h"
USING_NS_CC;

#include "PlayerDataManage.h"
#include "Scene_LogIn.h"
#include "Scene_Loading.h"
#include "Wrapper.h"

//#import <NdComPlatform/NDComPlatform.h>
//#import <NdComPlatform/NdComPlatformAPIResponse.h>
//#import <NdComPlatform/NdCPNotifications.h>

CCScene * Scene_StartGame::scene()
{
    //创建场景
    CCScene * scene = CCScene::node();
    if (scene == NULL)
        return NULL;
    
    //创建登陆图层
    Scene_StartGame * pStart = Scene_StartGame::node();
    if (pStart == NULL)
        return NULL;
    
    scene -> addChild(pStart, 0, LayerChild_Layer);
    
    return scene;
}

bool Scene_StartGame::init()
{
    if(! CCLayer::init())
        return false;
    
    pChange1 = pChange2 = NULL;
    
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> setEffectsVolume(50);
    CocosDenshion::SimpleAudioEngine::sharedEngine() -> resumeAllEffects();
    
    this -> setIsTouchEnabled(true);
    CCTouchDispatcher::sharedDispatcher() -> addTargetedDelegate(this, 0, true);
    
    m_WinSize = CCDirector::sharedDirector() -> getWinSize();
    
    //背景
    CCSprite * pSpriteBG = CCSprite::spriteWithFile("Default@2x.png");
    pSpriteBG -> setPosition(ccp(m_WinSize.width / 2, m_WinSize.height / 2));
    this -> addChild(pSpriteBG);
    
    //按钮
    CCSprite * pSpriteTouch = CCSprite::spriteWithSpriteFrameName("TouchMe.png");
    pSpriteTouch -> setPosition(ccp(m_WinSize.width / 2, 20));
    this -> addChild(pSpriteTouch, 0, LayerChild_Touch);
    
    m_TouchRect = CCRectMake(0, 0, m_WinSize.width, m_WinSize.height / 5);
    
    //切换按钮
    pChange1 = CCSprite::spriteWithSpriteFrameName("ChangePlayer.png"); 
    pChange1 -> setPosition(ccp(0 + pChange1 -> getTextureRect().size.width / 2, m_WinSize.height - pChange1 -> getTextureRect().size.height / 2));
    pChange1 -> retain();
    
    pChange2 = CCSprite::spriteWithSpriteFrameName("ChangePlayer1.png");
    pChange2 -> setPosition(ccp(0 + pChange1 -> getTextureRect().size.width / 2, m_WinSize.height - pChange1 -> getTextureRect().size.height / 2));
    pChange2 -> setOpacity(0);
    pChange2 -> retain();
    
    m_ChangeRect = CCRectMake(0, m_WinSize.height - pChange1 -> getTextureRect().size.height, pChange1 -> getTextureRect().size.width, pChange1 -> getTextureRect().size.height);
    this -> addChild(pChange1, 0, LayerChild_Change1);
    this -> addChild(pChange2, 0, LayerChild_Change2);
    
    CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.5f);
    CCFadeIn * pFadeIn = CCFadeIn::actionWithDuration(0.5f);
    CCFiniteTimeAction * pSequence = CCSequence::actions(pFadeOut, pFadeIn, NULL);
    CCRepeat * pAction = CCRepeat::actionWithAction(pSequence, 500);
    pSpriteTouch -> runAction(pAction);

    return true;
}


Scene_StartGame::~Scene_StartGame()
{
    printf("Scene_StartGame::释放完成\n");
}

void Scene_StartGame::onExit()
{
    CCLayer::onExit();
    
    if(pChange1)    pChange1 -> release();
    if(pChange2)    pChange2 -> release();
    this -> removeAllChildrenWithCleanup(true);
    this -> removeFromParentAndCleanup(true);
}

bool Scene_StartGame::ccTouchBegan(CCTouch * pTouch, CCEvent * pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    if(CCRect::CCRectContainsPoint(m_TouchRect, point))
    {
        if(! this -> getChildByTag(LayerChild_Touch) -> getActionByTag(ActionTag_Touch))
        {
            CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.5f, 3.0f);
            CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.5f);
            CCFiniteTimeAction * pS_F = CCSpawn::actions(pScaleTo, pFadeOut, NULL);
            CCCallFuncN * pFunc = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Scene_StartGame::TouchOver));
            CCFiniteTimeAction * pAction = CCSequence::actions(pS_F, pFunc, NULL);
            pAction -> setTag(ActionTag_Touch);

            this -> getChildByTag(LayerChild_Touch) -> runAction(pAction);
        }
        
//        //如果玩家已经是登陆状态
//        if([[NdComPlatform defaultPlatform] isLogined])
//        {
//            if(! this -> getChildByTag(LayerChild_Touch) -> getActionByTag(ActionTag_Touch))
//            {
//                CCScaleTo * pScaleTo = CCScaleTo::actionWithDuration(0.5f, 3.0f);
//                CCFadeOut * pFadeOut = CCFadeOut::actionWithDuration(0.5f);
//                CCFiniteTimeAction * pS_F = CCSpawn::actions(pScaleTo, pFadeOut, NULL);
//                CCCallFuncN * pFunc = CCCallFuncN::actionWithTarget(this, callfuncN_selector(Scene_StartGame::TouchOver));
//                CCFiniteTimeAction * pAction = CCSequence::actions(pS_F, pFunc, NULL);
//                pAction -> setTag(ActionTag_Touch);
//                
//                this -> getChildByTag(LayerChild_Touch) -> runAction(pAction);
//            }
//        }
//        else
//        {
//            [[NdComPlatform defaultPlatform] NdLogin:0];
//        }
    }
    else if(CCRect::CCRectContainsPoint(m_ChangeRect, point))
    {
        pChange1 -> setOpacity(0);
        pChange2 -> setOpacity(255);
    }

    return true;
}

void Scene_StartGame::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    if(CCRect::CCRectContainsPoint(m_ChangeRect, point))
    {
        pChange1 -> setOpacity(0);
        pChange2 -> setOpacity(255);
    }else
    {
        pChange1 -> setOpacity(255);
        pChange2 -> setOpacity(0);
    }
}

void Scene_StartGame::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
    CCPoint point = convertTouchToNodeSpace(pTouch);
    
    if(CCRect::CCRectContainsPoint(m_ChangeRect, point))
    {
        pChange1 -> setOpacity(255);
        pChange2 -> setOpacity(0);
        
//        [[NdComPlatform defaultPlatform] NdLogout:1];
//        [[NdComPlatform defaultPlatform] NdLogin:0];
    }
}

void Scene_StartGame::TouchOver(CCNode * sender)
{
    this -> removeChild(sender, true);
    
    //如果当前没有玩家信息，就进入登陆界面
    if(strcmp(PlayerDataManage::ShareInstance() -> m_PlayerUserID, "") == 0)
    {
        CCScene * pLogIn = Scene_LogIn::scene();
        
        CCTransitionFade * pFade = CCTransitionFade::transitionWithDuration(0.6f, pLogIn);
        CCDirector::sharedDirector() -> pushScene(pFade);
    }
    //如果当前有了玩家信息
    else
    {
        if(PlayerDataManage::m_GuideMark && PlayerDataManage::m_GuideBattleMark)
        {
            //进入战斗引导
            GameController::ShareInstance() -> ReadyToGameScene(GameState_BattleGuide);
        }
        else
        {
            //进入游戏场景
            GameController::ShareInstance() -> ReadyToGameScene(GameState_Game);
        }
    }
}































