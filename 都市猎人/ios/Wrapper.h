//
//  Wrapper.h
//  MidNightCity
//
//  Created by 强 张 on 12-7-18.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#ifndef MidNightCity_Wrapper_h
#define MidNightCity_Wrapper_h

void  SavePlayerInfo(const char * filename);
void  ReadPlayerInfo(const char * filename);

void  SaveBattleGuideInfo(bool guide, const char * filename);
void  ReadBattleGuideInfo(const char * filename);

void  SaveGameGuideInfo(int step, const char * filename);
void  ReadGameGuideInfo(const char * filename);

void  BuyDiamond();

#endif
