//
//  MidNightCityAppController.mm
//  MidNightCity
//
//  Created by 强 张 on 12-3-30.
//  Copyright 恺英网络 2012年. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "AppController.h"
#import "cocos2d.h"
#import "EAGLView.h"
#import "AppDelegate.h"

#import "RootViewController.h"
#include "Scene_StartGame.h"
#include "PlayerDataManage.h"
#include "Wrapper.h"
//
//#import <NdComPlatform/NDComPlatform.h>
//#import <NdComPlatform/NdComPlatformAPIResponse.h>
//#import <NdComPlatform/NdCPNotifications.h>

@implementation AppController

#pragma mark -
#pragma mark Application lifecycle

#ifndef    GameFor_91
//#define   GameFor_91
#endif

// cocos2d application instance
static AppDelegate s_sharedApplication;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    // Override point for customization after application launch.

    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    EAGLView *__glView = [EAGLView viewWithFrame: [window bounds]
                                     pixelFormat: kEAGLColorFormatRGBA8
                                     depthFormat: GL_DEPTH_COMPONENT16_OES
                              preserveBackbuffer: NO
                                      sharegroup: nil
                                   multiSampling: NO
                                 numberOfSamples: 0 ];

    // Use RootViewController manage EAGLView
    viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    viewController.wantsFullScreenLayout = YES;
    viewController.view = __glView;
    
    // Set RootViewController to window
    [window addSubview: viewController.view];
    [window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setStatusBarHidden: YES];
    cocos2d::CCApplication::sharedApplication().run();

    
#ifdef GameFor_91
    [[NdComPlatform defaultPlatform] setAppId:108535];
    [[NdComPlatform defaultPlatform] setAppKey:@"11bd998be5e93fd57c556d99142a6fa46992e9022fb8cd40"];
    
    [[NdComPlatform defaultPlatform] NdSetDebugMode:0];
    
    //[[NdComPlatform defaultPlatform] NdLogout:0];
    [[NdComPlatform defaultPlatform] NdAppVersionUpdate:0 delegate:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SNSLoginResult:) name:(NSString *) kNdCPLoginNotification object:nil];
#endif
   
    return YES;
}
//
//- (void) appVersionUpdateDidFinish : (ND_APP_UPDATE_RESULT) updateResult
//{
//    NSString * title = nil;
//    NSLog(@"update result:%d", updateResult);
//    
//    switch (updateResult)
//    {
//        case ND_APP_UPDATE_NO_NEW_VERSION:
//            title = @"无可用更新";
//            break;
//            
//        case ND_APP_UPDATE_FORCE_UPDATE_CANCEL_BY_USER:
//            title = @"下载强制更新被取消";
//            break;
//            
//        case ND_APP_UPDATE_NORMAL_UPDATE_CANCEL_BY_USER:
//            title = @"下载普通更新被取消";
//            break;
//            
//        case ND_APP_UPDATE_NEW_VERSION_DOWNLOAD_FAIL:
//            title = @"下载新版本失败";
//            break;
//            
//        case ND_APP_UPDATE_CHECK_NEW_VERSION_FAIL:
//            title = @"检测新版本信息失败";
//            break;
//            
//        default:
//        break;
//    }
//    if(updateResult == ND_APP_UPDATE_NO_NEW_VERSION)
//    {
//        if([[NdComPlatform defaultPlatform] isLogined])
//        { }
//        else
//        {
//            [[NdComPlatform defaultPlatform] NdLogin:0];
//        }
//    }
//    else if(updateResult == ND_APP_UPDATE_FORCE_UPDATE_CANCEL_BY_USER)
//    {
//        UIAlertView * pAlertView = [[UIAlertView alloc] initWithTitle:@"需要版本更新"
//                                                              message:@"很抱歉，您的游戏版本过旧，无法进入游戏。请及时更新到最新版本"
//                                                             delegate:self
//                                                    cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//        [window addSubview:pAlertView];
//        [pAlertView show];
//        [pAlertView release];
//    }
//    else
//    { }
//}

- (void) alertView:(UIAlertView *) alertView clickedButtonAtIndex:(NSInteger) buttonIndex
{
    cocos2d::CCDirector::sharedDirector() -> end();
    exit(0);
}

- (void) SNSLoginResult : (NSNotification *) notify
{
    NSDictionary * dict = [notify userInfo];
    BOOL success = [[dict objectForKey:@"result"] boolValue];
    
    memset(PlayerDataManage::ShareInstance() -> m_PlayerUserID, 0, sizeof(PlayerDataManage::ShareInstance() -> m_PlayerUserID));
    PlayerDataManage::m_GuideMark = true;
    PlayerDataManage::m_GuideStepMark = 0;
    PlayerDataManage::m_GuideBattleMark = true;
//    
//    if([[NdComPlatform defaultPlatform] isLogined] && success)
//    {
//        NSString * p91ID = [[NdComPlatform defaultPlatform] loginUin];
//        memcpy(PlayerDataManage::ShareInstance() -> m_Player91ID, [p91ID UTF8String], sizeof(PlayerDataManage::ShareInstance() -> m_Player91ID));
//        
//        // 读取玩家信息
//        ReadPlayerInfo(PlayerDataManage::ShareInstance() -> m_Player91ID);
//        
//        if(strcmp(PlayerDataManage::ShareInstance() -> m_PlayerUserID, "") == 0)
//        {
//            ServerDataManage::ShareInstance() -> RequestPlayer91Login();
//        }
//        else
//        {
//            // 读取战斗引导纪录
//            ReadBattleGuideInfo(PlayerDataManage::ShareInstance() -> m_Player91ID);
//            // 读取楼层引导纪录
//            ReadGameGuideInfo(PlayerDataManage::ShareInstance() -> m_Player91ID);
//            if(PlayerDataManage::m_GuideStepMark == 9)
//                PlayerDataManage::m_GuideMark = false;
//            else
//                PlayerDataManage::m_GuideMark = true;
//        }
//    }
//    else
//    {
//        int error = [[dict objectForKey:@"error"] intValue];
//        NSString * strTip = [NSString stringWithFormat:@"登录失败, error=%d", error];
//        switch (error)
//        {
//            case ND_COM_PLATFORM_ERROR_USER_CANCEL://用户取消登录
//                if (([[NdComPlatform defaultPlatform] getCurrentLoginState] == ND_LOGIN_STATE_GUEST_LOGIN))
//                    strTip = @"当前仍处于游客登录状态";
//                else
//                    strTip = @"用户未登录"; 
//                break;
//            case ND_COM_PLATFORM_ERROR_APP_KEY_INVALID://appId未授权接入, 或appKey 无效
//                strTip = @"登录失败, 请检查appId/appKey";
//                break;
//            case ND_COM_PLATFORM_ERROR_CLIENT_APP_ID_INVALID://无效的应用ID
//                strTip = @"登录失败, 无效的应用ID";
//                break;
//            case ND_COM_PLATFORM_ERROR_HAS_ASSOCIATE_91:
//                strTip = @"有关联的91账号,不能以游客方式登录";
//                break;
//            default: //其他类型的错误提示
//                break;
//        }
//    }
}

- (void) BuyDiamond : (NSString *) PayID
{
    //CFUUIDRef theUUID = CFUUIDCreate(NULL);
    //CFStringRef guid = CFUUIDCreateString(NULL, theUUID);
    //CFRelease(theUUID);
    NSString * uuidString = [((NSString *)PayID) stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //CFRelease(guid);
//    [[NdComPlatform defaultPlatform] NdUniPayForCoin:[uuidString lowercaseString] needPayCoins:0 payDescription:@""];
}



- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
	cocos2d::CCDirector::sharedDirector()->pause();
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
	cocos2d::CCDirector::sharedDirector()->resume();
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    cocos2d::CCApplication::sharedApplication().applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::CCApplication::sharedApplication().applicationWillEnterForeground();
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
     cocos2d::CCDirector::sharedDirector()->purgeCachedData();
}


- (void)dealloc {
    [super dealloc];
}


@end

