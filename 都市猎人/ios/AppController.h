//
//  MidNightCityAppController.h
//  MidNightCity
//
//  Created by 强 张 on 12-3-30.
//  Copyright 恺英网络 2012年. All rights reserved.
//

@class RootViewController;

@interface AppController : NSObject <UIAccelerometerDelegate, UIAlertViewDelegate, UITextFieldDelegate,UIApplicationDelegate>
{
    UIWindow * window;
    RootViewController	* viewController;
}


- (void) BuyDiamond : (NSString *) PayID;

@end

