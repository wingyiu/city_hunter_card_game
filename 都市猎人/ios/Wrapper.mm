//
//  Wrapper.mm
//  MidNightCity
//
//  Created by 强 张 on 12-7-18.
//  Copyright (c) 2012年 恺英网络. All rights reserved.
//

#import "AppController.h"
#include "PlayerDataManage.h"


//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark    储存玩家数据
//////////////////////////////////////////////////////////////////////////////////////////////////
void  SavePlayerInfo(const char * filename)
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
	if (!documentsDirectory) return;
    
    NSString * FolderName = [NSString stringWithUTF8String:filename];
    NSString * FolderPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:FolderName];
    if([[NSFileManager defaultManager] fileExistsAtPath:FolderPath] == NO)
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:FolderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
	NSString * appFile = [FolderPath stringByAppendingPathComponent:@"PlayerInfo"];
    
	NSMutableData * writer = [[NSMutableData alloc] init];
    
    [writer appendBytes:&PlayerDataManage::ShareInstance() -> m_PlayerUserID length:sizeof(PlayerDataManage::ShareInstance() -> m_PlayerUserID)];
    [writer appendBytes:&PlayerDataManage::ShareInstance() -> m_PlayerName length:sizeof(PlayerDataManage::ShareInstance() -> m_PlayerName)];
    [writer appendBytes:&PlayerDataManage::ShareInstance() -> m_PlayerPassword length:sizeof(PlayerDataManage::ShareInstance() -> m_PlayerPassword)];
    [writer appendBytes:&PlayerDataManage::ShareInstance() -> m_PlayerFistGameProfession length:sizeof(PlayerDataManage::ShareInstance() -> m_PlayerFistGameProfession)];
	[writer writeToFile:appFile atomically:YES];
    
	[writer release];
}

void  ReadPlayerInfo(const char * filename)
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString * documentsDirectory = [paths objectAtIndex:0];
	if (!documentsDirectory) return;
    
    NSString * FolderName = [NSString stringWithUTF8String:filename];
    NSString * FolderPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:FolderName];
    
	NSString * appFile = [FolderPath stringByAppendingPathComponent:@"PlayerInfo"];
    
	NSData * reader = [[NSData alloc] initWithContentsOfFile:appFile];
    if(reader != nil)
	{
		int offset = 0;
		
        char  PlayerUserID[32];
		[reader getBytes:&PlayerUserID range:NSMakeRange(offset, sizeof(PlayerUserID))];
		offset += sizeof(PlayerUserID);
        memcpy(PlayerDataManage::ShareInstance() -> m_PlayerUserID, PlayerUserID, sizeof(PlayerUserID));
        
        char  PlayerName[32];
		[reader getBytes:&PlayerName range:NSMakeRange(offset, sizeof(PlayerName))];
		offset += sizeof(PlayerName);
        memcpy(PlayerDataManage::ShareInstance() -> m_PlayerName, PlayerName, sizeof(PlayerName));
        
        char  PlayerPassword[64];
		[reader getBytes:&PlayerPassword range:NSMakeRange(offset, sizeof(PlayerPassword))];
		offset += sizeof(PlayerPassword);
        memcpy(PlayerDataManage::ShareInstance() -> m_PlayerPassword, PlayerPassword, sizeof(PlayerPassword));
        
        int   PlayerFistGameProfession = 0;
        [reader getBytes:&PlayerFistGameProfession range:NSMakeRange(offset, sizeof(PlayerFistGameProfession))];
        offset += sizeof(PlayerFistGameProfession);
        PlayerDataManage::ShareInstance() -> m_PlayerFistGameProfession = PlayerFistGameProfession;
	}
    [reader release];
}
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
//////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark  储存是否完成了战斗的引导
//////////////////////////////////////////////////////////////////////////////////////////////////
void SaveBattleGuideInfo(bool guide, const char * filename)
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
	if (!documentsDirectory) return;
    
    NSString * FolderName = [NSString stringWithUTF8String:filename];
    NSString * FolderPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:FolderName];
    if([[NSFileManager defaultManager] fileExistsAtPath:FolderPath] == NO)
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:FolderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
	NSString * appFile = [FolderPath stringByAppendingPathComponent:@"BattleGuideInfo"];
    
	NSMutableData * writer = [[NSMutableData alloc] init];
    
    [writer appendBytes:&guide length:sizeof(guide)];
	[writer writeToFile:appFile atomically:YES];
    
	[writer release];
}

void  ReadBattleGuideInfo(const char * filename)
{
	NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString * documentsDirectory = [paths objectAtIndex:0];
	if (!documentsDirectory) return;
    
    NSString * FolderName = [NSString stringWithUTF8String:filename];
    NSString * FolderPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:FolderName];
    
	NSString * appFile = [FolderPath stringByAppendingPathComponent:@"BattleGuideInfo"];
    
	NSData * reader = [[NSData alloc] initWithContentsOfFile:appFile];
    if(reader != nil)
	{
		int offset = 0;
		
		bool guide = false;
		[reader getBytes:&guide range:NSMakeRange(offset, sizeof(guide))];
		offset += sizeof(guide);
        
        PlayerDataManage::m_GuideBattleMark = guide;
	}
    [reader release];
}
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
//////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark 储存是否完成了楼层的引导
//////////////////////////////////////////////////////////////////////////////////////////////////
void  SaveGameGuideInfo(int step, const char * filename)
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
	if (!documentsDirectory) return;
    
    NSString * FolderName = [NSString stringWithUTF8String:filename];
    NSString * FolderPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:FolderName];
    if([[NSFileManager defaultManager] fileExistsAtPath:FolderPath] == NO)
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:FolderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
	NSString * appFile = [FolderPath stringByAppendingPathComponent:@"GameGuideInfo"];
    
	NSMutableData * writer = [[NSMutableData alloc] init];
    
    [writer appendBytes:&step length:sizeof(step)];
	[writer writeToFile:appFile atomically:YES];
    
	[writer release];
}

void  ReadGameGuideInfo(const char * filename)
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString * documentsDirectory = [paths objectAtIndex:0];
	if (!documentsDirectory) return;
    
    NSString * FolderName = [NSString stringWithUTF8String:filename];
    NSString * FolderPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:FolderName];
    
	NSString * appFile = [FolderPath stringByAppendingPathComponent:@"GameGuideInfo"];
    
	NSData * reader = [[NSData alloc] initWithContentsOfFile:appFile];
    if(reader != nil)
	{
		int offset = 0;
		
		int step = 1;
		[reader getBytes:&step range:NSMakeRange(offset, sizeof(step))];
		offset += sizeof(step);
        
        PlayerDataManage::m_GuideStepMark = step;
	}
    [reader release];
}
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
//////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark 购买钻石
//////////////////////////////////////////////////////////////////////////////////////////////////
void    BuyDiamond()
{
    //AppController * controller = (AppController*)[[UIApplication sharedApplication] delegate];
	//[controller BuyDiamond];
}












